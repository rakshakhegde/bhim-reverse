package org.apache.xml.security.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import org.apache.xml.security.utils.I18n;

public class XMLSecurityException extends Exception {
    protected Exception f3777a;
    protected String f3778b;

    public XMLSecurityException() {
        super("Missing message string");
        this.f3777a = null;
        this.f3778b = null;
        this.f3777a = null;
    }

    public XMLSecurityException(String str) {
        super(I18n.m6129b(str));
        this.f3777a = null;
        this.f3778b = str;
        this.f3777a = null;
    }

    public XMLSecurityException(String str, Exception exception) {
        super(I18n.m6126a(str, exception));
        this.f3777a = null;
        this.f3778b = str;
        this.f3777a = exception;
    }

    public XMLSecurityException(String str, Object[] objArr) {
        super(MessageFormat.format(I18n.m6129b(str), objArr));
        this.f3777a = null;
        this.f3778b = str;
        this.f3777a = null;
    }

    public XMLSecurityException(String str, Object[] objArr, Exception exception) {
        super(MessageFormat.format(I18n.m6129b(str), objArr));
        this.f3777a = null;
        this.f3778b = str;
        this.f3777a = exception;
    }

    public void printStackTrace() {
        synchronized (System.err) {
            super.printStackTrace(System.err);
            if (this.f3777a != null) {
                this.f3777a.printStackTrace(System.err);
            }
        }
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this.f3777a != null) {
            this.f3777a.printStackTrace(printStream);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this.f3777a != null) {
            this.f3777a.printStackTrace(printWriter);
        }
    }

    public String toString() {
        String name = getClass().getName();
        String localizedMessage = super.getLocalizedMessage();
        if (localizedMessage != null) {
            name = new StringBuffer().append(name).append(": ").append(localizedMessage).toString();
        }
        return this.f3777a != null ? new StringBuffer().append(name).append("\nOriginal Exception was ").append(this.f3777a.toString()).toString() : name;
    }
}
