package org.apache.xml.security.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import org.apache.xml.security.utils.I18n;

public class XMLSecurityRuntimeException extends RuntimeException {
    protected Exception f3848a;
    protected String f3849b;

    public XMLSecurityRuntimeException() {
        super("Missing message string");
        this.f3848a = null;
        this.f3849b = null;
        this.f3848a = null;
    }

    public XMLSecurityRuntimeException(String str, Exception exception) {
        super(I18n.m6126a(str, exception));
        this.f3848a = null;
        this.f3849b = str;
        this.f3848a = exception;
    }

    public XMLSecurityRuntimeException(String str, Object[] objArr, Exception exception) {
        super(MessageFormat.format(I18n.m6129b(str), objArr));
        this.f3848a = null;
        this.f3849b = str;
        this.f3848a = exception;
    }

    public void printStackTrace() {
        synchronized (System.err) {
            super.printStackTrace(System.err);
            if (this.f3848a != null) {
                this.f3848a.printStackTrace(System.err);
            }
        }
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this.f3848a != null) {
            this.f3848a.printStackTrace(printStream);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this.f3848a != null) {
            this.f3848a.printStackTrace(printWriter);
        }
    }

    public String toString() {
        String name = getClass().getName();
        String localizedMessage = super.getLocalizedMessage();
        if (localizedMessage != null) {
            name = new StringBuffer().append(name).append(": ").append(localizedMessage).toString();
        }
        return this.f3848a != null ? new StringBuffer().append(name).append("\nOriginal Exception was ").append(this.f3848a.toString()).toString() : name;
    }
}
