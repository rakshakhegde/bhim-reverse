package org.apache.xml.security.keys.keyresolver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.storage.StorageResolver;

public class KeyResolver {
    static Log f3866a;
    static boolean f3867b;
    static List f3868c;
    static Class f3869f;
    protected KeyResolverSpi f3870d;
    protected StorageResolver f3871e;

    class ResolverIterator implements Iterator {
        Iterator f3864a;
        int f3865b;

        public boolean hasNext() {
            return this.f3864a.hasNext();
        }

        public Object next() {
            this.f3865b++;
            KeyResolver keyResolver = (KeyResolver) this.f3864a.next();
            if (keyResolver != null) {
                return keyResolver.f3870d;
            }
            throw new RuntimeException("utils.resolver.noClass");
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove resolvers using the iterator");
        }
    }

    static {
        Class b;
        if (f3869f == null) {
            b = m5952b("org.apache.xml.security.keys.keyresolver.KeyResolver");
            f3869f = b;
        } else {
            b = f3869f;
        }
        f3866a = LogFactory.getLog(b.getName());
        f3867b = false;
        f3868c = null;
    }

    private KeyResolver(String str) {
        this.f3870d = null;
        this.f3871e = null;
        this.f3870d = (KeyResolverSpi) Class.forName(str).newInstance();
        this.f3870d.m5953a(true);
    }

    public static void m5950a() {
        if (!f3867b) {
            f3868c = new ArrayList(10);
            f3867b = true;
        }
    }

    public static void m5951a(String str) {
        f3868c.add(new KeyResolver(str));
    }

    static Class m5952b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
