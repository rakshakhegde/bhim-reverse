package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class X509SKIResolver extends KeyResolverSpi {
    static Log f3884c;
    static Class f3885d;

    static {
        Class a;
        if (f3885d == null) {
            a = m5959a("org.apache.xml.security.keys.keyresolver.implementations.X509SKIResolver");
            f3885d = a;
        } else {
            a = f3885d;
        }
        f3884c = LogFactory.getLog(a.getName());
    }

    static Class m5959a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
