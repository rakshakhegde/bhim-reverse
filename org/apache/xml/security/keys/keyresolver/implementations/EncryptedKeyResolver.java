package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class EncryptedKeyResolver extends KeyResolverSpi {
    static Log f3874c;
    static Class f3875d;

    static {
        Class a;
        if (f3875d == null) {
            a = m5954a("org.apache.xml.security.keys.keyresolver.implementations.RSAKeyValueResolver");
            f3875d = a;
        } else {
            a = f3875d;
        }
        f3874c = LogFactory.getLog(a.getName());
    }

    static Class m5954a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
