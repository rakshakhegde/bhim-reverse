package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class X509CertificateResolver extends KeyResolverSpi {
    static Log f3880c;
    static Class f3881d;

    static {
        Class a;
        if (f3881d == null) {
            a = m5957a("org.apache.xml.security.keys.keyresolver.implementations.X509CertificateResolver");
            f3881d = a;
        } else {
            a = f3881d;
        }
        f3880c = LogFactory.getLog(a.getName());
    }

    static Class m5957a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
