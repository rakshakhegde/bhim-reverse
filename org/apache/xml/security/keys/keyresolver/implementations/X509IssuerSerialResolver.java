package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class X509IssuerSerialResolver extends KeyResolverSpi {
    static Log f3882c;
    static Class f3883d;

    static {
        Class a;
        if (f3883d == null) {
            a = m5958a("org.apache.xml.security.keys.keyresolver.implementations.X509IssuerSerialResolver");
            f3883d = a;
        } else {
            a = f3883d;
        }
        f3882c = LogFactory.getLog(a.getName());
    }

    static Class m5958a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
