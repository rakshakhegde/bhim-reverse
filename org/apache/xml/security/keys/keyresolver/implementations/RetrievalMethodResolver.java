package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class RetrievalMethodResolver extends KeyResolverSpi {
    static Log f3878c;
    static Class f3879d;

    static {
        Class a;
        if (f3879d == null) {
            a = m5956a("org.apache.xml.security.keys.keyresolver.implementations.RetrievalMethodResolver");
            f3879d = a;
        } else {
            a = f3879d;
        }
        f3878c = LogFactory.getLog(a.getName());
    }

    static Class m5956a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
