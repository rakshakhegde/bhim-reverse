package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class RSAKeyValueResolver extends KeyResolverSpi {
    static Log f3876c;
    static Class f3877d;

    static {
        Class a;
        if (f3877d == null) {
            a = m5955a("org.apache.xml.security.keys.keyresolver.implementations.RSAKeyValueResolver");
            f3877d = a;
        } else {
            a = f3877d;
        }
        f3876c = LogFactory.getLog(a.getName());
    }

    static Class m5955a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
