package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class X509SubjectNameResolver extends KeyResolverSpi {
    static Log f3886c;
    static Class f3887d;

    static {
        Class a;
        if (f3887d == null) {
            a = m5960a("org.apache.xml.security.keys.keyresolver.implementations.X509SubjectNameResolver");
            f3887d = a;
        } else {
            a = f3887d;
        }
        f3886c = LogFactory.getLog(a.getName());
    }

    static Class m5960a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
