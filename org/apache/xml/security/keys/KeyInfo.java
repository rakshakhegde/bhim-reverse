package org.apache.xml.security.keys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.w3c.dom.Element;

public class KeyInfo extends SignatureElementProxy {
    static Log f3850a;
    static final List f3851d;
    static boolean f3852g;
    static Class f3853h;
    List f3854b;
    List f3855c;
    List f3856e;
    List f3857f;

    static {
        Class a;
        if (f3853h == null) {
            a = m5925a("org.apache.xml.security.keys.KeyInfo");
            f3853h = a;
        } else {
            a = f3853h;
        }
        f3850a = LogFactory.getLog(a.getName());
        List arrayList = new ArrayList();
        arrayList.add(null);
        f3851d = Collections.unmodifiableList(arrayList);
        f3852g = false;
    }

    public KeyInfo(Element element, String str) {
        super(element, str);
        this.f3854b = null;
        this.f3855c = null;
        this.f3856e = null;
        this.f3857f = f3851d;
    }

    static Class m5925a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static void m5926a() {
        if (!f3852g) {
            if (f3850a == null) {
                Class a;
                if (f3853h == null) {
                    a = m5925a("org.apache.xml.security.keys.KeyInfo");
                    f3853h = a;
                } else {
                    a = f3853h;
                }
                f3850a = LogFactory.getLog(a.getName());
                f3850a.error("Had to assign log in the init() function");
            }
            f3852g = true;
        }
    }

    public String m5927e() {
        return "KeyInfo";
    }
}
