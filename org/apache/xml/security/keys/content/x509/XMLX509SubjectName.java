package org.apache.xml.security.keys.content.x509;

import org.apache.xml.security.utils.RFC2253Parser;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509SubjectName extends SignatureElementProxy implements XMLX509DataContent {
    public String m5948a() {
        return RFC2253Parser.m6144a(m5724o());
    }

    public String m5949e() {
        return "X509SubjectName";
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof XMLX509SubjectName)) {
            return false;
        }
        return m5948a().equals(((XMLX509SubjectName) obj).m5948a());
    }

    public int hashCode() {
        return m5948a().hashCode() + 527;
    }
}
