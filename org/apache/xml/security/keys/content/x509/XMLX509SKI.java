package org.apache.xml.security.keys.content.x509;

import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509SKI extends SignatureElementProxy implements XMLX509DataContent {
    static Log f3862a;
    static Class f3863b;

    static {
        Class a;
        if (f3863b == null) {
            a = m5945a("org.apache.xml.security.keys.content.x509.XMLX509SKI");
            f3863b = a;
        } else {
            a = f3863b;
        }
        f3862a = LogFactory.getLog(a.getName());
    }

    static Class m5945a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public byte[] m5946a() {
        return m5723n();
    }

    public String m5947e() {
        return "X509SKI";
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj instanceof XMLX509SKI) {
            try {
                z = Arrays.equals(((XMLX509SKI) obj).m5946a(), m5946a());
            } catch (XMLSecurityException e) {
            }
        }
        return z;
    }

    public int hashCode() {
        int i = 17;
        try {
            byte[] a = m5946a();
            int i2 = 0;
            while (i2 < a.length) {
                int i3 = (i * 31) + a[i2];
                i2++;
                i = i3;
            }
        } catch (XMLSecurityException e) {
        }
        return i;
    }
}
