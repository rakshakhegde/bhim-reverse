package org.apache.xml.security.keys.content.x509;

import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.RFC2253Parser;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509IssuerSerial extends SignatureElementProxy implements XMLX509DataContent {
    static Log f3860a;
    static Class f3861b;

    static {
        Class a;
        if (f3861b == null) {
            a = m5941a("org.apache.xml.security.keys.content.x509.XMLX509IssuerSerial");
            f3861b = a;
        } else {
            a = f3861b;
        }
        f3860a = LogFactory.getLog(a.getName());
    }

    static Class m5941a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public BigInteger m5942a() {
        String b = m5716b("X509SerialNumber", "http://www.w3.org/2000/09/xmldsig#");
        if (f3860a.isDebugEnabled()) {
            f3860a.debug(new StringBuffer().append("X509SerialNumber text: ").append(b).toString());
        }
        return new BigInteger(b);
    }

    public String m5943b() {
        return RFC2253Parser.m6144a(m5716b("X509IssuerName", "http://www.w3.org/2000/09/xmldsig#"));
    }

    public String m5944e() {
        return "X509IssuerSerial";
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof XMLX509IssuerSerial)) {
            return false;
        }
        XMLX509IssuerSerial xMLX509IssuerSerial = (XMLX509IssuerSerial) obj;
        return m5942a().equals(xMLX509IssuerSerial.m5942a()) && m5943b().equals(xMLX509IssuerSerial.m5943b());
    }

    public int hashCode() {
        return ((m5942a().hashCode() + 527) * 31) + m5943b().hashCode();
    }
}
