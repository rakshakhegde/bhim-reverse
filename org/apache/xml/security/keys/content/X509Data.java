package org.apache.xml.security.keys.content;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.SignatureElementProxy;

public class X509Data extends SignatureElementProxy implements KeyInfoContent {
    static Log f3858a;
    static Class f3859b;

    static {
        Class a;
        if (f3859b == null) {
            a = m5934a("org.apache.xml.security.keys.content.X509Data");
            f3859b = a;
        } else {
            a = f3859b;
        }
        f3858a = LogFactory.getLog(a.getName());
    }

    static Class m5934a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public String m5935e() {
        return "X509Data";
    }
}
