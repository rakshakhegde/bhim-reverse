package org.apache.xml.security.keys.content;

import org.apache.xml.security.utils.SignatureElementProxy;

public class MgmtData extends SignatureElementProxy implements KeyInfoContent {
    public String m5930e() {
        return "MgmtData";
    }
}
