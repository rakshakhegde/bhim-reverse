package org.apache.xml.security.keys.storage;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StorageResolver {
    static Log f3890a;
    static Class f3891d;
    List f3892b;
    Iterator f3893c;

    class StorageResolverIterator implements Iterator {
        Iterator f3888a;
        Iterator f3889b;

        private Iterator m5961a() {
            while (this.f3888a.hasNext()) {
                Iterator a = ((StorageResolverSpi) this.f3888a.next()).m5963a();
                if (a.hasNext()) {
                    return a;
                }
            }
            return null;
        }

        public boolean hasNext() {
            if (this.f3889b == null) {
                return false;
            }
            if (this.f3889b.hasNext()) {
                return true;
            }
            this.f3889b = m5961a();
            return this.f3889b != null;
        }

        public Object next() {
            if (hasNext()) {
                return this.f3889b.next();
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove keys from KeyStore");
        }
    }

    static {
        Class a;
        if (f3891d == null) {
            a = m5962a("org.apache.xml.security.keys.storage.StorageResolver");
            f3891d = a;
        } else {
            a = f3891d;
        }
        f3890a = LogFactory.getLog(a.getName());
    }

    public StorageResolver() {
        this.f3892b = null;
        this.f3893c = null;
    }

    static Class m5962a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
