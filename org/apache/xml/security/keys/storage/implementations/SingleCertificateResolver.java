package org.apache.xml.security.keys.storage.implementations;

import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.xml.security.keys.storage.StorageResolverSpi;

public class SingleCertificateResolver extends StorageResolverSpi {
    X509Certificate f3906a;

    class InternalIterator implements Iterator {
        boolean f3904a;
        X509Certificate f3905b;

        public InternalIterator(X509Certificate x509Certificate) {
            this.f3904a = false;
            this.f3905b = null;
            this.f3905b = x509Certificate;
        }

        public boolean hasNext() {
            return !this.f3904a;
        }

        public Object next() {
            if (this.f3904a) {
                throw new NoSuchElementException();
            }
            this.f3904a = true;
            return this.f3905b;
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove keys from KeyStore");
        }
    }

    public Iterator m5968a() {
        return new InternalIterator(this.f3906a);
    }
}
