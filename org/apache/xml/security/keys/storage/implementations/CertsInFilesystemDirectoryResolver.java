package org.apache.xml.security.keys.storage.implementations;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.storage.StorageResolverSpi;

public class CertsInFilesystemDirectoryResolver extends StorageResolverSpi {
    static Log f3896a;
    static Class f3897b;
    private List f3898c;

    class FilesystemIterator implements Iterator {
        List f3894a;
        int f3895b;

        public FilesystemIterator(List list) {
            this.f3894a = null;
            this.f3894a = list;
            this.f3895b = 0;
        }

        public boolean hasNext() {
            return this.f3895b < this.f3894a.size();
        }

        public Object next() {
            List list = this.f3894a;
            int i = this.f3895b;
            this.f3895b = i + 1;
            return list.get(i);
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove keys from KeyStore");
        }
    }

    static {
        Class a;
        if (f3897b == null) {
            a = m5964a("org.apache.xml.security.keys.storage.implementations.CertsInFilesystemDirectoryResolver");
            f3897b = a;
        } else {
            a = f3897b;
        }
        f3896a = LogFactory.getLog(a.getName());
    }

    static Class m5964a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public Iterator m5965a() {
        return new FilesystemIterator(this.f3898c);
    }
}
