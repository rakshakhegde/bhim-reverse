package org.apache.xml.security.keys.storage.implementations;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.xml.security.keys.storage.StorageResolverSpi;

public class KeyStoreResolver extends StorageResolverSpi {
    KeyStore f3903a;

    /* renamed from: org.apache.xml.security.keys.storage.implementations.KeyStoreResolver.1 */
    class C06121 implements Enumeration {
        private final KeyStoreIterator f3899a;

        C06121(KeyStoreIterator keyStoreIterator) {
            this.f3899a = keyStoreIterator;
        }

        public boolean hasMoreElements() {
            return false;
        }

        public Object nextElement() {
            return null;
        }
    }

    class KeyStoreIterator implements Iterator {
        KeyStore f3900a;
        Enumeration f3901b;
        Certificate f3902c;

        public KeyStoreIterator(KeyStore keyStore) {
            this.f3900a = null;
            this.f3901b = null;
            this.f3902c = null;
            try {
                this.f3900a = keyStore;
                this.f3901b = this.f3900a.aliases();
            } catch (KeyStoreException e) {
                this.f3901b = new C06121(this);
            }
        }

        private Certificate m5966a() {
            while (this.f3901b.hasMoreElements()) {
                try {
                    Certificate certificate = this.f3900a.getCertificate((String) this.f3901b.nextElement());
                    if (certificate != null) {
                        return certificate;
                    }
                } catch (KeyStoreException e) {
                    return null;
                }
            }
            return null;
        }

        public boolean hasNext() {
            if (this.f3902c == null) {
                this.f3902c = m5966a();
            }
            return this.f3902c != null;
        }

        public Object next() {
            if (this.f3902c == null) {
                this.f3902c = m5966a();
                if (this.f3902c == null) {
                    throw new NoSuchElementException();
                }
            }
            Certificate certificate = this.f3902c;
            this.f3902c = null;
            return certificate;
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove keys from KeyStore");
        }
    }

    public Iterator m5967a() {
        return new KeyStoreIterator(this.f3903a);
    }
}
