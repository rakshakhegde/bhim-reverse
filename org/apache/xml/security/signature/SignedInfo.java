package org.apache.xml.security.signature;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class SignedInfo extends Manifest {
    private SignatureAlgorithm f3923g;
    private byte[] f3924h;
    private Element f3925i;
    private Element f3926q;

    public SignedInfo(Element element, String str) {
        super(m5992a(element), str);
        this.f3923g = null;
        this.f3924h = null;
        this.f3925i = XMLUtils.m6160a(element.getFirstChild());
        this.f3926q = XMLUtils.m6160a(this.f3925i.getNextSibling());
        this.f3923g = new SignatureAlgorithm(this.f3926q, m5721l());
    }

    private static Element m5992a(Element element) {
        String attributeNS = XMLUtils.m6160a(element.getFirstChild()).getAttributeNS(null, "Algorithm");
        if (attributeNS.equals("http://www.w3.org/TR/2001/REC-xml-c14n-20010315") || attributeNS.equals("http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments") || attributeNS.equals("http://www.w3.org/2001/10/xml-exc-c14n#") || attributeNS.equals("http://www.w3.org/2001/10/xml-exc-c14n#WithComments") || attributeNS.equals("http://www.w3.org/2006/12/xml-c14n11") || attributeNS.equals("http://www.w3.org/2006/12/xml-c14n11#WithComments")) {
            return element;
        }
        try {
            byte[] a = Canonicalizer.m5829a(attributeNS).m5834a((Node) element);
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            newInstance.setNamespaceAware(true);
            Node importNode = element.getOwnerDocument().importNode(newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(a)).getDocumentElement(), true);
            element.getParentNode().replaceChild(importNode, element);
            return (Element) importNode;
        } catch (Exception e) {
            throw new XMLSecurityException("empty", e);
        } catch (Exception e2) {
            throw new XMLSecurityException("empty", e2);
        } catch (Exception e22) {
            throw new XMLSecurityException("empty", e22);
        }
    }

    public void m5993a(OutputStream outputStream) {
        if (this.f3924h == null) {
            Canonicalizer a = Canonicalizer.m5829a(m5994b());
            a.m5833a(outputStream);
            String f = m5998f();
            if (f == null) {
                a.m5834a(this.k);
                return;
            } else {
                a.m5835a(this.k, f);
                return;
            }
        }
        try {
            outputStream.write(this.f3924h);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public String m5994b() {
        return this.f3925i.getAttributeNS(null, "Algorithm");
    }

    public boolean m5995b(boolean z) {
        return super.m5972a(z);
    }

    protected SignatureAlgorithm m5996c() {
        return this.f3923g;
    }

    public String m5997e() {
        return "SignedInfo";
    }

    public String m5998f() {
        String str = null;
        String attributeNS = this.f3925i.getAttributeNS(str, "Algorithm");
        if (attributeNS.equals("http://www.w3.org/2001/10/xml-exc-c14n#") || attributeNS.equals("http://www.w3.org/2001/10/xml-exc-c14n#WithComments")) {
            Element a = XMLUtils.m6160a(this.f3925i.getFirstChild());
            if (a != null) {
                try {
                    str = new InclusiveNamespaces(a, "http://www.w3.org/2001/10/xml-exc-c14n#").m6082a();
                } catch (XMLSecurityException e) {
                }
            }
        }
        return str;
    }
}
