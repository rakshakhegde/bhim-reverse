package org.apache.xml.security.signature;

import java.io.Writer;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.w3c.dom.Document;

public class XMLSignatureInputDebugger {
    static final AttrCompare f3948a;
    private Document f3949b;
    private Writer f3950c;

    static {
        f3948a = new AttrCompare();
    }

    private XMLSignatureInputDebugger() {
        this.f3949b = null;
        this.f3950c = null;
    }
}
