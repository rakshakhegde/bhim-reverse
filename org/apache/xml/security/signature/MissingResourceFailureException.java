package org.apache.xml.security.signature;

public class MissingResourceFailureException extends XMLSignatureException {
    Reference f3914c;

    public MissingResourceFailureException(String str, Reference reference) {
        super(str);
        this.f3914c = null;
        this.f3914c = reference;
    }

    public MissingResourceFailureException(String str, Object[] objArr, Exception exception, Reference reference) {
        super(str, objArr, exception);
        this.f3914c = null;
        this.f3914c = reference;
    }
}
