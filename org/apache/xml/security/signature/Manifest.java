package org.apache.xml.security.signature;

import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.utils.I18n;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Manifest extends SignatureElementProxy {
    static Log f3907a;
    static Class f3908f;
    List f3909b;
    Element[] f3910c;
    HashMap f3911d;
    List f3912e;
    private boolean[] f3913g;

    static {
        Class a;
        if (f3908f == null) {
            a = m5969a("org.apache.xml.security.signature.Manifest");
            f3908f = a;
        } else {
            a = f3908f;
        }
        f3907a = LogFactory.getLog(a.getName());
    }

    public Manifest(Element element, String str) {
        int i = 0;
        super(element, str);
        this.f3913g = null;
        this.f3911d = null;
        this.f3912e = null;
        this.f3910c = XMLUtils.m6167a(this.k.getFirstChild(), "Reference");
        int length = this.f3910c.length;
        if (length == 0) {
            throw new DOMException((short) 4, I18n.m6127a("xml.WrongContent", new Object[]{"Reference", "Manifest"}));
        }
        this.f3909b = new ArrayList(length);
        while (i < length) {
            this.f3909b.add(null);
            i++;
        }
    }

    static Class m5969a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private void m5970a(int i, boolean z) {
        if (this.f3913g == null) {
            this.f3913g = new boolean[m5971a()];
        }
        this.f3913g[i] = z;
    }

    public int m5971a() {
        return this.f3909b.size();
    }

    public boolean m5972a(boolean z) {
        if (this.f3910c == null) {
            this.f3910c = XMLUtils.m6167a(this.k.getFirstChild(), "Reference");
        }
        if (f3907a.isDebugEnabled()) {
            f3907a.debug(new StringBuffer().append("verify ").append(this.f3910c.length).append(" References").toString());
            f3907a.debug(new StringBuffer().append("I am ").append(z ? BuildConfig.FLAVOR : "not").append(" requested to follow nested Manifests").toString());
        }
        if (this.f3910c.length == 0) {
            throw new XMLSecurityException("empty");
        }
        this.f3913g = new boolean[this.f3910c.length];
        int i = 0;
        boolean z2 = true;
        while (i < this.f3910c.length) {
            Reference reference = new Reference(this.f3910c[i], this.l, this);
            this.f3909b.set(i, reference);
            try {
                boolean j = reference.m5989j();
                m5970a(i, j);
                boolean z3 = !j ? false : z2;
                if (f3907a.isDebugEnabled()) {
                    f3907a.debug(new StringBuffer().append("The Reference has Type ").append(reference.m5983c()).toString());
                }
                if (z3 && z && reference.m5985f()) {
                    Manifest manifest;
                    f3907a.debug("We have to follow a nested Manifest");
                    XMLSignatureInput a = reference.m5981a(null);
                    for (Node node : a.m6012b()) {
                        if (node.getNodeType() == (short) 1) {
                            if (((Element) node).getNamespaceURI().equals("http://www.w3.org/2000/09/xmldsig#")) {
                                if (((Element) node).getLocalName().equals("Manifest")) {
                                    try {
                                        manifest = new Manifest((Element) node, a.m6026k());
                                        break;
                                    } catch (XMLSecurityException e) {
                                    }
                                } else {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    manifest = null;
                    if (manifest == null) {
                        throw new MissingResourceFailureException("empty", reference);
                    }
                    manifest.f3912e = this.f3912e;
                    manifest.f3911d = this.f3911d;
                    if (manifest.m5972a(z)) {
                        f3907a.debug("The nested Manifest was valid (good)");
                        z2 = z3;
                    } else {
                        f3907a.warn("The nested Manifest was invalid (bad)");
                        z2 = false;
                    }
                    j = z2;
                } else {
                    j = z3;
                }
                i++;
                z2 = j;
            } catch (Exception e2) {
                throw new ReferenceNotInitializedException("empty", e2);
            } catch (Exception e22) {
                throw new ReferenceNotInitializedException("empty", e22);
            } catch (Exception e222) {
                throw new ReferenceNotInitializedException("empty", e222);
            } catch (Exception e2222) {
                throw new MissingResourceFailureException("signature.Verification.Reference.NoInput", new Object[]{reference.m5982b()}, e2222, reference);
            }
        }
        return z2;
    }

    public String m5973e() {
        return "Manifest";
    }
}
