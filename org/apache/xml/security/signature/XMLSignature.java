package org.apache.xml.security.signature;

import java.io.IOException;
import java.io.OutputStream;
import java.security.Key;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.SignerOutputStream;
import org.apache.xml.security.utils.UnsyncBufferedOutputStream;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;

public final class XMLSignature extends SignatureElementProxy {
    static Log f3927a;
    static Class f3928b;
    private SignedInfo f3929c;
    private KeyInfo f3930d;
    private boolean f3931e;
    private Element f3932f;
    private int f3933g;

    static {
        Class a;
        if (f3928b == null) {
            a = m5999a("org.apache.xml.security.signature.XMLSignature");
            f3928b = a;
        } else {
            a = f3928b;
        }
        f3927a = LogFactory.getLog(a.getName());
    }

    public XMLSignature(Element element, String str) {
        super(element, str);
        this.f3929c = null;
        this.f3930d = null;
        this.f3931e = false;
        this.f3933g = 0;
        Element a = XMLUtils.m6160a(element.getFirstChild());
        if (a == null) {
            throw new XMLSignatureException("xml.WrongContent", new Object[]{"SignedInfo", "Signature"});
        }
        this.f3929c = new SignedInfo(a, str);
        this.f3932f = XMLUtils.m6160a(XMLUtils.m6160a(element.getFirstChild()).getNextSibling());
        if (this.f3932f == null) {
            throw new XMLSignatureException("xml.WrongContent", new Object[]{"SignatureValue", "Signature"});
        }
        a = XMLUtils.m6160a(this.f3932f.getNextSibling());
        if (a != null && a.getNamespaceURI().equals("http://www.w3.org/2000/09/xmldsig#") && a.getLocalName().equals("KeyInfo")) {
            this.f3930d = new KeyInfo(a, str);
        }
        this.f3933g = 1;
    }

    static Class m5999a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public SignedInfo m6000a() {
        return this.f3929c;
    }

    public boolean m6001a(Key key) {
        SignatureAlgorithm c;
        boolean z = false;
        if (key == null) {
            throw new XMLSignatureException("empty", new Object[]{"Didn't get a key"});
        }
        try {
            SignedInfo a = m6000a();
            c = a.m5996c();
            if (f3927a.isDebugEnabled()) {
                f3927a.debug(new StringBuffer().append("SignatureMethodURI = ").append(c.m5726a()).toString());
                f3927a.debug(new StringBuffer().append("jceSigAlgorithm    = ").append(c.m5754b()).toString());
                f3927a.debug(new StringBuffer().append("jceSigProvider     = ").append(c.m5756c()).toString());
                f3927a.debug(new StringBuffer().append("PublicKey = ").append(key).toString());
            }
            byte[] bArr = null;
            c.m5751a(key);
            OutputStream unsyncBufferedOutputStream = new UnsyncBufferedOutputStream(new SignerOutputStream(c));
            a.m5993a(unsyncBufferedOutputStream);
            unsyncBufferedOutputStream.close();
            bArr = m6002b();
        } catch (IOException e) {
            c.m5759f();
        } catch (XMLSecurityException e2) {
            c.m5759f();
            throw e2;
        } catch (XMLSignatureException e3) {
            throw e3;
        } catch (Exception e4) {
            throw new XMLSignatureException("empty", e4);
        }
        if (c.m5755b(bArr)) {
            z = a.m5995b(this.f3931e);
        } else {
            f3927a.warn("Signature verification failed.");
        }
        return z;
    }

    public byte[] m6002b() {
        try {
            return Base64.m6105a(this.f3932f);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    public String m6003e() {
        return "Signature";
    }
}
