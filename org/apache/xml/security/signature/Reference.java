package org.apache.xml.security.signature;

import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.DigesterOutputStream;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.UnsyncBufferedOutputStream;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xml.security.utils.resolver.ResourceResolver;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

public class Reference extends SignatureElementProxy {
    static Log f3915a;
    static Class f3916d;
    private static boolean f3917e;
    Manifest f3918b;
    XMLSignatureInput f3919c;
    private Transforms f3920f;
    private Element f3921g;
    private Element f3922h;

    /* renamed from: org.apache.xml.security.signature.Reference.1 */
    class C06131 implements PrivilegedAction {
        C06131() {
        }

        public Object run() {
            return Boolean.valueOf(Boolean.getBoolean("org.apache.xml.security.useC14N11"));
        }
    }

    static {
        Class a;
        f3917e = ((Boolean) AccessController.doPrivileged(new C06131())).booleanValue();
        if (f3916d == null) {
            a = m5977a("org.apache.xml.security.signature.Reference");
            f3916d = a;
        } else {
            a = f3916d;
        }
        f3915a = LogFactory.getLog(a.getName());
    }

    protected Reference(Element element, String str, Manifest manifest) {
        super(element, str);
        this.f3918b = null;
        this.l = str;
        Element a = XMLUtils.m6160a(element.getFirstChild());
        if ("Transforms".equals(a.getLocalName()) && "http://www.w3.org/2000/09/xmldsig#".equals(a.getNamespaceURI())) {
            this.f3920f = new Transforms(a, this.l);
            a = XMLUtils.m6160a(a.getNextSibling());
        }
        this.f3921g = a;
        this.f3922h = XMLUtils.m6160a(this.f3921g.getNextSibling());
        this.f3918b = manifest;
    }

    static Class m5977a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private XMLSignatureInput m5978a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream) {
        try {
            Transforms h = m5987h();
            if (h == null) {
                return xMLSignatureInput;
            }
            xMLSignatureInput = h.m5918a(xMLSignatureInput, outputStream);
            this.f3919c = xMLSignatureInput;
            return xMLSignatureInput;
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        } catch (Exception e2) {
            throw new XMLSignatureException("empty", e2);
        } catch (Exception e22) {
            throw new XMLSignatureException("empty", e22);
        } catch (Exception e222) {
            throw new XMLSignatureException("empty", e222);
        } catch (Exception e2222) {
            throw new XMLSignatureException("empty", e2222);
        }
    }

    private byte[] m5979a(boolean z) {
        try {
            MessageDigestAlgorithm a = m5980a();
            a.m5739c();
            OutputStream digesterOutputStream = new DigesterOutputStream(a);
            OutputStream unsyncBufferedOutputStream = new UnsyncBufferedOutputStream(digesterOutputStream);
            XMLSignatureInput a2 = m5981a(unsyncBufferedOutputStream);
            if (!f3917e || z || a2.m6024i() || a2.m6023h()) {
                a2.m6005a(unsyncBufferedOutputStream);
            } else {
                if (this.f3920f == null) {
                    this.f3920f = new Transforms(this.m);
                    this.k.insertBefore(this.f3920f.m5720k(), this.f3921g);
                }
                this.f3920f.m5920a("http://www.w3.org/2006/12/xml-c14n11");
                a2.m6006a(unsyncBufferedOutputStream, true);
            }
            unsyncBufferedOutputStream.flush();
            return digesterOutputStream.m6124a();
        } catch (Exception e) {
            throw new ReferenceNotInitializedException("empty", e);
        } catch (Exception e2) {
            throw new ReferenceNotInitializedException("empty", e2);
        }
    }

    public MessageDigestAlgorithm m5980a() {
        if (this.f3921g == null) {
            return null;
        }
        String attributeNS = this.f3921g.getAttributeNS(null, "Algorithm");
        return attributeNS != null ? MessageDigestAlgorithm.m5733a(this.m, attributeNS) : null;
    }

    protected XMLSignatureInput m5981a(OutputStream outputStream) {
        try {
            XMLSignatureInput a = m5978a(m5986g(), outputStream);
            this.f3919c = a;
            return a;
        } catch (Exception e) {
            throw new ReferenceNotInitializedException("empty", e);
        }
    }

    public String m5982b() {
        return this.k.getAttributeNS(null, "URI");
    }

    public String m5983c() {
        return this.k.getAttributeNS(null, "Type");
    }

    public String m5984e() {
        return "Reference";
    }

    public boolean m5985f() {
        return "http://www.w3.org/2000/09/xmldsig#Manifest".equals(m5983c());
    }

    public XMLSignatureInput m5986g() {
        String str = null;
        try {
            Attr attributeNodeNS = this.k.getAttributeNodeNS(null, "URI");
            if (attributeNodeNS != null) {
                str = attributeNodeNS.getNodeValue();
            }
            ResourceResolver a = ResourceResolver.m6175a(attributeNodeNS, this.l, this.f3918b.f3912e);
            if (a == null) {
                throw new ReferenceNotInitializedException("signature.Verification.Reference.NoInput", new Object[]{str});
            }
            a.m6181a(this.f3918b.f3911d);
            return a.m6182b(attributeNodeNS, this.l);
        } catch (Exception e) {
            throw new ReferenceNotInitializedException("empty", e);
        } catch (Exception e2) {
            throw new ReferenceNotInitializedException("empty", e2);
        }
    }

    public Transforms m5987h() {
        return this.f3920f;
    }

    public byte[] m5988i() {
        if (this.f3922h != null) {
            return Base64.m6105a(this.f3922h);
        }
        throw new XMLSecurityException("signature.Verification.NoSignatureElement", new Object[]{"DigestValue", "http://www.w3.org/2000/09/xmldsig#"});
    }

    public boolean m5989j() {
        byte[] i = m5988i();
        byte[] a = m5979a(true);
        boolean a2 = MessageDigestAlgorithm.m5734a(i, a);
        if (a2) {
            f3915a.debug(new StringBuffer().append("Verification successful for URI \"").append(m5982b()).append("\"").toString());
        } else {
            f3915a.warn(new StringBuffer().append("Verification failed for URI \"").append(m5982b()).append("\"").toString());
            f3915a.warn(new StringBuffer().append("Expected Digest: ").append(Base64.m6107b(i)).toString());
            f3915a.warn(new StringBuffer().append("Actual Digest: ").append(Base64.m6107b(a)).toString());
        }
        return a2;
    }
}
