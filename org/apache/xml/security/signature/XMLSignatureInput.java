package org.apache.xml.security.signature;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.implementations.Canonicalizer11_OmitComments;
import org.apache.xml.security.c14n.implementations.Canonicalizer20010315OmitComments;
import org.apache.xml.security.c14n.implementations.CanonicalizerBase;
import org.apache.xml.security.exceptions.XMLSecurityRuntimeException;
import org.apache.xml.security.utils.IgnoreAllErrorHandler;
import org.apache.xml.security.utils.JavaUtils;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XMLSignatureInput implements Cloneable {
    static Log f3934a;
    static Class f3935l;
    InputStream f3936b;
    Set f3937c;
    Node f3938d;
    Node f3939e;
    boolean f3940f;
    boolean f3941g;
    byte[] f3942h;
    List f3943i;
    boolean f3944j;
    OutputStream f3945k;
    private String f3946m;
    private String f3947n;

    static {
        Class c;
        if (f3935l == null) {
            c = m6004c("org.apache.xml.security.signature.XMLSignatureInput");
            f3935l = c;
        } else {
            c = f3935l;
        }
        f3934a = LogFactory.getLog(c.getName());
    }

    public XMLSignatureInput(InputStream inputStream) {
        this.f3936b = null;
        this.f3937c = null;
        this.f3938d = null;
        this.f3939e = null;
        this.f3940f = false;
        this.f3941g = false;
        this.f3942h = null;
        this.f3946m = null;
        this.f3947n = null;
        this.f3943i = new ArrayList();
        this.f3944j = false;
        this.f3945k = null;
        this.f3936b = inputStream;
    }

    public XMLSignatureInput(Node node) {
        this.f3936b = null;
        this.f3937c = null;
        this.f3938d = null;
        this.f3939e = null;
        this.f3940f = false;
        this.f3941g = false;
        this.f3942h = null;
        this.f3946m = null;
        this.f3947n = null;
        this.f3943i = new ArrayList();
        this.f3944j = false;
        this.f3945k = null;
        this.f3938d = node;
    }

    public XMLSignatureInput(byte[] bArr) {
        this.f3936b = null;
        this.f3937c = null;
        this.f3938d = null;
        this.f3939e = null;
        this.f3940f = false;
        this.f3941g = false;
        this.f3942h = null;
        this.f3946m = null;
        this.f3947n = null;
        this.f3943i = new ArrayList();
        this.f3944j = false;
        this.f3945k = null;
        this.f3942h = bArr;
    }

    static Class m6004c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public void m6005a(OutputStream outputStream) {
        m6006a(outputStream, false);
    }

    public void m6006a(OutputStream outputStream, boolean z) {
        if (outputStream != this.f3945k) {
            if (this.f3942h != null) {
                outputStream.write(this.f3942h);
            } else if (this.f3936b == null) {
                CanonicalizerBase canonicalizer11_OmitComments = z ? new Canonicalizer11_OmitComments() : new Canonicalizer20010315OmitComments();
                canonicalizer11_OmitComments.m5854a(outputStream);
                canonicalizer11_OmitComments.m5863b(this);
            } else if (this.f3936b instanceof FileInputStream) {
                byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
                while (true) {
                    int read = this.f3936b.read(bArr);
                    if (read != -1) {
                        outputStream.write(bArr, 0, read);
                    } else {
                        return;
                    }
                }
            } else {
                InputStream o = m6030o();
                if (this.f3942h != null) {
                    outputStream.write(this.f3942h, 0, this.f3942h.length);
                    return;
                }
                o.reset();
                byte[] bArr2 = new byte[1024];
                while (true) {
                    int read2 = o.read(bArr2);
                    if (read2 > 0) {
                        outputStream.write(bArr2, 0, read2);
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void m6007a(String str) {
        this.f3946m = str;
    }

    public void m6008a(NodeFilter nodeFilter) {
        if (m6023h()) {
            try {
                m6032q();
            } catch (Exception e) {
                throw new XMLSecurityRuntimeException("signature.XMLSignatureInput.nodesetReference", e);
            }
        }
        this.f3943i.add(nodeFilter);
    }

    public void m6009a(Node node) {
        this.f3939e = node;
    }

    public void m6010a(boolean z) {
        this.f3944j = z;
    }

    public boolean m6011a() {
        return this.f3944j;
    }

    public Set m6012b() {
        return m6013b(false);
    }

    public Set m6013b(boolean z) {
        if (this.f3937c != null) {
            return this.f3937c;
        }
        if (this.f3936b == null && this.f3938d != null) {
            if (z) {
                XMLUtils.m6163a(XMLUtils.m6169b(this.f3938d));
            }
            this.f3937c = new HashSet();
            XMLUtils.m6164a(this.f3938d, this.f3937c, this.f3939e, this.f3940f);
            return this.f3937c;
        } else if (m6023h()) {
            m6032q();
            Set hashSet = new HashSet();
            XMLUtils.m6164a(this.f3938d, hashSet, null, false);
            return hashSet;
        } else {
            throw new RuntimeException("getNodeSet() called but no input data present");
        }
    }

    public void m6014b(OutputStream outputStream) {
        this.f3945k = outputStream;
    }

    public void m6015b(String str) {
        this.f3947n = str;
    }

    public InputStream m6016c() {
        return this.f3936b instanceof FileInputStream ? this.f3936b : m6030o();
    }

    public void m6017c(boolean z) {
        this.f3940f = z;
    }

    public InputStream m6018d() {
        return this.f3936b;
    }

    public void m6019d(boolean z) {
        this.f3941g = z;
    }

    public byte[] m6020e() {
        if (this.f3942h != null) {
            return this.f3942h;
        }
        InputStream o = m6030o();
        if (o != null) {
            if (this.f3942h == null) {
                o.reset();
                this.f3942h = JavaUtils.m6142a(o);
            }
            return this.f3942h;
        }
        this.f3942h = new Canonicalizer20010315OmitComments().m5863b(this);
        return this.f3942h;
    }

    public boolean m6021f() {
        return (this.f3936b == null && this.f3937c != null) || this.f3941g;
    }

    public boolean m6022g() {
        return this.f3936b == null && this.f3938d != null && this.f3937c == null && !this.f3941g;
    }

    public boolean m6023h() {
        return !(this.f3936b == null && this.f3942h == null) && this.f3937c == null && this.f3938d == null;
    }

    public boolean m6024i() {
        return this.f3945k != null;
    }

    public boolean m6025j() {
        return this.f3942h != null && this.f3937c == null && this.f3938d == null;
    }

    public String m6026k() {
        return this.f3947n;
    }

    public Node m6027l() {
        return this.f3939e;
    }

    public Node m6028m() {
        return this.f3938d;
    }

    public boolean m6029n() {
        return this.f3940f;
    }

    protected InputStream m6030o() {
        if (this.f3936b instanceof ByteArrayInputStream) {
            if (this.f3936b.markSupported()) {
                return this.f3936b;
            }
            throw new RuntimeException(new StringBuffer().append("Accepted as Markable but not truly been").append(this.f3936b).toString());
        } else if (this.f3942h != null) {
            this.f3936b = new ByteArrayInputStream(this.f3942h);
            return this.f3936b;
        } else if (this.f3936b == null) {
            return null;
        } else {
            if (this.f3936b.markSupported()) {
                f3934a.info("Mark Suported but not used as reset");
            }
            this.f3942h = JavaUtils.m6142a(this.f3936b);
            this.f3936b.close();
            this.f3936b = new ByteArrayInputStream(this.f3942h);
            return this.f3936b;
        }
    }

    public List m6031p() {
        return this.f3943i;
    }

    void m6032q() {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        newInstance.setValidating(false);
        newInstance.setNamespaceAware(true);
        DocumentBuilder newDocumentBuilder = newInstance.newDocumentBuilder();
        try {
            newDocumentBuilder.setErrorHandler(new IgnoreAllErrorHandler());
            this.f3938d = newDocumentBuilder.parse(m6016c());
        } catch (SAXException e) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write("<container>".getBytes());
            byteArrayOutputStream.write(m6020e());
            byteArrayOutputStream.write("</container>".getBytes());
            this.f3938d = newDocumentBuilder.parse(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).getDocumentElement().getFirstChild().getFirstChild();
        }
        this.f3936b = null;
        this.f3942h = null;
    }

    public String toString() {
        if (m6021f()) {
            return new StringBuffer().append("XMLSignatureInput/NodeSet/").append(this.f3937c.size()).append(" nodes/").append(m6026k()).toString();
        }
        if (m6022g()) {
            return new StringBuffer().append("XMLSignatureInput/Element/").append(this.f3938d).append(" exclude ").append(this.f3939e).append(" comments:").append(this.f3940f).append("/").append(m6026k()).toString();
        }
        try {
            return new StringBuffer().append("XMLSignatureInput/OctetStream/").append(m6020e().length).append(" octets/").append(m6026k()).toString();
        } catch (IOException e) {
            return new StringBuffer().append("XMLSignatureInput/OctetStream//").append(m6026k()).toString();
        } catch (CanonicalizationException e2) {
            return new StringBuffer().append("XMLSignatureInput/OctetStream//").append(m6026k()).toString();
        }
    }
}
