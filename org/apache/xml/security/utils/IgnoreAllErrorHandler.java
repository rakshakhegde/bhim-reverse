package org.apache.xml.security.utils;

import com.crashlytics.android.core.BuildConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class IgnoreAllErrorHandler implements ErrorHandler {
    static Log f4014a;
    static final boolean f4015b;
    static final boolean f4016c;
    static Class f4017d;

    static {
        Class a;
        if (f4017d == null) {
            a = m6140a("org.apache.xml.security.utils.IgnoreAllErrorHandler");
            f4017d = a;
        } else {
            a = f4017d;
        }
        f4014a = LogFactory.getLog(a.getName());
        f4015b = System.getProperty("org.apache.xml.security.test.warn.on.exceptions", "false").equals("true");
        f4016c = System.getProperty("org.apache.xml.security.test.throw.exceptions", "false").equals("true");
    }

    static Class m6140a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public void error(SAXParseException sAXParseException) {
        if (f4015b) {
            f4014a.error(BuildConfig.FLAVOR, sAXParseException);
        }
        if (f4016c) {
            throw sAXParseException;
        }
    }

    public void fatalError(SAXParseException sAXParseException) {
        if (f4015b) {
            f4014a.warn(BuildConfig.FLAVOR, sAXParseException);
        }
        if (f4016c) {
            throw sAXParseException;
        }
    }

    public void warning(SAXParseException sAXParseException) {
        if (f4015b) {
            f4014a.warn(BuildConfig.FLAVOR, sAXParseException);
        }
        if (f4016c) {
            throw sAXParseException;
        }
    }
}
