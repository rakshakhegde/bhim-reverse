package org.apache.xml.security.utils;

import com.crashlytics.android.core.BuildConfig;
import java.io.OutputStream;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class Base64 {
    private static final byte[] f3976a;
    private static final char[] f3977b;

    static {
        int i;
        int i2 = 0;
        f3976a = new byte[255];
        f3977b = new char[64];
        for (i = 0; i < 255; i++) {
            f3976a[i] = (byte) -1;
        }
        for (i = 90; i >= 65; i--) {
            f3976a[i] = (byte) (i - 65);
        }
        for (i = 122; i >= 97; i--) {
            f3976a[i] = (byte) ((i - 97) + 26);
        }
        for (i = 57; i >= 48; i--) {
            f3976a[i] = (byte) ((i - 48) + 52);
        }
        f3976a[43] = (byte) 62;
        f3976a[47] = (byte) 63;
        for (i = 0; i <= 25; i++) {
            f3977b[i] = (char) (i + 65);
        }
        int i3 = 26;
        i = 0;
        while (i3 <= 51) {
            f3977b[i3] = (char) (i + 97);
            i3++;
            i++;
        }
        i = 52;
        while (i <= 61) {
            f3977b[i] = (char) (i2 + 48);
            i++;
            i2++;
        }
        f3977b[62] = '+';
        f3977b[63] = '/';
    }

    private Base64() {
    }

    protected static final int m6097a(String str, byte[] bArr) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3;
            byte charAt = (byte) str.charAt(i);
            if (m6103a(charAt)) {
                i3 = i2;
            } else {
                i3 = i2 + 1;
                bArr[i2] = charAt;
            }
            i++;
            i2 = i3;
        }
        return i2;
    }

    public static final String m6098a(byte[] bArr, int i) {
        if (i < 4) {
            i = Integer.MAX_VALUE;
        }
        if (bArr == null) {
            return null;
        }
        int length = bArr.length * 8;
        if (length == 0) {
            return BuildConfig.FLAVOR;
        }
        int i2;
        int i3;
        int i4 = length % 24;
        int i5 = length / 24;
        length = i4 != 0 ? i5 + 1 : i5;
        int i6 = (length - 1) / (i / 4);
        char[] cArr = new char[((length * 4) + i6)];
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        length = 0;
        while (i9 < i6) {
            i2 = 0;
            int i10 = length;
            int i11 = i7;
            while (i2 < 19) {
                length = i8 + 1;
                byte b = bArr[i8];
                i7 = length + 1;
                byte b2 = bArr[length];
                i3 = i7 + 1;
                byte b3 = bArr[i7];
                byte b4 = (byte) (b2 & 15);
                byte b5 = (byte) (b & 3);
                if ((b & -128) == 0) {
                    i7 = (byte) (b >> 2);
                } else {
                    byte b6 = (byte) ((b >> 2) ^ 192);
                }
                if ((b2 & -128) == 0) {
                    i8 = (byte) (b2 >> 4);
                } else {
                    b = (byte) ((b2 >> 4) ^ 240);
                }
                length = (b3 & -128) == 0 ? (byte) (b3 >> 6) : (byte) ((b3 >> 6) ^ 252);
                int i12 = i11 + 1;
                cArr[i11] = f3977b[i7];
                i7 = i12 + 1;
                cArr[i12] = f3977b[i8 | (b5 << 4)];
                i8 = i7 + 1;
                cArr[i7] = f3977b[length | (b4 << 2)];
                i7 = i8 + 1;
                cArr[i8] = f3977b[b3 & 63];
                i2++;
                i10++;
                i11 = i7;
                i8 = i3;
            }
            i7 = i11 + 1;
            cArr[i11] = '\n';
            i9++;
            length = i10;
        }
        i2 = length;
        i3 = i7;
        length = i8;
        while (i2 < i5) {
            i8 = length + 1;
            byte b7 = bArr[length];
            i7 = i8 + 1;
            b = bArr[i8];
            i10 = i7 + 1;
            byte b8 = bArr[i7];
            byte b9 = (byte) (b & 15);
            byte b10 = (byte) (b7 & 3);
            if ((b7 & -128) == 0) {
                i7 = (byte) (b7 >> 2);
            } else {
                b6 = (byte) ((b7 >> 2) ^ 192);
            }
            if ((b & -128) == 0) {
                i8 = (byte) (b >> 4);
            } else {
                b = (byte) ((b >> 4) ^ 240);
            }
            length = (b8 & -128) == 0 ? (byte) (b8 >> 6) : (byte) ((b8 >> 6) ^ 252);
            i12 = i3 + 1;
            cArr[i3] = f3977b[i7];
            i7 = i12 + 1;
            cArr[i12] = f3977b[i8 | (b10 << 4)];
            i3 = i7 + 1;
            cArr[i7] = f3977b[length | (b9 << 2)];
            i8 = i3 + 1;
            cArr[i3] = f3977b[b8 & 63];
            i2++;
            i3 = i8;
            length = i10;
        }
        byte b11;
        if (i4 == 8) {
            b7 = bArr[length];
            b11 = (byte) (b7 & 3);
            i8 = i3 + 1;
            cArr[i3] = f3977b[(b7 & -128) == 0 ? (byte) (b7 >> 2) : (byte) ((b7 >> 2) ^ 192)];
            length = i8 + 1;
            cArr[i8] = f3977b[b11 << 4];
            i5 = length + 1;
            cArr[length] = '=';
            length = i5 + 1;
            cArr[i5] = '=';
        } else if (i4 == 16) {
            b11 = bArr[length];
            b = bArr[length + 1];
            b6 = (byte) (b & 15);
            byte b12 = (byte) (b11 & 3);
            if ((b11 & -128) == 0) {
                i5 = (byte) (b11 >> 2);
            } else {
                b11 = (byte) ((b11 >> 2) ^ 192);
            }
            length = (b & -128) == 0 ? (byte) (b >> 4) : (byte) ((b >> 4) ^ 240);
            i8 = i3 + 1;
            cArr[i3] = f3977b[i5];
            i5 = i8 + 1;
            cArr[i8] = f3977b[length | (b12 << 4)];
            length = i5 + 1;
            cArr[i5] = f3977b[b6 << 2];
            i5 = length + 1;
            cArr[length] = '=';
        }
        return new String(cArr);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m6099a(java.io.InputStream r11, java.io.OutputStream r12) {
        /*
        r10 = 2;
        r9 = 1;
        r8 = 3;
        r7 = -1;
        r1 = 0;
        r0 = 4;
        r3 = new byte[r0];
        r0 = r1;
    L_0x0009:
        r2 = r11.read();
        if (r2 <= 0) goto L_0x002b;
    L_0x000f:
        r4 = (byte) r2;
        r2 = m6103a(r4);
        if (r2 != 0) goto L_0x0009;
    L_0x0016:
        r2 = m6108b(r4);
        if (r2 == 0) goto L_0x005f;
    L_0x001c:
        r2 = r0 + 1;
        r3[r0] = r4;
        if (r2 != r8) goto L_0x002b;
    L_0x0022:
        r0 = r2 + 1;
        r0 = r11.read();
        r0 = (byte) r0;
        r3[r2] = r0;
    L_0x002b:
        r0 = r3[r1];
        r1 = r3[r9];
        r2 = r3[r10];
        r3 = r3[r8];
        r4 = f3976a;
        r0 = r4[r0];
        r4 = f3976a;
        r1 = r4[r1];
        r4 = f3976a;
        r4 = r4[r2];
        r5 = f3976a;
        r5 = r5[r3];
        if (r4 == r7) goto L_0x0047;
    L_0x0045:
        if (r5 != r7) goto L_0x00ef;
    L_0x0047:
        r4 = m6108b(r2);
        if (r4 == 0) goto L_0x00b4;
    L_0x004d:
        r4 = m6108b(r3);
        if (r4 == 0) goto L_0x00b4;
    L_0x0053:
        r2 = r1 & 15;
        if (r2 == 0) goto L_0x00aa;
    L_0x0057:
        r0 = new org.apache.xml.security.exceptions.Base64DecodingException;
        r1 = "decoding.general";
        r0.<init>(r1);
        throw r0;
    L_0x005f:
        r2 = r0 + 1;
        r3[r0] = r4;
        if (r4 != r7) goto L_0x006d;
    L_0x0065:
        r0 = new org.apache.xml.security.exceptions.Base64DecodingException;
        r1 = "decoding.general";
        r0.<init>(r1);
        throw r0;
    L_0x006d:
        r0 = 4;
        if (r2 == r0) goto L_0x0072;
    L_0x0070:
        r0 = r2;
        goto L_0x0009;
    L_0x0072:
        r0 = f3976a;
        r2 = r3[r1];
        r0 = r0[r2];
        r2 = f3976a;
        r4 = r3[r9];
        r2 = r2[r4];
        r4 = f3976a;
        r5 = r3[r10];
        r4 = r4[r5];
        r5 = f3976a;
        r6 = r3[r8];
        r5 = r5[r6];
        r0 = r0 << 2;
        r6 = r2 >> 4;
        r0 = r0 | r6;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r2 & 15;
        r0 = r0 << 4;
        r2 = r4 >> 2;
        r2 = r2 & 15;
        r0 = r0 | r2;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r4 << 6;
        r0 = r0 | r5;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r1;
        goto L_0x0009;
    L_0x00aa:
        r0 = r0 << 2;
        r1 = r1 >> 4;
        r0 = r0 | r1;
        r0 = (byte) r0;
        r12.write(r0);
    L_0x00b3:
        return;
    L_0x00b4:
        r4 = m6108b(r2);
        if (r4 != 0) goto L_0x00e7;
    L_0x00ba:
        r3 = m6108b(r3);
        if (r3 == 0) goto L_0x00e7;
    L_0x00c0:
        r3 = f3976a;
        r2 = r3[r2];
        r3 = r2 & 3;
        if (r3 == 0) goto L_0x00d0;
    L_0x00c8:
        r0 = new org.apache.xml.security.exceptions.Base64DecodingException;
        r1 = "decoding.general";
        r0.<init>(r1);
        throw r0;
    L_0x00d0:
        r0 = r0 << 2;
        r3 = r1 >> 4;
        r0 = r0 | r3;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r1 & 15;
        r0 = r0 << 4;
        r1 = r2 >> 2;
        r1 = r1 & 15;
        r0 = r0 | r1;
        r0 = (byte) r0;
        r12.write(r0);
        goto L_0x00b3;
    L_0x00e7:
        r0 = new org.apache.xml.security.exceptions.Base64DecodingException;
        r1 = "decoding.general";
        r0.<init>(r1);
        throw r0;
    L_0x00ef:
        r0 = r0 << 2;
        r2 = r1 >> 4;
        r0 = r0 | r2;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r1 & 15;
        r0 = r0 << 4;
        r1 = r4 >> 2;
        r1 = r1 & 15;
        r0 = r0 | r1;
        r0 = (byte) r0;
        r12.write(r0);
        r0 = r4 << 6;
        r0 = r0 | r5;
        r0 = (byte) r0;
        r12.write(r0);
        goto L_0x00b3;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.xml.security.utils.Base64.a(java.io.InputStream, java.io.OutputStream):void");
    }

    public static final void m6100a(String str, OutputStream outputStream) {
        byte[] bArr = new byte[str.length()];
        m6102a(bArr, outputStream, m6097a(str, bArr));
    }

    public static final void m6101a(byte[] bArr, OutputStream outputStream) {
        m6102a(bArr, outputStream, -1);
    }

    protected static final void m6102a(byte[] bArr, OutputStream outputStream, int i) {
        if (i == -1) {
            i = m6110c(bArr);
        }
        if (i % 4 != 0) {
            throw new Base64DecodingException("decoding.divisible.four");
        }
        int i2 = i / 4;
        if (i2 != 0) {
            int i3;
            byte b;
            int i4;
            byte b2;
            byte b3;
            byte b4;
            int i5 = 0;
            for (i2--; i2 > 0; i2--) {
                i3 = i5 + 1;
                b = f3976a[bArr[i5]];
                i4 = i3 + 1;
                b2 = f3976a[bArr[i3]];
                int i6 = i4 + 1;
                b3 = f3976a[bArr[i4]];
                i5 = i6 + 1;
                b4 = f3976a[bArr[i6]];
                if (b == (byte) -1 || b2 == (byte) -1 || b3 == (byte) -1 || b4 == (byte) -1) {
                    throw new Base64DecodingException("decoding.general");
                }
                outputStream.write((byte) ((b << 2) | (b2 >> 4)));
                outputStream.write((byte) (((b2 & 15) << 4) | ((b3 >> 2) & 15)));
                outputStream.write((byte) ((b3 << 6) | b4));
            }
            int i7 = i5 + 1;
            byte b5 = f3976a[bArr[i5]];
            i3 = i7 + 1;
            byte b6 = f3976a[bArr[i7]];
            if (b5 == (byte) -1 || b6 == (byte) -1) {
                throw new Base64DecodingException("decoding.general");
            }
            byte[] bArr2 = f3976a;
            i4 = i3 + 1;
            b2 = bArr[i3];
            b = bArr2[b2];
            byte[] bArr3 = f3976a;
            int i8 = i4 + 1;
            b3 = bArr[i4];
            b4 = bArr3[b3];
            if (b != (byte) -1 && b4 != (byte) -1) {
                outputStream.write((byte) ((b5 << 2) | (b6 >> 4)));
                outputStream.write((byte) (((b6 & 15) << 4) | ((b >> 2) & 15)));
                outputStream.write((byte) ((b << 6) | b4));
            } else if (m6108b(b2) && m6108b(b3)) {
                if ((b6 & 15) != 0) {
                    throw new Base64DecodingException("decoding.general");
                }
                outputStream.write((byte) ((b5 << 2) | (b6 >> 4)));
            } else if (m6108b(b2) || !m6108b(b3)) {
                throw new Base64DecodingException("decoding.general");
            } else if ((b & 3) != 0) {
                throw new Base64DecodingException("decoding.general");
            } else {
                outputStream.write((byte) ((b5 << 2) | (b6 >> 4)));
                outputStream.write((byte) (((b6 & 15) << 4) | ((b >> 2) & 15)));
            }
        }
    }

    protected static final boolean m6103a(byte b) {
        return b == 32 || b == 13 || b == 10 || b == 9;
    }

    public static final byte[] m6104a(String str) {
        if (str == null) {
            return null;
        }
        byte[] bArr = new byte[str.length()];
        return m6109b(bArr, m6097a(str, bArr));
    }

    public static final byte[] m6105a(Element element) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Node firstChild = element.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (firstChild.getNodeType() == (short) 3) {
                stringBuffer.append(((Text) firstChild).getData());
            }
        }
        return m6104a(stringBuffer.toString());
    }

    public static final byte[] m6106a(byte[] bArr) {
        return m6109b(bArr, -1);
    }

    public static final String m6107b(byte[] bArr) {
        return XMLUtils.m6165a() ? m6098a(bArr, Integer.MAX_VALUE) : m6098a(bArr, 76);
    }

    protected static final boolean m6108b(byte b) {
        return b == 61;
    }

    protected static final byte[] m6109b(byte[] bArr, int i) {
        int i2 = 0;
        if (i == -1) {
            i = m6110c(bArr);
        }
        if (i % 4 != 0) {
            throw new Base64DecodingException("decoding.divisible.four");
        }
        int i3 = i / 4;
        if (i3 == 0) {
            return new byte[0];
        }
        int i4 = (i3 - 1) * 4;
        int i5 = (i3 - 1) * 3;
        int i6 = i4 + 1;
        byte b = f3976a[bArr[i4]];
        int i7 = i6 + 1;
        byte b2 = f3976a[bArr[i6]];
        if (b == (byte) -1 || b2 == (byte) -1) {
            throw new Base64DecodingException("decoding.general");
        }
        byte[] bArr2 = f3976a;
        int i8 = i7 + 1;
        byte b3 = bArr[i7];
        byte b4 = bArr2[b3];
        bArr2 = f3976a;
        int i9 = i8 + 1;
        byte b5 = bArr[i8];
        byte b6 = bArr2[b5];
        if (b4 != (byte) -1 && b6 != (byte) -1) {
            bArr2 = new byte[(i5 + 3)];
            i7 = i5 + 1;
            bArr2[i5] = (byte) ((b << 2) | (b2 >> 4));
            i5 = i7 + 1;
            bArr2[i7] = (byte) (((b2 & 15) << 4) | ((b4 >> 2) & 15));
            int i10 = i5 + 1;
            bArr2[i5] = (byte) ((b4 << 6) | b6);
        } else if (m6108b(b3) && m6108b(b5)) {
            if ((b2 & 15) != 0) {
                throw new Base64DecodingException("decoding.general");
            }
            bArr2 = new byte[(i5 + 1)];
            bArr2[i5] = (byte) ((b << 2) | (b2 >> 4));
        } else if (m6108b(b3) || !m6108b(b5)) {
            throw new Base64DecodingException("decoding.general");
        } else if ((b4 & 3) != 0) {
            throw new Base64DecodingException("decoding.general");
        } else {
            bArr2 = new byte[(i5 + 2)];
            i7 = i5 + 1;
            bArr2[i5] = (byte) ((b << 2) | (b2 >> 4));
            bArr2[i7] = (byte) (((b2 & 15) << 4) | ((b4 >> 2) & 15));
        }
        i3 = 0;
        for (i5 = i3 - 1; i5 > 0; i5--) {
            i6 = i2 + 1;
            b = f3976a[bArr[i2]];
            i7 = i6 + 1;
            b2 = f3976a[bArr[i6]];
            i8 = i7 + 1;
            b3 = f3976a[bArr[i7]];
            i2 = i8 + 1;
            b5 = f3976a[bArr[i8]];
            if (b == (byte) -1 || b2 == (byte) -1 || b3 == (byte) -1 || b5 == (byte) -1) {
                throw new Base64DecodingException("decoding.general");
            }
            int i11 = i3 + 1;
            bArr2[i3] = (byte) ((b << 2) | (b2 >> 4));
            i10 = i11 + 1;
            bArr2[i11] = (byte) (((b2 & 15) << 4) | ((b3 >> 2) & 15));
            i3 = i10 + 1;
            bArr2[i10] = (byte) ((b3 << 6) | b5);
        }
        return bArr2;
    }

    protected static final int m6110c(byte[] bArr) {
        int i = 0;
        if (bArr != null) {
            int length = bArr.length;
            int i2 = 0;
            while (i2 < length) {
                int i3;
                byte b = bArr[i2];
                if (m6103a(b)) {
                    i3 = i;
                } else {
                    i3 = i + 1;
                    bArr[i] = b;
                }
                i2++;
                i = i3;
            }
        }
        return i;
    }
}
