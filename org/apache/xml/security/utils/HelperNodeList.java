package org.apache.xml.security.utils;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HelperNodeList implements NodeList {
    ArrayList f4001a;
    boolean f4002b;

    public HelperNodeList() {
        this(false);
    }

    public HelperNodeList(boolean z) {
        this.f4001a = new ArrayList(20);
        this.f4002b = false;
        this.f4002b = z;
    }

    public int getLength() {
        return this.f4001a.size();
    }

    public Node item(int i) {
        return (Node) this.f4001a.get(i);
    }
}
