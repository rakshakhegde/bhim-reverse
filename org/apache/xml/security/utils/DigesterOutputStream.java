package org.apache.xml.security.utils;

import java.io.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;

public class DigesterOutputStream extends ByteArrayOutputStream {
    static Log f3998b;
    static Class f3999c;
    final MessageDigestAlgorithm f4000a;

    static {
        Class a;
        if (f3999c == null) {
            a = m6123a("org.apache.xml.security.utils.DigesterOutputStream");
            f3999c = a;
        } else {
            a = f3999c;
        }
        f3998b = LogFactory.getLog(a.getName());
    }

    public DigesterOutputStream(MessageDigestAlgorithm messageDigestAlgorithm) {
        this.f4000a = messageDigestAlgorithm;
    }

    static Class m6123a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public byte[] m6124a() {
        return this.f4000a.m5738b();
    }

    public void write(int i) {
        this.f4000a.m5736a((byte) i);
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (f3998b.isDebugEnabled()) {
            f3998b.debug("Pre-digested input:");
            StringBuffer stringBuffer = new StringBuffer(i2);
            for (int i3 = i; i3 < i + i2; i3++) {
                stringBuffer.append((char) bArr[i3]);
            }
            f3998b.debug(stringBuffer.toString());
        }
        this.f4000a.m5737a(bArr, i, i2);
    }
}
