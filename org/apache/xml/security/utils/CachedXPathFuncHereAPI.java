package org.apache.xml.security.utils;

import java.lang.reflect.Method;
import javax.xml.transform.TransformerException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.dtm.DTMManager;
import org.apache.xml.security.transforms.implementations.FuncHere;
import org.apache.xml.security.transforms.implementations.FuncHereContext;
import org.apache.xml.utils.PrefixResolver;
import org.apache.xml.utils.PrefixResolverDefault;
import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.XPath;
import org.apache.xpath.XPathContext;
import org.apache.xpath.compiler.FunctionTable;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public class CachedXPathFuncHereAPI {
    static Log f3980a;
    static FunctionTable f3981g;
    static Class f3982h;
    static Class f3983i;
    static Class f3984j;
    static Class f3985k;
    static Class f3986l;
    static Class f3987m;
    static Class f3988n;
    static Class f3989o;
    static Class f3990p;
    static Class f3991q;
    FuncHereContext f3992b;
    DTMManager f3993c;
    XPathContext f3994d;
    String f3995e;
    XPath f3996f;

    static {
        Class a;
        if (f3982h == null) {
            a = m6113a("org.apache.xml.security.utils.CachedXPathFuncHereAPI");
            f3982h = a;
        } else {
            a = f3982h;
        }
        f3980a = LogFactory.getLog(a.getName());
        f3981g = null;
        m6116a();
    }

    private CachedXPathFuncHereAPI() {
        this.f3992b = null;
        this.f3993c = null;
        this.f3994d = null;
        this.f3995e = null;
        this.f3996f = null;
    }

    public CachedXPathFuncHereAPI(CachedXPathAPI cachedXPathAPI) {
        this.f3992b = null;
        this.f3993c = null;
        this.f3994d = null;
        this.f3995e = null;
        this.f3996f = null;
        this.f3993c = cachedXPathAPI.getXPathContext().getDTMManager();
        this.f3994d = cachedXPathAPI.getXPathContext();
    }

    static Class m6113a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static String m6114a(Node node) {
        if (node.getNodeType() != (short) 3) {
            return node.getNodeType() == (short) 2 ? ((Attr) node).getNodeValue() : node.getNodeType() == (short) 7 ? ((ProcessingInstruction) node).getNodeValue() : null;
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            for (Node firstChild = node.getParentNode().getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                if (firstChild.getNodeType() == (short) 3) {
                    stringBuffer.append(((Text) firstChild).getData());
                }
            }
            return stringBuffer.toString();
        }
    }

    private XPath m6115a(String str, PrefixResolver prefixResolver) {
        Class a;
        XPath xPath;
        Class[] clsArr = new Class[6];
        if (f3983i == null) {
            a = m6113a("java.lang.String");
            f3983i = a;
        } else {
            a = f3983i;
        }
        clsArr[0] = a;
        if (f3984j == null) {
            a = m6113a("javax.xml.transform.SourceLocator");
            f3984j = a;
        } else {
            a = f3984j;
        }
        clsArr[1] = a;
        if (f3985k == null) {
            a = m6113a("org.apache.xml.utils.PrefixResolver");
            f3985k = a;
        } else {
            a = f3985k;
        }
        clsArr[2] = a;
        clsArr[3] = Integer.TYPE;
        if (f3986l == null) {
            a = m6113a("javax.xml.transform.ErrorListener");
            f3986l = a;
        } else {
            a = f3986l;
        }
        clsArr[4] = a;
        if (f3987m == null) {
            a = m6113a("org.apache.xpath.compiler.FunctionTable");
            f3987m = a;
        } else {
            a = f3987m;
        }
        clsArr[5] = a;
        Object[] objArr = new Object[]{str, null, prefixResolver, new Integer(0), null, f3981g};
        try {
            if (f3988n == null) {
                a = m6113a("org.apache.xpath.XPath");
                f3988n = a;
            } else {
                a = f3988n;
            }
            xPath = (XPath) a.getConstructor(clsArr).newInstance(objArr);
        } catch (Throwable th) {
            xPath = null;
        }
        return xPath == null ? new XPath(str, null, prefixResolver, 0, null) : xPath;
    }

    private static void m6116a() {
        Class a;
        Log log;
        StringBuffer append;
        Class a2;
        Object obj = null;
        Object obj2 = 1;
        f3980a.info("Registering Here function");
        try {
            Class[] clsArr = new Class[2];
            if (f3983i == null) {
                a = m6113a("java.lang.String");
                f3983i = a;
            } else {
                a = f3983i;
            }
            clsArr[0] = a;
            if (f3989o == null) {
                a = m6113a("org.apache.xpath.Expression");
                f3989o = a;
            } else {
                a = f3989o;
            }
            clsArr[1] = a;
            if (f3987m == null) {
                a = m6113a("org.apache.xpath.compiler.FunctionTable");
                f3987m = a;
            } else {
                a = f3987m;
            }
            Method method = a.getMethod("installFunction", clsArr);
            if ((method.getModifiers() & 8) != 0) {
                method.invoke(null, new Object[]{"here", new FuncHere()});
                obj = 1;
            }
        } catch (Throwable th) {
            f3980a.debug("Error installing function using the static installFunction method", th);
        }
        if (obj == null) {
            try {
                f3981g = new FunctionTable();
                clsArr = new Class[2];
                if (f3983i == null) {
                    a = m6113a("java.lang.String");
                    f3983i = a;
                } else {
                    a = f3983i;
                }
                clsArr[0] = a;
                if (f3990p == null) {
                    a = m6113a("java.lang.Class");
                    f3990p = a;
                } else {
                    a = f3990p;
                }
                clsArr[1] = a;
                if (f3987m == null) {
                    a = m6113a("org.apache.xpath.compiler.FunctionTable");
                    f3987m = a;
                } else {
                    a = f3987m;
                }
                Method method2 = a.getMethod("installFunction", clsArr);
                Object[] objArr = new Object[2];
                objArr[0] = "here";
                if (f3991q == null) {
                    a = m6113a("org.apache.xml.security.transforms.implementations.FuncHere");
                    f3991q = a;
                } else {
                    a = f3991q;
                }
                objArr[1] = a;
                method2.invoke(f3981g, objArr);
            } catch (Throwable th2) {
                f3980a.debug("Error installing function using the static installFunction method", th2);
            }
            if (!f3980a.isDebugEnabled()) {
                if (obj2 == null) {
                    log = f3980a;
                    append = new StringBuffer().append("Registered class ");
                    if (f3991q != null) {
                        a2 = m6113a("org.apache.xml.security.transforms.implementations.FuncHere");
                        f3991q = a2;
                    } else {
                        a2 = f3991q;
                    }
                    log.debug(append.append(a2.getName()).append(" for XPath function 'here()' function in internal table").toString());
                }
                log = f3980a;
                append = new StringBuffer().append("Unable to register class ");
                if (f3991q != null) {
                    a2 = m6113a("org.apache.xml.security.transforms.implementations.FuncHere");
                    f3991q = a2;
                } else {
                    a2 = f3991q;
                }
                log.debug(append.append(a2.getName()).append(" for XPath function 'here()' function in internal table").toString());
                return;
            }
            return;
        }
        obj2 = obj;
        if (!f3980a.isDebugEnabled()) {
            return;
        }
        if (obj2 == null) {
            log = f3980a;
            append = new StringBuffer().append("Unable to register class ");
            if (f3991q != null) {
                a2 = f3991q;
            } else {
                a2 = m6113a("org.apache.xml.security.transforms.implementations.FuncHere");
                f3991q = a2;
            }
            log.debug(append.append(a2.getName()).append(" for XPath function 'here()' function in internal table").toString());
            return;
        }
        log = f3980a;
        append = new StringBuffer().append("Registered class ");
        if (f3991q != null) {
            a2 = f3991q;
        } else {
            a2 = m6113a("org.apache.xml.security.transforms.implementations.FuncHere");
            f3991q = a2;
        }
        log.debug(append.append(a2.getName()).append(" for XPath function 'here()' function in internal table").toString());
    }

    public XObject m6117a(Node node, Node node2, String str, PrefixResolver prefixResolver) {
        if (str != this.f3995e) {
            if (str.indexOf("here()") > 0) {
                this.f3994d.reset();
                this.f3993c = this.f3994d.getDTMManager();
            }
            try {
                this.f3996f = m6115a(str, prefixResolver);
                this.f3995e = str;
            } catch (TransformerException e) {
                Throwable cause = e.getCause();
                if (!(cause instanceof ClassNotFoundException) || cause.getMessage().indexOf("FuncHere") <= 0) {
                    throw e;
                }
                throw new RuntimeException(new StringBuffer().append(I18n.m6125a("endorsed.jdk1.4.0")).append(e).toString());
            }
        }
        if (this.f3992b == null) {
            this.f3992b = new FuncHereContext(node2, this.f3993c);
        }
        return this.f3996f.execute(this.f3992b, this.f3992b.getDTMHandleFromNode(node), prefixResolver);
    }

    public NodeList m6118a(Node node, Node node2, String str, Node node3) {
        return m6119b(node, node2, str, node3).nodelist();
    }

    public XObject m6119b(Node node, Node node2, String str, Node node3) {
        if (this.f3992b == null) {
            this.f3992b = new FuncHereContext(node2, this.f3993c);
        }
        if (node3.getNodeType() == (short) 9) {
            node3 = ((Document) node3).getDocumentElement();
        }
        PrefixResolver prefixResolverDefault = new PrefixResolverDefault(node3);
        if (str != this.f3995e) {
            if (str.indexOf("here()") > 0) {
                this.f3994d.reset();
                this.f3993c = this.f3994d.getDTMManager();
            }
            this.f3996f = m6115a(str, prefixResolverDefault);
            this.f3995e = str;
        }
        return this.f3996f.execute(this.f3992b, this.f3992b.getDTMHandleFromNode(node), prefixResolverDefault);
    }
}
