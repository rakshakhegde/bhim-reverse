package org.apache.xml.security.utils;

import java.util.Enumeration;

public final class ClassLoaderUtils {
    static Class f3997a;

    /* renamed from: org.apache.xml.security.utils.ClassLoaderUtils.1 */
    class C06141 implements Enumeration {
        C06141() {
        }

        public boolean hasMoreElements() {
            return false;
        }

        public Object nextElement() {
            return null;
        }
    }

    private ClassLoaderUtils() {
    }

    static Class m6120a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static Class m6121a(String str, Class cls) {
        try {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader != null) {
                return contextClassLoader.loadClass(str);
            }
        } catch (ClassNotFoundException e) {
        }
        return m6122b(str, cls);
    }

    private static Class m6122b(String str, Class cls) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            try {
                Class a;
                if (f3997a == null) {
                    a = m6120a("org.apache.xml.security.utils.ClassLoaderUtils");
                    f3997a = a;
                } else {
                    a = f3997a;
                }
                if (a.getClassLoader() != null) {
                    if (f3997a == null) {
                        a = m6120a("org.apache.xml.security.utils.ClassLoaderUtils");
                        f3997a = a;
                    } else {
                        a = f3997a;
                    }
                    return a.getClassLoader().loadClass(str);
                }
            } catch (ClassNotFoundException e2) {
                if (!(cls == null || cls.getClassLoader() == null)) {
                    return cls.getClassLoader().loadClass(str);
                }
            }
            throw e;
        }
    }
}
