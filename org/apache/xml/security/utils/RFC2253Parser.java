package org.apache.xml.security.utils;

import com.crashlytics.android.core.BuildConfig;
import java.io.IOException;
import java.io.StringReader;

public class RFC2253Parser {
    static boolean f4020a;
    static int f4021b;

    static {
        f4020a = true;
        f4021b = 0;
    }

    private static int m6143a(String str, int i, int i2) {
        int i3 = 0;
        while (i < i2) {
            if (str.charAt(i) == '\"') {
                i3++;
            }
            i++;
        }
        return i3;
    }

    public static String m6144a(String str) {
        if (str == null || str.equals(BuildConfig.FLAVOR)) {
            return BuildConfig.FLAVOR;
        }
        try {
            String f = m6150f(str);
            StringBuffer stringBuffer = new StringBuffer();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (true) {
                int indexOf = f.indexOf(",", i);
                if (indexOf >= 0) {
                    i = m6143a(f, i, indexOf) + i2;
                    if (!(indexOf <= 0 || f.charAt(indexOf - 1) == '\\' || i % 2 == 1)) {
                        stringBuffer.append(new StringBuffer().append(m6146b(f.substring(i3, indexOf).trim())).append(",").toString());
                        i3 = indexOf + 1;
                        i = 0;
                    }
                    i2 = i;
                    i = indexOf + 1;
                } else {
                    stringBuffer.append(m6146b(m6151g(f.substring(i3))));
                    return stringBuffer.toString();
                }
            }
        } catch (IOException e) {
            return str;
        }
    }

    static String m6145a(String str, String str2, String str3) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int indexOf = str.indexOf(str2, i);
            if (indexOf >= 0) {
                i2 += m6143a(str, i, indexOf);
                if (!(indexOf <= 0 || str.charAt(indexOf - 1) == '\\' || i2 % 2 == 1)) {
                    stringBuffer.append(new StringBuffer().append(m6151g(str.substring(i3, indexOf))).append(str3).toString());
                    i3 = indexOf + 1;
                    i2 = 0;
                }
                i = indexOf + 1;
            } else {
                stringBuffer.append(m6151g(str.substring(i3)));
                return stringBuffer.toString();
            }
        }
    }

    static String m6146b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int indexOf = str.indexOf("+", i);
            if (indexOf >= 0) {
                i2 += m6143a(str, i, indexOf);
                if (!(indexOf <= 0 || str.charAt(indexOf - 1) == '\\' || i2 % 2 == 1)) {
                    stringBuffer.append(new StringBuffer().append(m6147c(m6151g(str.substring(i3, indexOf)))).append("+").toString());
                    i3 = indexOf + 1;
                    i2 = 0;
                }
                i = indexOf + 1;
            } else {
                stringBuffer.append(m6147c(m6151g(str.substring(i3))));
                return stringBuffer.toString();
            }
        }
    }

    static String m6147c(String str) {
        int indexOf = str.indexOf("=");
        if (indexOf == -1) {
            return str;
        }
        if (indexOf > 0 && str.charAt(indexOf - 1) == '\\') {
            return str;
        }
        String d = m6148d(str.substring(0, indexOf));
        String e = (d.charAt(0) < '0' || d.charAt(0) > '9') ? m6149e(str.substring(indexOf + 1)) : str.substring(indexOf + 1);
        return new StringBuffer().append(d).append("=").append(e).toString();
    }

    static String m6148d(String str) {
        String trim = str.toUpperCase().trim();
        return trim.startsWith("OID") ? trim.substring(3) : trim;
    }

    static String m6149e(String str) {
        String g = m6151g(str);
        if (g.startsWith("\"")) {
            StringBuffer stringBuffer = new StringBuffer();
            StringReader stringReader = new StringReader(g.substring(1, g.length() - 1));
            while (true) {
                int read = stringReader.read();
                if (read <= -1) {
                    break;
                }
                char c = (char) read;
                if (c == ',' || c == '=' || c == '+' || c == '<' || c == '>' || c == '#' || c == ';') {
                    stringBuffer.append('\\');
                }
                stringBuffer.append(c);
            }
            g = m6151g(stringBuffer.toString());
        }
        return f4020a ? g.startsWith("#") ? new StringBuffer().append('\\').append(g).toString() : g : g.startsWith("\\#") ? g.substring(1) : g;
    }

    static String m6150f(String str) {
        return m6145a(str, ";", ",");
    }

    static String m6151g(String str) {
        String trim = str.trim();
        int indexOf = str.indexOf(trim) + trim.length();
        return (str.length() <= indexOf || !trim.endsWith("\\") || trim.endsWith("\\\\") || str.charAt(indexOf) != ' ') ? trim : new StringBuffer().append(trim).append(" ").toString();
    }
}
