package org.apache.xml.security.utils;

import org.apache.xpath.CachedXPathAPI;
import org.w3c.dom.Document;

public class CachedXPathAPIHolder {
    static ThreadLocal f3978a;
    static ThreadLocal f3979b;

    static {
        f3978a = new ThreadLocal();
        f3979b = new ThreadLocal();
    }

    public static CachedXPathAPI m6111a() {
        CachedXPathAPI cachedXPathAPI = (CachedXPathAPI) f3978a.get();
        if (cachedXPathAPI != null) {
            return cachedXPathAPI;
        }
        cachedXPathAPI = new CachedXPathAPI();
        f3978a.set(cachedXPathAPI);
        f3979b.set(null);
        return cachedXPathAPI;
    }

    public static void m6112a(Document document) {
        if (f3979b.get() != document) {
            CachedXPathAPI cachedXPathAPI = (CachedXPathAPI) f3978a.get();
            if (cachedXPathAPI == null) {
                f3978a.set(new CachedXPathAPI());
                f3979b.set(document);
                return;
            }
            cachedXPathAPI.getXPathContext().reset();
            f3979b.set(document);
        }
    }
}
