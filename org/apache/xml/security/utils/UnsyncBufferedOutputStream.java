package org.apache.xml.security.utils;

import java.io.OutputStream;

public class UnsyncBufferedOutputStream extends OutputStream {
    private static ThreadLocal f4025d;
    final OutputStream f4026a;
    final byte[] f4027b;
    int f4028c;

    /* renamed from: org.apache.xml.security.utils.UnsyncBufferedOutputStream.1 */
    class C06151 extends ThreadLocal {
        C06151() {
        }

        protected synchronized Object initialValue() {
            return new byte[8192];
        }
    }

    static {
        f4025d = new C06151();
    }

    public UnsyncBufferedOutputStream(OutputStream outputStream) {
        this.f4028c = 0;
        this.f4027b = (byte[]) f4025d.get();
        this.f4026a = outputStream;
    }

    private final void m6153a() {
        if (this.f4028c > 0) {
            this.f4026a.write(this.f4027b, 0, this.f4028c);
        }
        this.f4028c = 0;
    }

    public void close() {
        flush();
    }

    public void flush() {
        m6153a();
        this.f4026a.flush();
    }

    public void write(int i) {
        if (this.f4028c >= 8192) {
            m6153a();
        }
        byte[] bArr = this.f4027b;
        int i2 = this.f4028c;
        this.f4028c = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3 = this.f4028c + i2;
        if (i3 > 8192) {
            m6153a();
            if (i2 > 8192) {
                this.f4026a.write(bArr, i, i2);
                return;
            }
            i3 = i2;
        }
        System.arraycopy(bArr, i, this.f4027b, this.f4028c, i2);
        this.f4028c = i3;
    }
}
