package org.apache.xml.security.utils;

import java.io.InputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JavaUtils {
    static Log f4018a;
    static Class f4019b;

    static {
        Class a;
        if (f4019b == null) {
            a = m6141a("org.apache.xml.security.utils.JavaUtils");
            f4019b = a;
        } else {
            a = f4019b;
        }
        f4018a = LogFactory.getLog(a.getName());
    }

    private JavaUtils() {
    }

    static Class m6141a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static byte[] m6142a(InputStream inputStream) {
        UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                return unsyncByteArrayOutputStream.m6155a();
            }
            unsyncByteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
