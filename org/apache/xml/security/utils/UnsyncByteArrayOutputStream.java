package org.apache.xml.security.utils;

import java.io.OutputStream;

public class UnsyncByteArrayOutputStream extends OutputStream {
    private static ThreadLocal f4029a;
    private byte[] f4030b;
    private int f4031c;
    private int f4032d;

    /* renamed from: org.apache.xml.security.utils.UnsyncByteArrayOutputStream.1 */
    class C06161 extends ThreadLocal {
        C06161() {
        }

        protected synchronized Object initialValue() {
            return new byte[8192];
        }
    }

    static {
        f4029a = new C06161();
    }

    public UnsyncByteArrayOutputStream() {
        this.f4031c = 8192;
        this.f4032d = 0;
        this.f4030b = (byte[]) f4029a.get();
    }

    private void m6154a(int i) {
        int i2 = this.f4031c;
        while (i > i2) {
            i2 <<= 2;
        }
        Object obj = new byte[i2];
        System.arraycopy(this.f4030b, 0, obj, 0, this.f4032d);
        this.f4030b = obj;
        this.f4031c = i2;
    }

    public byte[] m6155a() {
        Object obj = new byte[this.f4032d];
        System.arraycopy(this.f4030b, 0, obj, 0, this.f4032d);
        return obj;
    }

    public void m6156b() {
        this.f4032d = 0;
    }

    public void write(int i) {
        int i2 = this.f4032d + 1;
        if (i2 > this.f4031c) {
            m6154a(i2);
        }
        byte[] bArr = this.f4030b;
        int i3 = this.f4032d;
        this.f4032d = i3 + 1;
        bArr[i3] = (byte) i;
    }

    public void write(byte[] bArr) {
        int length = this.f4032d + bArr.length;
        if (length > this.f4031c) {
            m6154a(length);
        }
        System.arraycopy(bArr, 0, this.f4030b, this.f4032d, bArr.length);
        this.f4032d = length;
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3 = this.f4032d + i2;
        if (i3 > this.f4031c) {
            m6154a(i3);
        }
        System.arraycopy(bArr, i, this.f4030b, this.f4032d, i2);
        this.f4032d = i3;
    }
}
