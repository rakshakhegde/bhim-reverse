package org.apache.xml.security.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.xml.security.Init;

public class I18n {
    static String f4003a;
    static String f4004b;
    static ResourceBundle f4005c;
    static boolean f4006d;
    static String f4007e;
    static String f4008f;

    static {
        f4006d = false;
        f4007e = null;
        f4008f = null;
    }

    private I18n() {
    }

    public static String m6125a(String str) {
        return m6129b(str);
    }

    public static String m6126a(String str, Exception exception) {
        try {
            return MessageFormat.format(f4005c.getString(str), new Object[]{exception.getMessage()});
        } catch (Throwable th) {
            return Init.m5712a() ? new StringBuffer().append("No message with ID \"").append(str).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity").append("\". Original Exception was a ").append(exception.getClass().getName()).append(" and message ").append(exception.getMessage()).toString() : "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
    }

    public static String m6127a(String str, Object[] objArr) {
        return m6130b(str, objArr);
    }

    public static void m6128a(String str, String str2) {
        f4003a = str;
        if (f4003a == null) {
            f4003a = Locale.getDefault().getLanguage();
        }
        f4004b = str2;
        if (f4004b == null) {
            f4004b = Locale.getDefault().getCountry();
        }
        m6131b(f4003a, f4004b);
    }

    public static String m6129b(String str) {
        try {
            return f4005c.getString(str);
        } catch (Throwable th) {
            return Init.m5712a() ? new StringBuffer().append("No message with ID \"").append(str).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity").append("\"").toString() : "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
    }

    public static String m6130b(String str, Object[] objArr) {
        try {
            return MessageFormat.format(f4005c.getString(str), objArr);
        } catch (Throwable th) {
            return Init.m5712a() ? new StringBuffer().append("No message with ID \"").append(str).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity").append("\"").toString() : "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
    }

    public static void m6131b(String str, String str2) {
        if (!f4006d || !str.equals(f4007e) || !str2.equals(f4008f)) {
            if (str == null || str2 == null || str.length() <= 0 || str2.length() <= 0) {
                f4008f = f4004b;
                f4007e = f4003a;
            } else {
                f4007e = str;
                f4008f = str2;
            }
            f4005c = ResourceBundle.getBundle("org/apache/xml/security/resource/xmlsecurity", new Locale(f4007e, f4008f));
        }
    }
}
