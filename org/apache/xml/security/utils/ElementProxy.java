package org.apache.xml.security.utils;

import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public abstract class ElementProxy {
    static Log f3732j;
    static HashMap f3733n;
    static HashMap f3734o;
    static Class f3735p;
    protected Element f3736k;
    protected String f3737l;
    protected Document f3738m;

    static {
        Class c;
        if (f3735p == null) {
            c = m5714c("org.apache.xml.security.utils.ElementProxy");
            f3735p = c;
        } else {
            c = f3735p;
        }
        f3732j = LogFactory.getLog(c.getName());
        f3733n = new HashMap();
        f3734o = new HashMap();
    }

    public ElementProxy() {
        this.f3736k = null;
        this.f3737l = null;
        this.f3738m = null;
    }

    public ElementProxy(Element element, String str) {
        this.f3736k = null;
        this.f3737l = null;
        this.f3738m = null;
        if (element == null) {
            throw new XMLSecurityException("ElementProxy.nullElement");
        }
        if (f3732j.isDebugEnabled()) {
            f3732j.debug(new StringBuffer().append("setElement(\"").append(element.getTagName()).append("\", \"").append(str).append("\")").toString());
        }
        this.f3738m = element.getOwnerDocument();
        this.f3736k = element;
        this.f3737l = str;
        m5722m();
    }

    static Class m5714c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static void m5715d(String str, String str2) {
        if (f3733n.containsValue(str2)) {
            if (!f3733n.get(str).equals(str2)) {
                throw new XMLSecurityException("prefix.AlreadyAssigned", new Object[]{str2, str, f3733n.get(str)});
            }
        }
        if ("http://www.w3.org/2000/09/xmldsig#".equals(str)) {
            XMLUtils.f4033a = str2;
        }
        if ("http://www.w3.org/2001/04/xmlenc#".equals(str)) {
            XMLUtils.f4034b = str2;
        }
        f3733n.put(str, str2.intern());
        if (str2.length() == 0) {
            f3734o.put(str, "xmlns");
        } else {
            f3734o.put(str, new StringBuffer().append("xmlns:").append(str2).toString().intern());
        }
    }

    public String m5716b(String str, String str2) {
        return ((Text) XMLUtils.m6162a(this.f3736k.getFirstChild(), str2, str, 0).getFirstChild()).getData();
    }

    public int m5717c(String str, String str2) {
        int i = 0;
        Node firstChild = this.f3736k.getFirstChild();
        while (firstChild != null) {
            if (str2.equals(firstChild.getLocalName()) && str.equals(firstChild.getNamespaceURI())) {
                i++;
            }
            firstChild = firstChild.getNextSibling();
        }
        return i;
    }

    public abstract String m5718d();

    public abstract String m5719e();

    public final Element m5720k() {
        return this.f3736k;
    }

    public String m5721l() {
        return this.f3737l;
    }

    void m5722m() {
        String e = m5719e();
        String d = m5718d();
        String localName = this.f3736k.getLocalName();
        if (!d.equals(this.f3736k.getNamespaceURI()) && !e.equals(localName)) {
            throw new XMLSecurityException("xml.WrongElement", new Object[]{new StringBuffer().append(r3).append(":").append(localName).toString(), new StringBuffer().append(d).append(":").append(e).toString()});
        }
    }

    public byte[] m5723n() {
        return Base64.m6104a(XMLUtils.m6157a(this.f3736k));
    }

    public String m5724o() {
        return XMLUtils.m6157a(this.f3736k);
    }
}
