package org.apache.xml.security.utils;

import com.crashlytics.android.core.BuildConfig;
import java.io.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.signature.XMLSignatureException;

public class SignerOutputStream extends ByteArrayOutputStream {
    static Log f4022b;
    static Class f4023c;
    final SignatureAlgorithm f4024a;

    static {
        Class a;
        if (f4023c == null) {
            a = m6152a("org.apache.xml.security.utils.SignerOutputStream");
            f4023c = a;
        } else {
            a = f4023c;
        }
        f4022b = LogFactory.getLog(a.getName());
    }

    public SignerOutputStream(SignatureAlgorithm signatureAlgorithm) {
        this.f4024a = signatureAlgorithm;
    }

    static Class m6152a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public void write(int i) {
        try {
            this.f4024a.m5750a((byte) i);
        } catch (XMLSignatureException e) {
            throw new RuntimeException(new StringBuffer().append(BuildConfig.FLAVOR).append(e).toString());
        }
    }

    public void write(byte[] bArr) {
        try {
            this.f4024a.m5752a(bArr);
        } catch (XMLSignatureException e) {
            throw new RuntimeException(new StringBuffer().append(BuildConfig.FLAVOR).append(e).toString());
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        if (f4022b.isDebugEnabled()) {
            f4022b.debug("Canonicalized SignedInfo:");
            StringBuffer stringBuffer = new StringBuffer(i2);
            for (int i3 = i; i3 < i + i2; i3++) {
                stringBuffer.append((char) bArr[i3]);
            }
            f4022b.debug(stringBuffer.toString());
        }
        try {
            this.f4024a.m5753a(bArr, i, i2);
        } catch (XMLSignatureException e) {
            throw new RuntimeException(new StringBuffer().append(BuildConfig.FLAVOR).append(e).toString());
        }
    }
}
