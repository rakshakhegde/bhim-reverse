package org.apache.xml.security.utils.resolver;

import org.apache.xml.security.exceptions.XMLSecurityException;
import org.w3c.dom.Attr;

public class ResourceResolverException extends XMLSecurityException {
    Attr f4043c;
    String f4044d;

    public ResourceResolverException(String str, Exception exception, Attr attr, String str2) {
        super(str, exception);
        this.f4043c = null;
        this.f4043c = attr;
        this.f4044d = str2;
    }

    public ResourceResolverException(String str, Object[] objArr, Attr attr, String str2) {
        super(str, objArr);
        this.f4043c = null;
        this.f4043c = attr;
        this.f4044d = str2;
    }
}
