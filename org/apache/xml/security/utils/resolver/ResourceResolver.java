package org.apache.xml.security.utils.resolver;

import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;

public class ResourceResolver {
    static Log f4037a;
    static boolean f4038b;
    static List f4039c;
    static boolean f4040d;
    static Class f4041f;
    protected ResourceResolverSpi f4042e;

    static {
        Class b;
        if (f4041f == null) {
            b = m6179b("org.apache.xml.security.utils.resolver.ResourceResolver");
            f4041f = b;
        } else {
            b = f4041f;
        }
        f4037a = LogFactory.getLog(b.getName());
        f4038b = false;
        f4039c = null;
        f4040d = true;
    }

    private ResourceResolver(String str) {
        this.f4042e = null;
        this.f4042e = (ResourceResolverSpi) Class.forName(str).newInstance();
    }

    public ResourceResolver(ResourceResolverSpi resourceResolverSpi) {
        this.f4042e = null;
        this.f4042e = resourceResolverSpi;
    }

    public static final ResourceResolver m6174a(Attr attr, String str) {
        int size = f4039c.size();
        int i = 0;
        while (i < size) {
            ResourceResolver resourceResolver = (ResourceResolver) f4039c.get(i);
            try {
                ResourceResolver resourceResolver2 = (f4040d || resourceResolver.f4042e.m6187a()) ? resourceResolver : new ResourceResolver((ResourceResolverSpi) resourceResolver.f4042e.getClass().newInstance());
                if (f4037a.isDebugEnabled()) {
                    f4037a.debug(new StringBuffer().append("check resolvability by class ").append(resourceResolver.f4042e.getClass().getName()).toString());
                }
                if (resourceResolver == null || !resourceResolver2.m6180c(attr, str)) {
                    i++;
                } else {
                    if (i != 0) {
                        List list = (List) ((ArrayList) f4039c).clone();
                        list.remove(i);
                        list.add(0, resourceResolver);
                        f4039c = list;
                    }
                    return resourceResolver2;
                }
            } catch (Exception e) {
                throw new ResourceResolverException(BuildConfig.FLAVOR, e, attr, str);
            } catch (Exception e2) {
                throw new ResourceResolverException(BuildConfig.FLAVOR, e2, attr, str);
            }
        }
        Object[] objArr = new Object[2];
        objArr[0] = attr != null ? attr.getNodeValue() : "null";
        objArr[1] = str;
        throw new ResourceResolverException("utils.resolver.noClass", objArr, attr, str);
    }

    public static final ResourceResolver m6175a(Attr attr, String str, List list) {
        int i = 0;
        if (f4037a.isDebugEnabled()) {
            f4037a.debug(new StringBuffer().append("I was asked to create a ResourceResolver and got ").append(list == null ? 0 : list.size()).toString());
            f4037a.debug(new StringBuffer().append(" extra resolvers to my existing ").append(f4039c.size()).append(" system-wide resolvers").toString());
        }
        if (list != null) {
            int size = list.size();
            if (size > 0) {
                while (i < size) {
                    ResourceResolver resourceResolver = (ResourceResolver) list.get(i);
                    if (resourceResolver != null) {
                        String name = resourceResolver.f4042e.getClass().getName();
                        if (f4037a.isDebugEnabled()) {
                            f4037a.debug(new StringBuffer().append("check resolvability by class ").append(name).toString());
                        }
                        if (resourceResolver.m6180c(attr, str)) {
                            return resourceResolver;
                        }
                    }
                    i++;
                }
            }
        }
        return m6174a(attr, str);
    }

    public static void m6176a() {
        if (!f4038b) {
            f4039c = new ArrayList(10);
            f4038b = true;
        }
    }

    public static void m6177a(String str) {
        m6178a(str, false);
    }

    private static void m6178a(String str, boolean z) {
        try {
            ResourceResolver resourceResolver = new ResourceResolver(str);
            if (z) {
                f4039c.add(0, resourceResolver);
                f4037a.debug("registered resolver");
            } else {
                f4039c.add(resourceResolver);
            }
            if (!resourceResolver.f4042e.m6187a()) {
                f4040d = false;
            }
        } catch (Exception e) {
            f4037a.warn(new StringBuffer().append("Error loading resolver ").append(str).append(" disabling it").toString());
        } catch (NoClassDefFoundError e2) {
            f4037a.warn(new StringBuffer().append("Error loading resolver ").append(str).append(" disabling it").toString());
        }
    }

    static Class m6179b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private boolean m6180c(Attr attr, String str) {
        return this.f4042e.m6188b(attr, str);
    }

    public void m6181a(Map map) {
        this.f4042e.m6186a(map);
    }

    public XMLSignatureInput m6182b(Attr attr, String str) {
        return this.f4042e.m6185a(attr, str);
    }
}
