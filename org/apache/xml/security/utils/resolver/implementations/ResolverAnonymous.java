package org.apache.xml.security.utils.resolver.implementations;

import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.w3c.dom.Attr;

public class ResolverAnonymous extends ResourceResolverSpi {
    private XMLSignatureInput f4048d;

    public XMLSignatureInput m6189a(Attr attr, String str) {
        return this.f4048d;
    }

    public boolean m6190b(Attr attr, String str) {
        return attr == null;
    }
}
