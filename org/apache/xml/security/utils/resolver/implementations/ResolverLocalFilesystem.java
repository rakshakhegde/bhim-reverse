package org.apache.xml.security.utils.resolver.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.io.FileInputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.apache.xml.utils.URI;
import org.w3c.dom.Attr;

public class ResolverLocalFilesystem extends ResourceResolverSpi {
    static Log f4054d;
    static Class f4055e;
    private static int f4056f;

    static {
        Class c;
        if (f4055e == null) {
            c = m6201c("org.apache.xml.security.utils.resolver.implementations.ResolverLocalFilesystem");
            f4055e = c;
        } else {
            c = f4055e;
        }
        f4054d = LogFactory.getLog(c.getName());
        f4056f = "file:/".length();
    }

    private static URI m6200a(String str, String str2) {
        return (str2 == null || BuildConfig.FLAVOR.equals(str2)) ? new URI(str) : new URI(new URI(str2), str);
    }

    static Class m6201c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static String m6202d(String str) {
        String stringBuffer;
        String substring = str.substring(f4056f);
        if (substring.indexOf("%20") > -1) {
            int i = 0;
            StringBuffer stringBuffer2 = new StringBuffer(substring.length());
            int indexOf;
            do {
                indexOf = substring.indexOf("%20", i);
                if (indexOf == -1) {
                    stringBuffer2.append(substring.substring(i));
                    continue;
                } else {
                    stringBuffer2.append(substring.substring(i, indexOf));
                    stringBuffer2.append(' ');
                    i = indexOf + 3;
                    continue;
                }
            } while (indexOf != -1);
            stringBuffer = stringBuffer2.toString();
        } else {
            stringBuffer = substring;
        }
        return stringBuffer.charAt(1) == ':' ? stringBuffer : new StringBuffer().append("/").append(stringBuffer).toString();
    }

    public XMLSignatureInput m6203a(Attr attr, String str) {
        try {
            URI a = m6200a(attr.getNodeValue(), str);
            URI uri = new URI(a);
            uri.setFragment(null);
            XMLSignatureInput xMLSignatureInput = new XMLSignatureInput(new FileInputStream(m6202d(uri.toString())));
            xMLSignatureInput.m6015b(a.toString());
            return xMLSignatureInput;
        } catch (Exception e) {
            throw new ResourceResolverException("generic.EmptyMessage", e, attr, str);
        }
    }

    public boolean m6204a() {
        return true;
    }

    public boolean m6205b(Attr attr, String str) {
        if (attr == null) {
            return false;
        }
        String nodeValue = attr.getNodeValue();
        if (nodeValue.equals(BuildConfig.FLAVOR) || nodeValue.charAt(0) == '#' || nodeValue.startsWith("http:")) {
            return false;
        }
        try {
            if (f4054d.isDebugEnabled()) {
                f4054d.debug(new StringBuffer().append("I was asked whether I can resolve ").append(nodeValue).toString());
            }
            if (nodeValue.startsWith("file:") || str.startsWith("file:")) {
                if (f4054d.isDebugEnabled()) {
                    f4054d.debug(new StringBuffer().append("I state that I can resolve ").append(nodeValue).toString());
                }
                return true;
            }
        } catch (Exception e) {
        }
        f4054d.debug("But I can't");
        return false;
    }
}
