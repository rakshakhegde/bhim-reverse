package org.apache.xml.security.utils.resolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.IdResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ResolverXPointer extends ResourceResolverSpi {
    static Log f4057d;
    static Class f4058e;
    private static final int f4059f;

    static {
        Class c;
        if (f4058e == null) {
            c = m6206c("org.apache.xml.security.utils.resolver.implementations.ResolverXPointer");
            f4058e = c;
        } else {
            c = f4058e;
        }
        f4057d = LogFactory.getLog(c.getName());
        f4059f = "#xpointer(id(".length();
    }

    static Class m6206c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static boolean m6207d(String str) {
        return str.equals("#xpointer(/)");
    }

    private static boolean m6208e(String str) {
        if (str.startsWith("#xpointer(id(") && str.endsWith("))")) {
            String substring = str.substring(f4059f, str.length() - 2);
            int length = substring.length() - 1;
            if ((substring.charAt(0) == '\"' && substring.charAt(length) == '\"') || (substring.charAt(0) == '\'' && substring.charAt(length) == '\'')) {
                if (!f4057d.isDebugEnabled()) {
                    return true;
                }
                f4057d.debug(new StringBuffer().append("Id=").append(substring.substring(1, length)).toString());
                return true;
            }
        }
        return false;
    }

    private static String m6209f(String str) {
        if (str.startsWith("#xpointer(id(") && str.endsWith("))")) {
            String substring = str.substring(f4059f, str.length() - 2);
            int length = substring.length() - 1;
            if ((substring.charAt(0) == '\"' && substring.charAt(length) == '\"') || (substring.charAt(0) == '\'' && substring.charAt(length) == '\'')) {
                return substring.substring(1, length);
            }
        }
        return null;
    }

    public XMLSignatureInput m6210a(Attr attr, String str) {
        Node a;
        Document ownerDocument = attr.getOwnerElement().getOwnerDocument();
        String nodeValue = attr.getNodeValue();
        if (!m6207d(nodeValue)) {
            if (m6208e(nodeValue)) {
                a = IdResolver.m6135a(ownerDocument, m6209f(nodeValue));
                if (a == null) {
                    throw new ResourceResolverException("signature.Verification.MissingID", new Object[]{r1}, attr, str);
                }
            }
            a = null;
        }
        XMLSignatureInput xMLSignatureInput = new XMLSignatureInput(a);
        xMLSignatureInput.m6007a("text/xml");
        if (str == null || str.length() <= 0) {
            xMLSignatureInput.m6015b(attr.getNodeValue());
        } else {
            xMLSignatureInput.m6015b(str.concat(attr.getNodeValue()));
        }
        return xMLSignatureInput;
    }

    public boolean m6211a() {
        return true;
    }

    public boolean m6212b(Attr attr, String str) {
        if (attr == null) {
            return false;
        }
        String nodeValue = attr.getNodeValue();
        return m6207d(nodeValue) || m6208e(nodeValue);
    }
}
