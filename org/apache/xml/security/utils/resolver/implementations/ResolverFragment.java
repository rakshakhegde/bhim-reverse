package org.apache.xml.security.utils.resolver.implementations;

import com.crashlytics.android.core.BuildConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.IdResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ResolverFragment extends ResourceResolverSpi {
    static Log f4052d;
    static Class f4053e;

    static {
        Class c;
        if (f4053e == null) {
            c = m6196c("org.apache.xml.security.utils.resolver.implementations.ResolverFragment");
            f4053e = c;
        } else {
            c = f4053e;
        }
        f4052d = LogFactory.getLog(c.getName());
    }

    static Class m6196c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public XMLSignatureInput m6197a(Attr attr, String str) {
        Node a;
        String nodeValue = attr.getNodeValue();
        Document ownerDocument = attr.getOwnerElement().getOwnerDocument();
        if (nodeValue.equals(BuildConfig.FLAVOR)) {
            f4052d.debug("ResolverFragment with empty URI (means complete document)");
        } else {
            nodeValue = nodeValue.substring(1);
            a = IdResolver.m6135a(ownerDocument, nodeValue);
            if (a == null) {
                throw new ResourceResolverException("signature.Verification.MissingID", new Object[]{nodeValue}, attr, str);
            } else if (f4052d.isDebugEnabled()) {
                f4052d.debug(new StringBuffer().append("Try to catch an Element with ID ").append(nodeValue).append(" and Element was ").append(a).toString());
            }
        }
        XMLSignatureInput xMLSignatureInput = new XMLSignatureInput(a);
        xMLSignatureInput.m6017c(true);
        xMLSignatureInput.m6007a("text/xml");
        xMLSignatureInput.m6015b(str != null ? str.concat(attr.getNodeValue()) : attr.getNodeValue());
        return xMLSignatureInput;
    }

    public boolean m6198a() {
        return true;
    }

    public boolean m6199b(Attr attr, String str) {
        if (attr == null) {
            f4052d.debug("Quick fail for null uri");
            return false;
        }
        String nodeValue = attr.getNodeValue();
        if (nodeValue.equals(BuildConfig.FLAVOR) || (nodeValue.charAt(0) == '#' && !(nodeValue.charAt(1) == 'x' && nodeValue.startsWith("#xpointer(")))) {
            if (f4052d.isDebugEnabled()) {
                f4052d.debug(new StringBuffer().append("State I can resolve reference: \"").append(nodeValue).append("\"").toString());
            }
            return true;
        } else if (!f4052d.isDebugEnabled()) {
            return false;
        } else {
            f4052d.debug(new StringBuffer().append("Do not seem to be able to resolve reference: \"").append(nodeValue).append("\"").toString());
            return false;
        }
    }
}
