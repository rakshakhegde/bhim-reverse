package org.apache.xml.security.utils.resolver;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;

public abstract class ResourceResolverSpi {
    static Log f4045a;
    static Class f4046c;
    protected Map f4047b;

    static {
        Class b;
        if (f4046c == null) {
            b = m6183b("org.apache.xml.security.utils.resolver.ResourceResolverSpi");
            f4046c = b;
        } else {
            b = f4046c;
        }
        f4045a = LogFactory.getLog(b.getName());
    }

    public ResourceResolverSpi() {
        this.f4047b = null;
    }

    static Class m6183b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public String m6184a(String str) {
        return this.f4047b == null ? null : (String) this.f4047b.get(str);
    }

    public abstract XMLSignatureInput m6185a(Attr attr, String str);

    public void m6186a(Map map) {
        if (map != null) {
            if (this.f4047b == null) {
                this.f4047b = new HashMap();
            }
            this.f4047b.putAll(map);
        }
    }

    public boolean m6187a() {
        return false;
    }

    public abstract boolean m6188b(Attr attr, String str);
}
