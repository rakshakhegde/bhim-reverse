package org.apache.xml.security.algorithms;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;

public class JCEMapper {
    static Log f3742a;
    static Class f3743b;
    private static Map f3744c;
    private static Map f3745d;
    private static String f3746e;

    public class Algorithm {
        String f3739a;
        String f3740b;
        String f3741c;

        public Algorithm(Element element) {
            this.f3739a = element.getAttribute("AlgorithmClass");
            this.f3740b = element.getAttribute("KeyLength");
            this.f3741c = element.getAttribute("RequiredKey");
        }
    }

    static {
        Class b;
        if (f3743b == null) {
            b = m5731b("org.apache.xml.security.algorithms.JCEMapper");
            f3743b = b;
        } else {
            b = f3743b;
        }
        f3742a = LogFactory.getLog(b.getName());
        f3746e = null;
    }

    public static String m5728a() {
        return f3746e;
    }

    public static String m5729a(String str) {
        if (f3742a.isDebugEnabled()) {
            f3742a.debug(new StringBuffer().append("Request for URI ").append(str).toString());
        }
        return (String) f3744c.get(str);
    }

    public static void m5730a(Element element) {
        m5732b((Element) element.getElementsByTagName("Algorithms").item(0));
    }

    static Class m5731b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static void m5732b(Element element) {
        Element[] a = XMLUtils.m6168a(element.getFirstChild(), "http://www.xmlsecurity.org/NS/#configuration", "Algorithm");
        f3744c = new HashMap(a.length * 2);
        f3745d = new HashMap(a.length * 2);
        for (Element element2 : a) {
            String attribute = element2.getAttribute("URI");
            f3744c.put(attribute, element2.getAttribute("JCEName"));
            f3745d.put(attribute, new Algorithm(element2));
        }
    }
}
