package org.apache.xml.security.algorithms.implementations;

import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.Base64;

public class SignatureDSA extends SignatureAlgorithmSpi {
    static Log f3769a;
    static Class f3770b;
    static Class f3771c;
    private Signature f3772d;

    static {
        Class a;
        if (f3770b == null) {
            a = m5809a("org.apache.xml.security.algorithms.implementations.SignatureDSA");
            f3770b = a;
        } else {
            a = f3770b;
        }
        f3769a = LogFactory.getLog(a.getName());
    }

    public SignatureDSA() {
        this.f3772d = null;
        String a = JCEMapper.m5729a("http://www.w3.org/2000/09/xmldsig#dsa-sha1");
        if (f3769a.isDebugEnabled()) {
            f3769a.debug(new StringBuffer().append("Created SignatureDSA using ").append(a).toString());
        }
        String a2 = JCEMapper.m5728a();
        if (a2 == null) {
            try {
                this.f3772d = Signature.getInstance(a);
                return;
            } catch (NoSuchAlgorithmException e) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e.getLocalizedMessage()});
            } catch (NoSuchProviderException e2) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e2.getLocalizedMessage()});
            }
        }
        this.f3772d = Signature.getInstance(a, a2);
    }

    static Class m5809a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static byte[] m5810c(byte[] bArr) {
        int i = 20;
        if (bArr.length != 40) {
            throw new IOException("Invalid XMLDSIG format of DSA signature");
        }
        int i2 = 20;
        while (i2 > 0 && bArr[20 - i2] == null) {
            i2--;
        }
        int i3 = bArr[20 - i2] < null ? i2 + 1 : i2;
        while (i > 0 && bArr[40 - i] == null) {
            i--;
        }
        int i4 = bArr[40 - i] < null ? i + 1 : i;
        Object obj = new byte[((i3 + 6) + i4)];
        obj[0] = (byte) 48;
        obj[1] = (byte) ((i3 + 4) + i4);
        obj[2] = 2;
        obj[3] = (byte) i3;
        System.arraycopy(bArr, 20 - i2, obj, (i3 + 4) - i2, i2);
        obj[i3 + 4] = 2;
        obj[i3 + 5] = (byte) i4;
        System.arraycopy(bArr, 40 - i, obj, ((i3 + 6) + i4) - i, i);
        return obj;
    }

    protected String m5811a() {
        return this.f3772d.getAlgorithm();
    }

    protected void m5812a(byte b) {
        try {
            this.f3772d.update(b);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5813a(Key key) {
        if (key instanceof PublicKey) {
            try {
                this.f3772d.initVerify((PublicKey) key);
                return;
            } catch (Exception e) {
                Signature signature = this.f3772d;
                try {
                    this.f3772d = Signature.getInstance(this.f3772d.getAlgorithm());
                } catch (Exception e2) {
                    if (f3769a.isDebugEnabled()) {
                        f3769a.debug(new StringBuffer().append("Exception when reinstantiating Signature:").append(e2).toString());
                    }
                    this.f3772d = signature;
                }
                throw new XMLSignatureException("empty", e);
            }
        }
        Class a;
        String name = key.getClass().getName();
        if (f3771c == null) {
            a = m5809a("java.security.PublicKey");
            f3771c = a;
        } else {
            a = f3771c;
        }
        String name2 = a.getName();
        throw new XMLSignatureException("algorithms.WrongKeyForThisOperation", new Object[]{name, name2});
    }

    protected void m5814a(byte[] bArr) {
        try {
            this.f3772d.update(bArr);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5815a(byte[] bArr, int i, int i2) {
        try {
            this.f3772d.update(bArr, i, i2);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected String m5816b() {
        return this.f3772d.getProvider().getName();
    }

    protected boolean m5817b(byte[] bArr) {
        try {
            if (f3769a.isDebugEnabled()) {
                f3769a.debug(new StringBuffer().append("Called DSA.verify() on ").append(Base64.m6107b(bArr)).toString());
            }
            return this.f3772d.verify(m5810c(bArr));
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        } catch (Exception e2) {
            throw new XMLSignatureException("empty", e2);
        }
    }
}
