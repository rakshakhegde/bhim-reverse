package org.apache.xml.security.algorithms.implementations;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;

public abstract class SignatureBaseRSA extends SignatureAlgorithmSpi {
    static Log f3765a;
    static Class f3766b;
    static Class f3767c;
    private Signature f3768d;

    public class SignatureRSAMD5 extends SignatureBaseRSA {
        public String m5803d() {
            return "http://www.w3.org/2001/04/xmldsig-more#rsa-md5";
        }
    }

    public class SignatureRSARIPEMD160 extends SignatureBaseRSA {
        public String m5804d() {
            return "http://www.w3.org/2001/04/xmldsig-more#rsa-ripemd160";
        }
    }

    public class SignatureRSASHA1 extends SignatureBaseRSA {
        public String m5805d() {
            return "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
        }
    }

    public class SignatureRSASHA256 extends SignatureBaseRSA {
        public String m5806d() {
            return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
        }
    }

    public class SignatureRSASHA384 extends SignatureBaseRSA {
        public String m5807d() {
            return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384";
        }
    }

    public class SignatureRSASHA512 extends SignatureBaseRSA {
        public String m5808d() {
            return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512";
        }
    }

    static {
        Class a;
        if (f3766b == null) {
            a = m5794a("org.apache.xml.security.algorithms.implementations.SignatureBaseRSA");
            f3766b = a;
        } else {
            a = f3766b;
        }
        f3765a = LogFactory.getLog(a.getName());
    }

    public SignatureBaseRSA() {
        this.f3768d = null;
        String a = JCEMapper.m5729a(m5802d());
        if (f3765a.isDebugEnabled()) {
            f3765a.debug(new StringBuffer().append("Created SignatureRSA using ").append(a).toString());
        }
        String a2 = JCEMapper.m5728a();
        if (a2 == null) {
            try {
                this.f3768d = Signature.getInstance(a);
                return;
            } catch (NoSuchAlgorithmException e) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e.getLocalizedMessage()});
            } catch (NoSuchProviderException e2) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e2.getLocalizedMessage()});
            }
        }
        this.f3768d = Signature.getInstance(a, a2);
    }

    static Class m5794a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    protected String m5795a() {
        return this.f3768d.getAlgorithm();
    }

    protected void m5796a(byte b) {
        try {
            this.f3768d.update(b);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5797a(Key key) {
        if (key instanceof PublicKey) {
            try {
                this.f3768d.initVerify((PublicKey) key);
                return;
            } catch (Exception e) {
                Signature signature = this.f3768d;
                try {
                    this.f3768d = Signature.getInstance(this.f3768d.getAlgorithm());
                } catch (Exception e2) {
                    if (f3765a.isDebugEnabled()) {
                        f3765a.debug(new StringBuffer().append("Exception when reinstantiating Signature:").append(e2).toString());
                    }
                    this.f3768d = signature;
                }
                throw new XMLSignatureException("empty", e);
            }
        }
        Class a;
        String name = key.getClass().getName();
        if (f3767c == null) {
            a = m5794a("java.security.PublicKey");
            f3767c = a;
        } else {
            a = f3767c;
        }
        String name2 = a.getName();
        throw new XMLSignatureException("algorithms.WrongKeyForThisOperation", new Object[]{name, name2});
    }

    protected void m5798a(byte[] bArr) {
        try {
            this.f3768d.update(bArr);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5799a(byte[] bArr, int i, int i2) {
        try {
            this.f3768d.update(bArr, i, i2);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected String m5800b() {
        return this.f3768d.getProvider().getName();
    }

    protected boolean m5801b(byte[] bArr) {
        try {
            return this.f3768d.verify(bArr);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    public abstract String m5802d();
}
