package org.apache.xml.security.algorithms.implementations;

import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.Base64;

public abstract class SignatureECDSA extends SignatureAlgorithmSpi {
    static Log f3773a;
    static Class f3774b;
    static Class f3775c;
    private Signature f3776d;

    public class SignatureECDSASHA1 extends SignatureECDSA {
        public String m5828d() {
            return "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha1";
        }
    }

    static {
        Class a;
        if (f3774b == null) {
            a = m5818a("org.apache.xml.security.algorithms.implementations.SignatureECDSA");
            f3774b = a;
        } else {
            a = f3774b;
        }
        f3773a = LogFactory.getLog(a.getName());
    }

    public SignatureECDSA() {
        this.f3776d = null;
        String a = JCEMapper.m5729a(m5827d());
        if (f3773a.isDebugEnabled()) {
            f3773a.debug(new StringBuffer().append("Created SignatureECDSA using ").append(a).toString());
        }
        String a2 = JCEMapper.m5728a();
        if (a2 == null) {
            try {
                this.f3776d = Signature.getInstance(a);
                return;
            } catch (NoSuchAlgorithmException e) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e.getLocalizedMessage()});
            } catch (NoSuchProviderException e2) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e2.getLocalizedMessage()});
            }
        }
        this.f3776d = Signature.getInstance(a, a2);
    }

    static Class m5818a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static byte[] m5819c(byte[] bArr) {
        if (bArr.length < 48) {
            throw new IOException("Invalid XMLDSIG format of ECDSA signature");
        }
        int length = bArr.length / 2;
        int i = length;
        while (i > 0 && bArr[length - i] == null) {
            i--;
        }
        int i2 = bArr[length - i] < null ? i + 1 : i;
        int i3 = length;
        while (i3 > 0 && bArr[(length * 2) - i3] == null) {
            i3--;
        }
        int i4 = bArr[(length * 2) - i3] < null ? i3 + 1 : i3;
        Object obj = new byte[((i2 + 6) + i4)];
        obj[0] = 48;
        obj[1] = (byte) ((i2 + 4) + i4);
        obj[2] = 2;
        obj[3] = (byte) i2;
        System.arraycopy(bArr, length - i, obj, (i2 + 4) - i, i);
        obj[i2 + 4] = 2;
        obj[i2 + 5] = (byte) i4;
        System.arraycopy(bArr, (length * 2) - i3, obj, ((i2 + 6) + i4) - i3, i3);
        return obj;
    }

    protected String m5820a() {
        return this.f3776d.getAlgorithm();
    }

    protected void m5821a(byte b) {
        try {
            this.f3776d.update(b);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5822a(Key key) {
        if (key instanceof PublicKey) {
            try {
                this.f3776d.initVerify((PublicKey) key);
                return;
            } catch (Exception e) {
                Signature signature = this.f3776d;
                try {
                    this.f3776d = Signature.getInstance(this.f3776d.getAlgorithm());
                } catch (Exception e2) {
                    if (f3773a.isDebugEnabled()) {
                        f3773a.debug(new StringBuffer().append("Exception when reinstantiating Signature:").append(e2).toString());
                    }
                    this.f3776d = signature;
                }
                throw new XMLSignatureException("empty", e);
            }
        }
        Class a;
        String name = key.getClass().getName();
        if (f3775c == null) {
            a = m5818a("java.security.PublicKey");
            f3775c = a;
        } else {
            a = f3775c;
        }
        String name2 = a.getName();
        throw new XMLSignatureException("algorithms.WrongKeyForThisOperation", new Object[]{name, name2});
    }

    protected void m5823a(byte[] bArr) {
        try {
            this.f3776d.update(bArr);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5824a(byte[] bArr, int i, int i2) {
        try {
            this.f3776d.update(bArr, i, i2);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected String m5825b() {
        return this.f3776d.getProvider().getName();
    }

    protected boolean m5826b(byte[] bArr) {
        try {
            byte[] c = m5819c(bArr);
            if (f3773a.isDebugEnabled()) {
                f3773a.debug(new StringBuffer().append("Called ECDSA.verify() on ").append(Base64.m6107b(bArr)).toString());
            }
            return this.f3776d.verify(c);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        } catch (Exception e2) {
            throw new XMLSignatureException("empty", e2);
        }
    }

    public abstract String m5827d();
}
