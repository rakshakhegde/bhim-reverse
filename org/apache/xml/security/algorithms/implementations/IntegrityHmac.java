package org.apache.xml.security.algorithms.implementations;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public abstract class IntegrityHmac extends SignatureAlgorithmSpi {
    static Log f3759a;
    static Class f3760c;
    static Class f3761d;
    int f3762b;
    private Mac f3763e;
    private boolean f3764f;

    public class IntegrityHmacMD5 extends IntegrityHmac {
        public String m5782d() {
            return "http://www.w3.org/2001/04/xmldsig-more#hmac-md5";
        }

        int m5783e() {
            return 128;
        }
    }

    public class IntegrityHmacRIPEMD160 extends IntegrityHmac {
        public String m5784d() {
            return "http://www.w3.org/2001/04/xmldsig-more#hmac-ripemd160";
        }

        int m5785e() {
            return 160;
        }
    }

    public class IntegrityHmacSHA1 extends IntegrityHmac {
        public String m5786d() {
            return "http://www.w3.org/2000/09/xmldsig#hmac-sha1";
        }

        int m5787e() {
            return 160;
        }
    }

    public class IntegrityHmacSHA256 extends IntegrityHmac {
        public String m5788d() {
            return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
        }

        int m5789e() {
            return 256;
        }
    }

    public class IntegrityHmacSHA384 extends IntegrityHmac {
        public String m5790d() {
            return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha384";
        }

        int m5791e() {
            return 384;
        }
    }

    public class IntegrityHmacSHA512 extends IntegrityHmac {
        public String m5792d() {
            return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha512";
        }

        int m5793e() {
            return 512;
        }
    }

    static {
        Class a;
        if (f3760c == null) {
            a = m5770a("org.apache.xml.security.algorithms.implementations.IntegrityHmac$IntegrityHmacSHA1");
            f3760c = a;
        } else {
            a = f3760c;
        }
        f3759a = LogFactory.getLog(a.getName());
    }

    public IntegrityHmac() {
        this.f3763e = null;
        this.f3762b = 0;
        this.f3764f = false;
        String a = JCEMapper.m5729a(m5780d());
        if (f3759a.isDebugEnabled()) {
            f3759a.debug(new StringBuffer().append("Created IntegrityHmacSHA1 using ").append(a).toString());
        }
        try {
            this.f3763e = Mac.getInstance(a);
        } catch (NoSuchAlgorithmException e) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e.getLocalizedMessage()});
        }
    }

    static Class m5770a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    protected String m5771a() {
        f3759a.debug("engineGetJCEAlgorithmString()");
        return this.f3763e.getAlgorithm();
    }

    protected void m5772a(byte b) {
        try {
            this.f3763e.update(b);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5773a(Key key) {
        if (key instanceof SecretKey) {
            try {
                this.f3763e.init(key);
                return;
            } catch (Exception e) {
                Mac mac = this.f3763e;
                try {
                    this.f3763e = Mac.getInstance(this.f3763e.getAlgorithm());
                } catch (Exception e2) {
                    if (f3759a.isDebugEnabled()) {
                        f3759a.debug(new StringBuffer().append("Exception when reinstantiating Mac:").append(e2).toString());
                    }
                    this.f3763e = mac;
                }
                throw new XMLSignatureException("empty", e);
            }
        }
        Class a;
        String name = key.getClass().getName();
        if (f3761d == null) {
            a = m5770a("javax.crypto.SecretKey");
            f3761d = a;
        } else {
            a = f3761d;
        }
        String name2 = a.getName();
        throw new XMLSignatureException("algorithms.WrongKeyForThisOperation", new Object[]{name, name2});
    }

    protected void m5774a(Element element) {
        super.m5764a(element);
        if (element == null) {
            throw new IllegalArgumentException("element null");
        }
        Text b = XMLUtils.m6170b(element.getFirstChild(), "HMACOutputLength", 0);
        if (b != null) {
            this.f3762b = Integer.parseInt(b.getData());
            this.f3764f = true;
        }
    }

    protected void m5775a(byte[] bArr) {
        try {
            this.f3763e.update(bArr);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected void m5776a(byte[] bArr, int i, int i2) {
        try {
            this.f3763e.update(bArr, i, i2);
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    protected String m5777b() {
        return this.f3763e.getProvider().getName();
    }

    protected boolean m5778b(byte[] bArr) {
        try {
            if (!this.f3764f || this.f3762b >= m5781e()) {
                return MessageDigestAlgorithm.m5734a(this.f3763e.doFinal(), bArr);
            }
            if (f3759a.isDebugEnabled()) {
                f3759a.debug(new StringBuffer().append("HMACOutputLength must not be less than ").append(m5781e()).toString());
            }
            throw new XMLSignatureException("algorithms.HMACOutputLengthMin", new Object[]{String.valueOf(m5781e())});
        } catch (Exception e) {
            throw new XMLSignatureException("empty", e);
        }
    }

    public void m5779c() {
        this.f3762b = 0;
        this.f3764f = false;
        this.f3763e.reset();
    }

    public abstract String m5780d();

    abstract int m5781e();
}
