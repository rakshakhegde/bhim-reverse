package org.apache.xml.security.algorithms;

import java.security.Key;
import org.w3c.dom.Element;

public abstract class SignatureAlgorithmSpi {
    protected abstract String m5761a();

    protected abstract void m5762a(byte b);

    protected abstract void m5763a(Key key);

    protected void m5764a(Element element) {
    }

    protected abstract void m5765a(byte[] bArr);

    protected abstract void m5766a(byte[] bArr, int i, int i2);

    protected abstract String m5767b();

    protected abstract boolean m5768b(byte[] bArr);

    public void m5769c() {
    }
}
