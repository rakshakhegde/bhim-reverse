package org.apache.xml.security.algorithms;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.apache.xml.security.signature.XMLSignatureException;
import org.w3c.dom.Element;

public class SignatureAlgorithm extends Algorithm {
    static Log f3749a;
    static boolean f3750b;
    static HashMap f3751c;
    static ThreadLocal f3752d;
    static ThreadLocal f3753e;
    static ThreadLocal f3754f;
    static ThreadLocal f3755g;
    static Class f3756i;
    protected SignatureAlgorithmSpi f3757h;
    private String f3758q;

    /* renamed from: org.apache.xml.security.algorithms.SignatureAlgorithm.1 */
    class C06071 extends ThreadLocal {
        C06071() {
        }

        protected Object initialValue() {
            return new HashMap();
        }
    }

    /* renamed from: org.apache.xml.security.algorithms.SignatureAlgorithm.2 */
    class C06082 extends ThreadLocal {
        C06082() {
        }

        protected Object initialValue() {
            return new HashMap();
        }
    }

    /* renamed from: org.apache.xml.security.algorithms.SignatureAlgorithm.3 */
    class C06093 extends ThreadLocal {
        C06093() {
        }

        protected Object initialValue() {
            return new HashMap();
        }
    }

    /* renamed from: org.apache.xml.security.algorithms.SignatureAlgorithm.4 */
    class C06104 extends ThreadLocal {
        C06104() {
        }

        protected Object initialValue() {
            return new HashMap();
        }
    }

    static {
        Class b;
        if (f3756i == null) {
            b = m5744b("org.apache.xml.security.algorithms.SignatureAlgorithm");
            f3756i = b;
        } else {
            b = f3756i;
        }
        f3749a = LogFactory.getLog(b.getName());
        f3750b = false;
        f3751c = null;
        f3752d = new C06071();
        f3753e = new C06082();
        f3754f = new C06093();
        f3755g = new C06104();
    }

    public SignatureAlgorithm(Element element, String str) {
        super(element, str);
        this.f3757h = null;
        this.f3758q = m5760g();
    }

    public static void m5742a(String str, String str2) {
        if (f3749a.isDebugEnabled()) {
            f3749a.debug(new StringBuffer().append("Try to register ").append(str).append(" ").append(str2).toString());
        }
        Class g = m5748g(str);
        if (g != null) {
            String name = g.getName();
            if (!(name == null || name.length() == 0)) {
                throw new AlgorithmAlreadyRegisteredException("algorithm.alreadyRegistered", new Object[]{str, name});
            }
        }
        try {
            f3751c.put(str, Class.forName(str2));
        } catch (Exception e) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{str, e.getMessage()}, e);
        } catch (Exception e2) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{str, e2.getMessage()}, e2);
        }
    }

    private void m5743a(boolean z) {
        this.f3757h = z ? m5745d(this.f3758q) : m5746e(this.f3758q);
        this.f3757h.m5764a(this.k);
    }

    static Class m5744b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static SignatureAlgorithmSpi m5745d(String str) {
        SignatureAlgorithmSpi signatureAlgorithmSpi = (SignatureAlgorithmSpi) ((Map) f3752d.get()).get(str);
        if (signatureAlgorithmSpi != null) {
            signatureAlgorithmSpi.m5769c();
            return signatureAlgorithmSpi;
        }
        SignatureAlgorithmSpi f = m5747f(str);
        ((Map) f3752d.get()).put(str, f);
        return f;
    }

    private static SignatureAlgorithmSpi m5746e(String str) {
        SignatureAlgorithmSpi signatureAlgorithmSpi = (SignatureAlgorithmSpi) ((Map) f3753e.get()).get(str);
        if (signatureAlgorithmSpi != null) {
            signatureAlgorithmSpi.m5769c();
            return signatureAlgorithmSpi;
        }
        SignatureAlgorithmSpi f = m5747f(str);
        ((Map) f3753e.get()).put(str, f);
        return f;
    }

    private static SignatureAlgorithmSpi m5747f(String str) {
        try {
            Class g = m5748g(str);
            if (f3749a.isDebugEnabled()) {
                f3749a.debug(new StringBuffer().append("Create URI \"").append(str).append("\" class \"").append(g).append("\"").toString());
            }
            return (SignatureAlgorithmSpi) g.newInstance();
        } catch (Exception e) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{str, e.getMessage()}, e);
        } catch (Exception e2) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{str, e2.getMessage()}, e2);
        } catch (Exception e22) {
            throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{str, e22.getMessage()}, e22);
        }
    }

    private static Class m5748g(String str) {
        return f3751c == null ? null : (Class) f3751c.get(str);
    }

    public static void m5749h() {
        if (f3749a == null) {
            Class b;
            if (f3756i == null) {
                b = m5744b("org.apache.xml.security.algorithms.SignatureAlgorithm");
                f3756i = b;
            } else {
                b = f3756i;
            }
            f3749a = LogFactory.getLog(b.getName());
        }
        f3749a.debug("Init() called");
        if (!f3750b) {
            f3751c = new HashMap(10);
            f3750b = true;
        }
    }

    public void m5750a(byte b) {
        this.f3757h.m5762a(b);
    }

    public void m5751a(Key key) {
        m5743a(false);
        Map map = (Map) f3755g.get();
        if (map.get(this.f3758q) != key) {
            map.put(this.f3758q, key);
            this.f3757h.m5763a(key);
        }
    }

    public void m5752a(byte[] bArr) {
        this.f3757h.m5765a(bArr);
    }

    public void m5753a(byte[] bArr, int i, int i2) {
        this.f3757h.m5766a(bArr, i, i2);
    }

    public String m5754b() {
        try {
            return m5746e(this.f3758q).m5761a();
        } catch (XMLSignatureException e) {
            return null;
        }
    }

    public boolean m5755b(byte[] bArr) {
        return this.f3757h.m5768b(bArr);
    }

    public String m5756c() {
        try {
            return m5746e(this.f3758q).m5767b();
        } catch (XMLSignatureException e) {
            return null;
        }
    }

    public String m5757d() {
        return "http://www.w3.org/2000/09/xmldsig#";
    }

    public String m5758e() {
        return "SignatureMethod";
    }

    public void m5759f() {
        ((Map) f3755g.get()).clear();
        ((Map) f3753e.get()).clear();
    }

    public final String m5760g() {
        return this.k.getAttributeNS(null, "Algorithm");
    }
}
