package org.apache.xml.security.algorithms;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import org.apache.xml.security.signature.XMLSignatureException;
import org.w3c.dom.Document;

public class MessageDigestAlgorithm extends Algorithm {
    static ThreadLocal f3747b;
    MessageDigest f3748a;

    /* renamed from: org.apache.xml.security.algorithms.MessageDigestAlgorithm.1 */
    class C06061 extends ThreadLocal {
        C06061() {
        }

        protected Object initialValue() {
            return new HashMap();
        }
    }

    static {
        f3747b = new C06061();
    }

    private MessageDigestAlgorithm(Document document, MessageDigest messageDigest, String str) {
        super(document, str);
        this.f3748a = null;
        this.f3748a = messageDigest;
    }

    public static MessageDigestAlgorithm m5733a(Document document, String str) {
        return new MessageDigestAlgorithm(document, m5735b(str), str);
    }

    public static boolean m5734a(byte[] bArr, byte[] bArr2) {
        return MessageDigest.isEqual(bArr, bArr2);
    }

    private static MessageDigest m5735b(String str) {
        MessageDigest messageDigest = (MessageDigest) ((Map) f3747b.get()).get(str);
        if (messageDigest != null) {
            return messageDigest;
        }
        String a = JCEMapper.m5729a(str);
        if (a == null) {
            throw new XMLSignatureException("algorithms.NoSuchMap", new Object[]{str});
        }
        MessageDigest instance;
        String a2 = JCEMapper.m5728a();
        if (a2 == null) {
            try {
                instance = MessageDigest.getInstance(a);
            } catch (NoSuchAlgorithmException e) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e.getLocalizedMessage()});
            } catch (NoSuchProviderException e2) {
                throw new XMLSignatureException("algorithms.NoSuchAlgorithm", new Object[]{a, e2.getLocalizedMessage()});
            }
        }
        instance = MessageDigest.getInstance(a, a2);
        ((Map) f3747b.get()).put(str, instance);
        return instance;
    }

    public void m5736a(byte b) {
        this.f3748a.update(b);
    }

    public void m5737a(byte[] bArr, int i, int i2) {
        this.f3748a.update(bArr, i, i2);
    }

    public byte[] m5738b() {
        return this.f3748a.digest();
    }

    public void m5739c() {
        this.f3748a.reset();
    }

    public String m5740d() {
        return "http://www.w3.org/2000/09/xmldsig#";
    }

    public String m5741e() {
        return "DigestMethod";
    }
}
