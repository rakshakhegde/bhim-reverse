package org.apache.xml.security.encryption;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class XMLCipherInput {
    static Class f3846a;
    private static Log f3847b;

    static {
        Class a;
        if (f3846a == null) {
            a = m5924a("org.apache.xml.security.encryption.XMLCipher");
            f3846a = a;
        } else {
            a = f3846a;
        }
        f3847b = LogFactory.getLog(a.getName());
    }

    static Class m5924a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
