package org.apache.xml.security.transforms;

import com.crashlytics.android.core.BuildConfig;
import java.io.OutputStream;
import org.apache.xml.security.signature.XMLSignatureInput;

public abstract class TransformSpi {
    protected Transform f3957a;

    public TransformSpi() {
        this.f3957a = null;
    }

    protected XMLSignatureInput m6044a(XMLSignatureInput xMLSignatureInput) {
        throw new UnsupportedOperationException();
    }

    protected XMLSignatureInput m6045a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream, Transform transform) {
        return m6046a(xMLSignatureInput, transform);
    }

    protected XMLSignatureInput m6046a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        try {
            TransformSpi transformSpi = (TransformSpi) getClass().newInstance();
            transformSpi.m6047a(transform);
            return transformSpi.m6044a(xMLSignatureInput);
        } catch (Exception e) {
            throw new TransformationException(BuildConfig.FLAVOR, e);
        } catch (Exception e2) {
            throw new TransformationException(BuildConfig.FLAVOR, e2);
        }
    }

    protected void m6047a(Transform transform) {
        this.f3957a = transform;
    }
}
