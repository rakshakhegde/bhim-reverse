package org.apache.xml.security.transforms.implementations;

import java.io.BufferedInputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.Base64;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class TransformBase64Decode extends TransformSpi {
    protected XMLSignatureInput m6048a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream, Transform transform) {
        try {
            StringBuffer stringBuffer;
            XMLSignatureInput xMLSignatureInput2;
            if (xMLSignatureInput.m6022g()) {
                Node m = xMLSignatureInput.m6028m();
                if (xMLSignatureInput.m6028m().getNodeType() == (short) 3) {
                    m = m.getParentNode();
                }
                stringBuffer = new StringBuffer();
                m6050a((Element) m, stringBuffer);
                if (outputStream == null) {
                    return new XMLSignatureInput(Base64.m6104a(stringBuffer.toString()));
                }
                Base64.m6100a(stringBuffer.toString(), outputStream);
                xMLSignatureInput2 = new XMLSignatureInput((byte[]) null);
                xMLSignatureInput2.m6014b(outputStream);
                return xMLSignatureInput2;
            } else if (!xMLSignatureInput.m6023h() && !xMLSignatureInput.m6021f()) {
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xMLSignatureInput.m6016c()).getDocumentElement();
                stringBuffer = new StringBuffer();
                m6050a(documentElement, stringBuffer);
                return new XMLSignatureInput(Base64.m6104a(stringBuffer.toString()));
            } else if (outputStream == null) {
                return new XMLSignatureInput(Base64.m6106a(xMLSignatureInput.m6020e()));
            } else {
                if (xMLSignatureInput.m6025j() || xMLSignatureInput.m6021f()) {
                    Base64.m6101a(xMLSignatureInput.m6020e(), outputStream);
                } else {
                    Base64.m6099a(new BufferedInputStream(xMLSignatureInput.m6018d()), outputStream);
                }
                xMLSignatureInput2 = new XMLSignatureInput((byte[]) null);
                xMLSignatureInput2.m6014b(outputStream);
                return xMLSignatureInput2;
            }
        } catch (Exception e) {
            throw new TransformationException("c14n.Canonicalizer.Exception", e);
        } catch (Exception e2) {
            throw new TransformationException("SAX exception", e2);
        } catch (Exception e22) {
            throw new TransformationException("Base64Decoding", e22);
        }
    }

    protected XMLSignatureInput m6049a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        return m6048a(xMLSignatureInput, null, transform);
    }

    void m6050a(Element element, StringBuffer stringBuffer) {
        for (Node firstChild = element.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            switch (firstChild.getNodeType()) {
                case R.View_android_focusable /*1*/:
                    m6050a((Element) firstChild, stringBuffer);
                    break;
                case R.View_paddingEnd /*3*/:
                    stringBuffer.append(((Text) firstChild).getData());
                    break;
                default:
                    break;
            }
        }
    }
}
