package org.apache.xml.security.transforms.implementations;

import java.io.OutputStream;
import org.apache.xml.security.c14n.implementations.Canonicalizer11_OmitComments;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;

public class TransformC14N11 extends TransformSpi {
    protected XMLSignatureInput m6051a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream, Transform transform) {
        Canonicalizer11_OmitComments canonicalizer11_OmitComments = new Canonicalizer11_OmitComments();
        if (outputStream != null) {
            canonicalizer11_OmitComments.m5854a(outputStream);
        }
        XMLSignatureInput xMLSignatureInput2 = new XMLSignatureInput(canonicalizer11_OmitComments.m5863b(xMLSignatureInput));
        if (outputStream != null) {
            xMLSignatureInput2.m6014b(outputStream);
        }
        return xMLSignatureInput2;
    }

    protected XMLSignatureInput m6052a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        return m6051a(xMLSignatureInput, null, transform);
    }
}
