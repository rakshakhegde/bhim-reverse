package org.apache.xml.security.transforms.implementations;

import java.util.Set;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;

class XPath2NodeFilter implements NodeFilter {
    boolean f3967a;
    boolean f3968b;
    boolean f3969c;
    Set f3970d;
    Set f3971e;
    Set f3972f;
    int f3973g;
    int f3974h;
    int f3975i;

    XPath2NodeFilter(Set set, Set set2, Set set3) {
        boolean z = true;
        this.f3973g = -1;
        this.f3974h = -1;
        this.f3975i = -1;
        this.f3970d = set;
        this.f3967a = !set.isEmpty();
        this.f3971e = set2;
        this.f3968b = !set2.isEmpty();
        this.f3972f = set3;
        if (set3.isEmpty()) {
            z = false;
        }
        this.f3969c = z;
    }

    static boolean m6077a(Node node, Set set) {
        if (set.contains(node)) {
            return true;
        }
        for (Node a : set) {
            if (XMLUtils.m6166a(a, node)) {
                return true;
            }
        }
        return false;
    }

    static boolean m6078b(Node node, Set set) {
        return set.contains(node);
    }

    public int m6079a(Node node) {
        int i = (this.f3968b && m6077a(node, this.f3971e)) ? -1 : (!this.f3969c || m6077a(node, this.f3972f)) ? 1 : 0;
        return i == 1 ? 1 : this.f3967a ? m6077a(node, this.f3970d) ? 1 : 0 : i;
    }

    public int m6080a(Node node, int i) {
        int i2;
        if (this.f3968b) {
            if (this.f3973g == -1 || i <= this.f3973g) {
                if (m6078b(node, this.f3971e)) {
                    this.f3973g = i;
                } else {
                    this.f3973g = -1;
                }
            }
            if (this.f3973g != -1) {
                i2 = -1;
                if (i2 != -1 && this.f3969c && (this.f3974h == -1 || i <= this.f3974h)) {
                    if (m6078b(node, this.f3972f)) {
                        this.f3974h = -1;
                        i2 = 0;
                    } else {
                        this.f3974h = i;
                    }
                }
                if (i <= this.f3975i) {
                    this.f3975i = -1;
                }
                if (i2 == 1) {
                    return 1;
                }
                if (this.f3967a) {
                    return i2;
                }
                if (this.f3975i == -1 && m6078b(node, this.f3970d)) {
                    this.f3975i = i;
                }
                return this.f3975i == -1 ? 1 : 0;
            }
        }
        i2 = 1;
        if (m6078b(node, this.f3972f)) {
            this.f3974h = i;
        } else {
            this.f3974h = -1;
            i2 = 0;
        }
        if (i <= this.f3975i) {
            this.f3975i = -1;
        }
        if (i2 == 1) {
            return 1;
        }
        if (this.f3967a) {
            return i2;
        }
        this.f3975i = i;
        if (this.f3975i == -1) {
        }
    }
}
