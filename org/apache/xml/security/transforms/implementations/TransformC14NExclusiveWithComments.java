package org.apache.xml.security.transforms.implementations;

import java.io.OutputStream;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.implementations.Canonicalizer20010315ExclWithComments;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;

public class TransformC14NExclusiveWithComments extends TransformSpi {
    protected XMLSignatureInput m6059a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream, Transform transform) {
        String str = null;
        try {
            if (transform.m5717c("http://www.w3.org/2001/10/xml-exc-c14n#", "InclusiveNamespaces") == 1) {
                str = new InclusiveNamespaces(XMLUtils.m6162a(transform.m5720k().getFirstChild(), "http://www.w3.org/2001/10/xml-exc-c14n#", "InclusiveNamespaces", 0), transform.m5721l()).m6082a();
            }
            Canonicalizer20010315ExclWithComments canonicalizer20010315ExclWithComments = new Canonicalizer20010315ExclWithComments();
            if (outputStream != null) {
                canonicalizer20010315ExclWithComments.m5854a(outputStream);
            }
            return new XMLSignatureInput(canonicalizer20010315ExclWithComments.m5887a(xMLSignatureInput, str));
        } catch (Exception e) {
            throw new CanonicalizationException("empty", e);
        }
    }

    protected XMLSignatureInput m6060a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        return m6059a(xMLSignatureInput, null, transform);
    }
}
