package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.exceptions.XMLSecurityRuntimeException;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.CachedXPathAPIHolder;
import org.apache.xml.security.utils.CachedXPathFuncHereAPI;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xml.utils.PrefixResolverDefault;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TransformXPath extends TransformSpi {

    class XPathNodeFilter implements NodeFilter {
        PrefixResolverDefault f3959a;
        CachedXPathFuncHereAPI f3960b;
        Node f3961c;
        String f3962d;

        XPathNodeFilter(Element element, Node node, String str) {
            this.f3960b = new CachedXPathFuncHereAPI(CachedXPathAPIHolder.m6111a());
            this.f3961c = node;
            this.f3962d = str;
            this.f3959a = new PrefixResolverDefault(element);
        }

        public int m6067a(Node node) {
            try {
                return this.f3960b.m6117a(node, this.f3961c, this.f3962d, this.f3959a).bool() ? 1 : 0;
            } catch (Exception e) {
                throw new XMLSecurityRuntimeException("signature.Transform.node", new Object[]{node}, e);
            } catch (Exception e2) {
                throw new XMLSecurityRuntimeException("signature.Transform.nodeAndType", new Object[]{node, new Short(node.getNodeType())}, e2);
            }
        }

        public int m6068a(Node node, int i) {
            return m6067a(node);
        }
    }

    private boolean m6071a(String str) {
        return (str.indexOf("namespace") == -1 && str.indexOf("name()") == -1) ? false : true;
    }

    protected XMLSignatureInput m6072a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        try {
            CachedXPathAPIHolder.m6112a(transform.m5720k().getOwnerDocument());
            Element a = XMLUtils.m6161a(transform.m5720k().getFirstChild(), "XPath", 0);
            if (a == null) {
                throw new TransformationException("xml.WrongContent", new Object[]{"ds:XPath", "Transform"});
            }
            Node item = a.getChildNodes().item(0);
            String a2 = CachedXPathFuncHereAPI.m6114a(item);
            xMLSignatureInput.m6010a(m6071a(a2));
            if (item == null) {
                throw new DOMException((short) 3, "Text must be in ds:Xpath");
            }
            xMLSignatureInput.m6008a(new XPathNodeFilter(a, item, a2));
            xMLSignatureInput.m6019d(true);
            return xMLSignatureInput;
        } catch (Exception e) {
            throw new TransformationException("empty", e);
        }
    }
}
