package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TransformEnvelopedSignature extends TransformSpi {

    class EnvelopedNodeFilter implements NodeFilter {
        Node f3958a;

        EnvelopedNodeFilter(Node node) {
            this.f3958a = node;
        }

        public int m6063a(Node node) {
            return (node == this.f3958a || XMLUtils.m6166a(this.f3958a, node)) ? -1 : 1;
        }

        public int m6064a(Node node, int i) {
            return node == this.f3958a ? -1 : 1;
        }
    }

    private static Node m6065a(Node node) {
        Object obj;
        Node node2 = node;
        while (node2 != null) {
            if (node2.getNodeType() != (short) 9) {
                Element element = (Element) node2;
                if (element.getNamespaceURI().equals("http://www.w3.org/2000/09/xmldsig#") && element.getLocalName().equals("Signature")) {
                    obj = 1;
                    break;
                }
                node2 = node2.getParentNode();
            } else {
                obj = null;
                break;
            }
        }
        obj = null;
        if (obj != null) {
            return node2;
        }
        throw new TransformationException("transform.envelopedSignatureTransformNotInSignatureElement");
    }

    protected XMLSignatureInput m6066a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        Node a = m6065a(transform.m5720k());
        xMLSignatureInput.m6009a(a);
        xMLSignatureInput.m6008a(new EnvelopedNodeFilter(a));
        return xMLSignatureInput;
    }
}
