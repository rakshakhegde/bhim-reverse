package org.apache.xml.security.transforms.implementations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.params.XPath2FilterContainer;
import org.apache.xml.security.utils.CachedXPathAPIHolder;
import org.apache.xml.security.utils.CachedXPathFuncHereAPI;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TransformXPath2Filter extends TransformSpi {
    static Set m6069a(List list) {
        Set hashSet = new HashSet();
        for (int i = 0; i < list.size(); i++) {
            NodeList nodeList = (NodeList) list.get(i);
            int length = nodeList.getLength();
            for (int i2 = 0; i2 < length; i2++) {
                hashSet.add(nodeList.item(i2));
            }
        }
        return hashSet;
    }

    protected XMLSignatureInput m6070a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        int i = 0;
        CachedXPathAPIHolder.m6112a(transform.m5720k().getOwnerDocument());
        try {
            List arrayList = new ArrayList();
            List arrayList2 = new ArrayList();
            List arrayList3 = new ArrayList();
            CachedXPathFuncHereAPI cachedXPathFuncHereAPI = new CachedXPathFuncHereAPI(CachedXPathAPIHolder.m6111a());
            int length = XMLUtils.m6168a(transform.m5720k().getFirstChild(), "http://www.w3.org/2002/06/xmldsig-filter2", "XPath").length;
            if (length == 0) {
                throw new TransformationException("xml.WrongContent", new Object[]{"http://www.w3.org/2002/06/xmldsig-filter2", "XPath"});
            }
            Node b = xMLSignatureInput.m6028m() != null ? XMLUtils.m6169b(xMLSignatureInput.m6028m()) : XMLUtils.m6158a(xMLSignatureInput.m6012b());
            while (i < length) {
                XPath2FilterContainer a = XPath2FilterContainer.m6087a(XMLUtils.m6162a(transform.m5720k().getFirstChild(), "http://www.w3.org/2002/06/xmldsig-filter2", "XPath", i), xMLSignatureInput.m6026k());
                NodeList a2 = cachedXPathFuncHereAPI.m6118a(b, a.m6093f(), CachedXPathFuncHereAPI.m6114a(a.m6093f()), a.m5720k());
                if (a.m6088a()) {
                    arrayList3.add(a2);
                } else if (a.m6089b()) {
                    arrayList2.add(a2);
                } else if (a.m6090c()) {
                    arrayList.add(a2);
                }
                i++;
            }
            xMLSignatureInput.m6008a(new XPath2NodeFilter(m6069a(arrayList), m6069a(arrayList2), m6069a(arrayList3)));
            xMLSignatureInput.m6019d(true);
            return xMLSignatureInput;
        } catch (Exception e) {
            throw new TransformationException("empty", e);
        } catch (Exception e2) {
            throw new TransformationException("empty", e2);
        } catch (Exception e22) {
            throw new TransformationException("empty", e22);
        } catch (Exception e222) {
            throw new TransformationException("empty", e222);
        } catch (Exception e2222) {
            throw new TransformationException("empty", e2222);
        } catch (Exception e22222) {
            throw new TransformationException("empty", e22222);
        } catch (Exception e222222) {
            throw new TransformationException("empty", e222222);
        } catch (Exception e2222222) {
            throw new TransformationException("empty", e2222222);
        }
    }
}
