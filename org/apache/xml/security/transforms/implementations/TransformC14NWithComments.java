package org.apache.xml.security.transforms.implementations;

import java.io.OutputStream;
import org.apache.xml.security.c14n.implementations.Canonicalizer20010315WithComments;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;

public class TransformC14NWithComments extends TransformSpi {
    protected XMLSignatureInput m6061a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream, Transform transform) {
        Canonicalizer20010315WithComments canonicalizer20010315WithComments = new Canonicalizer20010315WithComments();
        if (outputStream != null) {
            canonicalizer20010315WithComments.m5854a(outputStream);
        }
        XMLSignatureInput xMLSignatureInput2 = new XMLSignatureInput(canonicalizer20010315WithComments.m5863b(xMLSignatureInput));
        if (outputStream != null) {
            xMLSignatureInput2.m6014b(outputStream);
        }
        return xMLSignatureInput2;
    }

    protected XMLSignatureInput m6062a(XMLSignatureInput xMLSignatureInput, Transform transform) {
        return m6061a(xMLSignatureInput, null, transform);
    }
}
