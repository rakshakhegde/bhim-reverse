package org.apache.xml.security.transforms;

import java.io.OutputStream;
import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.ClassLoaderUtils;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class Transform extends SignatureElementProxy {
    static Class f3951a;
    private static Log f3952b;
    private static boolean f3953c;
    private static HashMap f3954d;
    private static HashMap f3955e;
    private TransformSpi f3956f;

    static {
        Class a;
        if (f3951a == null) {
            a = m6033a("org.apache.xml.security.transforms.Transform");
            f3951a = a;
        } else {
            a = f3951a;
        }
        f3952b = LogFactory.getLog(a.getName());
        f3953c = false;
        f3954d = null;
        f3955e = new HashMap();
    }

    public Transform(Document document, String str, NodeList nodeList) {
        int i = 0;
        super(document);
        this.f3956f = null;
        this.k.setAttributeNS(null, "Algorithm", str);
        this.f3956f = m6039d(str);
        if (this.f3956f == null) {
            throw new InvalidTransformException("signature.Transform.UnknownTransform", new Object[]{str});
        }
        if (f3952b.isDebugEnabled()) {
            f3952b.debug(new StringBuffer().append("Create URI \"").append(str).append("\" class \"").append(this.f3956f.getClass()).append("\"").toString());
            f3952b.debug(new StringBuffer().append("The NodeList is ").append(nodeList).toString());
        }
        if (nodeList != null) {
            while (i < nodeList.getLength()) {
                this.k.appendChild(nodeList.item(i).cloneNode(true));
                i++;
            }
        }
    }

    public Transform(Element element, String str) {
        super(element, str);
        this.f3956f = null;
        String attributeNS = element.getAttributeNS(null, "Algorithm");
        if (attributeNS == null || attributeNS.length() == 0) {
            throw new TransformationException("xml.WrongContent", new Object[]{"Algorithm", "Transform"});
        }
        this.f3956f = m6039d(attributeNS);
        if (this.f3956f == null) {
            throw new InvalidTransformException("signature.Transform.UnknownTransform", new Object[]{attributeNS});
        }
    }

    static Class m6033a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static Transform m6034a(Document document, String str) {
        return m6035a(document, str, (NodeList) null);
    }

    public static Transform m6035a(Document document, String str, NodeList nodeList) {
        return new Transform(document, str, nodeList);
    }

    public static void m6036a() {
        if (!f3953c) {
            f3954d = new HashMap(10);
            f3953c = true;
        }
    }

    public static void m6037a(String str, String str2) {
        if (m6038b(str) != null) {
            throw new AlgorithmAlreadyRegisteredException("algorithm.alreadyRegistered", new Object[]{str, m6038b(str)});
        }
        try {
            Class a;
            HashMap hashMap = f3954d;
            if (f3951a == null) {
                a = m6033a("org.apache.xml.security.transforms.Transform");
                f3951a = a;
            } else {
                a = f3951a;
            }
            hashMap.put(str, ClassLoaderUtils.m6121a(str2, a));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private static Class m6038b(String str) {
        return (Class) f3954d.get(str);
    }

    private static TransformSpi m6039d(String str) {
        try {
            Object obj = f3955e.get(str);
            if (obj != null) {
                return (TransformSpi) obj;
            }
            Class cls = (Class) f3954d.get(str);
            if (cls == null) {
                return null;
            }
            TransformSpi transformSpi = (TransformSpi) cls.newInstance();
            f3955e.put(str, transformSpi);
            return transformSpi;
        } catch (Exception e) {
            throw new InvalidTransformException("signature.Transform.UnknownTransform", new Object[]{str}, e);
        } catch (Exception e2) {
            throw new InvalidTransformException("signature.Transform.UnknownTransform", new Object[]{str}, e2);
        }
    }

    public XMLSignatureInput m6040a(XMLSignatureInput xMLSignatureInput) {
        try {
            return this.f3956f.m6046a(xMLSignatureInput, this);
        } catch (Exception e) {
            throw new CanonicalizationException("signature.Transform.ErrorDuringTransform", new Object[]{m6042b(), "ParserConfigurationException"}, e);
        } catch (Exception e2) {
            throw new CanonicalizationException("signature.Transform.ErrorDuringTransform", new Object[]{m6042b(), "SAXException"}, e2);
        }
    }

    public XMLSignatureInput m6041a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream) {
        try {
            return this.f3956f.m6045a(xMLSignatureInput, outputStream, this);
        } catch (Exception e) {
            throw new CanonicalizationException("signature.Transform.ErrorDuringTransform", new Object[]{m6042b(), "ParserConfigurationException"}, e);
        } catch (Exception e2) {
            throw new CanonicalizationException("signature.Transform.ErrorDuringTransform", new Object[]{m6042b(), "SAXException"}, e2);
        }
    }

    public String m6042b() {
        return this.k.getAttributeNS(null, "Algorithm");
    }

    public String m6043e() {
        return "Transform";
    }
}
