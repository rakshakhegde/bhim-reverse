package org.apache.xml.security.transforms.params;

import org.apache.xml.security.transforms.TransformParam;
import org.apache.xml.security.utils.ElementProxy;

public class XPath2FilterContainer04 extends ElementProxy implements TransformParam {
    private XPath2FilterContainer04() {
    }

    public final String m6085d() {
        return "http://www.w3.org/2002/04/xmldsig-filter2";
    }

    public final String m6086e() {
        return "XPath";
    }
}
