package org.apache.xml.security.transforms;

import java.io.OutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Transforms extends SignatureElementProxy {
    static Log f3841a;
    static Class f3842c;
    Element[] f3843b;

    static {
        Class b;
        if (f3842c == null) {
            b = m5916b("org.apache.xml.security.transforms.Transforms");
            f3842c = b;
        } else {
            b = f3842c;
        }
        f3841a = LogFactory.getLog(b.getName());
    }

    protected Transforms() {
    }

    public Transforms(Document document) {
        super(document);
        XMLUtils.m6171b(this.k);
    }

    public Transforms(Element element, String str) {
        super(element, str);
        if (m5917a() == 0) {
            throw new TransformationException("xml.WrongContent", new Object[]{"Transform", "Transforms"});
        }
    }

    private void m5915a(Transform transform) {
        if (f3841a.isDebugEnabled()) {
            f3841a.debug(new StringBuffer().append("Transforms.addTransform(").append(transform.m6042b()).append(")").toString());
        }
        this.k.appendChild(transform.m5720k());
        XMLUtils.m6171b(this.k);
    }

    static Class m5916b(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public int m5917a() {
        if (this.f3843b == null) {
            this.f3843b = XMLUtils.m6167a(this.k.getFirstChild(), "Transform");
        }
        return this.f3843b.length;
    }

    public XMLSignatureInput m5918a(XMLSignatureInput xMLSignatureInput, OutputStream outputStream) {
        try {
            int a = m5917a() - 1;
            int i = 0;
            XMLSignatureInput xMLSignatureInput2 = xMLSignatureInput;
            while (i < a) {
                Transform a2 = m5919a(i);
                if (f3841a.isDebugEnabled()) {
                    f3841a.debug(new StringBuffer().append("Perform the (").append(i).append(")th ").append(a2.m6042b()).append(" transform").toString());
                }
                i++;
                xMLSignatureInput2 = a2.m6040a(xMLSignatureInput2);
            }
            if (a >= 0) {
                xMLSignatureInput2 = m5919a(a).m6041a(xMLSignatureInput2, outputStream);
            }
            return xMLSignatureInput2;
        } catch (Exception e) {
            throw new TransformationException("empty", e);
        } catch (Exception e2) {
            throw new TransformationException("empty", e2);
        } catch (Exception e22) {
            throw new TransformationException("empty", e22);
        }
    }

    public Transform m5919a(int i) {
        try {
            if (this.f3843b == null) {
                this.f3843b = XMLUtils.m6167a(this.k.getFirstChild(), "Transform");
            }
            return new Transform(this.f3843b[i], this.l);
        } catch (Exception e) {
            throw new TransformationException("empty", e);
        }
    }

    public void m5920a(String str) {
        try {
            if (f3841a.isDebugEnabled()) {
                f3841a.debug(new StringBuffer().append("Transforms.addTransform(").append(str).append(")").toString());
            }
            m5915a(Transform.m6034a(this.m, str));
        } catch (Exception e) {
            throw new TransformationException("empty", e);
        }
    }

    public String m5921e() {
        return "Transforms";
    }
}
