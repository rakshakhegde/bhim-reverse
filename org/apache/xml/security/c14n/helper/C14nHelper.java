package org.apache.xml.security.c14n.helper;

import org.w3c.dom.Attr;

public class C14nHelper {
    private C14nHelper() {
    }

    public static boolean m5840a(String str) {
        return !m5842b(str);
    }

    public static boolean m5841a(Attr attr) {
        return !m5843b(attr);
    }

    public static boolean m5842b(String str) {
        return str.length() == 0 || str.indexOf(58) > 0;
    }

    public static boolean m5843b(Attr attr) {
        return m5842b(attr.getValue());
    }
}
