package org.apache.xml.security.c14n;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.w3c.dom.Node;

public class Canonicalizer {
    static boolean f3779a;
    static Map f3780b;
    protected CanonicalizerSpi f3781c;

    static {
        f3779a = false;
        f3780b = null;
    }

    private Canonicalizer(String str) {
        this.f3781c = null;
        try {
            this.f3781c = (CanonicalizerSpi) m5832b(str).newInstance();
            this.f3781c.f3782a = true;
        } catch (Exception e) {
            throw new InvalidCanonicalizerException("signature.Canonicalizer.UnknownCanonicalizer", new Object[]{str});
        }
    }

    public static final Canonicalizer m5829a(String str) {
        return new Canonicalizer(str);
    }

    public static void m5830a() {
        if (!f3779a) {
            f3780b = new HashMap(10);
            f3779a = true;
        }
    }

    public static void m5831a(String str, String str2) {
        if (m5832b(str) != null) {
            throw new AlgorithmAlreadyRegisteredException("algorithm.alreadyRegistered", new Object[]{str, m5832b(str)});
        }
        try {
            f3780b.put(str, Class.forName(str2));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("c14n class not found");
        }
    }

    private static Class m5832b(String str) {
        return (Class) f3780b.get(str);
    }

    public void m5833a(OutputStream outputStream) {
        this.f3781c.m5836a(outputStream);
    }

    public byte[] m5834a(Node node) {
        return this.f3781c.m5837a(node);
    }

    public byte[] m5835a(Node node, String str) {
        return this.f3781c.m5838a(node, str);
    }
}
