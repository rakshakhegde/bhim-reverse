package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer20010315 extends CanonicalizerBase {
    boolean f3822b;
    final SortedSet f3823c;
    XmlAttrStack f3824d;

    class XmlAttrStack {
        int f3818a;
        int f3819b;
        XmlsStackElement f3820c;
        List f3821d;

        class XmlsStackElement {
            int f3815a;
            boolean f3816b;
            List f3817c;

            XmlsStackElement() {
                this.f3816b = false;
                this.f3817c = new ArrayList();
            }
        }

        XmlAttrStack() {
            this.f3818a = 0;
            this.f3819b = 0;
            this.f3821d = new ArrayList();
        }

        void m5877a(int i) {
            this.f3818a = i;
            if (this.f3818a != -1) {
                this.f3820c = null;
                while (this.f3819b >= this.f3818a) {
                    this.f3821d.remove(this.f3821d.size() - 1);
                    if (this.f3821d.size() == 0) {
                        this.f3819b = 0;
                        return;
                    }
                    this.f3819b = ((XmlsStackElement) this.f3821d.get(this.f3821d.size() - 1)).f3815a;
                }
            }
        }

        void m5878a(Collection collection) {
            boolean z;
            int size = this.f3821d.size() - 1;
            if (this.f3820c == null) {
                this.f3820c = new XmlsStackElement();
                this.f3820c.f3815a = this.f3818a;
                this.f3819b = this.f3818a;
                this.f3821d.add(this.f3820c);
            }
            if (size == -1) {
                z = true;
            } else {
                XmlsStackElement xmlsStackElement = (XmlsStackElement) this.f3821d.get(size);
                z = xmlsStackElement.f3816b && xmlsStackElement.f3815a + 1 == this.f3818a;
            }
            if (z) {
                collection.addAll(this.f3820c.f3817c);
                this.f3820c.f3816b = true;
                return;
            }
            Map hashMap = new HashMap();
            while (size >= 0) {
                for (Attr attr : ((XmlsStackElement) this.f3821d.get(size)).f3817c) {
                    if (!hashMap.containsKey(attr.getName())) {
                        hashMap.put(attr.getName(), attr);
                    }
                }
                size--;
            }
            this.f3820c.f3816b = true;
            collection.addAll(hashMap.values());
        }

        void m5879a(Attr attr) {
            if (this.f3820c == null) {
                this.f3820c = new XmlsStackElement();
                this.f3820c.f3815a = this.f3818a;
                this.f3821d.add(this.f3820c);
                this.f3819b = this.f3818a;
            }
            this.f3820c.f3817c.add(attr);
        }
    }

    public Canonicalizer20010315(boolean z) {
        super(z);
        this.f3822b = true;
        this.f3823c = new TreeSet(g);
        this.f3824d = new XmlAttrStack();
    }

    Iterator m5880a(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        if (!element.hasAttributes() && !this.f3822b) {
            return null;
        }
        Collection collection = this.f3823c;
        collection.clear();
        NamedNodeMap attributes = element.getAttributes();
        int length = attributes.getLength();
        for (int i = 0; i < length; i++) {
            Attr attr = (Attr) attributes.item(i);
            if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                String localName = attr.getLocalName();
                String value = attr.getValue();
                if (!"xml".equals(localName) || !"http://www.w3.org/XML/1998/namespace".equals(value)) {
                    Node b = nameSpaceSymbTable.m5897b(localName, value, attr);
                    if (b != null) {
                        collection.add(b);
                        if (C14nHelper.m5841a(attr)) {
                            throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), localName, attr.getNodeValue()});
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                collection.add(attr);
            }
        }
        if (this.f3822b) {
            nameSpaceSymbTable.m5894a(collection);
            this.f3824d.m5878a(collection);
            this.f3822b = false;
        }
        return collection.iterator();
    }

    void m5881a(XMLSignatureInput xMLSignatureInput) {
        if (xMLSignatureInput.m6011a()) {
            XMLUtils.m6163a(xMLSignatureInput.m6028m() != null ? XMLUtils.m6169b(xMLSignatureInput.m6028m()) : XMLUtils.m6158a(xMLSignatureInput.m6012b()));
        }
    }

    public byte[] m5882a(Node node, String str) {
        throw new CanonicalizationException("c14n.Canonicalizer.UnsupportedOperation");
    }

    Iterator m5883b(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        NamedNodeMap namedNodeMap;
        int length;
        Object obj = null;
        this.f3824d.m5877a(nameSpaceSymbTable.m5905f());
        int i = m5852a((Node) element, nameSpaceSymbTable.m5905f()) == 1 ? 1 : 0;
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            namedNodeMap = attributes;
            length = attributes.getLength();
        } else {
            length = 0;
            namedNodeMap = null;
        }
        Collection collection = this.f3823c;
        collection.clear();
        for (int i2 = 0; i2 < length; i2++) {
            Attr attr = (Attr) namedNodeMap.item(i2);
            String namespaceURI = attr.getNamespaceURI();
            if ("http://www.w3.org/2000/xmlns/".equals(namespaceURI)) {
                namespaceURI = attr.getLocalName();
                String value = attr.getValue();
                if (!"xml".equals(namespaceURI) || !"http://www.w3.org/XML/1998/namespace".equals(value)) {
                    if (m5865c(attr)) {
                        if (i != 0 || !nameSpaceSymbTable.m5904e(namespaceURI)) {
                            Node b = nameSpaceSymbTable.m5897b(namespaceURI, value, attr);
                            if (b != null) {
                                collection.add(b);
                                if (C14nHelper.m5841a(attr)) {
                                    throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), namespaceURI, attr.getNodeValue()});
                                }
                            } else {
                                continue;
                            }
                        }
                    } else if (i == 0 || "xmlns".equals(namespaceURI)) {
                        nameSpaceSymbTable.m5895a(namespaceURI, value, attr);
                    } else {
                        nameSpaceSymbTable.m5900c(namespaceURI);
                    }
                }
            } else if ("http://www.w3.org/XML/1998/namespace".equals(namespaceURI)) {
                this.f3824d.m5879a(attr);
            } else if (i != 0) {
                collection.add(attr);
            }
        }
        if (i != 0) {
            Node attributeNodeNS = element.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", "xmlns");
            if (attributeNodeNS == null) {
                obj = nameSpaceSymbTable.m5892a("xmlns");
            } else if (!m5865c(attributeNodeNS)) {
                obj = nameSpaceSymbTable.m5897b("xmlns", BuildConfig.FLAVOR, i);
            }
            if (obj != null) {
                collection.add(obj);
            }
            this.f3824d.m5878a(collection);
            nameSpaceSymbTable.m5894a(collection);
        }
        return collection.iterator();
    }

    void m5884c(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        if (element.hasAttributes()) {
            this.f3824d.m5877a(-1);
            NamedNodeMap attributes = element.getAttributes();
            int length = attributes.getLength();
            for (int i = 0; i < length; i++) {
                Attr attr = (Attr) attributes.item(i);
                if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                    String localName = attr.getLocalName();
                    String nodeValue = attr.getNodeValue();
                    if (!"xml".equals(localName) || !"http://www.w3.org/XML/1998/namespace".equals(nodeValue)) {
                        nameSpaceSymbTable.m5895a(localName, nodeValue, attr);
                    }
                } else if ("http://www.w3.org/XML/1998/namespace".equals(attr.getNamespaceURI())) {
                    this.f3824d.m5879a(attr);
                }
            }
        }
    }
}
