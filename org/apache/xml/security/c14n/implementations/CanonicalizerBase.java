package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.CanonicalizerSpi;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.UnsyncByteArrayOutputStream;
import org.apache.xml.security.utils.XMLUtils;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;

public abstract class CanonicalizerBase extends CanonicalizerSpi {
    private static final byte[] f3790b;
    private static final byte[] f3791c;
    private static final byte[] f3792d;
    private static final byte[] f3793e;
    private static final byte[] f3794f;
    static final AttrCompare f3795g;
    static final byte[] f3796h;
    protected static final Attr f3797i;
    private static final byte[] f3798o;
    private static final byte[] f3799p;
    private static final byte[] f3800q;
    private static final byte[] f3801r;
    private static final byte[] f3802s;
    private static final byte[] f3803t;
    private static final byte[] f3804u;
    List f3805j;
    boolean f3806k;
    Set f3807l;
    Node f3808m;
    OutputStream f3809n;

    static {
        f3790b = new byte[]{(byte) 63, (byte) 62};
        f3791c = new byte[]{(byte) 60, (byte) 63};
        f3792d = new byte[]{(byte) 45, (byte) 45, (byte) 62};
        f3793e = new byte[]{(byte) 60, (byte) 33, (byte) 45, (byte) 45};
        f3794f = new byte[]{(byte) 38, (byte) 35, (byte) 120, (byte) 65, (byte) 59};
        f3798o = new byte[]{(byte) 38, (byte) 35, (byte) 120, (byte) 57, (byte) 59};
        f3799p = new byte[]{(byte) 38, (byte) 113, (byte) 117, (byte) 111, (byte) 116, (byte) 59};
        f3800q = new byte[]{(byte) 38, (byte) 35, (byte) 120, (byte) 68, (byte) 59};
        f3801r = new byte[]{(byte) 38, (byte) 103, (byte) 116, (byte) 59};
        f3802s = new byte[]{(byte) 38, (byte) 108, (byte) 116, (byte) 59};
        f3803t = new byte[]{(byte) 60, (byte) 47};
        f3804u = new byte[]{(byte) 38, (byte) 97, (byte) 109, (byte) 112, (byte) 59};
        f3795g = new AttrCompare();
        f3796h = new byte[]{(byte) 61, (byte) 34};
        try {
            f3797i = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument().createAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns");
            f3797i.setValue(BuildConfig.FLAVOR);
        } catch (Exception e) {
            throw new RuntimeException(new StringBuffer().append("Unable to create nullNode").append(e).toString());
        }
    }

    public CanonicalizerBase(boolean z) {
        this.f3807l = null;
        this.f3808m = null;
        this.f3809n = new UnsyncByteArrayOutputStream();
        this.f3806k = z;
    }

    static final void m5847a(String str, OutputStream outputStream) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            byte[] bArr;
            char charAt = str.charAt(i);
            switch (charAt) {
                case R.Toolbar_titleMarginStart /*13*/:
                    bArr = f3800q;
                    break;
                case R.AppCompatTheme_actionModeWebSearchDrawable /*38*/:
                    bArr = f3804u;
                    break;
                case R.AppCompatTheme_popupMenuStyle /*60*/:
                    bArr = f3802s;
                    break;
                case R.AppCompatTheme_editTextColor /*62*/:
                    bArr = f3801r;
                    break;
                default:
                    if (charAt >= '\u0080') {
                        UtfHelpper.m5911a(charAt, outputStream);
                        break;
                    } else {
                        outputStream.write(charAt);
                        continue;
                    }
            }
            outputStream.write(bArr);
        }
    }

    static final void m5848a(String str, String str2, OutputStream outputStream, Map map) {
        outputStream.write(32);
        UtfHelpper.m5913a(str, outputStream, map);
        outputStream.write(f3796h);
        int length = str2.length();
        int i = 0;
        while (i < length) {
            byte[] bArr;
            int i2 = i + 1;
            char charAt = str2.charAt(i);
            switch (charAt) {
                case R.Toolbar_popupTheme /*9*/:
                    bArr = f3798o;
                    break;
                case R.Toolbar_titleTextAppearance /*10*/:
                    bArr = f3794f;
                    break;
                case R.Toolbar_titleMarginStart /*13*/:
                    bArr = f3800q;
                    break;
                case R.AppCompatTheme_actionModePasteDrawable /*34*/:
                    bArr = f3799p;
                    break;
                case R.AppCompatTheme_actionModeWebSearchDrawable /*38*/:
                    bArr = f3804u;
                    break;
                case R.AppCompatTheme_popupMenuStyle /*60*/:
                    bArr = f3802s;
                    break;
                default:
                    if (charAt >= '\u0080') {
                        UtfHelpper.m5911a(charAt, outputStream);
                        i = i2;
                        break;
                    }
                    outputStream.write(charAt);
                    i = i2;
                    continue;
            }
            outputStream.write(bArr);
            i = i2;
        }
        outputStream.write(34);
    }

    static final void m5849a(Comment comment, OutputStream outputStream, int i) {
        if (i == 1) {
            outputStream.write(10);
        }
        outputStream.write(f3793e);
        String data = comment.getData();
        int length = data.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = data.charAt(i2);
            if (charAt == '\r') {
                outputStream.write(f3800q);
            } else if (charAt < '\u0080') {
                outputStream.write(charAt);
            } else {
                UtfHelpper.m5911a(charAt, outputStream);
            }
        }
        outputStream.write(f3792d);
        if (i == -1) {
            outputStream.write(10);
        }
    }

    static final void m5850a(ProcessingInstruction processingInstruction, OutputStream outputStream, int i) {
        int i2 = 0;
        if (i == 1) {
            outputStream.write(10);
        }
        outputStream.write(f3791c);
        String target = processingInstruction.getTarget();
        int length = target.length();
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = target.charAt(i3);
            if (charAt == '\r') {
                outputStream.write(f3800q);
            } else if (charAt < '\u0080') {
                outputStream.write(charAt);
            } else {
                UtfHelpper.m5911a(charAt, outputStream);
            }
        }
        String data = processingInstruction.getData();
        int length2 = data.length();
        if (length2 > 0) {
            outputStream.write(32);
            while (i2 < length2) {
                char charAt2 = data.charAt(i2);
                if (charAt2 == '\r') {
                    outputStream.write(f3800q);
                } else {
                    UtfHelpper.m5911a(charAt2, outputStream);
                }
                i2++;
            }
        }
        outputStream.write(f3790b);
        if (i == -1) {
            outputStream.write(10);
        }
    }

    private byte[] m5851d(Node node) {
        try {
            m5862b(node, node);
            this.f3809n.close();
            byte[] toByteArray;
            if (this.f3809n instanceof ByteArrayOutputStream) {
                toByteArray = ((ByteArrayOutputStream) this.f3809n).toByteArray();
                if (this.a) {
                    ((ByteArrayOutputStream) this.f3809n).reset();
                }
                return toByteArray;
            } else if (!(this.f3809n instanceof UnsyncByteArrayOutputStream)) {
                return null;
            } else {
                toByteArray = ((UnsyncByteArrayOutputStream) this.f3809n).m6155a();
                if (this.a) {
                    ((UnsyncByteArrayOutputStream) this.f3809n).m6156b();
                }
                return toByteArray;
            }
        } catch (Exception e) {
            throw new CanonicalizationException("empty", e);
        } catch (Exception e2) {
            throw new CanonicalizationException("empty", e2);
        }
    }

    int m5852a(Node node, int i) {
        if (this.f3805j != null) {
            for (NodeFilter a : this.f3805j) {
                int a2 = a.m5975a(node, i);
                if (a2 != 1) {
                    return a2;
                }
            }
        }
        return (this.f3807l == null || this.f3807l.contains(node)) ? 1 : 0;
    }

    abstract Iterator m5853a(Element element, NameSpaceSymbTable nameSpaceSymbTable);

    public void m5854a(OutputStream outputStream) {
        this.f3809n = outputStream;
    }

    abstract void m5855a(XMLSignatureInput xMLSignatureInput);

    final void m5856a(Node node, NameSpaceSymbTable nameSpaceSymbTable, Node node2, int i) {
        if (m5860b(node) != -1) {
            OutputStream outputStream = this.f3809n;
            Node node3 = this.f3808m;
            boolean z = this.f3806k;
            Map hashMap = new HashMap();
            Node node4 = null;
            Node node5 = null;
            Node node6 = node;
            while (true) {
                switch (node6.getNodeType()) {
                    case R.View_android_focusable /*1*/:
                        i = 0;
                        if (node6 != node3) {
                            Element element = (Element) node6;
                            nameSpaceSymbTable.m5893a();
                            outputStream.write(60);
                            String tagName = element.getTagName();
                            UtfHelpper.m5913a(tagName, outputStream, hashMap);
                            Iterator a = m5853a(element, nameSpaceSymbTable);
                            if (a != null) {
                                while (a.hasNext()) {
                                    Attr attr = (Attr) a.next();
                                    m5848a(attr.getNodeName(), attr.getNodeValue(), outputStream, hashMap);
                                }
                            }
                            outputStream.write(62);
                            node = node6.getFirstChild();
                            if (node == null) {
                                outputStream.write(f3803t);
                                UtfHelpper.m5912a(tagName, outputStream);
                                outputStream.write(62);
                                nameSpaceSymbTable.m5898b();
                                if (node4 == null) {
                                    node5 = node4;
                                    break;
                                }
                                node = node6.getNextSibling();
                                node5 = node4;
                                break;
                            }
                        }
                        node = node5;
                        node5 = node4;
                        break;
                        break;
                    case R.View_paddingStart /*2*/:
                    case R.Toolbar_contentInsetEnd /*6*/:
                    case R.Toolbar_titleMargins /*12*/:
                        throw new CanonicalizationException("empty");
                    case R.View_paddingEnd /*3*/:
                    case R.View_theme /*4*/:
                        m5847a(node6.getNodeValue(), outputStream);
                        node = node5;
                        node5 = node4;
                        break;
                    case R.Toolbar_contentInsetLeft /*7*/:
                        m5850a((ProcessingInstruction) node6, outputStream, i);
                        node = node5;
                        node5 = node4;
                        break;
                    case R.Toolbar_contentInsetRight /*8*/:
                        if (!z) {
                            node = node5;
                            node5 = node4;
                            break;
                        }
                        m5849a((Comment) node6, outputStream, i);
                        node = node5;
                        node5 = node4;
                        break;
                    case R.Toolbar_popupTheme /*9*/:
                    case R.Toolbar_subtitleTextAppearance /*11*/:
                        nameSpaceSymbTable.m5893a();
                        node = node6.getFirstChild();
                        node5 = node4;
                        break;
                    default:
                        node = node5;
                        node5 = node4;
                        break;
                }
                while (node == null && node5 != null) {
                    outputStream.write(f3803t);
                    UtfHelpper.m5913a(((Element) node5).getTagName(), outputStream, hashMap);
                    outputStream.write(62);
                    nameSpaceSymbTable.m5898b();
                    if (node5 != node2) {
                        node = node5.getNextSibling();
                        node4 = node5.getParentNode();
                        if (node4 == null || (short) 1 != node4.getNodeType()) {
                            i = 1;
                            node5 = null;
                        } else {
                            node5 = node4;
                        }
                    } else {
                        return;
                    }
                }
                if (node != null) {
                    node4 = node5;
                    node5 = node.getNextSibling();
                    node6 = node;
                } else {
                    return;
                }
            }
        }
    }

    public byte[] m5857a(Set set) {
        this.f3807l = set;
        return m5851d(XMLUtils.m6158a(this.f3807l));
    }

    public byte[] m5858a(Node node) {
        return m5859a(node, (Node) null);
    }

    byte[] m5859a(Node node, Node node2) {
        this.f3808m = node2;
        try {
            NameSpaceSymbTable nameSpaceSymbTable = new NameSpaceSymbTable();
            int i = -1;
            if (node != null && (short) 1 == node.getNodeType()) {
                m5866d((Element) node, nameSpaceSymbTable);
                i = 0;
            }
            m5856a(node, nameSpaceSymbTable, node, i);
            this.f3809n.close();
            byte[] toByteArray;
            if (this.f3809n instanceof ByteArrayOutputStream) {
                toByteArray = ((ByteArrayOutputStream) this.f3809n).toByteArray();
                if (this.a) {
                    ((ByteArrayOutputStream) this.f3809n).reset();
                }
                return toByteArray;
            } else if (!(this.f3809n instanceof UnsyncByteArrayOutputStream)) {
                return null;
            } else {
                toByteArray = ((UnsyncByteArrayOutputStream) this.f3809n).m6155a();
                if (this.a) {
                    ((UnsyncByteArrayOutputStream) this.f3809n).m6156b();
                }
                return toByteArray;
            }
        } catch (Exception e) {
            throw new CanonicalizationException("empty", e);
        } catch (Exception e2) {
            throw new CanonicalizationException("empty", e2);
        }
    }

    int m5860b(Node node) {
        if (this.f3805j != null) {
            for (NodeFilter a : this.f3805j) {
                int a2 = a.m5974a(node);
                if (a2 != 1) {
                    return a2;
                }
            }
        }
        return (this.f3807l == null || this.f3807l.contains(node)) ? 1 : 0;
    }

    abstract Iterator m5861b(Element element, NameSpaceSymbTable nameSpaceSymbTable);

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final void m5862b(org.w3c.dom.Node r13, org.w3c.dom.Node r14) {
        /*
        r12 = this;
        r0 = r12.m5860b(r13);
        r1 = -1;
        if (r0 != r1) goto L_0x0008;
    L_0x0007:
        return;
    L_0x0008:
        r7 = new org.apache.xml.security.c14n.implementations.NameSpaceSymbTable;
        r7.<init>();
        if (r13 == 0) goto L_0x001c;
    L_0x000f:
        r0 = 1;
        r1 = r13.getNodeType();
        if (r0 != r1) goto L_0x001c;
    L_0x0016:
        r0 = r13;
        r0 = (org.w3c.dom.Element) r0;
        r12.m5866d(r0, r7);
    L_0x001c:
        r2 = 0;
        r1 = 0;
        r8 = r12.f3809n;
        r0 = -1;
        r9 = new java.util.HashMap;
        r9.<init>();
        r3 = r1;
        r1 = r0;
        r0 = r13;
    L_0x0029:
        r4 = r0.getNodeType();
        switch(r4) {
            case 1: goto L_0x00d7;
            case 2: goto L_0x006c;
            case 3: goto L_0x00a6;
            case 4: goto L_0x00a6;
            case 5: goto L_0x0030;
            case 6: goto L_0x006c;
            case 7: goto L_0x0097;
            case 8: goto L_0x007f;
            case 9: goto L_0x0074;
            case 10: goto L_0x0030;
            case 11: goto L_0x0074;
            case 12: goto L_0x006c;
            default: goto L_0x0030;
        };
    L_0x0030:
        r13 = r2;
        r2 = r1;
        r1 = r3;
    L_0x0033:
        if (r13 != 0) goto L_0x0164;
    L_0x0035:
        if (r1 == 0) goto L_0x0164;
    L_0x0037:
        r0 = r12.m5865c(r1);
        if (r0 == 0) goto L_0x015f;
    L_0x003d:
        r0 = f3803t;
        r8.write(r0);
        r0 = r1;
        r0 = (org.w3c.dom.Element) r0;
        r0 = r0.getTagName();
        org.apache.xml.security.c14n.implementations.UtfHelpper.m5913a(r0, r8, r9);
        r0 = 62;
        r8.write(r0);
        r7.m5898b();
    L_0x0054:
        if (r1 == r14) goto L_0x0007;
    L_0x0056:
        r13 = r1.getNextSibling();
        r3 = r1.getParentNode();
        if (r3 == 0) goto L_0x0067;
    L_0x0060:
        r0 = 1;
        r1 = r3.getNodeType();
        if (r0 == r1) goto L_0x017a;
    L_0x0067:
        r3 = 0;
        r1 = 1;
        r2 = r1;
        r1 = r3;
        goto L_0x0033;
    L_0x006c:
        r0 = new org.apache.xml.security.c14n.CanonicalizationException;
        r1 = "empty";
        r0.<init>(r1);
        throw r0;
    L_0x0074:
        r7.m5893a();
        r2 = r0.getFirstChild();
        r13 = r2;
        r2 = r1;
        r1 = r3;
        goto L_0x0033;
    L_0x007f:
        r4 = r12.f3806k;
        if (r4 == 0) goto L_0x0175;
    L_0x0083:
        r4 = r7.m5905f();
        r4 = r12.m5852a(r0, r4);
        r5 = 1;
        if (r4 != r5) goto L_0x0175;
    L_0x008e:
        r0 = (org.w3c.dom.Comment) r0;
        m5849a(r0, r8, r1);
        r13 = r2;
        r2 = r1;
        r1 = r3;
        goto L_0x0033;
    L_0x0097:
        r4 = r12.m5865c(r0);
        if (r4 == 0) goto L_0x0175;
    L_0x009d:
        r0 = (org.w3c.dom.ProcessingInstruction) r0;
        m5850a(r0, r8, r1);
        r13 = r2;
        r2 = r1;
        r1 = r3;
        goto L_0x0033;
    L_0x00a6:
        r4 = r12.m5865c(r0);
        if (r4 == 0) goto L_0x0175;
    L_0x00ac:
        r4 = r0.getNodeValue();
        m5847a(r4, r8);
        r0 = r0.getNextSibling();
    L_0x00b7:
        if (r0 == 0) goto L_0x0175;
    L_0x00b9:
        r4 = r0.getNodeType();
        r5 = 3;
        if (r4 == r5) goto L_0x00c7;
    L_0x00c0:
        r4 = r0.getNodeType();
        r5 = 4;
        if (r4 != r5) goto L_0x0175;
    L_0x00c7:
        r2 = r0.getNodeValue();
        m5847a(r2, r8);
        r2 = r0.getNextSibling();
        r0 = r0.getNextSibling();
        goto L_0x00b7;
    L_0x00d7:
        r6 = 0;
        r1 = r0;
        r1 = (org.w3c.dom.Element) r1;
        r2 = 0;
        r4 = r7.m5905f();
        r4 = r12.m5852a(r0, r4);
        r5 = -1;
        if (r4 != r5) goto L_0x00f0;
    L_0x00e7:
        r2 = r0.getNextSibling();
        r1 = r3;
        r13 = r2;
        r2 = r6;
        goto L_0x0033;
    L_0x00f0:
        r5 = 1;
        if (r4 != r5) goto L_0x0125;
    L_0x00f3:
        r4 = 1;
        r5 = r4;
    L_0x00f5:
        if (r5 == 0) goto L_0x0128;
    L_0x00f7:
        r7.m5893a();
        r2 = 60;
        r8.write(r2);
        r2 = r1.getTagName();
        org.apache.xml.security.c14n.implementations.UtfHelpper.m5913a(r2, r8, r9);
        r4 = r2;
    L_0x0107:
        r10 = r12.m5861b(r1, r7);
        if (r10 == 0) goto L_0x012d;
    L_0x010d:
        r2 = r10.hasNext();
        if (r2 == 0) goto L_0x012d;
    L_0x0113:
        r2 = r10.next();
        r2 = (org.w3c.dom.Attr) r2;
        r11 = r2.getNodeName();
        r2 = r2.getNodeValue();
        m5848a(r11, r2, r8, r9);
        goto L_0x010d;
    L_0x0125:
        r4 = 0;
        r5 = r4;
        goto L_0x00f5;
    L_0x0128:
        r7.m5899c();
        r4 = r2;
        goto L_0x0107;
    L_0x012d:
        if (r5 == 0) goto L_0x0134;
    L_0x012f:
        r2 = 62;
        r8.write(r2);
    L_0x0134:
        r2 = r0.getFirstChild();
        if (r2 != 0) goto L_0x015b;
    L_0x013a:
        if (r5 == 0) goto L_0x0157;
    L_0x013c:
        r1 = f3803t;
        r8.write(r1);
        org.apache.xml.security.c14n.implementations.UtfHelpper.m5913a(r4, r8, r9);
        r1 = 62;
        r8.write(r1);
        r7.m5898b();
    L_0x014c:
        if (r3 == 0) goto L_0x0170;
    L_0x014e:
        r2 = r0.getNextSibling();
        r1 = r3;
        r13 = r2;
        r2 = r6;
        goto L_0x0033;
    L_0x0157:
        r7.m5901d();
        goto L_0x014c;
    L_0x015b:
        r13 = r2;
        r2 = r6;
        goto L_0x0033;
    L_0x015f:
        r7.m5901d();
        goto L_0x0054;
    L_0x0164:
        if (r13 == 0) goto L_0x0007;
    L_0x0166:
        r0 = r13.getNextSibling();
        r3 = r1;
        r1 = r2;
        r2 = r0;
        r0 = r13;
        goto L_0x0029;
    L_0x0170:
        r1 = r3;
        r13 = r2;
        r2 = r6;
        goto L_0x0033;
    L_0x0175:
        r13 = r2;
        r2 = r1;
        r1 = r3;
        goto L_0x0033;
    L_0x017a:
        r1 = r3;
        goto L_0x0033;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.xml.security.c14n.implementations.CanonicalizerBase.b(org.w3c.dom.Node, org.w3c.dom.Node):void");
    }

    public byte[] m5863b(XMLSignatureInput xMLSignatureInput) {
        try {
            if (xMLSignatureInput.m6029n()) {
                this.f3806k = false;
            }
            if (xMLSignatureInput.m6023h()) {
                return m5839a(xMLSignatureInput.m6020e());
            }
            if (xMLSignatureInput.m6022g()) {
                return m5859a(xMLSignatureInput.m6028m(), xMLSignatureInput.m6027l());
            }
            if (!xMLSignatureInput.m6021f()) {
                return null;
            }
            this.f3805j = xMLSignatureInput.m6031p();
            m5855a(xMLSignatureInput);
            return xMLSignatureInput.m6028m() != null ? m5851d(xMLSignatureInput.m6028m()) : m5857a(xMLSignatureInput.m6012b());
        } catch (Exception e) {
            throw new CanonicalizationException("empty", e);
        } catch (Exception e2) {
            throw new CanonicalizationException("empty", e2);
        } catch (Exception e22) {
            throw new CanonicalizationException("empty", e22);
        } catch (Exception e222) {
            throw new CanonicalizationException("empty", e222);
        }
    }

    void m5864c(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            int length = attributes.getLength();
            for (int i = 0; i < length; i++) {
                Attr attr = (Attr) attributes.item(i);
                if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                    String localName = attr.getLocalName();
                    String nodeValue = attr.getNodeValue();
                    if (!"xml".equals(localName) || !"http://www.w3.org/XML/1998/namespace".equals(nodeValue)) {
                        nameSpaceSymbTable.m5895a(localName, nodeValue, attr);
                    }
                }
            }
        }
    }

    boolean m5865c(Node node) {
        if (this.f3805j != null) {
            for (NodeFilter a : this.f3805j) {
                if (a.m5974a(node) != 1) {
                    return false;
                }
            }
        }
        return this.f3807l == null || this.f3807l.contains(node);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final void m5866d(org.w3c.dom.Element r5, org.apache.xml.security.c14n.implementations.NameSpaceSymbTable r6) {
        /*
        r4 = this;
        r3 = 1;
        r1 = new java.util.ArrayList;
        r0 = 10;
        r1.<init>(r0);
        r0 = r5.getParentNode();
        if (r0 == 0) goto L_0x0014;
    L_0x000e:
        r2 = r0.getNodeType();
        if (r3 == r2) goto L_0x0015;
    L_0x0014:
        return;
    L_0x0015:
        r0 = (org.w3c.dom.Element) r0;
    L_0x0017:
        if (r0 == 0) goto L_0x0028;
    L_0x0019:
        r1.add(r0);
        r0 = r0.getParentNode();
        if (r0 == 0) goto L_0x0028;
    L_0x0022:
        r2 = r0.getNodeType();
        if (r3 == r2) goto L_0x0040;
    L_0x0028:
        r0 = r1.size();
        r1 = r1.listIterator(r0);
    L_0x0030:
        r0 = r1.hasPrevious();
        if (r0 == 0) goto L_0x0043;
    L_0x0036:
        r0 = r1.previous();
        r0 = (org.w3c.dom.Element) r0;
        r4.m5864c(r0, r6);
        goto L_0x0030;
    L_0x0040:
        r0 = (org.w3c.dom.Element) r0;
        goto L_0x0017;
    L_0x0043:
        r0 = "xmlns";
        r0 = r6.m5896b(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x004b:
        r1 = "";
        r0 = r0.getValue();
        r0 = r1.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0057:
        r0 = "xmlns";
        r1 = "";
        r2 = f3797i;
        r6.m5897b(r0, r1, r2);
        goto L_0x0014;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.xml.security.c14n.implementations.CanonicalizerBase.d(org.w3c.dom.Element, org.apache.xml.security.c14n.implementations.NameSpaceSymbTable):void");
    }
}
