package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

public class NameSpaceSymbTable {
    static final SymbMap f3833e;
    SymbMap f3834a;
    int f3835b;
    List f3836c;
    boolean f3837d;

    static {
        f3833e = new SymbMap();
        NameSpaceSymbEntry nameSpaceSymbEntry = new NameSpaceSymbEntry(BuildConfig.FLAVOR, null, true, "xmlns");
        nameSpaceSymbEntry.f3830d = BuildConfig.FLAVOR;
        f3833e.m5910a("xmlns", nameSpaceSymbEntry);
    }

    public NameSpaceSymbTable() {
        this.f3835b = 0;
        this.f3837d = true;
        this.f3836c = new ArrayList(10);
        this.f3834a = (SymbMap) f3833e.clone();
    }

    public Attr m5892a(String str) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        if (a == null || a.f3831e) {
            return null;
        }
        NameSpaceSymbEntry nameSpaceSymbEntry = (NameSpaceSymbEntry) a.clone();
        m5903e();
        this.f3834a.m5910a(str, nameSpaceSymbEntry);
        nameSpaceSymbEntry.f3831e = true;
        nameSpaceSymbEntry.f3827a = this.f3835b;
        nameSpaceSymbEntry.f3830d = nameSpaceSymbEntry.f3829c;
        return nameSpaceSymbEntry.f3832f;
    }

    public void m5893a() {
        this.f3835b++;
        m5899c();
    }

    public void m5894a(Collection collection) {
        for (NameSpaceSymbEntry nameSpaceSymbEntry : this.f3834a.m5907a()) {
            NameSpaceSymbEntry nameSpaceSymbEntry2;
            if (!(nameSpaceSymbEntry2.f3831e || nameSpaceSymbEntry2.f3832f == null)) {
                nameSpaceSymbEntry2 = (NameSpaceSymbEntry) nameSpaceSymbEntry2.clone();
                m5903e();
                this.f3834a.m5910a(nameSpaceSymbEntry2.f3828b, nameSpaceSymbEntry2);
                nameSpaceSymbEntry2.f3830d = nameSpaceSymbEntry2.f3829c;
                nameSpaceSymbEntry2.f3831e = true;
                collection.add(nameSpaceSymbEntry2.f3832f);
            }
        }
    }

    public boolean m5895a(String str, String str2, Attr attr) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        if (a != null && str2.equals(a.f3829c)) {
            return false;
        }
        NameSpaceSymbEntry nameSpaceSymbEntry = new NameSpaceSymbEntry(str2, attr, false, str);
        m5903e();
        this.f3834a.m5910a(str, nameSpaceSymbEntry);
        if (a != null) {
            nameSpaceSymbEntry.f3830d = a.f3830d;
            if (a.f3830d != null && a.f3830d.equals(str2)) {
                nameSpaceSymbEntry.f3831e = true;
            }
        }
        return true;
    }

    public Attr m5896b(String str) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        return (a == null || a.f3831e) ? null : a.f3832f;
    }

    public Node m5897b(String str, String str2, Attr attr) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        if (a == null || !str2.equals(a.f3829c)) {
            NameSpaceSymbEntry nameSpaceSymbEntry = new NameSpaceSymbEntry(str2, attr, true, str);
            nameSpaceSymbEntry.f3830d = str2;
            m5903e();
            this.f3834a.m5910a(str, nameSpaceSymbEntry);
            if (a == null || a.f3830d == null || !a.f3830d.equals(str2)) {
                return nameSpaceSymbEntry.f3832f;
            }
            nameSpaceSymbEntry.f3831e = true;
            return null;
        } else if (a.f3831e) {
            return null;
        } else {
            NameSpaceSymbEntry nameSpaceSymbEntry2 = (NameSpaceSymbEntry) a.clone();
            m5903e();
            this.f3834a.m5910a(str, nameSpaceSymbEntry2);
            nameSpaceSymbEntry2.f3830d = str2;
            nameSpaceSymbEntry2.f3831e = true;
            return nameSpaceSymbEntry2.f3832f;
        }
    }

    public void m5898b() {
        this.f3835b--;
        m5901d();
    }

    public void m5899c() {
        this.f3836c.add(null);
        this.f3837d = false;
    }

    public void m5900c(String str) {
        if (this.f3834a.m5908a(str) != null) {
            m5903e();
            this.f3834a.m5910a(str, null);
        }
    }

    public void m5901d() {
        int size = this.f3836c.size() - 1;
        Object remove = this.f3836c.remove(size);
        if (remove != null) {
            this.f3834a = (SymbMap) remove;
            if (size == 0) {
                this.f3837d = false;
                return;
            } else {
                this.f3837d = this.f3836c.get(size + -1) != this.f3834a;
                return;
            }
        }
        this.f3837d = false;
    }

    public void m5902d(String str) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        if (a != null && !a.f3831e) {
            m5903e();
            this.f3834a.m5910a(str, null);
        }
    }

    final void m5903e() {
        if (!this.f3837d) {
            this.f3836c.set(this.f3836c.size() - 1, this.f3834a);
            this.f3834a = (SymbMap) this.f3834a.clone();
            this.f3837d = true;
        }
    }

    public boolean m5904e(String str) {
        NameSpaceSymbEntry a = this.f3834a.m5908a(str);
        if (a != null && a.f3831e) {
            m5903e();
            this.f3834a.m5910a(str, null);
        }
        return false;
    }

    public int m5905f() {
        return this.f3836c.size();
    }
}
