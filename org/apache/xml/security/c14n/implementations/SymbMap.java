package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.List;

class SymbMap implements Cloneable {
    int f3838a;
    NameSpaceSymbEntry[] f3839b;
    String[] f3840c;

    SymbMap() {
        this.f3838a = 23;
        this.f3839b = new NameSpaceSymbEntry[this.f3838a];
        this.f3840c = new String[this.f3838a];
    }

    protected int m5906a(Object obj) {
        String[] strArr = this.f3840c;
        int length = strArr.length;
        int hashCode = (obj.hashCode() & Integer.MAX_VALUE) % length;
        Object obj2 = strArr[hashCode];
        if (!(obj2 == null || obj2.equals(obj))) {
            length--;
            do {
                hashCode = hashCode == length ? 0 : hashCode + 1;
                obj2 = strArr[hashCode];
                if (obj2 == null) {
                    break;
                }
            } while (!obj2.equals(obj));
        }
        return hashCode;
    }

    List m5907a() {
        List arrayList = new ArrayList();
        int i = 0;
        while (i < this.f3839b.length) {
            if (!(this.f3839b[i] == null || BuildConfig.FLAVOR.equals(this.f3839b[i].f3829c))) {
                arrayList.add(this.f3839b[i]);
            }
            i++;
        }
        return arrayList;
    }

    NameSpaceSymbEntry m5908a(String str) {
        return this.f3839b[m5906a((Object) str)];
    }

    protected void m5909a(int i) {
        int length = this.f3840c.length;
        String[] strArr = this.f3840c;
        NameSpaceSymbEntry[] nameSpaceSymbEntryArr = this.f3839b;
        this.f3840c = new String[i];
        this.f3839b = new NameSpaceSymbEntry[i];
        while (true) {
            int i2 = length - 1;
            if (length <= 0) {
                return;
            }
            if (strArr[i2] != null) {
                Object obj = strArr[i2];
                int a = m5906a(obj);
                this.f3840c[a] = obj;
                this.f3839b[a] = nameSpaceSymbEntryArr[i2];
                length = i2;
            } else {
                length = i2;
            }
        }
    }

    void m5910a(String str, NameSpaceSymbEntry nameSpaceSymbEntry) {
        int a = m5906a((Object) str);
        Object obj = this.f3840c[a];
        this.f3840c[a] = str;
        this.f3839b[a] = nameSpaceSymbEntry;
        if (obj == null || !obj.equals(str)) {
            a = this.f3838a - 1;
            this.f3838a = a;
            if (a == 0) {
                this.f3838a = this.f3839b.length;
                m5909a(this.f3838a << 2);
            }
        }
    }

    protected Object clone() {
        try {
            SymbMap symbMap = (SymbMap) super.clone();
            symbMap.f3839b = new NameSpaceSymbEntry[this.f3839b.length];
            System.arraycopy(this.f3839b, 0, symbMap.f3839b, 0, this.f3839b.length);
            symbMap.f3840c = new String[this.f3840c.length];
            System.arraycopy(this.f3840c, 0, symbMap.f3840c, 0, this.f3840c.length);
            return symbMap;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
