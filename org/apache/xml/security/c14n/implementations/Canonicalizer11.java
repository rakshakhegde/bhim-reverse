package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer11 extends CanonicalizerBase {
    static Log f3810d;
    static Class f3811f;
    boolean f3812b;
    final SortedSet f3813c;
    XmlAttrStack f3814e;

    class XmlAttrStack {
        int f3786a;
        int f3787b;
        XmlsStackElement f3788c;
        List f3789d;

        class XmlsStackElement {
            int f3783a;
            boolean f3784b;
            List f3785c;

            XmlsStackElement() {
                this.f3784b = false;
                this.f3785c = new ArrayList();
            }
        }

        XmlAttrStack() {
            this.f3786a = 0;
            this.f3787b = 0;
            this.f3789d = new ArrayList();
        }

        void m5844a(int i) {
            this.f3786a = i;
            if (this.f3786a != -1) {
                this.f3788c = null;
                while (this.f3787b >= this.f3786a) {
                    this.f3789d.remove(this.f3789d.size() - 1);
                    if (this.f3789d.size() == 0) {
                        this.f3787b = 0;
                        return;
                    }
                    this.f3787b = ((XmlsStackElement) this.f3789d.get(this.f3789d.size() - 1)).f3783a;
                }
            }
        }

        void m5845a(Collection collection) {
            boolean z;
            String str = null;
            if (this.f3788c == null) {
                this.f3788c = new XmlsStackElement();
                this.f3788c.f3783a = this.f3786a;
                this.f3787b = this.f3786a;
                this.f3789d.add(this.f3788c);
            }
            int size = this.f3789d.size() - 2;
            if (size == -1) {
                z = true;
            } else {
                XmlsStackElement xmlsStackElement = (XmlsStackElement) this.f3789d.get(size);
                z = xmlsStackElement.f3784b && xmlsStackElement.f3783a + 1 == this.f3786a;
            }
            if (z) {
                collection.addAll(this.f3788c.f3785c);
                this.f3788c.f3784b = true;
                return;
            }
            Map hashMap = new HashMap();
            List<Attr> arrayList = new ArrayList();
            boolean z2 = true;
            for (int i = size; i >= 0; i--) {
                xmlsStackElement = (XmlsStackElement) this.f3789d.get(i);
                boolean z3 = xmlsStackElement.f3784b ? false : z2;
                Iterator it = xmlsStackElement.f3785c.iterator();
                while (it.hasNext() && z3) {
                    Attr attr = (Attr) it.next();
                    if (attr.getLocalName().equals("base")) {
                        if (!xmlsStackElement.f3784b) {
                            arrayList.add(attr);
                        }
                    } else if (!hashMap.containsKey(attr.getName())) {
                        hashMap.put(attr.getName(), attr);
                    }
                }
                z2 = z3;
            }
            if (!arrayList.isEmpty()) {
                for (Attr attr2 : this.f3788c.f3785c) {
                    if (attr2.getLocalName().equals("base")) {
                        str = attr2.getValue();
                        break;
                    }
                }
                Attr attr22 = null;
                attr = attr22;
                for (Attr attr222 : arrayList) {
                    if (str == null) {
                        str = attr222.getValue();
                        attr = attr222;
                    } else {
                        try {
                            str = Canonicalizer11.m5868a(attr222.getValue(), str);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (!(str == null || str.length() == 0)) {
                    attr.setValue(str);
                    collection.add(attr);
                }
            }
            this.f3788c.f3784b = true;
            collection.addAll(hashMap.values());
        }

        void m5846a(Attr attr) {
            if (this.f3788c == null) {
                this.f3788c = new XmlsStackElement();
                this.f3788c.f3783a = this.f3786a;
                this.f3789d.add(this.f3788c);
                this.f3787b = this.f3786a;
            }
            this.f3788c.f3785c.add(attr);
        }
    }

    static {
        Class a;
        if (f3811f == null) {
            a = m5867a("org.apache.xml.security.c14n.implementations.Canonicalizer11");
            f3811f = a;
        } else {
            a = f3811f;
        }
        f3810d = LogFactory.getLog(a.getName());
    }

    public Canonicalizer11(boolean z) {
        super(z);
        this.f3812b = true;
        this.f3813c = new TreeSet(g);
        this.f3814e = new XmlAttrStack();
    }

    static Class m5867a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static String m5868a(String str, String str2) {
        return m5871b(str, str2);
    }

    private static void m5869a(String str, String str2, String str3) {
        if (f3810d.isDebugEnabled()) {
            f3810d.debug(new StringBuffer().append(" ").append(str).append(":   ").append(str2).toString());
            if (str2.length() == 0) {
                f3810d.debug(new StringBuffer().append("\t\t\t\t").append(str3).toString());
            } else {
                f3810d.debug(new StringBuffer().append("\t\t\t").append(str3).toString());
            }
        }
    }

    private static String m5870b(String str) {
        f3810d.debug("STEP   OUTPUT BUFFER\t\tINPUT BUFFER");
        while (str.indexOf("//") > -1) {
            str = str.replaceAll("//", "/");
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (str.charAt(0) == '/') {
            stringBuffer.append("/");
            str = str.substring(1);
        }
        m5869a("1 ", stringBuffer.toString(), str);
        while (str.length() != 0) {
            if (str.startsWith("./")) {
                str = str.substring(2);
                m5869a("2A", stringBuffer.toString(), str);
            } else if (str.startsWith("../")) {
                str = str.substring(3);
                if (!stringBuffer.toString().equals("/")) {
                    stringBuffer.append("../");
                }
                m5869a("2A", stringBuffer.toString(), str);
            } else if (str.startsWith("/./")) {
                str = str.substring(2);
                m5869a("2B", stringBuffer.toString(), str);
            } else if (str.equals("/.")) {
                str = str.replaceFirst("/.", "/");
                m5869a("2B", stringBuffer.toString(), str);
            } else if (str.startsWith("/../")) {
                str = str.substring(3);
                if (stringBuffer.length() == 0) {
                    stringBuffer.append("/");
                } else if (stringBuffer.toString().endsWith("../")) {
                    stringBuffer.append("..");
                } else if (stringBuffer.toString().endsWith("..")) {
                    stringBuffer.append("/..");
                } else {
                    r1 = stringBuffer.lastIndexOf("/");
                    if (r1 == -1) {
                        stringBuffer = new StringBuffer();
                        if (str.charAt(0) == '/') {
                            str = str.substring(1);
                        }
                    } else {
                        stringBuffer = stringBuffer.delete(r1, stringBuffer.length());
                    }
                }
                m5869a("2C", stringBuffer.toString(), str);
            } else if (str.equals("/..")) {
                str = str.replaceFirst("/..", "/");
                if (stringBuffer.length() == 0) {
                    stringBuffer.append("/");
                } else if (stringBuffer.toString().endsWith("../")) {
                    stringBuffer.append("..");
                } else if (stringBuffer.toString().endsWith("..")) {
                    stringBuffer.append("/..");
                } else {
                    r1 = stringBuffer.lastIndexOf("/");
                    if (r1 == -1) {
                        stringBuffer = new StringBuffer();
                        if (str.charAt(0) == '/') {
                            str = str.substring(1);
                        }
                    } else {
                        stringBuffer = stringBuffer.delete(r1, stringBuffer.length());
                    }
                }
                m5869a("2C", stringBuffer.toString(), str);
            } else if (str.equals(".")) {
                str = BuildConfig.FLAVOR;
                m5869a("2D", stringBuffer.toString(), str);
            } else if (str.equals("..")) {
                if (!stringBuffer.toString().equals("/")) {
                    stringBuffer.append("..");
                }
                str = BuildConfig.FLAVOR;
                m5869a("2D", stringBuffer.toString(), str);
            } else {
                int indexOf;
                String substring;
                r1 = str.indexOf(47);
                if (r1 == 0) {
                    indexOf = str.indexOf(47, 1);
                } else {
                    indexOf = r1;
                    r1 = 0;
                }
                if (indexOf == -1) {
                    substring = str.substring(r1);
                    str = BuildConfig.FLAVOR;
                } else {
                    substring = str.substring(r1, indexOf);
                    str = str.substring(indexOf);
                }
                stringBuffer.append(substring);
                m5869a("2E", stringBuffer.toString(), str);
            }
        }
        if (stringBuffer.toString().endsWith("..")) {
            stringBuffer.append("/");
            m5869a("3 ", stringBuffer.toString(), str);
        }
        return stringBuffer.toString();
    }

    private static String m5871b(String str, String str2) {
        String scheme;
        String authority;
        String path;
        String str3 = BuildConfig.FLAVOR;
        if (str != null) {
            if (str.endsWith("..")) {
                str = new StringBuffer().append(str).append("/").toString();
            }
            URI uri = new URI(str);
            scheme = uri.getScheme();
            authority = uri.getAuthority();
            path = uri.getPath();
            str3 = uri.getQuery();
            uri.getFragment();
        } else {
            path = str3;
            authority = null;
            scheme = null;
            str3 = null;
        }
        URI uri2 = new URI(str2);
        String scheme2 = uri2.getScheme();
        String authority2 = uri2.getAuthority();
        String path2 = uri2.getPath();
        String query = uri2.getQuery();
        if (scheme2 != null && scheme2.equals(scheme)) {
            scheme2 = null;
        }
        if (scheme2 != null) {
            scheme = m5870b(path2);
            path2 = query;
            authority = authority2;
            path = scheme2;
        } else {
            if (authority2 != null) {
                path = m5870b(path2);
                authority = authority2;
            } else {
                if (path2.length() != 0) {
                    if (path2.startsWith("/")) {
                        str3 = m5870b(path2);
                    } else {
                        if (authority == null || path.length() != 0) {
                            int lastIndexOf = path.lastIndexOf(47);
                            str3 = lastIndexOf == -1 ? path2 : new StringBuffer().append(path.substring(0, lastIndexOf + 1)).append(path2).toString();
                        } else {
                            str3 = new StringBuffer().append("/").append(path2).toString();
                        }
                        str3 = m5870b(str3);
                    }
                    path = str3;
                    str3 = query;
                } else if (query != null) {
                    str3 = query;
                }
                query = str3;
            }
            path2 = query;
            String str4 = path;
            path = scheme;
            scheme = str4;
        }
        return new URI(path, authority, scheme, path2, null).toString();
    }

    Iterator m5872a(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        if (!element.hasAttributes() && !this.f3812b) {
            return null;
        }
        Collection collection = this.f3813c;
        collection.clear();
        NamedNodeMap attributes = element.getAttributes();
        int length = attributes.getLength();
        for (int i = 0; i < length; i++) {
            Attr attr = (Attr) attributes.item(i);
            if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                String localName = attr.getLocalName();
                String value = attr.getValue();
                if (!"xml".equals(localName) || !"http://www.w3.org/XML/1998/namespace".equals(value)) {
                    Node b = nameSpaceSymbTable.m5897b(localName, value, attr);
                    if (b != null) {
                        collection.add(b);
                        if (C14nHelper.m5841a(attr)) {
                            throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), localName, attr.getNodeValue()});
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                collection.add(attr);
            }
        }
        if (this.f3812b) {
            nameSpaceSymbTable.m5894a(collection);
            this.f3814e.m5845a(collection);
            this.f3812b = false;
        }
        return collection.iterator();
    }

    void m5873a(XMLSignatureInput xMLSignatureInput) {
        if (xMLSignatureInput.m6011a()) {
            XMLUtils.m6163a(xMLSignatureInput.m6028m() != null ? XMLUtils.m6169b(xMLSignatureInput.m6028m()) : XMLUtils.m6158a(xMLSignatureInput.m6012b()));
        }
    }

    public byte[] m5874a(Node node, String str) {
        throw new CanonicalizationException("c14n.Canonicalizer.UnsupportedOperation");
    }

    Iterator m5875b(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        NamedNodeMap namedNodeMap;
        int length;
        Object obj = null;
        this.f3814e.m5844a(nameSpaceSymbTable.m5905f());
        int i = m5852a((Node) element, nameSpaceSymbTable.m5905f()) == 1 ? 1 : 0;
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            namedNodeMap = attributes;
            length = attributes.getLength();
        } else {
            length = 0;
            namedNodeMap = null;
        }
        Collection collection = this.f3813c;
        collection.clear();
        for (int i2 = 0; i2 < length; i2++) {
            Attr attr = (Attr) namedNodeMap.item(i2);
            String namespaceURI = attr.getNamespaceURI();
            if ("http://www.w3.org/2000/xmlns/".equals(namespaceURI)) {
                namespaceURI = attr.getLocalName();
                String value = attr.getValue();
                if (!"xml".equals(namespaceURI) || !"http://www.w3.org/XML/1998/namespace".equals(value)) {
                    if (m5865c(attr)) {
                        if (i != 0 || !nameSpaceSymbTable.m5904e(namespaceURI)) {
                            Node b = nameSpaceSymbTable.m5897b(namespaceURI, value, attr);
                            if (b != null) {
                                collection.add(b);
                                if (C14nHelper.m5841a(attr)) {
                                    throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), namespaceURI, attr.getNodeValue()});
                                }
                            } else {
                                continue;
                            }
                        }
                    } else if (i == 0 || "xmlns".equals(namespaceURI)) {
                        nameSpaceSymbTable.m5895a(namespaceURI, value, attr);
                    } else {
                        nameSpaceSymbTable.m5900c(namespaceURI);
                    }
                }
            } else if ("http://www.w3.org/XML/1998/namespace".equals(namespaceURI)) {
                if (!attr.getLocalName().equals("id")) {
                    this.f3814e.m5846a(attr);
                } else if (i != 0) {
                    collection.add(attr);
                }
            } else if (i != 0) {
                collection.add(attr);
            }
        }
        if (i != 0) {
            Node attributeNodeNS = element.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", "xmlns");
            if (attributeNodeNS == null) {
                obj = nameSpaceSymbTable.m5892a("xmlns");
            } else if (!m5865c(attributeNodeNS)) {
                obj = nameSpaceSymbTable.m5897b("xmlns", BuildConfig.FLAVOR, i);
            }
            if (obj != null) {
                collection.add(obj);
            }
            this.f3814e.m5845a(collection);
            nameSpaceSymbTable.m5894a(collection);
        }
        return collection.iterator();
    }

    void m5876c(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        if (element.hasAttributes()) {
            this.f3814e.m5844a(-1);
            NamedNodeMap attributes = element.getAttributes();
            int length = attributes.getLength();
            for (int i = 0; i < length; i++) {
                Attr attr = (Attr) attributes.item(i);
                if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                    String localName = attr.getLocalName();
                    String nodeValue = attr.getNodeValue();
                    if (!"xml".equals(localName) || !"http://www.w3.org/XML/1998/namespace".equals(nodeValue)) {
                        nameSpaceSymbTable.m5895a(localName, nodeValue, attr);
                    }
                } else if (!"http://www.w3.org/XML/1998/namespace".equals(attr.getNamespaceURI())) {
                    this.f3814e.m5846a(attr);
                }
            }
        }
    }
}
