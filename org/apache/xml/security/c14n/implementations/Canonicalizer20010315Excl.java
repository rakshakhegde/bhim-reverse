package org.apache.xml.security.c14n.implementations;

import com.crashlytics.android.core.BuildConfig;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer20010315Excl extends CanonicalizerBase {
    TreeSet f3825b;
    final SortedSet f3826c;

    public Canonicalizer20010315Excl(boolean z) {
        super(z);
        this.f3825b = new TreeSet();
        this.f3826c = new TreeSet(g);
    }

    Iterator m5885a(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        int length;
        NamedNodeMap namedNodeMap;
        Object prefix;
        SortedSet sortedSet = this.f3826c;
        sortedSet.clear();
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            length = attributes.getLength();
            namedNodeMap = attributes;
        } else {
            length = 0;
            namedNodeMap = null;
        }
        SortedSet<String> sortedSet2 = (SortedSet) this.f3825b.clone();
        for (int i = 0; i < length; i++) {
            Attr attr = (Attr) namedNodeMap.item(i);
            String localName;
            if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                localName = attr.getLocalName();
                String nodeValue = attr.getNodeValue();
                if (!("xml".equals(localName) && "http://www.w3.org/XML/1998/namespace".equals(nodeValue)) && nameSpaceSymbTable.m5895a(localName, nodeValue, attr) && C14nHelper.m5840a(nodeValue)) {
                    throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), localName, attr.getNodeValue()});
                }
            }
            localName = attr.getPrefix();
            if (!(localName == null || localName.equals("xml") || localName.equals("xmlns"))) {
                sortedSet2.add(localName);
            }
            sortedSet.add(attr);
        }
        if (element.getNamespaceURI() != null) {
            prefix = element.getPrefix();
            if (prefix == null || prefix.length() == 0) {
                prefix = "xmlns";
            }
        } else {
            prefix = "xmlns";
        }
        sortedSet2.add(prefix);
        for (String a : sortedSet2) {
            Attr a2 = nameSpaceSymbTable.m5892a(a);
            if (a2 != null) {
                sortedSet.add(a2);
            }
        }
        return sortedSet.iterator();
    }

    void m5886a(XMLSignatureInput xMLSignatureInput) {
        if (xMLSignatureInput.m6011a() && !this.f3825b.isEmpty()) {
            XMLUtils.m6163a(xMLSignatureInput.m6028m() != null ? XMLUtils.m6169b(xMLSignatureInput.m6028m()) : XMLUtils.m6158a(xMLSignatureInput.m6012b()));
        }
    }

    public byte[] m5887a(XMLSignatureInput xMLSignatureInput, String str) {
        this.f3825b = (TreeSet) InclusiveNamespaces.m6081a(str);
        return super.m5863b(xMLSignatureInput);
    }

    public byte[] m5888a(Node node) {
        return m5890a(node, BuildConfig.FLAVOR, null);
    }

    public byte[] m5889a(Node node, String str) {
        return m5890a(node, str, null);
    }

    public byte[] m5890a(Node node, String str, Node node2) {
        this.f3825b = (TreeSet) InclusiveNamespaces.m6081a(str);
        return super.m5859a(node, node2);
    }

    final Iterator m5891b(Element element, NameSpaceSymbTable nameSpaceSymbTable) {
        NamedNodeMap namedNodeMap;
        int length;
        SortedSet sortedSet = this.f3826c;
        sortedSet.clear();
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            namedNodeMap = attributes;
            length = attributes.getLength();
        } else {
            namedNodeMap = null;
            length = 0;
        }
        Object obj = m5852a((Node) element, nameSpaceSymbTable.m5905f()) == 1 ? 1 : null;
        Set<String> set = obj != null ? (Set) this.f3825b.clone() : null;
        for (int i = 0; i < length; i++) {
            Attr attr = (Attr) namedNodeMap.item(i);
            String localName;
            if ("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) {
                localName = attr.getLocalName();
                if (obj == null || m5865c(attr) || "xmlns".equals(localName)) {
                    String nodeValue = attr.getNodeValue();
                    if (obj == null && m5865c(attr) && this.f3825b.contains(localName) && !nameSpaceSymbTable.m5904e(localName)) {
                        Node b = nameSpaceSymbTable.m5897b(localName, nodeValue, attr);
                        if (b != null) {
                            sortedSet.add(b);
                            if (C14nHelper.m5841a(attr)) {
                                throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), localName, attr.getNodeValue()});
                            }
                        }
                    }
                    if (nameSpaceSymbTable.m5895a(localName, nodeValue, attr) && C14nHelper.m5840a(nodeValue)) {
                        throw new CanonicalizationException("c14n.Canonicalizer.RelativeNamespace", new Object[]{element.getTagName(), localName, attr.getNodeValue()});
                    }
                }
                nameSpaceSymbTable.m5902d(localName);
            } else if (m5865c(attr) && obj != null) {
                localName = attr.getPrefix();
                if (!(localName == null || localName.equals("xml") || localName.equals("xmlns"))) {
                    set.add(localName);
                }
                sortedSet.add(attr);
            }
        }
        if (obj != null) {
            String prefix;
            Node attributeNodeNS = element.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", "xmlns");
            if (!(attributeNodeNS == null || m5865c(attributeNodeNS))) {
                nameSpaceSymbTable.m5895a("xmlns", BuildConfig.FLAVOR, i);
            }
            if (element.getNamespaceURI() != null) {
                prefix = element.getPrefix();
                if (prefix == null || prefix.length() == 0) {
                    set.add("xmlns");
                } else {
                    set.add(prefix);
                }
            } else {
                set.add("xmlns");
            }
            for (String prefix2 : set) {
                attr = nameSpaceSymbTable.m5892a(prefix2);
                if (attr != null) {
                    sortedSet.add(attr);
                }
            }
        }
        return sortedSet.iterator();
    }
}
