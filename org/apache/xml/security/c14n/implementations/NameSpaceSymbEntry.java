package org.apache.xml.security.c14n.implementations;

import org.w3c.dom.Attr;

class NameSpaceSymbEntry implements Cloneable {
    int f3827a;
    String f3828b;
    String f3829c;
    String f3830d;
    boolean f3831e;
    Attr f3832f;

    NameSpaceSymbEntry(String str, Attr attr, boolean z, String str2) {
        this.f3827a = 0;
        this.f3830d = null;
        this.f3831e = false;
        this.f3829c = str;
        this.f3831e = z;
        this.f3832f = attr;
        this.f3828b = str2;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
