package org.apache.xml.security.c14n;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public abstract class CanonicalizerSpi {
    protected boolean f3782a;

    public CanonicalizerSpi() {
        this.f3782a = false;
    }

    public abstract void m5836a(OutputStream outputStream);

    public abstract byte[] m5837a(Node node);

    public abstract byte[] m5838a(Node node, String str);

    public byte[] m5839a(byte[] bArr) {
        InputSource inputSource = new InputSource(new ByteArrayInputStream(bArr));
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        newInstance.setNamespaceAware(true);
        return m5837a(newInstance.newDocumentBuilder().parse(inputSource));
    }
}
