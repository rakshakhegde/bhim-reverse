package org.npci.upi.security.services;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: org.npci.upi.security.services.b */
public abstract class C0657b extends Binder implements C0656a {
    public C0657b() {
        attachInterface(this, "org.npci.upi.security.services.CLRemoteService");
    }

    public static C0656a m6401a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("org.npci.upi.security.services.CLRemoteService");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof C0656a)) ? new C0664c(iBinder) : (C0656a) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case R.View_android_focusable /*1*/:
                parcel.enforceInterface("org.npci.upi.security.services.CLRemoteService");
                String a = m6398a(parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(a);
                return true;
            case R.View_paddingStart /*2*/:
                parcel.enforceInterface("org.npci.upi.security.services.CLRemoteService");
                boolean a2 = m6400a(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(a2 ? 1 : 0);
                return true;
            case R.View_paddingEnd /*3*/:
                parcel.enforceInterface("org.npci.upi.security.services.CLRemoteService");
                m6399a(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), C0661e.m6412a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("org.npci.upi.security.services.CLRemoteService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
