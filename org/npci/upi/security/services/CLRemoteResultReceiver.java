package org.npci.upi.security.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;

public class CLRemoteResultReceiver extends Service {
    private IBinder mBinder;
    private ResultReceiver mResultReceiver;

    /* renamed from: org.npci.upi.security.services.CLRemoteResultReceiver.1 */
    class C06621 extends C0661e {
        final /* synthetic */ CLRemoteResultReceiver f4254a;

        C06621(CLRemoteResultReceiver cLRemoteResultReceiver) {
            this.f4254a = cLRemoteResultReceiver;
        }

        public void m6413a(Bundle bundle) {
            this.f4254a.mResultReceiver.send(1, bundle);
        }

        public void m6414b(Bundle bundle) {
            this.f4254a.mResultReceiver.send(2, bundle);
        }
    }

    public CLRemoteResultReceiver(ResultReceiver resultReceiver) {
        this.mBinder = new C06621(this);
        this.mResultReceiver = resultReceiver;
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public IBinder getBinder() {
        return this.mBinder;
    }
}
