package org.npci.upi.security.services;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: org.npci.upi.security.services.e */
public abstract class C0661e extends Binder implements C0660d {
    public C0661e() {
        attachInterface(this, "org.npci.upi.security.services.CLResultReceiver");
    }

    public static C0660d m6412a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("org.npci.upi.security.services.CLResultReceiver");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof C0660d)) ? new C0665f(iBinder) : (C0660d) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        Bundle bundle = null;
        switch (i) {
            case R.View_android_focusable /*1*/:
                parcel.enforceInterface("org.npci.upi.security.services.CLResultReceiver");
                if (parcel.readInt() != 0) {
                    bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                }
                m6410a(bundle);
                parcel2.writeNoException();
                return true;
            case R.View_paddingStart /*2*/:
                parcel.enforceInterface("org.npci.upi.security.services.CLResultReceiver");
                if (parcel.readInt() != 0) {
                    bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                }
                m6411b(bundle);
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("org.npci.upi.security.services.CLResultReceiver");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
