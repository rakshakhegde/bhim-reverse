package org.npci.upi.security.pinactivitycomponent;

import android.view.KeyEvent;
import android.view.View;

class ah implements C0618f {
    final /* synthetic */ GetCredential f4097a;

    ah(GetCredential getCredential) {
        this.f4097a = getCredential;
    }

    public void m6267a(View view, int i) {
        this.f4097a.dispatchKeyEvent(new KeyEvent(0, i));
        if (i == 66 && this.f4097a.f4078v != null) {
            this.f4097a.f4078v.m6289a();
        }
    }
}
