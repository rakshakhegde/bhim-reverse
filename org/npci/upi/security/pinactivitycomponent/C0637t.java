package org.npci.upi.security.pinactivitycomponent;

import android.util.Base64;
import com.crashlytics.android.core.BuildConfig;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: org.npci.upi.security.pinactivitycomponent.t */
public class C0637t {
    private String f4161a;
    private C0640w f4162b;

    C0637t(C0640w c0640w) {
        this.f4162b = c0640w;
        this.f4161a = m6341a("MS03LTItNA==");
    }

    private String m6341a(String str) {
        String str2 = BuildConfig.FLAVOR;
        for (String b : new String(Base64.decode(str, -1)).split("-")) {
            str2 = str2 + this.f4162b.m6352b(b);
        }
        return new String(Base64.decode(str2, -1));
    }

    private void m6342b(JSONObject jSONObject) {
    }

    public String m6343a(JSONObject jSONObject) {
        try {
            m6342b(jSONObject);
            StringBuilder stringBuilder = new StringBuilder();
            C0625g.m6312b("Common Library", "Salt Format: " + this.f4161a);
            CharSequence charSequence = this.f4161a;
            C0625g.m6312b("Common Library", "Temp Salt Format: " + charSequence);
            if (!(charSequence == null || charSequence.isEmpty())) {
                Matcher matcher = Pattern.compile("\\[([^\\]]*)\\]").matcher(charSequence);
                StringBuffer stringBuffer = new StringBuffer(1000);
                while (matcher.find()) {
                    String group = matcher.group();
                    matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(jSONObject.optString(group.substring(1, group.length() - 1))));
                }
                matcher.appendTail(stringBuffer);
                String stringBuffer2 = stringBuffer.toString();
                C0625g.m6312b("Common Library", "Output Salt: " + stringBuffer2);
                return stringBuffer2;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
