package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import org.npci.upi.security.services.C0657b;
import org.npci.upi.security.services.C0660d;

/* renamed from: org.npci.upi.security.pinactivitycomponent.y */
class C0658y extends C0657b {
    final /* synthetic */ CLRemoteServiceImpl f4247a;
    private Context f4248b;

    private C0658y(CLRemoteServiceImpl cLRemoteServiceImpl, Context context) {
        this.f4247a = cLRemoteServiceImpl;
        this.f4248b = null;
        this.f4248b = context;
    }

    public String m6402a(String str, String str2) {
        return this.f4247a.f4061b.m6407a(str, str2);
    }

    public void m6403a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, C0660d c0660d) {
        Bundle a = this.f4247a.m6213a(str, str2, str3, str4, str5, str6, str7, str8, c0660d);
        Intent intent = new Intent(this.f4248b, GetCredential.class);
        intent.setFlags(268435456);
        intent.putExtras(a);
        this.f4248b.startActivity(intent);
    }

    public boolean m6404a(String str, String str2, String str3, String str4) {
        return this.f4247a.f4061b.m6408a(str, str2, str3, str4);
    }
}
