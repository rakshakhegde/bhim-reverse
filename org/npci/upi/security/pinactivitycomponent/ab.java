package org.npci.upi.security.pinactivitycomponent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.List;

public class ab extends SQLiteOpenHelper {
    public ab(Context context) {
        super(context, "contactsManager", null, 1);
    }

    public String m6256a(String str, String str2, String str3) {
        List a = m6257a();
        return (a == null || a.isEmpty()) ? null : ((C0638u) a.get(0)).m6344a();
    }

    public List m6257a() {
        List arrayList = new ArrayList();
        Cursor rawQuery = getWritableDatabase().rawQuery("SELECT  * FROM contacts", null);
        if (rawQuery.moveToFirst()) {
            do {
                C0638u c0638u = new C0638u();
                c0638u.m6345a(rawQuery.getString(1));
                c0638u.m6347b(rawQuery.getString(2));
                c0638u.m6349c(rawQuery.getString(3));
                arrayList.add(c0638u);
            } while (rawQuery.moveToNext());
        }
        return arrayList;
    }

    void m6258a(C0638u c0638u) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("k0", c0638u.m6344a());
        contentValues.put("token", c0638u.m6346b());
        contentValues.put("date", c0638u.m6348c());
        writableDatabase.insert("contacts", null, contentValues);
        writableDatabase.close();
    }

    public int m6259b(C0638u c0638u) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("k0", c0638u.m6344a());
        contentValues.put("token", c0638u.m6346b());
        contentValues.put("date", c0638u.m6348c());
        return writableDatabase.update("contacts", contentValues, "k0 = ?", new String[]{c0638u.m6344a()});
    }

    public String m6260b() {
        String str = BuildConfig.FLAVOR;
        List a = m6257a();
        return (a == null || a.isEmpty()) ? str : ((C0638u) a.get(0)).m6346b();
    }

    public void m6261c() {
        C0625g.m6312b("DB Handler", "Deleting all");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("delete from contacts");
        writableDatabase.close();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE contacts(id INTEGER PRIMARY KEY,k0 TEXT,token TEXT,date TEXT)");
        Log.e("Dynamic DB", "tables created");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(sQLiteDatabase);
    }
}
