package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.p006f.af;
import android.support.v7.p013a.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class GetCredential extends ActionBarActivity {
    private static final String f4063m;
    private TransitionDrawable f4064A;
    private ImageView f4065B;
    private int f4066C;
    private boolean f4067D;
    private int f4068E;
    private UncaughtExceptionHandler f4069F;
    private JSONObject f4070n;
    private JSONObject f4071o;
    private JSONObject f4072p;
    private JSONArray f4073q;
    private JSONArray f4074r;
    private String f4075s;
    private an f4076t;
    private C0640w f4077u;
    private C0620h f4078v;
    private final Context f4079w;
    private am f4080x;
    private View f4081y;
    private View f4082z;

    static {
        f4063m = GetCredential.class.getSimpleName();
    }

    public GetCredential() {
        this.f4070n = null;
        this.f4071o = null;
        this.f4072p = null;
        this.f4073q = null;
        this.f4074r = new JSONArray();
        this.f4075s = CLConstants.DEFAULT_LANGUAGE_PREFERENCE;
        this.f4078v = null;
        this.f4079w = this;
        this.f4067D = false;
        this.f4068E = 0;
        this.f4069F = null;
    }

    private int m6216a(float f) {
        return (int) (((float) (getResources().getDisplayMetrics().densityDpi / 160)) * f);
    }

    private void m6221b(boolean z) {
        float f = 0.0f;
        if (z) {
            m6237a(0.0f, 180.0f, 300, this.f4065B);
        } else {
            m6237a(180.0f, 0.0f, 300, this.f4065B);
        }
        if (VERSION.SDK_INT > 14) {
            int height = this.f4081y.getHeight();
            if (height == 0) {
                height = this.f4066C;
            }
            this.f4081y.clearAnimation();
            ViewPropertyAnimator y = this.f4081y.animate().y(z ? 0.0f : -1.0f * ((float) height));
            if (z) {
                f = 1.0f;
            }
            y.alpha(f).setDuration(300).setInterpolator(new AccelerateInterpolator()).setListener(new al(this, z, height));
            return;
        }
        this.f4081y.setVisibility(z ? 0 : 8);
    }

    private boolean m6229q() {
        String[] strArr = new String[]{CLConstants.CREDTYPE_ATMPIN, "SMS|EMAIL|HOTP|TOTP", CLConstants.CREDTYPE_MPIN};
        String[] strArr2 = new String[3];
        if (this.f4073q != null && this.f4073q.length() == 3) {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            for (int i4 = 0; i4 < this.f4073q.length(); i4++) {
                try {
                    strArr2[i4] = ((JSONObject) this.f4073q.get(i4)).optString(CLConstants.FIELD_SUBTYPE, BuildConfig.FLAVOR);
                    if (strArr2[i4].matches(strArr[0])) {
                        i3 = true;
                    }
                    if (strArr2[i4].matches(strArr[1])) {
                        i2 = true;
                    }
                    if (strArr2[i4].matches(strArr[2])) {
                        i = true;
                    }
                } catch (Exception e) {
                    C0625g.m6310a(f4063m, e);
                }
            }
            if (!(i3 == 0 || r4 == 0 || r3 == 0)) {
                return true;
            }
        }
        return false;
    }

    private void m6230r() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                String string = extras.getString(CLConstants.INPUT_KEY_CONFIGURATION);
                if (string != null) {
                    this.f4070n = new JSONObject(string);
                }
                string = extras.getString(CLConstants.INPUT_KEY_CONTROLS);
                if (string != null) {
                    this.f4071o = new JSONObject(string);
                    if (this.f4071o != null) {
                        string = this.f4071o.getString(CLConstants.FIELD_CRED_ALLOWED);
                        if (string != null) {
                            this.f4073q = new JSONArray(string);
                        }
                    }
                }
                string = extras.getString(CLConstants.INPUT_KEY_SALT);
                if (string != null) {
                    this.f4072p = new JSONObject(string);
                }
                string = extras.getString(CLConstants.INPUT_KEY_PAY_INFO);
                if (string != null) {
                    this.f4074r = new JSONArray(string);
                }
                String string2 = extras.getString(CLConstants.INPUT_KEY_LANGUAGE_PREFERENCE);
                if (string2 != null) {
                    this.f4075s = string2;
                    String[] split = this.f4075s.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    Locale locale = new Locale(this.f4075s);
                    if (split.length == 2) {
                        locale = new Locale(split[0], split[1]);
                    }
                    Locale.setDefault(locale);
                    Configuration configuration = new Configuration();
                    configuration.locale = locale;
                    getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
                }
            } catch (Exception e) {
                C0625g.m6310a(f4063m, e);
            }
        }
    }

    private void m6231s() {
        Keypad keypad = (Keypad) findViewById(R.fragmentTelKeyboard);
        if (keypad != null) {
            keypad.setOnKeyPressCallback(new ah(this));
        }
    }

    private boolean m6232t() {
        return this.f4081y.getVisibility() == 0;
    }

    private void m6233u() {
        this.f4076t = new an();
        try {
            this.f4077u = new C0640w(getApplicationContext(), this.f4076t, this);
            this.f4076t.m6272a(getIntent().getExtras(), this);
        } catch (Throwable e) {
            Log.e(f4063m, e.m6308a(), e);
        } catch (Throwable e2) {
            Log.e(f4063m, "Error parsing and validating arguments to CL", e2);
        }
    }

    private void m6234v() {
        IntentFilter intentFilter = new IntentFilter();
        try {
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(999);
            registerReceiver(this.f4080x, intentFilter);
        } catch (Throwable th) {
            C0625g.m6311a(f4063m, "Failed to register SMS broadcast receiver (Ignoring)");
        }
    }

    private void m6235w() {
        try {
            if (this.f4080x != null) {
                unregisterReceiver(this.f4080x);
                this.f4080x = null;
            }
        } catch (Throwable th) {
            C0625g.m6311a(f4063m, "Failed to unregister SMS receiver (Ignoring)");
        }
    }

    private void m6236x() {
        Bundle bundle = new Bundle();
        bundle.putString(CLConstants.OUTPUT_KEY_ERROR, "USER_ABORTED");
        m6245o().m6355d().send(0, bundle);
        finish();
    }

    public void m6237a(float f, float f2, int i, View view) {
        Animation rotateAnimation = new RotateAnimation(f, f2, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration((long) i);
        rotateAnimation.setFillEnabled(true);
        rotateAnimation.setFillAfter(true);
        view.startAnimation(rotateAnimation);
    }

    public void m6238a(C0620h c0620h) {
        this.f4078v = c0620h;
    }

    public void m6239a(C0620h c0620h, Bundle bundle, boolean z) {
        try {
            FragmentManager f = m428f();
            if (bundle != null) {
                c0620h.m175g(bundle);
            }
            FragmentTransaction a = f.m462a();
            a.m318a(R.main_inner_layout, (Fragment) c0620h);
            if (z) {
                a.m321a(c0620h.getClass().getSimpleName());
            }
            a.m322b();
            m6238a(c0620h);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void m6240b(int i) {
        this.f4068E = i;
    }

    void m6241k() {
        String str = BuildConfig.FLAVOR;
        if (this.f4070n != null) {
            CharSequence optString = this.f4070n.optString(CLConstants.FIELD_PAYER_BANK_NAME);
        } else {
            Object obj = str;
        }
        if (this.f4072p == null) {
            C0623d c0623d = new C0623d(this, "l12", CLConstants.ERROR_MSG_SALT_MISSING);
            return;
        }
        CharSequence optString2;
        Object optString3;
        String optString4 = this.f4072p.optString(CLConstants.SALT_FIELD_TXN_AMOUNT);
        String str2 = BuildConfig.FLAVOR;
        if (str2.equals(BuildConfig.FLAVOR)) {
            int i = 0;
            while (i < this.f4074r.length()) {
                try {
                    if (((JSONObject) this.f4074r.get(i)).optString(CLConstants.FIELD_PAY_INFO_NAME, BuildConfig.FLAVOR).equals("payeeName")) {
                        optString2 = ((JSONObject) this.f4074r.get(i)).optString(CLConstants.FIELD_PAY_INFO_VALUE, BuildConfig.FLAVOR);
                        break;
                    } else if (((JSONObject) this.f4074r.get(i)).optString(CLConstants.FIELD_PAY_INFO_NAME, BuildConfig.FLAVOR).equals("account")) {
                        optString3 = ((JSONObject) this.f4074r.get(i)).optString(CLConstants.FIELD_PAY_INFO_VALUE, BuildConfig.FLAVOR);
                        break;
                    } else if (((JSONObject) this.f4074r.get(i)).optString(CLConstants.FIELD_PAY_INFO_NAME, BuildConfig.FLAVOR).equals(CLConstants.SALT_FIELD_MOBILE_NUMBER)) {
                        optString3 = ((JSONObject) this.f4074r.get(i)).optString(CLConstants.LABEL_MOBILE_NUMBER, BuildConfig.FLAVOR);
                        break;
                    } else {
                        i++;
                    }
                } catch (Exception e) {
                    C0625g.m6310a(f4063m, e);
                }
            }
        }
        optString3 = str2;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.transaction_bar_root);
        TextView textView = (TextView) findViewById(R.tv_acc_or_payee);
        TextView textView2 = (TextView) findViewById(R.transaction_bar_title);
        TextView textView3 = (TextView) findViewById(R.transaction_bar_info);
        this.f4065B = (ImageView) findViewById(R.transaction_bar_arrow);
        textView2.setText(optString2);
        if (!optString.equals(BuildConfig.FLAVOR)) {
            textView.setText(optString);
        }
        if (!optString4.equals(BuildConfig.FLAVOR)) {
            textView3.setText("\u20b9 " + optString4);
        }
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        if (VERSION.SDK_INT >= 13) {
            defaultDisplay.getSize(point);
            this.f4066C = point.y;
        } else {
            this.f4066C = defaultDisplay.getHeight();
        }
        linearLayout.setOnClickListener(new ai(this));
        this.f4081y = findViewById(R.transaction_details_scroller);
        this.f4082z = findViewById(R.transaction_details_expanded_space);
        this.f4081y.setOnTouchListener(new aj(this));
        if (this.f4082z != null) {
            this.f4082z.setOnTouchListener(new ak(this));
        }
        this.f4064A = (TransitionDrawable) findViewById(R.transaction_info_root).getBackground();
        this.f4064A.setCrossFadeEnabled(true);
    }

    void m6242l() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.transaction_details_root);
        for (int i = 0; i < this.f4074r.length(); i++) {
            ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(this).inflate(R.layout_transaction_details_item, linearLayout, false);
            TextView textView = (TextView) viewGroup.findViewById(R.transaction_details_item_name);
            TextView textView2 = (TextView) viewGroup.findViewById(R.transaction_details_item_value);
            JSONObject optJSONObject = this.f4074r.optJSONObject(i);
            textView.setText(optJSONObject.optString(CLConstants.FIELD_PAY_INFO_NAME).toUpperCase());
            textView2.setText(optJSONObject.optString(CLConstants.FIELD_PAY_INFO_VALUE));
            linearLayout.addView(viewGroup);
        }
        View view = new View(this);
        view.setLayoutParams(new LayoutParams(-1, m6216a(3.0f)));
        view.setBackgroundColor(-16777216);
        af.m1187b(view, 0.33f);
        linearLayout.addView(view);
    }

    public boolean m6243m() {
        return checkCallingOrSelfPermission("android.permission.RECEIVE_SMS") == 0;
    }

    public boolean m6244n() {
        return checkCallingOrSelfPermission("android.permission.READ_SMS") == 0;
    }

    public C0640w m6245o() {
        return this.f4077u;
    }

    public void onBackPressed() {
        if (this.f4067D) {
            Bundle bundle = new Bundle();
            bundle.putString(CLConstants.OUTPUT_KEY_ERROR, "USER_ABORTED");
            m6245o().m6355d().send(0, bundle);
            super.onBackPressed();
            return;
        }
        this.f4067D = true;
        Toast.makeText(this, getString(R.back_button_exit_message), 0).show();
        new Handler().postDelayed(new ag(this), 2000);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4069F = Thread.currentThread().getUncaughtExceptionHandler();
        Thread.currentThread().setUncaughtExceptionHandler(new ad());
        m6230r();
        setContentView(R.activity_pin_activity_component);
        m6231s();
        m6233u();
        m6241k();
        m6242l();
        if (m6229q()) {
            m6239a(new C0621b(), getIntent().getExtras(), false);
        } else {
            m6239a(new C0635r(), getIntent().getExtras(), false);
        }
        TextView textView = (TextView) findViewById(R.go_back);
        if (textView != null) {
            textView.setOnClickListener(new af(this));
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        Thread.currentThread().setUncaughtExceptionHandler(this.f4069F);
    }

    protected void onPause() {
        super.onPause();
        m6235w();
    }

    protected void onResume() {
        super.onResume();
        if (m6243m()) {
            this.f4080x = new am();
            m6234v();
            return;
        }
        Log.e(f4063m, "RECEIVE_SMS permission not provided by the App. This will affect Auto OTP detection feature of Common Library");
    }
}
