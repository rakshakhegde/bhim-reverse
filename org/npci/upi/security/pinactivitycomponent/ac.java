package org.npci.upi.security.pinactivitycomponent;

import com.crashlytics.android.core.BuildConfig;
import in.org.npci.commonlibrary.C0568e;
import in.org.npci.commonlibrary.C0569f;
import in.org.npci.commonlibrary.Message;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class ac {
    private C0568e f4091a;
    private String f4092b;
    private ab f4093c;

    public ac(C0568e c0568e, ab abVar, String str) {
        this.f4091a = c0568e;
        this.f4092b = str;
        this.f4093c = abVar;
    }

    private Message m6262a(String str, String str2, String str3, String str4, String str5, String str6) {
        Message a;
        C0569f e;
        try {
            a = this.f4091a.m5148a(this.f4092b, str4, str5, str, str6);
            try {
                a.setType(str2);
                a.setSubType(str3);
                a.getData().setEncryptedBase64String("2.0|" + a.getData().getEncryptedBase64String());
            } catch (C0569f e2) {
                e = e2;
                e.printStackTrace();
                return a;
            }
        } catch (C0569f e3) {
            e = e3;
            a = null;
            e.printStackTrace();
            return a;
        }
        return a;
    }

    public Message m6263a(String str, String str2, String str3, JSONObject jSONObject) {
        try {
            Message a;
            String string = jSONObject.getString(CLConstants.SALT_FIELD_TXN_ID);
            String string2 = jSONObject.getString(CLConstants.SALT_FIELD_CREDENTIAL);
            String string3 = jSONObject.getString(CLConstants.SALT_FIELD_APP_ID);
            String string4 = jSONObject.getString(CLConstants.SALT_FIELD_DEVICE_ID);
            String string5 = jSONObject.getString(CLConstants.SALT_FIELD_MOBILE_NUMBER);
            C0625g.m6312b("DBH in encryptor", this.f4093c == null ? "null" : this.f4093c.toString());
            string5 = this.f4093c.m6256a(string3, string4, string5);
            C0625g.m6312b("K0 in encryptor", string5);
            Matcher matcher = Pattern.compile("\\{([^}]*)\\}").matcher(str);
            StringBuffer stringBuffer = new StringBuffer();
            if (matcher.find()) {
                String group = matcher.group();
                a = m6262a(group.substring(1, group.length() - 1), str2, str3, string, string2, string5);
                matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(a.getData().getEncryptedBase64String().replaceAll("\n", BuildConfig.FLAVOR)));
            } else {
                a = null;
            }
            if (stringBuffer.length() > 0) {
                matcher.appendTail(stringBuffer);
            }
            if (a == null) {
                return a;
            }
            String stringBuffer2 = stringBuffer.toString();
            C0625g.m6312b("CommonLibrary", "Encrypted Data: " + stringBuffer2);
            a.getData().setEncryptedBase64String(stringBuffer2);
            return a;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
