package org.npci.upi.security.pinactivitycomponent;

import android.os.Handler;
import android.os.Looper;
import java.util.TimerTask;

/* renamed from: org.npci.upi.security.pinactivitycomponent.i */
class C0626i extends TimerTask {
    long f4135a;
    final /* synthetic */ C0634q f4136b;
    final /* synthetic */ int f4137c;
    final /* synthetic */ C0620h f4138d;

    C0626i(C0620h c0620h, C0634q c0634q, int i) {
        this.f4138d = c0620h;
        this.f4136b = c0634q;
        this.f4137c = i;
        this.f4135a = System.currentTimeMillis() - 45000;
    }

    public void run() {
        C0633p a = this.f4136b.m6328a(this.f4137c, this.f4135a - 2000);
        if (a != null) {
            new Handler(Looper.getMainLooper()).post(new C0627j(this, a));
        }
        this.f4135a = System.currentTimeMillis();
    }
}
