package org.npci.upi.security.pinactivitycomponent;

/* renamed from: org.npci.upi.security.pinactivitycomponent.p */
public class C0633p {
    private String f4147a;
    private String f4148b;
    private String f4149c;

    public String m6313a() {
        return this.f4147a;
    }

    public void m6314a(String str) {
        this.f4147a = str;
    }

    public String m6315b() {
        return this.f4148b;
    }

    public void m6316b(String str) {
        this.f4148b = str;
    }

    public String m6317c() {
        return this.f4149c;
    }

    public void m6318c(String str) {
        this.f4149c = str;
    }

    public String toString() {
        return "OtpSms{, sms='" + this.f4147a + '\'' + ", otp='" + this.f4148b + '\'' + ", id='" + this.f4149c + '\'' + '}';
    }
}
