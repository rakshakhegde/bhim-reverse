package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.crashlytics.android.core.BuildConfig;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: org.npci.upi.security.pinactivitycomponent.q */
public class C0634q {
    private static final String f4150b;
    JSONArray f4151a;
    private Context f4152c;
    private ap f4153d;
    private List f4154e;

    static {
        f4150b = C0634q.class.getName();
    }

    public C0634q(Context context) {
        this.f4151a = null;
        this.f4152c = context;
        this.f4153d = new ap(this.f4152c);
        this.f4151a = new JSONArray();
        byte[] a = ae.m6265a("npci_otp_rules.json", context);
        if (a != null) {
            try {
                this.f4151a = new JSONArray(new String(a));
            } catch (Exception e) {
                C0625g.m6310a(f4150b, e);
            }
        }
    }

    public static String m6319a(String str) {
        String toLowerCase = str.toLowerCase();
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(toLowerCase.getBytes(), 0, toLowerCase.length());
            toLowerCase = new BigInteger(1, instance.digest()).toString(16);
            while (toLowerCase.length() < 32) {
                toLowerCase = "0" + toLowerCase;
            }
            return toLowerCase;
        } catch (Exception e) {
            C0625g.m6310a(f4150b, e);
            return null;
        }
    }

    private String m6320a(ArrayList arrayList) {
        if (arrayList.size() <= 0) {
            return null;
        }
        String str = BuildConfig.FLAVOR + ((String) arrayList.get(0));
        int i = 1;
        while (i < arrayList.size()) {
            String str2 = str + "," + ((String) arrayList.get(i));
            i++;
            str = str2;
        }
        return str;
    }

    private C0633p m6321a(int i, String str, String str2, JSONObject jSONObject) {
        try {
            if (!m6323a(str2, jSONObject.getJSONArray("sender"))) {
                return null;
            }
            if (!m6322a(str, jSONObject.getString("message"))) {
                return null;
            }
            Matcher matcher = Pattern.compile(i != 0 ? "\\d{" + i + "}" : (String) jSONObject.get(CLConstants.OTP)).matcher(str);
            C0633p c0633p = new C0633p();
            c0633p.m6314a(str);
            if (!matcher.find() || 0 > matcher.groupCount()) {
                return null;
            }
            c0633p.m6316b(matcher.group(0));
            return c0633p;
        } catch (JSONException e) {
            return null;
        }
    }

    private boolean m6322a(String str, String str2) {
        return Pattern.compile(str2, 2).matcher(str).find();
    }

    private boolean m6323a(String str, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (Pattern.compile(jSONArray.getString(i), 2).matcher(str).find()) {
                return true;
            }
        }
        return false;
    }

    private boolean m6324b(String str) {
        if (this.f4154e == null) {
            this.f4154e = Arrays.asList(this.f4153d.m6280b(CLConstants.MGS_ID_PREFERENCES, BuildConfig.FLAVOR).split(","));
        }
        return this.f4154e.contains(str);
    }

    private boolean m6325b(C0633p c0633p) {
        return (m6324b(c0633p.m6317c()) || m6324b(m6326c(c0633p))) ? false : true;
    }

    private String m6326c(C0633p c0633p) {
        return C0634q.m6319a(c0633p.m6313a());
    }

    private void m6327c(String str) {
        if (str != null) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.f4153d.m6280b(CLConstants.MGS_ID_PREFERENCES, BuildConfig.FLAVOR).split(",")));
            if (!arrayList.contains(str)) {
                if (arrayList.size() >= 10) {
                    arrayList.remove(0);
                }
                arrayList.add(str);
                this.f4153d.m6279a(CLConstants.MGS_ID_PREFERENCES, m6320a(arrayList));
            }
        }
    }

    public C0633p m6328a(int i, long j) {
        Cursor query;
        Cursor cursor;
        Exception e;
        Throwable th;
        Uri parse = Uri.parse("content://sms/inbox");
        String[] strArr = new String[]{"_id", "address", "body", "date"};
        String format = String.format("date > ?", new Object[0]);
        String str = "date DESC";
        try {
            query = this.f4152c.getContentResolver().query(parse, strArr, format, new String[]{BuildConfig.FLAVOR + j}, str);
            while (query.moveToNext()) {
                try {
                    C0633p a = m6329a(i, query.getString(1), query.getString(2));
                    if (a != null) {
                        a.m6318c(String.valueOf(Long.valueOf(query.getLong(query.getColumnIndex("_id")))));
                        if (m6325b(a)) {
                            m6330a(a);
                            if (query == null) {
                                return a;
                            }
                            query.close();
                            return a;
                        }
                    }
                } catch (SecurityException e2) {
                    cursor = query;
                } catch (Exception e3) {
                    e = e3;
                }
            }
            if (query != null) {
                query.close();
            }
        } catch (SecurityException e4) {
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Exception e5) {
            e = e5;
            query = null;
            try {
                C0625g.m6310a(f4150b, e);
                if (query != null) {
                    query.close();
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            query = null;
            if (query != null) {
                query.close();
            }
            throw th;
        }
        return null;
    }

    public C0633p m6329a(int i, String str, String str2) {
        int i2 = 0;
        while (i2 < this.f4151a.length()) {
            try {
                C0633p a = m6321a(i, str2, str, this.f4151a.getJSONObject(i2));
                if (a != null) {
                    return a;
                }
                i2++;
            } catch (Exception e) {
                C0625g.m6310a(f4150b, e);
                return null;
            }
        }
        return null;
    }

    public void m6330a(C0633p c0633p) {
        m6327c(c0633p.m6317c() != null ? c0633p.m6317c() : m6326c(c0633p));
    }
}
