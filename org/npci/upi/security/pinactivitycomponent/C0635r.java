package org.npci.upi.security.pinactivitycomponent;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.p004a.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.commonlibrary.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.npci.upi.security.pinactivitycomponent.widget.C0619o;
import org.npci.upi.security.pinactivitycomponent.widget.C0641c;
import org.npci.upi.security.pinactivitycomponent.widget.C0642a;
import org.npci.upi.security.pinactivitycomponent.widget.C0643b;

/* renamed from: org.npci.upi.security.pinactivitycomponent.r */
public class C0635r extends C0620h implements C0619o {
    private static final String ar;
    LinearLayout ap;
    LinearLayout aq;
    private int as;
    private Timer at;
    private Boolean au;
    private HashMap av;
    private boolean aw;

    static {
        ar = C0620h.class.getSimpleName();
    }

    public C0635r() {
        this.as = 0;
        this.at = null;
        this.au = null;
        this.av = new HashMap();
        this.aw = false;
    }

    private void m6331M() {
        C0643b c0643b;
        String inputValue;
        int i = 0;
        if (this.g != -1 && (this.f.get(this.g) instanceof C0643b)) {
            c0643b = (C0643b) this.f.get(this.g);
            inputValue = c0643b.getInputValue();
            if (inputValue == null || inputValue.length() != c0643b.getInputLength()) {
                m6295b(c0643b, m139a(R.invalid_otp));
                return;
            }
        }
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (this.f.get(i2) instanceof C0643b) {
                c0643b = (C0643b) this.f.get(i2);
                if (c0643b.getInputValue().length() != c0643b.getInputLength()) {
                    m6295b(c0643b, m139a(R.componentMessage));
                    return;
                }
            }
        }
        if (!this.aw) {
            this.aw = true;
            while (i < this.f.size()) {
                try {
                    JSONObject jSONObject = (JSONObject) ((C0641c) this.f.get(i)).getFormDataTag();
                    inputValue = jSONObject.getString(CLConstants.FIELD_TYPE);
                    String string = jSONObject.getString(CLConstants.FIELD_SUBTYPE);
                    this.b.put(CLConstants.SALT_FIELD_CREDENTIAL, ((C0641c) this.f.get(i)).getInputValue());
                    Message a = ((GetCredential) this.ao).m6245o().m6353b().m6263a(((GetCredential) this.ao).m6245o().m6351a().m6343a(this.b), inputValue, string, this.b);
                    if (a != null) {
                        this.av.put(string, ao.m6277a(a));
                    }
                } catch (Exception e) {
                    C0625g.m6310a(ar, e);
                }
                i++;
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable("credBlocks", this.av);
            ((GetCredential) this.ao).m6245o().m6355d().send(1, bundle);
            ((GetCredential) this.ao).m6245o().m6354c().finish();
        }
    }

    private boolean m6332N() {
        if (this.au != null) {
            return this.au.booleanValue();
        }
        if (this.c != null) {
            List arrayList = new ArrayList();
            for (int i = 0; i < this.c.length(); i++) {
                try {
                    String string = this.c.getJSONObject(i).getString(CLConstants.FIELD_SUBTYPE);
                    if (string != null) {
                        arrayList.add(string);
                    }
                } catch (Exception e) {
                    C0625g.m6310a(ar, e);
                }
            }
            if (arrayList.contains(CLConstants.CREDTYPE_OTP) || arrayList.contains(CLConstants.CREDTYPE_SMS) || arrayList.contains(CLConstants.CREDTYPE_EMAIL) || arrayList.contains(CLConstants.CREDTYPE_HOTP) || (arrayList.contains(CLConstants.CREDTYPE_TOTP) && arrayList.contains(CLConstants.CREDTYPE_MPIN))) {
                this.au = Boolean.valueOf(true);
                return this.au.booleanValue();
            }
        }
        this.au = Boolean.valueOf(false);
        return this.au.booleanValue();
    }

    private void m6333O() {
        if (this.g != -1 && (this.f.get(this.g) instanceof C0643b)) {
            C0643b c0643b = (C0643b) this.f.get(this.g);
            m6294a(c0643b);
            c0643b.m6388a();
        }
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            if (i != this.g) {
                C0641c c0641c = (C0641c) this.f.get(i);
                Drawable a = ContextCompat.m77a(m176h(), R.ic_visibility_on);
                Drawable a2 = ContextCompat.m77a(m176h(), R.ic_visibility_off);
                String a3 = m139a(R.action_hide);
                String a4 = m139a(R.action_show);
                c0641c.m6367a(a4, a, new C0636s(this, c0641c, a3, a4, a2, a), 0, true, true);
            }
        }
    }

    private void m6334a(View view) {
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.main_inner_layout);
        if (this.c != null) {
            int i = 0;
            while (i < this.c.length()) {
                try {
                    JSONObject jSONObject = this.c.getJSONObject(i);
                    String string = jSONObject.getString(CLConstants.FIELD_SUBTYPE);
                    int optInt = jSONObject.optInt(CLConstants.FIELD_DLENGTH) == 0 ? 6 : jSONObject.optInt(CLConstants.FIELD_DLENGTH);
                    if (!string.equals(CLConstants.CREDTYPE_MPIN) && !string.equals(CLConstants.CREDTYPE_NMPIN) && !CLConstants.CREDTYPE_ATMPIN.equals(string) && !CLConstants.CREDTYPE_OTP.equals(string) && !CLConstants.CREDTYPE_SMS.equals(string) && !CLConstants.CREDTYPE_EMAIL.equals(string) && !CLConstants.CREDTYPE_HOTP.equals(string) && !CLConstants.CREDTYPE_TOTP.equals(string)) {
                        i++;
                    } else if (string.equals(CLConstants.CREDTYPE_NMPIN) || (string.equals(CLConstants.CREDTYPE_MPIN) && m6332N())) {
                        C0643b a = m6288a(m139a(R.npci_set_mpin_title), i, optInt);
                        C0643b a2 = m6288a(m139a(R.npci_confirm_mpin_title), i, optInt);
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(a);
                        arrayList.add(a2);
                        r1 = new C0642a(m176h());
                        r1.m6374a(arrayList, (C0619o) this);
                        r1.setFormDataTag(jSONObject);
                        this.f.add(r1);
                        linearLayout.addView(r1);
                        i++;
                    } else {
                        String str = BuildConfig.FLAVOR;
                        if (string.equals(CLConstants.CREDTYPE_MPIN)) {
                            str = m139a(R.npci_mpin_title);
                        } else if (CLConstants.CREDTYPE_OTP.equals(string) || CLConstants.CREDTYPE_SMS.equals(string) || CLConstants.CREDTYPE_EMAIL.equals(string) || CLConstants.CREDTYPE_HOTP.equals(string) || CLConstants.CREDTYPE_TOTP.equals(string)) {
                            string = m139a(R.npci_otp_title);
                            this.g = i;
                            if ((m176h() instanceof GetCredential) && ((GetCredential) m176h()).m6244n()) {
                                m6297c(optInt);
                                str = string;
                            } else {
                                str = string;
                            }
                        } else if (CLConstants.CREDTYPE_ATMPIN.equals(string)) {
                            str = m139a(R.npci_atm_title);
                        }
                        r1 = m6288a(str, i, optInt);
                        r1.setFormDataTag(jSONObject);
                        this.f.add(r1);
                        linearLayout.addView(r1);
                        i++;
                    }
                } catch (Throwable e) {
                    Log.e(ar, "Error while inflating Layout", e);
                }
            }
        }
    }

    public View m6335a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.fragment_pin, viewGroup, false);
    }

    public void m6336a() {
        if (this.as >= this.f.size() - 1) {
            m6331M();
        } else if (((C0641c) this.f.get(this.as + 1)).m6369d()) {
            this.as++;
            if (this.as >= this.f.size() - 1) {
                m6331M();
            }
        }
    }

    public void m6337a(int i, String str) {
        if (this.g != -1 && this.g == i && (this.f.get(this.g) instanceof C0643b)) {
            m6292a(this.at);
            ((C0643b) this.f.get(this.g)).m6393a(false);
            ((C0643b) this.f.get(this.g)).m6392a(BuildConfig.FLAVOR, null, false, false);
            ((C0643b) this.f.get(this.g)).m6390a(ContextCompat.m77a(m176h(), R.ic_tick_ok), true);
        }
    }

    public void m6338a(View view, Bundle bundle) {
        super.m6291a(view, bundle);
        this.aq = (LinearLayout) view.findViewById(R.main_inner_layout);
        this.ap = (LinearLayout) view.findViewById(R.main_layout);
        m6285K();
        m6334a(view);
        m6333O();
    }

    public void m6339a(View view, String str) {
        m6295b(view, str);
    }

    public void m6340b(int i) {
        if (!(this.f.get(i) instanceof C0642a)) {
            this.as = i;
        }
    }
}
