package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.f */
class C0646f implements AnimatorUpdateListener {
    final /* synthetic */ FormItemEditText f4237a;

    C0646f(FormItemEditText formItemEditText) {
        this.f4237a = formItemEditText;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4237a.f4199m.setAlpha(((Integer) valueAnimator.getAnimatedValue()).intValue());
        this.f4237a.invalidate();
    }
}
