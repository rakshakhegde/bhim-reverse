package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.af;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.a */
public class C0642a extends FrameLayout implements C0641c {
    private ArrayList f4213a;
    private int f4214b;
    private int f4215c;
    private Object f4216d;

    public C0642a(Context context) {
        super(context);
    }

    private void m6370a(View view) {
        if (VERSION.SDK_INT >= 12) {
            view.animate().x(0.0f);
        } else {
            af.m1190c(view, 0.0f);
        }
    }

    private void m6371a(View view, boolean z) {
        int i = z ? this.f4215c * -1 : this.f4215c;
        if (VERSION.SDK_INT >= 12) {
            view.animate().x((float) i);
        } else {
            af.m1190c(view, (float) i);
        }
    }

    private void m6372e() {
        for (int i = 0; i < this.f4213a.size(); i++) {
            ((C0643b) this.f4213a.get(i)).setText(BuildConfig.FLAVOR);
        }
        m6376b();
    }

    public void m6373a(String str, Drawable drawable, OnClickListener onClickListener, int i, boolean z, boolean z2) {
        for (int i2 = 0; i2 < this.f4213a.size(); i2++) {
            ((C0643b) this.f4213a.get(i2)).m6391a(str, drawable, onClickListener, i, z, z2);
        }
    }

    public void m6374a(ArrayList arrayList, C0619o c0619o) {
        this.f4213a = arrayList;
        addView((View) this.f4213a.get(0));
        ((C0643b) this.f4213a.get(0)).setFormItemListener(c0619o);
        this.f4214b = 0;
        this.f4215c = getResources().getDisplayMetrics().widthPixels;
        for (int i = 1; i < this.f4213a.size(); i++) {
            C0643b c0643b = (C0643b) this.f4213a.get(i);
            c0643b.setFormItemListener(c0619o);
            af.m1190c(c0643b, (float) this.f4215c);
            addView(c0643b);
        }
    }

    public boolean m6375a() {
        if (this.f4214b >= this.f4213a.size() - 1) {
            return false;
        }
        m6371a((C0643b) this.f4213a.get(this.f4214b), true);
        m6370a((C0643b) this.f4213a.get(this.f4214b + 1));
        this.f4214b++;
        ((C0643b) this.f4213a.get(this.f4214b)).requestFocus();
        return true;
    }

    public boolean m6376b() {
        if (this.f4214b == 0) {
            return false;
        }
        m6371a((C0643b) this.f4213a.get(this.f4214b), false);
        m6370a((C0643b) this.f4213a.get(this.f4214b - 1));
        this.f4214b--;
        ((C0643b) this.f4213a.get(this.f4214b)).requestFocus();
        return true;
    }

    public boolean m6377c() {
        return ((C0643b) this.f4213a.get(this.f4214b)).m6394c();
    }

    public boolean m6378d() {
        String inputValue = ((C0643b) this.f4213a.get(this.f4214b)).getInputValue();
        if (((C0643b) this.f4213a.get(this.f4214b)).getInputLength() != inputValue.length()) {
            ((C0643b) this.f4213a.get(this.f4214b)).requestFocus();
            return false;
        } else if (this.f4214b != this.f4213a.size() - 1) {
            return !m6375a();
        } else {
            ((C0643b) this.f4213a.get(this.f4214b)).requestFocus();
            int i = 0;
            while (i < this.f4213a.size()) {
                if (((C0643b) this.f4213a.get(i)).getInputValue().equals(inputValue)) {
                    i++;
                } else {
                    m6372e();
                    ((C0643b) this.f4213a.get(i)).getFormItemListener().m6282a((View) this, getContext().getString(R.info_pins_dont_match));
                    return false;
                }
            }
            return true;
        }
    }

    public Object getFormDataTag() {
        return this.f4216d == null ? ((C0643b) this.f4213a.get(0)).getFormDataTag() : this.f4216d;
    }

    public String getInputValue() {
        return ((C0643b) this.f4213a.get(0)).getInputValue();
    }

    public void setFormDataTag(Object obj) {
        this.f4216d = obj;
    }

    public void setText(String str) {
        for (int i = 0; i < this.f4213a.size(); i++) {
            ((C0643b) this.f4213a.get(i)).setText(str);
        }
    }
}
