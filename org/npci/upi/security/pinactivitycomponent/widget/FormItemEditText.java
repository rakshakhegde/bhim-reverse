package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.af;
import android.support.v4.p009d.TextUtilsCompat;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode.Callback;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import java.util.Locale;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class FormItemEditText extends EditText {
    private int[][] f4184A;
    private int[] f4185B;
    private ColorStateList f4186C;
    private String f4187a;
    private StringBuilder f4188b;
    private String f4189c;
    private int f4190d;
    private float f4191e;
    private float f4192f;
    private float f4193g;
    private float f4194h;
    private int f4195i;
    private RectF[] f4196j;
    private float[] f4197k;
    private Paint f4198l;
    private Paint f4199m;
    private Paint f4200n;
    private Drawable f4201o;
    private Rect f4202p;
    private boolean f4203q;
    private OnClickListener f4204r;
    private C0651k f4205s;
    private boolean f4206t;
    private float f4207u;
    private float f4208v;
    private Paint f4209w;
    private boolean f4210x;
    private boolean f4211y;
    private ColorStateList f4212z;

    public FormItemEditText(Context context) {
        super(context);
        this.f4187a = null;
        this.f4188b = null;
        this.f4189c = null;
        this.f4190d = 0;
        this.f4191e = 24.0f;
        this.f4193g = 4.0f;
        this.f4194h = 8.0f;
        this.f4195i = 4;
        this.f4202p = new Rect();
        this.f4203q = false;
        this.f4205s = null;
        this.f4207u = 1.0f;
        this.f4208v = 2.0f;
        this.f4210x = false;
        this.f4211y = false;
        int[][] iArr = new int[4][];
        iArr[0] = new int[]{16842913};
        iArr[1] = new int[]{16842914};
        iArr[2] = new int[]{16842908};
        iArr[3] = new int[]{-16842908};
        this.f4184A = iArr;
        this.f4185B = new int[]{-16711936, -65536, -16777216, -7829368};
        this.f4186C = new ColorStateList(this.f4184A, this.f4185B);
    }

    public FormItemEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4187a = null;
        this.f4188b = null;
        this.f4189c = null;
        this.f4190d = 0;
        this.f4191e = 24.0f;
        this.f4193g = 4.0f;
        this.f4194h = 8.0f;
        this.f4195i = 4;
        this.f4202p = new Rect();
        this.f4203q = false;
        this.f4205s = null;
        this.f4207u = 1.0f;
        this.f4208v = 2.0f;
        this.f4210x = false;
        this.f4211y = false;
        int[][] iArr = new int[4][];
        iArr[0] = new int[]{16842913};
        iArr[1] = new int[]{16842914};
        iArr[2] = new int[]{16842908};
        iArr[3] = new int[]{-16842908};
        this.f4184A = iArr;
        this.f4185B = new int[]{-16711936, -65536, -16777216, -7829368};
        this.f4186C = new ColorStateList(this.f4184A, this.f4185B);
        m6358a(context, attributeSet);
    }

    public FormItemEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f4187a = null;
        this.f4188b = null;
        this.f4189c = null;
        this.f4190d = 0;
        this.f4191e = 24.0f;
        this.f4193g = 4.0f;
        this.f4194h = 8.0f;
        this.f4195i = 4;
        this.f4202p = new Rect();
        this.f4203q = false;
        this.f4205s = null;
        this.f4207u = 1.0f;
        this.f4208v = 2.0f;
        this.f4210x = false;
        this.f4211y = false;
        int[][] iArr = new int[4][];
        iArr[0] = new int[]{16842913};
        iArr[1] = new int[]{16842914};
        iArr[2] = new int[]{16842908};
        iArr[3] = new int[]{-16842908};
        this.f4184A = iArr;
        this.f4185B = new int[]{-16711936, -65536, -16777216, -7829368};
        this.f4186C = new ColorStateList(this.f4184A, this.f4185B);
        m6358a(context, attributeSet);
    }

    @TargetApi(21)
    public FormItemEditText(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f4187a = null;
        this.f4188b = null;
        this.f4189c = null;
        this.f4190d = 0;
        this.f4191e = 24.0f;
        this.f4193g = 4.0f;
        this.f4194h = 8.0f;
        this.f4195i = 4;
        this.f4202p = new Rect();
        this.f4203q = false;
        this.f4205s = null;
        this.f4207u = 1.0f;
        this.f4208v = 2.0f;
        this.f4210x = false;
        this.f4211y = false;
        int[][] iArr = new int[4][];
        iArr[0] = new int[]{16842913};
        iArr[1] = new int[]{16842914};
        iArr[2] = new int[]{16842908};
        iArr[3] = new int[]{-16842908};
        this.f4184A = iArr;
        this.f4185B = new int[]{-16711936, -65536, -16777216, -7829368};
        this.f4186C = new ColorStateList(this.f4184A, this.f4185B);
        m6358a(context, attributeSet);
    }

    private int m6356a(int... iArr) {
        return this.f4186C.getColorForState(iArr, -7829368);
    }

    private void m6358a(Context context, AttributeSet attributeSet) {
        boolean z = true;
        this.f4207u = (float) m6366a(this.f4207u);
        this.f4208v = (float) m6366a(this.f4208v);
        this.f4191e = (float) m6366a(this.f4191e);
        this.f4194h = (float) m6366a(this.f4194h);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.FormItemEditText, 0, 0);
        try {
            TypedValue typedValue = new TypedValue();
            obtainStyledAttributes.getValue(R.FormItemEditText_pinAnimationType, typedValue);
            this.f4190d = typedValue.data;
            this.f4187a = obtainStyledAttributes.getString(R.FormItemEditText_pinCharacterMask);
            this.f4189c = obtainStyledAttributes.getString(R.FormItemEditText_pinRepeatedHint);
            this.f4207u = obtainStyledAttributes.getDimension(R.FormItemEditText_pinLineStroke, this.f4207u);
            this.f4208v = obtainStyledAttributes.getDimension(R.FormItemEditText_pinLineStrokeSelected, this.f4208v);
            this.f4206t = obtainStyledAttributes.getBoolean(R.FormItemEditText_pinLineStrokeCentered, false);
            this.f4192f = obtainStyledAttributes.getDimension(R.FormItemEditText_pinCharacterSize, 0.0f);
            this.f4191e = obtainStyledAttributes.getDimension(R.FormItemEditText_pinCharacterSpacing, this.f4191e);
            this.f4194h = obtainStyledAttributes.getDimension(R.FormItemEditText_pinTextBottomPadding, this.f4194h);
            this.f4203q = obtainStyledAttributes.getBoolean(R.FormItemEditText_pinBackgroundIsSquare, this.f4203q);
            this.f4201o = obtainStyledAttributes.getDrawable(R.FormItemEditText_pinBackgroundDrawable);
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(R.FormItemEditText_pinLineColors);
            if (colorStateList != null) {
                this.f4186C = colorStateList;
            }
            obtainStyledAttributes.recycle();
            this.f4198l = new Paint(getPaint());
            this.f4199m = new Paint(getPaint());
            this.f4200n = new Paint(getPaint());
            this.f4209w = new Paint(getPaint());
            this.f4209w.setStrokeWidth(this.f4207u);
            setFontSize(this.f4192f);
            TypedValue typedValue2 = new TypedValue();
            context.getTheme().resolveAttribute(R.colorControlActivated, typedValue2, true);
            this.f4185B[0] = typedValue2.data;
            this.f4185B[1] = -7829368;
            this.f4185B[2] = -7829368;
            setBackgroundResource(0);
            this.f4195i = attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLength", 4);
            this.f4193g = (float) this.f4195i;
            super.setOnClickListener(new C0644d(this));
            super.setOnLongClickListener(new C0645e(this));
            if ((getInputType() & 128) == 128 && TextUtils.isEmpty(this.f4187a)) {
                this.f4187a = "\u25cf";
            } else if ((getInputType() & 16) == 16 && TextUtils.isEmpty(this.f4187a)) {
                this.f4187a = "\u25cf";
            }
            if (!TextUtils.isEmpty(this.f4187a)) {
                this.f4188b = getMaskChars();
            }
            getPaint().getTextBounds(CLConstants.SALT_DELIMETER, 0, 1, this.f4202p);
            if (this.f4190d <= -1) {
                z = false;
            }
            this.f4210x = z;
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
        }
    }

    @TargetApi(11)
    private void m6359a(CharSequence charSequence) {
        this.f4199m.setAlpha(125);
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{125, 255});
        ofInt.setDuration(150);
        ofInt.addUpdateListener(new C0646f(this));
        AnimatorSet animatorSet = new AnimatorSet();
        if (charSequence.length() == this.f4195i && this.f4205s != null) {
            animatorSet.addListener(new C0647g(this));
        }
        animatorSet.playTogether(new Animator[]{ofInt});
        animatorSet.start();
    }

    @TargetApi(11)
    private void m6360a(CharSequence charSequence, int i) {
        this.f4197k[i] = this.f4196j[i].bottom - this.f4194h;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{this.f4197k[i] + getPaint().getTextSize(), this.f4197k[i]});
        ofFloat.setDuration(300);
        ofFloat.setInterpolator(new OvershootInterpolator());
        ofFloat.addUpdateListener(new C0648h(this, i));
        this.f4199m.setAlpha(255);
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{0, 255});
        ofInt.setDuration(300);
        ofInt.addUpdateListener(new C0649i(this));
        AnimatorSet animatorSet = new AnimatorSet();
        if (charSequence.length() == this.f4195i && this.f4205s != null) {
            animatorSet.addListener(new C0650j(this));
        }
        animatorSet.playTogether(new Animator[]{ofFloat, ofInt});
        animatorSet.start();
    }

    private void m6361a(boolean z, boolean z2) {
        if (this.f4211y) {
            this.f4209w.setColor(m6356a(16842914));
            return;
        }
        this.f4209w.setStrokeWidth(isFocused() ? this.f4208v : this.f4207u);
        if (z) {
            this.f4209w.setColor(m6356a(16842913));
        } else if (z2) {
            this.f4209w.setColor(isFocused() ? m6356a(16842918) : m6356a(-16842918));
        } else {
            this.f4209w.setColor(isFocused() ? m6356a(16842908) : m6356a(-16842908));
        }
    }

    private void m6363b(boolean z, boolean z2) {
        if (this.f4211y) {
            this.f4201o.setState(new int[]{16842914});
        } else if (isFocused()) {
            this.f4201o.setState(new int[]{16842908});
            if (z2) {
                this.f4201o.setState(new int[]{16842908, 16842913});
            } else if (z) {
                this.f4201o.setState(new int[]{16842908, 16842912});
            }
        } else {
            this.f4201o.setState(new int[]{-16842908});
        }
    }

    private CharSequence getFullText() {
        return this.f4187a == null ? getText() : getMaskChars();
    }

    private StringBuilder getMaskChars() {
        if (this.f4188b == null) {
            this.f4188b = new StringBuilder();
        }
        int length = getText().length();
        while (this.f4188b.length() != length) {
            if (this.f4188b.length() < length) {
                this.f4188b.append(this.f4187a);
            } else {
                this.f4188b.deleteCharAt(this.f4188b.length() - 1);
            }
        }
        return this.f4188b;
    }

    int m6366a(float f) {
        return (int) (((float) (getResources().getDisplayMetrics().densityDpi / 160)) * f);
    }

    protected void onDraw(Canvas canvas) {
        int i;
        CharSequence fullText = getFullText();
        int length = fullText.length();
        float[] fArr = new float[length];
        getPaint().getTextWidths(fullText, 0, length, fArr);
        float f = 0.0f;
        if (this.f4189c != null) {
            float[] fArr2 = new float[this.f4189c.length()];
            getPaint().getTextWidths(this.f4189c, fArr2);
            i = 0;
            while (i < fArr2.length) {
                float f2 = fArr2[i] + f;
                i++;
                f = f2;
            }
        }
        float f3 = f;
        i = 0;
        while (((float) i) < this.f4193g) {
            if (this.f4201o != null) {
                m6363b(i < length, i == length);
                this.f4201o.setBounds((int) this.f4196j[i].left, (int) this.f4196j[i].top, (int) this.f4196j[i].right, (int) this.f4196j[i].bottom);
                this.f4201o.draw(canvas);
            }
            f = this.f4196j[i].left + (this.f4192f / 2.0f);
            if (length > i) {
                if (this.f4210x && i == length - 1) {
                    canvas.drawText(fullText, i, i + 1, f - (fArr[i] / 2.0f), this.f4197k[i], this.f4199m);
                } else {
                    canvas.drawText(fullText, i, i + 1, f - (fArr[i] / 2.0f), this.f4197k[i], this.f4198l);
                }
            } else if (this.f4189c != null) {
                canvas.drawText(this.f4189c, f - (f3 / 2.0f), this.f4197k[i], this.f4200n);
            }
            if (this.f4201o == null) {
                m6361a(i < length, i == length);
                canvas.drawLine(this.f4196j[i].left, this.f4196j[i].top, this.f4196j[i].right, this.f4196j[i].bottom, this.f4209w);
            }
            i++;
        }
    }

    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        int width;
        super.onSizeChanged(i, i2, i3, i4);
        this.f4212z = getTextColors();
        if (this.f4212z != null) {
            this.f4199m.setColor(this.f4212z.getDefaultColor());
            this.f4198l.setColor(this.f4212z.getDefaultColor());
            this.f4200n.setColor(getCurrentHintTextColor());
        }
        int width2 = (getWidth() - af.m1196h(this)) - af.m1195g(this);
        if (this.f4191e < 0.0f) {
            this.f4192f = ((float) width2) / ((this.f4193g * 2.0f) - 1.0f);
        } else if (this.f4192f == 0.0f) {
            this.f4192f = (((float) width2) - (this.f4191e * (this.f4193g - 1.0f))) / this.f4193g;
        }
        this.f4196j = new RectF[((int) this.f4193g)];
        this.f4197k = new float[((int) this.f4193g)];
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if ((TextUtilsCompat.m729a(Locale.getDefault()) == 1 ? 1 : null) != null) {
            width2 = -1;
            width = (int) (((float) (getWidth() - af.m1195g(this))) - this.f4192f);
        } else {
            width2 = 1;
            width = af.m1195g(this);
        }
        int i5 = width;
        width = 0;
        while (((float) width) < this.f4193g) {
            this.f4196j[width] = new RectF((float) i5, (float) height, ((float) i5) + this.f4192f, (float) height);
            if (this.f4201o != null) {
                if (this.f4203q) {
                    this.f4196j[width].top = (float) getPaddingTop();
                    this.f4196j[width].right = ((float) i5) + this.f4196j[width].height();
                } else {
                    RectF rectF = this.f4196j[width];
                    rectF.top -= ((float) this.f4202p.height()) + (this.f4194h * 2.0f);
                }
            }
            i5 = this.f4191e < 0.0f ? (int) (((float) i5) + ((((float) width2) * this.f4192f) * 2.0f)) : (int) (((float) i5) + (((float) width2) * (this.f4192f + this.f4191e)));
            this.f4197k[width] = this.f4196j[width].bottom - this.f4194h;
            if (this.f4206t) {
                this.f4196j[width].top /= 2.0f;
                this.f4196j[width].bottom /= 2.0f;
            }
            width++;
        }
    }

    protected void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        setError(false);
        if (this.f4196j == null || !this.f4210x) {
            if (this.f4205s != null && charSequence.length() == this.f4195i) {
                this.f4205s.m6396a(charSequence);
            }
        } else if (this.f4190d == -1) {
            invalidate();
        } else if (i3 <= i2) {
        } else {
            if (this.f4190d == 0) {
                m6359a(charSequence);
            } else {
                m6360a(charSequence, i);
            }
        }
    }

    public void setAnimateText(boolean z) {
        this.f4210x = z;
    }

    public void setCharSize(float f) {
        this.f4192f = f;
        invalidate();
    }

    public void setColorStates(ColorStateList colorStateList) {
        this.f4186C = colorStateList;
        invalidate();
    }

    public void setCustomSelectionActionModeCallback(Callback callback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }

    public void setError(boolean z) {
        this.f4211y = z;
    }

    public void setFontSize(float f) {
        this.f4198l.setTextSize(f);
        this.f4199m.setTextSize(f);
        this.f4200n.setTextSize(f);
    }

    public void setLineStroke(float f) {
        this.f4207u = f;
        invalidate();
    }

    public void setLineStrokeCentered(boolean z) {
        this.f4206t = z;
        invalidate();
    }

    public void setLineStrokeSelected(float f) {
        this.f4208v = f;
        invalidate();
    }

    public void setMargin(int[] iArr) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) getLayoutParams();
        marginLayoutParams.setMargins(iArr[0], iArr[1], iArr[2], iArr[3]);
        setLayoutParams(marginLayoutParams);
    }

    public void setMaxLength(int i) {
        this.f4195i = i;
        this.f4193g = (float) i;
        setFilters(new InputFilter[]{new LengthFilter(i)});
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.f4204r = onClickListener;
    }

    public void setOnPinEnteredListener(C0651k c0651k) {
        this.f4205s = c0651k;
    }

    public void setSpace(float f) {
        this.f4191e = f;
        invalidate();
    }
}
