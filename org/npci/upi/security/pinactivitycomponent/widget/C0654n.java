package org.npci.upi.security.pinactivitycomponent.widget;

import android.support.v4.p006f.az;
import android.view.View;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.n */
class C0654n extends az {
    final /* synthetic */ boolean f4245a;
    final /* synthetic */ C0643b f4246b;

    C0654n(C0643b c0643b, boolean z) {
        this.f4246b = c0643b;
        this.f4245a = z;
    }

    public void m6397b(View view) {
        super.m1378b(view);
        view.setVisibility(this.f4245a ? 0 : 8);
    }
}
