package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.crashlytics.android.core.BuildConfig;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.b */
public class C0643b extends LinearLayout implements C0641c {
    private boolean f4217a;
    private String f4218b;
    private String f4219c;
    private int f4220d;
    private TextView f4221e;
    private FormItemEditText f4222f;
    private C0619o f4223g;
    private int f4224h;
    private Object f4225i;
    private LinearLayout f4226j;
    private Button f4227k;
    private ProgressBar f4228l;
    private ImageView f4229m;
    private String f4230n;
    private boolean f4231o;
    private boolean f4232p;
    private boolean f4233q;
    private RelativeLayout f4234r;

    public C0643b(Context context) {
        super(context);
        this.f4217a = false;
        this.f4230n = BuildConfig.FLAVOR;
        this.f4231o = false;
        m6389a(context, null);
    }

    public au m6387a(View view, boolean z) {
        float f = 0.0f;
        float f2 = 1.0f;
        au d = af.m1199k(view).m1363d(z ? 1.0f : 0.0f);
        if (z) {
            f = 1.0f;
        }
        d = d.m1361c(f).m1357a(new AccelerateInterpolator()).m1355a(new C0654n(this, z));
        if (!z) {
            f2 = 0.5f;
        }
        return d.m1353a(f2);
    }

    public void m6388a() {
        this.f4217a = true;
    }

    public void m6389a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.FormItemView);
        if (obtainStyledAttributes != null) {
            this.f4218b = obtainStyledAttributes.getString(R.FormItemView_formTitle);
            this.f4219c = obtainStyledAttributes.getString(R.FormItemView_formValidationError);
            this.f4220d = obtainStyledAttributes.getInteger(R.FormItemView_formInputLength, 6);
            this.f4232p = obtainStyledAttributes.getBoolean(R.FormItemView_formActionOnTop, false);
            obtainStyledAttributes.recycle();
        }
        C0643b.inflate(context, R.layout_form_item, this);
        this.f4234r = (RelativeLayout) findViewById(R.form_item_root);
        this.f4226j = (LinearLayout) findViewById(R.form_item_action_bar);
        this.f4221e = (TextView) findViewById(R.form_item_title);
        this.f4222f = (FormItemEditText) findViewById(R.form_item_input);
        this.f4227k = (Button) findViewById(R.form_item_button);
        this.f4228l = (ProgressBar) findViewById(R.form_item_progress);
        this.f4229m = (ImageView) findViewById(R.form_item_image);
        this.f4222f.setInputType(0);
        setTitle(this.f4218b);
        setInputLength(this.f4220d);
        this.f4222f.addTextChangedListener(new C0652l(this));
        this.f4222f.setOnTouchListener(new C0653m(this));
        setActionBarPositionTop(this.f4232p);
    }

    public void m6390a(Drawable drawable, boolean z) {
        if (drawable != null) {
            this.f4229m.setImageDrawable(drawable);
        }
        m6387a(this.f4229m, z);
    }

    public void m6391a(String str, Drawable drawable, OnClickListener onClickListener, int i, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str)) {
            this.f4227k.setText(str);
        }
        Button button = this.f4227k;
        Drawable drawable2 = i == 0 ? drawable : null;
        Drawable drawable3 = i == 1 ? drawable : null;
        Drawable drawable4 = i == 2 ? drawable : null;
        if (i != 3) {
            drawable = null;
        }
        button.setCompoundDrawablesWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable);
        this.f4227k.setOnClickListener(onClickListener);
        this.f4227k.setEnabled(z2);
        m6387a(this.f4227k, z);
    }

    public void m6392a(String str, OnClickListener onClickListener, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str)) {
            this.f4227k.setText(str);
        }
        m6387a(this.f4227k, z);
        this.f4227k.setEnabled(z2);
        this.f4227k.setOnClickListener(onClickListener);
    }

    public void m6393a(boolean z) {
        au a = m6387a(this.f4228l, z);
        a.m1357a(new AccelerateDecelerateInterpolator());
        a.m1362c();
    }

    public boolean m6394c() {
        if (this.f4231o) {
            this.f4231o = false;
            this.f4222f.setText(this.f4230n.replaceAll(".", "\u25cf"));
        } else {
            this.f4231o = true;
            setText(this.f4230n);
        }
        return this.f4231o;
    }

    public boolean m6395d() {
        this.f4222f.requestFocus();
        return true;
    }

    public Object getFormDataTag() {
        return this.f4225i;
    }

    public FormItemEditText getFormInputView() {
        return this.f4222f;
    }

    public C0619o getFormItemListener() {
        return this.f4223g;
    }

    public int getInputLength() {
        return this.f4220d;
    }

    public String getInputValue() {
        return (this.f4217a || this.f4231o) ? this.f4222f.getText().toString() : this.f4230n;
    }

    public void setActionBarPositionTop(boolean z) {
        this.f4233q = z;
        LayoutParams layoutParams = (LayoutParams) this.f4226j.getLayoutParams();
        if (this.f4233q) {
            layoutParams.addRule(10);
            layoutParams.addRule(8, 0);
        } else {
            layoutParams.addRule(10, 0);
            layoutParams.addRule(8, R.form_item_input);
        }
        this.f4226j.setLayoutParams(layoutParams);
    }

    public void setFormDataTag(Object obj) {
        this.f4225i = obj;
    }

    public void setFormItemListener(C0619o c0619o) {
        this.f4223g = c0619o;
    }

    public void setFormItemTag(int i) {
        this.f4224h = i;
    }

    public void setInputLength(int i) {
        this.f4222f.setMaxLength(i);
        this.f4220d = i;
    }

    public void setText(String str) {
        this.f4222f.setText(str);
        this.f4222f.setSelection(str.length());
    }

    public void setTitle(String str) {
        this.f4221e.setText(str);
        this.f4218b = str;
    }
}
