package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.j */
class C0650j implements AnimatorListener {
    final /* synthetic */ FormItemEditText f4242a;

    C0650j(FormItemEditText formItemEditText) {
        this.f4242a = formItemEditText;
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        this.f4242a.f4205s.m6396a(this.f4242a.getText());
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }
}
