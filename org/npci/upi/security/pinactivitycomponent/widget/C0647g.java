package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.g */
class C0647g implements AnimatorListener {
    final /* synthetic */ FormItemEditText f4238a;

    C0647g(FormItemEditText formItemEditText) {
        this.f4238a = formItemEditText;
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        this.f4238a.f4205s.m6396a(this.f4238a.getText());
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }
}
