package org.npci.upi.security.pinactivitycomponent.widget;

import android.view.View;
import android.view.View.OnLongClickListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.e */
class C0645e implements OnLongClickListener {
    final /* synthetic */ FormItemEditText f4236a;

    C0645e(FormItemEditText formItemEditText) {
        this.f4236a = formItemEditText;
    }

    public boolean onLongClick(View view) {
        this.f4236a.setSelection(this.f4236a.getText().length());
        return true;
    }
}
