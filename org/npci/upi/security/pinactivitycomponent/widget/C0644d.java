package org.npci.upi.security.pinactivitycomponent.widget;

import android.view.View;
import android.view.View.OnClickListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.d */
class C0644d implements OnClickListener {
    final /* synthetic */ FormItemEditText f4235a;

    C0644d(FormItemEditText formItemEditText) {
        this.f4235a = formItemEditText;
    }

    public void onClick(View view) {
        this.f4235a.setSelection(this.f4235a.getText().length());
        if (this.f4235a.f4204r != null) {
            this.f4235a.f4204r.onClick(view);
        }
    }
}
