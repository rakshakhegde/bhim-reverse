package org.npci.upi.security.pinactivitycomponent.widget;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.m */
class C0653m implements OnTouchListener {
    final /* synthetic */ C0643b f4244a;

    C0653m(C0643b c0643b) {
        this.f4244a = c0643b;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f4244a.f4223g != null && motionEvent.getAction() == 1) {
            this.f4244a.f4223g.m6283b(this.f4244a.f4224h);
        }
        return false;
    }
}
