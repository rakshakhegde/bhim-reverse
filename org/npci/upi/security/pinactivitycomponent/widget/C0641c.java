package org.npci.upi.security.pinactivitycomponent.widget;

import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.c */
public interface C0641c {
    void m6367a(String str, Drawable drawable, OnClickListener onClickListener, int i, boolean z, boolean z2);

    boolean m6368c();

    boolean m6369d();

    Object getFormDataTag();

    String getInputValue();

    void setText(String str);
}
