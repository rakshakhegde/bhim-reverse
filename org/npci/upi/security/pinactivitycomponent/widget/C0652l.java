package org.npci.upi.security.pinactivitycomponent.widget;

import android.text.Editable;
import android.text.TextWatcher;
import com.crashlytics.android.core.BuildConfig;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.l */
class C0652l implements TextWatcher {
    final /* synthetic */ C0643b f4243a;

    C0652l(C0643b c0643b) {
        this.f4243a = c0643b;
    }

    public void afterTextChanged(Editable editable) {
        if (this.f4243a.f4217a) {
            this.f4243a.f4230n = editable.toString();
        } else if (this.f4243a.f4231o) {
            this.f4243a.f4230n = editable.toString();
        } else if (editable.length() == 0) {
            this.f4243a.f4230n = BuildConfig.FLAVOR;
        } else if (this.f4243a.f4230n.length() > editable.length()) {
            this.f4243a.f4230n = this.f4243a.f4230n.substring(0, this.f4243a.f4230n.length() - 1);
        } else {
            char charAt = editable.toString().charAt(editable.length() - 1);
            if (charAt != '\u25cf') {
                this.f4243a.f4230n = this.f4243a.f4230n.concat(BuildConfig.FLAVOR + charAt);
                this.f4243a.f4222f.setText(this.f4243a.f4230n.replaceAll(".", "\u25cf"));
                return;
            }
            this.f4243a.f4222f.setSelection(editable.length());
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (this.f4243a.f4223g != null && this.f4243a.f4222f.getText() != null && this.f4243a.f4222f.getText().length() >= this.f4243a.f4220d) {
            this.f4243a.f4223g.m6281a(this.f4243a.f4224h, this.f4243a.f4222f.getText().toString());
        }
    }
}
