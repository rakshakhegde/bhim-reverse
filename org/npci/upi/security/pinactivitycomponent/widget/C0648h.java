package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.h */
class C0648h implements AnimatorUpdateListener {
    final /* synthetic */ int f4239a;
    final /* synthetic */ FormItemEditText f4240b;

    C0648h(FormItemEditText formItemEditText, int i) {
        this.f4240b = formItemEditText;
        this.f4239a = i;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4240b.f4197k[this.f4239a] = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.f4240b.invalidate();
    }
}
