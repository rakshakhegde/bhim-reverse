package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.widget.i */
class C0649i implements AnimatorUpdateListener {
    final /* synthetic */ FormItemEditText f4241a;

    C0649i(FormItemEditText formItemEditText) {
        this.f4241a = formItemEditText;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4241a.f4199m.setAlpha(((Integer) valueAnimator.getAnimatedValue()).intValue());
    }
}
