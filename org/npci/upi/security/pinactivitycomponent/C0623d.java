package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: org.npci.upi.security.pinactivitycomponent.d */
public class C0623d extends Exception {
    String f4130a;
    private String f4131b;
    private String f4132c;
    private Context f4133d;

    public C0623d(Context context, String str, String str2) {
        this.f4130a = "CLException";
        this.f4131b = str;
        this.f4132c = str2;
        this.f4133d = context;
        m6309a(context, str2);
    }

    public C0623d(Context context, String str, String str2, Throwable th) {
        super(th);
        this.f4130a = "CLException";
        this.f4131b = str;
        this.f4132c = str2;
        this.f4133d = context;
        m6309a(context, str2);
    }

    public String m6308a() {
        return this.f4132c;
    }

    public void m6309a(Context context, String str) {
        InputStream open;
        Properties properties = new Properties();
        try {
            open = context.getAssets().open("cl-messages_en_us.properties");
        } catch (IOException e) {
            Log.e(this.f4130a, e.getLocalizedMessage());
            open = null;
        }
        try {
            properties.load(open);
        } catch (IOException e2) {
            Log.e(this.f4130a, e2.getLocalizedMessage());
        }
        Log.e(this.f4130a, "ErrorMsg: " + properties.getProperty(str));
        CharSequence string = context.getResources().getString(R.error_msg);
        TextView textView = (TextView) ((Activity) context).findViewById(R.error_message);
        ((RelativeLayout) ((Activity) context).findViewById(R.error_layout)).setVisibility(0);
        textView.setText(string);
    }
}
