package org.npci.upi.security.pinactivitycomponent;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v4.p006f.af;

class al extends AnimatorListenerAdapter {
    final /* synthetic */ boolean f4101a;
    final /* synthetic */ int f4102b;
    final /* synthetic */ GetCredential f4103c;

    al(GetCredential getCredential, boolean z, int i) {
        this.f4103c = getCredential;
        this.f4101a = z;
        this.f4102b = i;
    }

    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        if (!this.f4101a) {
            this.f4103c.f4081y.setVisibility(8);
            this.f4103c.f4082z.setVisibility(8);
            this.f4103c.f4064A.resetTransition();
        }
    }

    public void onAnimationStart(Animator animator) {
        super.onAnimationStart(animator);
        if (this.f4101a) {
            this.f4103c.f4064A.startTransition(300);
            this.f4103c.f4081y.setVisibility(0);
            this.f4103c.f4082z.setVisibility(0);
            if (af.m1200l(this.f4103c.f4081y) == 0.0f) {
                this.f4103c.f4081y.setY((float) (this.f4102b * -1));
                return;
            }
            return;
        }
        this.f4103c.f4064A.reverseTransition(300);
    }
}
