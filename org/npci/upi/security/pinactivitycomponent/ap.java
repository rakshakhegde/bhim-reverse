package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ap {
    private final SharedPreferences f4116a;

    public ap(Context context) {
        this.f4116a = context.getSharedPreferences("NPCIPreferences", 0);
    }

    public void m6279a(String str, String str2) {
        Editor edit = this.f4116a.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public String m6280b(String str, String str2) {
        return this.f4116a.getString(str, str2);
    }
}
