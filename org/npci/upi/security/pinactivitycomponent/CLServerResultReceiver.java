package org.npci.upi.security.pinactivitycomponent;

import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ResultReceiver;
import org.npci.upi.security.services.C0660d;

public class CLServerResultReceiver extends ResultReceiver {
    private C0660d f4062a;

    public CLServerResultReceiver(C0660d c0660d) {
        super(new Handler());
        this.f4062a = c0660d;
    }

    protected void onReceiveResult(int i, Bundle bundle) {
        super.onReceiveResult(i, bundle);
        if (i == 2) {
            try {
                this.f4062a.m6411b(bundle);
                return;
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
        }
        this.f4062a.m6410a(bundle);
    }
}
