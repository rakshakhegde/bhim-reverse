package org.npci.upi.security.pinactivitycomponent;

import android.view.View;
import android.view.View.OnClickListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.e */
class C0624e implements OnClickListener {
    final /* synthetic */ Keypad f4134a;

    C0624e(Keypad keypad) {
        this.f4134a = keypad;
    }

    public void onClick(View view) {
        if (this.f4134a.f4087e != null) {
            this.f4134a.f4087e.m6266a(view, 66);
        }
    }
}
