package org.npci.upi.security.pinactivitycomponent;

/* renamed from: org.npci.upi.security.pinactivitycomponent.u */
public class C0638u {
    String f4163a;
    private String f4164b;
    private String f4165c;

    public C0638u(String str, String str2, String str3) {
        this.f4164b = str;
        this.f4163a = str2;
        this.f4165c = str3;
    }

    public String m6344a() {
        return this.f4164b;
    }

    public void m6345a(String str) {
        this.f4164b = str;
    }

    public String m6346b() {
        return this.f4163a;
    }

    public void m6347b(String str) {
        this.f4163a = str;
    }

    public String m6348c() {
        return this.f4165c;
    }

    public void m6349c(String str) {
        this.f4165c = str;
    }
}
