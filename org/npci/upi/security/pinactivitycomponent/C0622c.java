package org.npci.upi.security.pinactivitycomponent;

import android.view.View;
import android.view.View.OnClickListener;

/* renamed from: org.npci.upi.security.pinactivitycomponent.c */
class C0622c implements OnClickListener {
    final /* synthetic */ Keypad f4129a;

    C0622c(Keypad keypad) {
        this.f4129a = keypad;
    }

    public void onClick(View view) {
        if (this.f4129a.f4087e != null) {
            this.f4129a.f4087e.m6266a(view, 7);
        }
    }
}
