package org.npci.upi.security.pinactivitycomponent;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import org.npci.upi.security.services.C0657b;
import org.npci.upi.security.services.C0660d;

public class CLRemoteServiceImpl extends Service {
    private C0657b f4060a;
    private C0659z f4061b;

    public CLRemoteServiceImpl() {
        this.f4060a = null;
        this.f4061b = null;
    }

    private Bundle m6213a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, C0660d c0660d) {
        Bundle bundle = new Bundle();
        bundle.putString(CLConstants.INPUT_KEY_KEY_CODE, str);
        bundle.putString(CLConstants.INPUT_KEY_XML_PAYLOAD, str2);
        bundle.putString(CLConstants.INPUT_KEY_CONTROLS, str3);
        bundle.putString(CLConstants.INPUT_KEY_CONFIGURATION, str4);
        bundle.putString(CLConstants.INPUT_KEY_SALT, str5);
        bundle.putString(CLConstants.INPUT_KEY_PAY_INFO, str6);
        bundle.putString(CLConstants.INPUT_KEY_TRUST, str7);
        bundle.putString(CLConstants.INPUT_KEY_LANGUAGE_PREFERENCE, str8);
        an.m6270a(new CLServerResultReceiver(c0660d));
        return bundle;
    }

    public IBinder onBind(Intent intent) {
        if (this.f4060a == null) {
            this.f4060a = new C0658y(this, getBaseContext(), null);
        }
        try {
            this.f4061b = new C0659z(getBaseContext());
            return this.f4060a;
        } catch (Exception e) {
            throw new RuntimeException("Could not initialize service provider");
        }
    }
}
