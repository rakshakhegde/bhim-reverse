package org.npci.upi.security.pinactivitycomponent;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

class ak implements OnTouchListener {
    final /* synthetic */ GetCredential f4100a;

    ak(GetCredential getCredential) {
        this.f4100a = getCredential;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 1 || !this.f4100a.m6232t()) {
            return false;
        }
        this.f4100a.m6221b(false);
        return true;
    }
}
