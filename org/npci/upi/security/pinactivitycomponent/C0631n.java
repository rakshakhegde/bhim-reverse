package org.npci.upi.security.pinactivitycomponent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import org.npci.upi.security.pinactivitycomponent.widget.C0643b;

/* renamed from: org.npci.upi.security.pinactivitycomponent.n */
class C0631n implements OnClickListener {
    final /* synthetic */ C0643b f4144a;
    final /* synthetic */ C0630m f4145b;

    C0631n(C0630m c0630m, C0643b c0643b) {
        this.f4145b = c0630m;
        this.f4144a = c0643b;
    }

    public void onClick(View view) {
        this.f4145b.f4143a.m6294a(this.f4144a);
        Bundle bundle = new Bundle();
        bundle.putString(CLConstants.OUTPUT_KEY_ACTION, "TRIGGER_OTP");
        ((GetCredential) this.f4145b.f4143a.ao).m6245o().m6355d().send(2, bundle);
    }
}
