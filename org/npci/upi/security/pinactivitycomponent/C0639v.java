package org.npci.upi.security.pinactivitycomponent;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import org.npci.upi.security.pinactivitycomponent.widget.C0641c;

/* renamed from: org.npci.upi.security.pinactivitycomponent.v */
class C0639v implements OnClickListener {
    final /* synthetic */ C0641c f4166a;
    final /* synthetic */ String f4167b;
    final /* synthetic */ String f4168c;
    final /* synthetic */ Drawable f4169d;
    final /* synthetic */ Drawable f4170e;
    final /* synthetic */ C0621b f4171f;

    C0639v(C0621b c0621b, C0641c c0641c, String str, String str2, Drawable drawable, Drawable drawable2) {
        this.f4171f = c0621b;
        this.f4166a = c0641c;
        this.f4167b = str;
        this.f4168c = str2;
        this.f4169d = drawable;
        this.f4170e = drawable2;
    }

    public void onClick(View view) {
        boolean c = this.f4166a.m6368c();
        this.f4166a.m6367a(c ? this.f4167b : this.f4168c, c ? this.f4169d : this.f4170e, this, 0, true, true);
    }
}
