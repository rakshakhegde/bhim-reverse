package org.npci.upi.security.pinactivitycomponent;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import org.npci.upi.security.pinactivitycomponent.widget.C0641c;

/* renamed from: org.npci.upi.security.pinactivitycomponent.s */
class C0636s implements OnClickListener {
    final /* synthetic */ C0641c f4155a;
    final /* synthetic */ String f4156b;
    final /* synthetic */ String f4157c;
    final /* synthetic */ Drawable f4158d;
    final /* synthetic */ Drawable f4159e;
    final /* synthetic */ C0635r f4160f;

    C0636s(C0635r c0635r, C0641c c0641c, String str, String str2, Drawable drawable, Drawable drawable2) {
        this.f4160f = c0635r;
        this.f4155a = c0641c;
        this.f4156b = str;
        this.f4157c = str2;
        this.f4158d = drawable;
        this.f4159e = drawable2;
    }

    public void onClick(View view) {
        boolean c = this.f4155a.m6368c();
        this.f4155a.m6367a(c ? this.f4156b : this.f4157c, c ? this.f4158d : this.f4159e, this, 0, true, true);
    }
}
