package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.os.ResultReceiver;
import in.org.npci.commonlibrary.C0568e;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

/* renamed from: org.npci.upi.security.pinactivitycomponent.w */
public class C0640w {
    private Map f4172a;
    private Context f4173b;
    private Properties f4174c;
    private Properties f4175d;
    private Properties f4176e;
    private C0637t f4177f;
    private ac f4178g;
    private Locale f4179h;
    private C0568e f4180i;
    private Activity f4181j;
    private an f4182k;
    private ab f4183l;

    public C0640w(Context context, an anVar, Activity activity) {
        this.f4172a = new HashMap();
        this.f4182k = anVar;
        this.f4179h = anVar.m6273b();
        this.f4173b = context;
        this.f4180i = anVar.m6274c();
        this.f4181j = activity;
        this.f4176e = m6350a(CLConstants.CL_PROPERTIES);
        this.f4174c = m6350a(CLConstants.VALIDATION_PROPERTIES);
        this.f4175d = m6350a(CLConstants.VERSION_PROPERTIES);
        if (this.f4179h != null) {
            this.f4172a.put(this.f4179h.getLanguage(), m6350a("cl-messages_" + this.f4179h.getLanguage() + ".properties"));
        } else {
            Locale locale = new Locale(CLConstants.DEFAULT_LANGUAGE_PREFERENCE);
            this.f4172a.put(locale.getLanguage(), m6350a("cl-messages_" + locale.getLanguage() + ".properties"));
        }
        this.f4183l = anVar.m6275d();
        this.f4177f = new C0637t(this);
        if (anVar != null && anVar.m6274c() != null && anVar.m6271a() != null) {
            this.f4178g = new ac(this.f4180i, this.f4183l, anVar.m6271a());
        }
    }

    public Properties m6350a(String str) {
        Properties properties = new Properties();
        try {
            properties.load(this.f4173b.getAssets().open(str));
        } catch (IOException e) {
            C0625g.m6311a("AssetsPropertyReader", e.toString());
        }
        return properties;
    }

    public C0637t m6351a() {
        return this.f4177f;
    }

    public String m6352b(String str) {
        return this.f4175d != null ? this.f4175d.getProperty(str) : null;
    }

    public ac m6353b() {
        String str = null;
        if (this.f4178g == null && this.f4182k != null) {
            this.f4180i = this.f4182k.m6274c();
            this.f4178g = new ac(this.f4182k.m6274c(), this.f4182k.m6275d(), this.f4182k.m6271a());
        }
        C0625g.m6312b("Common Library", "get Encryptor");
        C0625g.m6312b("Common Library", "Input Analyzer :" + this.f4182k);
        C0625g.m6312b("Common Library", new StringBuilder().append("Input Analyzer Key Code:").append(this.f4182k).toString() == null ? null : this.f4182k.m6271a());
        String str2 = "Common Library";
        if (("Input Analyzer Common Library:" + this.f4182k) != null) {
            str = this.f4182k.m6274c().toString();
        }
        C0625g.m6312b(str2, str);
        return this.f4178g;
    }

    public Activity m6354c() {
        return this.f4181j;
    }

    public ResultReceiver m6355d() {
        return this.f4182k == null ? null : this.f4182k.m6276e();
    }
}
