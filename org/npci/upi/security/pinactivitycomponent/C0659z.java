package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.util.Base64;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.commonlibrary.C0575l;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;

/* renamed from: org.npci.upi.security.pinactivitycomponent.z */
public class C0659z {
    private Context f4249a;
    private String f4250b;
    private ab f4251c;
    private aa f4252d;
    private String f4253e;

    public C0659z(Context context) {
        this.f4250b = BuildConfig.FLAVOR;
        this.f4251c = null;
        this.f4253e = BuildConfig.FLAVOR;
        try {
            this.f4249a = context;
            this.f4251c = new ab(context);
            this.f4252d = new aa();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean m6405b(String str, String str2, String str3, String str4) {
        try {
            String b = this.f4251c.m6260b();
            C0625g.m6312b("Token in CL", b);
            b = Base64.encodeToString(this.f4252d.m6255b(Base64.decode(str, 0), this.f4252d.m6254b(b)), 0);
            String str5 = str2 + CLConstants.SALT_DELIMETER + str3 + CLConstants.SALT_DELIMETER + str4;
            C0625g.m6312b("CL Hmac Msg", str5);
            str5 = Base64.encodeToString(this.f4252d.m6252a(str5), 0);
            C0625g.m6312b("CL Hash", str5);
            return str5.equalsIgnoreCase(b);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalBlockSizeException e2) {
            e2.printStackTrace();
            return false;
        } catch (BadPaddingException e3) {
            e3.printStackTrace();
            return false;
        } catch (InvalidAlgorithmParameterException e4) {
            e4.printStackTrace();
            return false;
        } catch (InvalidKeyException e5) {
            e5.printStackTrace();
            return false;
        } catch (UnsupportedEncodingException e6) {
            e6.printStackTrace();
            return false;
        } catch (Exception e7) {
            e7.printStackTrace();
            return false;
        }
    }

    public String m6406a() {
        String str = BuildConfig.FLAVOR;
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(256);
            return this.f4252d.m6251a(instance.generateKey().getEncoded());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String m6407a(String str, String str2) {
        String a;
        Exception e;
        String str3 = BuildConfig.FLAVOR;
        String str4 = BuildConfig.FLAVOR;
        String format = new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis()));
        try {
            this.f4250b = m6409b();
            str3 = m6406a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            aa aaVar = new aa();
            if (str.equalsIgnoreCase(CLConstants.MODE_INITIAL)) {
                this.f4251c.m6261c();
                if (this.f4251c.m6257a().size() > 0) {
                    this.f4251c.m6259b(new C0638u(str3, this.f4250b, format));
                } else {
                    this.f4251c.m6258a(new C0638u(str3, this.f4250b, format));
                }
                a = aaVar.m6250a(this.f4250b + CLConstants.SALT_DELIMETER + str3 + CLConstants.SALT_DELIMETER + str2, C0575l.m5164a());
                try {
                    C0625g.m6312b("K0 in Challenge", str3);
                    C0625g.m6312b("token in Challenge", this.f4250b);
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    return a;
                }
                return a;
            }
            a = ((C0638u) this.f4251c.m6257a().get(0)).m6344a();
            str4 = this.f4250b + CLConstants.SALT_DELIMETER + str3 + CLConstants.SALT_DELIMETER + str2;
            C0625g.m6312b("K0 in Challenge", str3);
            C0625g.m6312b("token in Challenge", this.f4250b);
            a = Base64.encodeToString(aaVar.m6253a(str4.getBytes(), aaVar.m6254b(a)), 0);
            this.f4251c.m6261c();
            this.f4251c.m6258a(new C0638u(str3, this.f4250b, format));
            return a;
        } catch (Exception e22) {
            Exception exception = e22;
            a = str4;
            e = exception;
            e.printStackTrace();
            return a;
        }
    }

    public boolean m6408a(String str, String str2, String str3, String str4) {
        C0625g.m6312b("hmac", str4);
        return m6405b(str4, str, str2, str3);
    }

    public String m6409b() {
        String str = BuildConfig.FLAVOR;
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(256);
            return this.f4252d.m6251a(instance.generateKey().getEncoded());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
