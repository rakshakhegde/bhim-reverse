package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.p004a.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class Keypad extends TableLayout {
    private int f4083a;
    private int f4084b;
    private int f4085c;
    private float f4086d;
    private C0618f f4087e;

    public Keypad(Context context) {
        this(context, null);
    }

    public Keypad(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4083a = 61;
        m6249a(attributeSet);
        m6248a();
    }

    private int m6246a(float f) {
        return (int) (((float) (getResources().getDisplayMetrics().densityDpi / 160)) * f);
    }

    private void m6248a() {
        setBackgroundColor(this.f4084b);
        LayoutParams layoutParams = new TableLayout.LayoutParams(-1, 0, 1.0f);
        int i = 0;
        boolean z = true;
        while (i < 3) {
            View tableRow = new TableRow(getContext());
            tableRow.setLayoutParams(layoutParams);
            tableRow.setWeightSum(3.0f);
            boolean z2 = z;
            for (int i2 = 0; i2 < 3; i2++) {
                View textView = new TextView(getContext());
                textView.setGravity(17);
                textView.setLayoutParams(getItemParams());
                textView.setTextColor(this.f4085c);
                textView.setTextSize(2, this.f4086d);
                textView.setText(String.valueOf(z2));
                textView.setClickable(true);
                setClickFeedback(textView);
                textView.setOnClickListener(new aq(this, z2));
                tableRow.addView(textView);
                z2++;
            }
            addView(tableRow);
            i++;
            z = z2;
        }
        View imageView = new ImageView(getContext());
        imageView.setImageResource(R.ic_action_backspace);
        imageView.setLayoutParams(getItemParams());
        imageView.setClickable(true);
        setClickFeedback(imageView);
        imageView.setOnClickListener(new ar(this));
        View textView2 = new TextView(getContext());
        textView2.setLayoutParams(getItemParams());
        textView2.setGravity(17);
        textView2.setText(String.valueOf(0));
        textView2.setTextColor(this.f4085c);
        textView2.setTextSize(2, this.f4086d);
        textView2.setClickable(true);
        setClickFeedback(textView2);
        textView2.setOnClickListener(new C0622c(this));
        View imageView2 = new ImageView(getContext());
        imageView2.setImageResource(R.ic_action_submit);
        imageView2.setScaleType(ScaleType.CENTER_INSIDE);
        imageView2.setAdjustViewBounds(true);
        LayoutParams itemParams = getItemParams();
        itemParams.height = (int) (((float) m6246a((float) this.f4083a)) * 1.25f);
        imageView2.setLayoutParams(itemParams);
        imageView2.setClickable(true);
        setClickFeedback(imageView2);
        imageView2.setOnClickListener(new C0624e(this));
        View tableRow2 = new TableRow(getContext());
        tableRow2.setLayoutParams(layoutParams);
        tableRow2.setWeightSum(3.0f);
        tableRow2.addView(imageView);
        tableRow2.addView(textView2);
        tableRow2.addView(imageView2);
        addView(tableRow2);
    }

    private void m6249a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.Keypad, 0, 0);
        this.f4084b = obtainStyledAttributes.getColor(R.Keypad_keypad_bg_color, ContextCompat.m81c(getContext(), R.npci_keypad_bg_color));
        this.f4085c = obtainStyledAttributes.getColor(R.Keypad_key_digit_color, ContextCompat.m81c(getContext(), R.npci_key_digit_color));
        this.f4086d = (float) obtainStyledAttributes.getDimensionPixelSize(R.Keypad_key_digit_size, 36);
        this.f4083a = obtainStyledAttributes.getDimensionPixelSize(R.Keypad_key_digit_height, this.f4083a);
        obtainStyledAttributes.recycle();
    }

    private TableRow.LayoutParams getItemParams() {
        return new TableRow.LayoutParams(0, m6246a((float) this.f4083a), 1.0f);
    }

    private void setClickFeedback(View view) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(R.selectableItemBackground, typedValue, true);
        view.setBackgroundResource(typedValue.resourceId);
    }

    public void setOnKeyPressCallback(C0618f c0618f) {
        this.f4087e = c0618f;
    }
}
