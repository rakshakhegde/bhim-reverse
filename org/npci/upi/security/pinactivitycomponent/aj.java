package org.npci.upi.security.pinactivitycomponent;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import org.npci.upi.security.pinactivitycomponent.R.R;

class aj implements OnTouchListener {
    final /* synthetic */ GetCredential f4099a;

    aj(GetCredential getCredential) {
        this.f4099a = getCredential;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() != R.transaction_details_scroller || motionEvent.getAction() != 1 || !this.f4099a.m6232t()) {
            return false;
        }
        this.f4099a.m6221b(false);
        return true;
    }
}
