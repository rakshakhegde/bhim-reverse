package org.npci.upi.security.pinactivitycomponent;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.p004a.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.commonlibrary.Message;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.npci.upi.security.pinactivitycomponent.widget.C0619o;
import org.npci.upi.security.pinactivitycomponent.widget.C0641c;
import org.npci.upi.security.pinactivitycomponent.widget.C0642a;
import org.npci.upi.security.pinactivitycomponent.widget.C0643b;

/* renamed from: org.npci.upi.security.pinactivitycomponent.b */
public class C0621b extends C0620h implements C0619o {
    private static final String ap;
    private HashMap aq;
    private int ar;
    private boolean as;
    private ViewSwitcher at;

    static {
        ap = C0621b.class.getSimpleName();
    }

    public C0621b() {
        this.aq = new HashMap();
        this.ar = 0;
        this.as = false;
        this.at = null;
    }

    private void m6299M() {
        C0643b c0643b;
        String inputValue;
        int i = 0;
        if (this.g != -1 && (this.f.get(this.g) instanceof C0643b)) {
            c0643b = (C0643b) this.f.get(this.g);
            inputValue = c0643b.getInputValue();
            if (inputValue == null || inputValue.length() != c0643b.getInputLength()) {
                m6295b(c0643b, m139a(R.invalid_otp));
                return;
            }
        }
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (this.f.get(i2) instanceof C0643b) {
                c0643b = (C0643b) this.f.get(i2);
                if (c0643b.getInputValue().length() != c0643b.getInputLength()) {
                    m6295b(c0643b, m139a(R.componentMessage));
                    return;
                }
            }
        }
        if (!this.as) {
            this.as = true;
            while (i < this.f.size()) {
                try {
                    JSONObject jSONObject = (JSONObject) ((C0641c) this.f.get(i)).getFormDataTag();
                    inputValue = jSONObject.getString(CLConstants.FIELD_TYPE);
                    String string = jSONObject.getString(CLConstants.FIELD_SUBTYPE);
                    this.b.put(CLConstants.SALT_FIELD_CREDENTIAL, ((C0641c) this.f.get(i)).getInputValue());
                    Message a = ((GetCredential) this.ao).m6245o().m6353b().m6263a(((GetCredential) this.ao).m6245o().m6351a().m6343a(this.b), inputValue, string, this.b);
                    if (a != null) {
                        this.aq.put(string, ao.m6277a(a));
                    }
                } catch (Exception e) {
                    C0625g.m6310a(ap, e);
                }
                i++;
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable("credBlocks", this.aq);
            ((GetCredential) this.ao).m6245o().m6355d().send(1, bundle);
            ((GetCredential) this.ao).m6245o().m6354c().finish();
        }
    }

    private void m6300N() {
        if (this.g != -1 && (this.f.get(this.g) instanceof C0643b)) {
            C0643b c0643b = (C0643b) this.f.get(this.g);
            m6294a(c0643b);
            c0643b.m6388a();
        }
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            if (i != this.g) {
                C0641c c0641c = (C0641c) this.f.get(i);
                Drawable a = ContextCompat.m77a(m176h(), R.ic_visibility_on);
                Drawable a2 = ContextCompat.m77a(m176h(), R.ic_visibility_off);
                String a3 = m139a(R.action_hide);
                String a4 = m139a(R.action_show);
                c0641c.m6367a(a4, a, new C0639v(this, c0641c, a3, a4, a2, a), 0, true, true);
            }
        }
    }

    private void m6301a(View view) {
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.switcherLayout1);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.switcherLayout2);
        this.at = (ViewSwitcher) view.findViewById(R.view_switcher);
        if (this.c != null) {
            for (int i = 0; i < this.c.length(); i++) {
                try {
                    JSONObject jSONObject = this.c.getJSONObject(i);
                    String string = jSONObject.getString(CLConstants.FIELD_SUBTYPE);
                    int optInt = jSONObject.optInt(CLConstants.FIELD_DLENGTH) == 0 ? 6 : jSONObject.optInt(CLConstants.FIELD_DLENGTH);
                    View c0642a;
                    if (string.equals(CLConstants.CREDTYPE_MPIN)) {
                        C0643b a = m6288a(m139a(R.npci_set_mpin_title), i, optInt);
                        C0643b a2 = m6288a(m139a(R.npci_confirm_mpin_title), i, optInt);
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(a);
                        arrayList.add(a2);
                        c0642a = new C0642a(m176h());
                        c0642a.m6374a(arrayList, (C0619o) this);
                        c0642a.setFormDataTag(jSONObject);
                        this.f.add(c0642a);
                        linearLayout2.addView(c0642a);
                    } else {
                        String str = BuildConfig.FLAVOR;
                        if (string.equals(CLConstants.CREDTYPE_ATMPIN)) {
                            str = m139a(R.npci_atm_title);
                        } else if (CLConstants.CREDTYPE_OTP.equals(string) || CLConstants.CREDTYPE_SMS.equals(string) || CLConstants.CREDTYPE_EMAIL.equals(string) || CLConstants.CREDTYPE_HOTP.equals(string) || CLConstants.CREDTYPE_TOTP.equals(string)) {
                            string = m139a(R.npci_otp_title);
                            this.g = i;
                            if ((m176h() instanceof GetCredential) && ((GetCredential) m176h()).m6244n()) {
                                m6297c(optInt);
                            }
                            str = string;
                        }
                        c0642a = m6288a(str, i, optInt);
                        c0642a.setFormDataTag(jSONObject);
                        this.f.add(c0642a);
                        linearLayout.addView(c0642a);
                    }
                } catch (Throwable e) {
                    Log.e(ap, "Error while inflating Layout", e);
                }
            }
        }
    }

    public View m6302a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.fragment_atmpin, viewGroup, false);
    }

    public void m6303a() {
        if (this.ar == 0) {
            ((C0641c) this.f.get(this.ar + 1)).m6369d();
            this.ar++;
            return;
        }
        if (this.ar == 1) {
            if (((C0643b) this.f.get(0)).getInputLength() != ((C0641c) this.f.get(0)).getInputValue().length()) {
                m6295b((View) this.f.get(0), m139a(R.npci_otp_title));
                return;
            } else if (((C0643b) this.f.get(1)).getInputLength() != ((C0641c) this.f.get(1)).getInputValue().length()) {
                m6295b((View) this.f.get(1), m139a(R.npci_atm_title));
                return;
            } else if (this.at != null) {
                this.at.showNext();
                this.ar = 2;
                return;
            }
        }
        if (this.ar != 2) {
            m6299M();
        } else if (((C0641c) this.f.get(this.ar)).m6369d()) {
            m6299M();
        }
    }

    public void m6304a(int i, String str) {
        if (this.g != -1 && this.g == i && (this.f.get(this.g) instanceof C0643b)) {
            m6292a(this.i);
            ((C0643b) this.f.get(this.g)).m6393a(false);
            ((C0643b) this.f.get(this.g)).m6392a(BuildConfig.FLAVOR, null, false, false);
            ((C0643b) this.f.get(this.g)).m6390a(ContextCompat.m77a(m176h(), R.ic_tick_ok), true);
        }
    }

    public void m6305a(View view, Bundle bundle) {
        super.m6291a(view, bundle);
        m6285K();
        m6301a(view);
        m6300N();
    }

    public void m6306a(View view, String str) {
        m6295b(view, str);
    }

    public void m6307b(int i) {
        if (!(this.f.get(i) instanceof C0642a)) {
            this.ar = i;
        }
    }
}
