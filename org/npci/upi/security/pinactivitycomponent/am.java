package org.npci.upi.security.pinactivitycomponent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import java.util.Date;

class am extends BroadcastReceiver {
    final /* synthetic */ GetCredential f4104a;

    private am(GetCredential getCredential) {
        this.f4104a = getCredential;
    }

    private void m6268a(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Object[] objArr = (Object[]) extras.get("pdus");
            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
            for (int i = 0; i < smsMessageArr.length; i++) {
                smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                String toUpperCase = smsMessageArr[i].getOriginatingAddress().toUpperCase();
                String toUpperCase2 = smsMessageArr[i].getMessageBody().toUpperCase();
                Date date = new Date(smsMessageArr[i].getTimestampMillis());
                C0633p a = new C0634q(this.f4104a.f4079w).m6329a(this.f4104a.f4068E, toUpperCase, toUpperCase2);
                if (a != null) {
                    this.f4104a.f4078v.m6296b(a);
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                m6268a(intent);
            }
        } catch (Exception e) {
            C0625g.m6310a(GetCredential.f4063m, e);
        }
    }
}
