package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.os.Bundle;
import android.os.ResultReceiver;
import in.org.npci.commonlibrary.C0568e;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class an {
    private static ResultReceiver f4105k;
    private String f4106a;
    private String f4107b;
    private JSONObject f4108c;
    private JSONObject f4109d;
    private JSONObject f4110e;
    private JSONArray f4111f;
    private Locale f4112g;
    private C0568e f4113h;
    private String f4114i;
    private ab f4115j;

    private void m6269a(Context context) {
        String optString = this.f4110e.optString(CLConstants.SALT_FIELD_TXN_ID);
        String optString2 = this.f4110e.optString(CLConstants.SALT_FIELD_TXN_AMOUNT);
        String optString3 = this.f4110e.optString(CLConstants.SALT_FIELD_APP_ID);
        String optString4 = this.f4110e.optString(CLConstants.SALT_FIELD_DEVICE_ID);
        String optString5 = this.f4110e.optString(CLConstants.SALT_FIELD_MOBILE_NUMBER);
        String optString6 = this.f4110e.optString(CLConstants.SALT_FIELD_PAYER_ADDR);
        String optString7 = this.f4110e.optString(CLConstants.SALT_FIELD_PAYEE_ADDR);
        try {
            StringBuilder stringBuilder = new StringBuilder(100);
            if (!(optString2 == null || optString2.isEmpty())) {
                stringBuilder.append(optString2).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString == null || optString.isEmpty())) {
                stringBuilder.append(optString).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString6 == null || optString6.isEmpty())) {
                stringBuilder.append(optString6).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString7 == null || optString7.isEmpty())) {
                stringBuilder.append(optString7).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString3 == null || optString3.isEmpty())) {
                stringBuilder.append(optString3).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString5 == null || optString5.isEmpty())) {
                stringBuilder.append(optString5).append(CLConstants.SALT_DELIMETER);
            }
            if (!(optString4 == null || optString4.isEmpty())) {
                stringBuilder.append(optString4);
            }
            int lastIndexOf = stringBuilder.lastIndexOf(CLConstants.SALT_DELIMETER);
            if (lastIndexOf != -1 && lastIndexOf == stringBuilder.length() - 1) {
                stringBuilder.deleteCharAt(lastIndexOf);
            }
            optString = this.f4115j.m6260b();
            C0625g.m6312b("CL Trust Token", optString);
            C0625g.m6312b("CL Trust Param Message", stringBuilder.toString());
            this.f4113h.m5149a(this.f4114i, stringBuilder.toString(), optString);
        } catch (Exception e) {
            e.printStackTrace();
            throw new C0623d(context, CLConstants.ERROR_KEY_TRUST_NOT_VALID, CLConstants.ERROR_MSG_KEY_TRUST_NOT_VALID);
        }
    }

    public static void m6270a(CLServerResultReceiver cLServerResultReceiver) {
        f4105k = cLServerResultReceiver;
    }

    public String m6271a() {
        return this.f4106a;
    }

    public void m6272a(Bundle bundle, Context context) {
        this.f4115j = new ab(context);
        try {
            this.f4106a = bundle.getString(CLConstants.INPUT_KEY_KEY_CODE);
            if (this.f4106a == null || this.f4106a.isEmpty()) {
                throw new C0623d(context, CLConstants.ERROR_KEY_CODE_MISSING, CLConstants.ERROR_MSG_KEY_CODE_MISSING);
            }
            C0625g.m6312b("Common Library", this.f4106a);
            try {
                this.f4107b = bundle.getString(CLConstants.INPUT_KEY_XML_PAYLOAD);
                if (this.f4107b == null || this.f4107b.isEmpty()) {
                    throw new C0623d(context, CLConstants.ERROR_KEY_XML_PAYLOAD_MISSING, CLConstants.ERROR_MSG_KEY_XML_PAYLOAD_MISSING);
                }
                C0625g.m6312b("Common Library", this.f4107b);
                this.f4113h = new C0568e(this.f4107b);
                try {
                    String string = bundle.getString(CLConstants.INPUT_KEY_CONTROLS);
                    if (string == null || string.isEmpty()) {
                        C0625g.m6312b("Common Library", "Controls is not received. Setting MPIN as default. ");
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(CLConstants.FIELD_TYPE, "PIN");
                        jSONObject.put(CLConstants.FIELD_SUBTYPE, CLConstants.CREDTYPE_MPIN);
                        jSONObject.put(CLConstants.FIELD_DTYPE, "NUM|ALPH");
                        jSONObject.put(CLConstants.FIELD_DLENGTH, 6);
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject);
                        this.f4108c = new JSONObject();
                        this.f4108c.put(CLConstants.FIELD_CRED_ALLOWED, jSONArray);
                    } else {
                        C0625g.m6312b("Common Library", "Controls received: " + string);
                        this.f4108c = new JSONObject(string);
                    }
                    try {
                        string = bundle.getString(CLConstants.INPUT_KEY_CONFIGURATION);
                        if (string == null || string.isEmpty()) {
                            C0625g.m6312b("Common Library", "Configuration is not received");
                        } else {
                            C0625g.m6312b("Common Library", "Configuration received: " + string);
                            this.f4109d = new JSONObject(string);
                        }
                        try {
                            string = bundle.getString(CLConstants.INPUT_KEY_SALT);
                            if (string == null || string.isEmpty()) {
                                throw new C0623d(context, CLConstants.ERROR_SALT_MISSING, CLConstants.ERROR_MSG_SALT_MISSING);
                            }
                            C0625g.m6312b("Common Library", string);
                            this.f4110e = new JSONObject(string);
                            try {
                                this.f4114i = bundle.getString(CLConstants.INPUT_KEY_TRUST);
                                if (this.f4114i == null || this.f4114i.isEmpty()) {
                                    throw new C0623d(context, CLConstants.ERROR_KEY_TRUST_MISSING, CLConstants.ERROR_MSG_KEY_TRUST_MISSING);
                                }
                                C0625g.m6312b("Common Library", this.f4114i);
                                m6269a(context);
                                try {
                                    string = bundle.getString(CLConstants.INPUT_KEY_PAY_INFO);
                                    if (string == null || string.isEmpty()) {
                                        C0625g.m6312b("Common Library", "Pay Info not received");
                                    } else {
                                        C0625g.m6312b("Common Library", "Pay Info Received" + string);
                                        this.f4111f = new JSONArray(string);
                                    }
                                    try {
                                        String string2 = bundle.getString(CLConstants.INPUT_KEY_LANGUAGE_PREFERENCE);
                                        string = (string2 == null || string2.isEmpty()) ? CLConstants.DEFAULT_LANGUAGE_PREFERENCE : string2;
                                        this.f4112g = new Locale(string);
                                        C0625g.m6312b("Common Library", string2);
                                    } catch (Exception e) {
                                        throw new C0623d(context, CLConstants.ERROR_LOCALE_PARSE, CLConstants.ERROR_MSG_LOCALE_PARSE);
                                    }
                                } catch (Exception e2) {
                                    throw new C0623d(context, CLConstants.ERROR_PAY_INFO_PARSE, CLConstants.ERROR_MSG_PAY_INFO_PARSE);
                                }
                            } catch (C0623d e3) {
                                throw e3;
                            } catch (Throwable e4) {
                                throw new C0623d(context, CLConstants.ERROR_XML_PAYLOAD_PARSE, CLConstants.ERROR_MSG_XML_PAYLOAD_PARSE, e4);
                            }
                        } catch (C0623d e32) {
                            throw e32;
                        } catch (Throwable e42) {
                            throw new C0623d(context, CLConstants.ERROR_SALT_PARSE, CLConstants.ERROR_MSG_SALT_PARSE, e42);
                        }
                    } catch (Throwable e422) {
                        throw new C0623d(context, CLConstants.ERROR_CONFIG_PARSE, CLConstants.ERROR_MSG_CONFIG_PARSE, e422);
                    }
                } catch (Throwable e4222) {
                    throw new C0623d(context, CLConstants.ERROR_CONTROLS_PARSE, CLConstants.ERROR_MSG_CONTROLS_PARSE, e4222);
                }
            } catch (Throwable e42222) {
                C0625g.m6311a("CommonLibraryException", e42222.getMessage());
                throw new C0623d(context, CLConstants.ERROR_XMLPAYLOAD_VALIDATE, CLConstants.ERROR_MSG_XMLPAYLOAD_VALIDATE, e42222);
            } catch (C0623d e322) {
                throw e322;
            } catch (Throwable e422222) {
                throw new C0623d(context, CLConstants.ERROR_XML_PAYLOAD_PARSE, CLConstants.ERROR_MSG_XML_PAYLOAD_PARSE, e422222);
            }
        } catch (C0623d e3222) {
            throw e3222;
        } catch (Throwable e4222222) {
            throw new C0623d(context, CLConstants.ERROR_KEY_CODE_PARSE, CLConstants.ERROR_MSG_KEY_CODE_PARSE, e4222222);
        }
    }

    public Locale m6273b() {
        return this.f4112g;
    }

    public C0568e m6274c() {
        return this.f4113h;
    }

    public ab m6275d() {
        return this.f4115j;
    }

    public ResultReceiver m6276e() {
        return f4105k;
    }
}
