package org.npci.upi.security.pinactivitycomponent;

import android.support.v4.p004a.ContextCompat;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.npci.upi.security.pinactivitycomponent.widget.C0643b;

/* renamed from: org.npci.upi.security.pinactivitycomponent.m */
class C0630m implements Runnable {
    final /* synthetic */ C0620h f4143a;

    C0630m(C0620h c0620h) {
        this.f4143a = c0620h;
    }

    public void run() {
        if (this.f4143a.f4126g != -1 && (this.f4143a.f4125f.get(this.f4143a.f4126g) instanceof C0643b)) {
            C0643b c0643b = (C0643b) this.f4143a.f4125f.get(this.f4143a.f4126g);
            c0643b.m6393a(false);
            c0643b.m6391a(this.f4143a.m139a(R.action_resend), ContextCompat.m77a(this.f4143a.m176h(), R.ic_action_reload), new C0631n(this, c0643b), 0, true, true);
        }
    }
}
