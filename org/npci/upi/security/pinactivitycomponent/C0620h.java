package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.p004a.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.crashlytics.android.core.BuildConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.npci.upi.security.pinactivitycomponent.widget.C0619o;
import org.npci.upi.security.pinactivitycomponent.widget.C0641c;
import org.npci.upi.security.pinactivitycomponent.widget.C0643b;

/* renamed from: org.npci.upi.security.pinactivitycomponent.h */
public abstract class C0620h extends Fragment implements C0619o {
    protected JSONObject f4120a;
    protected Timer ai;
    protected Handler aj;
    protected Runnable ak;
    protected JSONObject al;
    protected JSONArray am;
    protected long an;
    protected Context ao;
    private boolean ap;
    protected JSONObject f4121b;
    protected JSONArray f4122c;
    protected Timer f4123d;
    protected long f4124e;
    protected ArrayList f4125f;
    protected int f4126g;
    protected PopupWindow f4127h;
    protected Timer f4128i;

    public C0620h() {
        this.f4120a = null;
        this.f4121b = null;
        this.f4122c = null;
        this.f4123d = null;
        this.f4124e = 45000;
        this.f4125f = new ArrayList();
        this.f4126g = -1;
        this.f4128i = null;
        this.al = null;
        this.am = new JSONArray();
        this.an = 3000;
        this.ap = false;
    }

    private void m6284M() {
        JSONObject jSONObject = null;
        Collection arrayList = new ArrayList();
        int i = 0;
        Object obj = null;
        Object obj2 = null;
        Object obj3 = null;
        while (i < this.f4122c.length()) {
            JSONObject jSONObject2;
            Object obj4;
            try {
                String optString = ((JSONObject) this.f4122c.get(i)).optString(CLConstants.FIELD_SUBTYPE, BuildConfig.FLAVOR);
                if (optString.equals(CLConstants.CREDTYPE_ATMPIN)) {
                    obj3 = this.f4122c.getJSONObject(i);
                }
                if (optString.matches("OTP|SMS|HOTP|TOTP")) {
                    obj2 = this.f4122c.getJSONObject(i);
                }
                if (optString.equals(CLConstants.CREDTYPE_MPIN)) {
                    obj = this.f4122c.getJSONObject(i);
                }
                jSONObject2 = optString.equals(CLConstants.CREDTYPE_NMPIN) ? this.f4122c.getJSONObject(i) : jSONObject;
                obj4 = obj;
                obj = obj2;
                obj2 = obj3;
            } catch (Exception e) {
                Exception exception = e;
                Object obj5 = obj;
                obj = obj2;
                obj2 = obj3;
                C0625g.m6310a("NPCIFragment", exception);
                JSONObject jSONObject3 = jSONObject;
                obj4 = obj5;
                jSONObject2 = jSONObject3;
            }
            i++;
            obj3 = obj2;
            obj2 = obj;
            obj = obj4;
            jSONObject = jSONObject2;
        }
        if (!(this.f4122c.length() != 3 || obj3 == null || obj2 == null || obj == null)) {
            arrayList.add(obj2);
            arrayList.add(obj3);
            arrayList.add(obj);
        }
        if (!(this.f4122c.length() != 2 || obj2 == null || obj == null)) {
            arrayList.add(obj2);
            arrayList.add(obj);
        }
        if (!(this.f4122c.length() != 2 || obj == null || jSONObject == null)) {
            arrayList.add(obj);
            arrayList.add(jSONObject);
        }
        if (arrayList.size() > 0) {
            this.f4122c = new JSONArray(arrayList);
        }
    }

    protected void m6285K() {
        Bundle g = m174g();
        if (g != null) {
            try {
                String string = g.getString(CLConstants.INPUT_KEY_CONFIGURATION);
                if (string != null) {
                    this.f4120a = new JSONObject(string);
                }
                string = g.getString(CLConstants.INPUT_KEY_CONTROLS);
                if (string != null) {
                    this.al = new JSONObject(string);
                    string = this.al.getString(CLConstants.FIELD_CRED_ALLOWED);
                    if (string != null) {
                        this.f4122c = new JSONArray(string);
                        m6284M();
                    }
                }
                string = g.getString(CLConstants.INPUT_KEY_SALT);
                if (string != null) {
                    this.f4121b = new JSONObject(string);
                }
                String string2 = g.getString(CLConstants.INPUT_KEY_PAY_INFO);
                if (string2 != null) {
                    this.am = new JSONArray(string2);
                }
            } catch (Throwable e) {
                Log.e("NPCIFragment", "Error while reading Arguments", e);
            }
        }
    }

    protected void m6286L() {
        if (!this.f4120a.optString(CLConstants.CONFIGURATION_RESEND_OTP_FEATURE, "false").equals("false") && !this.ap) {
            m176h().runOnUiThread(new C0630m(this));
        }
    }

    int m6287a(float f) {
        return (int) (((float) (m178i().getDisplayMetrics().densityDpi / 160)) * f);
    }

    protected C0643b m6288a(String str, int i, int i2) {
        LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        C0643b c0643b = new C0643b(m176h());
        if (this.f4122c.length() == 1) {
            c0643b.setActionBarPositionTop(true);
            layoutParams.width = m6287a(240.0f);
            layoutParams.topMargin = m6287a(40.0f);
            c0643b.getFormInputView().setCharSize(0.0f);
            c0643b.getFormInputView().setSpace((float) m6287a(15.0f));
            c0643b.getFormInputView().setFontSize((float) m6287a(32.0f));
            c0643b.getFormInputView().setPadding(0, m6287a(32.0f), 0, 0);
            c0643b.getFormInputView().setMargin(new int[]{0, m6287a(32.0f), 0, 0});
            c0643b.getFormInputView().setLineStrokeCentered(true);
            c0643b.getFormInputView().setLineStrokeSelected((float) m6287a(2.0f));
            c0643b.getFormInputView().setColorStates(ContextCompat.m80b(m176h(), R.form_item_input_colors_transparent));
        }
        c0643b.setLayoutParams(layoutParams);
        c0643b.setInputLength(i2);
        c0643b.setFormItemListener(this);
        c0643b.setTitle(str);
        c0643b.setFormItemTag(i);
        return c0643b;
    }

    public abstract void m6289a();

    public void m6290a(Context context) {
        super.m145a(context);
        this.ao = context;
    }

    public void m6291a(View view, Bundle bundle) {
        super.m153a(view, bundle);
        if (m176h() instanceof GetCredential) {
            ((GetCredential) m176h()).m6238a(this);
        }
    }

    protected void m6292a(Timer timer) {
        if (timer != null) {
            try {
                timer.cancel();
            } catch (Exception e) {
                C0625g.m6310a("NPCIFragment", e);
            }
        }
    }

    protected void m6293a(C0633p c0633p) {
        try {
            if (this.f4126g != -1) {
                this.ap = true;
                ((C0641c) this.f4125f.get(this.f4126g)).setText(c0633p.m6315b());
            }
        } catch (Exception e) {
        }
    }

    protected void m6294a(C0643b c0643b) {
        this.f4128i = new Timer();
        this.f4128i.schedule(new C0632o(this), this.f4124e);
        c0643b.m6391a(BuildConfig.FLAVOR, null, null, 0, false, false);
        c0643b.m6390a(null, false);
        c0643b.m6392a(m139a(R.detecting_otp), null, true, false);
        c0643b.m6393a(true);
    }

    protected void m6295b(View view, String str) {
        if (this.f4127h != null) {
            this.f4127h.dismiss();
        }
        View inflate = m176h().getLayoutInflater().inflate(R.layout_popup_view, null);
        ((TextView) inflate.findViewById(R.popup_text)).setText(str);
        this.f4127h = new PopupWindow(inflate, -2, m6287a(60.0f));
        this.f4127h.setAnimationStyle(R.PopupAnimation);
        this.f4127h.showAtLocation(view, 17, 0, 0);
        inflate.findViewById(R.popup_button).setOnClickListener(new C0628k(this));
        this.ai = new Timer();
        this.aj = new Handler(Looper.getMainLooper());
        this.ak = new C0629l(this);
        this.aj.postDelayed(this.ak, this.an);
    }

    public void m6296b(C0633p c0633p) {
        m6293a(c0633p);
    }

    public void m6297c(int i) {
        if (m176h() != null && (m176h() instanceof GetCredential)) {
            ((GetCredential) m176h()).m6240b(i);
        }
        C0634q c0634q = new C0634q(m176h());
        this.f4123d = new Timer();
        this.f4123d.scheduleAtFixedRate(new C0626i(this, c0634q, i), 100, 2000);
    }

    public void m6298q() {
        super.m189q();
        m6292a(this.f4123d);
        m6292a(this.f4128i);
        m6292a(this.ai);
        if (!(this.aj == null || this.ak == null)) {
            this.aj.removeCallbacks(this.ak);
        }
        if (this.f4127h != null) {
            this.f4127h.dismiss();
        }
    }
}
