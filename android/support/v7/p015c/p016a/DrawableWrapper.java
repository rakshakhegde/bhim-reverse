package android.support.v7.p015c.p016a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.support.v4.p002b.p003a.DrawableCompat;

/* renamed from: android.support.v7.c.a.a */
public class DrawableWrapper extends Drawable implements Callback {
    private Drawable f939a;

    public DrawableWrapper(Drawable drawable) {
        m2162a(drawable);
    }

    public void draw(Canvas canvas) {
        this.f939a.draw(canvas);
    }

    protected void onBoundsChange(Rect rect) {
        this.f939a.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f939a.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f939a.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f939a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f939a.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f939a.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f939a.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        return this.f939a.isStateful();
    }

    public boolean setState(int[] iArr) {
        return this.f939a.setState(iArr);
    }

    public int[] getState() {
        return this.f939a.getState();
    }

    public void jumpToCurrentState() {
        DrawableCompat.m655a(this.f939a);
    }

    public Drawable getCurrent() {
        return this.f939a.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f939a.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f939a.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f939a.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f939a.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f939a.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f939a.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f939a.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f939a.getPadding(rect);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    protected boolean onLevelChange(int i) {
        return this.f939a.setLevel(i);
    }

    public void setAutoMirrored(boolean z) {
        DrawableCompat.m663a(this.f939a, z);
    }

    public boolean isAutoMirrored() {
        return DrawableCompat.m664b(this.f939a);
    }

    public void setTint(int i) {
        DrawableCompat.m657a(this.f939a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        DrawableCompat.m659a(this.f939a, colorStateList);
    }

    public void setTintMode(Mode mode) {
        DrawableCompat.m662a(this.f939a, mode);
    }

    public void setHotspot(float f, float f2) {
        DrawableCompat.m656a(this.f939a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        DrawableCompat.m658a(this.f939a, i, i2, i3, i4);
    }

    public Drawable m2161a() {
        return this.f939a;
    }

    public void m2162a(Drawable drawable) {
        if (this.f939a != null) {
            this.f939a.setCallback(null);
        }
        this.f939a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
