package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.Theme;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.p000a.p001a.AnimatedVectorDrawableCompat;
import android.support.p000a.p001a.VectorDrawableCompat;
import android.support.v4.p002b.ColorUtils;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.p010e.ArrayMap;
import android.support.v4.p010e.LongSparseArray;
import android.support.v4.p010e.LruCache;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v7.widget.l */
public final class AppCompatDrawableManager {
    private static final Mode f1635a;
    private static AppCompatDrawableManager f1636b;
    private static final AppCompatDrawableManager f1637c;
    private static final int[] f1638d;
    private static final int[] f1639e;
    private static final int[] f1640f;
    private static final int[] f1641g;
    private static final int[] f1642h;
    private static final int[] f1643i;
    private WeakHashMap<Context, SparseArray<ColorStateList>> f1644j;
    private ArrayMap<String, AppCompatDrawableManager> f1645k;
    private SparseArray<String> f1646l;
    private final Object f1647m;
    private final WeakHashMap<Context, LongSparseArray<WeakReference<ConstantState>>> f1648n;
    private TypedValue f1649o;

    /* renamed from: android.support.v7.widget.l.c */
    private interface AppCompatDrawableManager {
        Drawable m2969a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme);
    }

    /* renamed from: android.support.v7.widget.l.a */
    private static class AppCompatDrawableManager implements AppCompatDrawableManager {
        private AppCompatDrawableManager() {
        }

        public Drawable m2970a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return AnimatedVectorDrawableCompat.m3a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Throwable e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    /* renamed from: android.support.v7.widget.l.b */
    private static class AppCompatDrawableManager extends LruCache<Integer, PorterDuffColorFilter> {
        public AppCompatDrawableManager(int i) {
            super(i);
        }

        PorterDuffColorFilter m2972a(int i, Mode mode) {
            return (PorterDuffColorFilter) m791a((Object) Integer.valueOf(AppCompatDrawableManager.m2971b(i, mode)));
        }

        PorterDuffColorFilter m2973a(int i, Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) m792a(Integer.valueOf(AppCompatDrawableManager.m2971b(i, mode)), porterDuffColorFilter);
        }

        private static int m2971b(int i, Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }
    }

    /* renamed from: android.support.v7.widget.l.d */
    private static class AppCompatDrawableManager implements AppCompatDrawableManager {
        private AppCompatDrawableManager() {
        }

        public Drawable m2974a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return VectorDrawableCompat.m66a(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Throwable e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    public AppCompatDrawableManager() {
        this.f1647m = new Object();
        this.f1648n = new WeakHashMap(0);
    }

    static {
        f1635a = Mode.SRC_IN;
        f1637c = new AppCompatDrawableManager(6);
        f1638d = new int[]{R.abc_textfield_search_default_mtrl_alpha, R.abc_textfield_default_mtrl_alpha, R.abc_ab_share_pack_mtrl_alpha};
        f1639e = new int[]{R.abc_ic_ab_back_mtrl_am_alpha, R.abc_ic_go_search_api_mtrl_alpha, R.abc_ic_search_api_mtrl_alpha, R.abc_ic_commit_search_api_mtrl_alpha, R.abc_ic_clear_mtrl_alpha, R.abc_ic_menu_share_mtrl_alpha, R.abc_ic_menu_copy_mtrl_am_alpha, R.abc_ic_menu_cut_mtrl_alpha, R.abc_ic_menu_selectall_mtrl_alpha, R.abc_ic_menu_paste_mtrl_am_alpha, R.abc_ic_menu_moreoverflow_mtrl_alpha, R.abc_ic_voice_search_api_mtrl_alpha};
        f1640f = new int[]{R.abc_textfield_activated_mtrl_alpha, R.abc_textfield_search_activated_mtrl_alpha, R.abc_cab_background_top_mtrl_alpha, R.abc_text_cursor_material};
        f1641g = new int[]{R.abc_popup_background_mtrl_mult, R.abc_cab_background_internal_bg, R.abc_menu_hardkey_panel_mtrl_mult};
        f1642h = new int[]{R.abc_edit_text_material, R.abc_tab_indicator_material, R.abc_textfield_search_material, R.abc_spinner_mtrl_am_alpha, R.abc_spinner_textfield_background_material, R.abc_ratingbar_full_material, R.abc_switch_track_mtrl_alpha, R.abc_switch_thumb_material, R.abc_btn_default_mtrl_shape, R.abc_btn_borderless_material};
        f1643i = new int[]{R.abc_btn_check_material, R.abc_btn_radio_material};
    }

    public static AppCompatDrawableManager m2981a() {
        if (f1636b == null) {
            f1636b = new AppCompatDrawableManager();
            AppCompatDrawableManager.m2985a(f1636b);
        }
        return f1636b;
    }

    private static void m2985a(AppCompatDrawableManager appCompatDrawableManager) {
        int i = VERSION.SDK_INT;
        if (i < 23) {
            appCompatDrawableManager.m2986a("vector", new AppCompatDrawableManager());
            if (i >= 11) {
                appCompatDrawableManager.m2986a("animated-vector", new AppCompatDrawableManager());
            }
        }
    }

    public Drawable m3004a(Context context, int i) {
        return m3005a(context, i, false);
    }

    public Drawable m3005a(Context context, int i, boolean z) {
        Drawable d = m2994d(context, i);
        if (d == null) {
            d = m2992c(context, i);
        }
        if (d == null) {
            d = ContextCompat.m77a(context, i);
        }
        if (d != null) {
            d = m2979a(context, i, z, d);
        }
        if (d != null) {
            ad.m2707a(d);
        }
        return d;
    }

    private static long m2975a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    private Drawable m2992c(Context context, int i) {
        if (this.f1649o == null) {
            this.f1649o = new TypedValue();
        }
        TypedValue typedValue = this.f1649o;
        context.getResources().getValue(i, typedValue, true);
        long a = AppCompatDrawableManager.m2975a(typedValue);
        Drawable a2 = m2980a(context, a);
        if (a2 == null) {
            if (i == R.abc_cab_background_top_material) {
                a2 = new LayerDrawable(new Drawable[]{m3004a(context, R.abc_cab_background_internal_bg), m3004a(context, R.abc_cab_background_top_mtrl_alpha)});
            }
            if (a2 != null) {
                a2.setChangingConfigurations(typedValue.changingConfigurations);
                m2988a(context, a, a2);
            }
        }
        return a2;
    }

    private Drawable m2979a(Context context, int i, boolean z, Drawable drawable) {
        ColorStateList b = m3007b(context, i);
        if (b != null) {
            if (ad.m2708b(drawable)) {
                drawable = drawable.mutate();
            }
            drawable = DrawableCompat.m668f(drawable);
            DrawableCompat.m659a(drawable, b);
            Mode a = m3003a(i);
            if (a == null) {
                return drawable;
            }
            DrawableCompat.m662a(drawable, a);
            return drawable;
        } else if (i == R.abc_seekbar_track_material) {
            r0 = (LayerDrawable) drawable;
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908288), am.m2800a(context, R.colorControlNormal), f1635a);
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908303), am.m2800a(context, R.colorControlNormal), f1635a);
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908301), am.m2800a(context, R.colorControlActivated), f1635a);
            return drawable;
        } else if (i == R.abc_ratingbar_indicator_material || i == R.abc_ratingbar_small_material) {
            r0 = (LayerDrawable) drawable;
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908288), am.m2804c(context, R.colorControlNormal), f1635a);
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908303), am.m2800a(context, R.colorControlActivated), f1635a);
            AppCompatDrawableManager.m2983a(r0.findDrawableByLayerId(16908301), am.m2800a(context, R.colorControlActivated), f1635a);
            return drawable;
        } else if (AppCompatDrawableManager.m2987a(context, i, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable m2994d(android.content.Context r10, int r11) {
        /*
        r9 = this;
        r1 = 0;
        r8 = 2;
        r7 = 1;
        r0 = r9.f1645k;
        if (r0 == 0) goto L_0x00bf;
    L_0x0007:
        r0 = r9.f1645k;
        r0 = r0.isEmpty();
        if (r0 != 0) goto L_0x00bf;
    L_0x000f:
        r0 = r9.f1646l;
        if (r0 == 0) goto L_0x002f;
    L_0x0013:
        r0 = r9.f1646l;
        r0 = r0.get(r11);
        r0 = (java.lang.String) r0;
        r2 = "appcompat_skip_skip";
        r2 = r2.equals(r0);
        if (r2 != 0) goto L_0x002d;
    L_0x0023:
        if (r0 == 0) goto L_0x0036;
    L_0x0025:
        r2 = r9.f1645k;
        r0 = r2.get(r0);
        if (r0 != 0) goto L_0x0036;
    L_0x002d:
        r0 = r1;
    L_0x002e:
        return r0;
    L_0x002f:
        r0 = new android.util.SparseArray;
        r0.<init>();
        r9.f1646l = r0;
    L_0x0036:
        r0 = r9.f1649o;
        if (r0 != 0) goto L_0x0041;
    L_0x003a:
        r0 = new android.util.TypedValue;
        r0.<init>();
        r9.f1649o = r0;
    L_0x0041:
        r2 = r9.f1649o;
        r0 = r10.getResources();
        r0.getValue(r11, r2, r7);
        r4 = android.support.v7.widget.AppCompatDrawableManager.m2975a(r2);
        r1 = r9.m2980a(r10, r4);
        if (r1 == 0) goto L_0x0056;
    L_0x0054:
        r0 = r1;
        goto L_0x002e;
    L_0x0056:
        r3 = r2.string;
        if (r3 == 0) goto L_0x008a;
    L_0x005a:
        r3 = r2.string;
        r3 = r3.toString();
        r6 = ".xml";
        r3 = r3.endsWith(r6);
        if (r3 == 0) goto L_0x008a;
    L_0x0068:
        r3 = r0.getXml(r11);	 Catch:{ Exception -> 0x0082 }
        r6 = android.util.Xml.asAttributeSet(r3);	 Catch:{ Exception -> 0x0082 }
    L_0x0070:
        r0 = r3.next();	 Catch:{ Exception -> 0x0082 }
        if (r0 == r8) goto L_0x0078;
    L_0x0076:
        if (r0 != r7) goto L_0x0070;
    L_0x0078:
        if (r0 == r8) goto L_0x0095;
    L_0x007a:
        r0 = new org.xmlpull.v1.XmlPullParserException;	 Catch:{ Exception -> 0x0082 }
        r2 = "No start tag found";
        r0.<init>(r2);	 Catch:{ Exception -> 0x0082 }
        throw r0;	 Catch:{ Exception -> 0x0082 }
    L_0x0082:
        r0 = move-exception;
        r2 = "AppCompatDrawableManager";
        r3 = "Exception while inflating drawable";
        android.util.Log.e(r2, r3, r0);
    L_0x008a:
        r0 = r1;
    L_0x008b:
        if (r0 != 0) goto L_0x002e;
    L_0x008d:
        r1 = r9.f1646l;
        r2 = "appcompat_skip_skip";
        r1.append(r11, r2);
        goto L_0x002e;
    L_0x0095:
        r0 = r3.getName();	 Catch:{ Exception -> 0x0082 }
        r7 = r9.f1646l;	 Catch:{ Exception -> 0x0082 }
        r7.append(r11, r0);	 Catch:{ Exception -> 0x0082 }
        r7 = r9.f1645k;	 Catch:{ Exception -> 0x0082 }
        r0 = r7.get(r0);	 Catch:{ Exception -> 0x0082 }
        r0 = (android.support.v7.widget.AppCompatDrawableManager.AppCompatDrawableManager) r0;	 Catch:{ Exception -> 0x0082 }
        if (r0 == 0) goto L_0x00b0;
    L_0x00a8:
        r7 = r10.getTheme();	 Catch:{ Exception -> 0x0082 }
        r1 = r0.m2969a(r10, r3, r6, r7);	 Catch:{ Exception -> 0x0082 }
    L_0x00b0:
        if (r1 == 0) goto L_0x00bd;
    L_0x00b2:
        r0 = r2.changingConfigurations;	 Catch:{ Exception -> 0x0082 }
        r1.setChangingConfigurations(r0);	 Catch:{ Exception -> 0x0082 }
        r0 = r9.m2988a(r10, r4, r1);	 Catch:{ Exception -> 0x0082 }
        if (r0 == 0) goto L_0x00bd;
    L_0x00bd:
        r0 = r1;
        goto L_0x008b;
    L_0x00bf:
        r0 = r1;
        goto L_0x002e;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.l.d(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    private Drawable m2980a(Context context, long j) {
        synchronized (this.f1647m) {
            LongSparseArray longSparseArray = (LongSparseArray) this.f1648n.get(context);
            if (longSparseArray == null) {
                return null;
            }
            WeakReference weakReference = (WeakReference) longSparseArray.m784a(j);
            if (weakReference != null) {
                ConstantState constantState = (ConstantState) weakReference.get();
                if (constantState != null) {
                    Drawable newDrawable = constantState.newDrawable(context.getResources());
                    return newDrawable;
                }
                longSparseArray.m788b(j);
            }
            return null;
        }
    }

    private boolean m2988a(Context context, long j, Drawable drawable) {
        ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        synchronized (this.f1647m) {
            LongSparseArray longSparseArray = (LongSparseArray) this.f1648n.get(context);
            if (longSparseArray == null) {
                longSparseArray = new LongSparseArray();
                this.f1648n.put(context, longSparseArray);
            }
            longSparseArray.m789b(j, new WeakReference(constantState));
        }
        return true;
    }

    public final Drawable m3006a(Context context, at atVar, int i) {
        Drawable d = m2994d(context, i);
        if (d == null) {
            d = atVar.m2880a(i);
        }
        if (d != null) {
            return m2979a(context, i, false, d);
        }
        return null;
    }

    static boolean m2987a(Context context, int i, Drawable drawable) {
        int i2;
        Mode mode;
        boolean z;
        int i3;
        Mode mode2 = f1635a;
        if (AppCompatDrawableManager.m2989a(f1638d, i)) {
            i2 = R.colorControlNormal;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (AppCompatDrawableManager.m2989a(f1640f, i)) {
            i2 = R.colorControlActivated;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (AppCompatDrawableManager.m2989a(f1641g, i)) {
            z = true;
            mode = Mode.MULTIPLY;
            i2 = 16842801;
            i3 = -1;
        } else if (i == R.abc_list_divider_mtrl_alpha) {
            i2 = 16842800;
            i3 = Math.round(40.8f);
            mode = mode2;
            z = true;
        } else {
            i3 = -1;
            i2 = 0;
            mode = mode2;
            z = false;
        }
        if (!z) {
            return false;
        }
        if (ad.m2708b(drawable)) {
            drawable = drawable.mutate();
        }
        drawable.setColorFilter(AppCompatDrawableManager.m2977a(am.m2800a(context, i2), mode));
        if (i3 == -1) {
            return true;
        }
        drawable.setAlpha(i3);
        return true;
    }

    private void m2986a(String str, AppCompatDrawableManager appCompatDrawableManager) {
        if (this.f1645k == null) {
            this.f1645k = new ArrayMap();
        }
        this.f1645k.put(str, appCompatDrawableManager);
    }

    private static boolean m2989a(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    final Mode m3003a(int i) {
        if (i == R.abc_switch_thumb_material) {
            return Mode.MULTIPLY;
        }
        return null;
    }

    public final ColorStateList m3007b(Context context, int i) {
        ColorStateList e = m2996e(context, i);
        if (e == null) {
            if (i == R.abc_edit_text_material) {
                e = m2995e(context);
            } else if (i == R.abc_switch_track_mtrl_alpha) {
                e = m2991c(context);
            } else if (i == R.abc_switch_thumb_material) {
                e = m2993d(context);
            } else if (i == R.abc_btn_default_mtrl_shape) {
                e = m2997f(context);
            } else if (i == R.abc_btn_borderless_material) {
                e = m2999g(context);
            } else if (i == R.abc_btn_colored_material) {
                e = m3000h(context);
            } else if (i == R.abc_spinner_mtrl_am_alpha || i == R.abc_spinner_textfield_background_material) {
                e = m3001i(context);
            } else if (AppCompatDrawableManager.m2989a(f1639e, i)) {
                e = am.m2803b(context, R.colorControlNormal);
            } else if (AppCompatDrawableManager.m2989a(f1642h, i)) {
                e = m2976a(context);
            } else if (AppCompatDrawableManager.m2989a(f1643i, i)) {
                e = m2990b(context);
            } else if (i == R.abc_seekbar_thumb_material) {
                e = m3002j(context);
            }
            if (e != null) {
                m2982a(context, i, e);
            }
        }
        return e;
    }

    private ColorStateList m2996e(Context context, int i) {
        if (this.f1644j == null) {
            return null;
        }
        SparseArray sparseArray = (SparseArray) this.f1644j.get(context);
        if (sparseArray != null) {
            return (ColorStateList) sparseArray.get(i);
        }
        return null;
    }

    private void m2982a(Context context, int i, ColorStateList colorStateList) {
        if (this.f1644j == null) {
            this.f1644j = new WeakHashMap();
        }
        SparseArray sparseArray = (SparseArray) this.f1644j.get(context);
        if (sparseArray == null) {
            sparseArray = new SparseArray();
            this.f1644j.put(context, sparseArray);
        }
        sparseArray.append(i, colorStateList);
    }

    private ColorStateList m2976a(Context context) {
        int a = am.m2800a(context, R.colorControlNormal);
        int a2 = am.m2800a(context, R.colorControlActivated);
        r2 = new int[7][];
        int[] iArr = new int[]{am.f1512a, am.m2804c(context, R.colorControlNormal), am.f1513b, a2, am.f1514c, a2, am.f1515d};
        iArr[3] = a2;
        r2[4] = am.f1516e;
        iArr[4] = a2;
        r2[5] = am.f1517f;
        iArr[5] = a2;
        r2[6] = am.f1519h;
        iArr[6] = a;
        return new ColorStateList(r2, iArr);
    }

    private ColorStateList m2990b(Context context) {
        r0 = new int[3][];
        int[] iArr = new int[]{am.f1512a, am.m2804c(context, R.colorControlNormal), am.f1516e};
        iArr[1] = am.m2800a(context, R.colorControlActivated);
        r0[2] = am.f1519h;
        iArr[2] = am.m2800a(context, R.colorControlNormal);
        return new ColorStateList(r0, iArr);
    }

    private ColorStateList m2991c(Context context) {
        r0 = new int[3][];
        int[] iArr = new int[]{am.f1512a, am.m2801a(context, 16842800, 0.1f), am.f1516e};
        iArr[1] = am.m2801a(context, R.colorControlActivated, 0.3f);
        r0[2] = am.f1519h;
        iArr[2] = am.m2801a(context, 16842800, 0.3f);
        return new ColorStateList(r0, iArr);
    }

    private ColorStateList m2993d(Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList b = am.m2803b(context, R.colorSwitchThumbNormal);
        if (b == null || !b.isStateful()) {
            iArr[0] = am.f1512a;
            iArr2[0] = am.m2804c(context, R.colorSwitchThumbNormal);
            iArr[1] = am.f1516e;
            iArr2[1] = am.m2800a(context, R.colorControlActivated);
            iArr[2] = am.f1519h;
            iArr2[2] = am.m2800a(context, R.colorSwitchThumbNormal);
        } else {
            iArr[0] = am.f1512a;
            iArr2[0] = b.getColorForState(iArr[0], 0);
            iArr[1] = am.f1516e;
            iArr2[1] = am.m2800a(context, R.colorControlActivated);
            iArr[2] = am.f1519h;
            iArr2[2] = b.getDefaultColor();
        }
        return new ColorStateList(iArr, iArr2);
    }

    private ColorStateList m2995e(Context context) {
        r0 = new int[3][];
        int[] iArr = new int[]{am.f1512a, am.m2804c(context, R.colorControlNormal), am.f1518g};
        iArr[1] = am.m2800a(context, R.colorControlNormal);
        r0[2] = am.f1519h;
        iArr[2] = am.m2800a(context, R.colorControlActivated);
        return new ColorStateList(r0, iArr);
    }

    private ColorStateList m2997f(Context context) {
        return m2998f(context, am.m2800a(context, R.colorButtonNormal));
    }

    private ColorStateList m2999g(Context context) {
        return m2998f(context, 0);
    }

    private ColorStateList m3000h(Context context) {
        return m2998f(context, am.m2800a(context, R.colorAccent));
    }

    private ColorStateList m2998f(Context context, int i) {
        r0 = new int[4][];
        r1 = new int[4];
        int a = am.m2800a(context, R.colorControlHighlight);
        r0[0] = am.f1512a;
        r1[0] = am.m2804c(context, R.colorButtonNormal);
        r0[1] = am.f1515d;
        r1[1] = ColorUtils.m710a(a, i);
        r0[2] = am.f1513b;
        r1[2] = ColorUtils.m710a(a, i);
        r0[3] = am.f1519h;
        r1[3] = i;
        return new ColorStateList(r0, r1);
    }

    private ColorStateList m3001i(Context context) {
        r0 = new int[3][];
        int[] iArr = new int[]{am.f1512a, am.m2804c(context, R.colorControlNormal), am.f1518g};
        iArr[1] = am.m2800a(context, R.colorControlNormal);
        r0[2] = am.f1519h;
        iArr[2] = am.m2800a(context, R.colorControlActivated);
        return new ColorStateList(r0, iArr);
    }

    private ColorStateList m3002j(Context context) {
        r0 = new int[2][];
        int[] iArr = new int[]{am.f1512a, am.m2804c(context, R.colorControlActivated)};
        r0[1] = am.f1519h;
        iArr[1] = am.m2800a(context, R.colorControlActivated);
        return new ColorStateList(r0, iArr);
    }

    public static void m2984a(Drawable drawable, ap apVar, int[] iArr) {
        if (!ad.m2708b(drawable) || drawable.mutate() == drawable) {
            if (apVar.f1528d || apVar.f1527c) {
                drawable.setColorFilter(AppCompatDrawableManager.m2978a(apVar.f1528d ? apVar.f1525a : null, apVar.f1527c ? apVar.f1526b : f1635a, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManager", "Mutated drawable is not the same instance as the input.");
    }

    private static PorterDuffColorFilter m2978a(ColorStateList colorStateList, Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return AppCompatDrawableManager.m2977a(colorStateList.getColorForState(iArr, 0), mode);
    }

    public static PorterDuffColorFilter m2977a(int i, Mode mode) {
        PorterDuffColorFilter a = f1637c.m2972a(i, mode);
        if (a != null) {
            return a;
        }
        a = new PorterDuffColorFilter(i, mode);
        f1637c.m2973a(i, mode, a);
        return a;
    }

    private static void m2983a(Drawable drawable, int i, Mode mode) {
        if (ad.m2708b(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f1635a;
        }
        drawable.setColorFilter(AppCompatDrawableManager.m2977a(i, mode));
    }
}
