package android.support.v7.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckedTextView;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.j */
public class AppCompatCheckedTextView extends CheckedTextView {
    private static final int[] f1625a;
    private AppCompatDrawableManager f1626b;
    private AppCompatTextHelper f1627c;

    static {
        f1625a = new int[]{16843016};
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1627c = AppCompatTextHelper.m3036a((TextView) this);
        this.f1627c.m3040a(attributeSet, i);
        this.f1627c.m3037a();
        this.f1626b = AppCompatDrawableManager.m2981a();
        ar a = ar.m2811a(getContext(), attributeSet, f1625a, i, 0);
        setCheckMarkDrawable(a.m2814a(0));
        a.m2815a();
    }

    public void setCheckMarkDrawable(int i) {
        if (this.f1626b != null) {
            setCheckMarkDrawable(this.f1626b.m3004a(getContext(), i));
        } else {
            super.setCheckMarkDrawable(i);
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1627c != null) {
            this.f1627c.m3038a(context, i);
        }
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1627c != null) {
            this.f1627c.m3037a();
        }
    }
}
