package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v7.p015c.p016a.DrawableWrapper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* compiled from: ListViewCompat */
public class ah extends ListView {
    private static final int[] f1409g;
    final Rect f1410a;
    int f1411b;
    int f1412c;
    int f1413d;
    int f1414e;
    protected int f1415f;
    private Field f1416h;
    private ListViewCompat f1417i;

    /* renamed from: android.support.v7.widget.ah.a */
    private static class ListViewCompat extends DrawableWrapper {
        private boolean f1462a;

        public ListViewCompat(Drawable drawable) {
            super(drawable);
            this.f1462a = true;
        }

        void m2758a(boolean z) {
            this.f1462a = z;
        }

        public boolean setState(int[] iArr) {
            if (this.f1462a) {
                return super.setState(iArr);
            }
            return false;
        }

        public void draw(Canvas canvas) {
            if (this.f1462a) {
                super.draw(canvas);
            }
        }

        public void setHotspot(float f, float f2) {
            if (this.f1462a) {
                super.setHotspot(f, f2);
            }
        }

        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f1462a) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        public boolean setVisible(boolean z, boolean z2) {
            if (this.f1462a) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }

    static {
        f1409g = new int[]{0};
    }

    public ah(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1410a = new Rect();
        this.f1411b = 0;
        this.f1412c = 0;
        this.f1413d = 0;
        this.f1414e = 0;
        try {
            this.f1416h = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.f1416h.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void setSelector(Drawable drawable) {
        this.f1417i = drawable != null ? new ListViewCompat(drawable) : null;
        super.setSelector(this.f1417i);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f1411b = rect.left;
        this.f1412c = rect.top;
        this.f1413d = rect.right;
        this.f1414e = rect.bottom;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        setSelectorEnabled(true);
        m2716b();
    }

    protected void dispatchDraw(Canvas canvas) {
        m2714a(canvas);
        super.dispatchDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case R.View_android_theme /*0*/:
                this.f1415f = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    protected void m2716b() {
        Drawable selector = getSelector();
        if (selector != null && m2718c()) {
            selector.setState(getDrawableState());
        }
    }

    protected boolean m2718c() {
        return m2715a() && isPressed();
    }

    protected boolean m2715a() {
        return false;
    }

    protected void m2714a(Canvas canvas) {
        if (!this.f1410a.isEmpty()) {
            Drawable selector = getSelector();
            if (selector != null) {
                selector.setBounds(this.f1410a);
                selector.draw(canvas);
            }
        }
    }

    protected void m2713a(int i, View view, float f, float f2) {
        m2712a(i, view);
        Drawable selector = getSelector();
        if (selector != null && i != -1) {
            DrawableCompat.m656a(selector, f, f2);
        }
    }

    protected void m2712a(int i, View view) {
        boolean z = true;
        Drawable selector = getSelector();
        boolean z2 = (selector == null || i == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        m2717b(i, view);
        if (z2) {
            Rect rect = this.f1410a;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            DrawableCompat.m656a(selector, exactCenterX, exactCenterY);
        }
    }

    protected void m2717b(int i, View view) {
        Rect rect = this.f1410a;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f1411b;
        rect.top -= this.f1412c;
        rect.right += this.f1413d;
        rect.bottom += this.f1414e;
        try {
            boolean z = this.f1416h.getBoolean(this);
            if (view.isEnabled() != z) {
                this.f1416h.set(this, Boolean.valueOf(!z));
                if (i != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public int m2711a(int i, int i2, int i3, int i4, int i5) {
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        listPaddingBottom += listPaddingTop;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int i6 = 0;
        View view = null;
        int i7 = 0;
        int count = adapter.getCount();
        int i8 = 0;
        while (i8 < count) {
            View view2;
            listPaddingTop = adapter.getItemViewType(i8);
            if (listPaddingTop != i7) {
                int i9 = listPaddingTop;
                view2 = null;
                i7 = i9;
            } else {
                view2 = view;
            }
            view = adapter.getView(i8, view2, this);
            LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            if (layoutParams.height > 0) {
                listPaddingTop = MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            } else {
                listPaddingTop = MeasureSpec.makeMeasureSpec(0, 0);
            }
            view.measure(i, listPaddingTop);
            view.forceLayout();
            if (i8 > 0) {
                listPaddingTop = listPaddingBottom + dividerHeight;
            } else {
                listPaddingTop = listPaddingBottom;
            }
            listPaddingTop += view.getMeasuredHeight();
            if (listPaddingTop < i4) {
                if (i5 >= 0 && i8 >= i5) {
                    i6 = listPaddingTop;
                }
                i8++;
                listPaddingBottom = listPaddingTop;
            } else if (i5 < 0 || i8 <= i5 || i6 <= 0 || listPaddingTop == i4) {
                return i4;
            } else {
                return i6;
            }
        }
        return listPaddingBottom;
    }

    protected void setSelectorEnabled(boolean z) {
        if (this.f1417i != null) {
            this.f1417i.m2758a(z);
        }
    }
}
