package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ActionProvider;
import android.support.v4.p006f.af;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class ActivityChooserView extends ViewGroup {
    ActionProvider f1277a;
    private final C0019a f1278b;
    private final C0020b f1279c;
    private final af f1280d;
    private final FrameLayout f1281e;
    private final ImageView f1282f;
    private final FrameLayout f1283g;
    private final int f1284h;
    private final DataSetObserver f1285i;
    private final OnGlobalLayoutListener f1286j;
    private ag f1287k;
    private OnDismissListener f1288l;
    private boolean f1289m;
    private int f1290n;
    private boolean f1291o;
    private int f1292p;

    public static class InnerLayout extends af {
        private static final int[] f1269a;

        static {
            f1269a = new int[]{16842964};
        }

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            ar a = ar.m2810a(context, attributeSet, f1269a);
            setBackgroundDrawable(a.m2814a(0));
            a.m2815a();
        }
    }

    /* renamed from: android.support.v7.widget.ActivityChooserView.a */
    private class C0019a extends BaseAdapter {
        final /* synthetic */ ActivityChooserView f1270a;
        private ActivityChooserModel f1271b;
        private int f1272c;
        private boolean f1273d;
        private boolean f1274e;
        private boolean f1275f;

        public void m2560a(ActivityChooserModel activityChooserModel) {
            ActivityChooserModel d = this.f1270a.f1278b.m2565d();
            if (d != null && this.f1270a.isShown()) {
                d.unregisterObserver(this.f1270a.f1285i);
            }
            this.f1271b = activityChooserModel;
            if (activityChooserModel != null && this.f1270a.isShown()) {
                activityChooserModel.registerObserver(this.f1270a.f1285i);
            }
            notifyDataSetChanged();
        }

        public int getItemViewType(int i) {
            if (this.f1275f && i == getCount() - 1) {
                return 1;
            }
            return 0;
        }

        public int getViewTypeCount() {
            return 3;
        }

        public int getCount() {
            int a = this.f1271b.m2945a();
            if (!(this.f1273d || this.f1271b.m2949b() == null)) {
                a--;
            }
            a = Math.min(a, this.f1272c);
            if (this.f1275f) {
                return a + 1;
            }
            return a;
        }

        public Object getItem(int i) {
            switch (getItemViewType(i)) {
                case R.View_android_theme /*0*/:
                    if (!(this.f1273d || this.f1271b.m2949b() == null)) {
                        i++;
                    }
                    return this.f1271b.m2947a(i);
                case R.View_android_focusable /*1*/:
                    return null;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            switch (getItemViewType(i)) {
                case R.View_android_theme /*0*/:
                    if (view == null || view.getId() != android.support.v7.p014b.R.R.list_item) {
                        view = LayoutInflater.from(this.f1270a.getContext()).inflate(android.support.v7.p014b.R.R.abc_activity_chooser_view_list_item, viewGroup, false);
                    }
                    PackageManager packageManager = this.f1270a.getContext().getPackageManager();
                    ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
                    ((ImageView) view.findViewById(android.support.v7.p014b.R.R.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
                    ((TextView) view.findViewById(android.support.v7.p014b.R.R.title)).setText(resolveInfo.loadLabel(packageManager));
                    if (this.f1273d && i == 0 && this.f1274e) {
                        af.m1188b(view, true);
                        return view;
                    }
                    af.m1188b(view, false);
                    return view;
                case R.View_android_focusable /*1*/:
                    if (view != null && view.getId() == 1) {
                        return view;
                    }
                    view = LayoutInflater.from(this.f1270a.getContext()).inflate(android.support.v7.p014b.R.R.abc_activity_chooser_view_list_item, viewGroup, false);
                    view.setId(1);
                    ((TextView) view.findViewById(android.support.v7.p014b.R.R.title)).setText(this.f1270a.getContext().getString(android.support.v7.p014b.R.R.abc_activity_chooser_view_see_all));
                    return view;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public int m2558a() {
            int i = 0;
            int i2 = this.f1272c;
            this.f1272c = Integer.MAX_VALUE;
            int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(0, 0);
            int count = getCount();
            View view = null;
            int i3 = 0;
            while (i < count) {
                view = getView(i, view, null);
                view.measure(makeMeasureSpec, makeMeasureSpec2);
                i3 = Math.max(i3, view.getMeasuredWidth());
                i++;
            }
            this.f1272c = i2;
            return i3;
        }

        public void m2559a(int i) {
            if (this.f1272c != i) {
                this.f1272c = i;
                notifyDataSetChanged();
            }
        }

        public ResolveInfo m2563b() {
            return this.f1271b.m2949b();
        }

        public void m2561a(boolean z) {
            if (this.f1275f != z) {
                this.f1275f = z;
                notifyDataSetChanged();
            }
        }

        public int m2564c() {
            return this.f1271b.m2945a();
        }

        public ActivityChooserModel m2565d() {
            return this.f1271b;
        }

        public void m2562a(boolean z, boolean z2) {
            if (this.f1273d != z || this.f1274e != z2) {
                this.f1273d = z;
                this.f1274e = z2;
                notifyDataSetChanged();
            }
        }

        public boolean m2566e() {
            return this.f1273d;
        }
    }

    /* renamed from: android.support.v7.widget.ActivityChooserView.b */
    private class C0020b implements OnClickListener, OnLongClickListener, OnItemClickListener, OnDismissListener {
        final /* synthetic */ ActivityChooserView f1276a;

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            switch (((C0019a) adapterView.getAdapter()).getItemViewType(i)) {
                case R.View_android_theme /*0*/:
                    this.f1276a.m2579b();
                    if (!this.f1276a.f1289m) {
                        if (!this.f1276a.f1278b.m2566e()) {
                            i++;
                        }
                        Intent b = this.f1276a.f1278b.m2565d().m2948b(i);
                        if (b != null) {
                            b.addFlags(524288);
                            this.f1276a.getContext().startActivity(b);
                        }
                    } else if (i > 0) {
                        this.f1276a.f1278b.m2565d().m2950c(i);
                    }
                case R.View_android_focusable /*1*/:
                    this.f1276a.m2569a(Integer.MAX_VALUE);
                default:
                    throw new IllegalArgumentException();
            }
        }

        public void onClick(View view) {
            if (view == this.f1276a.f1283g) {
                this.f1276a.m2579b();
                Intent b = this.f1276a.f1278b.m2565d().m2948b(this.f1276a.f1278b.m2565d().m2946a(this.f1276a.f1278b.m2563b()));
                if (b != null) {
                    b.addFlags(524288);
                    this.f1276a.getContext().startActivity(b);
                }
            } else if (view == this.f1276a.f1281e) {
                this.f1276a.f1289m = false;
                this.f1276a.m2569a(this.f1276a.f1290n);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public boolean onLongClick(View view) {
            if (view == this.f1276a.f1283g) {
                if (this.f1276a.f1278b.getCount() > 0) {
                    this.f1276a.f1289m = true;
                    this.f1276a.m2569a(this.f1276a.f1290n);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }

        public void onDismiss() {
            m2567a();
            if (this.f1276a.f1277a != null) {
                this.f1276a.f1277a.m1411a(false);
            }
        }

        private void m2567a() {
            if (this.f1276a.f1288l != null) {
                this.f1276a.f1288l.onDismiss();
            }
        }
    }

    public void setActivityChooserModel(ActivityChooserModel activityChooserModel) {
        this.f1278b.m2560a(activityChooserModel);
        if (m2580c()) {
            m2579b();
            m2578a();
        }
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.f1282f.setImageDrawable(drawable);
    }

    public void setExpandActivityOverflowButtonContentDescription(int i) {
        this.f1282f.setContentDescription(getContext().getString(i));
    }

    public void setProvider(ActionProvider actionProvider) {
        this.f1277a = actionProvider;
    }

    public boolean m2578a() {
        if (m2580c() || !this.f1291o) {
            return false;
        }
        this.f1289m = false;
        m2569a(this.f1290n);
        return true;
    }

    private void m2569a(int i) {
        if (this.f1278b.m2565d() == null) {
            throw new IllegalStateException("No data model. Did you call #setDataModel?");
        }
        getViewTreeObserver().addOnGlobalLayoutListener(this.f1286j);
        boolean z = this.f1283g.getVisibility() == 0;
        int c = this.f1278b.m2564c();
        int i2;
        if (z) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (i == Integer.MAX_VALUE || c <= r3 + i) {
            this.f1278b.m2561a(false);
            this.f1278b.m2559a(i);
        } else {
            this.f1278b.m2561a(true);
            this.f1278b.m2559a(i - 1);
        }
        ag listPopupWindow = getListPopupWindow();
        if (!listPopupWindow.m2755k()) {
            if (this.f1289m || !z) {
                this.f1278b.m2562a(true, z);
            } else {
                this.f1278b.m2562a(false, false);
            }
            listPopupWindow.m2748f(Math.min(this.f1278b.m2558a(), this.f1284h));
            listPopupWindow.m2741c();
            if (this.f1277a != null) {
                this.f1277a.m1411a(true);
            }
            listPopupWindow.m2757m().setContentDescription(getContext().getString(android.support.v7.p014b.R.R.abc_activitychooserview_choose_application));
        }
    }

    public boolean m2579b() {
        if (m2580c()) {
            getListPopupWindow().m2753i();
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeGlobalOnLayoutListener(this.f1286j);
            }
        }
        return true;
    }

    public boolean m2580c() {
        return getListPopupWindow().m2755k();
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ActivityChooserModel d = this.f1278b.m2565d();
        if (d != null) {
            d.registerObserver(this.f1285i);
        }
        this.f1291o = true;
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ActivityChooserModel d = this.f1278b.m2565d();
        if (d != null) {
            d.unregisterObserver(this.f1285i);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.f1286j);
        }
        if (m2580c()) {
            m2579b();
        }
        this.f1291o = false;
    }

    protected void onMeasure(int i, int i2) {
        View view = this.f1280d;
        if (this.f1283g.getVisibility() != 0) {
            i2 = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(i2), 1073741824);
        }
        measureChild(view, i, i2);
        setMeasuredDimension(view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.f1280d.layout(0, 0, i3 - i, i4 - i2);
        if (!m2580c()) {
            m2579b();
        }
    }

    public ActivityChooserModel getDataModel() {
        return this.f1278b.m2565d();
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.f1288l = onDismissListener;
    }

    public void setInitialActivityCount(int i) {
        this.f1290n = i;
    }

    public void setDefaultActionButtonContentDescription(int i) {
        this.f1292p = i;
    }

    private ag getListPopupWindow() {
        if (this.f1287k == null) {
            this.f1287k = new ag(getContext());
            this.f1287k.m2737a(this.f1278b);
            this.f1287k.m2735a((View) this);
            this.f1287k.m2739a(true);
            this.f1287k.m2736a(this.f1279c);
            this.f1287k.m2738a(this.f1279c);
        }
        return this.f1287k;
    }
}
