package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ac;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.h */
public class AppCompatButton extends Button implements ac {
    private final AppCompatDrawableManager f1620a;
    private final AppCompatBackgroundHelper f1621b;
    private final AppCompatTextHelper f1622c;

    public AppCompatButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.buttonStyle);
    }

    public AppCompatButton(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1620a = AppCompatDrawableManager.m2981a();
        this.f1621b = new AppCompatBackgroundHelper(this, this.f1620a);
        this.f1621b.m2957a(attributeSet, i);
        this.f1622c = AppCompatTextHelper.m3036a((TextView) this);
        this.f1622c.m3040a(attributeSet, i);
        this.f1622c.m3037a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1621b != null) {
            this.f1621b.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1621b != null) {
            this.f1621b.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1621b != null) {
            this.f1621b.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1621b != null ? this.f1621b.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1621b != null) {
            this.f1621b.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1621b != null ? this.f1621b.m2958b() : null;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1621b != null) {
            this.f1621b.m2960c();
        }
        if (this.f1622c != null) {
            this.f1622c.m3037a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1622c != null) {
            this.f1622c.m3038a(context, i);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    public void setSupportAllCaps(boolean z) {
        if (this.f1622c != null) {
            this.f1622c.m3041a(z);
        }
    }
}
