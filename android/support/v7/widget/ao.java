package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build.VERSION;
import android.support.v7.p013a.AppCompatDelegate;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: TintContextWrapper */
public class ao extends ContextWrapper {
    private static final ArrayList<WeakReference<ao>> f1522a;
    private Resources f1523b;
    private final Theme f1524c;

    static {
        f1522a = new ArrayList();
    }

    public static Context m2807a(Context context) {
        if (!m2808b(context)) {
            return context;
        }
        Context context2;
        int size = f1522a.size();
        for (int i = 0; i < size; i++) {
            WeakReference weakReference = (WeakReference) f1522a.get(i);
            context2 = weakReference != null ? (ao) weakReference.get() : null;
            if (context2 != null && context2.getBaseContext() == context) {
                return context2;
            }
        }
        context2 = new ao(context);
        f1522a.add(new WeakReference(context2));
        return context2;
    }

    private static boolean m2808b(Context context) {
        if ((context instanceof ao) || (context.getResources() instanceof aq) || (context.getResources() instanceof at)) {
            return false;
        }
        if (!AppCompatDelegate.m1879j() || VERSION.SDK_INT <= 20) {
            return true;
        }
        return false;
    }

    private ao(Context context) {
        super(context);
        if (at.m2879a()) {
            this.f1524c = getResources().newTheme();
            this.f1524c.setTo(context.getTheme());
            return;
        }
        this.f1524c = null;
    }

    public Theme getTheme() {
        return this.f1524c == null ? super.getTheme() : this.f1524c;
    }

    public void setTheme(int i) {
        if (this.f1524c == null) {
            super.setTheme(i);
        } else {
            this.f1524c.applyStyle(i, true);
        }
    }

    public Resources getResources() {
        if (this.f1523b == null) {
            this.f1523b = this.f1524c == null ? new aq(this, super.getResources()) : new at(this, super.getResources());
        }
        return this.f1523b;
    }
}
