package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.ae.FitWindowsViewGroup;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class FitWindowsFrameLayout extends FrameLayout implements ae {
    private FitWindowsViewGroup f1295a;

    public FitWindowsFrameLayout(Context context) {
        super(context);
    }

    public FitWindowsFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnFitSystemWindowsListener(FitWindowsViewGroup fitWindowsViewGroup) {
        this.f1295a = fitWindowsViewGroup;
    }

    protected boolean fitSystemWindows(Rect rect) {
        if (this.f1295a != null) {
            this.f1295a.m2003a(rect);
        }
        return super.fitSystemWindows(rect);
    }
}
