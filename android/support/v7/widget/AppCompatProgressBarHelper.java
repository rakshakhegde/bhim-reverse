package android.support.v7.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.p002b.p003a.DrawableWrapper;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;

/* renamed from: android.support.v7.widget.s */
class AppCompatProgressBarHelper {
    private static final int[] f1666b;
    final AppCompatDrawableManager f1667a;
    private final ProgressBar f1668c;
    private Bitmap f1669d;

    static {
        f1666b = new int[]{16843067, 16843068};
    }

    AppCompatProgressBarHelper(ProgressBar progressBar, AppCompatDrawableManager appCompatDrawableManager) {
        this.f1668c = progressBar;
        this.f1667a = appCompatDrawableManager;
    }

    void m3016a(AttributeSet attributeSet, int i) {
        ar a = ar.m2811a(this.f1668c.getContext(), attributeSet, f1666b, i, 0);
        Drawable b = a.m2818b(0);
        if (b != null) {
            this.f1668c.setIndeterminateDrawable(m3012a(b));
        }
        b = a.m2818b(1);
        if (b != null) {
            this.f1668c.setProgressDrawable(m3013a(b, false));
        }
        a.m2815a();
    }

    private Drawable m3013a(Drawable drawable, boolean z) {
        int i = 0;
        Drawable a;
        if (drawable instanceof DrawableWrapper) {
            a = ((DrawableWrapper) drawable).m694a();
            if (a != null) {
                ((DrawableWrapper) drawable).m695a(m3013a(a, z));
            }
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            Drawable[] drawableArr = new Drawable[numberOfLayers];
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                boolean z2;
                int id = layerDrawable.getId(i2);
                Drawable drawable2 = layerDrawable.getDrawable(i2);
                if (id == 16908301 || id == 16908303) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                drawableArr[i2] = m3013a(drawable2, z2);
            }
            a = new LayerDrawable(drawableArr);
            while (i < numberOfLayers) {
                a.setId(i, layerDrawable.getId(i));
                i++;
            }
            return a;
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (this.f1669d == null) {
                this.f1669d = bitmap;
            }
            Drawable shapeDrawable = new ShapeDrawable(m3014b());
            shapeDrawable.getPaint().setShader(new BitmapShader(bitmap, TileMode.REPEAT, TileMode.CLAMP));
            shapeDrawable.getPaint().setColorFilter(bitmapDrawable.getPaint().getColorFilter());
            return z ? new ClipDrawable(shapeDrawable, 3, 1) : shapeDrawable;
        }
        return drawable;
    }

    private Drawable m3012a(Drawable drawable) {
        if (!(drawable instanceof AnimationDrawable)) {
            return drawable;
        }
        AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        Drawable animationDrawable2 = new AnimationDrawable();
        animationDrawable2.setOneShot(animationDrawable.isOneShot());
        for (int i = 0; i < numberOfFrames; i++) {
            Drawable a = m3013a(animationDrawable.getFrame(i), true);
            a.setLevel(AbstractSpiCall.DEFAULT_TIMEOUT);
            animationDrawable2.addFrame(a, animationDrawable.getDuration(i));
        }
        animationDrawable2.setLevel(AbstractSpiCall.DEFAULT_TIMEOUT);
        return animationDrawable2;
    }

    private Shape m3014b() {
        return new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
    }

    Bitmap m3015a() {
        return this.f1669d;
    }
}
