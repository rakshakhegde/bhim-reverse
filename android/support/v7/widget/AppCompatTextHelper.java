package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.p014b.R.R;
import android.support.v7.p017d.AllCapsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.y */
class AppCompatTextHelper {
    private static final int[] f1703b;
    private static final int[] f1704c;
    final TextView f1705a;
    private ap f1706d;
    private ap f1707e;
    private ap f1708f;
    private ap f1709g;

    static AppCompatTextHelper m3036a(TextView textView) {
        if (VERSION.SDK_INT >= 17) {
            return new AppCompatTextHelperV17(textView);
        }
        return new AppCompatTextHelper(textView);
    }

    static {
        f1703b = new int[]{16842804, 16843119, 16843117, 16843120, 16843118};
        f1704c = new int[]{R.textAllCaps};
    }

    AppCompatTextHelper(TextView textView) {
        this.f1705a = textView;
    }

    void m3040a(AttributeSet attributeSet, int i) {
        int i2 = 1;
        Context context = this.f1705a.getContext();
        AppCompatDrawableManager a = AppCompatDrawableManager.m2981a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f1703b, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, -1);
        if (obtainStyledAttributes.hasValue(1)) {
            this.f1706d = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(1, 0));
        }
        if (obtainStyledAttributes.hasValue(2)) {
            this.f1707e = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(2, 0));
        }
        if (obtainStyledAttributes.hasValue(3)) {
            this.f1708f = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(3, 0));
        }
        if (obtainStyledAttributes.hasValue(4)) {
            this.f1709g = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(4, 0));
        }
        obtainStyledAttributes.recycle();
        if (!(this.f1705a.getTransformationMethod() instanceof PasswordTransformationMethod)) {
            boolean z;
            int i3;
            boolean z2;
            if (resourceId != -1) {
                TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, R.TextAppearance);
                if (obtainStyledAttributes2.hasValue(R.TextAppearance_textAllCaps)) {
                    z = obtainStyledAttributes2.getBoolean(R.TextAppearance_textAllCaps, false);
                    i3 = 1;
                } else {
                    z2 = false;
                    z = false;
                }
                obtainStyledAttributes2.recycle();
            } else {
                z2 = false;
                z = false;
            }
            TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, f1704c, i, 0);
            if (obtainStyledAttributes3.hasValue(0)) {
                z = obtainStyledAttributes3.getBoolean(0, false);
            } else {
                i2 = i3;
            }
            obtainStyledAttributes3.recycle();
            if (i2 != 0) {
                m3041a(z);
            }
        }
    }

    void m3038a(Context context, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, f1704c);
        if (obtainStyledAttributes.hasValue(0)) {
            m3041a(obtainStyledAttributes.getBoolean(0, false));
        }
        obtainStyledAttributes.recycle();
    }

    void m3041a(boolean z) {
        this.f1705a.setTransformationMethod(z ? new AllCapsTransformationMethod(this.f1705a.getContext()) : null);
    }

    void m3037a() {
        if (this.f1706d != null || this.f1707e != null || this.f1708f != null || this.f1709g != null) {
            Drawable[] compoundDrawables = this.f1705a.getCompoundDrawables();
            m3039a(compoundDrawables[0], this.f1706d);
            m3039a(compoundDrawables[1], this.f1707e);
            m3039a(compoundDrawables[2], this.f1708f);
            m3039a(compoundDrawables[3], this.f1709g);
        }
    }

    final void m3039a(Drawable drawable, ap apVar) {
        if (drawable != null && apVar != null) {
            AppCompatDrawableManager.m2984a(drawable, apVar, this.f1705a.getDrawableState());
        }
    }

    protected static ap m3035a(Context context, AppCompatDrawableManager appCompatDrawableManager, int i) {
        ColorStateList b = appCompatDrawableManager.m3007b(context, i);
        if (b == null) {
            return null;
        }
        ap apVar = new ap();
        apVar.f1528d = true;
        apVar.f1525a = b;
        return apVar;
    }
}
