package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlSerializer;

/* renamed from: android.support.v7.widget.e */
class ActivityChooserModel extends DataSetObservable {
    private static final String f1599a;
    private static final Object f1600b;
    private static final Map<String, ActivityChooserModel> f1601c;
    private final Object f1602d;
    private final List<ActivityChooserModel> f1603e;
    private final List<ActivityChooserModel> f1604f;
    private final Context f1605g;
    private final String f1606h;
    private Intent f1607i;
    private ActivityChooserModel f1608j;
    private int f1609k;
    private boolean f1610l;
    private boolean f1611m;
    private boolean f1612n;
    private boolean f1613o;
    private ActivityChooserModel f1614p;

    /* renamed from: android.support.v7.widget.e.a */
    public final class ActivityChooserModel implements Comparable<ActivityChooserModel> {
        public final ResolveInfo f1592a;
        public float f1593b;
        final /* synthetic */ ActivityChooserModel f1594c;

        public /* synthetic */ int compareTo(Object obj) {
            return m2929a((ActivityChooserModel) obj);
        }

        public ActivityChooserModel(ActivityChooserModel activityChooserModel, ResolveInfo resolveInfo) {
            this.f1594c = activityChooserModel;
            this.f1592a = resolveInfo;
        }

        public int hashCode() {
            return Float.floatToIntBits(this.f1593b) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (Float.floatToIntBits(this.f1593b) != Float.floatToIntBits(((ActivityChooserModel) obj).f1593b)) {
                return false;
            }
            return true;
        }

        public int m2929a(ActivityChooserModel activityChooserModel) {
            return Float.floatToIntBits(activityChooserModel.f1593b) - Float.floatToIntBits(this.f1593b);
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            stringBuilder.append("resolveInfo:").append(this.f1592a.toString());
            stringBuilder.append("; weight:").append(new BigDecimal((double) this.f1593b));
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
    }

    /* renamed from: android.support.v7.widget.e.b */
    public interface ActivityChooserModel {
        void m2930a(Intent intent, List<ActivityChooserModel> list, List<ActivityChooserModel> list2);
    }

    /* renamed from: android.support.v7.widget.e.c */
    public static final class ActivityChooserModel {
        public final ComponentName f1595a;
        public final long f1596b;
        public final float f1597c;

        public ActivityChooserModel(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public ActivityChooserModel(ComponentName componentName, long j, float f) {
            this.f1595a = componentName;
            this.f1596b = j;
            this.f1597c = f;
        }

        public int hashCode() {
            return (((((this.f1595a == null ? 0 : this.f1595a.hashCode()) + 31) * 31) + ((int) (this.f1596b ^ (this.f1596b >>> 32)))) * 31) + Float.floatToIntBits(this.f1597c);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ActivityChooserModel activityChooserModel = (ActivityChooserModel) obj;
            if (this.f1595a == null) {
                if (activityChooserModel.f1595a != null) {
                    return false;
                }
            } else if (!this.f1595a.equals(activityChooserModel.f1595a)) {
                return false;
            }
            if (this.f1596b != activityChooserModel.f1596b) {
                return false;
            }
            if (Float.floatToIntBits(this.f1597c) != Float.floatToIntBits(activityChooserModel.f1597c)) {
                return false;
            }
            return true;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            stringBuilder.append("; activity:").append(this.f1595a);
            stringBuilder.append("; time:").append(this.f1596b);
            stringBuilder.append("; weight:").append(new BigDecimal((double) this.f1597c));
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
    }

    /* renamed from: android.support.v7.widget.e.d */
    public interface ActivityChooserModel {
        boolean m2931a(ActivityChooserModel activityChooserModel, Intent intent);
    }

    /* renamed from: android.support.v7.widget.e.e */
    private final class ActivityChooserModel extends AsyncTask<Object, Void, Void> {
        final /* synthetic */ ActivityChooserModel f1598a;

        private ActivityChooserModel(ActivityChooserModel activityChooserModel) {
            this.f1598a = activityChooserModel;
        }

        public /* synthetic */ Object doInBackground(Object[] objArr) {
            return m2932a(objArr);
        }

        public Void m2932a(Object... objArr) {
            int i = 0;
            List list = (List) objArr[0];
            String str = (String) objArr[1];
            try {
                OutputStream openFileOutput = this.f1598a.f1605g.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", Boolean.valueOf(true));
                    newSerializer.startTag(null, "historical-records");
                    int size = list.size();
                    while (i < size) {
                        ActivityChooserModel activityChooserModel = (ActivityChooserModel) list.remove(0);
                        newSerializer.startTag(null, "historical-record");
                        newSerializer.attribute(null, "activity", activityChooserModel.f1595a.flattenToString());
                        newSerializer.attribute(null, "time", String.valueOf(activityChooserModel.f1596b));
                        newSerializer.attribute(null, "weight", String.valueOf(activityChooserModel.f1597c));
                        newSerializer.endTag(null, "historical-record");
                        i++;
                    }
                    newSerializer.endTag(null, "historical-records");
                    newSerializer.endDocument();
                    this.f1598a.f1610l = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (Throwable e2) {
                    Log.e(ActivityChooserModel.f1599a, "Error writing historical recrod file: " + this.f1598a.f1606h, e2);
                    this.f1598a.f1610l = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (Throwable e22) {
                    Log.e(ActivityChooserModel.f1599a, "Error writing historical recrod file: " + this.f1598a.f1606h, e22);
                    this.f1598a.f1610l = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e4) {
                        }
                    }
                } catch (Throwable e222) {
                    Log.e(ActivityChooserModel.f1599a, "Error writing historical recrod file: " + this.f1598a.f1606h, e222);
                    this.f1598a.f1610l = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (Throwable th) {
                    this.f1598a.f1610l = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e6) {
                        }
                    }
                }
            } catch (Throwable e2222) {
                Log.e(ActivityChooserModel.f1599a, "Error writing historical recrod file: " + str, e2222);
            }
            return null;
        }
    }

    static {
        f1599a = ActivityChooserModel.class.getSimpleName();
        f1600b = new Object();
        f1601c = new HashMap();
    }

    public int m2945a() {
        int size;
        synchronized (this.f1602d) {
            m2939e();
            size = this.f1603e.size();
        }
        return size;
    }

    public ResolveInfo m2947a(int i) {
        ResolveInfo resolveInfo;
        synchronized (this.f1602d) {
            m2939e();
            resolveInfo = ((ActivityChooserModel) this.f1603e.get(i)).f1592a;
        }
        return resolveInfo;
    }

    public int m2946a(ResolveInfo resolveInfo) {
        synchronized (this.f1602d) {
            m2939e();
            List list = this.f1603e;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (((ActivityChooserModel) list.get(i)).f1592a == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }

    public Intent m2948b(int i) {
        synchronized (this.f1602d) {
            if (this.f1607i == null) {
                return null;
            }
            m2939e();
            ActivityChooserModel activityChooserModel = (ActivityChooserModel) this.f1603e.get(i);
            ComponentName componentName = new ComponentName(activityChooserModel.f1592a.activityInfo.packageName, activityChooserModel.f1592a.activityInfo.name);
            Intent intent = new Intent(this.f1607i);
            intent.setComponent(componentName);
            if (this.f1614p != null) {
                if (this.f1614p.m2931a(this, new Intent(intent))) {
                    return null;
                }
            }
            m2934a(new ActivityChooserModel(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    public ResolveInfo m2949b() {
        synchronized (this.f1602d) {
            m2939e();
            if (this.f1603e.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = ((ActivityChooserModel) this.f1603e.get(0)).f1592a;
            return resolveInfo;
        }
    }

    public void m2950c(int i) {
        synchronized (this.f1602d) {
            float f;
            m2939e();
            ActivityChooserModel activityChooserModel = (ActivityChooserModel) this.f1603e.get(i);
            ActivityChooserModel activityChooserModel2 = (ActivityChooserModel) this.f1603e.get(0);
            if (activityChooserModel2 != null) {
                f = (activityChooserModel2.f1593b - activityChooserModel.f1593b) + 5.0f;
            } else {
                f = 1.0f;
            }
            m2934a(new ActivityChooserModel(new ComponentName(activityChooserModel.f1592a.activityInfo.packageName, activityChooserModel.f1592a.activityInfo.name), System.currentTimeMillis(), f));
        }
    }

    private void m2938d() {
        if (!this.f1611m) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.f1612n) {
            this.f1612n = false;
            if (!TextUtils.isEmpty(this.f1606h)) {
                AsyncTaskCompat.m1533a(new ActivityChooserModel(), new ArrayList(this.f1604f), this.f1606h);
            }
        }
    }

    private void m2939e() {
        int g = m2941g() | m2942h();
        m2943i();
        if (g != 0) {
            m2940f();
            notifyChanged();
        }
    }

    private boolean m2940f() {
        if (this.f1608j == null || this.f1607i == null || this.f1603e.isEmpty() || this.f1604f.isEmpty()) {
            return false;
        }
        this.f1608j.m2930a(this.f1607i, this.f1603e, Collections.unmodifiableList(this.f1604f));
        return true;
    }

    private boolean m2941g() {
        if (!this.f1613o || this.f1607i == null) {
            return false;
        }
        this.f1613o = false;
        this.f1603e.clear();
        List queryIntentActivities = this.f1605g.getPackageManager().queryIntentActivities(this.f1607i, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            this.f1603e.add(new ActivityChooserModel(this, (ResolveInfo) queryIntentActivities.get(i)));
        }
        return true;
    }

    private boolean m2942h() {
        if (!this.f1610l || !this.f1612n || TextUtils.isEmpty(this.f1606h)) {
            return false;
        }
        this.f1610l = false;
        this.f1611m = true;
        m2944j();
        return true;
    }

    private boolean m2934a(ActivityChooserModel activityChooserModel) {
        boolean add = this.f1604f.add(activityChooserModel);
        if (add) {
            this.f1612n = true;
            m2943i();
            m2938d();
            m2940f();
            notifyChanged();
        }
        return add;
    }

    private void m2943i() {
        int size = this.f1604f.size() - this.f1609k;
        if (size > 0) {
            this.f1612n = true;
            for (int i = 0; i < size; i++) {
                ActivityChooserModel activityChooserModel = (ActivityChooserModel) this.f1604f.remove(0);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2944j() {
        /*
        r9 = this;
        r8 = 1;
        r0 = r9.f1605g;	 Catch:{ FileNotFoundException -> 0x00d3 }
        r1 = r9.f1606h;	 Catch:{ FileNotFoundException -> 0x00d3 }
        r1 = r0.openFileInput(r1);	 Catch:{ FileNotFoundException -> 0x00d3 }
        r2 = android.util.Xml.newPullParser();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r0 = "UTF-8";
        r2.setInput(r1, r0);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r0 = 0;
    L_0x0013:
        if (r0 == r8) goto L_0x001d;
    L_0x0015:
        r3 = 2;
        if (r0 == r3) goto L_0x001d;
    L_0x0018:
        r0 = r2.next();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        goto L_0x0013;
    L_0x001d:
        r0 = "historical-records";
        r3 = r2.getName();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r0 = r0.equals(r3);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        if (r0 != 0) goto L_0x0052;
    L_0x0029:
        r0 = new org.xmlpull.v1.XmlPullParserException;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r2 = "Share records file does not start with historical-records tag.";
        r0.<init>(r2);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        throw r0;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
    L_0x0031:
        r0 = move-exception;
        r2 = f1599a;	 Catch:{ all -> 0x00c8 }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c8 }
        r3.<init>();	 Catch:{ all -> 0x00c8 }
        r4 = "Error reading historical recrod file: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x00c8 }
        r4 = r9.f1606h;	 Catch:{ all -> 0x00c8 }
        r3 = r3.append(r4);	 Catch:{ all -> 0x00c8 }
        r3 = r3.toString();	 Catch:{ all -> 0x00c8 }
        android.util.Log.e(r2, r3, r0);	 Catch:{ all -> 0x00c8 }
        if (r1 == 0) goto L_0x0051;
    L_0x004e:
        r1.close();	 Catch:{ IOException -> 0x00cf }
    L_0x0051:
        return;
    L_0x0052:
        r0 = r9.f1604f;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r0.clear();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
    L_0x0057:
        r3 = r2.next();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        if (r3 != r8) goto L_0x0065;
    L_0x005d:
        if (r1 == 0) goto L_0x0051;
    L_0x005f:
        r1.close();	 Catch:{ IOException -> 0x0063 }
        goto L_0x0051;
    L_0x0063:
        r0 = move-exception;
        goto L_0x0051;
    L_0x0065:
        r4 = 3;
        if (r3 == r4) goto L_0x0057;
    L_0x0068:
        r4 = 4;
        if (r3 == r4) goto L_0x0057;
    L_0x006b:
        r3 = r2.getName();	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r4 = "historical-record";
        r3 = r4.equals(r3);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        if (r3 != 0) goto L_0x00a2;
    L_0x0077:
        r0 = new org.xmlpull.v1.XmlPullParserException;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r2 = "Share records file not well-formed.";
        r0.<init>(r2);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        throw r0;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
    L_0x007f:
        r0 = move-exception;
        r2 = f1599a;	 Catch:{ all -> 0x00c8 }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c8 }
        r3.<init>();	 Catch:{ all -> 0x00c8 }
        r4 = "Error reading historical recrod file: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x00c8 }
        r4 = r9.f1606h;	 Catch:{ all -> 0x00c8 }
        r3 = r3.append(r4);	 Catch:{ all -> 0x00c8 }
        r3 = r3.toString();	 Catch:{ all -> 0x00c8 }
        android.util.Log.e(r2, r3, r0);	 Catch:{ all -> 0x00c8 }
        if (r1 == 0) goto L_0x0051;
    L_0x009c:
        r1.close();	 Catch:{ IOException -> 0x00a0 }
        goto L_0x0051;
    L_0x00a0:
        r0 = move-exception;
        goto L_0x0051;
    L_0x00a2:
        r3 = 0;
        r4 = "activity";
        r3 = r2.getAttributeValue(r3, r4);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r4 = 0;
        r5 = "time";
        r4 = r2.getAttributeValue(r4, r5);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r4 = java.lang.Long.parseLong(r4);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r6 = 0;
        r7 = "weight";
        r6 = r2.getAttributeValue(r6, r7);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r6 = java.lang.Float.parseFloat(r6);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r7 = new android.support.v7.widget.e$c;	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r7.<init>(r3, r4, r6);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        r0.add(r7);	 Catch:{ XmlPullParserException -> 0x0031, IOException -> 0x007f }
        goto L_0x0057;
    L_0x00c8:
        r0 = move-exception;
        if (r1 == 0) goto L_0x00ce;
    L_0x00cb:
        r1.close();	 Catch:{ IOException -> 0x00d1 }
    L_0x00ce:
        throw r0;
    L_0x00cf:
        r0 = move-exception;
        goto L_0x0051;
    L_0x00d1:
        r1 = move-exception;
        goto L_0x00ce;
    L_0x00d3:
        r0 = move-exception;
        goto L_0x0051;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.e.j():void");
    }
}
