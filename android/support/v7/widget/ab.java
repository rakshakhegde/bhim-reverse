package android.support.v7.widget;

import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.view.Menu;
import android.view.Window.Callback;

/* compiled from: DecorContentParent */
public interface ab {
    void m2474a(int i);

    void m2475a(Menu menu, MenuPresenter menuPresenter);

    boolean m2476d();

    boolean m2477e();

    boolean m2478f();

    boolean m2479g();

    boolean m2480h();

    void m2481i();

    void m2482j();

    void setWindowCallback(Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
