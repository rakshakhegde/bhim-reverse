package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.p002b.ColorUtils;
import android.util.TypedValue;

/* compiled from: ThemeUtils */
class am {
    static final int[] f1512a;
    static final int[] f1513b;
    static final int[] f1514c;
    static final int[] f1515d;
    static final int[] f1516e;
    static final int[] f1517f;
    static final int[] f1518g;
    static final int[] f1519h;
    private static final ThreadLocal<TypedValue> f1520i;
    private static final int[] f1521j;

    static {
        f1520i = new ThreadLocal();
        f1512a = new int[]{-16842910};
        f1513b = new int[]{16842908};
        f1514c = new int[]{16843518};
        f1515d = new int[]{16842919};
        f1516e = new int[]{16842912};
        f1517f = new int[]{16842913};
        f1518g = new int[]{-16842919, -16842908};
        f1519h = new int[0];
        f1521j = new int[1];
    }

    public static int m2800a(Context context, int i) {
        f1521j[0] = i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, f1521j);
        try {
            int color = obtainStyledAttributes.getColor(0, 0);
            return color;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public static ColorStateList m2803b(Context context, int i) {
        f1521j[0] = i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, f1521j);
        try {
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(0);
            return colorStateList;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public static int m2804c(Context context, int i) {
        ColorStateList b = m2803b(context, i);
        if (b != null && b.isStateful()) {
            return b.getColorForState(f1512a, b.getDefaultColor());
        }
        TypedValue a = m2802a();
        context.getTheme().resolveAttribute(16842803, a, true);
        return m2801a(context, i, a.getFloat());
    }

    private static TypedValue m2802a() {
        TypedValue typedValue = (TypedValue) f1520i.get();
        if (typedValue != null) {
            return typedValue;
        }
        typedValue = new TypedValue();
        f1520i.set(typedValue);
        return typedValue;
    }

    static int m2801a(Context context, int i, float f) {
        int a = m2800a(context, i);
        return ColorUtils.m712b(a, Math.round(((float) Color.alpha(a)) * f));
    }
}
