package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.NestedScrollingParent;
import android.support.v4.p006f.NestedScrollingParentHelper;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.support.v4.p006f.ay;
import android.support.v4.p006f.az;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window.Callback;

public class ActionBarOverlayLayout extends ViewGroup implements NestedScrollingParent, ab {
    static final int[] f1204a;
    private final Runnable f1205A;
    private final NestedScrollingParentHelper f1206B;
    private int f1207b;
    private int f1208c;
    private ContentFrameLayout f1209d;
    private ActionBarContainer f1210e;
    private ac f1211f;
    private Drawable f1212g;
    private boolean f1213h;
    private boolean f1214i;
    private boolean f1215j;
    private boolean f1216k;
    private boolean f1217l;
    private int f1218m;
    private int f1219n;
    private final Rect f1220o;
    private final Rect f1221p;
    private final Rect f1222q;
    private final Rect f1223r;
    private final Rect f1224s;
    private final Rect f1225t;
    private C0005a f1226u;
    private final int f1227v;
    private ScrollerCompat f1228w;
    private au f1229x;
    private final ay f1230y;
    private final Runnable f1231z;

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout.a */
    public interface C0005a {
        void m2108a(int i);

        void m2109g(boolean z);

        void m2110l();

        void m2111m();

        void m2112n();

        void m2113o();
    }

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout.1 */
    class C00101 extends az {
        final /* synthetic */ ActionBarOverlayLayout f1201a;

        C00101(ActionBarOverlayLayout actionBarOverlayLayout) {
            this.f1201a = actionBarOverlayLayout;
        }

        public void m2472b(View view) {
            this.f1201a.f1229x = null;
            this.f1201a.f1217l = false;
        }

        public void m2473c(View view) {
            this.f1201a.f1229x = null;
            this.f1201a.f1217l = false;
        }
    }

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout.2 */
    class C00112 implements Runnable {
        final /* synthetic */ ActionBarOverlayLayout f1202a;

        C00112(ActionBarOverlayLayout actionBarOverlayLayout) {
            this.f1202a = actionBarOverlayLayout;
        }

        public void run() {
            this.f1202a.m2492k();
            this.f1202a.f1229x = af.m1199k(this.f1202a.f1210e).m1358b(0.0f).m1355a(this.f1202a.f1230y);
        }
    }

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout.3 */
    class C00123 implements Runnable {
        final /* synthetic */ ActionBarOverlayLayout f1203a;

        C00123(ActionBarOverlayLayout actionBarOverlayLayout) {
            this.f1203a = actionBarOverlayLayout;
        }

        public void run() {
            this.f1203a.m2492k();
            this.f1203a.f1229x = af.m1199k(this.f1203a.f1210e).m1358b((float) (-this.f1203a.f1210e.getHeight())).m1355a(this.f1203a.f1230y);
        }
    }

    /* renamed from: android.support.v7.widget.ActionBarOverlayLayout.b */
    public static class C0013b extends MarginLayoutParams {
        public C0013b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0013b(int i, int i2) {
            super(i, i2);
        }

        public C0013b(LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    protected /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return m2501b();
    }

    public /* synthetic */ LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return m2497a(attributeSet);
    }

    static {
        f1204a = new int[]{R.actionBarSize, 16842841};
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1208c = 0;
        this.f1220o = new Rect();
        this.f1221p = new Rect();
        this.f1222q = new Rect();
        this.f1223r = new Rect();
        this.f1224s = new Rect();
        this.f1225t = new Rect();
        this.f1227v = 600;
        this.f1230y = new C00101(this);
        this.f1231z = new C00112(this);
        this.f1205A = new C00123(this);
        m2485a(context);
        this.f1206B = new NestedScrollingParentHelper(this);
    }

    private void m2485a(Context context) {
        boolean z = true;
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(f1204a);
        this.f1207b = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.f1212g = obtainStyledAttributes.getDrawable(1);
        setWillNotDraw(this.f1212g == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion >= 19) {
            z = false;
        }
        this.f1213h = z;
        this.f1228w = ScrollerCompat.m1750a(context);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m2492k();
    }

    public void setActionBarVisibilityCallback(C0005a c0005a) {
        this.f1226u = c0005a;
        if (getWindowToken() != null) {
            this.f1226u.m2108a(this.f1208c);
            if (this.f1219n != 0) {
                onWindowSystemUiVisibilityChanged(this.f1219n);
                af.m1202n(this);
            }
        }
    }

    public void setOverlayMode(boolean z) {
        this.f1214i = z;
        boolean z2 = z && getContext().getApplicationInfo().targetSdkVersion < 19;
        this.f1213h = z2;
    }

    public boolean m2500a() {
        return this.f1214i;
    }

    public void setHasNonEmbeddedTabs(boolean z) {
        this.f1215j = z;
    }

    public void setShowingForActionMode(boolean z) {
    }

    protected void onConfigurationChanged(Configuration configuration) {
        if (VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        m2485a(getContext());
        af.m1202n(this);
    }

    public void onWindowSystemUiVisibilityChanged(int i) {
        boolean z;
        boolean z2 = true;
        if (VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i);
        }
        m2502c();
        int i2 = this.f1219n ^ i;
        this.f1219n = i;
        boolean z3 = (i & 4) == 0;
        if ((i & 256) != 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.f1226u != null) {
            C0005a c0005a = this.f1226u;
            if (z) {
                z2 = false;
            }
            c0005a.m2109g(z2);
            if (z3 || !z) {
                this.f1226u.m2110l();
            } else {
                this.f1226u.m2111m();
            }
        }
        if ((i2 & 256) != 0 && this.f1226u != null) {
            af.m1202n(this);
        }
    }

    protected void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        this.f1208c = i;
        if (this.f1226u != null) {
            this.f1226u.m2108a(i);
        }
    }

    private boolean m2489a(View view, Rect rect, boolean z, boolean z2, boolean z3, boolean z4) {
        boolean z5 = false;
        C0013b c0013b = (C0013b) view.getLayoutParams();
        if (z && c0013b.leftMargin != rect.left) {
            c0013b.leftMargin = rect.left;
            z5 = true;
        }
        if (z2 && c0013b.topMargin != rect.top) {
            c0013b.topMargin = rect.top;
            z5 = true;
        }
        if (z4 && c0013b.rightMargin != rect.right) {
            c0013b.rightMargin = rect.right;
            z5 = true;
        }
        if (!z3 || c0013b.bottomMargin == rect.bottom) {
            return z5;
        }
        c0013b.bottomMargin = rect.bottom;
        return true;
    }

    protected boolean fitSystemWindows(Rect rect) {
        boolean a;
        m2502c();
        if ((af.m1201m(this) & 256) != 0) {
            a = m2489a(this.f1210e, rect, true, true, false, true);
            this.f1223r.set(rect);
            au.m2882a(this, this.f1223r, this.f1220o);
        } else {
            a = m2489a(this.f1210e, rect, true, true, false, true);
            this.f1223r.set(rect);
            au.m2882a(this, this.f1223r, this.f1220o);
        }
        if (!this.f1221p.equals(this.f1220o)) {
            this.f1221p.set(this.f1220o);
            a = true;
        }
        if (a) {
            requestLayout();
        }
        return true;
    }

    protected C0013b m2501b() {
        return new C0013b(-1, -1);
    }

    public C0013b m2497a(AttributeSet attributeSet) {
        return new C0013b(getContext(), attributeSet);
    }

    protected LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return new C0013b(layoutParams);
    }

    protected boolean checkLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof C0013b;
    }

    protected void onMeasure(int i, int i2) {
        Object obj;
        int i3;
        m2502c();
        measureChildWithMargins(this.f1210e, i, 0, i2, 0);
        C0013b c0013b = (C0013b) this.f1210e.getLayoutParams();
        int max = Math.max(0, (this.f1210e.getMeasuredWidth() + c0013b.leftMargin) + c0013b.rightMargin);
        int max2 = Math.max(0, c0013b.bottomMargin + (this.f1210e.getMeasuredHeight() + c0013b.topMargin));
        int a = au.m2881a(0, af.m1194f(this.f1210e));
        if ((af.m1201m(this) & 256) != 0) {
            obj = 1;
        } else {
            obj = null;
        }
        if (obj != null) {
            i3 = this.f1207b;
            if (this.f1215j && this.f1210e.getTabContainer() != null) {
                i3 += this.f1207b;
            }
        } else {
            i3 = this.f1210e.getVisibility() != 8 ? this.f1210e.getMeasuredHeight() : 0;
        }
        this.f1222q.set(this.f1220o);
        this.f1224s.set(this.f1223r);
        Rect rect;
        Rect rect2;
        if (this.f1214i || obj != null) {
            rect = this.f1224s;
            rect.top = i3 + rect.top;
            rect2 = this.f1224s;
            rect2.bottom += 0;
        } else {
            rect = this.f1222q;
            rect.top = i3 + rect.top;
            rect2 = this.f1222q;
            rect2.bottom += 0;
        }
        m2489a(this.f1209d, this.f1222q, true, true, true, true);
        if (!this.f1225t.equals(this.f1224s)) {
            this.f1225t.set(this.f1224s);
            this.f1209d.m2027a(this.f1224s);
        }
        measureChildWithMargins(this.f1209d, i, 0, i2, 0);
        c0013b = (C0013b) this.f1209d.getLayoutParams();
        int max3 = Math.max(max, (this.f1209d.getMeasuredWidth() + c0013b.leftMargin) + c0013b.rightMargin);
        i3 = Math.max(max2, c0013b.bottomMargin + (this.f1209d.getMeasuredHeight() + c0013b.topMargin));
        int a2 = au.m2881a(a, af.m1194f(this.f1209d));
        setMeasuredDimension(af.m1172a(Math.max(max3 + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), i, a2), af.m1172a(Math.max(i3 + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight()), i2, a2 << 16));
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingRight = (i3 - i) - getPaddingRight();
        int paddingTop = getPaddingTop();
        paddingRight = (i4 - i2) - getPaddingBottom();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                C0013b c0013b = (C0013b) childAt.getLayoutParams();
                int i6 = c0013b.leftMargin + paddingLeft;
                paddingRight = c0013b.topMargin + paddingTop;
                childAt.layout(i6, paddingRight, childAt.getMeasuredWidth() + i6, childAt.getMeasuredHeight() + paddingRight);
            }
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f1212g != null && !this.f1213h) {
            int bottom = this.f1210e.getVisibility() == 0 ? (int) ((((float) this.f1210e.getBottom()) + af.m1197i(this.f1210e)) + 0.5f) : 0;
            this.f1212g.setBounds(0, bottom, getWidth(), this.f1212g.getIntrinsicHeight() + bottom);
            this.f1212g.draw(canvas);
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        if ((i & 2) == 0 || this.f1210e.getVisibility() != 0) {
            return false;
        }
        return this.f1216k;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f1206B.m1532a(view, view2, i);
        this.f1218m = getActionBarHideOffset();
        m2492k();
        if (this.f1226u != null) {
            this.f1226u.m2112n();
        }
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        this.f1218m += i2;
        setActionBarHideOffset(this.f1218m);
    }

    public void onStopNestedScroll(View view) {
        if (this.f1216k && !this.f1217l) {
            if (this.f1218m <= this.f1210e.getHeight()) {
                m2493l();
            } else {
                m2494m();
            }
        }
        if (this.f1226u != null) {
            this.f1226u.m2113o();
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (!this.f1216k || !z) {
            return false;
        }
        if (m2487a(f, f2)) {
            m2496o();
        } else {
            m2495n();
        }
        this.f1217l = true;
        return true;
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return false;
    }

    public int getNestedScrollAxes() {
        return this.f1206B.m1530a();
    }

    void m2502c() {
        if (this.f1209d == null) {
            this.f1209d = (ContentFrameLayout) findViewById(R.action_bar_activity_content);
            this.f1210e = (ActionBarContainer) findViewById(R.action_bar_container);
            this.f1211f = m2484a(findViewById(R.action_bar));
        }
    }

    private ac m2484a(View view) {
        if (view instanceof ac) {
            return (ac) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    public void setHideOnContentScrollEnabled(boolean z) {
        if (z != this.f1216k) {
            this.f1216k = z;
            if (!z) {
                m2492k();
                setActionBarHideOffset(0);
            }
        }
    }

    public int getActionBarHideOffset() {
        return this.f1210e != null ? -((int) af.m1197i(this.f1210e)) : 0;
    }

    public void setActionBarHideOffset(int i) {
        m2492k();
        af.m1175a(this.f1210e, (float) (-Math.max(0, Math.min(i, this.f1210e.getHeight()))));
    }

    private void m2492k() {
        removeCallbacks(this.f1231z);
        removeCallbacks(this.f1205A);
        if (this.f1229x != null) {
            this.f1229x.m1360b();
        }
    }

    private void m2493l() {
        m2492k();
        postDelayed(this.f1231z, 600);
    }

    private void m2494m() {
        m2492k();
        postDelayed(this.f1205A, 600);
    }

    private void m2495n() {
        m2492k();
        this.f1231z.run();
    }

    private void m2496o() {
        m2492k();
        this.f1205A.run();
    }

    private boolean m2487a(float f, float f2) {
        this.f1228w.m1753a(0, 0, 0, (int) f2, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        if (this.f1228w.m1759d() > this.f1210e.getHeight()) {
            return true;
        }
        return false;
    }

    public void setWindowCallback(Callback callback) {
        m2502c();
        this.f1211f.m2683a(callback);
    }

    public void setWindowTitle(CharSequence charSequence) {
        m2502c();
        this.f1211f.m2684a(charSequence);
    }

    public CharSequence getTitle() {
        m2502c();
        return this.f1211f.m2692e();
    }

    public void m2498a(int i) {
        m2502c();
        switch (i) {
            case org.npci.upi.security.pinactivitycomponent.R.R.View_paddingStart /*2*/:
                this.f1211f.m2693f();
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_contentInsetStart /*5*/:
                this.f1211f.m2694g();
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_seekBarStyle /*109*/:
                setOverlayMode(true);
            default:
        }
    }

    public void setUiOptions(int i) {
    }

    public void setIcon(int i) {
        m2502c();
        this.f1211f.m2678a(i);
    }

    public void setIcon(Drawable drawable) {
        m2502c();
        this.f1211f.m2679a(drawable);
    }

    public void setLogo(int i) {
        m2502c();
        this.f1211f.m2687b(i);
    }

    public boolean m2503d() {
        m2502c();
        return this.f1211f.m2695h();
    }

    public boolean m2504e() {
        m2502c();
        return this.f1211f.m2696i();
    }

    public boolean m2505f() {
        m2502c();
        return this.f1211f.m2697j();
    }

    public boolean m2506g() {
        m2502c();
        return this.f1211f.m2698k();
    }

    public boolean m2507h() {
        m2502c();
        return this.f1211f.m2699l();
    }

    public void m2508i() {
        m2502c();
        this.f1211f.m2700m();
    }

    public void m2499a(Menu menu, MenuPresenter menuPresenter) {
        m2502c();
        this.f1211f.m2682a(menu, menuPresenter);
    }

    public void m2509j() {
        m2502c();
        this.f1211f.m2701n();
    }
}
