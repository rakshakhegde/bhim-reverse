package android.support.v7.widget;

import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import java.io.InputStream;

/* compiled from: ResourcesWrapper */
class ai extends Resources {
    private final Resources f1463a;

    public ai(Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f1463a = resources;
    }

    public CharSequence getText(int i) {
        return this.f1463a.getText(i);
    }

    public CharSequence getQuantityText(int i, int i2) {
        return this.f1463a.getQuantityText(i, i2);
    }

    public String getString(int i) {
        return this.f1463a.getString(i);
    }

    public String getString(int i, Object... objArr) {
        return this.f1463a.getString(i, objArr);
    }

    public String getQuantityString(int i, int i2, Object... objArr) {
        return this.f1463a.getQuantityString(i, i2, objArr);
    }

    public String getQuantityString(int i, int i2) {
        return this.f1463a.getQuantityString(i, i2);
    }

    public CharSequence getText(int i, CharSequence charSequence) {
        return this.f1463a.getText(i, charSequence);
    }

    public CharSequence[] getTextArray(int i) {
        return this.f1463a.getTextArray(i);
    }

    public String[] getStringArray(int i) {
        return this.f1463a.getStringArray(i);
    }

    public int[] getIntArray(int i) {
        return this.f1463a.getIntArray(i);
    }

    public TypedArray obtainTypedArray(int i) {
        return this.f1463a.obtainTypedArray(i);
    }

    public float getDimension(int i) {
        return this.f1463a.getDimension(i);
    }

    public int getDimensionPixelOffset(int i) {
        return this.f1463a.getDimensionPixelOffset(i);
    }

    public int getDimensionPixelSize(int i) {
        return this.f1463a.getDimensionPixelSize(i);
    }

    public float getFraction(int i, int i2, int i3) {
        return this.f1463a.getFraction(i, i2, i3);
    }

    public Drawable getDrawable(int i) {
        return this.f1463a.getDrawable(i);
    }

    public Drawable getDrawable(int i, Theme theme) {
        return this.f1463a.getDrawable(i, theme);
    }

    public Drawable getDrawableForDensity(int i, int i2) {
        return this.f1463a.getDrawableForDensity(i, i2);
    }

    public Drawable getDrawableForDensity(int i, int i2, Theme theme) {
        return this.f1463a.getDrawableForDensity(i, i2, theme);
    }

    public Movie getMovie(int i) {
        return this.f1463a.getMovie(i);
    }

    public int getColor(int i) {
        return this.f1463a.getColor(i);
    }

    public ColorStateList getColorStateList(int i) {
        return this.f1463a.getColorStateList(i);
    }

    public boolean getBoolean(int i) {
        return this.f1463a.getBoolean(i);
    }

    public int getInteger(int i) {
        return this.f1463a.getInteger(i);
    }

    public XmlResourceParser getLayout(int i) {
        return this.f1463a.getLayout(i);
    }

    public XmlResourceParser getAnimation(int i) {
        return this.f1463a.getAnimation(i);
    }

    public XmlResourceParser getXml(int i) {
        return this.f1463a.getXml(i);
    }

    public InputStream openRawResource(int i) {
        return this.f1463a.openRawResource(i);
    }

    public InputStream openRawResource(int i, TypedValue typedValue) {
        return this.f1463a.openRawResource(i, typedValue);
    }

    public AssetFileDescriptor openRawResourceFd(int i) {
        return this.f1463a.openRawResourceFd(i);
    }

    public void getValue(int i, TypedValue typedValue, boolean z) {
        this.f1463a.getValue(i, typedValue, z);
    }

    public void getValueForDensity(int i, int i2, TypedValue typedValue, boolean z) {
        this.f1463a.getValueForDensity(i, i2, typedValue, z);
    }

    public void getValue(String str, TypedValue typedValue, boolean z) {
        this.f1463a.getValue(str, typedValue, z);
    }

    public TypedArray obtainAttributes(AttributeSet attributeSet, int[] iArr) {
        return this.f1463a.obtainAttributes(attributeSet, iArr);
    }

    public void updateConfiguration(Configuration configuration, DisplayMetrics displayMetrics) {
        super.updateConfiguration(configuration, displayMetrics);
        if (this.f1463a != null) {
            this.f1463a.updateConfiguration(configuration, displayMetrics);
        }
    }

    public DisplayMetrics getDisplayMetrics() {
        return this.f1463a.getDisplayMetrics();
    }

    public Configuration getConfiguration() {
        return this.f1463a.getConfiguration();
    }

    public int getIdentifier(String str, String str2, String str3) {
        return this.f1463a.getIdentifier(str, str2, str3);
    }

    public String getResourceName(int i) {
        return this.f1463a.getResourceName(i);
    }

    public String getResourcePackageName(int i) {
        return this.f1463a.getResourcePackageName(i);
    }

    public String getResourceTypeName(int i) {
        return this.f1463a.getResourceTypeName(i);
    }

    public String getResourceEntryName(int i) {
        return this.f1463a.getResourceEntryName(i);
    }

    public void parseBundleExtras(XmlResourceParser xmlResourceParser, Bundle bundle) {
        this.f1463a.parseBundleExtras(xmlResourceParser, bundle);
    }

    public void parseBundleExtra(String str, AttributeSet attributeSet, Bundle bundle) {
        this.f1463a.parseBundleExtra(str, attributeSet, bundle);
    }
}
