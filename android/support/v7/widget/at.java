package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.p013a.AppCompatDelegate;
import java.lang.ref.WeakReference;

/* compiled from: VectorEnabledTintResources */
public class at extends Resources {
    private final WeakReference<Context> f1555a;

    public static boolean m2879a() {
        return AppCompatDelegate.m1879j() && VERSION.SDK_INT <= 20;
    }

    public at(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f1555a = new WeakReference(context);
    }

    public Drawable getDrawable(int i) {
        Context context = (Context) this.f1555a.get();
        if (context != null) {
            return AppCompatDrawableManager.m2981a().m3006a(context, this, i);
        }
        return super.getDrawable(i);
    }

    final Drawable m2880a(int i) {
        return super.getDrawable(i);
    }
}
