package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* renamed from: android.support.v7.widget.b */
class ActionBarBackgroundDrawable extends Drawable {
    final ActionBarContainer f1557a;

    public ActionBarBackgroundDrawable(ActionBarContainer actionBarContainer) {
        this.f1557a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f1557a.f1173d) {
            if (this.f1557a.f1170a != null) {
                this.f1557a.f1170a.draw(canvas);
            }
            if (this.f1557a.f1171b != null && this.f1557a.f1174e) {
                this.f1557a.f1171b.draw(canvas);
            }
        } else if (this.f1557a.f1172c != null) {
            this.f1557a.f1172c.draw(canvas);
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return 0;
    }
}
