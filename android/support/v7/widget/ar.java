package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/* compiled from: TintTypedArray */
public class ar {
    private final Context f1530a;
    private final TypedArray f1531b;

    public static ar m2810a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new ar(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public static ar m2811a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new ar(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    private ar(Context context, TypedArray typedArray) {
        this.f1530a = context;
        this.f1531b = typedArray;
    }

    public Drawable m2814a(int i) {
        if (this.f1531b.hasValue(i)) {
            int resourceId = this.f1531b.getResourceId(i, 0);
            if (resourceId != 0) {
                return AppCompatDrawableManager.m2981a().m3004a(this.f1530a, resourceId);
            }
        }
        return this.f1531b.getDrawable(i);
    }

    public Drawable m2818b(int i) {
        if (this.f1531b.hasValue(i)) {
            int resourceId = this.f1531b.getResourceId(i, 0);
            if (resourceId != 0) {
                return AppCompatDrawableManager.m2981a().m3005a(this.f1530a, resourceId, true);
            }
        }
        return null;
    }

    public CharSequence m2820c(int i) {
        return this.f1531b.getText(i);
    }

    public String m2822d(int i) {
        return this.f1531b.getString(i);
    }

    public boolean m2816a(int i, boolean z) {
        return this.f1531b.getBoolean(i, z);
    }

    public int m2813a(int i, int i2) {
        return this.f1531b.getInt(i, i2);
    }

    public float m2812a(int i, float f) {
        return this.f1531b.getFloat(i, f);
    }

    public int m2817b(int i, int i2) {
        return this.f1531b.getColor(i, i2);
    }

    public int m2819c(int i, int i2) {
        return this.f1531b.getInteger(i, i2);
    }

    public int m2821d(int i, int i2) {
        return this.f1531b.getDimensionPixelOffset(i, i2);
    }

    public int m2823e(int i, int i2) {
        return this.f1531b.getDimensionPixelSize(i, i2);
    }

    public int m2825f(int i, int i2) {
        return this.f1531b.getLayoutDimension(i, i2);
    }

    public int m2827g(int i, int i2) {
        return this.f1531b.getResourceId(i, i2);
    }

    public CharSequence[] m2824e(int i) {
        return this.f1531b.getTextArray(i);
    }

    public boolean m2826f(int i) {
        return this.f1531b.hasValue(i);
    }

    public void m2815a() {
        this.f1531b.recycle();
    }
}
