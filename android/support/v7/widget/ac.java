package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.au;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window.Callback;

/* compiled from: DecorToolbar */
public interface ac {
    au m2676a(int i, long j);

    ViewGroup m2677a();

    void m2678a(int i);

    void m2679a(Drawable drawable);

    void m2680a(MenuPresenter menuPresenter, MenuBuilder menuBuilder);

    void m2681a(ak akVar);

    void m2682a(Menu menu, MenuPresenter menuPresenter);

    void m2683a(Callback callback);

    void m2684a(CharSequence charSequence);

    void m2685a(boolean z);

    Context m2686b();

    void m2687b(int i);

    void m2688b(boolean z);

    void m2689c(int i);

    boolean m2690c();

    void m2691d();

    CharSequence m2692e();

    void m2693f();

    void m2694g();

    boolean m2695h();

    boolean m2696i();

    boolean m2697j();

    boolean m2698k();

    boolean m2699l();

    void m2700m();

    void m2701n();

    int m2702o();

    int m2703p();

    int m2704q();

    Menu m2705r();
}
