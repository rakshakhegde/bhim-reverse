package android.support.v7.widget;

import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.DrawableContainer.DrawableContainerState;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.v4.p002b.p003a.DrawableWrapper;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* compiled from: DrawableUtils */
public class ad {
    public static final Rect f1404a;
    private static Class<?> f1405b;

    static {
        f1404a = new Rect();
        if (VERSION.SDK_INT >= 18) {
            try {
                f1405b = Class.forName("android.graphics.Insets");
            } catch (ClassNotFoundException e) {
            }
        }
    }

    static void m2707a(Drawable drawable) {
        if (VERSION.SDK_INT == 21 && "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName())) {
            m2709c(drawable);
        }
    }

    public static boolean m2708b(Drawable drawable) {
        if (drawable instanceof LayerDrawable) {
            boolean z;
            if (VERSION.SDK_INT >= 16) {
                z = true;
            } else {
                z = false;
            }
            return z;
        } else if (drawable instanceof InsetDrawable) {
            if (VERSION.SDK_INT < 14) {
                return false;
            }
            return true;
        } else if (drawable instanceof StateListDrawable) {
            if (VERSION.SDK_INT < 8) {
                return false;
            }
            return true;
        } else if (drawable instanceof GradientDrawable) {
            if (VERSION.SDK_INT < 14) {
                return false;
            }
            return true;
        } else if (drawable instanceof DrawableContainer) {
            ConstantState constantState = drawable.getConstantState();
            if (!(constantState instanceof DrawableContainerState)) {
                return true;
            }
            for (Drawable b : ((DrawableContainerState) constantState).getChildren()) {
                if (!m2708b(b)) {
                    return false;
                }
            }
            return true;
        } else if (drawable instanceof DrawableWrapper) {
            return m2708b(((DrawableWrapper) drawable).m694a());
        } else {
            if (drawable instanceof android.support.v7.p015c.p016a.DrawableWrapper) {
                return m2708b(((android.support.v7.p015c.p016a.DrawableWrapper) drawable).m2161a());
            }
            if (drawable instanceof ScaleDrawable) {
                return m2708b(((ScaleDrawable) drawable).getDrawable());
            }
            return true;
        }
    }

    private static void m2709c(Drawable drawable) {
        int[] state = drawable.getState();
        if (state == null || state.length == 0) {
            drawable.setState(am.f1516e);
        } else {
            drawable.setState(am.f1519h);
        }
        drawable.setState(state);
    }

    static Mode m2706a(int i, Mode mode) {
        switch (i) {
            case R.View_paddingEnd /*3*/:
                return Mode.SRC_OVER;
            case R.Toolbar_contentInsetStart /*5*/:
                return Mode.SRC_IN;
            case R.Toolbar_popupTheme /*9*/:
                return Mode.SRC_ATOP;
            case R.Toolbar_titleMarginEnd /*14*/:
                return Mode.MULTIPLY;
            case R.Toolbar_titleMarginTop /*15*/:
                return Mode.SCREEN;
            case R.Toolbar_titleMarginBottom /*16*/:
                if (VERSION.SDK_INT >= 11) {
                    return Mode.valueOf("ADD");
                }
                return mode;
            default:
                return mode;
        }
    }
}
