package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

public class ActionBarContainer extends FrameLayout {
    Drawable f1170a;
    Drawable f1171b;
    Drawable f1172c;
    boolean f1173d;
    boolean f1174e;
    private boolean f1175f;
    private View f1176g;
    private View f1177h;
    private View f1178i;
    private int f1179j;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        Drawable actionBarBackgroundDrawableV21;
        super(context, attributeSet);
        if (VERSION.SDK_INT >= 21) {
            actionBarBackgroundDrawableV21 = new ActionBarBackgroundDrawableV21(this);
        } else {
            actionBarBackgroundDrawableV21 = new ActionBarBackgroundDrawable(this);
        }
        setBackgroundDrawable(actionBarBackgroundDrawableV21);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.ActionBar);
        this.f1170a = obtainStyledAttributes.getDrawable(R.ActionBar_background);
        this.f1171b = obtainStyledAttributes.getDrawable(R.ActionBar_backgroundStacked);
        this.f1179j = obtainStyledAttributes.getDimensionPixelSize(R.ActionBar_height, -1);
        if (getId() == R.split_action_bar) {
            this.f1173d = true;
            this.f1172c = obtainStyledAttributes.getDrawable(R.ActionBar_backgroundSplit);
        }
        obtainStyledAttributes.recycle();
        boolean z = this.f1173d ? this.f1172c == null : this.f1170a == null && this.f1171b == null;
        setWillNotDraw(z);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.f1177h = findViewById(R.action_bar);
        this.f1178i = findViewById(R.action_context_bar);
    }

    public void setPrimaryBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1170a != null) {
            this.f1170a.setCallback(null);
            unscheduleDrawable(this.f1170a);
        }
        this.f1170a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1177h != null) {
                this.f1170a.setBounds(this.f1177h.getLeft(), this.f1177h.getTop(), this.f1177h.getRight(), this.f1177h.getBottom());
            }
        }
        if (this.f1173d) {
            if (this.f1172c != null) {
                z = false;
            }
        } else if (!(this.f1170a == null && this.f1171b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setStackedBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1171b != null) {
            this.f1171b.setCallback(null);
            unscheduleDrawable(this.f1171b);
        }
        this.f1171b = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1174e && this.f1171b != null) {
                this.f1171b.setBounds(this.f1176g.getLeft(), this.f1176g.getTop(), this.f1176g.getRight(), this.f1176g.getBottom());
            }
        }
        if (this.f1173d) {
            if (this.f1172c != null) {
                z = false;
            }
        } else if (!(this.f1170a == null && this.f1171b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setSplitBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1172c != null) {
            this.f1172c.setCallback(null);
            unscheduleDrawable(this.f1172c);
        }
        this.f1172c = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1173d && this.f1172c != null) {
                this.f1172c.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (this.f1173d) {
            if (this.f1172c != null) {
                z = false;
            }
        } else if (!(this.f1170a == null && this.f1171b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setVisibility(int i) {
        boolean z;
        super.setVisibility(i);
        if (i == 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.f1170a != null) {
            this.f1170a.setVisible(z, false);
        }
        if (this.f1171b != null) {
            this.f1171b.setVisible(z, false);
        }
        if (this.f1172c != null) {
            this.f1172c.setVisible(z, false);
        }
    }

    protected boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.f1170a && !this.f1173d) || ((drawable == this.f1171b && this.f1174e) || ((drawable == this.f1172c && this.f1173d) || super.verifyDrawable(drawable)));
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1170a != null && this.f1170a.isStateful()) {
            this.f1170a.setState(getDrawableState());
        }
        if (this.f1171b != null && this.f1171b.isStateful()) {
            this.f1171b.setState(getDrawableState());
        }
        if (this.f1172c != null && this.f1172c.isStateful()) {
            this.f1172c.setState(getDrawableState());
        }
    }

    public void jumpDrawablesToCurrentState() {
        if (VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.f1170a != null) {
                this.f1170a.jumpToCurrentState();
            }
            if (this.f1171b != null) {
                this.f1171b.jumpToCurrentState();
            }
            if (this.f1172c != null) {
                this.f1172c.jumpToCurrentState();
            }
        }
    }

    public void setTransitioning(boolean z) {
        this.f1175f = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f1175f || super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public void setTabContainer(ak akVar) {
        if (this.f1176g != null) {
            removeView(this.f1176g);
        }
        this.f1176g = akVar;
        if (akVar != null) {
            addView(akVar);
            LayoutParams layoutParams = akVar.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            akVar.setAllowCollapse(false);
        }
    }

    public View getTabContainer() {
        return this.f1176g;
    }

    public ActionMode startActionModeForChild(View view, Callback callback) {
        return null;
    }

    private boolean m2456a(View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }

    private int m2457b(View view) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        return layoutParams.bottomMargin + (view.getMeasuredHeight() + layoutParams.topMargin);
    }

    public void onMeasure(int i, int i2) {
        if (this.f1177h == null && MeasureSpec.getMode(i2) == Integer.MIN_VALUE && this.f1179j >= 0) {
            i2 = MeasureSpec.makeMeasureSpec(Math.min(this.f1179j, MeasureSpec.getSize(i2)), Integer.MIN_VALUE);
        }
        super.onMeasure(i, i2);
        if (this.f1177h != null) {
            int mode = MeasureSpec.getMode(i2);
            if (this.f1176g != null && this.f1176g.getVisibility() != 8 && mode != 1073741824) {
                int b;
                if (!m2456a(this.f1177h)) {
                    b = m2457b(this.f1177h);
                } else if (m2456a(this.f1178i)) {
                    b = 0;
                } else {
                    b = m2457b(this.f1178i);
                }
                setMeasuredDimension(getMeasuredWidth(), Math.min(b + m2457b(this.f1176g), mode == Integer.MIN_VALUE ? MeasureSpec.getSize(i2) : Integer.MAX_VALUE));
            }
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = 1;
        super.onLayout(z, i, i2, i3, i4);
        View view = this.f1176g;
        boolean z2 = (view == null || view.getVisibility() == 8) ? false : true;
        if (!(view == null || view.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.layout(i, (measuredHeight - view.getMeasuredHeight()) - layoutParams.bottomMargin, i3, measuredHeight - layoutParams.bottomMargin);
        }
        if (!this.f1173d) {
            int i6;
            if (this.f1170a != null) {
                if (this.f1177h.getVisibility() == 0) {
                    this.f1170a.setBounds(this.f1177h.getLeft(), this.f1177h.getTop(), this.f1177h.getRight(), this.f1177h.getBottom());
                } else if (this.f1178i == null || this.f1178i.getVisibility() != 0) {
                    this.f1170a.setBounds(0, 0, 0, 0);
                } else {
                    this.f1170a.setBounds(this.f1178i.getLeft(), this.f1178i.getTop(), this.f1178i.getRight(), this.f1178i.getBottom());
                }
                i6 = 1;
            } else {
                i6 = 0;
            }
            this.f1174e = z2;
            if (!z2 || this.f1171b == null) {
                i5 = i6;
            } else {
                this.f1171b.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        } else if (this.f1172c != null) {
            this.f1172c.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
        } else {
            i5 = 0;
        }
        if (i5 != 0) {
            invalidate();
        }
    }
}
