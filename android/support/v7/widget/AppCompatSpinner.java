package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.p006f.ac;
import android.support.v4.p006f.af;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.ag.ListPopupWindow;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

/* renamed from: android.support.v7.widget.x */
public class AppCompatSpinner extends Spinner implements ac {
    private static final boolean f1691a;
    private static final boolean f1692b;
    private static final int[] f1693c;
    private AppCompatDrawableManager f1694d;
    private AppCompatBackgroundHelper f1695e;
    private Context f1696f;
    private ListPopupWindow f1697g;
    private SpinnerAdapter f1698h;
    private boolean f1699i;
    private AppCompatSpinner f1700j;
    private int f1701k;
    private final Rect f1702l;

    /* renamed from: android.support.v7.widget.x.1 */
    class AppCompatSpinner extends ListPopupWindow {
        final /* synthetic */ AppCompatSpinner f1678a;
        final /* synthetic */ AppCompatSpinner f1679b;

        AppCompatSpinner(AppCompatSpinner appCompatSpinner, View view, AppCompatSpinner appCompatSpinner2) {
            this.f1679b = appCompatSpinner;
            this.f1678a = appCompatSpinner2;
            super(view);
        }

        public ag m3018a() {
            return this.f1678a;
        }

        public boolean m3019b() {
            if (!this.f1679b.f1700j.m2755k()) {
                this.f1679b.f1700j.m3028c();
            }
            return true;
        }
    }

    /* renamed from: android.support.v7.widget.x.a */
    private static class AppCompatSpinner implements ListAdapter, SpinnerAdapter {
        private SpinnerAdapter f1680a;
        private ListAdapter f1681b;

        public AppCompatSpinner(SpinnerAdapter spinnerAdapter, Theme theme) {
            this.f1680a = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.f1681b = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (AppCompatSpinner.f1691a && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof an) {
                an anVar = (an) spinnerAdapter;
                if (anVar.m2805a() == null) {
                    anVar.m2806a(theme);
                }
            }
        }

        public int getCount() {
            return this.f1680a == null ? 0 : this.f1680a.getCount();
        }

        public Object getItem(int i) {
            return this.f1680a == null ? null : this.f1680a.getItem(i);
        }

        public long getItemId(int i) {
            return this.f1680a == null ? -1 : this.f1680a.getItemId(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            return this.f1680a == null ? null : this.f1680a.getDropDownView(i, view, viewGroup);
        }

        public boolean hasStableIds() {
            return this.f1680a != null && this.f1680a.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1680a != null) {
                this.f1680a.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1680a != null) {
                this.f1680a.unregisterDataSetObserver(dataSetObserver);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.f1681b;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int i) {
            ListAdapter listAdapter = this.f1681b;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i);
            }
            return true;
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }

    /* renamed from: android.support.v7.widget.x.b */
    private class AppCompatSpinner extends ag {
        final /* synthetic */ AppCompatSpinner f1687a;
        private CharSequence f1688c;
        private ListAdapter f1689d;
        private final Rect f1690e;

        /* renamed from: android.support.v7.widget.x.b.1 */
        class AppCompatSpinner implements OnItemClickListener {
            final /* synthetic */ AppCompatSpinner f1682a;
            final /* synthetic */ AppCompatSpinner f1683b;

            AppCompatSpinner(AppCompatSpinner appCompatSpinner, AppCompatSpinner appCompatSpinner2) {
                this.f1683b = appCompatSpinner;
                this.f1682a = appCompatSpinner2;
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                this.f1683b.f1687a.setSelection(i);
                if (this.f1683b.f1687a.getOnItemClickListener() != null) {
                    this.f1683b.f1687a.performItemClick(view, i, this.f1683b.f1689d.getItemId(i));
                }
                this.f1683b.m2753i();
            }
        }

        /* renamed from: android.support.v7.widget.x.b.2 */
        class AppCompatSpinner implements OnGlobalLayoutListener {
            final /* synthetic */ AppCompatSpinner f1684a;

            AppCompatSpinner(AppCompatSpinner appCompatSpinner) {
                this.f1684a = appCompatSpinner;
            }

            public void onGlobalLayout() {
                if (this.f1684a.m3023b(this.f1684a.f1687a)) {
                    this.f1684a.m3027b();
                    super.m2741c();
                    return;
                }
                this.f1684a.m2753i();
            }
        }

        /* renamed from: android.support.v7.widget.x.b.3 */
        class AppCompatSpinner implements OnDismissListener {
            final /* synthetic */ OnGlobalLayoutListener f1685a;
            final /* synthetic */ AppCompatSpinner f1686b;

            AppCompatSpinner(AppCompatSpinner appCompatSpinner, OnGlobalLayoutListener onGlobalLayoutListener) {
                this.f1686b = appCompatSpinner;
                this.f1685a = onGlobalLayoutListener;
            }

            public void onDismiss() {
                ViewTreeObserver viewTreeObserver = this.f1686b.f1687a.getViewTreeObserver();
                if (viewTreeObserver != null) {
                    viewTreeObserver.removeGlobalOnLayoutListener(this.f1685a);
                }
            }
        }

        public AppCompatSpinner(AppCompatSpinner appCompatSpinner, Context context, AttributeSet attributeSet, int i) {
            this.f1687a = appCompatSpinner;
            super(context, attributeSet, i);
            this.f1690e = new Rect();
            m2735a((View) appCompatSpinner);
            m2739a(true);
            m2733a(0);
            m2736a(new AppCompatSpinner(this, appCompatSpinner));
        }

        public void m3025a(ListAdapter listAdapter) {
            super.m2737a(listAdapter);
            this.f1689d = listAdapter;
        }

        public CharSequence m3024a() {
            return this.f1688c;
        }

        public void m3026a(CharSequence charSequence) {
            this.f1688c = charSequence;
        }

        void m3027b() {
            int i;
            int i2;
            Drawable d = m2743d();
            if (d != null) {
                d.getPadding(this.f1687a.f1702l);
                i = au.m2883a(this.f1687a) ? this.f1687a.f1702l.right : -this.f1687a.f1702l.left;
            } else {
                Rect b = this.f1687a.f1702l;
                this.f1687a.f1702l.right = 0;
                b.left = 0;
                i = 0;
            }
            int paddingLeft = this.f1687a.getPaddingLeft();
            int paddingRight = this.f1687a.getPaddingRight();
            int width = this.f1687a.getWidth();
            if (this.f1687a.f1701k == -2) {
                int a = this.f1687a.m3030a((SpinnerAdapter) this.f1689d, m2743d());
                i2 = (this.f1687a.getContext().getResources().getDisplayMetrics().widthPixels - this.f1687a.f1702l.left) - this.f1687a.f1702l.right;
                if (a <= i2) {
                    i2 = a;
                }
                m2748f(Math.max(i2, (width - paddingLeft) - paddingRight));
            } else if (this.f1687a.f1701k == -1) {
                m2748f((width - paddingLeft) - paddingRight);
            } else {
                m2748f(this.f1687a.f1701k);
            }
            if (au.m2883a(this.f1687a)) {
                i2 = ((width - paddingRight) - m2751h()) + i;
            } else {
                i2 = i + paddingLeft;
            }
            m2740b(i2);
        }

        public void m3028c() {
            boolean k = m2755k();
            m3027b();
            m2750g(2);
            super.m2741c();
            m2757m().setChoiceMode(1);
            m2752h(this.f1687a.getSelectedItemPosition());
            if (!k) {
                ViewTreeObserver viewTreeObserver = this.f1687a.getViewTreeObserver();
                if (viewTreeObserver != null) {
                    OnGlobalLayoutListener appCompatSpinner = new AppCompatSpinner(this);
                    viewTreeObserver.addOnGlobalLayoutListener(appCompatSpinner);
                    m2738a(new AppCompatSpinner(this, appCompatSpinner));
                }
            }
        }

        private boolean m3023b(View view) {
            return af.m1209u(view) && view.getGlobalVisibleRect(this.f1690e);
        }
    }

    static {
        boolean z;
        if (VERSION.SDK_INT >= 23) {
            z = true;
        } else {
            z = false;
        }
        f1691a = z;
        if (VERSION.SDK_INT >= 16) {
            z = true;
        } else {
            z = false;
        }
        f1692b = z;
        f1693c = new int[]{16843505};
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.spinnerStyle);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, -1);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i, int i2) {
        this(context, attributeSet, i, i2, null);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i, int i2, Theme theme) {
        Throwable e;
        AppCompatSpinner appCompatSpinner;
        ar a;
        CharSequence[] e2;
        SpinnerAdapter arrayAdapter;
        super(context, attributeSet, i);
        this.f1702l = new Rect();
        ar a2 = ar.m2811a(context, attributeSet, R.Spinner, i, 0);
        this.f1694d = AppCompatDrawableManager.m2981a();
        this.f1695e = new AppCompatBackgroundHelper(this, this.f1694d);
        if (theme != null) {
            this.f1696f = new ContextThemeWrapper(context, theme);
        } else {
            int g = a2.m2827g(R.Spinner_popupTheme, 0);
            if (g != 0) {
                this.f1696f = new ContextThemeWrapper(context, g);
            } else {
                this.f1696f = !f1691a ? context : null;
            }
        }
        if (this.f1696f != null) {
            if (i2 == -1) {
                if (VERSION.SDK_INT >= 11) {
                    TypedArray obtainStyledAttributes;
                    try {
                        obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f1693c, i, 0);
                        try {
                            if (obtainStyledAttributes.hasValue(0)) {
                                i2 = obtainStyledAttributes.getInt(0, 0);
                            }
                            if (obtainStyledAttributes != null) {
                                obtainStyledAttributes.recycle();
                            }
                        } catch (Exception e3) {
                            e = e3;
                            try {
                                Log.i("AppCompatSpinner", "Could not read android:spinnerMode", e);
                                if (obtainStyledAttributes != null) {
                                    obtainStyledAttributes.recycle();
                                }
                                if (i2 == 1) {
                                    appCompatSpinner = new AppCompatSpinner(this, this.f1696f, attributeSet, i);
                                    a = ar.m2811a(this.f1696f, attributeSet, R.Spinner, i, 0);
                                    this.f1701k = a.m2825f(R.Spinner_android_dropDownWidth, -2);
                                    appCompatSpinner.m2734a(a.m2814a(R.Spinner_android_popupBackground));
                                    appCompatSpinner.m3026a(a2.m2822d(R.Spinner_android_prompt));
                                    a.m2815a();
                                    this.f1700j = appCompatSpinner;
                                    this.f1697g = new AppCompatSpinner(this, this, appCompatSpinner);
                                }
                                e2 = a2.m2824e(R.Spinner_android_entries);
                                if (e2 != null) {
                                    arrayAdapter = new ArrayAdapter(context, 17367048, e2);
                                    arrayAdapter.setDropDownViewResource(R.support_simple_spinner_dropdown_item);
                                    setAdapter(arrayAdapter);
                                }
                                a2.m2815a();
                                this.f1699i = true;
                                if (this.f1698h != null) {
                                    setAdapter(this.f1698h);
                                    this.f1698h = null;
                                }
                                this.f1695e.m2957a(attributeSet, i);
                            } catch (Throwable th) {
                                e = th;
                                if (obtainStyledAttributes != null) {
                                    obtainStyledAttributes.recycle();
                                }
                                throw e;
                            }
                        }
                    } catch (Exception e4) {
                        e = e4;
                        obtainStyledAttributes = null;
                        Log.i("AppCompatSpinner", "Could not read android:spinnerMode", e);
                        if (obtainStyledAttributes != null) {
                            obtainStyledAttributes.recycle();
                        }
                        if (i2 == 1) {
                            appCompatSpinner = new AppCompatSpinner(this, this.f1696f, attributeSet, i);
                            a = ar.m2811a(this.f1696f, attributeSet, R.Spinner, i, 0);
                            this.f1701k = a.m2825f(R.Spinner_android_dropDownWidth, -2);
                            appCompatSpinner.m2734a(a.m2814a(R.Spinner_android_popupBackground));
                            appCompatSpinner.m3026a(a2.m2822d(R.Spinner_android_prompt));
                            a.m2815a();
                            this.f1700j = appCompatSpinner;
                            this.f1697g = new AppCompatSpinner(this, this, appCompatSpinner);
                        }
                        e2 = a2.m2824e(R.Spinner_android_entries);
                        if (e2 != null) {
                            arrayAdapter = new ArrayAdapter(context, 17367048, e2);
                            arrayAdapter.setDropDownViewResource(R.support_simple_spinner_dropdown_item);
                            setAdapter(arrayAdapter);
                        }
                        a2.m2815a();
                        this.f1699i = true;
                        if (this.f1698h != null) {
                            setAdapter(this.f1698h);
                            this.f1698h = null;
                        }
                        this.f1695e.m2957a(attributeSet, i);
                    } catch (Throwable th2) {
                        e = th2;
                        obtainStyledAttributes = null;
                        if (obtainStyledAttributes != null) {
                            obtainStyledAttributes.recycle();
                        }
                        throw e;
                    }
                }
                i2 = 1;
            }
            if (i2 == 1) {
                appCompatSpinner = new AppCompatSpinner(this, this.f1696f, attributeSet, i);
                a = ar.m2811a(this.f1696f, attributeSet, R.Spinner, i, 0);
                this.f1701k = a.m2825f(R.Spinner_android_dropDownWidth, -2);
                appCompatSpinner.m2734a(a.m2814a(R.Spinner_android_popupBackground));
                appCompatSpinner.m3026a(a2.m2822d(R.Spinner_android_prompt));
                a.m2815a();
                this.f1700j = appCompatSpinner;
                this.f1697g = new AppCompatSpinner(this, this, appCompatSpinner);
            }
        }
        e2 = a2.m2824e(R.Spinner_android_entries);
        if (e2 != null) {
            arrayAdapter = new ArrayAdapter(context, 17367048, e2);
            arrayAdapter.setDropDownViewResource(R.support_simple_spinner_dropdown_item);
            setAdapter(arrayAdapter);
        }
        a2.m2815a();
        this.f1699i = true;
        if (this.f1698h != null) {
            setAdapter(this.f1698h);
            this.f1698h = null;
        }
        this.f1695e.m2957a(attributeSet, i);
    }

    public Context getPopupContext() {
        if (this.f1700j != null) {
            return this.f1696f;
        }
        if (f1691a) {
            return super.getPopupContext();
        }
        return null;
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        if (this.f1700j != null) {
            this.f1700j.m2734a(drawable);
        } else if (f1692b) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i) {
        setPopupBackgroundDrawable(ContextCompat.m77a(getPopupContext(), i));
    }

    public Drawable getPopupBackground() {
        if (this.f1700j != null) {
            return this.f1700j.m2743d();
        }
        if (f1692b) {
            return super.getPopupBackground();
        }
        return null;
    }

    public void setDropDownVerticalOffset(int i) {
        if (this.f1700j != null) {
            this.f1700j.m2742c(i);
        } else if (f1692b) {
            super.setDropDownVerticalOffset(i);
        }
    }

    public int getDropDownVerticalOffset() {
        if (this.f1700j != null) {
            return this.f1700j.m2749g();
        }
        if (f1692b) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public void setDropDownHorizontalOffset(int i) {
        if (this.f1700j != null) {
            this.f1700j.m2740b(i);
        } else if (f1692b) {
            super.setDropDownHorizontalOffset(i);
        }
    }

    public int getDropDownHorizontalOffset() {
        if (this.f1700j != null) {
            return this.f1700j.m2747f();
        }
        if (f1692b) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public void setDropDownWidth(int i) {
        if (this.f1700j != null) {
            this.f1701k = i;
        } else if (f1692b) {
            super.setDropDownWidth(i);
        }
    }

    public int getDropDownWidth() {
        if (this.f1700j != null) {
            return this.f1701k;
        }
        if (f1692b) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (this.f1699i) {
            super.setAdapter(spinnerAdapter);
            if (this.f1700j != null) {
                this.f1700j.m3025a(new AppCompatSpinner(spinnerAdapter, (this.f1696f == null ? getContext() : this.f1696f).getTheme()));
                return;
            }
            return;
        }
        this.f1698h = spinnerAdapter;
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1700j != null && this.f1700j.m2755k()) {
            this.f1700j.m2753i();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f1697g == null || !this.f1697g.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f1700j != null && MeasureSpec.getMode(i) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), m3030a(getAdapter(), getBackground())), MeasureSpec.getSize(i)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        if (this.f1700j == null) {
            return super.performClick();
        }
        if (!this.f1700j.m2755k()) {
            this.f1700j.m3028c();
        }
        return true;
    }

    public void setPrompt(CharSequence charSequence) {
        if (this.f1700j != null) {
            this.f1700j.m3026a(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public CharSequence getPrompt() {
        return this.f1700j != null ? this.f1700j.m3024a() : super.getPrompt();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1695e != null) {
            this.f1695e.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1695e != null) {
            this.f1695e.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1695e != null) {
            this.f1695e.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1695e != null ? this.f1695e.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1695e != null) {
            this.f1695e.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1695e != null ? this.f1695e.m2958b() : null;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1695e != null) {
            this.f1695e.m2960c();
        }
    }

    private int m3030a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view = null;
        int i = 0;
        max = 0;
        while (max2 < min) {
            View view2;
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != max) {
                view2 = null;
            } else {
                itemViewType = max;
                view2 = view;
            }
            view = spinnerAdapter.getView(max2, view2, this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new LayoutParams(-2, -2));
            }
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            i = Math.max(i, view.getMeasuredWidth());
            max2++;
            max = itemViewType;
        }
        if (drawable == null) {
            return i;
        }
        drawable.getPadding(this.f1702l);
        return (this.f1702l.left + this.f1702l.right) + i;
    }
}
