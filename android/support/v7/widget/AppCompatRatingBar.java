package android.support.v7.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.p006f.af;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.RatingBar;

/* renamed from: android.support.v7.widget.u */
public class AppCompatRatingBar extends RatingBar {
    private AppCompatProgressBarHelper f1672a;
    private AppCompatDrawableManager f1673b;

    public AppCompatRatingBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.ratingBarStyle);
    }

    public AppCompatRatingBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1673b = AppCompatDrawableManager.m2981a();
        this.f1672a = new AppCompatProgressBarHelper(this, this.f1673b);
        this.f1672a.m3016a(attributeSet, i);
    }

    protected synchronized void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        Bitmap a = this.f1672a.m3015a();
        if (a != null) {
            setMeasuredDimension(af.m1172a(a.getWidth() * getNumStars(), i, 0), getMeasuredHeight());
        }
    }
}
