package android.support.v7.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.p006f.au;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ActionMode;
import android.support.v7.view.menu.MenuBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends AbsActionBarView {
    private CharSequence f1190g;
    private CharSequence f1191h;
    private View f1192i;
    private View f1193j;
    private LinearLayout f1194k;
    private TextView f1195l;
    private TextView f1196m;
    private int f1197n;
    private int f1198o;
    private boolean f1199p;
    private int f1200q;

    /* renamed from: android.support.v7.widget.ActionBarContextView.1 */
    class C00091 implements OnClickListener {
        final /* synthetic */ ActionMode f1180a;
        final /* synthetic */ ActionBarContextView f1181b;

        C00091(ActionBarContextView actionBarContextView, ActionMode actionMode) {
            this.f1181b = actionBarContextView;
            this.f1180a = actionMode;
        }

        public void onClick(View view) {
            this.f1180a.m2083c();
        }
    }

    public /* bridge */ /* synthetic */ au m2466a(int i, long j) {
        return super.m2463a(i, j);
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public /* bridge */ /* synthetic */ boolean onHoverEvent(MotionEvent motionEvent) {
        return super.onHoverEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ar a = ar.m2811a(context, attributeSet, R.ActionMode, i, 0);
        setBackgroundDrawable(a.m2814a(R.ActionMode_background));
        this.f1197n = a.m2827g(R.ActionMode_titleTextStyle, 0);
        this.f1198o = a.m2827g(R.ActionMode_subtitleTextStyle, 0);
        this.e = a.m2825f(R.ActionMode_height, 0);
        this.f1200q = a.m2827g(R.ActionMode_closeItemLayout, R.abc_action_mode_close_item_material);
        a.m2815a();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            this.d.m2924e();
            this.d.m2926g();
        }
    }

    public void setContentHeight(int i) {
        this.e = i;
    }

    public void setCustomView(View view) {
        if (this.f1193j != null) {
            removeView(this.f1193j);
        }
        this.f1193j = view;
        if (!(view == null || this.f1194k == null)) {
            removeView(this.f1194k);
            this.f1194k = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setTitle(CharSequence charSequence) {
        this.f1190g = charSequence;
        m2465e();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1191h = charSequence;
        m2465e();
    }

    public CharSequence getTitle() {
        return this.f1190g;
    }

    public CharSequence getSubtitle() {
        return this.f1191h;
    }

    private void m2465e() {
        int i;
        int i2 = 8;
        Object obj = 1;
        if (this.f1194k == null) {
            LayoutInflater.from(getContext()).inflate(R.abc_action_bar_title_item, this);
            this.f1194k = (LinearLayout) getChildAt(getChildCount() - 1);
            this.f1195l = (TextView) this.f1194k.findViewById(R.action_bar_title);
            this.f1196m = (TextView) this.f1194k.findViewById(R.action_bar_subtitle);
            if (this.f1197n != 0) {
                this.f1195l.setTextAppearance(getContext(), this.f1197n);
            }
            if (this.f1198o != 0) {
                this.f1196m.setTextAppearance(getContext(), this.f1198o);
            }
        }
        this.f1195l.setText(this.f1190g);
        this.f1196m.setText(this.f1191h);
        Object obj2 = !TextUtils.isEmpty(this.f1190g) ? 1 : null;
        if (TextUtils.isEmpty(this.f1191h)) {
            obj = null;
        }
        TextView textView = this.f1196m;
        if (obj != null) {
            i = 0;
        } else {
            i = 8;
        }
        textView.setVisibility(i);
        LinearLayout linearLayout = this.f1194k;
        if (!(obj2 == null && obj == null)) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (this.f1194k.getParent() == null) {
            addView(this.f1194k);
        }
    }

    public void m2467a(ActionMode actionMode) {
        if (this.f1192i == null) {
            this.f1192i = LayoutInflater.from(getContext()).inflate(this.f1200q, this, false);
            addView(this.f1192i);
        } else if (this.f1192i.getParent() == null) {
            addView(this.f1192i);
        }
        this.f1192i.findViewById(R.action_mode_close_button).setOnClickListener(new C00091(this, actionMode));
        MenuBuilder menuBuilder = (MenuBuilder) actionMode.m2080b();
        if (this.d != null) {
            this.d.m2925f();
        }
        this.d = new ActionMenuPresenter(getContext());
        this.d.m2921c(true);
        LayoutParams layoutParams = new LayoutParams(-2, -1);
        menuBuilder.m2338a(this.d, this.b);
        this.c = (ActionMenuView) this.d.m2906a((ViewGroup) this);
        this.c.setBackgroundDrawable(null);
        addView(this.c, layoutParams);
    }

    public void m2469b() {
        if (this.f1192i == null) {
            m2470c();
        }
    }

    public void m2470c() {
        removeAllViews();
        this.f1193j = null;
        this.c = null;
    }

    public boolean m2468a() {
        if (this.d != null) {
            return this.d.m2923d();
        }
        return false;
    }

    protected LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(-1, -2);
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new MarginLayoutParams(getContext(), attributeSet);
    }

    protected void onMeasure(int i, int i2) {
        int i3 = 1073741824;
        int i4 = 0;
        if (MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (MeasureSpec.getMode(i2) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        } else {
            int a;
            int size = MeasureSpec.getSize(i);
            int size2 = this.e > 0 ? this.e : MeasureSpec.getSize(i2);
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i5 = size2 - paddingTop;
            int makeMeasureSpec = MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE);
            if (this.f1192i != null) {
                a = m2461a(this.f1192i, paddingLeft, makeMeasureSpec, 0);
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.f1192i.getLayoutParams();
                paddingLeft = a - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.c != null && this.c.getParent() == this) {
                paddingLeft = m2461a(this.c, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.f1194k != null && this.f1193j == null) {
                if (this.f1199p) {
                    this.f1194k.measure(MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    a = this.f1194k.getMeasuredWidth();
                    makeMeasureSpec = a <= paddingLeft ? 1 : 0;
                    if (makeMeasureSpec != 0) {
                        paddingLeft -= a;
                    }
                    this.f1194k.setVisibility(makeMeasureSpec != 0 ? 0 : 8);
                } else {
                    paddingLeft = m2461a(this.f1194k, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.f1193j != null) {
                int min;
                LayoutParams layoutParams = this.f1193j.getLayoutParams();
                if (layoutParams.width != -2) {
                    makeMeasureSpec = 1073741824;
                } else {
                    makeMeasureSpec = Integer.MIN_VALUE;
                }
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i3 = Integer.MIN_VALUE;
                }
                if (layoutParams.height >= 0) {
                    min = Math.min(layoutParams.height, i5);
                } else {
                    min = i5;
                }
                this.f1193j.measure(MeasureSpec.makeMeasureSpec(paddingLeft, makeMeasureSpec), MeasureSpec.makeMeasureSpec(min, i3));
            }
            if (this.e <= 0) {
                makeMeasureSpec = getChildCount();
                size2 = 0;
                while (i4 < makeMeasureSpec) {
                    paddingLeft = getChildAt(i4).getMeasuredHeight() + paddingTop;
                    if (paddingLeft <= size2) {
                        paddingLeft = size2;
                    }
                    i4++;
                    size2 = paddingLeft;
                }
                setMeasuredDimension(size, size2);
                return;
            }
            setMeasuredDimension(size, size2);
        }
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        boolean a = au.m2883a(this);
        int paddingRight = a ? (i3 - i) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        if (this.f1192i == null || this.f1192i.getVisibility() == 8) {
            i5 = paddingRight;
        } else {
            MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.f1192i.getLayoutParams();
            i5 = a ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i6 = a ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            i5 = AbsActionBarView.m2458a(paddingRight, i5, a);
            i5 = AbsActionBarView.m2458a(m2462a(this.f1192i, i5, paddingTop, paddingTop2, a) + i5, i6, a);
        }
        if (!(this.f1194k == null || this.f1193j != null || this.f1194k.getVisibility() == 8)) {
            i5 += m2462a(this.f1194k, i5, paddingTop, paddingTop2, a);
        }
        if (this.f1193j != null) {
            int a2 = m2462a(this.f1193j, i5, paddingTop, paddingTop2, a) + i5;
        }
        i5 = a ? getPaddingLeft() : (i3 - i) - getPaddingRight();
        if (this.c != null) {
            a2 = m2462a(this.c, i5, paddingTop, paddingTop2, !a) + i5;
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (VERSION.SDK_INT < 14) {
            return;
        }
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.f1190g);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    public void setTitleOptional(boolean z) {
        if (z != this.f1199p) {
            requestLayout();
        }
        this.f1199p = z;
    }

    public boolean m2471d() {
        return this.f1199p;
    }
}
