package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.support.v4.p006f.az;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.ActionMenuItem;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.widget.Toolbar.C0031b;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window.Callback;

/* compiled from: ToolbarWidgetWrapper */
public class as implements ac {
    private Toolbar f1537a;
    private int f1538b;
    private View f1539c;
    private View f1540d;
    private Drawable f1541e;
    private Drawable f1542f;
    private Drawable f1543g;
    private boolean f1544h;
    private CharSequence f1545i;
    private CharSequence f1546j;
    private CharSequence f1547k;
    private Callback f1548l;
    private boolean f1549m;
    private ActionMenuPresenter f1550n;
    private int f1551o;
    private final AppCompatDrawableManager f1552p;
    private int f1553q;
    private Drawable f1554r;

    /* renamed from: android.support.v7.widget.as.1 */
    class ToolbarWidgetWrapper implements OnClickListener {
        final ActionMenuItem f1532a;
        final /* synthetic */ as f1533b;

        ToolbarWidgetWrapper(as asVar) {
            this.f1533b = asVar;
            this.f1532a = new ActionMenuItem(this.f1533b.f1537a.getContext(), 0, 16908332, 0, 0, this.f1533b.f1545i);
        }

        public void onClick(View view) {
            if (this.f1533b.f1548l != null && this.f1533b.f1549m) {
                this.f1533b.f1548l.onMenuItemSelected(0, this.f1532a);
            }
        }
    }

    /* renamed from: android.support.v7.widget.as.2 */
    class ToolbarWidgetWrapper extends az {
        final /* synthetic */ int f1534a;
        final /* synthetic */ as f1535b;
        private boolean f1536c;

        ToolbarWidgetWrapper(as asVar, int i) {
            this.f1535b = asVar;
            this.f1534a = i;
            this.f1536c = false;
        }

        public void m2828a(View view) {
            this.f1535b.f1537a.setVisibility(0);
        }

        public void m2829b(View view) {
            if (!this.f1536c) {
                this.f1535b.f1537a.setVisibility(this.f1534a);
            }
        }

        public void m2830c(View view) {
            this.f1536c = true;
        }
    }

    public as(Toolbar toolbar, boolean z) {
        this(toolbar, z, R.abc_action_bar_up_description, R.abc_ic_ab_back_mtrl_am_alpha);
    }

    public as(Toolbar toolbar, boolean z, int i, int i2) {
        this.f1551o = 0;
        this.f1553q = 0;
        this.f1537a = toolbar;
        this.f1545i = toolbar.getTitle();
        this.f1546j = toolbar.getSubtitle();
        this.f1544h = this.f1545i != null;
        this.f1543g = toolbar.getNavigationIcon();
        if (z) {
            ar a = ar.m2811a(toolbar.getContext(), null, R.ActionBar, R.actionBarStyle, 0);
            CharSequence c = a.m2820c(R.ActionBar_title);
            if (!TextUtils.isEmpty(c)) {
                m2854b(c);
            }
            c = a.m2820c(R.ActionBar_subtitle);
            if (!TextUtils.isEmpty(c)) {
                m2858c(c);
            }
            Drawable a2 = a.m2814a(R.ActionBar_logo);
            if (a2 != null) {
                m2857c(a2);
            }
            a2 = a.m2814a(R.ActionBar_icon);
            if (this.f1543g == null && a2 != null) {
                m2843a(a2);
            }
            a2 = a.m2814a(R.ActionBar_homeAsUpIndicator);
            if (a2 != null) {
                m2862d(a2);
            }
            m2856c(a.m2813a(R.ActionBar_displayOptions, 0));
            int g = a.m2827g(R.ActionBar_customNavigationLayout, 0);
            if (g != 0) {
                m2847a(LayoutInflater.from(this.f1537a.getContext()).inflate(g, this.f1537a, false));
                m2856c(this.f1538b | 16);
            }
            g = a.m2825f(R.ActionBar_height, 0);
            if (g > 0) {
                LayoutParams layoutParams = this.f1537a.getLayoutParams();
                layoutParams.height = g;
                this.f1537a.setLayoutParams(layoutParams);
            }
            g = a.m2821d(R.ActionBar_contentInsetStart, -1);
            int d = a.m2821d(R.ActionBar_contentInsetEnd, -1);
            if (g >= 0 || d >= 0) {
                this.f1537a.m2654a(Math.max(g, 0), Math.max(d, 0));
            }
            g = a.m2827g(R.ActionBar_titleTextStyle, 0);
            if (g != 0) {
                this.f1537a.m2655a(this.f1537a.getContext(), g);
            }
            g = a.m2827g(R.ActionBar_subtitleTextStyle, 0);
            if (g != 0) {
                this.f1537a.m2659b(this.f1537a.getContext(), g);
            }
            int g2 = a.m2827g(R.ActionBar_popupTheme, 0);
            if (g2 != 0) {
                this.f1537a.setPopupTheme(g2);
            }
            a.m2815a();
        } else {
            this.f1538b = m2836s();
        }
        this.f1552p = AppCompatDrawableManager.m2981a();
        m2861d(i);
        this.f1547k = this.f1537a.getNavigationContentDescription();
        m2853b(this.f1552p.m3004a(m2851b(), i2));
        this.f1537a.setNavigationOnClickListener(new ToolbarWidgetWrapper(this));
    }

    public void m2861d(int i) {
        if (i != this.f1553q) {
            this.f1553q = i;
            if (TextUtils.isEmpty(this.f1537a.getNavigationContentDescription())) {
                m2865e(this.f1553q);
            }
        }
    }

    public void m2853b(Drawable drawable) {
        if (this.f1554r != drawable) {
            this.f1554r = drawable;
            m2839v();
        }
    }

    private int m2836s() {
        if (this.f1537a.getNavigationIcon() != null) {
            return 15;
        }
        return 11;
    }

    public ViewGroup m2841a() {
        return this.f1537a;
    }

    public Context m2851b() {
        return this.f1537a.getContext();
    }

    public boolean m2859c() {
        return this.f1537a.m2665g();
    }

    public void m2860d() {
        this.f1537a.m2666h();
    }

    public void m2848a(Callback callback) {
        this.f1548l = callback;
    }

    public void m2849a(CharSequence charSequence) {
        if (!this.f1544h) {
            m2835e(charSequence);
        }
    }

    public CharSequence m2864e() {
        return this.f1537a.getTitle();
    }

    public void m2854b(CharSequence charSequence) {
        this.f1544h = true;
        m2835e(charSequence);
    }

    private void m2835e(CharSequence charSequence) {
        this.f1545i = charSequence;
        if ((this.f1538b & 8) != 0) {
            this.f1537a.setTitle(charSequence);
        }
    }

    public void m2858c(CharSequence charSequence) {
        this.f1546j = charSequence;
        if ((this.f1538b & 8) != 0) {
            this.f1537a.setSubtitle(charSequence);
        }
    }

    public void m2866f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void m2867g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void m2842a(int i) {
        m2843a(i != 0 ? this.f1552p.m3004a(m2851b(), i) : null);
    }

    public void m2843a(Drawable drawable) {
        this.f1541e = drawable;
        m2837t();
    }

    public void m2852b(int i) {
        m2857c(i != 0 ? this.f1552p.m3004a(m2851b(), i) : null);
    }

    public void m2857c(Drawable drawable) {
        this.f1542f = drawable;
        m2837t();
    }

    private void m2837t() {
        Drawable drawable = null;
        if ((this.f1538b & 2) != 0) {
            drawable = (this.f1538b & 1) != 0 ? this.f1542f != null ? this.f1542f : this.f1541e : this.f1541e;
        }
        this.f1537a.setLogo(drawable);
    }

    public boolean m2868h() {
        return this.f1537a.m2658a();
    }

    public boolean m2869i() {
        return this.f1537a.m2660b();
    }

    public boolean m2870j() {
        return this.f1537a.m2661c();
    }

    public boolean m2871k() {
        return this.f1537a.m2662d();
    }

    public boolean m2872l() {
        return this.f1537a.m2663e();
    }

    public void m2873m() {
        this.f1549m = true;
    }

    public void m2846a(Menu menu, MenuPresenter menuPresenter) {
        if (this.f1550n == null) {
            this.f1550n = new ActionMenuPresenter(this.f1537a.getContext());
            this.f1550n.m2286a(R.action_menu_presenter);
        }
        this.f1550n.m2290a(menuPresenter);
        this.f1537a.m2656a((MenuBuilder) menu, this.f1550n);
    }

    public void m2874n() {
        this.f1537a.m2664f();
    }

    public int m2875o() {
        return this.f1538b;
    }

    public void m2856c(int i) {
        int i2 = this.f1538b ^ i;
        this.f1538b = i;
        if (i2 != 0) {
            if ((i2 & 4) != 0) {
                if ((i & 4) != 0) {
                    m2839v();
                    m2838u();
                } else {
                    this.f1537a.setNavigationIcon(null);
                }
            }
            if ((i2 & 3) != 0) {
                m2837t();
            }
            if ((i2 & 8) != 0) {
                if ((i & 8) != 0) {
                    this.f1537a.setTitle(this.f1545i);
                    this.f1537a.setSubtitle(this.f1546j);
                } else {
                    this.f1537a.setTitle(null);
                    this.f1537a.setSubtitle(null);
                }
            }
            if ((i2 & 16) != 0 && this.f1540d != null) {
                if ((i & 16) != 0) {
                    this.f1537a.addView(this.f1540d);
                } else {
                    this.f1537a.removeView(this.f1540d);
                }
            }
        }
    }

    public void m2845a(ak akVar) {
        if (this.f1539c != null && this.f1539c.getParent() == this.f1537a) {
            this.f1537a.removeView(this.f1539c);
        }
        this.f1539c = akVar;
        if (akVar != null && this.f1551o == 2) {
            this.f1537a.addView(this.f1539c, 0);
            C0031b c0031b = (C0031b) this.f1539c.getLayoutParams();
            c0031b.width = -2;
            c0031b.height = -2;
            c0031b.a = 8388691;
            akVar.setAllowCollapse(true);
        }
    }

    public void m2850a(boolean z) {
        this.f1537a.setCollapsible(z);
    }

    public void m2855b(boolean z) {
    }

    public int m2876p() {
        return this.f1551o;
    }

    public void m2847a(View view) {
        if (!(this.f1540d == null || (this.f1538b & 16) == 0)) {
            this.f1537a.removeView(this.f1540d);
        }
        this.f1540d = view;
        if (view != null && (this.f1538b & 16) != 0) {
            this.f1537a.addView(this.f1540d);
        }
    }

    public au m2840a(int i, long j) {
        return af.m1199k(this.f1537a).m1353a(i == 0 ? 1.0f : 0.0f).m1354a(j).m1355a(new ToolbarWidgetWrapper(this, i));
    }

    public void m2862d(Drawable drawable) {
        this.f1543g = drawable;
        m2839v();
    }

    public void m2863d(CharSequence charSequence) {
        this.f1547k = charSequence;
        m2838u();
    }

    public void m2865e(int i) {
        m2863d(i == 0 ? null : m2851b().getString(i));
    }

    private void m2838u() {
        if ((this.f1538b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.f1547k)) {
            this.f1537a.setNavigationContentDescription(this.f1553q);
        } else {
            this.f1537a.setNavigationContentDescription(this.f1547k);
        }
    }

    private void m2839v() {
        if ((this.f1538b & 4) != 0) {
            this.f1537a.setNavigationIcon(this.f1543g != null ? this.f1543g : this.f1554r);
        }
    }

    public int m2877q() {
        return this.f1537a.getVisibility();
    }

    public void m2844a(MenuPresenter menuPresenter, MenuBuilder.MenuBuilder menuBuilder) {
        this.f1537a.m2657a(menuPresenter, menuBuilder);
    }

    public Menu m2878r() {
        return this.f1537a.getMenu();
    }
}
