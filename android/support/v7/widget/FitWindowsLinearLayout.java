package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.ae.FitWindowsViewGroup;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class FitWindowsLinearLayout extends LinearLayout implements ae {
    private FitWindowsViewGroup f1296a;

    public FitWindowsLinearLayout(Context context) {
        super(context);
    }

    public FitWindowsLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnFitSystemWindowsListener(FitWindowsViewGroup fitWindowsViewGroup) {
        this.f1296a = fitWindowsViewGroup;
    }

    protected boolean fitSystemWindows(Rect rect) {
        if (this.f1296a != null) {
            this.f1296a.m2003a(rect);
        }
        return super.fitSystemWindows(rect);
    }
}
