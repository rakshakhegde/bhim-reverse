package android.support.v7.widget;

import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* renamed from: android.support.v7.widget.w */
class AppCompatSeekBarHelper extends AppCompatProgressBarHelper {
    private static final int[] f1676b;
    private final SeekBar f1677c;

    static {
        f1676b = new int[]{16843074};
    }

    AppCompatSeekBarHelper(SeekBar seekBar, AppCompatDrawableManager appCompatDrawableManager) {
        super(seekBar, appCompatDrawableManager);
        this.f1677c = seekBar;
    }

    void m3017a(AttributeSet attributeSet, int i) {
        super.m3016a(attributeSet, i);
        ar a = ar.m2811a(this.f1677c.getContext(), attributeSet, f1676b, i, 0);
        Drawable b = a.m2818b(0);
        if (b != null) {
            this.f1677c.setThumb(b);
        }
        a.m2815a();
    }
}
