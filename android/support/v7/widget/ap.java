package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;

/* compiled from: TintInfo */
class ap {
    public ColorStateList f1525a;
    public Mode f1526b;
    public boolean f1527c;
    public boolean f1528d;

    ap() {
    }

    void m2809a() {
        this.f1525a = null;
        this.f1528d = false;
        this.f1526b = null;
        this.f1527c = false;
    }
}
