package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.widget.TintableCompoundButton;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.RadioButton;

/* renamed from: android.support.v7.widget.t */
public class AppCompatRadioButton extends RadioButton implements TintableCompoundButton {
    private AppCompatDrawableManager f1670a;
    private AppCompatCompoundButtonHelper f1671b;

    public AppCompatRadioButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.radioButtonStyle);
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1670a = AppCompatDrawableManager.m2981a();
        this.f1671b = new AppCompatCompoundButtonHelper(this, this.f1670a);
        this.f1671b.m2965a(attributeSet, i);
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        if (this.f1671b != null) {
            this.f1671b.m2967c();
        }
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(this.f1670a != null ? this.f1670a.m3004a(getContext(), i) : ContextCompat.m77a(getContext(), i));
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return this.f1671b != null ? this.f1671b.m2961a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        if (this.f1671b != null) {
            this.f1671b.m2963a(colorStateList);
        }
    }

    public ColorStateList getSupportButtonTintList() {
        return this.f1671b != null ? this.f1671b.m2962a() : null;
    }

    public void setSupportButtonTintMode(Mode mode) {
        if (this.f1671b != null) {
            this.f1671b.m2964a(mode);
        }
    }

    public Mode getSupportButtonTintMode() {
        return this.f1671b != null ? this.f1671b.m2966b() : null;
    }
}
