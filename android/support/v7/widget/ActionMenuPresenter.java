package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v4.p006f.ActionProvider.ActionProvider;
import android.support.v7.p014b.R.R;
import android.support.v7.p018e.ActionBarTransition;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.ActionMenuItemView.C0007b;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionMenuView.C0008a;
import android.support.v7.widget.ag.ListPopupWindow;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import java.util.ArrayList;

/* renamed from: android.support.v7.widget.d */
class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider {
    private ActionMenuPresenter f1571A;
    final ActionMenuPresenter f1572g;
    int f1573h;
    private ActionMenuPresenter f1574i;
    private Drawable f1575j;
    private boolean f1576k;
    private boolean f1577l;
    private boolean f1578m;
    private int f1579n;
    private int f1580o;
    private int f1581p;
    private boolean f1582q;
    private boolean f1583r;
    private boolean f1584s;
    private boolean f1585t;
    private int f1586u;
    private final SparseBooleanArray f1587v;
    private View f1588w;
    private ActionMenuPresenter f1589x;
    private ActionMenuPresenter f1590y;
    private ActionMenuPresenter f1591z;

    /* renamed from: android.support.v7.widget.d.a */
    private class ActionMenuPresenter extends MenuPopupHelper {
        final /* synthetic */ ActionMenuPresenter f1558c;
        private SubMenuBuilder f1559d;

        public ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter, Context context, SubMenuBuilder subMenuBuilder) {
            boolean z = false;
            this.f1558c = actionMenuPresenter;
            super(context, subMenuBuilder, null, false, R.actionOverflowMenuStyle);
            this.f1559d = subMenuBuilder;
            if (!((MenuItemImpl) subMenuBuilder.getItem()).m2399j()) {
                m2432a(actionMenuPresenter.f1574i == null ? (View) actionMenuPresenter.f : actionMenuPresenter.f1574i);
            }
            m2431a(actionMenuPresenter.f1572g);
            int size = subMenuBuilder.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = subMenuBuilder.getItem(i);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
            }
            m2433a(z);
        }

        public void onDismiss() {
            super.onDismiss();
            this.f1558c.f1590y = null;
            this.f1558c.f1573h = 0;
        }
    }

    /* renamed from: android.support.v7.widget.d.b */
    private class ActionMenuPresenter extends C0007b {
        final /* synthetic */ ActionMenuPresenter f1560a;

        private ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter) {
            this.f1560a = actionMenuPresenter;
        }

        public ag m2885a() {
            return this.f1560a.f1590y != null ? this.f1560a.f1590y.m2439c() : null;
        }
    }

    /* renamed from: android.support.v7.widget.d.c */
    private class ActionMenuPresenter implements Runnable {
        final /* synthetic */ ActionMenuPresenter f1561a;
        private ActionMenuPresenter f1562b;

        public ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter, ActionMenuPresenter actionMenuPresenter2) {
            this.f1561a = actionMenuPresenter;
            this.f1562b = actionMenuPresenter2;
        }

        public void run() {
            this.f1561a.c.m2358f();
            View view = (View) this.f1561a.f;
            if (!(view == null || view.getWindowToken() == null || !this.f1562b.m2440d())) {
                this.f1561a.f1589x = this.f1562b;
            }
            this.f1561a.f1591z = null;
        }
    }

    /* renamed from: android.support.v7.widget.d.d */
    private class ActionMenuPresenter extends AppCompatImageView implements C0008a {
        final /* synthetic */ ActionMenuPresenter f1567a;
        private final float[] f1568b;

        /* renamed from: android.support.v7.widget.d.d.1 */
        class ActionMenuPresenter extends ListPopupWindow {
            final /* synthetic */ ActionMenuPresenter f1563a;
            final /* synthetic */ ActionMenuPresenter f1564b;

            ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter, View view, ActionMenuPresenter actionMenuPresenter2) {
                this.f1564b = actionMenuPresenter;
                this.f1563a = actionMenuPresenter2;
                super(view);
            }

            public ag m2886a() {
                if (this.f1564b.f1567a.f1589x == null) {
                    return null;
                }
                return this.f1564b.f1567a.f1589x.m2439c();
            }

            public boolean m2887b() {
                this.f1564b.f1567a.m2923d();
                return true;
            }

            public boolean m2888c() {
                if (this.f1564b.f1567a.f1591z != null) {
                    return false;
                }
                this.f1564b.f1567a.m2924e();
                return true;
            }
        }

        public ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter, Context context) {
            this.f1567a = actionMenuPresenter;
            super(context, null, R.actionOverflowButtonStyle);
            this.f1568b = new float[2];
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            setOnTouchListener(new ActionMenuPresenter(this, this, actionMenuPresenter));
        }

        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                this.f1567a.m2923d();
            }
            return true;
        }

        public boolean m2889c() {
            return false;
        }

        public boolean m2890d() {
            return false;
        }

        protected boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                width = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                height = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.m658a(background, width - max, height - max, width + max, height + max);
            }
            return frame;
        }
    }

    /* renamed from: android.support.v7.widget.d.e */
    private class ActionMenuPresenter extends MenuPopupHelper {
        final /* synthetic */ ActionMenuPresenter f1569c;

        public ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter, Context context, MenuBuilder menuBuilder, View view, boolean z) {
            this.f1569c = actionMenuPresenter;
            super(context, menuBuilder, view, z, R.actionOverflowMenuStyle);
            m2428a(8388613);
            m2431a(actionMenuPresenter.f1572g);
        }

        public void onDismiss() {
            super.onDismiss();
            if (this.f1569c.c != null) {
                this.f1569c.c.close();
            }
            this.f1569c.f1589x = null;
        }
    }

    /* renamed from: android.support.v7.widget.d.f */
    private class ActionMenuPresenter implements MenuPresenter {
        final /* synthetic */ ActionMenuPresenter f1570a;

        private ActionMenuPresenter(ActionMenuPresenter actionMenuPresenter) {
            this.f1570a = actionMenuPresenter;
        }

        public boolean m2892a(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            this.f1570a.f1573h = ((SubMenuBuilder) menuBuilder).getItem().getItemId();
            MenuPresenter a = this.f1570a.m2283a();
            return a != null ? a.m2014a(menuBuilder) : false;
        }

        public void m2891a(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                ((SubMenuBuilder) menuBuilder).m2453p().m2341a(false);
            }
            MenuPresenter a = this.f1570a.m2283a();
            if (a != null) {
                a.m2013a(menuBuilder, z);
            }
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, R.abc_action_menu_layout, R.abc_action_menu_item_layout);
        this.f1587v = new SparseBooleanArray();
        this.f1572g = new ActionMenuPresenter();
    }

    public void m2908a(Context context, MenuBuilder menuBuilder) {
        super.m2287a(context, menuBuilder);
        Resources resources = context.getResources();
        ActionBarPolicy a = ActionBarPolicy.m2164a(context);
        if (!this.f1578m) {
            this.f1577l = a.m2166b();
        }
        if (!this.f1584s) {
            this.f1579n = a.m2167c();
        }
        if (!this.f1582q) {
            this.f1581p = a.m2165a();
        }
        int i = this.f1579n;
        if (this.f1577l) {
            if (this.f1574i == null) {
                this.f1574i = new ActionMenuPresenter(this, this.a);
                if (this.f1576k) {
                    this.f1574i.setImageDrawable(this.f1575j);
                    this.f1575j = null;
                    this.f1576k = false;
                }
                int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
                this.f1574i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.f1574i.getMeasuredWidth();
        } else {
            this.f1574i = null;
        }
        this.f1580o = i;
        this.f1586u = (int) (56.0f * resources.getDisplayMetrics().density);
        this.f1588w = null;
    }

    public void m2909a(Configuration configuration) {
        if (!this.f1582q) {
            this.f1581p = this.b.getResources().getInteger(R.abc_max_action_buttons);
        }
        if (this.c != null) {
            this.c.m2349b(true);
        }
    }

    public void m2921c(boolean z) {
        this.f1577l = z;
        this.f1578m = true;
    }

    public void m2922d(boolean z) {
        this.f1585t = z;
    }

    public void m2910a(Drawable drawable) {
        if (this.f1574i != null) {
            this.f1574i.setImageDrawable(drawable);
            return;
        }
        this.f1576k = true;
        this.f1575j = drawable;
    }

    public Drawable m2920c() {
        if (this.f1574i != null) {
            return this.f1574i.getDrawable();
        }
        if (this.f1576k) {
            return this.f1575j;
        }
        return null;
    }

    public MenuView m2906a(ViewGroup viewGroup) {
        MenuView a = super.m2284a(viewGroup);
        ((ActionMenuView) a).setPresenter(this);
        return a;
    }

    public View m2907a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.m2403n()) {
            actionView = super.m2285a(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.m2541a(layoutParams));
        }
        return actionView;
    }

    public void m2912a(MenuItemImpl menuItemImpl, MenuView.MenuView menuView) {
        menuView.m2247a(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) menuView;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f);
        if (this.f1571A == null) {
            this.f1571A = new ActionMenuPresenter();
        }
        actionMenuItemView.setPopupCallback(this.f1571A);
    }

    public boolean m2915a(int i, MenuItemImpl menuItemImpl) {
        return menuItemImpl.m2399j();
    }

    public void m2918b(boolean z) {
        int i;
        int i2 = 1;
        int i3 = 0;
        ViewGroup viewGroup = (ViewGroup) ((View) this.f).getParent();
        if (viewGroup != null) {
            ActionBarTransition.m2163a(viewGroup);
        }
        super.m2297b(z);
        ((View) this.f).requestLayout();
        if (this.c != null) {
            ArrayList k = this.c.m2363k();
            int size = k.size();
            for (i = 0; i < size; i++) {
                android.support.v4.p006f.ActionProvider a = ((MenuItemImpl) k.get(i)).m2381a();
                if (a != null) {
                    a.m1408a((ActionProvider) this);
                }
            }
        }
        ArrayList l = this.c != null ? this.c.m2364l() : null;
        if (this.f1577l && l != null) {
            i = l.size();
            if (i == 1) {
                int i4;
                if (((MenuItemImpl) l.get(0)).isActionViewExpanded()) {
                    i4 = 0;
                } else {
                    i4 = 1;
                }
                i3 = i4;
            } else {
                if (i <= 0) {
                    i2 = 0;
                }
                i3 = i2;
            }
        }
        if (i3 != 0) {
            if (this.f1574i == null) {
                this.f1574i = new ActionMenuPresenter(this, this.a);
            }
            viewGroup = (ViewGroup) this.f1574i.getParent();
            if (viewGroup != this.f) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f1574i);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f;
                actionMenuView.addView(this.f1574i, actionMenuView.m2550c());
            }
        } else if (this.f1574i != null && this.f1574i.getParent() == this.f) {
            ((ViewGroup) this.f).removeView(this.f1574i);
        }
        ((ActionMenuView) this.f).setOverflowReserved(this.f1577l);
    }

    public boolean m2917a(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.f1574i) {
            return false;
        }
        return super.m2295a(viewGroup, i);
    }

    public boolean m2916a(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (subMenuBuilder2.m2454s() != this.c) {
            subMenuBuilder2 = (SubMenuBuilder) subMenuBuilder2.m2454s();
        }
        View a = m2897a(subMenuBuilder2.getItem());
        if (a == null) {
            if (this.f1574i == null) {
                return false;
            }
            a = this.f1574i;
        }
        this.f1573h = subMenuBuilder.getItem().getItemId();
        this.f1590y = new ActionMenuPresenter(this, this.b, subMenuBuilder);
        this.f1590y.m2432a(a);
        this.f1590y.m2427a();
        super.m2294a(subMenuBuilder);
        return true;
    }

    private View m2897a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof MenuView.MenuView) && ((MenuView.MenuView) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean m2923d() {
        if (!this.f1577l || m2927h() || this.c == null || this.f == null || this.f1591z != null || this.c.m2364l().isEmpty()) {
            return false;
        }
        this.f1591z = new ActionMenuPresenter(this, new ActionMenuPresenter(this, this.b, this.c, this.f1574i, true));
        ((View) this.f).post(this.f1591z);
        super.m2294a(null);
        return true;
    }

    public boolean m2924e() {
        if (this.f1591z == null || this.f == null) {
            MenuPopupHelper menuPopupHelper = this.f1589x;
            if (menuPopupHelper == null) {
                return false;
            }
            menuPopupHelper.m2441e();
            return true;
        }
        ((View) this.f).removeCallbacks(this.f1591z);
        this.f1591z = null;
        return true;
    }

    public boolean m2925f() {
        return m2924e() | m2926g();
    }

    public boolean m2926g() {
        if (this.f1590y == null) {
            return false;
        }
        this.f1590y.m2441e();
        return true;
    }

    public boolean m2927h() {
        return this.f1589x != null && this.f1589x.m2442f();
    }

    public boolean m2928i() {
        return this.f1591z != null || m2927h();
    }

    public boolean m2919b() {
        int i;
        ArrayList i2 = this.c.m2361i();
        int size = i2.size();
        int i3 = this.f1581p;
        int i4 = this.f1580o;
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f;
        int i5 = 0;
        int i6 = 0;
        Object obj = null;
        int i7 = 0;
        while (i7 < size) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) i2.get(i7);
            if (menuItemImpl.m2401l()) {
                i5++;
            } else if (menuItemImpl.m2400k()) {
                i6++;
            } else {
                obj = 1;
            }
            if (this.f1585t && menuItemImpl.isActionViewExpanded()) {
                i = 0;
            } else {
                i = i3;
            }
            i7++;
            i3 = i;
        }
        if (this.f1577l && (r4 != null || i5 + i6 > i3)) {
            i3--;
        }
        i7 = i3 - i5;
        SparseBooleanArray sparseBooleanArray = this.f1587v;
        sparseBooleanArray.clear();
        i = 0;
        if (this.f1583r) {
            i = i4 / this.f1586u;
            i6 = ((i4 % this.f1586u) / i) + this.f1586u;
        } else {
            i6 = 0;
        }
        int i8 = 0;
        i3 = 0;
        int i9 = i;
        while (i8 < size) {
            menuItemImpl = (MenuItemImpl) i2.get(i8);
            int i10;
            if (menuItemImpl.m2401l()) {
                View a = m2907a(menuItemImpl, this.f1588w, viewGroup);
                if (this.f1588w == null) {
                    this.f1588w = a;
                }
                if (this.f1583r) {
                    i9 -= ActionMenuView.m2536a(a, i6, i9, makeMeasureSpec, 0);
                } else {
                    a.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i5 = a.getMeasuredWidth();
                i10 = i4 - i5;
                if (i3 != 0) {
                    i5 = i3;
                }
                i3 = menuItemImpl.getGroupId();
                if (i3 != 0) {
                    sparseBooleanArray.put(i3, true);
                }
                menuItemImpl.m2392d(true);
                i = i10;
                i3 = i7;
            } else if (menuItemImpl.m2400k()) {
                boolean z;
                int groupId = menuItemImpl.getGroupId();
                boolean z2 = sparseBooleanArray.get(groupId);
                boolean z3 = (i7 > 0 || z2) && i4 > 0 && (!this.f1583r || i9 > 0);
                if (z3) {
                    View a2 = m2907a(menuItemImpl, this.f1588w, viewGroup);
                    if (this.f1588w == null) {
                        this.f1588w = a2;
                    }
                    boolean z4;
                    if (this.f1583r) {
                        int a3 = ActionMenuView.m2536a(a2, i6, i9, makeMeasureSpec, 0);
                        i10 = i9 - a3;
                        if (a3 == 0) {
                            i9 = 0;
                        } else {
                            z4 = z3;
                        }
                        i5 = i10;
                    } else {
                        a2.measure(makeMeasureSpec, makeMeasureSpec);
                        boolean z5 = z3;
                        i5 = i9;
                        z4 = z5;
                    }
                    i10 = a2.getMeasuredWidth();
                    i4 -= i10;
                    if (i3 == 0) {
                        i3 = i10;
                    }
                    if (this.f1583r) {
                        z = i9 & (i4 >= 0 ? 1 : 0);
                        i10 = i3;
                        i3 = i5;
                    } else {
                        z = i9 & (i4 + i3 > 0 ? 1 : 0);
                        i10 = i3;
                        i3 = i5;
                    }
                } else {
                    z = z3;
                    i10 = i3;
                    i3 = i9;
                }
                if (z && groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                    i9 = i7;
                } else if (z2) {
                    sparseBooleanArray.put(groupId, false);
                    i5 = i7;
                    for (i7 = 0; i7 < i8; i7++) {
                        MenuItemImpl menuItemImpl2 = (MenuItemImpl) i2.get(i7);
                        if (menuItemImpl2.getGroupId() == groupId) {
                            if (menuItemImpl2.m2399j()) {
                                i5++;
                            }
                            menuItemImpl2.m2392d(false);
                        }
                    }
                    i9 = i5;
                } else {
                    i9 = i7;
                }
                if (z) {
                    i9--;
                }
                menuItemImpl.m2392d(z);
                i5 = i10;
                i = i4;
                int i11 = i3;
                i3 = i9;
                i9 = i11;
            } else {
                menuItemImpl.m2392d(false);
                i5 = i3;
                i = i4;
                i3 = i7;
            }
            i8++;
            i4 = i;
            i7 = i3;
            i3 = i5;
        }
        return true;
    }

    public void m2911a(MenuBuilder menuBuilder, boolean z) {
        m2925f();
        super.m2288a(menuBuilder, z);
    }

    public void m2914a(boolean z) {
        if (z) {
            super.m2294a(null);
        } else {
            this.c.m2341a(false);
        }
    }

    public void m2913a(ActionMenuView actionMenuView) {
        this.f = actionMenuView;
        actionMenuView.m2542a(this.c);
    }
}
