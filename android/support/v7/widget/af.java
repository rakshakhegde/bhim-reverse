package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.GravityCompat;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

/* compiled from: LinearLayoutCompat */
public class af extends ViewGroup {
    private boolean f1242a;
    private int f1243b;
    private int f1244c;
    private int f1245d;
    private int f1246e;
    private int f1247f;
    private float f1248g;
    private boolean f1249h;
    private int[] f1250i;
    private int[] f1251j;
    private Drawable f1252k;
    private int f1253l;
    private int f1254m;
    private int f1255n;
    private int f1256o;

    /* renamed from: android.support.v7.widget.af.a */
    public static class LinearLayoutCompat extends MarginLayoutParams {
        public float f1233g;
        public int f1234h;

        public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f1234h = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.LinearLayoutCompat_Layout);
            this.f1233g = obtainStyledAttributes.getFloat(R.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.f1234h = obtainStyledAttributes.getInt(R.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }

        public LinearLayoutCompat(int i, int i2) {
            super(i, i2);
            this.f1234h = -1;
            this.f1233g = 0.0f;
        }

        public LinearLayoutCompat(LayoutParams layoutParams) {
            super(layoutParams);
            this.f1234h = -1;
        }
    }

    protected /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return m2535j();
    }

    public /* synthetic */ LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return m2526b(attributeSet);
    }

    protected /* synthetic */ LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return m2527b(layoutParams);
    }

    public af(Context context) {
        this(context, null);
    }

    public af(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public af(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1242a = true;
        this.f1243b = -1;
        this.f1244c = 0;
        this.f1246e = 8388659;
        ar a = ar.m2811a(context, attributeSet, R.LinearLayoutCompat, i, 0);
        int a2 = a.m2813a(R.LinearLayoutCompat_android_orientation, -1);
        if (a2 >= 0) {
            setOrientation(a2);
        }
        a2 = a.m2813a(R.LinearLayoutCompat_android_gravity, -1);
        if (a2 >= 0) {
            setGravity(a2);
        }
        boolean a3 = a.m2816a(R.LinearLayoutCompat_android_baselineAligned, true);
        if (!a3) {
            setBaselineAligned(a3);
        }
        this.f1248g = a.m2812a(R.LinearLayoutCompat_android_weightSum, -1.0f);
        this.f1243b = a.m2813a(R.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.f1249h = a.m2816a(R.LinearLayoutCompat_measureWithLargestChild, false);
        setDividerDrawable(a.m2814a(R.LinearLayoutCompat_divider));
        this.f1255n = a.m2813a(R.LinearLayoutCompat_showDividers, 0);
        this.f1256o = a.m2823e(R.LinearLayoutCompat_dividerPadding, 0);
        a.m2815a();
    }

    public void setShowDividers(int i) {
        if (i != this.f1255n) {
            requestLayout();
        }
        this.f1255n = i;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public int getShowDividers() {
        return this.f1255n;
    }

    public Drawable getDividerDrawable() {
        return this.f1252k;
    }

    public void setDividerDrawable(Drawable drawable) {
        boolean z = false;
        if (drawable != this.f1252k) {
            this.f1252k = drawable;
            if (drawable != null) {
                this.f1253l = drawable.getIntrinsicWidth();
                this.f1254m = drawable.getIntrinsicHeight();
            } else {
                this.f1253l = 0;
                this.f1254m = 0;
            }
            if (drawable == null) {
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i) {
        this.f1256o = i;
    }

    public int getDividerPadding() {
        return this.f1256o;
    }

    public int getDividerWidth() {
        return this.f1253l;
    }

    protected void onDraw(Canvas canvas) {
        if (this.f1252k != null) {
            if (this.f1245d == 1) {
                m2522a(canvas);
            } else {
                m2531b(canvas);
            }
        }
    }

    void m2522a(Canvas canvas) {
        int virtualChildCount = getVirtualChildCount();
        int i = 0;
        while (i < virtualChildCount) {
            View b = m2528b(i);
            if (!(b == null || b.getVisibility() == 8 || !m2533c(i))) {
                m2523a(canvas, (b.getTop() - ((LinearLayoutCompat) b.getLayoutParams()).topMargin) - this.f1254m);
            }
            i++;
        }
        if (m2533c(virtualChildCount)) {
            int height;
            View b2 = m2528b(virtualChildCount - 1);
            if (b2 == null) {
                height = (getHeight() - getPaddingBottom()) - this.f1254m;
            } else {
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b2.getLayoutParams();
                height = linearLayoutCompat.bottomMargin + b2.getBottom();
            }
            m2523a(canvas, height);
        }
    }

    void m2531b(Canvas canvas) {
        int virtualChildCount = getVirtualChildCount();
        boolean a = au.m2883a(this);
        int i = 0;
        while (i < virtualChildCount) {
            LinearLayoutCompat linearLayoutCompat;
            int right;
            View b = m2528b(i);
            if (!(b == null || b.getVisibility() == 8 || !m2533c(i))) {
                linearLayoutCompat = (LinearLayoutCompat) b.getLayoutParams();
                if (a) {
                    right = linearLayoutCompat.rightMargin + b.getRight();
                } else {
                    right = (b.getLeft() - linearLayoutCompat.leftMargin) - this.f1253l;
                }
                m2532b(canvas, right);
            }
            i++;
        }
        if (m2533c(virtualChildCount)) {
            View b2 = m2528b(virtualChildCount - 1);
            if (b2 != null) {
                linearLayoutCompat = (LinearLayoutCompat) b2.getLayoutParams();
                if (a) {
                    right = (b2.getLeft() - linearLayoutCompat.leftMargin) - this.f1253l;
                } else {
                    right = linearLayoutCompat.rightMargin + b2.getRight();
                }
            } else if (a) {
                right = getPaddingLeft();
            } else {
                right = (getWidth() - getPaddingRight()) - this.f1253l;
            }
            m2532b(canvas, right);
        }
    }

    void m2523a(Canvas canvas, int i) {
        this.f1252k.setBounds(getPaddingLeft() + this.f1256o, i, (getWidth() - getPaddingRight()) - this.f1256o, this.f1254m + i);
        this.f1252k.draw(canvas);
    }

    void m2532b(Canvas canvas, int i) {
        this.f1252k.setBounds(i, getPaddingTop() + this.f1256o, this.f1253l + i, (getHeight() - getPaddingBottom()) - this.f1256o);
        this.f1252k.draw(canvas);
    }

    public void setBaselineAligned(boolean z) {
        this.f1242a = z;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.f1249h = z;
    }

    public int getBaseline() {
        if (this.f1243b < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.f1243b) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.f1243b);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i;
            int i2 = this.f1244c;
            if (this.f1245d == 1) {
                i = this.f1246e & 112;
                if (i != 48) {
                    switch (i) {
                        case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_titleMarginBottom /*16*/:
                            i = i2 + (((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.f1247f) / 2);
                            break;
                        case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                            i = ((getBottom() - getTop()) - getPaddingBottom()) - this.f1247f;
                            break;
                    }
                }
            }
            i = i2;
            return (((LinearLayoutCompat) childAt.getLayoutParams()).topMargin + i) + baseline;
        } else if (this.f1243b == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.f1243b;
    }

    public void setBaselineAlignedChildIndex(int i) {
        if (i < 0 || i >= getChildCount()) {
            throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
        }
        this.f1243b = i;
    }

    View m2528b(int i) {
        return getChildAt(i);
    }

    int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.f1248g;
    }

    public void setWeightSum(float f) {
        this.f1248g = Math.max(0.0f, f);
    }

    protected void onMeasure(int i, int i2) {
        if (this.f1245d == 1) {
            m2520a(i, i2);
        } else {
            m2529b(i, i2);
        }
    }

    protected boolean m2533c(int i) {
        if (i == 0) {
            if ((this.f1255n & 1) != 0) {
                return true;
            }
            return false;
        } else if (i == getChildCount()) {
            if ((this.f1255n & 4) == 0) {
                return false;
            }
            return true;
        } else if ((this.f1255n & 2) == 0) {
            return false;
        } else {
            for (int i2 = i - 1; i2 >= 0; i2--) {
                if (getChildAt(i2).getVisibility() != 8) {
                    return true;
                }
            }
            return false;
        }
    }

    void m2520a(int i, int i2) {
        int i3;
        Object obj;
        int i4;
        View b;
        this.f1247f = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        Object obj2 = 1;
        float f = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        Object obj3 = null;
        Object obj4 = null;
        int i9 = this.f1243b;
        boolean z = this.f1249h;
        int i10 = Integer.MIN_VALUE;
        int i11 = 0;
        while (i11 < virtualChildCount) {
            Object obj5;
            int i12;
            int i13;
            View b2 = m2528b(i11);
            if (b2 == null) {
                this.f1247f += m2534d(i11);
                i3 = i10;
                obj5 = obj4;
                obj = obj2;
                i12 = i6;
                i13 = i5;
            } else if (b2.getVisibility() == 8) {
                i11 += m2519a(b2, i11);
                i3 = i10;
                obj5 = obj4;
                obj = obj2;
                i12 = i6;
                i13 = i5;
            } else {
                if (m2533c(i11)) {
                    this.f1247f += this.f1254m;
                }
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b2.getLayoutParams();
                float f2 = f + linearLayoutCompat.f1233g;
                if (mode2 == 1073741824 && linearLayoutCompat.height == 0 && linearLayoutCompat.f1233g > 0.0f) {
                    i3 = this.f1247f;
                    this.f1247f = Math.max(i3, (linearLayoutCompat.topMargin + i3) + linearLayoutCompat.bottomMargin);
                    obj4 = 1;
                } else {
                    i3 = Integer.MIN_VALUE;
                    if (linearLayoutCompat.height == 0 && linearLayoutCompat.f1233g > 0.0f) {
                        i3 = 0;
                        linearLayoutCompat.height = -2;
                    }
                    int i14 = i3;
                    m2524a(b2, i11, i, 0, i2, f2 == 0.0f ? this.f1247f : 0);
                    if (i14 != Integer.MIN_VALUE) {
                        linearLayoutCompat.height = i14;
                    }
                    i3 = b2.getMeasuredHeight();
                    int i15 = this.f1247f;
                    this.f1247f = Math.max(i15, (((i15 + i3) + linearLayoutCompat.topMargin) + linearLayoutCompat.bottomMargin) + m2525b(b2));
                    if (z) {
                        i10 = Math.max(i3, i10);
                    }
                }
                if (i9 >= 0 && i9 == i11 + 1) {
                    this.f1244c = this.f1247f;
                }
                if (i11 >= i9 || linearLayoutCompat.f1233g <= 0.0f) {
                    Object obj6;
                    Object obj7 = null;
                    if (mode == 1073741824 || linearLayoutCompat.width != -1) {
                        obj6 = obj3;
                    } else {
                        obj6 = 1;
                        obj7 = 1;
                    }
                    i12 = linearLayoutCompat.rightMargin + linearLayoutCompat.leftMargin;
                    i13 = b2.getMeasuredWidth() + i12;
                    i5 = Math.max(i5, i13);
                    int a = au.m2881a(i6, android.support.v4.p006f.af.m1194f(b2));
                    obj = (obj2 == null || linearLayoutCompat.width != -1) ? null : 1;
                    if (linearLayoutCompat.f1233g > 0.0f) {
                        if (obj7 != null) {
                            i3 = i12;
                        } else {
                            i3 = i13;
                        }
                        i3 = Math.max(i8, i3);
                        i12 = i7;
                    } else {
                        if (obj7 == null) {
                            i12 = i13;
                        }
                        i12 = Math.max(i7, i12);
                        i3 = i8;
                    }
                    i11 += m2519a(b2, i11);
                    obj5 = obj4;
                    i8 = i3;
                    i7 = i12;
                    i13 = i5;
                    i3 = i10;
                    i12 = a;
                    obj3 = obj6;
                    f = f2;
                } else {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
            }
            i11++;
            i10 = i3;
            obj4 = obj5;
            obj2 = obj;
            i6 = i12;
            i5 = i13;
        }
        if (this.f1247f > 0 && m2533c(virtualChildCount)) {
            this.f1247f += this.f1254m;
        }
        if (z && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
            this.f1247f = 0;
            i4 = 0;
            while (i4 < virtualChildCount) {
                b = m2528b(i4);
                if (b == null) {
                    this.f1247f += m2534d(i4);
                    i3 = i4;
                } else if (b.getVisibility() == 8) {
                    i3 = m2519a(b, i4) + i4;
                } else {
                    LinearLayoutCompat linearLayoutCompat2 = (LinearLayoutCompat) b.getLayoutParams();
                    int i16 = this.f1247f;
                    this.f1247f = Math.max(i16, (linearLayoutCompat2.bottomMargin + ((i16 + i10) + linearLayoutCompat2.topMargin)) + m2525b(b));
                    i3 = i4;
                }
                i4 = i3 + 1;
            }
        }
        this.f1247f += getPaddingTop() + getPaddingBottom();
        int a2 = android.support.v4.p006f.af.m1172a(Math.max(this.f1247f, getSuggestedMinimumHeight()), i2, 0);
        i4 = (16777215 & a2) - this.f1247f;
        int i17;
        if (obj4 != null || (i4 != 0 && f > 0.0f)) {
            if (this.f1248g > 0.0f) {
                f = this.f1248g;
            }
            this.f1247f = 0;
            i10 = 0;
            float f3 = f;
            Object obj8 = obj2;
            i17 = i7;
            i16 = i6;
            i8 = i5;
            i15 = i4;
            while (i10 < virtualChildCount) {
                View b3 = m2528b(i10);
                if (b3.getVisibility() == 8) {
                    i3 = i17;
                    i4 = i16;
                    i12 = i8;
                    obj = obj8;
                } else {
                    float f4;
                    float f5;
                    linearLayoutCompat2 = (LinearLayoutCompat) b3.getLayoutParams();
                    float f6 = linearLayoutCompat2.f1233g;
                    if (f6 > 0.0f) {
                        i4 = (int) ((((float) i15) * f6) / f3);
                        f3 -= f6;
                        i15 -= i4;
                        i12 = getChildMeasureSpec(i, ((getPaddingLeft() + getPaddingRight()) + linearLayoutCompat2.leftMargin) + linearLayoutCompat2.rightMargin, linearLayoutCompat2.width);
                        if (linearLayoutCompat2.height == 0 && mode2 == 1073741824) {
                            if (i4 <= 0) {
                                i4 = 0;
                            }
                            b3.measure(i12, MeasureSpec.makeMeasureSpec(i4, 1073741824));
                        } else {
                            i4 += b3.getMeasuredHeight();
                            if (i4 < 0) {
                                i4 = 0;
                            }
                            b3.measure(i12, MeasureSpec.makeMeasureSpec(i4, 1073741824));
                        }
                        f4 = f3;
                        i11 = i15;
                        i15 = au.m2881a(i16, android.support.v4.p006f.af.m1194f(b3) & -256);
                        f5 = f4;
                    } else {
                        f5 = f3;
                        i11 = i15;
                        i15 = i16;
                    }
                    i16 = linearLayoutCompat2.leftMargin + linearLayoutCompat2.rightMargin;
                    i12 = b3.getMeasuredWidth() + i16;
                    i8 = Math.max(i8, i12);
                    Object obj9 = (mode == 1073741824 || linearLayoutCompat2.width != -1) ? null : 1;
                    if (obj9 == null) {
                        i16 = i12;
                    }
                    i12 = Math.max(i17, i16);
                    obj = (obj8 == null || linearLayoutCompat2.width != -1) ? null : 1;
                    i13 = this.f1247f;
                    this.f1247f = Math.max(i13, (linearLayoutCompat2.bottomMargin + ((b3.getMeasuredHeight() + i13) + linearLayoutCompat2.topMargin)) + m2525b(b3));
                    i3 = i12;
                    i12 = i8;
                    f4 = f5;
                    i4 = i15;
                    i15 = i11;
                    f3 = f4;
                }
                i10++;
                i17 = i3;
                i8 = i12;
                obj8 = obj;
                i16 = i4;
            }
            this.f1247f += getPaddingTop() + getPaddingBottom();
            obj2 = obj8;
            i3 = i17;
            i6 = i16;
            i4 = i8;
        } else {
            i17 = Math.max(i7, i8);
            if (z && mode2 != 1073741824) {
                for (i4 = 0; i4 < virtualChildCount; i4++) {
                    b = m2528b(i4);
                    if (!(b == null || b.getVisibility() == 8 || ((LinearLayoutCompat) b.getLayoutParams()).f1233g <= 0.0f)) {
                        b.measure(MeasureSpec.makeMeasureSpec(b.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(i10, 1073741824));
                    }
                }
            }
            i3 = i17;
            i4 = i5;
        }
        if (obj2 != null || mode == 1073741824) {
            i3 = i4;
        }
        setMeasuredDimension(android.support.v4.p006f.af.m1172a(Math.max(i3 + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), i, i6), a2);
        if (obj3 != null) {
            m2516c(virtualChildCount, i2);
        }
    }

    private void m2516c(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int i3 = 0; i3 < i; i3++) {
            View b = m2528b(i3);
            if (b.getVisibility() != 8) {
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b.getLayoutParams();
                if (linearLayoutCompat.width == -1) {
                    int i4 = linearLayoutCompat.height;
                    linearLayoutCompat.height = b.getMeasuredHeight();
                    measureChildWithMargins(b, makeMeasureSpec, 0, i2, 0);
                    linearLayoutCompat.height = i4;
                }
            }
        }
    }

    void m2529b(int i, int i2) {
        Object obj;
        int i3;
        int i4;
        LinearLayoutCompat linearLayoutCompat;
        this.f1247f = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        Object obj2 = 1;
        float f = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        Object obj3 = null;
        Object obj4 = null;
        if (this.f1250i == null || this.f1251j == null) {
            this.f1250i = new int[4];
            this.f1251j = new int[4];
        }
        int[] iArr = this.f1250i;
        int[] iArr2 = this.f1251j;
        iArr[3] = -1;
        iArr[2] = -1;
        iArr[1] = -1;
        iArr[0] = -1;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        boolean z = this.f1242a;
        boolean z2 = this.f1249h;
        if (mode == 1073741824) {
            obj = 1;
        } else {
            obj = null;
        }
        int i9 = Integer.MIN_VALUE;
        int i10 = 0;
        while (i10 < virtualChildCount) {
            Object obj5;
            Object obj6;
            int i11;
            int i12;
            View b = m2528b(i10);
            if (b == null) {
                this.f1247f += m2534d(i10);
                i3 = i9;
                obj5 = obj4;
                obj6 = obj2;
                i11 = i6;
                i12 = i5;
            } else if (b.getVisibility() == 8) {
                i10 += m2519a(b, i10);
                i3 = i9;
                obj5 = obj4;
                obj6 = obj2;
                i11 = i6;
                i12 = i5;
            } else {
                Object obj7;
                if (m2533c(i10)) {
                    this.f1247f += this.f1253l;
                }
                LinearLayoutCompat linearLayoutCompat2 = (LinearLayoutCompat) b.getLayoutParams();
                float f2 = f + linearLayoutCompat2.f1233g;
                if (mode == 1073741824 && linearLayoutCompat2.width == 0 && linearLayoutCompat2.f1233g > 0.0f) {
                    if (obj != null) {
                        this.f1247f += linearLayoutCompat2.leftMargin + linearLayoutCompat2.rightMargin;
                    } else {
                        i3 = this.f1247f;
                        this.f1247f = Math.max(i3, (linearLayoutCompat2.leftMargin + i3) + linearLayoutCompat2.rightMargin);
                    }
                    if (z) {
                        i3 = MeasureSpec.makeMeasureSpec(0, 0);
                        b.measure(i3, i3);
                    } else {
                        obj4 = 1;
                    }
                } else {
                    i3 = Integer.MIN_VALUE;
                    if (linearLayoutCompat2.width == 0 && linearLayoutCompat2.f1233g > 0.0f) {
                        i3 = 0;
                        linearLayoutCompat2.width = -2;
                    }
                    int i13 = i3;
                    m2524a(b, i10, i, f2 == 0.0f ? this.f1247f : 0, i2, 0);
                    if (i13 != Integer.MIN_VALUE) {
                        linearLayoutCompat2.width = i13;
                    }
                    i3 = b.getMeasuredWidth();
                    if (obj != null) {
                        this.f1247f += ((linearLayoutCompat2.leftMargin + i3) + linearLayoutCompat2.rightMargin) + m2525b(b);
                    } else {
                        int i14 = this.f1247f;
                        this.f1247f = Math.max(i14, (((i14 + i3) + linearLayoutCompat2.leftMargin) + linearLayoutCompat2.rightMargin) + m2525b(b));
                    }
                    if (z2) {
                        i9 = Math.max(i3, i9);
                    }
                }
                Object obj8 = null;
                if (mode2 == 1073741824 || linearLayoutCompat2.height != -1) {
                    obj7 = obj3;
                } else {
                    obj7 = 1;
                    obj8 = 1;
                }
                i11 = linearLayoutCompat2.bottomMargin + linearLayoutCompat2.topMargin;
                i12 = b.getMeasuredHeight() + i11;
                int a = au.m2881a(i6, android.support.v4.p006f.af.m1194f(b));
                if (z) {
                    i6 = b.getBaseline();
                    if (i6 != -1) {
                        int i15 = ((((linearLayoutCompat2.f1234h < 0 ? this.f1246e : linearLayoutCompat2.f1234h) & 112) >> 4) & -2) >> 1;
                        iArr[i15] = Math.max(iArr[i15], i6);
                        iArr2[i15] = Math.max(iArr2[i15], i12 - i6);
                    }
                }
                i6 = Math.max(i5, i12);
                obj6 = (obj2 == null || linearLayoutCompat2.height != -1) ? null : 1;
                if (linearLayoutCompat2.f1233g > 0.0f) {
                    if (obj8 != null) {
                        i3 = i11;
                    } else {
                        i3 = i12;
                    }
                    i3 = Math.max(i8, i3);
                    i11 = i7;
                } else {
                    if (obj8 == null) {
                        i11 = i12;
                    }
                    i11 = Math.max(i7, i11);
                    i3 = i8;
                }
                i10 += m2519a(b, i10);
                obj5 = obj4;
                i8 = i3;
                i7 = i11;
                i12 = i6;
                i3 = i9;
                i11 = a;
                obj3 = obj7;
                f = f2;
            }
            i10++;
            i9 = i3;
            obj4 = obj5;
            obj2 = obj6;
            i6 = i11;
            i5 = i12;
        }
        if (this.f1247f > 0 && m2533c(virtualChildCount)) {
            this.f1247f += this.f1253l;
        }
        if (iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1) {
            i10 = i5;
        } else {
            i10 = Math.max(i5, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
        }
        if (z2 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.f1247f = 0;
            i4 = 0;
            while (i4 < virtualChildCount) {
                View b2 = m2528b(i4);
                if (b2 == null) {
                    this.f1247f += m2534d(i4);
                    i3 = i4;
                } else if (b2.getVisibility() == 8) {
                    i3 = m2519a(b2, i4) + i4;
                } else {
                    linearLayoutCompat = (LinearLayoutCompat) b2.getLayoutParams();
                    if (obj != null) {
                        this.f1247f = ((linearLayoutCompat.rightMargin + (linearLayoutCompat.leftMargin + i9)) + m2525b(b2)) + this.f1247f;
                        i3 = i4;
                    } else {
                        i11 = this.f1247f;
                        this.f1247f = Math.max(i11, (linearLayoutCompat.rightMargin + ((i11 + i9) + linearLayoutCompat.leftMargin)) + m2525b(b2));
                        i3 = i4;
                    }
                }
                i4 = i3 + 1;
            }
        }
        this.f1247f += getPaddingLeft() + getPaddingRight();
        int a2 = android.support.v4.p006f.af.m1172a(Math.max(this.f1247f, getSuggestedMinimumWidth()), i, 0);
        i4 = (16777215 & a2) - this.f1247f;
        int i16;
        if (obj4 != null || (i4 != 0 && f > 0.0f)) {
            if (this.f1248g > 0.0f) {
                f = this.f1248g;
            }
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            this.f1247f = 0;
            i9 = 0;
            float f3 = f;
            Object obj9 = obj2;
            i16 = i7;
            i15 = i6;
            i14 = i4;
            i7 = -1;
            while (i9 < virtualChildCount) {
                float f4;
                Object obj10;
                View b3 = m2528b(i9);
                if (b3 == null) {
                    f4 = f3;
                    i4 = i14;
                    i11 = i7;
                    i14 = i16;
                    obj10 = obj9;
                } else if (b3.getVisibility() == 8) {
                    f4 = f3;
                    i4 = i14;
                    i11 = i7;
                    i14 = i16;
                    obj10 = obj9;
                } else {
                    float f5;
                    linearLayoutCompat = (LinearLayoutCompat) b3.getLayoutParams();
                    float f6 = linearLayoutCompat.f1233g;
                    if (f6 > 0.0f) {
                        i4 = (int) ((((float) i14) * f6) / f3);
                        f3 -= f6;
                        i11 = i14 - i4;
                        i14 = getChildMeasureSpec(i2, ((getPaddingTop() + getPaddingBottom()) + linearLayoutCompat.topMargin) + linearLayoutCompat.bottomMargin, linearLayoutCompat.height);
                        if (linearLayoutCompat.width == 0 && mode == 1073741824) {
                            if (i4 <= 0) {
                                i4 = 0;
                            }
                            b3.measure(MeasureSpec.makeMeasureSpec(i4, 1073741824), i14);
                        } else {
                            i4 += b3.getMeasuredWidth();
                            if (i4 < 0) {
                                i4 = 0;
                            }
                            b3.measure(MeasureSpec.makeMeasureSpec(i4, 1073741824), i14);
                        }
                        i8 = au.m2881a(i15, android.support.v4.p006f.af.m1194f(b3) & -16777216);
                        f5 = f3;
                    } else {
                        i11 = i14;
                        i8 = i15;
                        f5 = f3;
                    }
                    if (obj != null) {
                        this.f1247f += ((b3.getMeasuredWidth() + linearLayoutCompat.leftMargin) + linearLayoutCompat.rightMargin) + m2525b(b3);
                    } else {
                        i4 = this.f1247f;
                        this.f1247f = Math.max(i4, (((b3.getMeasuredWidth() + i4) + linearLayoutCompat.leftMargin) + linearLayoutCompat.rightMargin) + m2525b(b3));
                    }
                    obj5 = (mode2 == 1073741824 || linearLayoutCompat.height != -1) ? null : 1;
                    i10 = linearLayoutCompat.topMargin + linearLayoutCompat.bottomMargin;
                    i14 = b3.getMeasuredHeight() + i10;
                    i7 = Math.max(i7, i14);
                    if (obj5 != null) {
                        i4 = i10;
                    } else {
                        i4 = i14;
                    }
                    i10 = Math.max(i16, i4);
                    obj5 = (obj9 == null || linearLayoutCompat.height != -1) ? null : 1;
                    if (z) {
                        i12 = b3.getBaseline();
                        if (i12 != -1) {
                            i3 = ((((linearLayoutCompat.f1234h < 0 ? this.f1246e : linearLayoutCompat.f1234h) & 112) >> 4) & -2) >> 1;
                            iArr[i3] = Math.max(iArr[i3], i12);
                            iArr2[i3] = Math.max(iArr2[i3], i14 - i12);
                        }
                    }
                    f4 = f5;
                    i14 = i10;
                    obj10 = obj5;
                    i15 = i8;
                    i4 = i11;
                    i11 = i7;
                }
                i9++;
                i16 = i14;
                i7 = i11;
                obj9 = obj10;
                i14 = i4;
                f3 = f4;
            }
            this.f1247f += getPaddingLeft() + getPaddingRight();
            if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
                i7 = Math.max(i7, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
            }
            obj2 = obj9;
            i3 = i16;
            i6 = i15;
            i4 = i7;
        } else {
            i16 = Math.max(i7, i8);
            if (z2 && mode != 1073741824) {
                for (i4 = 0; i4 < virtualChildCount; i4++) {
                    View b4 = m2528b(i4);
                    if (!(b4 == null || b4.getVisibility() == 8 || ((LinearLayoutCompat) b4.getLayoutParams()).f1233g <= 0.0f)) {
                        b4.measure(MeasureSpec.makeMeasureSpec(i9, 1073741824), MeasureSpec.makeMeasureSpec(b4.getMeasuredHeight(), 1073741824));
                    }
                }
            }
            i3 = i16;
            i4 = i10;
        }
        if (obj2 != null || mode2 == 1073741824) {
            i3 = i4;
        }
        setMeasuredDimension((-16777216 & i6) | a2, android.support.v4.p006f.af.m1172a(Math.max(i3 + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight()), i2, i6 << 16));
        if (obj3 != null) {
            m2517d(virtualChildCount, i);
        }
    }

    private void m2517d(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        for (int i3 = 0; i3 < i; i3++) {
            View b = m2528b(i3);
            if (b.getVisibility() != 8) {
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b.getLayoutParams();
                if (linearLayoutCompat.height == -1) {
                    int i4 = linearLayoutCompat.width;
                    linearLayoutCompat.width = b.getMeasuredWidth();
                    measureChildWithMargins(b, i2, 0, makeMeasureSpec, 0);
                    linearLayoutCompat.width = i4;
                }
            }
        }
    }

    int m2519a(View view, int i) {
        return 0;
    }

    int m2534d(int i) {
        return 0;
    }

    void m2524a(View view, int i, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    int m2518a(View view) {
        return 0;
    }

    int m2525b(View view) {
        return 0;
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.f1245d == 1) {
            m2521a(i, i2, i3, i4);
        } else {
            m2530b(i, i2, i3, i4);
        }
    }

    void m2521a(int i, int i2, int i3, int i4) {
        int paddingLeft = getPaddingLeft();
        int i5 = i3 - i;
        int paddingRight = i5 - getPaddingRight();
        int paddingRight2 = (i5 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i6 = this.f1246e & 8388615;
        switch (this.f1246e & 112) {
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_titleMarginBottom /*16*/:
                i5 = getPaddingTop() + (((i4 - i2) - this.f1247f) / 2);
                break;
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                i5 = ((getPaddingTop() + i4) - i2) - this.f1247f;
                break;
            default:
                i5 = getPaddingTop();
                break;
        }
        int i7 = 0;
        int i8 = i5;
        while (i7 < virtualChildCount) {
            View b = m2528b(i7);
            if (b == null) {
                i8 += m2534d(i7);
                i5 = i7;
            } else if (b.getVisibility() != 8) {
                int i9;
                int measuredWidth = b.getMeasuredWidth();
                int measuredHeight = b.getMeasuredHeight();
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b.getLayoutParams();
                i5 = linearLayoutCompat.f1234h;
                if (i5 < 0) {
                    i5 = i6;
                }
                switch (GravityCompat.m1420a(i5, android.support.v4.p006f.af.m1191d(this)) & 7) {
                    case org.npci.upi.security.pinactivitycomponent.R.R.View_android_focusable /*1*/:
                        i9 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft) + linearLayoutCompat.leftMargin) - linearLayoutCompat.rightMargin;
                        break;
                    case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_contentInsetStart /*5*/:
                        i9 = (paddingRight - measuredWidth) - linearLayoutCompat.rightMargin;
                        break;
                    default:
                        i9 = paddingLeft + linearLayoutCompat.leftMargin;
                        break;
                }
                if (m2533c(i7)) {
                    i5 = this.f1254m + i8;
                } else {
                    i5 = i8;
                }
                int i10 = i5 + linearLayoutCompat.topMargin;
                m2515a(b, i9, i10 + m2518a(b), measuredWidth, measuredHeight);
                i8 = i10 + ((linearLayoutCompat.bottomMargin + measuredHeight) + m2525b(b));
                i5 = m2519a(b, i7) + i7;
            } else {
                i5 = i7;
            }
            i7 = i5 + 1;
        }
    }

    void m2530b(int i, int i2, int i3, int i4) {
        int paddingLeft;
        int i5;
        int i6;
        boolean a = au.m2883a(this);
        int paddingTop = getPaddingTop();
        int i7 = i4 - i2;
        int paddingBottom = i7 - getPaddingBottom();
        int paddingBottom2 = (i7 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        i7 = this.f1246e & 8388615;
        int i8 = this.f1246e & 112;
        boolean z = this.f1242a;
        int[] iArr = this.f1250i;
        int[] iArr2 = this.f1251j;
        switch (GravityCompat.m1420a(i7, android.support.v4.p006f.af.m1191d(this))) {
            case org.npci.upi.security.pinactivitycomponent.R.R.View_android_focusable /*1*/:
                paddingLeft = getPaddingLeft() + (((i3 - i) - this.f1247f) / 2);
                break;
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_contentInsetStart /*5*/:
                paddingLeft = ((getPaddingLeft() + i3) - i) - this.f1247f;
                break;
            default:
                paddingLeft = getPaddingLeft();
                break;
        }
        if (a) {
            i5 = -1;
            i6 = virtualChildCount - 1;
        } else {
            i5 = 1;
            i6 = 0;
        }
        int i9 = 0;
        while (i9 < virtualChildCount) {
            int i10 = i6 + (i5 * i9);
            View b = m2528b(i10);
            if (b == null) {
                paddingLeft += m2534d(i10);
                i7 = i9;
            } else if (b.getVisibility() != 8) {
                int i11;
                int measuredWidth = b.getMeasuredWidth();
                int measuredHeight = b.getMeasuredHeight();
                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) b.getLayoutParams();
                if (!z || linearLayoutCompat.height == -1) {
                    i7 = -1;
                } else {
                    i7 = b.getBaseline();
                }
                int i12 = linearLayoutCompat.f1234h;
                if (i12 < 0) {
                    i12 = i8;
                }
                switch (i12 & 112) {
                    case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_titleMarginBottom /*16*/:
                        i11 = ((((paddingBottom2 - measuredHeight) / 2) + paddingTop) + linearLayoutCompat.topMargin) - linearLayoutCompat.bottomMargin;
                        break;
                    case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_homeAsUpIndicator /*48*/:
                        i11 = paddingTop + linearLayoutCompat.topMargin;
                        if (i7 != -1) {
                            i11 += iArr[1] - i7;
                            break;
                        }
                        break;
                    case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                        i11 = (paddingBottom - measuredHeight) - linearLayoutCompat.bottomMargin;
                        if (i7 != -1) {
                            i11 -= iArr2[2] - (b.getMeasuredHeight() - i7);
                            break;
                        }
                        break;
                    default:
                        i11 = paddingTop;
                        break;
                }
                if (m2533c(i10)) {
                    i7 = this.f1253l + paddingLeft;
                } else {
                    i7 = paddingLeft;
                }
                paddingLeft = i7 + linearLayoutCompat.leftMargin;
                m2515a(b, paddingLeft + m2518a(b), i11, measuredWidth, measuredHeight);
                paddingLeft += (linearLayoutCompat.rightMargin + measuredWidth) + m2525b(b);
                i7 = m2519a(b, i10) + i9;
            } else {
                i7 = i9;
            }
            i9 = i7 + 1;
        }
    }

    private void m2515a(View view, int i, int i2, int i3, int i4) {
        view.layout(i, i2, i + i3, i2 + i4);
    }

    public void setOrientation(int i) {
        if (this.f1245d != i) {
            this.f1245d = i;
            requestLayout();
        }
    }

    public int getOrientation() {
        return this.f1245d;
    }

    public void setGravity(int i) {
        if (this.f1246e != i) {
            int i2;
            if ((8388615 & i) == 0) {
                i2 = 8388611 | i;
            } else {
                i2 = i;
            }
            if ((i2 & 112) == 0) {
                i2 |= 48;
            }
            this.f1246e = i2;
            requestLayout();
        }
    }

    public void setHorizontalGravity(int i) {
        int i2 = i & 8388615;
        if ((this.f1246e & 8388615) != i2) {
            this.f1246e = i2 | (this.f1246e & -8388616);
            requestLayout();
        }
    }

    public void setVerticalGravity(int i) {
        int i2 = i & 112;
        if ((this.f1246e & 112) != i2) {
            this.f1246e = i2 | (this.f1246e & -113);
            requestLayout();
        }
    }

    public LinearLayoutCompat m2526b(AttributeSet attributeSet) {
        return new LinearLayoutCompat(getContext(), attributeSet);
    }

    protected LinearLayoutCompat m2535j() {
        if (this.f1245d == 0) {
            return new LinearLayoutCompat(-2, -2);
        }
        if (this.f1245d == 1) {
            return new LinearLayoutCompat(-1, -2);
        }
        return null;
    }

    protected LinearLayoutCompat m2527b(LayoutParams layoutParams) {
        return new LinearLayoutCompat(layoutParams);
    }

    protected boolean checkLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof LinearLayoutCompat;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(af.class.getName());
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        if (VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(af.class.getName());
        }
    }
}
