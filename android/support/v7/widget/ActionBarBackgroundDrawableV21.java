package android.support.v7.widget;

import android.graphics.Outline;

/* renamed from: android.support.v7.widget.c */
class ActionBarBackgroundDrawableV21 extends ActionBarBackgroundDrawable {
    public ActionBarBackgroundDrawableV21(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.a.f1173d) {
            if (this.a.f1172c != null) {
                this.a.f1172c.getOutline(outline);
            }
        } else if (this.a.f1170a != null) {
            this.a.f1170a.getOutline(outline);
        }
    }
}
