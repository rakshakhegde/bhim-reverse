package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.af.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView extends af implements MenuBuilder, MenuView {
    private android.support.v7.view.menu.MenuBuilder f1257a;
    private Context f1258b;
    private int f1259c;
    private boolean f1260d;
    private ActionMenuPresenter f1261e;
    private MenuPresenter f1262f;
    private MenuBuilder f1263g;
    private boolean f1264h;
    private int f1265i;
    private int f1266j;
    private int f1267k;
    private C0018e f1268l;

    /* renamed from: android.support.v7.widget.ActionMenuView.a */
    public interface C0008a {
        boolean m2249c();

        boolean m2250d();
    }

    /* renamed from: android.support.v7.widget.ActionMenuView.b */
    private class C0015b implements MenuPresenter {
        final /* synthetic */ ActionMenuView f1232a;

        private C0015b(ActionMenuView actionMenuView) {
            this.f1232a = actionMenuView;
        }

        public void m2510a(android.support.v7.view.menu.MenuBuilder menuBuilder, boolean z) {
        }

        public boolean m2511a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            return false;
        }
    }

    /* renamed from: android.support.v7.widget.ActionMenuView.c */
    public static class C0016c extends LinearLayoutCompat {
        @ExportedProperty
        public boolean f1235a;
        @ExportedProperty
        public int f1236b;
        @ExportedProperty
        public int f1237c;
        @ExportedProperty
        public boolean f1238d;
        @ExportedProperty
        public boolean f1239e;
        boolean f1240f;

        public C0016c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public C0016c(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0016c(C0016c c0016c) {
            super(c0016c);
            this.f1235a = c0016c.f1235a;
        }

        public C0016c(int i, int i2) {
            super(i, i2);
            this.f1235a = false;
        }
    }

    /* renamed from: android.support.v7.widget.ActionMenuView.d */
    private class C0017d implements MenuBuilder {
        final /* synthetic */ ActionMenuView f1241a;

        private C0017d(ActionMenuView actionMenuView) {
            this.f1241a = actionMenuView;
        }

        public boolean m2513a(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItem menuItem) {
            return this.f1241a.f1268l != null && this.f1241a.f1268l.m2514a(menuItem);
        }

        public void m2512a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            if (this.f1241a.f1263g != null) {
                this.f1241a.f1263g.m1919a(menuBuilder);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ActionMenuView.e */
    public interface C0018e {
        boolean m2514a(MenuItem menuItem);
    }

    public /* synthetic */ LinearLayoutCompat m2548b(AttributeSet attributeSet) {
        return m2540a(attributeSet);
    }

    protected /* synthetic */ LinearLayoutCompat m2549b(LayoutParams layoutParams) {
        return m2541a(layoutParams);
    }

    protected /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return m2547b();
    }

    public /* synthetic */ LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return m2540a(attributeSet);
    }

    protected /* synthetic */ LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return m2541a(layoutParams);
    }

    protected /* synthetic */ LinearLayoutCompat m2557j() {
        return m2547b();
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f = context.getResources().getDisplayMetrics().density;
        this.f1266j = (int) (56.0f * f);
        this.f1267k = (int) (f * 4.0f);
        this.f1258b = context;
        this.f1259c = 0;
    }

    public void setPopupTheme(int i) {
        if (this.f1259c != i) {
            this.f1259c = i;
            if (i == 0) {
                this.f1258b = getContext();
            } else {
                this.f1258b = new ContextThemeWrapper(getContext(), i);
            }
        }
    }

    public int getPopupTheme() {
        return this.f1259c;
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.f1261e = actionMenuPresenter;
        this.f1261e.m2913a(this);
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        if (this.f1261e != null) {
            this.f1261e.m2918b(false);
            if (this.f1261e.m2927h()) {
                this.f1261e.m2924e();
                this.f1261e.m2923d();
            }
        }
    }

    public void setOnMenuItemClickListener(C0018e c0018e) {
        this.f1268l = c0018e;
    }

    protected void onMeasure(int i, int i2) {
        boolean z = this.f1264h;
        this.f1264h = MeasureSpec.getMode(i) == 1073741824;
        if (z != this.f1264h) {
            this.f1265i = 0;
        }
        int size = MeasureSpec.getSize(i);
        if (!(!this.f1264h || this.f1257a == null || size == this.f1265i)) {
            this.f1265i = size;
            this.f1257a.m2349b(true);
        }
        int childCount = getChildCount();
        if (!this.f1264h || childCount <= 0) {
            for (int i3 = 0; i3 < childCount; i3++) {
                C0016c c0016c = (C0016c) getChildAt(i3).getLayoutParams();
                c0016c.rightMargin = 0;
                c0016c.leftMargin = 0;
            }
            super.onMeasure(i, i2);
            return;
        }
        m2539c(i, i2);
    }

    private void m2539c(int i, int i2) {
        int mode = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i2, paddingTop, -2);
        int i3 = size - paddingLeft;
        int i4 = i3 / this.f1266j;
        size = i3 % this.f1266j;
        if (i4 == 0) {
            setMeasuredDimension(i3, 0);
            return;
        }
        int i5;
        C0016c c0016c;
        Object obj;
        Object obj2;
        int i6 = this.f1266j + (size / i4);
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        paddingLeft = 0;
        Object obj3 = null;
        long j = 0;
        int childCount = getChildCount();
        int i10 = 0;
        while (i10 < childCount) {
            int i11;
            long j2;
            int i12;
            int i13;
            View childAt = getChildAt(i10);
            if (childAt.getVisibility() == 8) {
                i11 = paddingLeft;
                j2 = j;
                i12 = i7;
                i13 = i4;
                i4 = i8;
            } else {
                boolean z = childAt instanceof ActionMenuItemView;
                i5 = paddingLeft + 1;
                if (z) {
                    childAt.setPadding(this.f1267k, 0, this.f1267k, 0);
                }
                c0016c = (C0016c) childAt.getLayoutParams();
                c0016c.f1240f = false;
                c0016c.f1237c = 0;
                c0016c.f1236b = 0;
                c0016c.f1238d = false;
                c0016c.leftMargin = 0;
                c0016c.rightMargin = 0;
                boolean z2 = z && ((ActionMenuItemView) childAt).m2257b();
                c0016c.f1239e = z2;
                if (c0016c.f1235a) {
                    paddingLeft = 1;
                } else {
                    paddingLeft = i4;
                }
                int a = m2536a(childAt, i6, paddingLeft, childMeasureSpec, paddingTop);
                i8 = Math.max(i8, a);
                if (c0016c.f1238d) {
                    paddingLeft = i9 + 1;
                } else {
                    paddingLeft = i9;
                }
                if (c0016c.f1235a) {
                    obj = 1;
                } else {
                    obj = obj3;
                }
                int i14 = i4 - a;
                i9 = Math.max(i7, childAt.getMeasuredHeight());
                if (a == 1) {
                    long j3 = ((long) (1 << i10)) | j;
                    i12 = i9;
                    i13 = i14;
                    i9 = paddingLeft;
                    obj3 = obj;
                    j2 = j3;
                    i4 = i8;
                    i11 = i5;
                } else {
                    i11 = i5;
                    i4 = i8;
                    long j4 = j;
                    i12 = i9;
                    i13 = i14;
                    obj3 = obj;
                    i9 = paddingLeft;
                    j2 = j4;
                }
            }
            i10++;
            i8 = i4;
            i7 = i12;
            i4 = i13;
            j = j2;
            paddingLeft = i11;
        }
        if (obj3 == null || paddingLeft != 2) {
            obj2 = null;
        } else {
            obj2 = 1;
        }
        Object obj4 = null;
        long j5 = j;
        paddingTop = i4;
        while (i9 > 0 && paddingTop > 0) {
            i5 = Integer.MAX_VALUE;
            j = 0;
            i4 = 0;
            int i15 = 0;
            while (i15 < childCount) {
                c0016c = (C0016c) getChildAt(i15).getLayoutParams();
                if (c0016c.f1238d) {
                    int i16 = c0016c.f1236b;
                    if (r0 < i5) {
                        i4 = c0016c.f1236b;
                        j = (long) (1 << i15);
                        size = 1;
                    } else if (c0016c.f1236b == i5) {
                        j |= (long) (1 << i15);
                        size = i4 + 1;
                        i4 = i5;
                    } else {
                        size = i4;
                        i4 = i5;
                    }
                } else {
                    size = i4;
                    i4 = i5;
                }
                i15++;
                i5 = i4;
                i4 = size;
            }
            j5 |= j;
            if (i4 > paddingTop) {
                j = j5;
                break;
            }
            i15 = i5 + 1;
            i5 = 0;
            i4 = paddingTop;
            long j6 = j5;
            while (i5 < childCount) {
                View childAt2 = getChildAt(i5);
                c0016c = (C0016c) childAt2.getLayoutParams();
                if ((((long) (1 << i5)) & j) != 0) {
                    if (obj2 != null && c0016c.f1239e && i4 == 1) {
                        childAt2.setPadding(this.f1267k + i6, 0, this.f1267k, 0);
                    }
                    c0016c.f1236b++;
                    c0016c.f1240f = true;
                    size = i4 - 1;
                } else if (c0016c.f1236b == i15) {
                    j6 |= (long) (1 << i5);
                    size = i4;
                } else {
                    size = i4;
                }
                i5++;
                i4 = size;
            }
            j5 = j6;
            i10 = 1;
            paddingTop = i4;
        }
        j = j5;
        obj = (obj3 == null && paddingLeft == 1) ? 1 : null;
        if (paddingTop <= 0 || j == 0 || (paddingTop >= paddingLeft - 1 && obj == null && i8 <= 1)) {
            obj2 = obj4;
        } else {
            float f;
            View childAt3;
            float bitCount = (float) Long.bitCount(j);
            if (obj == null) {
                if (!((1 & j) == 0 || ((C0016c) getChildAt(0).getLayoutParams()).f1239e)) {
                    bitCount -= 0.5f;
                }
                if (!((((long) (1 << (childCount - 1))) & j) == 0 || ((C0016c) getChildAt(childCount - 1).getLayoutParams()).f1239e)) {
                    f = bitCount - 0.5f;
                    paddingLeft = f <= 0.0f ? (int) (((float) (paddingTop * i6)) / f) : 0;
                    i4 = 0;
                    obj2 = obj4;
                    while (i4 < childCount) {
                        if ((((long) (1 << i4)) & j) != 0) {
                            obj = obj2;
                        } else {
                            childAt3 = getChildAt(i4);
                            c0016c = (C0016c) childAt3.getLayoutParams();
                            if (childAt3 instanceof ActionMenuItemView) {
                                c0016c.f1237c = paddingLeft;
                                c0016c.f1240f = true;
                                if (i4 == 0 && !c0016c.f1239e) {
                                    c0016c.leftMargin = (-paddingLeft) / 2;
                                }
                                obj = 1;
                            } else if (c0016c.f1235a) {
                                if (i4 != 0) {
                                    c0016c.leftMargin = paddingLeft / 2;
                                }
                                if (i4 != childCount - 1) {
                                    c0016c.rightMargin = paddingLeft / 2;
                                }
                                obj = obj2;
                            } else {
                                c0016c.f1237c = paddingLeft;
                                c0016c.f1240f = true;
                                c0016c.rightMargin = (-paddingLeft) / 2;
                                obj = 1;
                            }
                        }
                        i4++;
                        obj2 = obj;
                    }
                }
            }
            f = bitCount;
            if (f <= 0.0f) {
            }
            i4 = 0;
            obj2 = obj4;
            while (i4 < childCount) {
                if ((((long) (1 << i4)) & j) != 0) {
                    childAt3 = getChildAt(i4);
                    c0016c = (C0016c) childAt3.getLayoutParams();
                    if (childAt3 instanceof ActionMenuItemView) {
                        c0016c.f1237c = paddingLeft;
                        c0016c.f1240f = true;
                        c0016c.leftMargin = (-paddingLeft) / 2;
                        obj = 1;
                    } else if (c0016c.f1235a) {
                        if (i4 != 0) {
                            c0016c.leftMargin = paddingLeft / 2;
                        }
                        if (i4 != childCount - 1) {
                            c0016c.rightMargin = paddingLeft / 2;
                        }
                        obj = obj2;
                    } else {
                        c0016c.f1237c = paddingLeft;
                        c0016c.f1240f = true;
                        c0016c.rightMargin = (-paddingLeft) / 2;
                        obj = 1;
                    }
                } else {
                    obj = obj2;
                }
                i4++;
                obj2 = obj;
            }
        }
        if (obj2 != null) {
            for (paddingLeft = 0; paddingLeft < childCount; paddingLeft++) {
                childAt = getChildAt(paddingLeft);
                c0016c = (C0016c) childAt.getLayoutParams();
                if (c0016c.f1240f) {
                    childAt.measure(MeasureSpec.makeMeasureSpec(c0016c.f1237c + (c0016c.f1236b * i6), 1073741824), childMeasureSpec);
                }
            }
        }
        if (mode == 1073741824) {
            i7 = size2;
        }
        setMeasuredDimension(i3, i7);
    }

    static int m2536a(View view, int i, int i2, int i3, int i4) {
        boolean z;
        int i5;
        boolean z2 = false;
        C0016c c0016c = (C0016c) view.getLayoutParams();
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(i3) - i4, MeasureSpec.getMode(i3));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        if (actionMenuItemView == null || !actionMenuItemView.m2257b()) {
            z = false;
        } else {
            z = true;
        }
        if (i2 <= 0 || (z && i2 < 2)) {
            i5 = 0;
        } else {
            view.measure(MeasureSpec.makeMeasureSpec(i * i2, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            i5 = measuredWidth / i;
            if (measuredWidth % i != 0) {
                i5++;
            }
            if (z && r1 < 2) {
                i5 = 2;
            }
        }
        if (!c0016c.f1235a && z) {
            z2 = true;
        }
        c0016c.f1238d = z2;
        c0016c.f1236b = i5;
        view.measure(MeasureSpec.makeMeasureSpec(i5 * i, 1073741824), makeMeasureSpec);
        return i5;
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.f1264h) {
            int i5;
            int i6;
            C0016c c0016c;
            int paddingLeft;
            int childCount = getChildCount();
            int i7 = (i4 - i2) / 2;
            int dividerWidth = getDividerWidth();
            int i8 = 0;
            int i9 = 0;
            int paddingRight = ((i3 - i) - getPaddingRight()) - getPaddingLeft();
            Object obj = null;
            boolean a = au.m2883a(this);
            int i10 = 0;
            while (i10 < childCount) {
                Object obj2;
                View childAt = getChildAt(i10);
                if (childAt.getVisibility() == 8) {
                    obj2 = obj;
                    i5 = i9;
                    i6 = paddingRight;
                    paddingRight = i8;
                } else {
                    c0016c = (C0016c) childAt.getLayoutParams();
                    if (c0016c.f1235a) {
                        i6 = childAt.getMeasuredWidth();
                        if (m2545a(i10)) {
                            i6 += dividerWidth;
                        }
                        int measuredHeight = childAt.getMeasuredHeight();
                        if (a) {
                            paddingLeft = c0016c.leftMargin + getPaddingLeft();
                            i5 = paddingLeft + i6;
                        } else {
                            i5 = (getWidth() - getPaddingRight()) - c0016c.rightMargin;
                            paddingLeft = i5 - i6;
                        }
                        int i11 = i7 - (measuredHeight / 2);
                        childAt.layout(paddingLeft, i11, i5, measuredHeight + i11);
                        i6 = paddingRight - i6;
                        obj2 = 1;
                        i5 = i9;
                        paddingRight = i8;
                    } else {
                        i5 = (childAt.getMeasuredWidth() + c0016c.leftMargin) + c0016c.rightMargin;
                        paddingLeft = i8 + i5;
                        i5 = paddingRight - i5;
                        if (m2545a(i10)) {
                            paddingLeft += dividerWidth;
                        }
                        Object obj3 = obj;
                        i6 = i5;
                        i5 = i9 + 1;
                        paddingRight = paddingLeft;
                        obj2 = obj3;
                    }
                }
                i10++;
                i8 = paddingRight;
                paddingRight = i6;
                i9 = i5;
                obj = obj2;
            }
            if (childCount == 1 && obj == null) {
                View childAt2 = getChildAt(0);
                i6 = childAt2.getMeasuredWidth();
                i5 = childAt2.getMeasuredHeight();
                paddingRight = ((i3 - i) / 2) - (i6 / 2);
                i9 = i7 - (i5 / 2);
                childAt2.layout(paddingRight, i9, i6 + paddingRight, i5 + i9);
                return;
            }
            paddingLeft = i9 - (obj != null ? 0 : 1);
            paddingRight = Math.max(0, paddingLeft > 0 ? paddingRight / paddingLeft : 0);
            View childAt3;
            if (a) {
                i6 = getWidth() - getPaddingRight();
                i5 = 0;
                while (i5 < childCount) {
                    childAt3 = getChildAt(i5);
                    c0016c = (C0016c) childAt3.getLayoutParams();
                    if (childAt3.getVisibility() == 8) {
                        paddingLeft = i6;
                    } else if (c0016c.f1235a) {
                        paddingLeft = i6;
                    } else {
                        i6 -= c0016c.rightMargin;
                        i8 = childAt3.getMeasuredWidth();
                        i10 = childAt3.getMeasuredHeight();
                        dividerWidth = i7 - (i10 / 2);
                        childAt3.layout(i6 - i8, dividerWidth, i6, i10 + dividerWidth);
                        paddingLeft = i6 - ((c0016c.leftMargin + i8) + paddingRight);
                    }
                    i5++;
                    i6 = paddingLeft;
                }
                return;
            }
            i6 = getPaddingLeft();
            i5 = 0;
            while (i5 < childCount) {
                childAt3 = getChildAt(i5);
                c0016c = (C0016c) childAt3.getLayoutParams();
                if (childAt3.getVisibility() == 8) {
                    paddingLeft = i6;
                } else if (c0016c.f1235a) {
                    paddingLeft = i6;
                } else {
                    i6 += c0016c.leftMargin;
                    i8 = childAt3.getMeasuredWidth();
                    i10 = childAt3.getMeasuredHeight();
                    dividerWidth = i7 - (i10 / 2);
                    childAt3.layout(i6, dividerWidth, i6 + i8, i10 + dividerWidth);
                    paddingLeft = ((c0016c.rightMargin + i8) + paddingRight) + i6;
                }
                i5++;
                i6 = paddingLeft;
            }
            return;
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m2556i();
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        this.f1261e.m2910a(drawable);
    }

    public Drawable getOverflowIcon() {
        getMenu();
        return this.f1261e.m2920c();
    }

    public boolean m2544a() {
        return this.f1260d;
    }

    public void setOverflowReserved(boolean z) {
        this.f1260d = z;
    }

    protected C0016c m2547b() {
        C0016c c0016c = new C0016c(-2, -2);
        c0016c.h = 16;
        return c0016c;
    }

    public C0016c m2540a(AttributeSet attributeSet) {
        return new C0016c(getContext(), attributeSet);
    }

    protected C0016c m2541a(LayoutParams layoutParams) {
        if (layoutParams == null) {
            return m2547b();
        }
        C0016c c0016c = layoutParams instanceof C0016c ? new C0016c((C0016c) layoutParams) : new C0016c(layoutParams);
        if (c0016c.h > 0) {
            return c0016c;
        }
        c0016c.h = 16;
        return c0016c;
    }

    protected boolean checkLayoutParams(LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof C0016c);
    }

    public C0016c m2550c() {
        C0016c b = m2547b();
        b.f1235a = true;
        return b;
    }

    public boolean m2546a(MenuItemImpl menuItemImpl) {
        return this.f1257a.m2343a((MenuItem) menuItemImpl, 0);
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void m2542a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
        this.f1257a = menuBuilder;
    }

    public Menu getMenu() {
        if (this.f1257a == null) {
            Context context = getContext();
            this.f1257a = new android.support.v7.view.menu.MenuBuilder(context);
            this.f1257a.m2335a(new C0017d());
            this.f1261e = new ActionMenuPresenter(context);
            this.f1261e.m2921c(true);
            this.f1261e.m2290a(this.f1262f != null ? this.f1262f : new C0015b());
            this.f1257a.m2338a(this.f1261e, this.f1258b);
            this.f1261e.m2913a(this);
        }
        return this.f1257a;
    }

    public void m2543a(MenuPresenter menuPresenter, MenuBuilder menuBuilder) {
        this.f1262f = menuPresenter;
        this.f1263g = menuBuilder;
    }

    public android.support.v7.view.menu.MenuBuilder m2551d() {
        return this.f1257a;
    }

    public boolean m2552e() {
        return this.f1261e != null && this.f1261e.m2923d();
    }

    public boolean m2553f() {
        return this.f1261e != null && this.f1261e.m2924e();
    }

    public boolean m2554g() {
        return this.f1261e != null && this.f1261e.m2927h();
    }

    public boolean m2555h() {
        return this.f1261e != null && this.f1261e.m2928i();
    }

    public void m2556i() {
        if (this.f1261e != null) {
            this.f1261e.m2925f();
        }
    }

    protected boolean m2545a(int i) {
        boolean z = false;
        if (i == 0) {
            return false;
        }
        View childAt = getChildAt(i - 1);
        View childAt2 = getChildAt(i);
        if (i < getChildCount() && (childAt instanceof C0008a)) {
            z = 0 | ((C0008a) childAt).m2250d();
        }
        return (i <= 0 || !(childAt2 instanceof C0008a)) ? z : ((C0008a) childAt2).m2249c() | z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.f1261e.m2922d(z);
    }
}
