package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ac;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: AppCompatTextView */
public class aa extends TextView implements ac {
    private AppCompatDrawableManager f1013a;
    private AppCompatBackgroundHelper f1014b;
    private AppCompatTextHelper f1015c;

    public aa(Context context) {
        this(context, null);
    }

    public aa(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public aa(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1013a = AppCompatDrawableManager.m2981a();
        this.f1014b = new AppCompatBackgroundHelper(this, this.f1013a);
        this.f1014b.m2957a(attributeSet, i);
        this.f1015c = AppCompatTextHelper.m3036a((TextView) this);
        this.f1015c.m3040a(attributeSet, i);
        this.f1015c.m3037a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1014b != null) {
            this.f1014b.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1014b != null) {
            this.f1014b.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1014b != null) {
            this.f1014b.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1014b != null ? this.f1014b.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1014b != null) {
            this.f1014b.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1014b != null ? this.f1014b.m2958b() : null;
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1015c != null) {
            this.f1015c.m3038a(context, i);
        }
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1014b != null) {
            this.f1014b.m2960c();
        }
        if (this.f1015c != null) {
            this.f1015c.m3037a();
        }
    }
}
