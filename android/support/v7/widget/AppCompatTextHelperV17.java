package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.z */
class AppCompatTextHelperV17 extends AppCompatTextHelper {
    private static final int[] f1710b;
    private ap f1711c;
    private ap f1712d;

    static {
        f1710b = new int[]{16843666, 16843667};
    }

    AppCompatTextHelperV17(TextView textView) {
        super(textView);
    }

    void m3043a(AttributeSet attributeSet, int i) {
        super.m3040a(attributeSet, i);
        Context context = this.a.getContext();
        AppCompatDrawableManager a = AppCompatDrawableManager.m2981a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f1710b, i, 0);
        if (obtainStyledAttributes.hasValue(0)) {
            this.f1711c = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(0, 0));
        }
        if (obtainStyledAttributes.hasValue(1)) {
            this.f1712d = AppCompatTextHelper.m3035a(context, a, obtainStyledAttributes.getResourceId(1, 0));
        }
        obtainStyledAttributes.recycle();
    }

    void m3042a() {
        super.m3037a();
        if (this.f1711c != null || this.f1712d != null) {
            Drawable[] compoundDrawablesRelative = this.a.getCompoundDrawablesRelative();
            m3039a(compoundDrawablesRelative[0], this.f1711c);
            m3039a(compoundDrawablesRelative[2], this.f1712d);
        }
    }
}
