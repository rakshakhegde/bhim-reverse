package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ac;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.m */
public class AppCompatEditText extends EditText implements ac {
    private AppCompatDrawableManager f1650a;
    private AppCompatBackgroundHelper f1651b;
    private AppCompatTextHelper f1652c;

    public AppCompatEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.editTextStyle);
    }

    public AppCompatEditText(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1650a = AppCompatDrawableManager.m2981a();
        this.f1651b = new AppCompatBackgroundHelper(this, this.f1650a);
        this.f1651b.m2957a(attributeSet, i);
        this.f1652c = AppCompatTextHelper.m3036a((TextView) this);
        this.f1652c.m3040a(attributeSet, i);
        this.f1652c.m3037a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1651b != null) {
            this.f1651b.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1651b != null) {
            this.f1651b.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1651b != null) {
            this.f1651b.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1651b != null ? this.f1651b.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1651b != null) {
            this.f1651b.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1651b != null ? this.f1651b.m2958b() : null;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1651b != null) {
            this.f1651b.m2960c();
        }
        if (this.f1652c != null) {
            this.f1652c.m3037a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1652c != null) {
            this.f1652c.m3038a(context, i);
        }
    }
}
