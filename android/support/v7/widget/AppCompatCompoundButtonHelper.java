package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.CompoundButton;

/* renamed from: android.support.v7.widget.k */
class AppCompatCompoundButtonHelper {
    private final CompoundButton f1628a;
    private final AppCompatDrawableManager f1629b;
    private ColorStateList f1630c;
    private Mode f1631d;
    private boolean f1632e;
    private boolean f1633f;
    private boolean f1634g;

    AppCompatCompoundButtonHelper(CompoundButton compoundButton, AppCompatDrawableManager appCompatDrawableManager) {
        this.f1630c = null;
        this.f1631d = null;
        this.f1632e = false;
        this.f1633f = false;
        this.f1628a = compoundButton;
        this.f1629b = appCompatDrawableManager;
    }

    void m2965a(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = this.f1628a.getContext().obtainStyledAttributes(attributeSet, R.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(R.CompoundButton_android_button)) {
                int resourceId = obtainStyledAttributes.getResourceId(R.CompoundButton_android_button, 0);
                if (resourceId != 0) {
                    this.f1628a.setButtonDrawable(this.f1629b.m3004a(this.f1628a.getContext(), resourceId));
                }
            }
            if (obtainStyledAttributes.hasValue(R.CompoundButton_buttonTint)) {
                CompoundButtonCompat.m1628a(this.f1628a, obtainStyledAttributes.getColorStateList(R.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(R.CompoundButton_buttonTintMode)) {
                CompoundButtonCompat.m1629a(this.f1628a, ad.m2706a(obtainStyledAttributes.getInt(R.CompoundButton_buttonTintMode, -1), null));
            }
            obtainStyledAttributes.recycle();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
        }
    }

    void m2963a(ColorStateList colorStateList) {
        this.f1630c = colorStateList;
        this.f1632e = true;
        m2968d();
    }

    ColorStateList m2962a() {
        return this.f1630c;
    }

    void m2964a(Mode mode) {
        this.f1631d = mode;
        this.f1633f = true;
        m2968d();
    }

    Mode m2966b() {
        return this.f1631d;
    }

    void m2967c() {
        if (this.f1634g) {
            this.f1634g = false;
            return;
        }
        this.f1634g = true;
        m2968d();
    }

    void m2968d() {
        Drawable a = CompoundButtonCompat.m1627a(this.f1628a);
        if (a == null) {
            return;
        }
        if (this.f1632e || this.f1633f) {
            a = DrawableCompat.m668f(a).mutate();
            if (this.f1632e) {
                DrawableCompat.m659a(a, this.f1630c);
            }
            if (this.f1633f) {
                DrawableCompat.m662a(a, this.f1631d);
            }
            if (a.isStateful()) {
                a.setState(this.f1628a.getDrawableState());
            }
            this.f1628a.setButtonDrawable(a);
        }
    }

    int m2961a(int i) {
        if (VERSION.SDK_INT >= 17) {
            return i;
        }
        Drawable a = CompoundButtonCompat.m1627a(this.f1628a);
        if (a != null) {
            return i + a.getIntrinsicWidth();
        }
        return i;
    }
}
