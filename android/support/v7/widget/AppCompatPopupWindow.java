package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.PopupWindow;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/* renamed from: android.support.v7.widget.r */
public class AppCompatPopupWindow extends PopupWindow {
    private static final boolean f1664a;
    private boolean f1665b;

    /* renamed from: android.support.v7.widget.r.1 */
    static class AppCompatPopupWindow implements OnScrollChangedListener {
        final /* synthetic */ Field f1661a;
        final /* synthetic */ PopupWindow f1662b;
        final /* synthetic */ OnScrollChangedListener f1663c;

        AppCompatPopupWindow(Field field, PopupWindow popupWindow, OnScrollChangedListener onScrollChangedListener) {
            this.f1661a = field;
            this.f1662b = popupWindow;
            this.f1663c = onScrollChangedListener;
        }

        public void onScrollChanged() {
            try {
                WeakReference weakReference = (WeakReference) this.f1661a.get(this.f1662b);
                if (weakReference != null && weakReference.get() != null) {
                    this.f1663c.onScrollChanged();
                }
            } catch (IllegalAccessException e) {
            }
        }
    }

    static {
        f1664a = VERSION.SDK_INT < 21;
    }

    public AppCompatPopupWindow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ar a = ar.m2811a(context, attributeSet, R.PopupWindow, i, 0);
        if (a.m2826f(R.PopupWindow_overlapAnchor)) {
            m3011a(a.m2816a(R.PopupWindow_overlapAnchor, false));
        }
        setBackgroundDrawable(a.m2814a(R.PopupWindow_android_popupBackground));
        a.m2815a();
        if (VERSION.SDK_INT < 14) {
            AppCompatPopupWindow.m3010a((PopupWindow) this);
        }
    }

    public void showAsDropDown(View view, int i, int i2) {
        if (f1664a && this.f1665b) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    @TargetApi(19)
    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (f1664a && this.f1665b) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        int height;
        if (f1664a && this.f1665b) {
            height = i2 - view.getHeight();
        } else {
            height = i2;
        }
        super.update(view, i, height, i3, i4);
    }

    private static void m3010a(PopupWindow popupWindow) {
        try {
            Field declaredField = PopupWindow.class.getDeclaredField("mAnchor");
            declaredField.setAccessible(true);
            Field declaredField2 = PopupWindow.class.getDeclaredField("mOnScrollChangedListener");
            declaredField2.setAccessible(true);
            declaredField2.set(popupWindow, new AppCompatPopupWindow(declaredField, popupWindow, (OnScrollChangedListener) declaredField2.get(popupWindow)));
        } catch (Throwable e) {
            Log.d("AppCompatPopupWindow", "Exception while installing workaround OnScrollChangedListener", e);
        }
    }

    public void m3011a(boolean z) {
        if (f1664a) {
            this.f1665b = z;
        } else {
            PopupWindowCompat.m1705a((PopupWindow) this, z);
        }
    }
}
