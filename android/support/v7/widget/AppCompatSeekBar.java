package android.support.v7.widget;

import android.content.Context;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* renamed from: android.support.v7.widget.v */
public class AppCompatSeekBar extends SeekBar {
    private AppCompatSeekBarHelper f1674a;
    private AppCompatDrawableManager f1675b;

    public AppCompatSeekBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.seekBarStyle);
    }

    public AppCompatSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1675b = AppCompatDrawableManager.m2981a();
        this.f1674a = new AppCompatSeekBarHelper(this, this.f1675b);
        this.f1674a.m3017a(attributeSet, i);
    }
}
