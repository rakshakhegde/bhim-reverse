package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.widget.TintableCompoundButton;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.CheckBox;

/* renamed from: android.support.v7.widget.i */
public class AppCompatCheckBox extends CheckBox implements TintableCompoundButton {
    private AppCompatDrawableManager f1623a;
    private AppCompatCompoundButtonHelper f1624b;

    public AppCompatCheckBox(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.checkboxStyle);
    }

    public AppCompatCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1623a = AppCompatDrawableManager.m2981a();
        this.f1624b = new AppCompatCompoundButtonHelper(this, this.f1623a);
        this.f1624b.m2965a(attributeSet, i);
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        if (this.f1624b != null) {
            this.f1624b.m2967c();
        }
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(this.f1623a != null ? this.f1623a.m3004a(getContext(), i) : ContextCompat.m77a(getContext(), i));
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return this.f1624b != null ? this.f1624b.m2961a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        if (this.f1624b != null) {
            this.f1624b.m2963a(colorStateList);
        }
    }

    public ColorStateList getSupportButtonTintList() {
        return this.f1624b != null ? this.f1624b.m2962a() : null;
    }

    public void setSupportButtonTintMode(Mode mode) {
        if (this.f1624b != null) {
            this.f1624b.m2964a(mode);
        }
    }

    public Mode getSupportButtonTintMode() {
        return this.f1624b != null ? this.f1624b.m2966b() : null;
    }
}
