package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.p006f.GravityCompat;
import android.support.v4.p006f.MarginLayoutParamsCompat;
import android.support.v4.p006f.MenuItemCompat;
import android.support.v4.p006f.MotionEventCompat;
import android.support.v4.p006f.af;
import android.support.v7.p013a.ActionBar.ActionBar;
import android.support.v7.p014b.R.R;
import android.support.v7.view.CollapsibleActionView;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionMenuView.C0018e;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private boolean f1356A;
    private final ArrayList<View> f1357B;
    private final ArrayList<View> f1358C;
    private final int[] f1359D;
    private C0032c f1360E;
    private final C0018e f1361F;
    private as f1362G;
    private ActionMenuPresenter f1363H;
    private C0030a f1364I;
    private MenuPresenter f1365J;
    private MenuBuilder f1366K;
    private boolean f1367L;
    private final Runnable f1368M;
    private final AppCompatDrawableManager f1369N;
    View f1370a;
    private ActionMenuView f1371b;
    private TextView f1372c;
    private TextView f1373d;
    private ImageButton f1374e;
    private ImageView f1375f;
    private Drawable f1376g;
    private CharSequence f1377h;
    private ImageButton f1378i;
    private Context f1379j;
    private int f1380k;
    private int f1381l;
    private int f1382m;
    private int f1383n;
    private int f1384o;
    private int f1385p;
    private int f1386q;
    private int f1387r;
    private int f1388s;
    private final aj f1389t;
    private int f1390u;
    private CharSequence f1391v;
    private CharSequence f1392w;
    private int f1393x;
    private int f1394y;
    private boolean f1395z;

    /* renamed from: android.support.v7.widget.Toolbar.1 */
    class C00261 implements C0018e {
        final /* synthetic */ Toolbar f1347a;

        C00261(Toolbar toolbar) {
            this.f1347a = toolbar;
        }

        public boolean m2615a(MenuItem menuItem) {
            if (this.f1347a.f1360E != null) {
                return this.f1347a.f1360E.m2626a(menuItem);
            }
            return false;
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar.2 */
    class C00272 implements Runnable {
        final /* synthetic */ Toolbar f1348a;

        C00272(Toolbar toolbar) {
            this.f1348a = toolbar;
        }

        public void run() {
            this.f1348a.m2662d();
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar.3 */
    class C00283 implements OnClickListener {
        final /* synthetic */ Toolbar f1349a;

        C00283(Toolbar toolbar) {
            this.f1349a = toolbar;
        }

        public void onClick(View view) {
            this.f1349a.m2666h();
        }
    }

    public static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        int f1350a;
        boolean f1351b;

        /* renamed from: android.support.v7.widget.Toolbar.SavedState.1 */
        static class C00291 implements Creator<SavedState> {
            C00291() {
            }

            public /* synthetic */ Object createFromParcel(Parcel parcel) {
                return m2616a(parcel);
            }

            public /* synthetic */ Object[] newArray(int i) {
                return m2617a(i);
            }

            public SavedState m2616a(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] m2617a(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f1350a = parcel.readInt();
            this.f1351b = parcel.readInt() != 0;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1350a);
            parcel.writeInt(this.f1351b ? 1 : 0);
        }

        static {
            CREATOR = new C00291();
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar.a */
    private class C0030a implements android.support.v7.view.menu.MenuPresenter {
        android.support.v7.view.menu.MenuBuilder f1352a;
        MenuItemImpl f1353b;
        final /* synthetic */ Toolbar f1354c;

        private C0030a(Toolbar toolbar) {
            this.f1354c = toolbar;
        }

        public void m2618a(Context context, android.support.v7.view.menu.MenuBuilder menuBuilder) {
            if (!(this.f1352a == null || this.f1353b == null)) {
                this.f1352a.m2356d(this.f1353b);
            }
            this.f1352a = menuBuilder;
        }

        public void m2622b(boolean z) {
            Object obj = null;
            if (this.f1353b != null) {
                if (this.f1352a != null) {
                    int size = this.f1352a.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f1352a.getItem(i) == this.f1353b) {
                            obj = 1;
                            break;
                        }
                    }
                }
                if (obj == null) {
                    m2624b(this.f1352a, this.f1353b);
                }
            }
        }

        public boolean m2621a(SubMenuBuilder subMenuBuilder) {
            return false;
        }

        public void m2619a(android.support.v7.view.menu.MenuBuilder menuBuilder, boolean z) {
        }

        public boolean m2623b() {
            return false;
        }

        public boolean m2620a(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            this.f1354c.m2649p();
            if (this.f1354c.f1378i.getParent() != this.f1354c) {
                this.f1354c.addView(this.f1354c.f1378i);
            }
            this.f1354c.f1370a = menuItemImpl.getActionView();
            this.f1353b = menuItemImpl;
            if (this.f1354c.f1370a.getParent() != this.f1354c) {
                LayoutParams i = this.f1354c.m2667i();
                i.a = 8388611 | (this.f1354c.f1383n & 112);
                i.f1355b = 2;
                this.f1354c.f1370a.setLayoutParams(i);
                this.f1354c.addView(this.f1354c.f1370a);
            }
            this.f1354c.m2668j();
            this.f1354c.requestLayout();
            menuItemImpl.m2394e(true);
            if (this.f1354c.f1370a instanceof CollapsibleActionView) {
                ((CollapsibleActionView) this.f1354c.f1370a).m2172a();
            }
            return true;
        }

        public boolean m2624b(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            if (this.f1354c.f1370a instanceof CollapsibleActionView) {
                ((CollapsibleActionView) this.f1354c.f1370a).m2173b();
            }
            this.f1354c.removeView(this.f1354c.f1370a);
            this.f1354c.removeView(this.f1354c.f1378i);
            this.f1354c.f1370a = null;
            this.f1354c.m2669k();
            this.f1353b = null;
            this.f1354c.requestLayout();
            menuItemImpl.m2394e(false);
            return true;
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar.b */
    public static class C0031b extends ActionBar {
        int f1355b;

        public C0031b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f1355b = 0;
        }

        public C0031b(int i, int i2) {
            super(i, i2);
            this.f1355b = 0;
            this.a = 8388627;
        }

        public C0031b(C0031b c0031b) {
            super((ActionBar) c0031b);
            this.f1355b = 0;
            this.f1355b = c0031b.f1355b;
        }

        public C0031b(ActionBar actionBar) {
            super(actionBar);
            this.f1355b = 0;
        }

        public C0031b(MarginLayoutParams marginLayoutParams) {
            super((LayoutParams) marginLayoutParams);
            this.f1355b = 0;
            m2625a(marginLayoutParams);
        }

        public C0031b(LayoutParams layoutParams) {
            super(layoutParams);
            this.f1355b = 0;
        }

        void m2625a(MarginLayoutParams marginLayoutParams) {
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    /* renamed from: android.support.v7.widget.Toolbar.c */
    public interface C0032c {
        boolean m2626a(MenuItem menuItem);
    }

    protected /* synthetic */ LayoutParams generateDefaultLayoutParams() {
        return m2667i();
    }

    public /* synthetic */ LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return m2652a(attributeSet);
    }

    protected /* synthetic */ LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return m2653a(layoutParams);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1389t = new aj();
        this.f1390u = 8388627;
        this.f1357B = new ArrayList();
        this.f1358C = new ArrayList();
        this.f1359D = new int[2];
        this.f1361F = new C00261(this);
        this.f1368M = new C00272(this);
        ar a = ar.m2811a(getContext(), attributeSet, R.Toolbar, i, 0);
        this.f1381l = a.m2827g(R.Toolbar_titleTextAppearance, 0);
        this.f1382m = a.m2827g(R.Toolbar_subtitleTextAppearance, 0);
        this.f1390u = a.m2819c(R.Toolbar_android_gravity, this.f1390u);
        this.f1383n = 48;
        int d = a.m2821d(R.Toolbar_titleMargins, 0);
        this.f1388s = d;
        this.f1387r = d;
        this.f1386q = d;
        this.f1385p = d;
        d = a.m2821d(R.Toolbar_titleMarginStart, -1);
        if (d >= 0) {
            this.f1385p = d;
        }
        d = a.m2821d(R.Toolbar_titleMarginEnd, -1);
        if (d >= 0) {
            this.f1386q = d;
        }
        d = a.m2821d(R.Toolbar_titleMarginTop, -1);
        if (d >= 0) {
            this.f1387r = d;
        }
        d = a.m2821d(R.Toolbar_titleMarginBottom, -1);
        if (d >= 0) {
            this.f1388s = d;
        }
        this.f1384o = a.m2823e(R.Toolbar_maxButtonHeight, -1);
        d = a.m2821d(R.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int d2 = a.m2821d(R.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        this.f1389t.m2763b(a.m2823e(R.Toolbar_contentInsetLeft, 0), a.m2823e(R.Toolbar_contentInsetRight, 0));
        if (!(d == Integer.MIN_VALUE && d2 == Integer.MIN_VALUE)) {
            this.f1389t.m2760a(d, d2);
        }
        this.f1376g = a.m2814a(R.Toolbar_collapseIcon);
        this.f1377h = a.m2820c(R.Toolbar_collapseContentDescription);
        CharSequence c = a.m2820c(R.Toolbar_title);
        if (!TextUtils.isEmpty(c)) {
            setTitle(c);
        }
        c = a.m2820c(R.Toolbar_subtitle);
        if (!TextUtils.isEmpty(c)) {
            setSubtitle(c);
        }
        this.f1379j = getContext();
        setPopupTheme(a.m2827g(R.Toolbar_popupTheme, 0));
        Drawable a2 = a.m2814a(R.Toolbar_navigationIcon);
        if (a2 != null) {
            setNavigationIcon(a2);
        }
        c = a.m2820c(R.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(c)) {
            setNavigationContentDescription(c);
        }
        a2 = a.m2814a(R.Toolbar_logo);
        if (a2 != null) {
            setLogo(a2);
        }
        c = a.m2820c(R.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(c)) {
            setLogoDescription(c);
        }
        if (a.m2826f(R.Toolbar_titleTextColor)) {
            setTitleTextColor(a.m2817b(R.Toolbar_titleTextColor, -1));
        }
        if (a.m2826f(R.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(a.m2817b(R.Toolbar_subtitleTextColor, -1));
        }
        a.m2815a();
        this.f1369N = AppCompatDrawableManager.m2981a();
    }

    public void setPopupTheme(int i) {
        if (this.f1380k != i) {
            this.f1380k = i;
            if (i == 0) {
                this.f1379j = getContext();
            } else {
                this.f1379j = new ContextThemeWrapper(getContext(), i);
            }
        }
    }

    public int getPopupTheme() {
        return this.f1380k;
    }

    public void onRtlPropertiesChanged(int i) {
        boolean z = true;
        if (VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i);
        }
        aj ajVar = this.f1389t;
        if (i != 1) {
            z = false;
        }
        ajVar.m2761a(z);
    }

    public void setLogo(int i) {
        setLogo(this.f1369N.m3004a(getContext(), i));
    }

    public boolean m2658a() {
        return getVisibility() == 0 && this.f1371b != null && this.f1371b.m2544a();
    }

    public boolean m2660b() {
        return this.f1371b != null && this.f1371b.m2554g();
    }

    public boolean m2661c() {
        return this.f1371b != null && this.f1371b.m2555h();
    }

    public boolean m2662d() {
        return this.f1371b != null && this.f1371b.m2552e();
    }

    public boolean m2663e() {
        return this.f1371b != null && this.f1371b.m2553f();
    }

    public void m2656a(android.support.v7.view.menu.MenuBuilder menuBuilder, ActionMenuPresenter actionMenuPresenter) {
        if (menuBuilder != null || this.f1371b != null) {
            m2647n();
            android.support.v7.view.menu.MenuBuilder d = this.f1371b.m2551d();
            if (d != menuBuilder) {
                if (d != null) {
                    d.m2348b(this.f1363H);
                    d.m2348b(this.f1364I);
                }
                if (this.f1364I == null) {
                    this.f1364I = new C0030a();
                }
                actionMenuPresenter.m2922d(true);
                if (menuBuilder != null) {
                    menuBuilder.m2338a((android.support.v7.view.menu.MenuPresenter) actionMenuPresenter, this.f1379j);
                    menuBuilder.m2338a(this.f1364I, this.f1379j);
                } else {
                    actionMenuPresenter.m2908a(this.f1379j, null);
                    this.f1364I.m2618a(this.f1379j, null);
                    actionMenuPresenter.m2918b(true);
                    this.f1364I.m2622b(true);
                }
                this.f1371b.setPopupTheme(this.f1380k);
                this.f1371b.setPresenter(actionMenuPresenter);
                this.f1363H = actionMenuPresenter;
            }
        }
    }

    public void m2664f() {
        if (this.f1371b != null) {
            this.f1371b.m2556i();
        }
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            m2645l();
            if (!m2644d(this.f1375f)) {
                m2634a(this.f1375f, true);
            }
        } else if (this.f1375f != null && m2644d(this.f1375f)) {
            removeView(this.f1375f);
            this.f1358C.remove(this.f1375f);
        }
        if (this.f1375f != null) {
            this.f1375f.setImageDrawable(drawable);
        }
    }

    public Drawable getLogo() {
        return this.f1375f != null ? this.f1375f.getDrawable() : null;
    }

    public void setLogoDescription(int i) {
        setLogoDescription(getContext().getText(i));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m2645l();
        }
        if (this.f1375f != null) {
            this.f1375f.setContentDescription(charSequence);
        }
    }

    public CharSequence getLogoDescription() {
        return this.f1375f != null ? this.f1375f.getContentDescription() : null;
    }

    private void m2645l() {
        if (this.f1375f == null) {
            this.f1375f = new ImageView(getContext());
        }
    }

    public boolean m2665g() {
        return (this.f1364I == null || this.f1364I.f1353b == null) ? false : true;
    }

    public void m2666h() {
        MenuItemImpl menuItemImpl = this.f1364I == null ? null : this.f1364I.f1353b;
        if (menuItemImpl != null) {
            menuItemImpl.collapseActionView();
        }
    }

    public CharSequence getTitle() {
        return this.f1391v;
    }

    public void setTitle(int i) {
        setTitle(getContext().getText(i));
    }

    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f1372c == null) {
                Context context = getContext();
                this.f1372c = new TextView(context);
                this.f1372c.setSingleLine();
                this.f1372c.setEllipsize(TruncateAt.END);
                if (this.f1381l != 0) {
                    this.f1372c.setTextAppearance(context, this.f1381l);
                }
                if (this.f1393x != 0) {
                    this.f1372c.setTextColor(this.f1393x);
                }
            }
            if (!m2644d(this.f1372c)) {
                m2634a(this.f1372c, true);
            }
        } else if (this.f1372c != null && m2644d(this.f1372c)) {
            removeView(this.f1372c);
            this.f1358C.remove(this.f1372c);
        }
        if (this.f1372c != null) {
            this.f1372c.setText(charSequence);
        }
        this.f1391v = charSequence;
    }

    public CharSequence getSubtitle() {
        return this.f1392w;
    }

    public void setSubtitle(int i) {
        setSubtitle(getContext().getText(i));
    }

    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f1373d == null) {
                Context context = getContext();
                this.f1373d = new TextView(context);
                this.f1373d.setSingleLine();
                this.f1373d.setEllipsize(TruncateAt.END);
                if (this.f1382m != 0) {
                    this.f1373d.setTextAppearance(context, this.f1382m);
                }
                if (this.f1394y != 0) {
                    this.f1373d.setTextColor(this.f1394y);
                }
            }
            if (!m2644d(this.f1373d)) {
                m2634a(this.f1373d, true);
            }
        } else if (this.f1373d != null && m2644d(this.f1373d)) {
            removeView(this.f1373d);
            this.f1358C.remove(this.f1373d);
        }
        if (this.f1373d != null) {
            this.f1373d.setText(charSequence);
        }
        this.f1392w = charSequence;
    }

    public void m2655a(Context context, int i) {
        this.f1381l = i;
        if (this.f1372c != null) {
            this.f1372c.setTextAppearance(context, i);
        }
    }

    public void m2659b(Context context, int i) {
        this.f1382m = i;
        if (this.f1373d != null) {
            this.f1373d.setTextAppearance(context, i);
        }
    }

    public void setTitleTextColor(int i) {
        this.f1393x = i;
        if (this.f1372c != null) {
            this.f1372c.setTextColor(i);
        }
    }

    public void setSubtitleTextColor(int i) {
        this.f1394y = i;
        if (this.f1373d != null) {
            this.f1373d.setTextColor(i);
        }
    }

    public CharSequence getNavigationContentDescription() {
        return this.f1374e != null ? this.f1374e.getContentDescription() : null;
    }

    public void setNavigationContentDescription(int i) {
        setNavigationContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m2648o();
        }
        if (this.f1374e != null) {
            this.f1374e.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i) {
        setNavigationIcon(this.f1369N.m3004a(getContext(), i));
    }

    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            m2648o();
            if (!m2644d(this.f1374e)) {
                m2634a(this.f1374e, true);
            }
        } else if (this.f1374e != null && m2644d(this.f1374e)) {
            removeView(this.f1374e);
            this.f1358C.remove(this.f1374e);
        }
        if (this.f1374e != null) {
            this.f1374e.setImageDrawable(drawable);
        }
    }

    public Drawable getNavigationIcon() {
        return this.f1374e != null ? this.f1374e.getDrawable() : null;
    }

    public void setNavigationOnClickListener(OnClickListener onClickListener) {
        m2648o();
        this.f1374e.setOnClickListener(onClickListener);
    }

    public Menu getMenu() {
        m2646m();
        return this.f1371b.getMenu();
    }

    public void setOverflowIcon(Drawable drawable) {
        m2646m();
        this.f1371b.setOverflowIcon(drawable);
    }

    public Drawable getOverflowIcon() {
        m2646m();
        return this.f1371b.getOverflowIcon();
    }

    private void m2646m() {
        m2647n();
        if (this.f1371b.m2551d() == null) {
            android.support.v7.view.menu.MenuBuilder menuBuilder = (android.support.v7.view.menu.MenuBuilder) this.f1371b.getMenu();
            if (this.f1364I == null) {
                this.f1364I = new C0030a();
            }
            this.f1371b.setExpandedActionViewsExclusive(true);
            menuBuilder.m2338a(this.f1364I, this.f1379j);
        }
    }

    private void m2647n() {
        if (this.f1371b == null) {
            this.f1371b = new ActionMenuView(getContext());
            this.f1371b.setPopupTheme(this.f1380k);
            this.f1371b.setOnMenuItemClickListener(this.f1361F);
            this.f1371b.m2543a(this.f1365J, this.f1366K);
            LayoutParams i = m2667i();
            i.a = 8388613 | (this.f1383n & 112);
            this.f1371b.setLayoutParams(i);
            m2634a(this.f1371b, false);
        }
    }

    private MenuInflater getMenuInflater() {
        return new SupportMenuInflater(getContext());
    }

    public void setOnMenuItemClickListener(C0032c c0032c) {
        this.f1360E = c0032c;
    }

    public void m2654a(int i, int i2) {
        this.f1389t.m2760a(i, i2);
    }

    public int getContentInsetStart() {
        return this.f1389t.m2764c();
    }

    public int getContentInsetEnd() {
        return this.f1389t.m2765d();
    }

    public int getContentInsetLeft() {
        return this.f1389t.m2759a();
    }

    public int getContentInsetRight() {
        return this.f1389t.m2762b();
    }

    private void m2648o() {
        if (this.f1374e == null) {
            this.f1374e = new ImageButton(getContext(), null, R.toolbarNavigationButtonStyle);
            LayoutParams i = m2667i();
            i.a = 8388611 | (this.f1383n & 112);
            this.f1374e.setLayoutParams(i);
        }
    }

    private void m2649p() {
        if (this.f1378i == null) {
            this.f1378i = new ImageButton(getContext(), null, R.toolbarNavigationButtonStyle);
            this.f1378i.setImageDrawable(this.f1376g);
            this.f1378i.setContentDescription(this.f1377h);
            LayoutParams i = m2667i();
            i.a = 8388611 | (this.f1383n & 112);
            i.f1355b = 2;
            this.f1378i.setLayoutParams(i);
            this.f1378i.setOnClickListener(new C00283(this));
        }
    }

    private void m2634a(View view, boolean z) {
        LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = m2667i();
        } else if (checkLayoutParams(layoutParams)) {
            C0031b c0031b = (C0031b) layoutParams;
        } else {
            layoutParams = m2653a(layoutParams);
        }
        layoutParams.f1355b = 1;
        if (!z || this.f1370a == null) {
            addView(view, layoutParams);
            return;
        }
        view.setLayoutParams(layoutParams);
        this.f1358C.add(view);
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.f1364I == null || this.f1364I.f1353b == null)) {
            savedState.f1350a = this.f1364I.f1353b.getItemId();
        }
        savedState.f1351b = m2660b();
        return savedState;
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            Menu d = this.f1371b != null ? this.f1371b.m2551d() : null;
            if (!(savedState.f1350a == 0 || this.f1364I == null || d == null)) {
                MenuItem findItem = d.findItem(savedState.f1350a);
                if (findItem != null) {
                    MenuItemCompat.m1481b(findItem);
                }
            }
            if (savedState.f1351b) {
                m2650q();
                return;
            }
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    private void m2650q() {
        removeCallbacks(this.f1368M);
        post(this.f1368M);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.f1368M);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m1507a(motionEvent);
        if (a == 0) {
            this.f1395z = false;
        }
        if (!this.f1395z) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (a == 0 && !onTouchEvent) {
                this.f1395z = true;
            }
        }
        if (a == 1 || a == 3) {
            this.f1395z = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m1507a(motionEvent);
        if (a == 9) {
            this.f1356A = false;
        }
        if (!this.f1356A) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (a == 9 && !onHoverEvent) {
                this.f1356A = true;
            }
        }
        if (a == 10 || a == 3) {
            this.f1356A = false;
        }
        return true;
    }

    private void m2633a(View view, int i, int i2, int i3, int i4, int i5) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i, (((getPaddingLeft() + getPaddingRight()) + marginLayoutParams.leftMargin) + marginLayoutParams.rightMargin) + i2, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i3, (((getPaddingTop() + getPaddingBottom()) + marginLayoutParams.topMargin) + marginLayoutParams.bottomMargin) + i4, marginLayoutParams.height);
        int mode = MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i5 >= 0) {
            if (mode != 0) {
                i5 = Math.min(MeasureSpec.getSize(childMeasureSpec2), i5);
            }
            childMeasureSpec2 = MeasureSpec.makeMeasureSpec(i5, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private int m2629a(View view, int i, int i2, int i3, int i4, int[] iArr) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        int i5 = marginLayoutParams.leftMargin - iArr[0];
        int i6 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i5) + Math.max(0, i6);
        iArr[0] = Math.max(0, -i5);
        iArr[1] = Math.max(0, -i6);
        view.measure(getChildMeasureSpec(i, ((getPaddingLeft() + getPaddingRight()) + max) + i2, marginLayoutParams.width), getChildMeasureSpec(i3, (((getPaddingTop() + getPaddingBottom()) + marginLayoutParams.topMargin) + marginLayoutParams.bottomMargin) + i4, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private boolean m2651r() {
        if (!this.f1367L) {
            return false;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (m2636a(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    protected void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int max;
        int i5 = 0;
        int i6 = 0;
        int[] iArr = this.f1359D;
        if (au.m2883a(this)) {
            i3 = 0;
            i4 = 1;
        } else {
            i3 = 1;
            i4 = 0;
        }
        int i7 = 0;
        if (m2636a(this.f1374e)) {
            m2633a(this.f1374e, i, 0, i2, 0, this.f1384o);
            i7 = this.f1374e.getMeasuredWidth() + m2638b(this.f1374e);
            max = Math.max(0, this.f1374e.getMeasuredHeight() + m2641c(this.f1374e));
            i6 = au.m2881a(0, af.m1194f(this.f1374e));
            i5 = max;
        }
        if (m2636a(this.f1378i)) {
            m2633a(this.f1378i, i, 0, i2, 0, this.f1384o);
            i7 = this.f1378i.getMeasuredWidth() + m2638b(this.f1378i);
            i5 = Math.max(i5, this.f1378i.getMeasuredHeight() + m2641c(this.f1378i));
            i6 = au.m2881a(i6, af.m1194f(this.f1378i));
        }
        int contentInsetStart = getContentInsetStart();
        int max2 = 0 + Math.max(contentInsetStart, i7);
        iArr[i4] = Math.max(0, contentInsetStart - i7);
        i7 = 0;
        if (m2636a(this.f1371b)) {
            m2633a(this.f1371b, i, max2, i2, 0, this.f1384o);
            i7 = this.f1371b.getMeasuredWidth() + m2638b(this.f1371b);
            i5 = Math.max(i5, this.f1371b.getMeasuredHeight() + m2641c(this.f1371b));
            i6 = au.m2881a(i6, af.m1194f(this.f1371b));
        }
        contentInsetStart = getContentInsetEnd();
        max2 += Math.max(contentInsetStart, i7);
        iArr[i3] = Math.max(0, contentInsetStart - i7);
        if (m2636a(this.f1370a)) {
            max2 += m2629a(this.f1370a, i, max2, i2, 0, iArr);
            i5 = Math.max(i5, this.f1370a.getMeasuredHeight() + m2641c(this.f1370a));
            i6 = au.m2881a(i6, af.m1194f(this.f1370a));
        }
        if (m2636a(this.f1375f)) {
            max2 += m2629a(this.f1375f, i, max2, i2, 0, iArr);
            i5 = Math.max(i5, this.f1375f.getMeasuredHeight() + m2641c(this.f1375f));
            i6 = au.m2881a(i6, af.m1194f(this.f1375f));
        }
        i4 = getChildCount();
        i3 = 0;
        int i8 = i5;
        i5 = i6;
        while (i3 < i4) {
            View childAt = getChildAt(i3);
            if (((C0031b) childAt.getLayoutParams()).f1355b != 0) {
                i7 = i5;
                contentInsetStart = i8;
            } else if (m2636a(childAt)) {
                max2 += m2629a(childAt, i, max2, i2, 0, iArr);
                max = Math.max(i8, childAt.getMeasuredHeight() + m2641c(childAt));
                i7 = au.m2881a(i5, af.m1194f(childAt));
                contentInsetStart = max;
            } else {
                i7 = i5;
                contentInsetStart = i8;
            }
            i3++;
            i5 = i7;
            i8 = contentInsetStart;
        }
        contentInsetStart = 0;
        i7 = 0;
        i6 = this.f1387r + this.f1388s;
        max = this.f1385p + this.f1386q;
        if (m2636a(this.f1372c)) {
            m2629a(this.f1372c, i, max2 + max, i2, i6, iArr);
            contentInsetStart = m2638b(this.f1372c) + this.f1372c.getMeasuredWidth();
            i7 = this.f1372c.getMeasuredHeight() + m2641c(this.f1372c);
            i5 = au.m2881a(i5, af.m1194f(this.f1372c));
        }
        if (m2636a(this.f1373d)) {
            contentInsetStart = Math.max(contentInsetStart, m2629a(this.f1373d, i, max2 + max, i2, i6 + i7, iArr));
            i7 += this.f1373d.getMeasuredHeight() + m2641c(this.f1373d);
            i5 = au.m2881a(i5, af.m1194f(this.f1373d));
        }
        contentInsetStart += max2;
        i7 = Math.max(i8, i7) + (getPaddingTop() + getPaddingBottom());
        contentInsetStart = af.m1172a(Math.max(contentInsetStart + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), i, -16777216 & i5);
        i7 = af.m1172a(Math.max(i7, getSuggestedMinimumHeight()), i2, i5 << 16);
        if (m2651r()) {
            i7 = 0;
        }
        setMeasuredDimension(contentInsetStart, i7);
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        Object obj;
        int i5;
        int i6;
        int i7;
        int measuredHeight;
        int measuredWidth;
        if (af.m1191d(this) == 1) {
            obj = 1;
        } else {
            obj = null;
        }
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i8 = width - paddingRight;
        int[] iArr = this.f1359D;
        iArr[1] = 0;
        iArr[0] = 0;
        int j = af.m1198j(this);
        if (!m2636a(this.f1374e)) {
            i5 = paddingLeft;
        } else if (obj != null) {
            i8 = m2639b(this.f1374e, i8, iArr, j);
            i5 = paddingLeft;
        } else {
            i5 = m2630a(this.f1374e, paddingLeft, iArr, j);
        }
        if (m2636a(this.f1378i)) {
            if (obj != null) {
                i8 = m2639b(this.f1378i, i8, iArr, j);
            } else {
                i5 = m2630a(this.f1378i, i5, iArr, j);
            }
        }
        if (m2636a(this.f1371b)) {
            if (obj != null) {
                i5 = m2630a(this.f1371b, i5, iArr, j);
            } else {
                i8 = m2639b(this.f1371b, i8, iArr, j);
            }
        }
        iArr[0] = Math.max(0, getContentInsetLeft() - i5);
        iArr[1] = Math.max(0, getContentInsetRight() - ((width - paddingRight) - i8));
        i5 = Math.max(i5, getContentInsetLeft());
        i8 = Math.min(i8, (width - paddingRight) - getContentInsetRight());
        if (m2636a(this.f1370a)) {
            if (obj != null) {
                i8 = m2639b(this.f1370a, i8, iArr, j);
            } else {
                i5 = m2630a(this.f1370a, i5, iArr, j);
            }
        }
        if (!m2636a(this.f1375f)) {
            i6 = i8;
            i7 = i5;
        } else if (obj != null) {
            i6 = m2639b(this.f1375f, i8, iArr, j);
            i7 = i5;
        } else {
            i6 = i8;
            i7 = m2630a(this.f1375f, i5, iArr, j);
        }
        boolean a = m2636a(this.f1372c);
        boolean a2 = m2636a(this.f1373d);
        i5 = 0;
        if (a) {
            C0031b c0031b = (C0031b) this.f1372c.getLayoutParams();
            i5 = 0 + (c0031b.bottomMargin + (c0031b.topMargin + this.f1372c.getMeasuredHeight()));
        }
        if (a2) {
            c0031b = (C0031b) this.f1373d.getLayoutParams();
            measuredHeight = (c0031b.bottomMargin + (c0031b.topMargin + this.f1373d.getMeasuredHeight())) + i5;
        } else {
            measuredHeight = i5;
        }
        if (a || a2) {
            int paddingTop2;
            c0031b = (C0031b) (a ? this.f1372c : this.f1373d).getLayoutParams();
            C0031b c0031b2 = (C0031b) (a2 ? this.f1373d : this.f1372c).getLayoutParams();
            Object obj2 = ((!a || this.f1372c.getMeasuredWidth() <= 0) && (!a2 || this.f1373d.getMeasuredWidth() <= 0)) ? null : 1;
            switch (this.f1390u & 112) {
                case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_homeAsUpIndicator /*48*/:
                    paddingTop2 = (c0031b.topMargin + getPaddingTop()) + this.f1387r;
                    break;
                case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                    paddingTop2 = (((height - paddingBottom) - c0031b2.bottomMargin) - this.f1388s) - measuredHeight;
                    break;
                default:
                    paddingTop2 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                    if (paddingTop2 < c0031b.topMargin + this.f1387r) {
                        i8 = c0031b.topMargin + this.f1387r;
                    } else {
                        measuredHeight = (((height - paddingBottom) - measuredHeight) - paddingTop2) - paddingTop;
                        if (measuredHeight < c0031b.bottomMargin + this.f1388s) {
                            i8 = Math.max(0, paddingTop2 - ((c0031b2.bottomMargin + this.f1388s) - measuredHeight));
                        } else {
                            i8 = paddingTop2;
                        }
                    }
                    paddingTop2 = paddingTop + i8;
                    break;
            }
            if (obj != null) {
                i8 = (obj2 != null ? this.f1385p : 0) - iArr[1];
                i5 = i6 - Math.max(0, i8);
                iArr[1] = Math.max(0, -i8);
                if (a) {
                    c0031b = (C0031b) this.f1372c.getLayoutParams();
                    measuredWidth = i5 - this.f1372c.getMeasuredWidth();
                    i6 = this.f1372c.getMeasuredHeight() + paddingTop2;
                    this.f1372c.layout(measuredWidth, paddingTop2, i5, i6);
                    paddingTop2 = i6 + c0031b.bottomMargin;
                    i6 = measuredWidth - this.f1386q;
                } else {
                    i6 = i5;
                }
                if (a2) {
                    c0031b = (C0031b) this.f1373d.getLayoutParams();
                    measuredWidth = c0031b.topMargin + paddingTop2;
                    measuredHeight = this.f1373d.getMeasuredHeight() + measuredWidth;
                    this.f1373d.layout(i5 - this.f1373d.getMeasuredWidth(), measuredWidth, i5, measuredHeight);
                    i8 = c0031b.bottomMargin + measuredHeight;
                    i8 = i5 - this.f1386q;
                } else {
                    i8 = i5;
                }
                if (obj2 != null) {
                    i8 = Math.min(i6, i8);
                } else {
                    i8 = i5;
                }
                i6 = i8;
            } else {
                i8 = (obj2 != null ? this.f1385p : 0) - iArr[0];
                i7 += Math.max(0, i8);
                iArr[0] = Math.max(0, -i8);
                if (a) {
                    c0031b = (C0031b) this.f1372c.getLayoutParams();
                    i5 = this.f1372c.getMeasuredWidth() + i7;
                    measuredWidth = this.f1372c.getMeasuredHeight() + paddingTop2;
                    this.f1372c.layout(i7, paddingTop2, i5, measuredWidth);
                    i8 = c0031b.bottomMargin + measuredWidth;
                    measuredWidth = i5 + this.f1386q;
                    i5 = i8;
                } else {
                    measuredWidth = i7;
                    i5 = paddingTop2;
                }
                if (a2) {
                    c0031b = (C0031b) this.f1373d.getLayoutParams();
                    i5 += c0031b.topMargin;
                    paddingTop2 = this.f1373d.getMeasuredWidth() + i7;
                    measuredHeight = this.f1373d.getMeasuredHeight() + i5;
                    this.f1373d.layout(i7, i5, paddingTop2, measuredHeight);
                    i8 = c0031b.bottomMargin + measuredHeight;
                    i8 = this.f1386q + paddingTop2;
                } else {
                    i8 = i7;
                }
                if (obj2 != null) {
                    i7 = Math.max(measuredWidth, i8);
                }
            }
        }
        m2635a(this.f1357B, 3);
        int size = this.f1357B.size();
        i5 = i7;
        for (measuredWidth = 0; measuredWidth < size; measuredWidth++) {
            i5 = m2630a((View) this.f1357B.get(measuredWidth), i5, iArr, j);
        }
        m2635a(this.f1357B, 5);
        i7 = this.f1357B.size();
        for (measuredWidth = 0; measuredWidth < i7; measuredWidth++) {
            i6 = m2639b((View) this.f1357B.get(measuredWidth), i6, iArr, j);
        }
        m2635a(this.f1357B, 1);
        measuredWidth = m2631a(this.f1357B, iArr);
        i8 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (measuredWidth / 2);
        measuredWidth += i8;
        if (i8 < i5) {
            i8 = i5;
        } else if (measuredWidth > i6) {
            i8 -= measuredWidth - i6;
        }
        paddingLeft = this.f1357B.size();
        measuredWidth = i8;
        for (i5 = 0; i5 < paddingLeft; i5++) {
            measuredWidth = m2630a((View) this.f1357B.get(i5), measuredWidth, iArr, j);
        }
        this.f1357B.clear();
    }

    private int m2631a(List<View> list, int[] iArr) {
        int i = iArr[0];
        int i2 = iArr[1];
        int size = list.size();
        int i3 = 0;
        int i4 = 0;
        int i5 = i2;
        int i6 = i;
        while (i3 < size) {
            View view = (View) list.get(i3);
            C0031b c0031b = (C0031b) view.getLayoutParams();
            i6 = c0031b.leftMargin - i6;
            i = c0031b.rightMargin - i5;
            int max = Math.max(0, i6);
            int max2 = Math.max(0, i);
            i6 = Math.max(0, -i6);
            i5 = Math.max(0, -i);
            i3++;
            i4 += (view.getMeasuredWidth() + max) + max2;
        }
        return i4;
    }

    private int m2630a(View view, int i, int[] iArr, int i2) {
        C0031b c0031b = (C0031b) view.getLayoutParams();
        int i3 = c0031b.leftMargin - iArr[0];
        int max = Math.max(0, i3) + i;
        iArr[0] = Math.max(0, -i3);
        i3 = m2628a(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, i3, max + measuredWidth, view.getMeasuredHeight() + i3);
        return (c0031b.rightMargin + measuredWidth) + max;
    }

    private int m2639b(View view, int i, int[] iArr, int i2) {
        C0031b c0031b = (C0031b) view.getLayoutParams();
        int i3 = c0031b.rightMargin - iArr[1];
        int max = i - Math.max(0, i3);
        iArr[1] = Math.max(0, -i3);
        i3 = m2628a(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, i3, max, view.getMeasuredHeight() + i3);
        return max - (c0031b.leftMargin + measuredWidth);
    }

    private int m2628a(View view, int i) {
        C0031b c0031b = (C0031b) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i2 = i > 0 ? (measuredHeight - i) / 2 : 0;
        switch (m2627a(c0031b.a)) {
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_homeAsUpIndicator /*48*/:
                return getPaddingTop() - i2;
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - c0031b.bottomMargin) - i2;
            default:
                int i3;
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                i2 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i2 < c0031b.topMargin) {
                    i3 = c0031b.topMargin;
                } else {
                    measuredHeight = (((height - paddingBottom) - measuredHeight) - i2) - paddingTop;
                    i3 = measuredHeight < c0031b.bottomMargin ? Math.max(0, i2 - (c0031b.bottomMargin - measuredHeight)) : i2;
                }
                return i3 + paddingTop;
        }
    }

    private int m2627a(int i) {
        int i2 = i & 112;
        switch (i2) {
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_titleMarginBottom /*16*/:
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_homeAsUpIndicator /*48*/:
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                return i2;
            default:
                return this.f1390u & 112;
        }
    }

    private void m2635a(List<View> list, int i) {
        int i2 = 1;
        int i3 = 0;
        if (af.m1191d(this) != 1) {
            i2 = 0;
        }
        int childCount = getChildCount();
        int a = GravityCompat.m1420a(i, af.m1191d(this));
        list.clear();
        C0031b c0031b;
        if (i2 != 0) {
            for (i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                c0031b = (C0031b) childAt.getLayoutParams();
                if (c0031b.f1355b == 0 && m2636a(childAt) && m2637b(c0031b.a) == a) {
                    list.add(childAt);
                }
            }
            return;
        }
        while (i3 < childCount) {
            View childAt2 = getChildAt(i3);
            c0031b = (C0031b) childAt2.getLayoutParams();
            if (c0031b.f1355b == 0 && m2636a(childAt2) && m2637b(c0031b.a) == a) {
                list.add(childAt2);
            }
            i3++;
        }
    }

    private int m2637b(int i) {
        int d = af.m1191d(this);
        int a = GravityCompat.m1420a(i, d) & 7;
        switch (a) {
            case org.npci.upi.security.pinactivitycomponent.R.R.View_android_focusable /*1*/:
            case org.npci.upi.security.pinactivitycomponent.R.R.View_paddingEnd /*3*/:
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_contentInsetStart /*5*/:
                return a;
            default:
                return d == 1 ? 5 : 3;
        }
    }

    private boolean m2636a(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private int m2638b(View view) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        return MarginLayoutParamsCompat.m1451b(marginLayoutParams) + MarginLayoutParamsCompat.m1450a(marginLayoutParams);
    }

    private int m2641c(View view) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    public C0031b m2652a(AttributeSet attributeSet) {
        return new C0031b(getContext(), attributeSet);
    }

    protected C0031b m2653a(LayoutParams layoutParams) {
        if (layoutParams instanceof C0031b) {
            return new C0031b((C0031b) layoutParams);
        }
        if (layoutParams instanceof ActionBar) {
            return new C0031b((ActionBar) layoutParams);
        }
        if (layoutParams instanceof MarginLayoutParams) {
            return new C0031b((MarginLayoutParams) layoutParams);
        }
        return new C0031b(layoutParams);
    }

    protected C0031b m2667i() {
        return new C0031b(-2, -2);
    }

    protected boolean checkLayoutParams(LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof C0031b);
    }

    public ac getWrapper() {
        if (this.f1362G == null) {
            this.f1362G = new as(this, true);
        }
        return this.f1362G;
    }

    void m2668j() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((C0031b) childAt.getLayoutParams()).f1355b == 2 || childAt == this.f1371b)) {
                removeViewAt(childCount);
                this.f1358C.add(childAt);
            }
        }
    }

    void m2669k() {
        for (int size = this.f1358C.size() - 1; size >= 0; size--) {
            addView((View) this.f1358C.get(size));
        }
        this.f1358C.clear();
    }

    private boolean m2644d(View view) {
        return view.getParent() == this || this.f1358C.contains(view);
    }

    public void setCollapsible(boolean z) {
        this.f1367L = z;
        requestLayout();
    }

    public void m2657a(MenuPresenter menuPresenter, MenuBuilder menuBuilder) {
        this.f1365J = menuPresenter;
        this.f1366K = menuBuilder;
        if (this.f1371b != null) {
            this.f1371b.m2543a(menuPresenter, menuBuilder);
        }
    }
}
