package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.af;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: android.support.v7.widget.g */
class AppCompatBackgroundHelper {
    private final View f1615a;
    private final AppCompatDrawableManager f1616b;
    private ap f1617c;
    private ap f1618d;
    private ap f1619e;

    AppCompatBackgroundHelper(View view, AppCompatDrawableManager appCompatDrawableManager) {
        this.f1615a = view;
        this.f1616b = appCompatDrawableManager;
    }

    void m2957a(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = this.f1615a.getContext().obtainStyledAttributes(attributeSet, R.ViewBackgroundHelper, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(R.ViewBackgroundHelper_android_background)) {
                ColorStateList b = this.f1616b.m3007b(this.f1615a.getContext(), obtainStyledAttributes.getResourceId(R.ViewBackgroundHelper_android_background, -1));
                if (b != null) {
                    m2959b(b);
                }
            }
            if (obtainStyledAttributes.hasValue(R.ViewBackgroundHelper_backgroundTint)) {
                af.m1178a(this.f1615a, obtainStyledAttributes.getColorStateList(R.ViewBackgroundHelper_backgroundTint));
            }
            if (obtainStyledAttributes.hasValue(R.ViewBackgroundHelper_backgroundTintMode)) {
                af.m1179a(this.f1615a, ad.m2706a(obtainStyledAttributes.getInt(R.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
            obtainStyledAttributes.recycle();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
        }
    }

    void m2953a(int i) {
        m2959b(this.f1616b != null ? this.f1616b.m3007b(this.f1615a.getContext(), i) : null);
    }

    void m2956a(Drawable drawable) {
        m2959b(null);
    }

    void m2954a(ColorStateList colorStateList) {
        if (this.f1618d == null) {
            this.f1618d = new ap();
        }
        this.f1618d.f1525a = colorStateList;
        this.f1618d.f1528d = true;
        m2960c();
    }

    ColorStateList m2952a() {
        return this.f1618d != null ? this.f1618d.f1525a : null;
    }

    void m2955a(Mode mode) {
        if (this.f1618d == null) {
            this.f1618d = new ap();
        }
        this.f1618d.f1526b = mode;
        this.f1618d.f1527c = true;
        m2960c();
    }

    Mode m2958b() {
        return this.f1618d != null ? this.f1618d.f1526b : null;
    }

    void m2960c() {
        Drawable background = this.f1615a.getBackground();
        if (background == null) {
            return;
        }
        if (VERSION.SDK_INT != 21 || !m2951b(background)) {
            if (this.f1618d != null) {
                AppCompatDrawableManager.m2984a(background, this.f1618d, this.f1615a.getDrawableState());
            } else if (this.f1617c != null) {
                AppCompatDrawableManager.m2984a(background, this.f1617c, this.f1615a.getDrawableState());
            }
        }
    }

    void m2959b(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.f1617c == null) {
                this.f1617c = new ap();
            }
            this.f1617c.f1525a = colorStateList;
            this.f1617c.f1528d = true;
        } else {
            this.f1617c = null;
        }
        m2960c();
    }

    private boolean m2951b(Drawable drawable) {
        if (this.f1619e == null) {
            this.f1619e = new ap();
        }
        ap apVar = this.f1619e;
        apVar.m2809a();
        ColorStateList q = af.m1205q(this.f1615a);
        if (q != null) {
            apVar.f1528d = true;
            apVar.f1525a = q;
        }
        Mode r = af.m1206r(this.f1615a);
        if (r != null) {
            apVar.f1527c = true;
            apVar.f1526b = r;
        }
        if (!apVar.f1528d && !apVar.f1527c) {
            return false;
        }
        AppCompatDrawableManager.m2984a(drawable, apVar, this.f1615a.getDrawableState());
        return true;
    }
}
