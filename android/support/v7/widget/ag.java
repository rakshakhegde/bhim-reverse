package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.p006f.MotionEventCompat;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.support.v4.p009d.TextUtilsCompat;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v4.widget.PopupWindowCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import java.lang.reflect.Method;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* compiled from: ListPopupWindow */
public class ag {
    private static Method f1430a;
    private static Method f1431c;
    private final ListPopupWindow f1432A;
    private Runnable f1433B;
    private final Handler f1434C;
    private Rect f1435D;
    private boolean f1436E;
    private int f1437F;
    int f1438b;
    private Context f1439d;
    private PopupWindow f1440e;
    private ListAdapter f1441f;
    private ListPopupWindow f1442g;
    private int f1443h;
    private int f1444i;
    private int f1445j;
    private int f1446k;
    private int f1447l;
    private boolean f1448m;
    private int f1449n;
    private boolean f1450o;
    private boolean f1451p;
    private View f1452q;
    private int f1453r;
    private DataSetObserver f1454s;
    private View f1455t;
    private Drawable f1456u;
    private OnItemClickListener f1457v;
    private OnItemSelectedListener f1458w;
    private final ListPopupWindow f1459x;
    private final ListPopupWindow f1460y;
    private final ListPopupWindow f1461z;

    /* renamed from: android.support.v7.widget.ag.b */
    public static abstract class ListPopupWindow implements OnTouchListener {
        private final float f1002a;
        private final int f1003b;
        private final int f1004c;
        private final View f1005d;
        private Runnable f1006e;
        private Runnable f1007f;
        private boolean f1008g;
        private boolean f1009h;
        private int f1010i;
        private final int[] f1011j;

        /* renamed from: android.support.v7.widget.ag.b.a */
        private class ListPopupWindow implements Runnable {
            final /* synthetic */ ListPopupWindow f1423a;

            private ListPopupWindow(ListPopupWindow listPopupWindow) {
                this.f1423a = listPopupWindow;
            }

            public void run() {
                this.f1423a.f1005d.getParent().requestDisallowInterceptTouchEvent(true);
            }
        }

        /* renamed from: android.support.v7.widget.ag.b.b */
        private class ListPopupWindow implements Runnable {
            final /* synthetic */ ListPopupWindow f1424a;

            private ListPopupWindow(ListPopupWindow listPopupWindow) {
                this.f1424a = listPopupWindow;
            }

            public void run() {
                this.f1424a.m2240e();
            }
        }

        public abstract ag m2241a();

        public ListPopupWindow(View view) {
            this.f1011j = new int[2];
            this.f1005d = view;
            this.f1002a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
            this.f1003b = ViewConfiguration.getTapTimeout();
            this.f1004c = (this.f1003b + ViewConfiguration.getLongPressTimeout()) / 2;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean b;
            boolean z = this.f1008g;
            if (z) {
                b = this.f1009h ? m2237b(motionEvent) : m2237b(motionEvent) || !m2243c();
            } else {
                boolean z2 = m2233a(motionEvent) && m2242b();
                if (z2) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    this.f1005d.onTouchEvent(obtain);
                    obtain.recycle();
                }
                b = z2;
            }
            this.f1008g = b;
            if (b || z) {
                return true;
            }
            return false;
        }

        protected boolean m2242b() {
            ag a = m2241a();
            if (!(a == null || a.m2755k())) {
                a.m2741c();
            }
            return true;
        }

        protected boolean m2243c() {
            ag a = m2241a();
            if (a != null && a.m2755k()) {
                a.m2753i();
            }
            return true;
        }

        private boolean m2233a(MotionEvent motionEvent) {
            View view = this.f1005d;
            if (!view.isEnabled()) {
                return false;
            }
            switch (MotionEventCompat.m1507a(motionEvent)) {
                case R.View_android_theme /*0*/:
                    this.f1010i = motionEvent.getPointerId(0);
                    this.f1009h = false;
                    if (this.f1006e == null) {
                        this.f1006e = new ListPopupWindow();
                    }
                    view.postDelayed(this.f1006e, (long) this.f1003b);
                    if (this.f1007f == null) {
                        this.f1007f = new ListPopupWindow();
                    }
                    view.postDelayed(this.f1007f, (long) this.f1004c);
                    return false;
                case R.View_android_focusable /*1*/:
                case R.View_paddingEnd /*3*/:
                    m2239d();
                    return false;
                case R.View_paddingStart /*2*/:
                    int findPointerIndex = motionEvent.findPointerIndex(this.f1010i);
                    if (findPointerIndex < 0 || ListPopupWindow.m2234a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.f1002a)) {
                        return false;
                    }
                    m2239d();
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                default:
                    return false;
            }
        }

        private void m2239d() {
            if (this.f1007f != null) {
                this.f1005d.removeCallbacks(this.f1007f);
            }
            if (this.f1006e != null) {
                this.f1005d.removeCallbacks(this.f1006e);
            }
        }

        private void m2240e() {
            m2239d();
            View view = this.f1005d;
            if (view.isEnabled() && !view.isLongClickable() && m2242b()) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                view.onTouchEvent(obtain);
                obtain.recycle();
                this.f1008g = true;
                this.f1009h = true;
            }
        }

        private boolean m2237b(MotionEvent motionEvent) {
            boolean z = true;
            View view = this.f1005d;
            ag a = m2241a();
            if (a == null || !a.m2755k()) {
                return false;
            }
            View a2 = a.f1442g;
            if (a2 == null || !a2.isShown()) {
                return false;
            }
            MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
            m2238b(view, obtainNoHistory);
            m2235a(a2, obtainNoHistory);
            boolean a3 = a2.m2724a(obtainNoHistory, this.f1010i);
            obtainNoHistory.recycle();
            int a4 = MotionEventCompat.m1507a(motionEvent);
            boolean z2;
            if (a4 == 1 || a4 == 3) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (!(a3 && r2)) {
                z = false;
            }
            return z;
        }

        private static boolean m2234a(View view, float f, float f2, float f3) {
            return f >= (-f3) && f2 >= (-f3) && f < ((float) (view.getRight() - view.getLeft())) + f3 && f2 < ((float) (view.getBottom() - view.getTop())) + f3;
        }

        private boolean m2235a(View view, MotionEvent motionEvent) {
            int[] iArr = this.f1011j;
            view.getLocationOnScreen(iArr);
            motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
            return true;
        }

        private boolean m2238b(View view, MotionEvent motionEvent) {
            int[] iArr = this.f1011j;
            view.getLocationOnScreen(iArr);
            motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
            return true;
        }
    }

    /* renamed from: android.support.v7.widget.ag.1 */
    class ListPopupWindow extends ListPopupWindow {
        final /* synthetic */ ag f1406a;

        public ag m2710a() {
            return this.f1406a;
        }
    }

    /* renamed from: android.support.v7.widget.ag.2 */
    class ListPopupWindow implements Runnable {
        final /* synthetic */ ag f1407a;

        ListPopupWindow(ag agVar) {
            this.f1407a = agVar;
        }

        public void run() {
            View e = this.f1407a.m2745e();
            if (e != null && e.getWindowToken() != null) {
                this.f1407a.m2741c();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ag.3 */
    class ListPopupWindow implements OnItemSelectedListener {
        final /* synthetic */ ag f1408a;

        ListPopupWindow(ag agVar) {
            this.f1408a = agVar;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            if (i != -1) {
                ListPopupWindow a = this.f1408a.f1442g;
                if (a != null) {
                    a.f1418g = false;
                }
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: android.support.v7.widget.ag.a */
    private static class ListPopupWindow extends ah {
        private boolean f1418g;
        private boolean f1419h;
        private boolean f1420i;
        private au f1421j;
        private ListViewAutoScrollHelper f1422k;

        public ListPopupWindow(Context context, boolean z) {
            super(context, null, android.support.v7.p014b.R.R.dropDownListViewStyle);
            this.f1419h = z;
            setCacheColorHint(0);
        }

        public boolean m2724a(MotionEvent motionEvent, int i) {
            boolean z;
            boolean z2;
            int a = MotionEventCompat.m1507a(motionEvent);
            switch (a) {
                case R.View_android_focusable /*1*/:
                    z = false;
                    break;
                case R.View_paddingStart /*2*/:
                    z = true;
                    break;
                case R.View_paddingEnd /*3*/:
                    z = false;
                    z2 = false;
                    break;
                default:
                    z = false;
                    z2 = true;
                    break;
            }
            int findPointerIndex = motionEvent.findPointerIndex(i);
            if (findPointerIndex < 0) {
                z = false;
                z2 = false;
            } else {
                int x = (int) motionEvent.getX(findPointerIndex);
                findPointerIndex = (int) motionEvent.getY(findPointerIndex);
                int pointToPosition = pointToPosition(x, findPointerIndex);
                if (pointToPosition == -1) {
                    z2 = z;
                    z = true;
                } else {
                    View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                    m2720a(childAt, pointToPosition, (float) x, (float) findPointerIndex);
                    if (a == 1) {
                        m2719a(childAt, pointToPosition);
                    }
                    z = false;
                    z2 = true;
                }
            }
            if (!z2 || r0) {
                m2722d();
            }
            if (z2) {
                if (this.f1422k == null) {
                    this.f1422k = new ListViewAutoScrollHelper(this);
                }
                this.f1422k.m1607a(true);
                this.f1422k.onTouch(this, motionEvent);
            } else if (this.f1422k != null) {
                this.f1422k.m1607a(false);
            }
            return z2;
        }

        private void m2719a(View view, int i) {
            performItemClick(view, i, getItemIdAtPosition(i));
        }

        private void m2722d() {
            this.f1420i = false;
            setPressed(false);
            drawableStateChanged();
            View childAt = getChildAt(this.f - getFirstVisiblePosition());
            if (childAt != null) {
                childAt.setPressed(false);
            }
            if (this.f1421j != null) {
                this.f1421j.m1360b();
                this.f1421j = null;
            }
        }

        private void m2720a(View view, int i, float f, float f2) {
            this.f1420i = true;
            if (VERSION.SDK_INT >= 21) {
                drawableHotspotChanged(f, f2);
            }
            if (!isPressed()) {
                setPressed(true);
            }
            layoutChildren();
            if (this.f != -1) {
                View childAt = getChildAt(this.f - getFirstVisiblePosition());
                if (!(childAt == null || childAt == view || !childAt.isPressed())) {
                    childAt.setPressed(false);
                }
            }
            this.f = i;
            float left = f - ((float) view.getLeft());
            float top = f2 - ((float) view.getTop());
            if (VERSION.SDK_INT >= 21) {
                view.drawableHotspotChanged(left, top);
            }
            if (!view.isPressed()) {
                view.setPressed(true);
            }
            m2713a(i, view, f, f2);
            setSelectorEnabled(false);
            refreshDrawableState();
        }

        protected boolean m2723a() {
            return this.f1420i || super.m2715a();
        }

        public boolean isInTouchMode() {
            return (this.f1419h && this.f1418g) || super.isInTouchMode();
        }

        public boolean hasWindowFocus() {
            return this.f1419h || super.hasWindowFocus();
        }

        public boolean isFocused() {
            return this.f1419h || super.isFocused();
        }

        public boolean hasFocus() {
            return this.f1419h || super.hasFocus();
        }
    }

    /* renamed from: android.support.v7.widget.ag.c */
    private class ListPopupWindow implements Runnable {
        final /* synthetic */ ag f1425a;

        private ListPopupWindow(ag agVar) {
            this.f1425a = agVar;
        }

        public void run() {
            this.f1425a.m2754j();
        }
    }

    /* renamed from: android.support.v7.widget.ag.d */
    private class ListPopupWindow extends DataSetObserver {
        final /* synthetic */ ag f1426a;

        private ListPopupWindow(ag agVar) {
            this.f1426a = agVar;
        }

        public void onChanged() {
            if (this.f1426a.m2755k()) {
                this.f1426a.m2741c();
            }
        }

        public void onInvalidated() {
            this.f1426a.m2753i();
        }
    }

    /* renamed from: android.support.v7.widget.ag.e */
    private class ListPopupWindow implements OnScrollListener {
        final /* synthetic */ ag f1427a;

        private ListPopupWindow(ag agVar) {
            this.f1427a = agVar;
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 1 && !this.f1427a.m2756l() && this.f1427a.f1440e.getContentView() != null) {
                this.f1427a.f1434C.removeCallbacks(this.f1427a.f1459x);
                this.f1427a.f1459x.run();
            }
        }
    }

    /* renamed from: android.support.v7.widget.ag.f */
    private class ListPopupWindow implements OnTouchListener {
        final /* synthetic */ ag f1428a;

        private ListPopupWindow(ag agVar) {
            this.f1428a = agVar;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && this.f1428a.f1440e != null && this.f1428a.f1440e.isShowing() && x >= 0 && x < this.f1428a.f1440e.getWidth() && y >= 0 && y < this.f1428a.f1440e.getHeight()) {
                this.f1428a.f1434C.postDelayed(this.f1428a.f1459x, 250);
            } else if (action == 1) {
                this.f1428a.f1434C.removeCallbacks(this.f1428a.f1459x);
            }
            return false;
        }
    }

    /* renamed from: android.support.v7.widget.ag.g */
    private class ListPopupWindow implements Runnable {
        final /* synthetic */ ag f1429a;

        private ListPopupWindow(ag agVar) {
            this.f1429a = agVar;
        }

        public void run() {
            if (this.f1429a.f1442g != null && af.m1209u(this.f1429a.f1442g) && this.f1429a.f1442g.getCount() > this.f1429a.f1442g.getChildCount() && this.f1429a.f1442g.getChildCount() <= this.f1429a.f1438b) {
                this.f1429a.f1440e.setInputMethodMode(2);
                this.f1429a.m2741c();
            }
        }
    }

    static {
        try {
            f1430a = PopupWindow.class.getDeclaredMethod("setClipToScreenEnabled", new Class[]{Boolean.TYPE});
        } catch (NoSuchMethodException e) {
            Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
        try {
            f1431c = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", new Class[]{View.class, Integer.TYPE, Boolean.TYPE});
        } catch (NoSuchMethodException e2) {
            Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
        }
    }

    public ag(Context context) {
        this(context, null, android.support.v7.p014b.R.R.listPopupWindowStyle);
    }

    public ag(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public ag(Context context, AttributeSet attributeSet, int i, int i2) {
        this.f1443h = -2;
        this.f1444i = -2;
        this.f1447l = 1002;
        this.f1449n = 0;
        this.f1450o = false;
        this.f1451p = false;
        this.f1438b = Integer.MAX_VALUE;
        this.f1453r = 0;
        this.f1459x = new ListPopupWindow();
        this.f1460y = new ListPopupWindow();
        this.f1461z = new ListPopupWindow();
        this.f1432A = new ListPopupWindow();
        this.f1435D = new Rect();
        this.f1439d = context;
        this.f1434C = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, android.support.v7.p014b.R.R.ListPopupWindow, i, i2);
        this.f1445j = obtainStyledAttributes.getDimensionPixelOffset(android.support.v7.p014b.R.R.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.f1446k = obtainStyledAttributes.getDimensionPixelOffset(android.support.v7.p014b.R.R.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.f1446k != 0) {
            this.f1448m = true;
        }
        obtainStyledAttributes.recycle();
        this.f1440e = new AppCompatPopupWindow(context, attributeSet, i);
        this.f1440e.setInputMethodMode(1);
        this.f1437F = TextUtilsCompat.m729a(this.f1439d.getResources().getConfiguration().locale);
    }

    public void m2737a(ListAdapter listAdapter) {
        if (this.f1454s == null) {
            this.f1454s = new ListPopupWindow();
        } else if (this.f1441f != null) {
            this.f1441f.unregisterDataSetObserver(this.f1454s);
        }
        this.f1441f = listAdapter;
        if (this.f1441f != null) {
            listAdapter.registerDataSetObserver(this.f1454s);
        }
        if (this.f1442g != null) {
            this.f1442g.setAdapter(this.f1441f);
        }
    }

    public void m2733a(int i) {
        this.f1453r = i;
    }

    public void m2739a(boolean z) {
        this.f1436E = z;
        this.f1440e.setFocusable(z);
    }

    public Drawable m2743d() {
        return this.f1440e.getBackground();
    }

    public void m2734a(Drawable drawable) {
        this.f1440e.setBackgroundDrawable(drawable);
    }

    public View m2745e() {
        return this.f1455t;
    }

    public void m2735a(View view) {
        this.f1455t = view;
    }

    public int m2747f() {
        return this.f1445j;
    }

    public void m2740b(int i) {
        this.f1445j = i;
    }

    public int m2749g() {
        if (this.f1448m) {
            return this.f1446k;
        }
        return 0;
    }

    public void m2742c(int i) {
        this.f1446k = i;
        this.f1448m = true;
    }

    public void m2744d(int i) {
        this.f1449n = i;
    }

    public int m2751h() {
        return this.f1444i;
    }

    public void m2746e(int i) {
        this.f1444i = i;
    }

    public void m2748f(int i) {
        Drawable background = this.f1440e.getBackground();
        if (background != null) {
            background.getPadding(this.f1435D);
            this.f1444i = (this.f1435D.left + this.f1435D.right) + i;
            return;
        }
        m2746e(i);
    }

    public void m2736a(OnItemClickListener onItemClickListener) {
        this.f1457v = onItemClickListener;
    }

    public void m2741c() {
        boolean z = true;
        boolean z2 = false;
        int i = -1;
        int b = m2728b();
        boolean l = m2756l();
        PopupWindowCompat.m1703a(this.f1440e, this.f1447l);
        int i2;
        if (this.f1440e.isShowing()) {
            int i3;
            int i4;
            if (this.f1444i == -1) {
                i3 = -1;
            } else if (this.f1444i == -2) {
                i3 = m2745e().getWidth();
            } else {
                i3 = this.f1444i;
            }
            if (this.f1443h == -1) {
                if (!l) {
                    b = -1;
                }
                PopupWindow popupWindow;
                if (l) {
                    popupWindow = this.f1440e;
                    if (this.f1444i == -1) {
                        i2 = -1;
                    } else {
                        i2 = 0;
                    }
                    popupWindow.setWidth(i2);
                    this.f1440e.setHeight(0);
                    i4 = b;
                } else {
                    popupWindow = this.f1440e;
                    if (this.f1444i == -1) {
                        i2 = -1;
                    } else {
                        i2 = 0;
                    }
                    popupWindow.setWidth(i2);
                    this.f1440e.setHeight(-1);
                    i4 = b;
                }
            } else if (this.f1443h == -2) {
                i4 = b;
            } else {
                i4 = this.f1443h;
            }
            PopupWindow popupWindow2 = this.f1440e;
            if (!(this.f1451p || this.f1450o)) {
                z2 = true;
            }
            popupWindow2.setOutsideTouchable(z2);
            popupWindow2 = this.f1440e;
            View e = m2745e();
            b = this.f1445j;
            int i5 = this.f1446k;
            if (i3 < 0) {
                i3 = -1;
            }
            if (i4 >= 0) {
                i = i4;
            }
            popupWindow2.update(e, b, i5, i3, i);
            return;
        }
        if (this.f1444i == -1) {
            i2 = -1;
        } else if (this.f1444i == -2) {
            i2 = m2745e().getWidth();
        } else {
            i2 = this.f1444i;
        }
        if (this.f1443h == -1) {
            b = -1;
        } else if (this.f1443h != -2) {
            b = this.f1443h;
        }
        this.f1440e.setWidth(i2);
        this.f1440e.setHeight(b);
        m2730b(true);
        popupWindow2 = this.f1440e;
        if (this.f1451p || this.f1450o) {
            z = false;
        }
        popupWindow2.setOutsideTouchable(z);
        this.f1440e.setTouchInterceptor(this.f1460y);
        PopupWindowCompat.m1704a(this.f1440e, m2745e(), this.f1445j, this.f1446k, this.f1449n);
        this.f1442g.setSelection(-1);
        if (!this.f1436E || this.f1442g.isInTouchMode()) {
            m2754j();
        }
        if (!this.f1436E) {
            this.f1434C.post(this.f1432A);
        }
    }

    public void m2753i() {
        this.f1440e.dismiss();
        m2727a();
        this.f1440e.setContentView(null);
        this.f1442g = null;
        this.f1434C.removeCallbacks(this.f1459x);
    }

    public void m2738a(OnDismissListener onDismissListener) {
        this.f1440e.setOnDismissListener(onDismissListener);
    }

    private void m2727a() {
        if (this.f1452q != null) {
            ViewParent parent = this.f1452q.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.f1452q);
            }
        }
    }

    public void m2750g(int i) {
        this.f1440e.setInputMethodMode(i);
    }

    public void m2752h(int i) {
        ListPopupWindow listPopupWindow = this.f1442g;
        if (m2755k() && listPopupWindow != null) {
            listPopupWindow.f1418g = false;
            listPopupWindow.setSelection(i);
            if (VERSION.SDK_INT >= 11 && listPopupWindow.getChoiceMode() != 0) {
                listPopupWindow.setItemChecked(i, true);
            }
        }
    }

    public void m2754j() {
        ListPopupWindow listPopupWindow = this.f1442g;
        if (listPopupWindow != null) {
            listPopupWindow.f1418g = true;
            listPopupWindow.requestLayout();
        }
    }

    public boolean m2755k() {
        return this.f1440e.isShowing();
    }

    public boolean m2756l() {
        return this.f1440e.getInputMethodMode() == 2;
    }

    public ListView m2757m() {
        return this.f1442g;
    }

    private int m2728b() {
        int i;
        int i2;
        int i3;
        int i4;
        boolean z = true;
        LayoutParams layoutParams;
        View view;
        if (this.f1442g == null) {
            Context context = this.f1439d;
            this.f1433B = new ListPopupWindow(this);
            this.f1442g = new ListPopupWindow(context, !this.f1436E);
            if (this.f1456u != null) {
                this.f1442g.setSelector(this.f1456u);
            }
            this.f1442g.setAdapter(this.f1441f);
            this.f1442g.setOnItemClickListener(this.f1457v);
            this.f1442g.setFocusable(true);
            this.f1442g.setFocusableInTouchMode(true);
            this.f1442g.setOnItemSelectedListener(new ListPopupWindow(this));
            this.f1442g.setOnScrollListener(this.f1461z);
            if (this.f1458w != null) {
                this.f1442g.setOnItemSelectedListener(this.f1458w);
            }
            View view2 = this.f1442g;
            View view3 = this.f1452q;
            if (view3 != null) {
                View linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                ViewGroup.LayoutParams layoutParams2 = new LayoutParams(-1, 0, 1.0f);
                switch (this.f1453r) {
                    case R.View_android_theme /*0*/:
                        linearLayout.addView(view3);
                        linearLayout.addView(view2, layoutParams2);
                        break;
                    case R.View_android_focusable /*1*/:
                        linearLayout.addView(view2, layoutParams2);
                        linearLayout.addView(view3);
                        break;
                    default:
                        Log.e("ListPopupWindow", "Invalid hint position " + this.f1453r);
                        break;
                }
                if (this.f1444i >= 0) {
                    i = this.f1444i;
                    i2 = Integer.MIN_VALUE;
                } else {
                    i2 = 0;
                    i = 0;
                }
                view3.measure(MeasureSpec.makeMeasureSpec(i, i2), 0);
                layoutParams = (LayoutParams) view3.getLayoutParams();
                i2 = layoutParams.bottomMargin + (view3.getMeasuredHeight() + layoutParams.topMargin);
                view = linearLayout;
            } else {
                view = view2;
                i2 = 0;
            }
            this.f1440e.setContentView(view);
            i3 = i2;
        } else {
            ViewGroup viewGroup = (ViewGroup) this.f1440e.getContentView();
            view = this.f1452q;
            if (view != null) {
                layoutParams = (LayoutParams) view.getLayoutParams();
                i3 = layoutParams.bottomMargin + (view.getMeasuredHeight() + layoutParams.topMargin);
            } else {
                i3 = 0;
            }
        }
        Drawable background = this.f1440e.getBackground();
        if (background != null) {
            background.getPadding(this.f1435D);
            i2 = this.f1435D.top + this.f1435D.bottom;
            if (this.f1448m) {
                i4 = i2;
            } else {
                this.f1446k = -this.f1435D.top;
                i4 = i2;
            }
        } else {
            this.f1435D.setEmpty();
            i4 = 0;
        }
        if (this.f1440e.getInputMethodMode() != 2) {
            z = false;
        }
        i = m2725a(m2745e(), this.f1446k, z);
        if (this.f1450o || this.f1443h == -1) {
            return i + i4;
        }
        int makeMeasureSpec;
        switch (this.f1444i) {
            case -2:
                makeMeasureSpec = MeasureSpec.makeMeasureSpec(this.f1439d.getResources().getDisplayMetrics().widthPixels - (this.f1435D.left + this.f1435D.right), Integer.MIN_VALUE);
                break;
            case -1:
                makeMeasureSpec = MeasureSpec.makeMeasureSpec(this.f1439d.getResources().getDisplayMetrics().widthPixels - (this.f1435D.left + this.f1435D.right), 1073741824);
                break;
            default:
                makeMeasureSpec = MeasureSpec.makeMeasureSpec(this.f1444i, 1073741824);
                break;
        }
        i2 = this.f1442g.m2711a(makeMeasureSpec, 0, -1, i - i3, -1);
        if (i2 > 0) {
            i3 += i4;
        }
        return i2 + i3;
    }

    private void m2730b(boolean z) {
        if (f1430a != null) {
            try {
                f1430a.invoke(this.f1440e, new Object[]{Boolean.valueOf(z)});
            } catch (Exception e) {
                Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
    }

    private int m2725a(View view, int i, boolean z) {
        if (f1431c != null) {
            try {
                return ((Integer) f1431c.invoke(this.f1440e, new Object[]{view, Integer.valueOf(i), Boolean.valueOf(z)})).intValue();
            } catch (Exception e) {
                Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.f1440e.getMaxAvailableHeight(view, i);
    }
}
