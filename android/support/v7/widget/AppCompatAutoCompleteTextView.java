package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ac;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

/* renamed from: android.support.v7.widget.f */
public class AppCompatAutoCompleteTextView extends AutoCompleteTextView implements ac {
    private static final int[] f1298a;
    private AppCompatDrawableManager f1299b;
    private AppCompatBackgroundHelper f1300c;
    private AppCompatTextHelper f1301d;

    static {
        f1298a = new int[]{16843126};
    }

    public AppCompatAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public AppCompatAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.autoCompleteTextViewStyle);
    }

    public AppCompatAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        this.f1299b = AppCompatDrawableManager.m2981a();
        ar a = ar.m2811a(getContext(), attributeSet, f1298a, i, 0);
        if (a.m2826f(0)) {
            setDropDownBackgroundDrawable(a.m2814a(0));
        }
        a.m2815a();
        this.f1300c = new AppCompatBackgroundHelper(this, this.f1299b);
        this.f1300c.m2957a(attributeSet, i);
        this.f1301d = AppCompatTextHelper.m3036a((TextView) this);
        this.f1301d.m3040a(attributeSet, i);
        this.f1301d.m3037a();
    }

    public void setDropDownBackgroundResource(int i) {
        if (this.f1299b != null) {
            setDropDownBackgroundDrawable(this.f1299b.m3004a(getContext(), i));
        } else {
            super.setDropDownBackgroundResource(i);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1300c != null) {
            this.f1300c.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1300c != null) {
            this.f1300c.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1300c != null) {
            this.f1300c.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1300c != null ? this.f1300c.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1300c != null) {
            this.f1300c.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1300c != null ? this.f1300c.m2958b() : null;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1300c != null) {
            this.f1300c.m2960c();
        }
        if (this.f1301d != null) {
            this.f1301d.m3037a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1301d != null) {
            this.f1301d.m3038a(context, i);
        }
    }
}
