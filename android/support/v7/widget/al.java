package android.support.v7.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v7.p014b.R.R;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.crashlytics.android.core.BuildConfig;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: SuggestionsAdapter */
class al extends ResourceCursorAdapter implements OnClickListener {
    private final SearchManager f1497j;
    private final SearchView f1498k;
    private final SearchableInfo f1499l;
    private final Context f1500m;
    private final WeakHashMap<String, ConstantState> f1501n;
    private final int f1502o;
    private boolean f1503p;
    private int f1504q;
    private ColorStateList f1505r;
    private int f1506s;
    private int f1507t;
    private int f1508u;
    private int f1509v;
    private int f1510w;
    private int f1511x;

    /* renamed from: android.support.v7.widget.al.a */
    private static final class SuggestionsAdapter {
        public final TextView f1492a;
        public final TextView f1493b;
        public final ImageView f1494c;
        public final ImageView f1495d;
        public final ImageView f1496e;

        public SuggestionsAdapter(View view) {
            this.f1492a = (TextView) view.findViewById(16908308);
            this.f1493b = (TextView) view.findViewById(16908309);
            this.f1494c = (ImageView) view.findViewById(16908295);
            this.f1495d = (ImageView) view.findViewById(16908296);
            this.f1496e = (ImageView) view.findViewById(R.edit_query);
        }
    }

    public al(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap<String, ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), null, true);
        this.f1503p = false;
        this.f1504q = 1;
        this.f1506s = -1;
        this.f1507t = -1;
        this.f1508u = -1;
        this.f1509v = -1;
        this.f1510w = -1;
        this.f1511x = -1;
        this.f1497j = (SearchManager) this.d.getSystemService("search");
        this.f1498k = searchView;
        this.f1499l = searchableInfo;
        this.f1502o = searchView.getSuggestionCommitIconResId();
        this.f1500m = context;
        this.f1501n = weakHashMap;
    }

    public void m2796a(int i) {
        this.f1504q = i;
    }

    public boolean hasStableIds() {
        return false;
    }

    public Cursor m2793a(CharSequence charSequence) {
        String charSequence2 = charSequence == null ? BuildConfig.FLAVOR : charSequence.toString();
        if (this.f1498k.getVisibility() != 0 || this.f1498k.getWindowVisibility() != 0) {
            return null;
        }
        try {
            Cursor a = m2792a(this.f1499l, charSequence2, 50);
            if (a != null) {
                a.getCount();
                return a;
            }
        } catch (Throwable e) {
            Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
        }
        return null;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        m2788d(m1640a());
    }

    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        m2788d(m1640a());
    }

    private void m2788d(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras != null && !extras.getBoolean("in_progress")) {
        }
    }

    public void m2797a(Cursor cursor) {
        if (this.f1503p) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.m1644a(cursor);
            if (cursor != null) {
                this.f1506s = cursor.getColumnIndex("suggest_text_1");
                this.f1507t = cursor.getColumnIndex("suggest_text_2");
                this.f1508u = cursor.getColumnIndex("suggest_text_2_url");
                this.f1509v = cursor.getColumnIndex("suggest_icon_1");
                this.f1510w = cursor.getColumnIndex("suggest_icon_2");
                this.f1511x = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Throwable e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    public View m2795a(Context context, Cursor cursor, ViewGroup viewGroup) {
        View a = super.m1711a(context, cursor, viewGroup);
        a.setTag(new SuggestionsAdapter(a));
        ((ImageView) a.findViewById(R.edit_query)).setImageResource(this.f1502o);
        return a;
    }

    public void m2798a(View view, Context context, Cursor cursor) {
        SuggestionsAdapter suggestionsAdapter = (SuggestionsAdapter) view.getTag();
        int i;
        if (this.f1511x != -1) {
            i = cursor.getInt(this.f1511x);
        } else {
            i = 0;
        }
        if (suggestionsAdapter.f1492a != null) {
            m2782a(suggestionsAdapter.f1492a, m2779a(cursor, this.f1506s));
        }
        if (suggestionsAdapter.f1493b != null) {
            CharSequence a = m2779a(cursor, this.f1508u);
            if (a != null) {
                a = m2787b(a);
            } else {
                a = m2779a(cursor, this.f1507t);
            }
            if (TextUtils.isEmpty(a)) {
                if (suggestionsAdapter.f1492a != null) {
                    suggestionsAdapter.f1492a.setSingleLine(false);
                    suggestionsAdapter.f1492a.setMaxLines(2);
                }
            } else if (suggestionsAdapter.f1492a != null) {
                suggestionsAdapter.f1492a.setSingleLine(true);
                suggestionsAdapter.f1492a.setMaxLines(1);
            }
            m2782a(suggestionsAdapter.f1493b, a);
        }
        if (suggestionsAdapter.f1494c != null) {
            m2781a(suggestionsAdapter.f1494c, m2789e(cursor), 4);
        }
        if (suggestionsAdapter.f1495d != null) {
            m2781a(suggestionsAdapter.f1495d, m2790f(cursor), 8);
        }
        if (this.f1504q == 2 || (this.f1504q == 1 && (r1 & 1) != 0)) {
            suggestionsAdapter.f1496e.setVisibility(0);
            suggestionsAdapter.f1496e.setTag(suggestionsAdapter.f1492a.getText());
            suggestionsAdapter.f1496e.setOnClickListener(this);
            return;
        }
        suggestionsAdapter.f1496e.setVisibility(8);
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.f1498k.m2610a((CharSequence) tag);
        }
    }

    private CharSequence m2787b(CharSequence charSequence) {
        if (this.f1505r == null) {
            TypedValue typedValue = new TypedValue();
            this.d.getTheme().resolveAttribute(R.textColorSearchUrl, typedValue, true);
            this.f1505r = this.d.getResources().getColorStateList(typedValue.resourceId);
        }
        CharSequence spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(null, 0, 0, this.f1505r, null), 0, charSequence.length(), 33);
        return spannableString;
    }

    private void m2782a(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    private Drawable m2789e(Cursor cursor) {
        if (this.f1509v == -1) {
            return null;
        }
        Drawable a = m2778a(cursor.getString(this.f1509v));
        return a == null ? m2791g(cursor) : a;
    }

    private Drawable m2790f(Cursor cursor) {
        if (this.f1510w == -1) {
            return null;
        }
        return m2778a(cursor.getString(this.f1510w));
    }

    private void m2781a(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    public CharSequence m2799c(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        String a = m2780a(cursor, "suggest_intent_query");
        if (a != null) {
            return a;
        }
        if (this.f1499l.shouldRewriteQueryFromData()) {
            a = m2780a(cursor, "suggest_intent_data");
            if (a != null) {
                return a;
            }
        }
        if (!this.f1499l.shouldRewriteQueryFromText()) {
            return null;
        }
        a = m2780a(cursor, "suggest_text_1");
        if (a != null) {
            return a;
        }
        return null;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (Throwable e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View a = m2795a(this.d, this.c, viewGroup);
            if (a != null) {
                ((SuggestionsAdapter) a.getTag()).f1492a.setText(e.toString());
            }
            return a;
        }
    }

    private Drawable m2778a(String str) {
        if (str == null || str.length() == 0 || "0".equals(str)) {
            return null;
        }
        Drawable b;
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.f1500m.getPackageName() + "/" + parseInt;
            b = m2786b(str2);
            if (b != null) {
                return b;
            }
            b = ContextCompat.m77a(this.f1500m, parseInt);
            m2783a(str2, b);
            return b;
        } catch (NumberFormatException e) {
            b = m2786b(str);
            if (b != null) {
                return b;
            }
            b = m2785b(Uri.parse(str));
            m2783a(str, b);
            return b;
        } catch (NotFoundException e2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    private Drawable m2785b(Uri uri) {
        InputStream openInputStream;
        try {
            if ("android.resource".equals(uri.getScheme())) {
                return m2794a(uri);
            }
            openInputStream = this.f1500m.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                throw new FileNotFoundException("Failed to open " + uri);
            }
            Drawable createFromStream = Drawable.createFromStream(openInputStream, null);
            try {
                openInputStream.close();
                return createFromStream;
            } catch (Throwable e) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e);
                return createFromStream;
            }
        } catch (NotFoundException e2) {
            throw new FileNotFoundException("Resource does not exist: " + uri);
        } catch (FileNotFoundException e3) {
            Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e3.getMessage());
            return null;
        } catch (Throwable th) {
            try {
                openInputStream.close();
            } catch (Throwable e4) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e4);
            }
        }
    }

    private Drawable m2786b(String str) {
        ConstantState constantState = (ConstantState) this.f1501n.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    private void m2783a(String str, Drawable drawable) {
        if (drawable != null) {
            this.f1501n.put(str, drawable.getConstantState());
        }
    }

    private Drawable m2791g(Cursor cursor) {
        Drawable a = m2777a(this.f1499l.getSearchActivity());
        return a != null ? a : this.d.getPackageManager().getDefaultActivityIcon();
    }

    private Drawable m2777a(ComponentName componentName) {
        Object obj = null;
        String flattenToShortString = componentName.flattenToShortString();
        if (this.f1501n.containsKey(flattenToShortString)) {
            ConstantState constantState = (ConstantState) this.f1501n.get(flattenToShortString);
            return constantState == null ? null : constantState.newDrawable(this.f1500m.getResources());
        } else {
            Drawable b = m2784b(componentName);
            if (b != null) {
                obj = b.getConstantState();
            }
            this.f1501n.put(flattenToShortString, obj);
            return b;
        }
    }

    private Drawable m2784b(ComponentName componentName) {
        PackageManager packageManager = this.d.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    public static String m2780a(Cursor cursor, String str) {
        return m2779a(cursor, cursor.getColumnIndex(str));
    }

    private static String m2779a(Cursor cursor, int i) {
        String str = null;
        if (i != -1) {
            try {
                str = cursor.getString(i);
            } catch (Throwable e) {
                Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            }
        }
        return str;
    }

    Drawable m2794a(Uri uri) {
        String authority = uri.getAuthority();
        if (TextUtils.isEmpty(authority)) {
            throw new FileNotFoundException("No authority: " + uri);
        }
        try {
            Resources resourcesForApplication = this.d.getPackageManager().getResourcesForApplication(authority);
            List pathSegments = uri.getPathSegments();
            if (pathSegments == null) {
                throw new FileNotFoundException("No path: " + uri);
            }
            int size = pathSegments.size();
            if (size == 1) {
                try {
                    size = Integer.parseInt((String) pathSegments.get(0));
                } catch (NumberFormatException e) {
                    throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                }
            } else if (size == 2) {
                size = resourcesForApplication.getIdentifier((String) pathSegments.get(1), (String) pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + uri);
            }
            if (size != 0) {
                return resourcesForApplication.getDrawable(size);
            }
            throw new FileNotFoundException("No resource found for: " + uri);
        } catch (NameNotFoundException e2) {
            throw new FileNotFoundException("No package found for authority: " + uri);
        }
    }

    Cursor m2792a(SearchableInfo searchableInfo, String str, int i) {
        if (searchableInfo == null) {
            return null;
        }
        String suggestAuthority = searchableInfo.getSuggestAuthority();
        if (suggestAuthority == null) {
            return null;
        }
        String[] strArr;
        Builder fragment = new Builder().scheme("content").authority(suggestAuthority).query(BuildConfig.FLAVOR).fragment(BuildConfig.FLAVOR);
        String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo.getSuggestSelection();
        if (suggestSelection != null) {
            strArr = new String[]{str};
        } else {
            fragment.appendPath(str);
            strArr = null;
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.d.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
    }
}
