package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.p006f.MotionEventCompat;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.support.v4.p006f.ay;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

/* renamed from: android.support.v7.widget.a */
abstract class AbsActionBarView extends ViewGroup {
    protected final AbsActionBarView f1182a;
    protected final Context f1183b;
    protected ActionMenuView f1184c;
    protected ActionMenuPresenter f1185d;
    protected int f1186e;
    protected au f1187f;
    private boolean f1188g;
    private boolean f1189h;

    /* renamed from: android.support.v7.widget.a.a */
    protected class AbsActionBarView implements ay {
        int f1401a;
        final /* synthetic */ AbsActionBarView f1402b;
        private boolean f1403c;

        protected AbsActionBarView(AbsActionBarView absActionBarView) {
            this.f1402b = absActionBarView;
            this.f1403c = false;
        }

        public AbsActionBarView m2672a(au auVar, int i) {
            this.f1402b.f1187f = auVar;
            this.f1401a = i;
            return this;
        }

        public void m2673a(View view) {
            super.setVisibility(0);
            this.f1403c = false;
        }

        public void m2674b(View view) {
            if (!this.f1403c) {
                this.f1402b.f1187f = null;
                super.setVisibility(this.f1401a);
            }
        }

        public void m2675c(View view) {
            this.f1403c = true;
        }
    }

    AbsActionBarView(Context context) {
        this(context, null);
    }

    AbsActionBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    AbsActionBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1182a = new AbsActionBarView(this);
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(R.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.f1183b = context;
        } else {
            this.f1183b = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    protected void onConfigurationChanged(Configuration configuration) {
        if (VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, R.ActionBar, R.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(R.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        if (this.f1185d != null) {
            this.f1185d.m2909a(configuration);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m1507a(motionEvent);
        if (a == 0) {
            this.f1188g = false;
        }
        if (!this.f1188g) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (a == 0 && !onTouchEvent) {
                this.f1188g = true;
            }
        }
        if (a == 1 || a == 3) {
            this.f1188g = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m1507a(motionEvent);
        if (a == 9) {
            this.f1189h = false;
        }
        if (!this.f1189h) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (a == 9 && !onHoverEvent) {
                this.f1189h = true;
            }
        }
        if (a == 10 || a == 3) {
            this.f1189h = false;
        }
        return true;
    }

    public void setContentHeight(int i) {
        this.f1186e = i;
        requestLayout();
    }

    public int getContentHeight() {
        return this.f1186e;
    }

    public int getAnimatedVisibility() {
        if (this.f1187f != null) {
            return this.f1182a.f1401a;
        }
        return getVisibility();
    }

    public au m2463a(int i, long j) {
        if (this.f1187f != null) {
            this.f1187f.m1360b();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                af.m1187b((View) this, 0.0f);
            }
            au a = af.m1199k(this).m1353a(1.0f);
            a.m1354a(j);
            a.m1355a(this.f1182a.m2672a(a, i));
            return a;
        }
        a = af.m1199k(this).m1353a(0.0f);
        a.m1354a(j);
        a.m1355a(this.f1182a.m2672a(a, i));
        return a;
    }

    public void setVisibility(int i) {
        if (i != getVisibility()) {
            if (this.f1187f != null) {
                this.f1187f.m1360b();
            }
            super.setVisibility(i);
        }
    }

    public boolean m2464a() {
        if (this.f1185d != null) {
            return this.f1185d.m2923d();
        }
        return false;
    }

    protected int m2461a(View view, int i, int i2, int i3) {
        view.measure(MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    protected static int m2458a(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    protected int m2462a(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
