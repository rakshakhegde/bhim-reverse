package android.support.v7.widget;

/* compiled from: RtlSpacingHelper */
class aj {
    private int f1464a;
    private int f1465b;
    private int f1466c;
    private int f1467d;
    private int f1468e;
    private int f1469f;
    private boolean f1470g;
    private boolean f1471h;

    aj() {
        this.f1464a = 0;
        this.f1465b = 0;
        this.f1466c = Integer.MIN_VALUE;
        this.f1467d = Integer.MIN_VALUE;
        this.f1468e = 0;
        this.f1469f = 0;
        this.f1470g = false;
        this.f1471h = false;
    }

    public int m2759a() {
        return this.f1464a;
    }

    public int m2762b() {
        return this.f1465b;
    }

    public int m2764c() {
        return this.f1470g ? this.f1465b : this.f1464a;
    }

    public int m2765d() {
        return this.f1470g ? this.f1464a : this.f1465b;
    }

    public void m2760a(int i, int i2) {
        this.f1466c = i;
        this.f1467d = i2;
        this.f1471h = true;
        if (this.f1470g) {
            if (i2 != Integer.MIN_VALUE) {
                this.f1464a = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f1465b = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f1464a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1465b = i2;
        }
    }

    public void m2763b(int i, int i2) {
        this.f1471h = false;
        if (i != Integer.MIN_VALUE) {
            this.f1468e = i;
            this.f1464a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1469f = i2;
            this.f1465b = i2;
        }
    }

    public void m2761a(boolean z) {
        if (z != this.f1470g) {
            this.f1470g = z;
            if (!this.f1471h) {
                this.f1464a = this.f1468e;
                this.f1465b = this.f1469f;
            } else if (z) {
                this.f1464a = this.f1467d != Integer.MIN_VALUE ? this.f1467d : this.f1468e;
                this.f1465b = this.f1466c != Integer.MIN_VALUE ? this.f1466c : this.f1469f;
            } else {
                this.f1464a = this.f1466c != Integer.MIN_VALUE ? this.f1466c : this.f1468e;
                this.f1465b = this.f1467d != Integer.MIN_VALUE ? this.f1467d : this.f1469f;
            }
        }
    }
}
