package android.support.v7.widget;

import android.annotation.TargetApi;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.ResultReceiver;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.p014b.R.R;
import android.support.v7.view.CollapsibleActionView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import com.crashlytics.android.core.BuildConfig;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView extends af implements CollapsibleActionView {
    static final C0022a f1308a;
    private static final boolean f1309b;
    private boolean f1310A;
    private boolean f1311B;
    private int f1312C;
    private boolean f1313D;
    private CharSequence f1314E;
    private boolean f1315F;
    private int f1316G;
    private SearchableInfo f1317H;
    private Bundle f1318I;
    private Runnable f1319J;
    private final Runnable f1320K;
    private Runnable f1321L;
    private final WeakHashMap<String, ConstantState> f1322M;
    private final SearchAutoComplete f1323c;
    private final View f1324d;
    private final View f1325e;
    private final ImageView f1326f;
    private final ImageView f1327g;
    private final ImageView f1328h;
    private final ImageView f1329i;
    private final ImageView f1330j;
    private final Drawable f1331k;
    private final int f1332l;
    private final int f1333m;
    private final Intent f1334n;
    private final Intent f1335o;
    private final CharSequence f1336p;
    private C0024c f1337q;
    private C0023b f1338r;
    private OnFocusChangeListener f1339s;
    private C0025d f1340t;
    private OnClickListener f1341u;
    private boolean f1342v;
    private boolean f1343w;
    private CursorAdapter f1344x;
    private boolean f1345y;
    private CharSequence f1346z;

    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        boolean f1297a;

        /* renamed from: android.support.v7.widget.SearchView.SavedState.1 */
        static class C00211 implements Creator<SavedState> {
            C00211() {
            }

            public /* synthetic */ Object createFromParcel(Parcel parcel) {
                return m2582a(parcel);
            }

            public /* synthetic */ Object[] newArray(int i) {
                return m2583a(i);
            }

            public SavedState m2582a(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] m2583a(int i) {
                return new SavedState[i];
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f1297a = ((Boolean) parcel.readValue(null)).booleanValue();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeValue(Boolean.valueOf(this.f1297a));
        }

        public String toString() {
            return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.f1297a + "}";
        }

        static {
            CREATOR = new C00211();
        }
    }

    public static class SearchAutoComplete extends AppCompatAutoCompleteTextView {
        private int f1302a;
        private SearchView f1303b;

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, R.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.f1302a = getThreshold();
        }

        void setSearchView(SearchView searchView) {
            this.f1303b = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.f1302a = i;
        }

        protected void replaceText(CharSequence charSequence) {
        }

        public void performCompletion() {
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.f1303b.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.m2593a(getContext())) {
                    SearchView.f1308a.m2585a(this, true);
                }
            }
        }

        protected void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.f1303b.m2614d();
        }

        public boolean enoughToFilter() {
            return this.f1302a <= 0 || super.enoughToFilter();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                DispatcherState keyDispatcherState;
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.f1303b.clearFocus();
                        this.f1303b.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }
    }

    /* renamed from: android.support.v7.widget.SearchView.a */
    private static class C0022a {
        private Method f1304a;
        private Method f1305b;
        private Method f1306c;
        private Method f1307d;

        C0022a() {
            try {
                this.f1304a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.f1304a.setAccessible(true);
            } catch (NoSuchMethodException e) {
            }
            try {
                this.f1305b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.f1305b.setAccessible(true);
            } catch (NoSuchMethodException e2) {
            }
            try {
                this.f1306c = AutoCompleteTextView.class.getMethod("ensureImeVisible", new Class[]{Boolean.TYPE});
                this.f1306c.setAccessible(true);
            } catch (NoSuchMethodException e3) {
            }
            try {
                this.f1307d = InputMethodManager.class.getMethod("showSoftInputUnchecked", new Class[]{Integer.TYPE, ResultReceiver.class});
                this.f1307d.setAccessible(true);
            } catch (NoSuchMethodException e4) {
            }
        }

        void m2584a(AutoCompleteTextView autoCompleteTextView) {
            if (this.f1304a != null) {
                try {
                    this.f1304a.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception e) {
                }
            }
        }

        void m2586b(AutoCompleteTextView autoCompleteTextView) {
            if (this.f1305b != null) {
                try {
                    this.f1305b.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception e) {
                }
            }
        }

        void m2585a(AutoCompleteTextView autoCompleteTextView, boolean z) {
            if (this.f1306c != null) {
                try {
                    this.f1306c.invoke(autoCompleteTextView, new Object[]{Boolean.valueOf(z)});
                } catch (Exception e) {
                }
            }
        }
    }

    /* renamed from: android.support.v7.widget.SearchView.b */
    public interface C0023b {
        boolean m2587a();
    }

    /* renamed from: android.support.v7.widget.SearchView.c */
    public interface C0024c {
        boolean m2588a(String str);
    }

    /* renamed from: android.support.v7.widget.SearchView.d */
    public interface C0025d {
    }

    static {
        boolean z;
        if (VERSION.SDK_INT >= 8) {
            z = true;
        } else {
            z = false;
        }
        f1309b = z;
        f1308a = new C0022a();
    }

    int getSuggestionRowLayout() {
        return this.f1332l;
    }

    int getSuggestionCommitIconResId() {
        return this.f1333m;
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.f1317H = searchableInfo;
        if (this.f1317H != null) {
            if (f1309b) {
                m2603l();
            }
            m2602k();
        }
        boolean z = f1309b && m2597e();
        this.f1313D = z;
        if (this.f1313D) {
            this.f1323c.setPrivateImeOptions("nm");
        }
        m2592a(m2613c());
    }

    public void setAppSearchData(Bundle bundle) {
        this.f1318I = bundle;
    }

    public void setImeOptions(int i) {
        this.f1323c.setImeOptions(i);
    }

    public int getImeOptions() {
        return this.f1323c.getImeOptions();
    }

    public void setInputType(int i) {
        this.f1323c.setInputType(i);
    }

    public int getInputType() {
        return this.f1323c.getInputType();
    }

    public boolean requestFocus(int i, Rect rect) {
        if (this.f1311B || !isFocusable()) {
            return false;
        }
        if (m2613c()) {
            return super.requestFocus(i, rect);
        }
        boolean requestFocus = this.f1323c.requestFocus(i, rect);
        if (requestFocus) {
            m2592a(false);
        }
        return requestFocus;
    }

    public void clearFocus() {
        this.f1311B = true;
        setImeVisibility(false);
        super.clearFocus();
        this.f1323c.clearFocus();
        this.f1311B = false;
    }

    public void setOnQueryTextListener(C0024c c0024c) {
        this.f1337q = c0024c;
    }

    public void setOnCloseListener(C0023b c0023b) {
        this.f1338r = c0023b;
    }

    public void setOnQueryTextFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.f1339s = onFocusChangeListener;
    }

    public void setOnSuggestionListener(C0025d c0025d) {
        this.f1340t = c0025d;
    }

    public void setOnSearchClickListener(OnClickListener onClickListener) {
        this.f1341u = onClickListener;
    }

    public CharSequence getQuery() {
        return this.f1323c.getText();
    }

    public void m2611a(CharSequence charSequence, boolean z) {
        this.f1323c.setText(charSequence);
        if (charSequence != null) {
            this.f1323c.setSelection(this.f1323c.length());
            this.f1314E = charSequence;
        }
        if (z && !TextUtils.isEmpty(charSequence)) {
            m2604m();
        }
    }

    public void setQueryHint(CharSequence charSequence) {
        this.f1346z = charSequence;
        m2602k();
    }

    public CharSequence getQueryHint() {
        if (this.f1346z != null) {
            return this.f1346z;
        }
        if (!f1309b || this.f1317H == null || this.f1317H.getHintId() == 0) {
            return this.f1336p;
        }
        return getContext().getText(this.f1317H.getHintId());
    }

    public void setIconifiedByDefault(boolean z) {
        if (this.f1342v != z) {
            this.f1342v = z;
            m2592a(z);
            m2602k();
        }
    }

    public void setIconified(boolean z) {
        if (z) {
            m2606o();
        } else {
            m2607p();
        }
    }

    public boolean m2613c() {
        return this.f1343w;
    }

    public void setSubmitButtonEnabled(boolean z) {
        this.f1345y = z;
        m2592a(m2613c());
    }

    public void setQueryRefinementEnabled(boolean z) {
        this.f1310A = z;
        if (this.f1344x instanceof al) {
            ((al) this.f1344x).m2796a(z ? 2 : 1);
        }
    }

    public void setSuggestionsAdapter(CursorAdapter cursorAdapter) {
        this.f1344x = cursorAdapter;
        this.f1323c.setAdapter(this.f1344x);
    }

    public CursorAdapter getSuggestionsAdapter() {
        return this.f1344x;
    }

    public void setMaxWidth(int i) {
        this.f1312C = i;
        requestLayout();
    }

    public int getMaxWidth() {
        return this.f1312C;
    }

    protected void onMeasure(int i, int i2) {
        if (m2613c()) {
            super.onMeasure(i, i2);
            return;
        }
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.f1312C <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.f1312C, size);
                    break;
                }
            case org.npci.upi.security.pinactivitycomponent.R.R.View_android_theme /*0*/:
                if (this.f1312C <= 0) {
                    size = getPreferredWidth();
                    break;
                } else {
                    size = this.f1312C;
                    break;
                }
            case 1073741824:
                if (this.f1312C > 0) {
                    size = Math.min(this.f1312C, size);
                    break;
                }
                break;
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, 1073741824), i2);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(R.abc_search_view_preferred_width);
    }

    private void m2592a(boolean z) {
        boolean z2;
        boolean z3 = true;
        int i = 8;
        this.f1343w = z;
        int i2 = z ? 0 : 8;
        if (TextUtils.isEmpty(this.f1323c.getText())) {
            z2 = false;
        } else {
            z2 = true;
        }
        this.f1326f.setVisibility(i2);
        m2595b(z2);
        View view = this.f1324d;
        if (z) {
            i2 = 8;
        } else {
            i2 = 0;
        }
        view.setVisibility(i2);
        if (!(this.f1330j.getDrawable() == null || this.f1342v)) {
            i = 0;
        }
        this.f1330j.setVisibility(i);
        m2600h();
        if (z2) {
            z3 = false;
        }
        m2596c(z3);
        m2599g();
    }

    @TargetApi(8)
    private boolean m2597e() {
        if (this.f1317H == null || !this.f1317H.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.f1317H.getVoiceSearchLaunchWebSearch()) {
            intent = this.f1334n;
        } else if (this.f1317H.getVoiceSearchLaunchRecognizer()) {
            intent = this.f1335o;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) {
            return false;
        }
        return true;
    }

    private boolean m2598f() {
        return (this.f1345y || this.f1313D) && !m2613c();
    }

    private void m2595b(boolean z) {
        int i = 8;
        if (this.f1345y && m2598f() && hasFocus() && (z || !this.f1313D)) {
            i = 0;
        }
        this.f1327g.setVisibility(i);
    }

    private void m2599g() {
        int i = 8;
        if (m2598f() && (this.f1327g.getVisibility() == 0 || this.f1329i.getVisibility() == 0)) {
            i = 0;
        }
        this.f1325e.setVisibility(i);
    }

    private void m2600h() {
        int i = 1;
        int i2 = 0;
        int i3 = !TextUtils.isEmpty(this.f1323c.getText()) ? 1 : 0;
        if (i3 == 0 && (!this.f1342v || this.f1315F)) {
            i = 0;
        }
        ImageView imageView = this.f1328h;
        if (i == 0) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        Drawable drawable = this.f1328h.getDrawable();
        if (drawable != null) {
            drawable.setState(i3 != 0 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
        }
    }

    private void m2601i() {
        post(this.f1320K);
    }

    protected void onDetachedFromWindow() {
        removeCallbacks(this.f1320K);
        post(this.f1321L);
        super.onDetachedFromWindow();
    }

    private void setImeVisibility(boolean z) {
        if (z) {
            post(this.f1319J);
            return;
        }
        removeCallbacks(this.f1319J);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    void m2610a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    private CharSequence m2594b(CharSequence charSequence) {
        if (!this.f1342v || this.f1331k == null) {
            return charSequence;
        }
        int textSize = (int) (((double) this.f1323c.getTextSize()) * 1.25d);
        this.f1331k.setBounds(0, 0, textSize, textSize);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.setSpan(new ImageSpan(this.f1331k), 1, 2, 33);
        spannableStringBuilder.append(charSequence);
        return spannableStringBuilder;
    }

    private void m2602k() {
        CharSequence queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.f1323c;
        if (queryHint == null) {
            queryHint = BuildConfig.FLAVOR;
        }
        searchAutoComplete.setHint(m2594b(queryHint));
    }

    @TargetApi(8)
    private void m2603l() {
        int i = 1;
        this.f1323c.setThreshold(this.f1317H.getSuggestThreshold());
        this.f1323c.setImeOptions(this.f1317H.getImeOptions());
        int inputType = this.f1317H.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.f1317H.getSuggestAuthority() != null) {
                inputType = (inputType | 65536) | 524288;
            }
        }
        this.f1323c.setInputType(inputType);
        if (this.f1344x != null) {
            this.f1344x.m1644a(null);
        }
        if (this.f1317H.getSuggestAuthority() != null) {
            this.f1344x = new al(getContext(), this, this.f1317H, this.f1322M);
            this.f1323c.setAdapter(this.f1344x);
            al alVar = (al) this.f1344x;
            if (this.f1310A) {
                i = 2;
            }
            alVar.m2796a(i);
        }
    }

    private void m2596c(boolean z) {
        int i;
        if (this.f1313D && !m2613c() && z) {
            i = 0;
            this.f1327g.setVisibility(8);
        } else {
            i = 8;
        }
        this.f1329i.setVisibility(i);
    }

    private void m2604m() {
        CharSequence text = this.f1323c.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            if (this.f1337q == null || !this.f1337q.m2588a(text.toString())) {
                if (this.f1317H != null) {
                    m2590a(0, null, text.toString());
                }
                setImeVisibility(false);
                m2605n();
            }
        }
    }

    private void m2605n() {
        this.f1323c.dismissDropDown();
    }

    private void m2606o() {
        if (!TextUtils.isEmpty(this.f1323c.getText())) {
            this.f1323c.setText(BuildConfig.FLAVOR);
            this.f1323c.requestFocus();
            setImeVisibility(true);
        } else if (!this.f1342v) {
        } else {
            if (this.f1338r == null || !this.f1338r.m2587a()) {
                clearFocus();
                m2592a(true);
            }
        }
    }

    private void m2607p() {
        m2592a(false);
        this.f1323c.requestFocus();
        setImeVisibility(true);
        if (this.f1341u != null) {
            this.f1341u.onClick(this);
        }
    }

    void m2614d() {
        m2592a(m2613c());
        m2601i();
        if (this.f1323c.hasFocus()) {
            m2608q();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        m2601i();
    }

    public void m2612b() {
        m2611a(BuildConfig.FLAVOR, false);
        clearFocus();
        m2592a(true);
        this.f1323c.setImeOptions(this.f1316G);
        this.f1315F = false;
    }

    public void m2609a() {
        if (!this.f1315F) {
            this.f1315F = true;
            this.f1316G = this.f1323c.getImeOptions();
            this.f1323c.setImeOptions(this.f1316G | 33554432);
            this.f1323c.setText(BuildConfig.FLAVOR);
            setIconified(false);
        }
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1297a = m2613c();
        return savedState;
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            m2592a(savedState.f1297a);
            requestLayout();
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    private void setQuery(CharSequence charSequence) {
        this.f1323c.setText(charSequence);
        this.f1323c.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    private void m2590a(int i, String str, String str2) {
        getContext().startActivity(m2589a("android.intent.action.SEARCH", null, null, str2, i, str));
    }

    private Intent m2589a(String str, Uri uri, String str2, String str3, int i, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.f1314E);
        if (str3 != null) {
            intent.putExtra("query", str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        if (this.f1318I != null) {
            intent.putExtra("app_data", this.f1318I);
        }
        if (i != 0) {
            intent.putExtra("action_key", i);
            intent.putExtra("action_msg", str4);
        }
        if (f1309b) {
            intent.setComponent(this.f1317H.getSearchActivity());
        }
        return intent;
    }

    private void m2608q() {
        f1308a.m2584a(this.f1323c);
        f1308a.m2586b(this.f1323c);
    }

    static boolean m2593a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }
}
