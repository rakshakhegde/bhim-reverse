package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.p006f.ac;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.ImageButton;

/* renamed from: android.support.v7.widget.n */
public class AppCompatImageButton extends ImageButton implements ac {
    private AppCompatBackgroundHelper f1653a;
    private AppCompatImageHelper f1654b;

    public AppCompatImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.imageButtonStyle);
    }

    public AppCompatImageButton(Context context, AttributeSet attributeSet, int i) {
        super(ao.m2807a(context), attributeSet, i);
        AppCompatDrawableManager a = AppCompatDrawableManager.m2981a();
        this.f1653a = new AppCompatBackgroundHelper(this, a);
        this.f1653a.m2957a(attributeSet, i);
        this.f1654b = new AppCompatImageHelper(this, a);
        this.f1654b.m3009a(attributeSet, i);
    }

    public void setImageResource(int i) {
        this.f1654b.m3008a(i);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1653a != null) {
            this.f1653a.m2953a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1653a != null) {
            this.f1653a.m2956a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1653a != null) {
            this.f1653a.m2954a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        return this.f1653a != null ? this.f1653a.m2952a() : null;
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1653a != null) {
            this.f1653a.m2955a(mode);
        }
    }

    public Mode getSupportBackgroundTintMode() {
        return this.f1653a != null ? this.f1653a.m2958b() : null;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1653a != null) {
            this.f1653a.m2960c();
        }
    }
}
