package android.support.v7.widget;

import android.graphics.drawable.Drawable;
import android.support.v4.p004a.ContextCompat;
import android.support.v7.p014b.R.R;
import android.util.AttributeSet;
import android.widget.ImageView;

/* renamed from: android.support.v7.widget.o */
public class AppCompatImageHelper {
    private final ImageView f1655a;
    private final AppCompatDrawableManager f1656b;

    public AppCompatImageHelper(ImageView imageView, AppCompatDrawableManager appCompatDrawableManager) {
        this.f1655a = imageView;
        this.f1656b = appCompatDrawableManager;
    }

    public void m3009a(AttributeSet attributeSet, int i) {
        ar a = ar.m2811a(this.f1655a.getContext(), attributeSet, R.AppCompatImageView, i, 0);
        try {
            Drawable b = a.m2818b(R.AppCompatImageView_android_src);
            if (b != null) {
                this.f1655a.setImageDrawable(b);
            }
            int g = a.m2827g(R.AppCompatImageView_srcCompat, -1);
            if (g != -1) {
                b = this.f1656b.m3004a(this.f1655a.getContext(), g);
                if (b != null) {
                    this.f1655a.setImageDrawable(b);
                }
            }
            b = this.f1655a.getDrawable();
            if (b != null) {
                ad.m2707a(b);
            }
            a.m2815a();
        } catch (Throwable th) {
            a.m2815a();
        }
    }

    public void m3008a(int i) {
        if (i != 0) {
            Drawable a = this.f1656b != null ? this.f1656b.m3004a(this.f1655a.getContext(), i) : ContextCompat.m77a(this.f1655a.getContext(), i);
            if (a != null) {
                ad.m2707a(a);
            }
            this.f1655a.setImageDrawable(a);
            return;
        }
        this.f1655a.setImageDrawable(null);
    }
}
