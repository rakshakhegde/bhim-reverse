package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.p013a.ActionBar.ActionBar;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.widget.af.LinearLayoutCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/* compiled from: ScrollingTabContainerView */
public class ak extends HorizontalScrollView implements OnItemSelectedListener {
    private static final Interpolator f1482j;
    Runnable f1483a;
    int f1484b;
    int f1485c;
    private ScrollingTabContainerView f1486d;
    private af f1487e;
    private Spinner f1488f;
    private boolean f1489g;
    private int f1490h;
    private int f1491i;

    /* renamed from: android.support.v7.widget.ak.1 */
    class ScrollingTabContainerView implements Runnable {
        final /* synthetic */ View f1472a;
        final /* synthetic */ ak f1473b;

        ScrollingTabContainerView(ak akVar, View view) {
            this.f1473b = akVar;
            this.f1472a = view;
        }

        public void run() {
            this.f1473b.smoothScrollTo(this.f1472a.getLeft() - ((this.f1473b.getWidth() - this.f1472a.getWidth()) / 2), 0);
            this.f1473b.f1483a = null;
        }
    }

    /* renamed from: android.support.v7.widget.ak.a */
    private class ScrollingTabContainerView extends BaseAdapter {
        final /* synthetic */ ak f1474a;

        private ScrollingTabContainerView(ak akVar) {
            this.f1474a = akVar;
        }

        public int getCount() {
            return this.f1474a.f1487e.getChildCount();
        }

        public Object getItem(int i) {
            return ((ScrollingTabContainerView) this.f1474a.f1487e.getChildAt(i)).m2768b();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return this.f1474a.m2770a((ActionBar) getItem(i), true);
            }
            ((ScrollingTabContainerView) view).m2767a((ActionBar) getItem(i));
            return view;
        }
    }

    /* renamed from: android.support.v7.widget.ak.b */
    private class ScrollingTabContainerView implements OnClickListener {
        final /* synthetic */ ak f1475a;

        private ScrollingTabContainerView(ak akVar) {
            this.f1475a = akVar;
        }

        public void onClick(View view) {
            ((ScrollingTabContainerView) view).m2768b().m1779d();
            int childCount = this.f1475a.f1487e.getChildCount();
            for (int i = 0; i < childCount; i++) {
                boolean z;
                View childAt = this.f1475a.f1487e.getChildAt(i);
                if (childAt == view) {
                    z = true;
                } else {
                    z = false;
                }
                childAt.setSelected(z);
            }
        }
    }

    /* renamed from: android.support.v7.widget.ak.c */
    private class ScrollingTabContainerView extends af implements OnLongClickListener {
        final /* synthetic */ ak f1476a;
        private final int[] f1477b;
        private ActionBar f1478c;
        private TextView f1479d;
        private ImageView f1480e;
        private View f1481f;

        public ScrollingTabContainerView(ak akVar, Context context, ActionBar actionBar, boolean z) {
            this.f1476a = akVar;
            super(context, null, R.actionBarTabStyle);
            this.f1477b = new int[]{16842964};
            this.f1478c = actionBar;
            ar a = ar.m2811a(context, null, this.f1477b, R.actionBarTabStyle, 0);
            if (a.m2826f(0)) {
                setBackgroundDrawable(a.m2814a(0));
            }
            a.m2815a();
            if (z) {
                setGravity(8388627);
            }
            m2766a();
        }

        public void m2767a(ActionBar actionBar) {
            this.f1478c = actionBar;
            m2766a();
        }

        public void setSelected(boolean z) {
            Object obj = isSelected() != z ? 1 : null;
            super.setSelected(z);
            if (obj != null && z) {
                sendAccessibilityEvent(4);
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(ActionBar.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            if (VERSION.SDK_INT >= 14) {
                accessibilityNodeInfo.setClassName(ActionBar.class.getName());
            }
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (this.f1476a.f1484b > 0 && getMeasuredWidth() > this.f1476a.f1484b) {
                super.onMeasure(MeasureSpec.makeMeasureSpec(this.f1476a.f1484b, 1073741824), i2);
            }
        }

        public void m2766a() {
            ActionBar actionBar = this.f1478c;
            View c = actionBar.m1778c();
            if (c != null) {
                ScrollingTabContainerView parent = c.getParent();
                if (parent != this) {
                    if (parent != null) {
                        parent.removeView(c);
                    }
                    addView(c);
                }
                this.f1481f = c;
                if (this.f1479d != null) {
                    this.f1479d.setVisibility(8);
                }
                if (this.f1480e != null) {
                    this.f1480e.setVisibility(8);
                    this.f1480e.setImageDrawable(null);
                    return;
                }
                return;
            }
            boolean z;
            if (this.f1481f != null) {
                removeView(this.f1481f);
                this.f1481f = null;
            }
            Drawable a = actionBar.m1776a();
            CharSequence b = actionBar.m1777b();
            if (a != null) {
                if (this.f1480e == null) {
                    View imageView = new ImageView(getContext());
                    LayoutParams linearLayoutCompat = new LinearLayoutCompat(-2, -2);
                    linearLayoutCompat.f1234h = 16;
                    imageView.setLayoutParams(linearLayoutCompat);
                    addView(imageView, 0);
                    this.f1480e = imageView;
                }
                this.f1480e.setImageDrawable(a);
                this.f1480e.setVisibility(0);
            } else if (this.f1480e != null) {
                this.f1480e.setVisibility(8);
                this.f1480e.setImageDrawable(null);
            }
            if (TextUtils.isEmpty(b)) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                if (this.f1479d == null) {
                    imageView = new aa(getContext(), null, R.actionBarTabTextStyle);
                    imageView.setEllipsize(TruncateAt.END);
                    linearLayoutCompat = new LinearLayoutCompat(-2, -2);
                    linearLayoutCompat.f1234h = 16;
                    imageView.setLayoutParams(linearLayoutCompat);
                    addView(imageView);
                    this.f1479d = imageView;
                }
                this.f1479d.setText(b);
                this.f1479d.setVisibility(0);
            } else if (this.f1479d != null) {
                this.f1479d.setVisibility(8);
                this.f1479d.setText(null);
            }
            if (this.f1480e != null) {
                this.f1480e.setContentDescription(actionBar.m1780e());
            }
            if (z || TextUtils.isEmpty(actionBar.m1780e())) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.f1478c.m1780e(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public ActionBar m2768b() {
            return this.f1478c;
        }
    }

    static {
        f1482j = new DecelerateInterpolator();
    }

    public void onMeasure(int i, int i2) {
        int i3 = 1;
        int mode = MeasureSpec.getMode(i);
        boolean z = mode == 1073741824;
        setFillViewport(z);
        int childCount = this.f1487e.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f1484b = -1;
        } else {
            if (childCount > 2) {
                this.f1484b = (int) (((float) MeasureSpec.getSize(i)) * 0.4f);
            } else {
                this.f1484b = MeasureSpec.getSize(i) / 2;
            }
            this.f1484b = Math.min(this.f1484b, this.f1485c);
        }
        mode = MeasureSpec.makeMeasureSpec(this.f1490h, 1073741824);
        if (z || !this.f1489g) {
            i3 = 0;
        }
        if (i3 != 0) {
            this.f1487e.measure(0, mode);
            if (this.f1487e.getMeasuredWidth() > MeasureSpec.getSize(i)) {
                m2773b();
            } else {
                m2774c();
            }
        } else {
            m2774c();
        }
        i3 = getMeasuredWidth();
        super.onMeasure(i, mode);
        int measuredWidth = getMeasuredWidth();
        if (z && i3 != measuredWidth) {
            setTabSelected(this.f1491i);
        }
    }

    private boolean m2772a() {
        return this.f1488f != null && this.f1488f.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.f1489g = z;
    }

    private void m2773b() {
        if (!m2772a()) {
            if (this.f1488f == null) {
                this.f1488f = m2775d();
            }
            removeView(this.f1487e);
            addView(this.f1488f, new LayoutParams(-2, -1));
            if (this.f1488f.getAdapter() == null) {
                this.f1488f.setAdapter(new ScrollingTabContainerView());
            }
            if (this.f1483a != null) {
                removeCallbacks(this.f1483a);
                this.f1483a = null;
            }
            this.f1488f.setSelection(this.f1491i);
        }
    }

    private boolean m2774c() {
        if (m2772a()) {
            removeView(this.f1488f);
            addView(this.f1487e, new LayoutParams(-2, -1));
            setTabSelected(this.f1488f.getSelectedItemPosition());
        }
        return false;
    }

    public void setTabSelected(int i) {
        this.f1491i = i;
        int childCount = this.f1487e.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            boolean z;
            View childAt = this.f1487e.getChildAt(i2);
            if (i2 == i) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                m2776a(i);
            }
        }
        if (this.f1488f != null && i >= 0) {
            this.f1488f.setSelection(i);
        }
    }

    public void setContentHeight(int i) {
        this.f1490h = i;
        requestLayout();
    }

    private Spinner m2775d() {
        Spinner appCompatSpinner = new AppCompatSpinner(getContext(), null, R.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    protected void onConfigurationChanged(Configuration configuration) {
        if (VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        ActionBarPolicy a = ActionBarPolicy.m2164a(getContext());
        setContentHeight(a.m2169e());
        this.f1485c = a.m2171g();
    }

    public void m2776a(int i) {
        View childAt = this.f1487e.getChildAt(i);
        if (this.f1483a != null) {
            removeCallbacks(this.f1483a);
        }
        this.f1483a = new ScrollingTabContainerView(this, childAt);
        post(this.f1483a);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f1483a != null) {
            post(this.f1483a);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1483a != null) {
            removeCallbacks(this.f1483a);
        }
    }

    private ScrollingTabContainerView m2770a(ActionBar actionBar, boolean z) {
        ScrollingTabContainerView scrollingTabContainerView = new ScrollingTabContainerView(this, getContext(), actionBar, z);
        if (z) {
            scrollingTabContainerView.setBackgroundDrawable(null);
            scrollingTabContainerView.setLayoutParams(new AbsListView.LayoutParams(-1, this.f1490h));
        } else {
            scrollingTabContainerView.setFocusable(true);
            if (this.f1486d == null) {
                this.f1486d = new ScrollingTabContainerView();
            }
            scrollingTabContainerView.setOnClickListener(this.f1486d);
        }
        return scrollingTabContainerView;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        ((ScrollingTabContainerView) view).m2768b().m1779d();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
