package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.support.v4.p006f.ActionProvider;
import android.support.v4.p006f.MenuItemCompat;
import android.support.v4.p007c.p008a.SupportMenu;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/* renamed from: android.support.v7.view.g */
public class SupportMenuInflater extends MenuInflater {
    private static final Class<?>[] f987a;
    private static final Class<?>[] f988b;
    private final Object[] f989c;
    private final Object[] f990d;
    private Context f991e;
    private Object f992f;

    /* renamed from: android.support.v7.view.g.a */
    private static class SupportMenuInflater implements OnMenuItemClickListener {
        private static final Class<?>[] f958a;
        private Object f959b;
        private Method f960c;

        static {
            f958a = new Class[]{MenuItem.class};
        }

        public SupportMenuInflater(Object obj, String str) {
            this.f959b = obj;
            Class cls = obj.getClass();
            try {
                this.f960c = cls.getMethod(str, f958a);
            } catch (Throwable e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.f960c.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.f960c.invoke(this.f959b, new Object[]{menuItem})).booleanValue();
                }
                this.f960c.invoke(this.f959b, new Object[]{menuItem});
                return true;
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: android.support.v7.view.g.b */
    private class SupportMenuInflater {
        final /* synthetic */ SupportMenuInflater f961a;
        private Menu f962b;
        private int f963c;
        private int f964d;
        private int f965e;
        private int f966f;
        private boolean f967g;
        private boolean f968h;
        private boolean f969i;
        private int f970j;
        private int f971k;
        private CharSequence f972l;
        private CharSequence f973m;
        private int f974n;
        private char f975o;
        private char f976p;
        private int f977q;
        private boolean f978r;
        private boolean f979s;
        private boolean f980t;
        private int f981u;
        private int f982v;
        private String f983w;
        private String f984x;
        private String f985y;
        private ActionProvider f986z;

        public SupportMenuInflater(SupportMenuInflater supportMenuInflater, Menu menu) {
            this.f961a = supportMenuInflater;
            this.f962b = menu;
            m2203a();
        }

        public void m2203a() {
            this.f963c = 0;
            this.f964d = 0;
            this.f965e = 0;
            this.f966f = 0;
            this.f967g = true;
            this.f968h = true;
        }

        public void m2204a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = this.f961a.f991e.obtainStyledAttributes(attributeSet, R.MenuGroup);
            this.f963c = obtainStyledAttributes.getResourceId(R.MenuGroup_android_id, 0);
            this.f964d = obtainStyledAttributes.getInt(R.MenuGroup_android_menuCategory, 0);
            this.f965e = obtainStyledAttributes.getInt(R.MenuGroup_android_orderInCategory, 0);
            this.f966f = obtainStyledAttributes.getInt(R.MenuGroup_android_checkableBehavior, 0);
            this.f967g = obtainStyledAttributes.getBoolean(R.MenuGroup_android_visible, true);
            this.f968h = obtainStyledAttributes.getBoolean(R.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        public void m2206b(AttributeSet attributeSet) {
            boolean z = true;
            TypedArray obtainStyledAttributes = this.f961a.f991e.obtainStyledAttributes(attributeSet, R.MenuItem);
            this.f970j = obtainStyledAttributes.getResourceId(R.MenuItem_android_id, 0);
            this.f971k = (obtainStyledAttributes.getInt(R.MenuItem_android_menuCategory, this.f964d) & -65536) | (obtainStyledAttributes.getInt(R.MenuItem_android_orderInCategory, this.f965e) & 65535);
            this.f972l = obtainStyledAttributes.getText(R.MenuItem_android_title);
            this.f973m = obtainStyledAttributes.getText(R.MenuItem_android_titleCondensed);
            this.f974n = obtainStyledAttributes.getResourceId(R.MenuItem_android_icon, 0);
            this.f975o = m2199a(obtainStyledAttributes.getString(R.MenuItem_android_alphabeticShortcut));
            this.f976p = m2199a(obtainStyledAttributes.getString(R.MenuItem_android_numericShortcut));
            if (obtainStyledAttributes.hasValue(R.MenuItem_android_checkable)) {
                this.f977q = obtainStyledAttributes.getBoolean(R.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.f977q = this.f966f;
            }
            this.f978r = obtainStyledAttributes.getBoolean(R.MenuItem_android_checked, false);
            this.f979s = obtainStyledAttributes.getBoolean(R.MenuItem_android_visible, this.f967g);
            this.f980t = obtainStyledAttributes.getBoolean(R.MenuItem_android_enabled, this.f968h);
            this.f981u = obtainStyledAttributes.getInt(R.MenuItem_showAsAction, -1);
            this.f985y = obtainStyledAttributes.getString(R.MenuItem_android_onClick);
            this.f982v = obtainStyledAttributes.getResourceId(R.MenuItem_actionLayout, 0);
            this.f983w = obtainStyledAttributes.getString(R.MenuItem_actionViewClass);
            this.f984x = obtainStyledAttributes.getString(R.MenuItem_actionProviderClass);
            if (this.f984x == null) {
                z = false;
            }
            if (z && this.f982v == 0 && this.f983w == null) {
                this.f986z = (ActionProvider) m2201a(this.f984x, SupportMenuInflater.f988b, this.f961a.f990d);
            } else {
                if (z) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f986z = null;
            }
            obtainStyledAttributes.recycle();
            this.f969i = false;
        }

        private char m2199a(String str) {
            if (str == null) {
                return '\u0000';
            }
            return str.charAt(0);
        }

        private void m2202a(MenuItem menuItem) {
            boolean z = true;
            menuItem.setChecked(this.f978r).setVisible(this.f979s).setEnabled(this.f980t).setCheckable(this.f977q >= 1).setTitleCondensed(this.f973m).setIcon(this.f974n).setAlphabeticShortcut(this.f975o).setNumericShortcut(this.f976p);
            if (this.f981u >= 0) {
                MenuItemCompat.m1479a(menuItem, this.f981u);
            }
            if (this.f985y != null) {
                if (this.f961a.f991e.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener(new SupportMenuInflater(this.f961a.m2215c(), this.f985y));
            }
            if (menuItem instanceof MenuItemImpl) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
            }
            if (this.f977q >= 2) {
                if (menuItem instanceof MenuItemImpl) {
                    ((MenuItemImpl) menuItem).m2385a(true);
                } else if (menuItem instanceof MenuItemWrapperICS) {
                    ((MenuItemWrapperICS) menuItem).m2414a(true);
                }
            }
            if (this.f983w != null) {
                MenuItemCompat.m1477a(menuItem, (View) m2201a(this.f983w, SupportMenuInflater.f987a, this.f961a.f989c));
            } else {
                z = false;
            }
            if (this.f982v > 0) {
                if (z) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                } else {
                    MenuItemCompat.m1480b(menuItem, this.f982v);
                }
            }
            if (this.f986z != null) {
                MenuItemCompat.m1476a(menuItem, this.f986z);
            }
        }

        public void m2205b() {
            this.f969i = true;
            m2202a(this.f962b.add(this.f963c, this.f970j, this.f971k, this.f972l));
        }

        public SubMenu m2207c() {
            this.f969i = true;
            SubMenu addSubMenu = this.f962b.addSubMenu(this.f963c, this.f970j, this.f971k, this.f972l);
            m2202a(addSubMenu.getItem());
            return addSubMenu;
        }

        public boolean m2208d() {
            return this.f969i;
        }

        private <T> T m2201a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor constructor = this.f961a.f991e.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Throwable e) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e);
                return null;
            }
        }
    }

    static {
        f987a = new Class[]{Context.class};
        f988b = f987a;
    }

    public SupportMenuInflater(Context context) {
        super(context);
        this.f991e = context;
        this.f989c = new Object[]{context};
        this.f990d = this.f989c;
    }

    public void inflate(int i, Menu menu) {
        if (menu instanceof SupportMenu) {
            XmlResourceParser xmlResourceParser = null;
            try {
                xmlResourceParser = this.f991e.getResources().getLayout(i);
                m2211a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
                if (xmlResourceParser != null) {
                    xmlResourceParser.close();
                }
            } catch (Throwable e) {
                throw new InflateException("Error inflating menu XML", e);
            } catch (Throwable e2) {
                throw new InflateException("Error inflating menu XML", e2);
            } catch (Throwable th) {
                if (xmlResourceParser != null) {
                    xmlResourceParser.close();
                }
            }
        } else {
            super.inflate(i, menu);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2211a(org.xmlpull.v1.XmlPullParser r11, android.util.AttributeSet r12, android.view.Menu r13) {
        /*
        r10 = this;
        r4 = 0;
        r1 = 1;
        r6 = 0;
        r7 = new android.support.v7.view.g$b;
        r7.<init>(r10, r13);
        r0 = r11.getEventType();
    L_0x000c:
        r2 = 2;
        if (r0 != r2) goto L_0x004a;
    L_0x000f:
        r0 = r11.getName();
        r2 = "menu";
        r2 = r0.equals(r2);
        if (r2 == 0) goto L_0x0031;
    L_0x001b:
        r0 = r11.next();
    L_0x001f:
        r2 = r4;
        r5 = r6;
        r3 = r0;
        r0 = r6;
    L_0x0023:
        if (r0 != 0) goto L_0x00e1;
    L_0x0025:
        switch(r3) {
            case 1: goto L_0x00d9;
            case 2: goto L_0x0051;
            case 3: goto L_0x0087;
            default: goto L_0x0028;
        };
    L_0x0028:
        r3 = r5;
    L_0x0029:
        r5 = r11.next();
        r9 = r3;
        r3 = r5;
        r5 = r9;
        goto L_0x0023;
    L_0x0031:
        r1 = new java.lang.RuntimeException;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "Expecting menu, got ";
        r2 = r2.append(r3);
        r0 = r2.append(r0);
        r0 = r0.toString();
        r1.<init>(r0);
        throw r1;
    L_0x004a:
        r0 = r11.next();
        if (r0 != r1) goto L_0x000c;
    L_0x0050:
        goto L_0x001f;
    L_0x0051:
        if (r5 == 0) goto L_0x0055;
    L_0x0053:
        r3 = r5;
        goto L_0x0029;
    L_0x0055:
        r3 = r11.getName();
        r8 = "group";
        r8 = r3.equals(r8);
        if (r8 == 0) goto L_0x0066;
    L_0x0061:
        r7.m2204a(r12);
        r3 = r5;
        goto L_0x0029;
    L_0x0066:
        r8 = "item";
        r8 = r3.equals(r8);
        if (r8 == 0) goto L_0x0073;
    L_0x006e:
        r7.m2206b(r12);
        r3 = r5;
        goto L_0x0029;
    L_0x0073:
        r8 = "menu";
        r8 = r3.equals(r8);
        if (r8 == 0) goto L_0x0084;
    L_0x007b:
        r3 = r7.m2207c();
        r10.m2211a(r11, r12, r3);
        r3 = r5;
        goto L_0x0029;
    L_0x0084:
        r2 = r3;
        r3 = r1;
        goto L_0x0029;
    L_0x0087:
        r3 = r11.getName();
        if (r5 == 0) goto L_0x0096;
    L_0x008d:
        r8 = r3.equals(r2);
        if (r8 == 0) goto L_0x0096;
    L_0x0093:
        r2 = r4;
        r3 = r6;
        goto L_0x0029;
    L_0x0096:
        r8 = "group";
        r8 = r3.equals(r8);
        if (r8 == 0) goto L_0x00a3;
    L_0x009e:
        r7.m2203a();
        r3 = r5;
        goto L_0x0029;
    L_0x00a3:
        r8 = "item";
        r8 = r3.equals(r8);
        if (r8 == 0) goto L_0x00cd;
    L_0x00ab:
        r3 = r7.m2208d();
        if (r3 != 0) goto L_0x0028;
    L_0x00b1:
        r3 = r7.f986z;
        if (r3 == 0) goto L_0x00c7;
    L_0x00b7:
        r3 = r7.f986z;
        r3 = r3.m1415e();
        if (r3 == 0) goto L_0x00c7;
    L_0x00c1:
        r7.m2207c();
        r3 = r5;
        goto L_0x0029;
    L_0x00c7:
        r7.m2205b();
        r3 = r5;
        goto L_0x0029;
    L_0x00cd:
        r8 = "menu";
        r3 = r3.equals(r8);
        if (r3 == 0) goto L_0x0028;
    L_0x00d5:
        r0 = r1;
        r3 = r5;
        goto L_0x0029;
    L_0x00d9:
        r0 = new java.lang.RuntimeException;
        r1 = "Unexpected end of document";
        r0.<init>(r1);
        throw r0;
    L_0x00e1:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.g.a(org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.view.Menu):void");
    }

    private Object m2215c() {
        if (this.f992f == null) {
            this.f992f = m2210a(this.f991e);
        }
        return this.f992f;
    }

    private Object m2210a(Object obj) {
        if (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) {
            return m2210a(((ContextWrapper) obj).getBaseContext());
        }
        return obj;
    }
}
