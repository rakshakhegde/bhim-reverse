package android.support.v7.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.p006f.aq;
import android.support.v7.p014b.R.R;
import android.view.ViewConfiguration;

/* renamed from: android.support.v7.view.a */
public class ActionBarPolicy {
    private Context f941a;

    public static ActionBarPolicy m2164a(Context context) {
        return new ActionBarPolicy(context);
    }

    private ActionBarPolicy(Context context) {
        this.f941a = context;
    }

    public int m2165a() {
        return this.f941a.getResources().getInteger(R.abc_max_action_buttons);
    }

    public boolean m2166b() {
        if (VERSION.SDK_INT < 19 && aq.m1262a(ViewConfiguration.get(this.f941a))) {
            return false;
        }
        return true;
    }

    public int m2167c() {
        return this.f941a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean m2168d() {
        if (this.f941a.getApplicationInfo().targetSdkVersion >= 16) {
            return this.f941a.getResources().getBoolean(R.abc_action_bar_embed_tabs);
        }
        return this.f941a.getResources().getBoolean(R.abc_action_bar_embed_tabs_pre_jb);
    }

    public int m2169e() {
        TypedArray obtainStyledAttributes = this.f941a.obtainStyledAttributes(null, R.ActionBar, R.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(R.ActionBar_height, 0);
        Resources resources = this.f941a.getResources();
        if (!m2168d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(R.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public boolean m2170f() {
        return this.f941a.getApplicationInfo().targetSdkVersion < 14;
    }

    public int m2171g() {
        return this.f941a.getResources().getDimensionPixelSize(R.abc_action_bar_stacked_tab_max_width);
    }
}
