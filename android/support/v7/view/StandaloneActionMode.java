package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.ActionMode.ActionMode;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

/* renamed from: android.support.v7.view.e */
public class StandaloneActionMode extends ActionMode implements MenuBuilder {
    private Context f945a;
    private ActionBarContextView f946b;
    private ActionMode f947c;
    private WeakReference<View> f948d;
    private boolean f949e;
    private boolean f950f;
    private android.support.v7.view.menu.MenuBuilder f951g;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, ActionMode actionMode, boolean z) {
        this.f945a = context;
        this.f946b = actionBarContextView;
        this.f947c = actionMode;
        this.f951g = new android.support.v7.view.menu.MenuBuilder(actionBarContextView.getContext()).m2327a(1);
        this.f951g.m2335a((MenuBuilder) this);
        this.f950f = z;
    }

    public void m2186b(CharSequence charSequence) {
        this.f946b.setTitle(charSequence);
    }

    public void m2181a(CharSequence charSequence) {
        this.f946b.setSubtitle(charSequence);
    }

    public void m2178a(int i) {
        m2186b(this.f945a.getString(i));
    }

    public void m2185b(int i) {
        m2181a(this.f945a.getString(i));
    }

    public void m2182a(boolean z) {
        super.m2079a(z);
        this.f946b.setTitleOptional(z);
    }

    public boolean m2191h() {
        return this.f946b.m2471d();
    }

    public void m2180a(View view) {
        this.f946b.setCustomView(view);
        this.f948d = view != null ? new WeakReference(view) : null;
    }

    public void m2188d() {
        this.f947c.m2021b(this, this.f951g);
    }

    public void m2187c() {
        if (!this.f949e) {
            this.f949e = true;
            this.f946b.sendAccessibilityEvent(32);
            this.f947c.m2018a(this);
        }
    }

    public Menu m2184b() {
        return this.f951g;
    }

    public CharSequence m2189f() {
        return this.f946b.getTitle();
    }

    public CharSequence m2190g() {
        return this.f946b.getSubtitle();
    }

    public View m2192i() {
        return this.f948d != null ? (View) this.f948d.get() : null;
    }

    public MenuInflater m2177a() {
        return new SupportMenuInflater(this.f946b.getContext());
    }

    public boolean m2183a(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.f947c.m2020a((ActionMode) this, menuItem);
    }

    public void m2179a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
        m2188d();
        this.f946b.m2468a();
    }
}
