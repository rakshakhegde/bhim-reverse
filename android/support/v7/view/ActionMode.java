package android.support.v7.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v7.view.b */
public abstract class ActionMode {
    private Object f897a;
    private boolean f898b;

    /* renamed from: android.support.v7.view.b.a */
    public interface ActionMode {
        void m2018a(ActionMode actionMode);

        boolean m2019a(ActionMode actionMode, Menu menu);

        boolean m2020a(ActionMode actionMode, MenuItem menuItem);

        boolean m2021b(ActionMode actionMode, Menu menu);
    }

    public abstract MenuInflater m2074a();

    public abstract void m2075a(int i);

    public abstract void m2076a(View view);

    public abstract void m2077a(CharSequence charSequence);

    public abstract Menu m2080b();

    public abstract void m2081b(int i);

    public abstract void m2082b(CharSequence charSequence);

    public abstract void m2083c();

    public abstract void m2084d();

    public abstract CharSequence m2085f();

    public abstract CharSequence m2086g();

    public abstract View m2088i();

    public void m2078a(Object obj) {
        this.f897a = obj;
    }

    public Object m2089j() {
        return this.f897a;
    }

    public void m2079a(boolean z) {
        this.f898b = z;
    }

    public boolean m2090k() {
        return this.f898b;
    }

    public boolean m2087h() {
        return false;
    }
}
