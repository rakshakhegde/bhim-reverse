package android.support.v7.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.p007c.p008a.SupportMenu;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.support.v4.p010e.SimpleArrayMap;
import android.support.v7.view.menu.MenuWrapperFactory;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@TargetApi(11)
/* renamed from: android.support.v7.view.f */
public class SupportActionModeWrapper extends ActionMode {
    final Context f956a;
    final ActionMode f957b;

    /* renamed from: android.support.v7.view.f.a */
    public static class SupportActionModeWrapper implements ActionMode.ActionMode {
        final Callback f952a;
        final Context f953b;
        final ArrayList<SupportActionModeWrapper> f954c;
        final SimpleArrayMap<Menu, Menu> f955d;

        public SupportActionModeWrapper(Context context, Callback callback) {
            this.f953b = context;
            this.f952a = callback;
            this.f954c = new ArrayList();
            this.f955d = new SimpleArrayMap();
        }

        public boolean m2195a(ActionMode actionMode, Menu menu) {
            return this.f952a.onCreateActionMode(m2197b(actionMode), m2193a(menu));
        }

        public boolean m2198b(ActionMode actionMode, Menu menu) {
            return this.f952a.onPrepareActionMode(m2197b(actionMode), m2193a(menu));
        }

        public boolean m2196a(ActionMode actionMode, MenuItem menuItem) {
            return this.f952a.onActionItemClicked(m2197b(actionMode), MenuWrapperFactory.m2444a(this.f953b, (SupportMenuItem) menuItem));
        }

        public void m2194a(ActionMode actionMode) {
            this.f952a.onDestroyActionMode(m2197b(actionMode));
        }

        private Menu m2193a(Menu menu) {
            Menu menu2 = (Menu) this.f955d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            menu2 = MenuWrapperFactory.m2443a(this.f953b, (SupportMenu) menu);
            this.f955d.put(menu, menu2);
            return menu2;
        }

        public ActionMode m2197b(ActionMode actionMode) {
            int size = this.f954c.size();
            for (int i = 0; i < size; i++) {
                SupportActionModeWrapper supportActionModeWrapper = (SupportActionModeWrapper) this.f954c.get(i);
                if (supportActionModeWrapper != null && supportActionModeWrapper.f957b == actionMode) {
                    return supportActionModeWrapper;
                }
            }
            ActionMode supportActionModeWrapper2 = new SupportActionModeWrapper(this.f953b, actionMode);
            this.f954c.add(supportActionModeWrapper2);
            return supportActionModeWrapper2;
        }
    }

    public SupportActionModeWrapper(Context context, ActionMode actionMode) {
        this.f956a = context;
        this.f957b = actionMode;
    }

    public Object getTag() {
        return this.f957b.m2089j();
    }

    public void setTag(Object obj) {
        this.f957b.m2078a(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.f957b.m2082b(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f957b.m2077a(charSequence);
    }

    public void invalidate() {
        this.f957b.m2084d();
    }

    public void finish() {
        this.f957b.m2083c();
    }

    public Menu getMenu() {
        return MenuWrapperFactory.m2443a(this.f956a, (SupportMenu) this.f957b.m2080b());
    }

    public CharSequence getTitle() {
        return this.f957b.m2085f();
    }

    public void setTitle(int i) {
        this.f957b.m2075a(i);
    }

    public CharSequence getSubtitle() {
        return this.f957b.m2086g();
    }

    public void setSubtitle(int i) {
        this.f957b.m2081b(i);
    }

    public View getCustomView() {
        return this.f957b.m2088i();
    }

    public void setCustomView(View view) {
        this.f957b.m2076a(view);
    }

    public MenuInflater getMenuInflater() {
        return this.f957b.m2074a();
    }

    public boolean getTitleOptionalHint() {
        return this.f957b.m2090k();
    }

    public void setTitleOptionalHint(boolean z) {
        this.f957b.m2079a(z);
    }

    public boolean isTitleOptional() {
        return this.f957b.m2087h();
    }
}
