package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.p007c.p008a.SupportSubMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.q */
class SubMenuWrapperICS extends MenuWrapperICS implements SubMenu {
    SubMenuWrapperICS(Context context, SupportSubMenu supportSubMenu) {
        super(context, supportSubMenu);
    }

    public SupportSubMenu m2455b() {
        return (SupportSubMenu) this.b;
    }

    public SubMenu setHeaderTitle(int i) {
        m2455b().setHeaderTitle(i);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        m2455b().setHeaderTitle(charSequence);
        return this;
    }

    public SubMenu setHeaderIcon(int i) {
        m2455b().setHeaderIcon(i);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        m2455b().setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        m2455b().setHeaderView(view);
        return this;
    }

    public void clearHeader() {
        m2455b().clearHeader();
    }

    public SubMenu setIcon(int i) {
        m2455b().setIcon(i);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        m2455b().setIcon(drawable);
        return this;
    }

    public MenuItem getItem() {
        return m2300a(m2455b().getItem());
    }
}
