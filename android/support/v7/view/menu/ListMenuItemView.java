package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuView.MenuView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class ListMenuItemView extends LinearLayout implements MenuView {
    private MenuItemImpl f1030a;
    private ImageView f1031b;
    private RadioButton f1032c;
    private TextView f1033d;
    private CheckBox f1034e;
    private TextView f1035f;
    private Drawable f1036g;
    private int f1037h;
    private Context f1038i;
    private boolean f1039j;
    private int f1040k;
    private Context f1041l;
    private LayoutInflater f1042m;
    private boolean f1043n;

    public ListMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.f1041l = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.MenuView, i, 0);
        this.f1036g = obtainStyledAttributes.getDrawable(R.MenuView_android_itemBackground);
        this.f1037h = obtainStyledAttributes.getResourceId(R.MenuView_android_itemTextAppearance, -1);
        this.f1039j = obtainStyledAttributes.getBoolean(R.MenuView_preserveIconSpacing, false);
        this.f1038i = context;
        obtainStyledAttributes.recycle();
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        setBackgroundDrawable(this.f1036g);
        this.f1033d = (TextView) findViewById(R.title);
        if (this.f1037h != -1) {
            this.f1033d.setTextAppearance(this.f1038i, this.f1037h);
        }
        this.f1035f = (TextView) findViewById(R.shortcut);
    }

    public void m2267a(MenuItemImpl menuItemImpl, int i) {
        this.f1030a = menuItemImpl;
        this.f1040k = i;
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setTitle(menuItemImpl.m2382a((MenuView) this));
        setCheckable(menuItemImpl.isCheckable());
        m2268a(menuItemImpl.m2395f(), menuItemImpl.m2391d());
        setIcon(menuItemImpl.getIcon());
        setEnabled(menuItemImpl.isEnabled());
    }

    public void setForceShowIcon(boolean z) {
        this.f1043n = z;
        this.f1039j = z;
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f1033d.setText(charSequence);
            if (this.f1033d.getVisibility() != 0) {
                this.f1033d.setVisibility(0);
            }
        } else if (this.f1033d.getVisibility() != 8) {
            this.f1033d.setVisibility(8);
        }
    }

    public MenuItemImpl getItemData() {
        return this.f1030a;
    }

    public void setCheckable(boolean z) {
        if (z || this.f1032c != null || this.f1034e != null) {
            CompoundButton compoundButton;
            CompoundButton compoundButton2;
            if (this.f1030a.m2396g()) {
                if (this.f1032c == null) {
                    m2265c();
                }
                compoundButton = this.f1032c;
                compoundButton2 = this.f1034e;
            } else {
                if (this.f1034e == null) {
                    m2266d();
                }
                compoundButton = this.f1034e;
                compoundButton2 = this.f1032c;
            }
            if (z) {
                int i;
                compoundButton.setChecked(this.f1030a.isChecked());
                if (z) {
                    i = 0;
                } else {
                    i = 8;
                }
                if (compoundButton.getVisibility() != i) {
                    compoundButton.setVisibility(i);
                }
                if (compoundButton2 != null && compoundButton2.getVisibility() != 8) {
                    compoundButton2.setVisibility(8);
                    return;
                }
                return;
            }
            if (this.f1034e != null) {
                this.f1034e.setVisibility(8);
            }
            if (this.f1032c != null) {
                this.f1032c.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.f1030a.m2396g()) {
            if (this.f1032c == null) {
                m2265c();
            }
            compoundButton = this.f1032c;
        } else {
            if (this.f1034e == null) {
                m2266d();
            }
            compoundButton = this.f1034e;
        }
        compoundButton.setChecked(z);
    }

    public void m2268a(boolean z, char c) {
        int i = (z && this.f1030a.m2395f()) ? 0 : 8;
        if (i == 0) {
            this.f1035f.setText(this.f1030a.m2393e());
        }
        if (this.f1035f.getVisibility() != i) {
            this.f1035f.setVisibility(i);
        }
    }

    public void setIcon(Drawable drawable) {
        int i = (this.f1030a.m2398i() || this.f1043n) ? 1 : 0;
        if (i == 0 && !this.f1039j) {
            return;
        }
        if (this.f1031b != null || drawable != null || this.f1039j) {
            if (this.f1031b == null) {
                m2264b();
            }
            if (drawable != null || this.f1039j) {
                ImageView imageView = this.f1031b;
                if (i == 0) {
                    drawable = null;
                }
                imageView.setImageDrawable(drawable);
                if (this.f1031b.getVisibility() != 0) {
                    this.f1031b.setVisibility(0);
                    return;
                }
                return;
            }
            this.f1031b.setVisibility(8);
        }
    }

    protected void onMeasure(int i, int i2) {
        if (this.f1031b != null && this.f1039j) {
            LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f1031b.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i, i2);
    }

    private void m2264b() {
        this.f1031b = (ImageView) getInflater().inflate(R.abc_list_menu_item_icon, this, false);
        addView(this.f1031b, 0);
    }

    private void m2265c() {
        this.f1032c = (RadioButton) getInflater().inflate(R.abc_list_menu_item_radio, this, false);
        addView(this.f1032c);
    }

    private void m2266d() {
        this.f1034e = (CheckBox) getInflater().inflate(R.abc_list_menu_item_checkbox, this, false);
        addView(this.f1034e);
    }

    public boolean m2269a() {
        return false;
    }

    private LayoutInflater getInflater() {
        if (this.f1042m == null) {
            this.f1042m = LayoutInflater.from(this.f1041l);
        }
        return this.f1042m;
    }
}
