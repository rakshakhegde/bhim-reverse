package android.support.v7.view.menu;

import android.content.Context;

/* renamed from: android.support.v7.view.menu.l */
public interface MenuPresenter {

    /* renamed from: android.support.v7.view.menu.l.a */
    public interface MenuPresenter {
        void m2013a(MenuBuilder menuBuilder, boolean z);

        boolean m2014a(MenuBuilder menuBuilder);
    }

    void m2276a(Context context, MenuBuilder menuBuilder);

    void m2277a(MenuBuilder menuBuilder, boolean z);

    boolean m2278a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);

    boolean m2279a(SubMenuBuilder subMenuBuilder);

    void m2280b(boolean z);

    boolean m2281b();

    boolean m2282b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);
}
