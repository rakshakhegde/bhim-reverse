package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.p006f.MenuItemCompat.MenuItemCompat;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.view.ActionProvider;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.a */
public class ActionMenuItem implements SupportMenuItem {
    private final int f1044a;
    private final int f1045b;
    private final int f1046c;
    private final int f1047d;
    private CharSequence f1048e;
    private CharSequence f1049f;
    private Intent f1050g;
    private char f1051h;
    private char f1052i;
    private Drawable f1053j;
    private int f1054k;
    private Context f1055l;
    private OnMenuItemClickListener f1056m;
    private int f1057n;

    public /* synthetic */ MenuItem setActionView(int i) {
        return m2270a(i);
    }

    public /* synthetic */ MenuItem setActionView(View view) {
        return m2273a(view);
    }

    public /* synthetic */ MenuItem setShowAsActionFlags(int i) {
        return m2275b(i);
    }

    public ActionMenuItem(Context context, int i, int i2, int i3, int i4, CharSequence charSequence) {
        this.f1054k = 0;
        this.f1057n = 16;
        this.f1055l = context;
        this.f1044a = i2;
        this.f1045b = i;
        this.f1046c = i3;
        this.f1047d = i4;
        this.f1048e = charSequence;
    }

    public char getAlphabeticShortcut() {
        return this.f1052i;
    }

    public int getGroupId() {
        return this.f1045b;
    }

    public Drawable getIcon() {
        return this.f1053j;
    }

    public Intent getIntent() {
        return this.f1050g;
    }

    public int getItemId() {
        return this.f1044a;
    }

    public ContextMenuInfo getMenuInfo() {
        return null;
    }

    public char getNumericShortcut() {
        return this.f1051h;
    }

    public int getOrder() {
        return this.f1047d;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public CharSequence getTitle() {
        return this.f1048e;
    }

    public CharSequence getTitleCondensed() {
        return this.f1049f != null ? this.f1049f : this.f1048e;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isCheckable() {
        return (this.f1057n & 1) != 0;
    }

    public boolean isChecked() {
        return (this.f1057n & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.f1057n & 16) != 0;
    }

    public boolean isVisible() {
        return (this.f1057n & 8) == 0;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        this.f1052i = c;
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.f1057n = (z ? 1 : 0) | (this.f1057n & -2);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.f1057n = (z ? 2 : 0) | (this.f1057n & -3);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.f1057n = (z ? 16 : 0) | (this.f1057n & -17);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1053j = drawable;
        this.f1054k = 0;
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1054k = i;
        this.f1053j = ContextCompat.m77a(this.f1055l, i);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1050g = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        this.f1051h = c;
        return this;
    }

    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.f1056m = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1051h = c;
        this.f1052i = c2;
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1048e = charSequence;
        return this;
    }

    public MenuItem setTitle(int i) {
        this.f1048e = this.f1055l.getResources().getString(i);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1049f = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        this.f1057n = (z ? 0 : 8) | (this.f1057n & 8);
        return this;
    }

    public void setShowAsAction(int i) {
    }

    public SupportMenuItem m2273a(View view) {
        throw new UnsupportedOperationException();
    }

    public View getActionView() {
        return null;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    public SupportMenuItem m2270a(int i) {
        throw new UnsupportedOperationException();
    }

    public android.support.v4.p006f.ActionProvider m2274a() {
        return null;
    }

    public SupportMenuItem m2271a(android.support.v4.p006f.ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public SupportMenuItem m2275b(int i) {
        setShowAsAction(i);
        return this;
    }

    public boolean expandActionView() {
        return false;
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    public SupportMenuItem m2272a(MenuItemCompat menuItemCompat) {
        return this;
    }
}
