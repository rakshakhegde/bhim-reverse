package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.p006f.af;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView.MenuView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.b */
public abstract class BaseMenuPresenter implements MenuPresenter {
    protected Context f1058a;
    protected Context f1059b;
    protected MenuBuilder f1060c;
    protected LayoutInflater f1061d;
    protected LayoutInflater f1062e;
    protected MenuView f1063f;
    private MenuPresenter f1064g;
    private int f1065h;
    private int f1066i;
    private int f1067j;

    public abstract void m2289a(MenuItemImpl menuItemImpl, MenuView menuView);

    public BaseMenuPresenter(Context context, int i, int i2) {
        this.f1058a = context;
        this.f1061d = LayoutInflater.from(context);
        this.f1065h = i;
        this.f1066i = i2;
    }

    public void m2287a(Context context, MenuBuilder menuBuilder) {
        this.f1059b = context;
        this.f1062e = LayoutInflater.from(this.f1059b);
        this.f1060c = menuBuilder;
    }

    public MenuView m2284a(ViewGroup viewGroup) {
        if (this.f1063f == null) {
            this.f1063f = (MenuView) this.f1061d.inflate(this.f1065h, viewGroup, false);
            this.f1063f.m2261a(this.f1060c);
            m2297b(true);
        }
        return this.f1063f;
    }

    public void m2297b(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.f1063f;
        if (viewGroup != null) {
            int i;
            if (this.f1060c != null) {
                this.f1060c.m2362j();
                ArrayList i2 = this.f1060c.m2361i();
                int size = i2.size();
                int i3 = 0;
                i = 0;
                while (i3 < size) {
                    int i4;
                    MenuItemImpl menuItemImpl = (MenuItemImpl) i2.get(i3);
                    if (m2292a(i, menuItemImpl)) {
                        View childAt = viewGroup.getChildAt(i);
                        MenuItemImpl itemData = childAt instanceof MenuView ? ((MenuView) childAt).getItemData() : null;
                        View a = m2285a(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            a.setPressed(false);
                            af.m1203o(a);
                        }
                        if (a != childAt) {
                            m2291a(a, i);
                        }
                        i4 = i + 1;
                    } else {
                        i4 = i;
                    }
                    i3++;
                    i = i4;
                }
            } else {
                i = 0;
            }
            while (i < viewGroup.getChildCount()) {
                if (!m2295a(viewGroup, i)) {
                    i++;
                }
            }
        }
    }

    protected void m2291a(View view, int i) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f1063f).addView(view, i);
    }

    protected boolean m2295a(ViewGroup viewGroup, int i) {
        viewGroup.removeViewAt(i);
        return true;
    }

    public void m2290a(MenuPresenter menuPresenter) {
        this.f1064g = menuPresenter;
    }

    public MenuPresenter m2283a() {
        return this.f1064g;
    }

    public MenuView m2296b(ViewGroup viewGroup) {
        return (MenuView) this.f1061d.inflate(this.f1066i, viewGroup, false);
    }

    public View m2285a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        MenuView menuView;
        if (view instanceof MenuView) {
            menuView = (MenuView) view;
        } else {
            menuView = m2296b(viewGroup);
        }
        m2289a(menuItemImpl, menuView);
        return (View) menuView;
    }

    public boolean m2292a(int i, MenuItemImpl menuItemImpl) {
        return true;
    }

    public void m2288a(MenuBuilder menuBuilder, boolean z) {
        if (this.f1064g != null) {
            this.f1064g.m2013a(menuBuilder, z);
        }
    }

    public boolean m2294a(SubMenuBuilder subMenuBuilder) {
        if (this.f1064g != null) {
            return this.f1064g.m2014a(subMenuBuilder);
        }
        return false;
    }

    public boolean m2298b() {
        return false;
    }

    public boolean m2293a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean m2299b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void m2286a(int i) {
        this.f1067j = i;
    }
}
