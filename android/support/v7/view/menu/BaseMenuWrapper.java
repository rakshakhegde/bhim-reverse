package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.support.v4.p007c.p008a.SupportSubMenu;
import android.support.v4.p010e.ArrayMap;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

/* renamed from: android.support.v7.view.menu.c */
abstract class BaseMenuWrapper<T> extends BaseWrapper<T> {
    final Context f1069a;
    private Map<SupportMenuItem, MenuItem> f1070c;
    private Map<SupportSubMenu, SubMenu> f1071d;

    BaseMenuWrapper(Context context, T t) {
        super(t);
        this.f1069a = context;
    }

    final MenuItem m2300a(MenuItem menuItem) {
        if (!(menuItem instanceof SupportMenuItem)) {
            return menuItem;
        }
        SupportMenuItem supportMenuItem = (SupportMenuItem) menuItem;
        if (this.f1070c == null) {
            this.f1070c = new ArrayMap();
        }
        MenuItem menuItem2 = (MenuItem) this.f1070c.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        menuItem2 = MenuWrapperFactory.m2444a(this.f1069a, supportMenuItem);
        this.f1070c.put(supportMenuItem, menuItem2);
        return menuItem2;
    }

    final SubMenu m2301a(SubMenu subMenu) {
        if (!(subMenu instanceof SupportSubMenu)) {
            return subMenu;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu;
        if (this.f1071d == null) {
            this.f1071d = new ArrayMap();
        }
        SubMenu subMenu2 = (SubMenu) this.f1071d.get(supportSubMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        subMenu2 = MenuWrapperFactory.m2445a(this.f1069a, supportSubMenu);
        this.f1071d.put(supportSubMenu, subMenu2);
        return subMenu2;
    }

    final void m2302a() {
        if (this.f1070c != null) {
            this.f1070c.clear();
        }
        if (this.f1071d != null) {
            this.f1071d.clear();
        }
    }

    final void m2303a(int i) {
        if (this.f1070c != null) {
            Iterator it = this.f1070c.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    final void m2304b(int i) {
        if (this.f1070c != null) {
            Iterator it = this.f1070c.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
