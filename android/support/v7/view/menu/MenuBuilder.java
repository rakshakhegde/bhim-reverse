package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.p006f.ActionProvider;
import android.support.v4.p006f.MenuItemCompat;
import android.support.v4.p007c.p008a.SupportMenu;
import android.support.v7.p014b.R.R;
import android.util.SparseArray;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyCharacterMap.KeyData;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: android.support.v7.view.menu.f */
public class MenuBuilder implements SupportMenu {
    private static final int[] f1083d;
    CharSequence f1084a;
    Drawable f1085b;
    View f1086c;
    private final Context f1087e;
    private final Resources f1088f;
    private boolean f1089g;
    private boolean f1090h;
    private MenuBuilder f1091i;
    private ArrayList<MenuItemImpl> f1092j;
    private ArrayList<MenuItemImpl> f1093k;
    private boolean f1094l;
    private ArrayList<MenuItemImpl> f1095m;
    private ArrayList<MenuItemImpl> f1096n;
    private boolean f1097o;
    private int f1098p;
    private ContextMenuInfo f1099q;
    private boolean f1100r;
    private boolean f1101s;
    private boolean f1102t;
    private boolean f1103u;
    private ArrayList<MenuItemImpl> f1104v;
    private CopyOnWriteArrayList<WeakReference<MenuPresenter>> f1105w;
    private MenuItemImpl f1106x;
    private boolean f1107y;

    /* renamed from: android.support.v7.view.menu.f.a */
    public interface MenuBuilder {
        void m1919a(MenuBuilder menuBuilder);

        boolean m1920a(MenuBuilder menuBuilder, MenuItem menuItem);
    }

    /* renamed from: android.support.v7.view.menu.f.b */
    public interface MenuBuilder {
        boolean m2260a(MenuItemImpl menuItemImpl);
    }

    static {
        f1083d = new int[]{1, 4, 5, 3, 2, 0};
    }

    public MenuBuilder(Context context) {
        this.f1098p = 0;
        this.f1100r = false;
        this.f1101s = false;
        this.f1102t = false;
        this.f1103u = false;
        this.f1104v = new ArrayList();
        this.f1105w = new CopyOnWriteArrayList();
        this.f1087e = context;
        this.f1088f = context.getResources();
        this.f1092j = new ArrayList();
        this.f1093k = new ArrayList();
        this.f1094l = true;
        this.f1095m = new ArrayList();
        this.f1096n = new ArrayList();
        this.f1097o = true;
        m2325e(true);
    }

    public MenuBuilder m2327a(int i) {
        this.f1098p = i;
        return this;
    }

    public void m2337a(MenuPresenter menuPresenter) {
        m2338a(menuPresenter, this.f1087e);
    }

    public void m2338a(MenuPresenter menuPresenter, Context context) {
        this.f1105w.add(new WeakReference(menuPresenter));
        menuPresenter.m2276a(context, this);
        this.f1097o = true;
    }

    public void m2348b(MenuPresenter menuPresenter) {
        Iterator it = this.f1105w.iterator();
        while (it.hasNext()) {
            WeakReference weakReference = (WeakReference) it.next();
            MenuPresenter menuPresenter2 = (MenuPresenter) weakReference.get();
            if (menuPresenter2 == null || menuPresenter2 == menuPresenter) {
                this.f1105w.remove(weakReference);
            }
        }
    }

    private void m2324d(boolean z) {
        if (!this.f1105w.isEmpty()) {
            m2359g();
            Iterator it = this.f1105w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                MenuPresenter menuPresenter = (MenuPresenter) weakReference.get();
                if (menuPresenter == null) {
                    this.f1105w.remove(weakReference);
                } else {
                    menuPresenter.m2280b(z);
                }
            }
            m2360h();
        }
    }

    private boolean m2322a(SubMenuBuilder subMenuBuilder, MenuPresenter menuPresenter) {
        boolean z = false;
        if (this.f1105w.isEmpty()) {
            return false;
        }
        if (menuPresenter != null) {
            z = menuPresenter.m2279a(subMenuBuilder);
        }
        Iterator it = this.f1105w.iterator();
        boolean z2 = z;
        while (it.hasNext()) {
            WeakReference weakReference = (WeakReference) it.next();
            MenuPresenter menuPresenter2 = (MenuPresenter) weakReference.get();
            if (menuPresenter2 == null) {
                this.f1105w.remove(weakReference);
                z = z2;
            } else if (z2) {
                z = z2;
            } else {
                z = menuPresenter2.m2279a(subMenuBuilder);
            }
            z2 = z;
        }
        return z2;
    }

    public void m2334a(Bundle bundle) {
        int size = size();
        int i = 0;
        SparseArray sparseArray = null;
        while (i < size) {
            MenuItem item = getItem(i);
            View a = MenuItemCompat.m1478a(item);
            if (!(a == null || a.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                a.saveHierarchyState(sparseArray);
                if (MenuItemCompat.m1482c(item)) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            SparseArray sparseArray2 = sparseArray;
            if (item.hasSubMenu()) {
                ((SubMenuBuilder) item.getSubMenu()).m2334a(bundle);
            }
            i++;
            sparseArray = sparseArray2;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(m2333a(), sparseArray);
        }
    }

    public void m2346b(Bundle bundle) {
        if (bundle != null) {
            MenuItem item;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(m2333a());
            int size = size();
            for (int i = 0; i < size; i++) {
                item = getItem(i);
                View a = MenuItemCompat.m1478a(item);
                if (!(a == null || a.getId() == -1)) {
                    a.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((SubMenuBuilder) item.getSubMenu()).m2346b(bundle);
                }
            }
            int i2 = bundle.getInt("android:menu:expandedactionview");
            if (i2 > 0) {
                item = findItem(i2);
                if (item != null) {
                    MenuItemCompat.m1481b(item);
                }
            }
        }
    }

    protected String m2333a() {
        return "android:menu:actionviewstates";
    }

    public void m2335a(MenuBuilder menuBuilder) {
        this.f1091i = menuBuilder;
    }

    protected MenuItem m2332a(int i, int i2, int i3, CharSequence charSequence) {
        int d = MenuBuilder.m2323d(i3);
        MenuItem a = m2319a(i, i2, i3, d, charSequence, this.f1098p);
        if (this.f1099q != null) {
            a.m2384a(this.f1099q);
        }
        this.f1092j.add(MenuBuilder.m2318a(this.f1092j, d), a);
        m2349b(true);
        return a;
    }

    private MenuItemImpl m2319a(int i, int i2, int i3, int i4, CharSequence charSequence, int i5) {
        return new MenuItemImpl(this, i, i2, i3, i4, charSequence, i5);
    }

    public MenuItem add(CharSequence charSequence) {
        return m2332a(0, 0, 0, charSequence);
    }

    public MenuItem add(int i) {
        return m2332a(0, 0, 0, this.f1088f.getString(i));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return m2332a(i, i2, i3, charSequence);
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return m2332a(i, i2, i3, this.f1088f.getString(i4));
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    public SubMenu addSubMenu(int i) {
        return addSubMenu(0, 0, 0, this.f1088f.getString(i));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        MenuItemImpl menuItemImpl = (MenuItemImpl) m2332a(i, i2, i3, charSequence);
        SubMenuBuilder subMenuBuilder = new SubMenuBuilder(this.f1087e, this, menuItemImpl);
        menuItemImpl.m2383a(subMenuBuilder);
        return subMenuBuilder;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return addSubMenu(i, i2, i3, this.f1088f.getString(i4));
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.f1087e.getPackageManager();
        List queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i4 & 1) == 0) {
            removeGroup(i);
        }
        for (int i5 = 0; i5 < size; i5++) {
            Intent intent2;
            ResolveInfo resolveInfo = (ResolveInfo) queryIntentActivityOptions.get(i5);
            if (resolveInfo.specificIndex < 0) {
                intent2 = intent;
            } else {
                intent2 = intentArr[resolveInfo.specificIndex];
            }
            Intent intent3 = new Intent(intent2);
            intent3.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent4 = add(i, i2, i3, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent4;
            }
        }
        return size;
    }

    public void removeItem(int i) {
        m2321a(m2345b(i), true);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeGroup(int r6) {
        /*
        r5 = this;
        r1 = 0;
        r3 = r5.m2351c(r6);
        if (r3 < 0) goto L_0x002b;
    L_0x0007:
        r0 = r5.f1092j;
        r0 = r0.size();
        r4 = r0 - r3;
        r0 = r1;
    L_0x0010:
        r2 = r0 + 1;
        if (r0 >= r4) goto L_0x0027;
    L_0x0014:
        r0 = r5.f1092j;
        r0 = r0.get(r3);
        r0 = (android.support.v7.view.menu.MenuItemImpl) r0;
        r0 = r0.getGroupId();
        if (r0 != r6) goto L_0x0027;
    L_0x0022:
        r5.m2321a(r3, r1);
        r0 = r2;
        goto L_0x0010;
    L_0x0027:
        r0 = 1;
        r5.m2349b(r0);
    L_0x002b:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.f.removeGroup(int):void");
    }

    private void m2321a(int i, boolean z) {
        if (i >= 0 && i < this.f1092j.size()) {
            this.f1092j.remove(i);
            if (z) {
                m2349b(true);
            }
        }
    }

    public void clear() {
        if (this.f1106x != null) {
            m2356d(this.f1106x);
        }
        this.f1092j.clear();
        m2349b(true);
    }

    void m2339a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.f1092j.size();
        for (int i = 0; i < size; i++) {
            MenuItem menuItem2 = (MenuItemImpl) this.f1092j.get(i);
            if (menuItem2.getGroupId() == groupId && menuItem2.m2396g() && menuItem2.isCheckable()) {
                menuItem2.m2387b(menuItem2 == menuItem);
            }
        }
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
        int size = this.f1092j.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i2);
            if (menuItemImpl.getGroupId() == i) {
                menuItemImpl.m2385a(z2);
                menuItemImpl.setCheckable(z);
            }
        }
    }

    public void setGroupVisible(int i, boolean z) {
        int size = this.f1092j.size();
        int i2 = 0;
        boolean z2 = false;
        while (i2 < size) {
            boolean z3;
            MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i2);
            if (menuItemImpl.getGroupId() == i && menuItemImpl.m2390c(z)) {
                z3 = true;
            } else {
                z3 = z2;
            }
            i2++;
            z2 = z3;
        }
        if (z2) {
            m2349b(true);
        }
    }

    public void setGroupEnabled(int i, boolean z) {
        int size = this.f1092j.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i2);
            if (menuItemImpl.getGroupId() == i) {
                menuItemImpl.setEnabled(z);
            }
        }
    }

    public boolean hasVisibleItems() {
        if (this.f1107y) {
            return true;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (((MenuItemImpl) this.f1092j.get(i)).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public MenuItem findItem(int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i2);
            if (menuItemImpl.getItemId() == i) {
                return menuItemImpl;
            }
            if (menuItemImpl.hasSubMenu()) {
                MenuItem findItem = menuItemImpl.getSubMenu().findItem(i);
                if (findItem != null) {
                    return findItem;
                }
            }
        }
        return null;
    }

    public int m2345b(int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((MenuItemImpl) this.f1092j.get(i2)).getItemId() == i) {
                return i2;
            }
        }
        return -1;
    }

    public int m2351c(int i) {
        return m2326a(i, 0);
    }

    public int m2326a(int i, int i2) {
        int size = size();
        if (i2 < 0) {
            i2 = 0;
        }
        for (int i3 = i2; i3 < size; i3++) {
            if (((MenuItemImpl) this.f1092j.get(i3)).getGroupId() == i) {
                return i3;
            }
        }
        return -1;
    }

    public int size() {
        return this.f1092j.size();
    }

    public MenuItem getItem(int i) {
        return (MenuItem) this.f1092j.get(i);
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return m2331a(i, keyEvent) != null;
    }

    public void setQwertyMode(boolean z) {
        this.f1089g = z;
        m2349b(false);
    }

    private static int m2323d(int i) {
        int i2 = (-65536 & i) >> 16;
        if (i2 >= 0 && i2 < f1083d.length) {
            return (f1083d[i2] << 16) | (65535 & i);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    boolean m2350b() {
        return this.f1089g;
    }

    private void m2325e(boolean z) {
        boolean z2 = true;
        if (!(z && this.f1088f.getConfiguration().keyboard != 1 && this.f1088f.getBoolean(R.abc_config_showMenuShortcutsWhenKeyboardPresent))) {
            z2 = false;
        }
        this.f1090h = z2;
    }

    public boolean m2353c() {
        return this.f1090h;
    }

    Resources m2355d() {
        return this.f1088f;
    }

    public Context m2357e() {
        return this.f1087e;
    }

    boolean m2342a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.f1091i != null && this.f1091i.m1920a(menuBuilder, menuItem);
    }

    public void m2358f() {
        if (this.f1091i != null) {
            this.f1091i.m1919a(this);
        }
    }

    private static int m2318a(ArrayList<MenuItemImpl> arrayList, int i) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((MenuItemImpl) arrayList.get(size)).m2389c() <= i) {
                return size + 1;
            }
        }
        return 0;
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        MenuItem a = m2331a(i, keyEvent);
        boolean z = false;
        if (a != null) {
            z = m2343a(a, i2);
        }
        if ((i2 & 2) != 0) {
            m2341a(true);
        }
        return z;
    }

    void m2340a(List<MenuItemImpl> list, int i, KeyEvent keyEvent) {
        boolean b = m2350b();
        int metaState = keyEvent.getMetaState();
        KeyData keyData = new KeyData();
        if (keyEvent.getKeyData(keyData) || i == 67) {
            int size = this.f1092j.size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i2);
                if (menuItemImpl.hasSubMenu()) {
                    ((MenuBuilder) menuItemImpl.getSubMenu()).m2340a((List) list, i, keyEvent);
                }
                char alphabeticShortcut = b ? menuItemImpl.getAlphabeticShortcut() : menuItemImpl.getNumericShortcut();
                if ((metaState & 5) == 0 && alphabeticShortcut != '\u0000' && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (b && alphabeticShortcut == '\b' && i == 67)) && menuItemImpl.isEnabled())) {
                    list.add(menuItemImpl);
                }
            }
        }
    }

    MenuItemImpl m2331a(int i, KeyEvent keyEvent) {
        List list = this.f1104v;
        list.clear();
        m2340a(list, i, keyEvent);
        if (list.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyData keyData = new KeyData();
        keyEvent.getKeyData(keyData);
        int size = list.size();
        if (size == 1) {
            return (MenuItemImpl) list.get(0);
        }
        boolean b = m2350b();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) list.get(i2);
            char alphabeticShortcut = b ? menuItemImpl.getAlphabeticShortcut() : menuItemImpl.getNumericShortcut();
            if (alphabeticShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return menuItemImpl;
            }
            if (alphabeticShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return menuItemImpl;
            }
            if (b && alphabeticShortcut == '\b' && i == 67) {
                return menuItemImpl;
            }
        }
        return null;
    }

    public boolean performIdentifierAction(int i, int i2) {
        return m2343a(findItem(i), i2);
    }

    public boolean m2343a(MenuItem menuItem, int i) {
        return m2344a(menuItem, null, i);
    }

    public boolean m2344a(MenuItem menuItem, MenuPresenter menuPresenter, int i) {
        MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
        if (menuItemImpl == null || !menuItemImpl.isEnabled()) {
            return false;
        }
        boolean z;
        boolean b = menuItemImpl.m2388b();
        ActionProvider a = menuItemImpl.m2381a();
        if (a == null || !a.m1415e()) {
            z = false;
        } else {
            z = true;
        }
        boolean expandActionView;
        if (menuItemImpl.m2403n()) {
            expandActionView = menuItemImpl.expandActionView() | b;
            if (!expandActionView) {
                return expandActionView;
            }
            m2341a(true);
            return expandActionView;
        } else if (menuItemImpl.hasSubMenu() || z) {
            m2341a(false);
            if (!menuItemImpl.hasSubMenu()) {
                menuItemImpl.m2383a(new SubMenuBuilder(m2357e(), this, menuItemImpl));
            }
            SubMenuBuilder subMenuBuilder = (SubMenuBuilder) menuItemImpl.getSubMenu();
            if (z) {
                a.m1410a((SubMenu) subMenuBuilder);
            }
            expandActionView = m2322a(subMenuBuilder, menuPresenter) | b;
            if (expandActionView) {
                return expandActionView;
            }
            m2341a(true);
            return expandActionView;
        } else {
            if ((i & 1) == 0) {
                m2341a(true);
            }
            return b;
        }
    }

    public final void m2341a(boolean z) {
        if (!this.f1103u) {
            this.f1103u = true;
            Iterator it = this.f1105w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                MenuPresenter menuPresenter = (MenuPresenter) weakReference.get();
                if (menuPresenter == null) {
                    this.f1105w.remove(weakReference);
                } else {
                    menuPresenter.m2277a(this, z);
                }
            }
            this.f1103u = false;
        }
    }

    public void close() {
        m2341a(true);
    }

    public void m2349b(boolean z) {
        if (this.f1100r) {
            this.f1101s = true;
            return;
        }
        if (z) {
            this.f1094l = true;
            this.f1097o = true;
        }
        m2324d(z);
    }

    public void m2359g() {
        if (!this.f1100r) {
            this.f1100r = true;
            this.f1101s = false;
        }
    }

    public void m2360h() {
        this.f1100r = false;
        if (this.f1101s) {
            this.f1101s = false;
            m2349b(true);
        }
    }

    void m2336a(MenuItemImpl menuItemImpl) {
        this.f1094l = true;
        m2349b(true);
    }

    void m2347b(MenuItemImpl menuItemImpl) {
        this.f1097o = true;
        m2349b(true);
    }

    public ArrayList<MenuItemImpl> m2361i() {
        if (!this.f1094l) {
            return this.f1093k;
        }
        this.f1093k.clear();
        int size = this.f1092j.size();
        for (int i = 0; i < size; i++) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) this.f1092j.get(i);
            if (menuItemImpl.isVisible()) {
                this.f1093k.add(menuItemImpl);
            }
        }
        this.f1094l = false;
        this.f1097o = true;
        return this.f1093k;
    }

    public void m2362j() {
        ArrayList i = m2361i();
        if (this.f1097o) {
            Iterator it = this.f1105w.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                int i3;
                WeakReference weakReference = (WeakReference) it.next();
                MenuPresenter menuPresenter = (MenuPresenter) weakReference.get();
                if (menuPresenter == null) {
                    this.f1105w.remove(weakReference);
                    i3 = i2;
                } else {
                    i3 = menuPresenter.m2281b() | i2;
                }
                i2 = i3;
            }
            if (i2 != 0) {
                this.f1095m.clear();
                this.f1096n.clear();
                i2 = i.size();
                for (int i4 = 0; i4 < i2; i4++) {
                    MenuItemImpl menuItemImpl = (MenuItemImpl) i.get(i4);
                    if (menuItemImpl.m2399j()) {
                        this.f1095m.add(menuItemImpl);
                    } else {
                        this.f1096n.add(menuItemImpl);
                    }
                }
            } else {
                this.f1095m.clear();
                this.f1096n.clear();
                this.f1096n.addAll(m2361i());
            }
            this.f1097o = false;
        }
    }

    public ArrayList<MenuItemImpl> m2363k() {
        m2362j();
        return this.f1095m;
    }

    public ArrayList<MenuItemImpl> m2364l() {
        m2362j();
        return this.f1096n;
    }

    public void clearHeader() {
        this.f1085b = null;
        this.f1084a = null;
        this.f1086c = null;
        m2349b(false);
    }

    private void m2320a(int i, CharSequence charSequence, int i2, Drawable drawable, View view) {
        Resources d = m2355d();
        if (view != null) {
            this.f1086c = view;
            this.f1084a = null;
            this.f1085b = null;
        } else {
            if (i > 0) {
                this.f1084a = d.getText(i);
            } else if (charSequence != null) {
                this.f1084a = charSequence;
            }
            if (i2 > 0) {
                this.f1085b = ContextCompat.m77a(m2357e(), i2);
            } else if (drawable != null) {
                this.f1085b = drawable;
            }
            this.f1086c = null;
        }
        m2349b(false);
    }

    protected MenuBuilder m2330a(CharSequence charSequence) {
        m2320a(0, charSequence, 0, null, null);
        return this;
    }

    protected MenuBuilder m2328a(Drawable drawable) {
        m2320a(0, null, 0, drawable, null);
        return this;
    }

    protected MenuBuilder m2329a(View view) {
        m2320a(0, null, 0, null, view);
        return this;
    }

    public CharSequence m2365m() {
        return this.f1084a;
    }

    public Drawable m2366n() {
        return this.f1085b;
    }

    public View m2367o() {
        return this.f1086c;
    }

    public MenuBuilder m2368p() {
        return this;
    }

    boolean m2369q() {
        return this.f1102t;
    }

    public boolean m2354c(MenuItemImpl menuItemImpl) {
        boolean z = false;
        if (!this.f1105w.isEmpty()) {
            m2359g();
            Iterator it = this.f1105w.iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                MenuPresenter menuPresenter = (MenuPresenter) weakReference.get();
                if (menuPresenter == null) {
                    this.f1105w.remove(weakReference);
                    z = z2;
                } else {
                    z = menuPresenter.m2278a(this, menuItemImpl);
                    if (z) {
                        break;
                    }
                }
                z2 = z;
            }
            z = z2;
            m2360h();
            if (z) {
                this.f1106x = menuItemImpl;
            }
        }
        return z;
    }

    public boolean m2356d(MenuItemImpl menuItemImpl) {
        boolean z = false;
        if (!this.f1105w.isEmpty() && this.f1106x == menuItemImpl) {
            m2359g();
            Iterator it = this.f1105w.iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                MenuPresenter menuPresenter = (MenuPresenter) weakReference.get();
                if (menuPresenter == null) {
                    this.f1105w.remove(weakReference);
                    z = z2;
                } else {
                    z = menuPresenter.m2282b(this, menuItemImpl);
                    if (z) {
                        break;
                    }
                }
                z2 = z;
            }
            z = z2;
            m2360h();
            if (z) {
                this.f1106x = null;
            }
        }
        return z;
    }

    public MenuItemImpl m2370r() {
        return this.f1106x;
    }

    public void m2352c(boolean z) {
        this.f1107y = z;
    }
}
