package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.widget.ar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements MenuBuilder, MenuView, OnItemClickListener {
    private static final int[] f1027a;
    private MenuBuilder f1028b;
    private int f1029c;

    static {
        f1027a = new int[]{16842964, 16843049};
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842868);
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        setOnItemClickListener(this);
        ar a = ar.m2811a(context, attributeSet, f1027a, i, 0);
        if (a.m2826f(0)) {
            setBackgroundDrawable(a.m2814a(0));
        }
        if (a.m2826f(1)) {
            setDivider(a.m2814a(1));
        }
        a.m2815a();
    }

    public void m2262a(MenuBuilder menuBuilder) {
        this.f1028b = menuBuilder;
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    public boolean m2263a(MenuItemImpl menuItemImpl) {
        return this.f1028b.m2343a((MenuItem) menuItemImpl, 0);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        m2263a((MenuItemImpl) getAdapter().getItem(i));
    }

    public int getWindowAnimations() {
        return this.f1029c;
    }
}
