package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.af;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.view.menu.MenuView.MenuView;
import android.support.v7.widget.ActionMenuView.C0008a;
import android.support.v7.widget.aa;
import android.support.v7.widget.ag;
import android.support.v7.widget.ag.ListPopupWindow;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Toast;

public class ActionMenuItemView extends aa implements MenuView, C0008a, OnClickListener, OnLongClickListener {
    private MenuItemImpl f1016a;
    private CharSequence f1017b;
    private Drawable f1018c;
    private MenuBuilder f1019d;
    private ListPopupWindow f1020e;
    private C0007b f1021f;
    private boolean f1022g;
    private boolean f1023h;
    private int f1024i;
    private int f1025j;
    private int f1026k;

    /* renamed from: android.support.v7.view.menu.ActionMenuItemView.a */
    private class C0006a extends ListPopupWindow {
        final /* synthetic */ ActionMenuItemView f1012a;

        public C0006a(ActionMenuItemView actionMenuItemView) {
            this.f1012a = actionMenuItemView;
            super(actionMenuItemView);
        }

        public ag m2244a() {
            if (this.f1012a.f1021f != null) {
                return this.f1012a.f1021f.m2246a();
            }
            return null;
        }

        protected boolean m2245b() {
            if (this.f1012a.f1019d == null || !this.f1012a.f1019d.m2260a(this.f1012a.f1016a)) {
                return false;
            }
            ag a = m2244a();
            if (a == null || !a.m2755k()) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: android.support.v7.view.menu.ActionMenuItemView.b */
    public static abstract class C0007b {
        public abstract ag m2246a();
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Resources resources = context.getResources();
        this.f1022g = resources.getBoolean(R.abc_config_allowActionMenuItemTextWithIcon);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.ActionMenuItemView, i, 0);
        this.f1024i = obtainStyledAttributes.getDimensionPixelSize(R.ActionMenuItemView_android_minWidth, 0);
        obtainStyledAttributes.recycle();
        this.f1026k = (int) ((resources.getDisplayMetrics().density * 32.0f) + 0.5f);
        setOnClickListener(this);
        setOnLongClickListener(this);
        this.f1025j = -1;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        this.f1022g = getContext().getResources().getBoolean(R.abc_config_allowActionMenuItemTextWithIcon);
        m2254e();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f1025j = i;
        super.setPadding(i, i2, i3, i4);
    }

    public MenuItemImpl getItemData() {
        return this.f1016a;
    }

    public void m2255a(MenuItemImpl menuItemImpl, int i) {
        this.f1016a = menuItemImpl;
        setIcon(menuItemImpl.getIcon());
        setTitle(menuItemImpl.m2382a((MenuView) this));
        setId(menuItemImpl.getItemId());
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setEnabled(menuItemImpl.isEnabled());
        if (menuItemImpl.hasSubMenu() && this.f1020e == null) {
            this.f1020e = new C0006a(this);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f1016a.hasSubMenu() && this.f1020e != null && this.f1020e.onTouch(this, motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void onClick(View view) {
        if (this.f1019d != null) {
            this.f1019d.m2260a(this.f1016a);
        }
    }

    public void setItemInvoker(MenuBuilder menuBuilder) {
        this.f1019d = menuBuilder;
    }

    public void setPopupCallback(C0007b c0007b) {
        this.f1021f = c0007b;
    }

    public boolean m2256a() {
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        if (this.f1023h != z) {
            this.f1023h = z;
            if (this.f1016a != null) {
                this.f1016a.m2397h();
            }
        }
    }

    private void m2254e() {
        int i = 0;
        int i2 = !TextUtils.isEmpty(this.f1017b) ? 1 : 0;
        if (this.f1018c == null || (this.f1016a.m2402m() && (this.f1022g || this.f1023h))) {
            i = 1;
        }
        setText((i2 & i) != 0 ? this.f1017b : null);
    }

    public void setIcon(Drawable drawable) {
        this.f1018c = drawable;
        if (drawable != null) {
            float f;
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth > this.f1026k) {
                f = ((float) this.f1026k) / ((float) intrinsicWidth);
                intrinsicWidth = this.f1026k;
                intrinsicHeight = (int) (((float) intrinsicHeight) * f);
            }
            if (intrinsicHeight > this.f1026k) {
                f = ((float) this.f1026k) / ((float) intrinsicHeight);
                intrinsicHeight = this.f1026k;
                intrinsicWidth = (int) (((float) intrinsicWidth) * f);
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable, null, null, null);
        m2254e();
    }

    public boolean m2257b() {
        return !TextUtils.isEmpty(getText());
    }

    public void setTitle(CharSequence charSequence) {
        this.f1017b = charSequence;
        setContentDescription(this.f1017b);
        m2254e();
    }

    public boolean m2258c() {
        return m2257b() && this.f1016a.getIcon() == null;
    }

    public boolean m2259d() {
        return m2257b();
    }

    public boolean onLongClick(View view) {
        if (m2257b()) {
            return false;
        }
        int[] iArr = new int[2];
        Rect rect = new Rect();
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i = iArr[1] + (height / 2);
        width = (width / 2) + iArr[0];
        if (af.m1191d(view) == 0) {
            width = context.getResources().getDisplayMetrics().widthPixels - width;
        }
        Toast makeText = Toast.makeText(context, this.f1016a.getTitle(), 0);
        if (i < rect.height()) {
            makeText.setGravity(8388661, width, (iArr[1] + height) - rect.top);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
        return true;
    }

    protected void onMeasure(int i, int i2) {
        boolean b = m2257b();
        if (b && this.f1025j >= 0) {
            super.setPadding(this.f1025j, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i, i2);
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int measuredWidth = getMeasuredWidth();
        size = mode == Integer.MIN_VALUE ? Math.min(size, this.f1024i) : this.f1024i;
        if (mode != 1073741824 && this.f1024i > 0 && measuredWidth < size) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(size, 1073741824), i2);
        }
        if (!b && this.f1018c != null) {
            super.setPadding((getMeasuredWidth() - this.f1018c.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }
}
