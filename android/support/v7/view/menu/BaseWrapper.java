package android.support.v7.view.menu;

/* renamed from: android.support.v7.view.menu.d */
class BaseWrapper<T> {
    final T f1068b;

    BaseWrapper(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.f1068b = t;
    }
}
