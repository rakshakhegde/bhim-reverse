package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView.MenuView;
import android.support.v7.widget.ag;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow.OnDismissListener;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.k */
public class MenuPopupHelper implements MenuPresenter, OnKeyListener, OnGlobalLayoutListener, OnItemClickListener, OnDismissListener {
    static final int f1150a;
    boolean f1151b;
    private final Context f1152c;
    private final LayoutInflater f1153d;
    private final MenuBuilder f1154e;
    private final MenuPopupHelper f1155f;
    private final boolean f1156g;
    private final int f1157h;
    private final int f1158i;
    private final int f1159j;
    private View f1160k;
    private ag f1161l;
    private ViewTreeObserver f1162m;
    private MenuPresenter f1163n;
    private ViewGroup f1164o;
    private boolean f1165p;
    private int f1166q;
    private int f1167r;

    /* renamed from: android.support.v7.view.menu.k.a */
    private class MenuPopupHelper extends BaseAdapter {
        final /* synthetic */ MenuPopupHelper f1147a;
        private MenuBuilder f1148b;
        private int f1149c;

        public /* synthetic */ Object getItem(int i) {
            return m2421a(i);
        }

        public MenuPopupHelper(MenuPopupHelper menuPopupHelper, MenuBuilder menuBuilder) {
            this.f1147a = menuPopupHelper;
            this.f1149c = -1;
            this.f1148b = menuBuilder;
            m2422a();
        }

        public int getCount() {
            ArrayList l = this.f1147a.f1156g ? this.f1148b.m2364l() : this.f1148b.m2361i();
            if (this.f1149c < 0) {
                return l.size();
            }
            return l.size() - 1;
        }

        public MenuItemImpl m2421a(int i) {
            ArrayList l = this.f1147a.f1156g ? this.f1148b.m2364l() : this.f1148b.m2361i();
            if (this.f1149c >= 0 && i >= this.f1149c) {
                i++;
            }
            return (MenuItemImpl) l.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate;
            if (view == null) {
                inflate = this.f1147a.f1153d.inflate(MenuPopupHelper.f1150a, viewGroup, false);
            } else {
                inflate = view;
            }
            MenuView menuView = (MenuView) inflate;
            if (this.f1147a.f1151b) {
                ((ListMenuItemView) inflate).setForceShowIcon(true);
            }
            menuView.m2247a(m2421a(i), 0);
            return inflate;
        }

        void m2422a() {
            MenuItemImpl r = this.f1147a.f1154e.m2370r();
            if (r != null) {
                ArrayList l = this.f1147a.f1154e.m2364l();
                int size = l.size();
                for (int i = 0; i < size; i++) {
                    if (((MenuItemImpl) l.get(i)) == r) {
                        this.f1149c = i;
                        return;
                    }
                }
            }
            this.f1149c = -1;
        }

        public void notifyDataSetChanged() {
            m2422a();
            super.notifyDataSetChanged();
        }
    }

    static {
        f1150a = R.abc_popup_menu_item_layout;
    }

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view) {
        this(context, menuBuilder, view, false, R.popupMenuStyle);
    }

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view, boolean z, int i) {
        this(context, menuBuilder, view, z, i, 0);
    }

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view, boolean z, int i, int i2) {
        this.f1167r = 0;
        this.f1152c = context;
        this.f1153d = LayoutInflater.from(context);
        this.f1154e = menuBuilder;
        this.f1155f = new MenuPopupHelper(this, this.f1154e);
        this.f1156g = z;
        this.f1158i = i;
        this.f1159j = i2;
        Resources resources = context.getResources();
        this.f1157h = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.abc_config_prefDialogWidth));
        this.f1160k = view;
        menuBuilder.m2338a((MenuPresenter) this, context);
    }

    public void m2432a(View view) {
        this.f1160k = view;
    }

    public void m2433a(boolean z) {
        this.f1151b = z;
    }

    public void m2428a(int i) {
        this.f1167r = i;
    }

    public void m2427a() {
        if (!m2440d()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public ag m2439c() {
        return this.f1161l;
    }

    public boolean m2440d() {
        boolean z = false;
        this.f1161l = new ag(this.f1152c, null, this.f1158i, this.f1159j);
        this.f1161l.m2738a((OnDismissListener) this);
        this.f1161l.m2736a((OnItemClickListener) this);
        this.f1161l.m2737a(this.f1155f);
        this.f1161l.m2739a(true);
        View view = this.f1160k;
        if (view == null) {
            return false;
        }
        if (this.f1162m == null) {
            z = true;
        }
        this.f1162m = view.getViewTreeObserver();
        if (z) {
            this.f1162m.addOnGlobalLayoutListener(this);
        }
        this.f1161l.m2735a(view);
        this.f1161l.m2744d(this.f1167r);
        if (!this.f1165p) {
            this.f1166q = m2426g();
            this.f1165p = true;
        }
        this.f1161l.m2748f(this.f1166q);
        this.f1161l.m2750g(2);
        this.f1161l.m2741c();
        this.f1161l.m2757m().setOnKeyListener(this);
        return true;
    }

    public void m2441e() {
        if (m2442f()) {
            this.f1161l.m2753i();
        }
    }

    public void onDismiss() {
        this.f1161l = null;
        this.f1154e.close();
        if (this.f1162m != null) {
            if (!this.f1162m.isAlive()) {
                this.f1162m = this.f1160k.getViewTreeObserver();
            }
            this.f1162m.removeGlobalOnLayoutListener(this);
            this.f1162m = null;
        }
    }

    public boolean m2442f() {
        return this.f1161l != null && this.f1161l.m2755k();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        MenuPopupHelper menuPopupHelper = this.f1155f;
        menuPopupHelper.f1148b.m2343a(menuPopupHelper.m2421a(i), 0);
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        m2441e();
        return true;
    }

    private int m2426g() {
        ListAdapter listAdapter = this.f1155f;
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        int i = 0;
        int i2 = 0;
        View view = null;
        int i3 = 0;
        while (i < count) {
            View view2;
            int itemViewType = listAdapter.getItemViewType(i);
            if (itemViewType != i2) {
                i2 = itemViewType;
                view2 = null;
            } else {
                view2 = view;
            }
            if (this.f1164o == null) {
                this.f1164o = new FrameLayout(this.f1152c);
            }
            view = listAdapter.getView(i, view2, this.f1164o);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            itemViewType = view.getMeasuredWidth();
            if (itemViewType >= this.f1157h) {
                return this.f1157h;
            }
            if (itemViewType <= i3) {
                itemViewType = i3;
            }
            i++;
            i3 = itemViewType;
        }
        return i3;
    }

    public void onGlobalLayout() {
        if (m2442f()) {
            View view = this.f1160k;
            if (view == null || !view.isShown()) {
                m2441e();
            } else if (m2442f()) {
                this.f1161l.m2741c();
            }
        }
    }

    public void m2429a(Context context, MenuBuilder menuBuilder) {
    }

    public void m2436b(boolean z) {
        this.f1165p = false;
        if (this.f1155f != null) {
            this.f1155f.notifyDataSetChanged();
        }
    }

    public void m2431a(MenuPresenter menuPresenter) {
        this.f1163n = menuPresenter;
    }

    public boolean m2435a(SubMenuBuilder subMenuBuilder) {
        if (subMenuBuilder.hasVisibleItems()) {
            boolean z;
            MenuPopupHelper menuPopupHelper = new MenuPopupHelper(this.f1152c, subMenuBuilder, this.f1160k);
            menuPopupHelper.m2431a(this.f1163n);
            int size = subMenuBuilder.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = subMenuBuilder.getItem(i);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
            }
            z = false;
            menuPopupHelper.m2433a(z);
            if (menuPopupHelper.m2440d()) {
                if (this.f1163n == null) {
                    return true;
                }
                this.f1163n.m2014a(subMenuBuilder);
                return true;
            }
        }
        return false;
    }

    public void m2430a(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.f1154e) {
            m2441e();
            if (this.f1163n != null) {
                this.f1163n.m2013a(menuBuilder, z);
            }
        }
    }

    public boolean m2437b() {
        return false;
    }

    public boolean m2434a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean m2438b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }
}
