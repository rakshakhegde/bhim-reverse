package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.p006f.ActionProvider.ActionProvider;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.support.v7.view.menu.MenuItemWrapperICS.MenuItemWrapperICS;
import android.view.ActionProvider.VisibilityListener;
import android.view.MenuItem;
import android.view.View;

@TargetApi(16)
/* renamed from: android.support.v7.view.menu.j */
class MenuItemWrapperJB extends MenuItemWrapperICS {

    /* renamed from: android.support.v7.view.menu.j.a */
    class MenuItemWrapperJB extends MenuItemWrapperICS implements VisibilityListener {
        ActionProvider f1145c;
        final /* synthetic */ MenuItemWrapperJB f1146d;

        public MenuItemWrapperJB(MenuItemWrapperJB menuItemWrapperJB, Context context, android.view.ActionProvider actionProvider) {
            this.f1146d = menuItemWrapperJB;
            super(menuItemWrapperJB, context, actionProvider);
        }

        public View m2415a(MenuItem menuItem) {
            return this.a.onCreateActionView(menuItem);
        }

        public boolean m2417b() {
            return this.a.overridesItemVisibility();
        }

        public boolean m2418c() {
            return this.a.isVisible();
        }

        public void m2416a(ActionProvider actionProvider) {
            VisibilityListener visibilityListener;
            this.f1145c = actionProvider;
            android.view.ActionProvider actionProvider2 = this.a;
            if (actionProvider == null) {
                visibilityListener = null;
            }
            actionProvider2.setVisibilityListener(visibilityListener);
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            if (this.f1145c != null) {
                this.f1145c.m1405a(z);
            }
        }
    }

    MenuItemWrapperJB(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    MenuItemWrapperICS m2419a(android.view.ActionProvider actionProvider) {
        return new MenuItemWrapperJB(this, this.a, actionProvider);
    }
}
