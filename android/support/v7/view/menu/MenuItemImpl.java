package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.p006f.ActionProvider;
import android.support.v4.p006f.MenuItemCompat.MenuItemCompat;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.support.v7.view.menu.MenuView.MenuView;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.Log;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.LinearLayout;
import com.crashlytics.android.core.BuildConfig;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.view.menu.h */
public final class MenuItemImpl implements SupportMenuItem {
    private static String f1113w;
    private static String f1114x;
    private static String f1115y;
    private static String f1116z;
    private final int f1117a;
    private final int f1118b;
    private final int f1119c;
    private final int f1120d;
    private CharSequence f1121e;
    private CharSequence f1122f;
    private Intent f1123g;
    private char f1124h;
    private char f1125i;
    private Drawable f1126j;
    private int f1127k;
    private MenuBuilder f1128l;
    private SubMenuBuilder f1129m;
    private Runnable f1130n;
    private OnMenuItemClickListener f1131o;
    private int f1132p;
    private int f1133q;
    private View f1134r;
    private ActionProvider f1135s;
    private MenuItemCompat f1136t;
    private boolean f1137u;
    private ContextMenuInfo f1138v;

    /* renamed from: android.support.v7.view.menu.h.1 */
    class MenuItemImpl implements ActionProvider.ActionProvider {
        final /* synthetic */ MenuItemImpl f1112a;

        MenuItemImpl(MenuItemImpl menuItemImpl) {
            this.f1112a = menuItemImpl;
        }

        public void m2375a(boolean z) {
            this.f1112a.f1128l.m2336a(this.f1112a);
        }
    }

    public /* synthetic */ MenuItem setActionView(int i) {
        return m2377a(i);
    }

    public /* synthetic */ MenuItem setActionView(View view) {
        return m2380a(view);
    }

    public /* synthetic */ MenuItem setShowAsActionFlags(int i) {
        return m2386b(i);
    }

    MenuItemImpl(MenuBuilder menuBuilder, int i, int i2, int i3, int i4, CharSequence charSequence, int i5) {
        this.f1127k = 0;
        this.f1132p = 16;
        this.f1133q = 0;
        this.f1137u = false;
        this.f1128l = menuBuilder;
        this.f1117a = i2;
        this.f1118b = i;
        this.f1119c = i3;
        this.f1120d = i4;
        this.f1121e = charSequence;
        this.f1133q = i5;
    }

    public boolean m2388b() {
        if ((this.f1131o != null && this.f1131o.onMenuItemClick(this)) || this.f1128l.m2342a(this.f1128l.m2368p(), (MenuItem) this)) {
            return true;
        }
        if (this.f1130n != null) {
            this.f1130n.run();
            return true;
        }
        if (this.f1123g != null) {
            try {
                this.f1128l.m2357e().startActivity(this.f1123g);
                return true;
            } catch (Throwable e) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e);
            }
        }
        if (this.f1135s == null || !this.f1135s.m1414d()) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return (this.f1132p & 16) != 0;
    }

    public MenuItem setEnabled(boolean z) {
        if (z) {
            this.f1132p |= 16;
        } else {
            this.f1132p &= -17;
        }
        this.f1128l.m2349b(false);
        return this;
    }

    public int getGroupId() {
        return this.f1118b;
    }

    @CapturedViewProperty
    public int getItemId() {
        return this.f1117a;
    }

    public int getOrder() {
        return this.f1119c;
    }

    public int m2389c() {
        return this.f1120d;
    }

    public Intent getIntent() {
        return this.f1123g;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1123g = intent;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.f1125i;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        if (this.f1125i != c) {
            this.f1125i = Character.toLowerCase(c);
            this.f1128l.m2349b(false);
        }
        return this;
    }

    public char getNumericShortcut() {
        return this.f1124h;
    }

    public MenuItem setNumericShortcut(char c) {
        if (this.f1124h != c) {
            this.f1124h = c;
            this.f1128l.m2349b(false);
        }
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1124h = c;
        this.f1125i = Character.toLowerCase(c2);
        this.f1128l.m2349b(false);
        return this;
    }

    char m2391d() {
        return this.f1128l.m2350b() ? this.f1125i : this.f1124h;
    }

    String m2393e() {
        char d = m2391d();
        if (d == '\u0000') {
            return BuildConfig.FLAVOR;
        }
        StringBuilder stringBuilder = new StringBuilder(f1113w);
        switch (d) {
            case R.Toolbar_contentInsetRight /*8*/:
                stringBuilder.append(f1115y);
                break;
            case R.Toolbar_titleTextAppearance /*10*/:
                stringBuilder.append(f1114x);
                break;
            case R.AppCompatTheme_actionModeCutDrawable /*32*/:
                stringBuilder.append(f1116z);
                break;
            default:
                stringBuilder.append(d);
                break;
        }
        return stringBuilder.toString();
    }

    boolean m2395f() {
        return this.f1128l.m2353c() && m2391d() != '\u0000';
    }

    public SubMenu getSubMenu() {
        return this.f1129m;
    }

    public boolean hasSubMenu() {
        return this.f1129m != null;
    }

    public void m2383a(SubMenuBuilder subMenuBuilder) {
        this.f1129m = subMenuBuilder;
        subMenuBuilder.setHeaderTitle(getTitle());
    }

    @CapturedViewProperty
    public CharSequence getTitle() {
        return this.f1121e;
    }

    CharSequence m2382a(MenuView menuView) {
        return (menuView == null || !menuView.m2248a()) ? getTitle() : getTitleCondensed();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1121e = charSequence;
        this.f1128l.m2349b(false);
        if (this.f1129m != null) {
            this.f1129m.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitle(int i) {
        return setTitle(this.f1128l.m2357e().getString(i));
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f1122f != null ? this.f1122f : this.f1121e;
        if (VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) {
            return charSequence;
        }
        return charSequence.toString();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1122f = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f1121e;
        }
        this.f1128l.m2349b(false);
        return this;
    }

    public Drawable getIcon() {
        if (this.f1126j != null) {
            return this.f1126j;
        }
        if (this.f1127k == 0) {
            return null;
        }
        Drawable a = AppCompatDrawableManager.m2981a().m3004a(this.f1128l.m2357e(), this.f1127k);
        this.f1127k = 0;
        this.f1126j = a;
        return a;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1127k = 0;
        this.f1126j = drawable;
        this.f1128l.m2349b(false);
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1126j = null;
        this.f1127k = i;
        this.f1128l.m2349b(false);
        return this;
    }

    public boolean isCheckable() {
        return (this.f1132p & 1) == 1;
    }

    public MenuItem setCheckable(boolean z) {
        int i = this.f1132p;
        this.f1132p = (z ? 1 : 0) | (this.f1132p & -2);
        if (i != this.f1132p) {
            this.f1128l.m2349b(false);
        }
        return this;
    }

    public void m2385a(boolean z) {
        this.f1132p = (z ? 4 : 0) | (this.f1132p & -5);
    }

    public boolean m2396g() {
        return (this.f1132p & 4) != 0;
    }

    public boolean isChecked() {
        return (this.f1132p & 2) == 2;
    }

    public MenuItem setChecked(boolean z) {
        if ((this.f1132p & 4) != 0) {
            this.f1128l.m2339a((MenuItem) this);
        } else {
            m2387b(z);
        }
        return this;
    }

    void m2387b(boolean z) {
        int i;
        int i2 = this.f1132p;
        int i3 = this.f1132p & -3;
        if (z) {
            i = 2;
        } else {
            i = 0;
        }
        this.f1132p = i | i3;
        if (i2 != this.f1132p) {
            this.f1128l.m2349b(false);
        }
    }

    public boolean isVisible() {
        if (this.f1135s == null || !this.f1135s.m1412b()) {
            if ((this.f1132p & 8) != 0) {
                return false;
            }
            return true;
        } else if ((this.f1132p & 8) == 0 && this.f1135s.m1413c()) {
            return true;
        } else {
            return false;
        }
    }

    boolean m2390c(boolean z) {
        int i = this.f1132p;
        this.f1132p = (z ? 0 : 8) | (this.f1132p & -9);
        if (i != this.f1132p) {
            return true;
        }
        return false;
    }

    public MenuItem setVisible(boolean z) {
        if (m2390c(z)) {
            this.f1128l.m2336a(this);
        }
        return this;
    }

    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.f1131o = onMenuItemClickListener;
        return this;
    }

    public String toString() {
        return this.f1121e != null ? this.f1121e.toString() : null;
    }

    void m2384a(ContextMenuInfo contextMenuInfo) {
        this.f1138v = contextMenuInfo;
    }

    public ContextMenuInfo getMenuInfo() {
        return this.f1138v;
    }

    public void m2397h() {
        this.f1128l.m2347b(this);
    }

    public boolean m2398i() {
        return this.f1128l.m2369q();
    }

    public boolean m2399j() {
        return (this.f1132p & 32) == 32;
    }

    public boolean m2400k() {
        return (this.f1133q & 1) == 1;
    }

    public boolean m2401l() {
        return (this.f1133q & 2) == 2;
    }

    public void m2392d(boolean z) {
        if (z) {
            this.f1132p |= 32;
        } else {
            this.f1132p &= -33;
        }
    }

    public boolean m2402m() {
        return (this.f1133q & 4) == 4;
    }

    public void setShowAsAction(int i) {
        switch (i & 3) {
            case R.View_android_theme /*0*/:
            case R.View_android_focusable /*1*/:
            case R.View_paddingStart /*2*/:
                this.f1133q = i;
                this.f1128l.m2347b(this);
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    public SupportMenuItem m2380a(View view) {
        this.f1134r = view;
        this.f1135s = null;
        if (view != null && view.getId() == -1 && this.f1117a > 0) {
            view.setId(this.f1117a);
        }
        this.f1128l.m2347b(this);
        return this;
    }

    public SupportMenuItem m2377a(int i) {
        Context e = this.f1128l.m2357e();
        m2380a(LayoutInflater.from(e).inflate(i, new LinearLayout(e), false));
        return this;
    }

    public View getActionView() {
        if (this.f1134r != null) {
            return this.f1134r;
        }
        if (this.f1135s == null) {
            return null;
        }
        this.f1134r = this.f1135s.m1407a((MenuItem) this);
        return this.f1134r;
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public ActionProvider m2381a() {
        return this.f1135s;
    }

    public SupportMenuItem m2378a(ActionProvider actionProvider) {
        if (this.f1135s != null) {
            this.f1135s.m1416f();
        }
        this.f1134r = null;
        this.f1135s = actionProvider;
        this.f1128l.m2349b(true);
        if (this.f1135s != null) {
            this.f1135s.m1409a(new MenuItemImpl(this));
        }
        return this;
    }

    public SupportMenuItem m2386b(int i) {
        setShowAsAction(i);
        return this;
    }

    public boolean expandActionView() {
        if (!m2403n()) {
            return false;
        }
        if (this.f1136t == null || this.f1136t.m1474a(this)) {
            return this.f1128l.m2354c(this);
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.f1133q & 8) == 0) {
            return false;
        }
        if (this.f1134r == null) {
            return true;
        }
        if (this.f1136t == null || this.f1136t.m1475b(this)) {
            return this.f1128l.m2356d(this);
        }
        return false;
    }

    public SupportMenuItem m2379a(MenuItemCompat menuItemCompat) {
        this.f1136t = menuItemCompat;
        return this;
    }

    public boolean m2403n() {
        if ((this.f1133q & 8) == 0) {
            return false;
        }
        if (this.f1134r == null && this.f1135s != null) {
            this.f1134r = this.f1135s.m1407a((MenuItem) this);
        }
        if (this.f1134r != null) {
            return true;
        }
        return false;
    }

    public void m2394e(boolean z) {
        this.f1137u = z;
        this.f1128l.m2349b(false);
    }

    public boolean isActionViewExpanded() {
        return this.f1137u;
    }

    public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }
}
