package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.p004a.ContextCompat;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v7.view.menu.p */
public class SubMenuBuilder extends MenuBuilder implements SubMenu {
    private MenuBuilder f1168d;
    private MenuItemImpl f1169e;

    public SubMenuBuilder(Context context, MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        super(context);
        this.f1168d = menuBuilder;
        this.f1169e = menuItemImpl;
    }

    public void setQwertyMode(boolean z) {
        this.f1168d.setQwertyMode(z);
    }

    public boolean m2449b() {
        return this.f1168d.m2350b();
    }

    public boolean m2450c() {
        return this.f1168d.m2353c();
    }

    public Menu m2454s() {
        return this.f1168d;
    }

    public MenuItem getItem() {
        return this.f1169e;
    }

    public void m2447a(MenuBuilder menuBuilder) {
        this.f1168d.m2335a(menuBuilder);
    }

    public MenuBuilder m2453p() {
        return this.f1168d;
    }

    boolean m2448a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return super.m2342a(menuBuilder, menuItem) || this.f1168d.m2342a(menuBuilder, menuItem);
    }

    public SubMenu setIcon(Drawable drawable) {
        this.f1169e.setIcon(drawable);
        return this;
    }

    public SubMenu setIcon(int i) {
        this.f1169e.setIcon(i);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        super.m2328a(drawable);
        return this;
    }

    public SubMenu setHeaderIcon(int i) {
        super.m2328a(ContextCompat.m77a(m2357e(), i));
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.m2330a(charSequence);
        return this;
    }

    public SubMenu setHeaderTitle(int i) {
        super.m2330a(m2357e().getResources().getString(i));
        return this;
    }

    public SubMenu setHeaderView(View view) {
        super.m2329a(view);
        return this;
    }

    public boolean m2451c(MenuItemImpl menuItemImpl) {
        return this.f1168d.m2354c(menuItemImpl);
    }

    public boolean m2452d(MenuItemImpl menuItemImpl) {
        return this.f1168d.m2356d(menuItemImpl);
    }

    public String m2446a() {
        int itemId = this.f1169e != null ? this.f1169e.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.m2333a() + ":" + itemId;
    }
}
