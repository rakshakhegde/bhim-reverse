package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView.MenuView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

/* renamed from: android.support.v7.view.menu.e */
public class ListMenuPresenter implements MenuPresenter, OnItemClickListener {
    Context f1074a;
    LayoutInflater f1075b;
    MenuBuilder f1076c;
    ExpandedMenuView f1077d;
    int f1078e;
    int f1079f;
    ListMenuPresenter f1080g;
    private int f1081h;
    private MenuPresenter f1082i;

    /* renamed from: android.support.v7.view.menu.e.a */
    private class ListMenuPresenter extends BaseAdapter {
        final /* synthetic */ ListMenuPresenter f1072a;
        private int f1073b;

        public /* synthetic */ Object getItem(int i) {
            return m2305a(i);
        }

        public ListMenuPresenter(ListMenuPresenter listMenuPresenter) {
            this.f1072a = listMenuPresenter;
            this.f1073b = -1;
            m2306a();
        }

        public int getCount() {
            int size = this.f1072a.f1076c.m2364l().size() - this.f1072a.f1081h;
            return this.f1073b < 0 ? size : size - 1;
        }

        public MenuItemImpl m2305a(int i) {
            ArrayList l = this.f1072a.f1076c.m2364l();
            int a = this.f1072a.f1081h + i;
            if (this.f1073b >= 0 && a >= this.f1073b) {
                a++;
            }
            return (MenuItemImpl) l.get(a);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate;
            if (view == null) {
                inflate = this.f1072a.f1075b.inflate(this.f1072a.f1079f, viewGroup, false);
            } else {
                inflate = view;
            }
            ((MenuView) inflate).m2247a(m2305a(i), 0);
            return inflate;
        }

        void m2306a() {
            MenuItemImpl r = this.f1072a.f1076c.m2370r();
            if (r != null) {
                ArrayList l = this.f1072a.f1076c.m2364l();
                int size = l.size();
                for (int i = 0; i < size; i++) {
                    if (((MenuItemImpl) l.get(i)) == r) {
                        this.f1073b = i;
                        return;
                    }
                }
            }
            this.f1073b = -1;
        }

        public void notifyDataSetChanged() {
            m2306a();
            super.notifyDataSetChanged();
        }
    }

    public ListMenuPresenter(Context context, int i) {
        this(i, 0);
        this.f1074a = context;
        this.f1075b = LayoutInflater.from(this.f1074a);
    }

    public ListMenuPresenter(int i, int i2) {
        this.f1079f = i;
        this.f1078e = i2;
    }

    public void m2310a(Context context, MenuBuilder menuBuilder) {
        if (this.f1078e != 0) {
            this.f1074a = new ContextThemeWrapper(context, this.f1078e);
            this.f1075b = LayoutInflater.from(this.f1074a);
        } else if (this.f1074a != null) {
            this.f1074a = context;
            if (this.f1075b == null) {
                this.f1075b = LayoutInflater.from(this.f1074a);
            }
        }
        this.f1076c = menuBuilder;
        if (this.f1080g != null) {
            this.f1080g.notifyDataSetChanged();
        }
    }

    public MenuView m2308a(ViewGroup viewGroup) {
        if (this.f1077d == null) {
            this.f1077d = (ExpandedMenuView) this.f1075b.inflate(R.abc_expanded_menu_layout, viewGroup, false);
            if (this.f1080g == null) {
                this.f1080g = new ListMenuPresenter(this);
            }
            this.f1077d.setAdapter(this.f1080g);
            this.f1077d.setOnItemClickListener(this);
        }
        return this.f1077d;
    }

    public ListAdapter m2309a() {
        if (this.f1080g == null) {
            this.f1080g = new ListMenuPresenter(this);
        }
        return this.f1080g;
    }

    public void m2315b(boolean z) {
        if (this.f1080g != null) {
            this.f1080g.notifyDataSetChanged();
        }
    }

    public void m2312a(MenuPresenter menuPresenter) {
        this.f1082i = menuPresenter;
    }

    public boolean m2314a(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        new MenuDialogHelper(subMenuBuilder).m2372a(null);
        if (this.f1082i != null) {
            this.f1082i.m2014a(subMenuBuilder);
        }
        return true;
    }

    public void m2311a(MenuBuilder menuBuilder, boolean z) {
        if (this.f1082i != null) {
            this.f1082i.m2013a(menuBuilder, z);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1076c.m2344a(this.f1080g.m2305a(i), (MenuPresenter) this, 0);
    }

    public boolean m2316b() {
        return false;
    }

    public boolean m2313a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean m2317b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }
}
