package android.support.v7.view.menu;

/* renamed from: android.support.v7.view.menu.m */
public interface MenuView {

    /* renamed from: android.support.v7.view.menu.m.a */
    public interface MenuView {
        void m2247a(MenuItemImpl menuItemImpl, int i);

        boolean m2248a();

        MenuItemImpl getItemData();
    }

    void m2261a(MenuBuilder menuBuilder);
}
