package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.os.IBinder;
import android.support.v7.p013a.AlertDialog;
import android.support.v7.p014b.R.R;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

/* renamed from: android.support.v7.view.menu.g */
class MenuDialogHelper implements OnClickListener, OnDismissListener, OnKeyListener, MenuPresenter {
    ListMenuPresenter f1108a;
    private MenuBuilder f1109b;
    private AlertDialog f1110c;
    private MenuPresenter f1111d;

    public MenuDialogHelper(MenuBuilder menuBuilder) {
        this.f1109b = menuBuilder;
    }

    public void m2372a(IBinder iBinder) {
        MenuBuilder menuBuilder = this.f1109b;
        AlertDialog.AlertDialog alertDialog = new AlertDialog.AlertDialog(menuBuilder.m2357e());
        this.f1108a = new ListMenuPresenter(alertDialog.m1860a(), R.abc_list_menu_item_layout);
        this.f1108a.m2312a((MenuPresenter) this);
        this.f1109b.m2337a(this.f1108a);
        alertDialog.m1864a(this.f1108a.m2309a(), this);
        View o = menuBuilder.m2367o();
        if (o != null) {
            alertDialog.m1863a(o);
        } else {
            alertDialog.m1862a(menuBuilder.m2366n()).m1865a(menuBuilder.m2365m());
        }
        alertDialog.m1861a((OnKeyListener) this);
        this.f1110c = alertDialog.m1866b();
        this.f1110c.setOnDismissListener(this);
        LayoutParams attributes = this.f1110c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f1110c.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 82 || i == 4) {
            Window window;
            View decorView;
            DispatcherState keyDispatcherState;
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                window = this.f1110c.getWindow();
                if (window != null) {
                    decorView = window.getDecorView();
                    if (decorView != null) {
                        keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null) {
                            keyDispatcherState.startTracking(keyEvent, this);
                            return true;
                        }
                    }
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled()) {
                window = this.f1110c.getWindow();
                if (window != null) {
                    decorView = window.getDecorView();
                    if (decorView != null) {
                        keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null && keyDispatcherState.isTracking(keyEvent)) {
                            this.f1109b.m2341a(true);
                            dialogInterface.dismiss();
                            return true;
                        }
                    }
                }
            }
        }
        return this.f1109b.performShortcut(i, keyEvent, 0);
    }

    public void m2371a() {
        if (this.f1110c != null) {
            this.f1110c.dismiss();
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f1108a.m2311a(this.f1109b, true);
    }

    public void m2373a(MenuBuilder menuBuilder, boolean z) {
        if (z || menuBuilder == this.f1109b) {
            m2371a();
        }
        if (this.f1111d != null) {
            this.f1111d.m2013a(menuBuilder, z);
        }
    }

    public boolean m2374a(MenuBuilder menuBuilder) {
        if (this.f1111d != null) {
            return this.f1111d.m2014a(menuBuilder);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1109b.m2343a((MenuItemImpl) this.f1108a.m2309a().getItem(i), 0);
    }
}
