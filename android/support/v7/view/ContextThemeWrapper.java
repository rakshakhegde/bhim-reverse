package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources.Theme;
import android.support.v7.p014b.R.R;
import android.view.LayoutInflater;

/* renamed from: android.support.v7.view.d */
public class ContextThemeWrapper extends ContextWrapper {
    private int f942a;
    private Theme f943b;
    private LayoutInflater f944c;

    public ContextThemeWrapper(Context context, int i) {
        super(context);
        this.f942a = i;
    }

    public ContextThemeWrapper(Context context, Theme theme) {
        super(context);
        this.f943b = theme;
    }

    public void setTheme(int i) {
        if (this.f942a != i) {
            this.f942a = i;
            m2174b();
        }
    }

    public int m2175a() {
        return this.f942a;
    }

    public Theme getTheme() {
        if (this.f943b != null) {
            return this.f943b;
        }
        if (this.f942a == 0) {
            this.f942a = R.Theme_AppCompat_Light;
        }
        m2174b();
        return this.f943b;
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f944c == null) {
            this.f944c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f944c;
    }

    protected void m2176a(Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    private void m2174b() {
        boolean z = this.f943b == null;
        if (z) {
            this.f943b = getResources().newTheme();
            Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f943b.setTo(theme);
            }
        }
        m2176a(this.f943b, this.f942a, z);
    }
}
