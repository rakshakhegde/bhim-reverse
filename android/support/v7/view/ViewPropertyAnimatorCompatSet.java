package android.support.v7.view;

import android.support.v4.p006f.au;
import android.support.v4.p006f.ay;
import android.support.v4.p006f.az;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: android.support.v7.view.h */
public class ViewPropertyAnimatorCompatSet {
    private final ArrayList<au> f996a;
    private long f997b;
    private Interpolator f998c;
    private ay f999d;
    private boolean f1000e;
    private final az f1001f;

    /* renamed from: android.support.v7.view.h.1 */
    class ViewPropertyAnimatorCompatSet extends az {
        final /* synthetic */ ViewPropertyAnimatorCompatSet f993a;
        private boolean f994b;
        private int f995c;

        ViewPropertyAnimatorCompatSet(ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet) {
            this.f993a = viewPropertyAnimatorCompatSet;
            this.f994b = false;
            this.f995c = 0;
        }

        public void m2219a(View view) {
            if (!this.f994b) {
                this.f994b = true;
                if (this.f993a.f999d != null) {
                    this.f993a.f999d.m1327a(null);
                }
            }
        }

        void m2218a() {
            this.f995c = 0;
            this.f994b = false;
            this.f993a.m2224c();
        }

        public void m2220b(View view) {
            int i = this.f995c + 1;
            this.f995c = i;
            if (i == this.f993a.f996a.size()) {
                if (this.f993a.f999d != null) {
                    this.f993a.f999d.m1328b(null);
                }
                m2218a();
            }
        }
    }

    public ViewPropertyAnimatorCompatSet() {
        this.f997b = -1;
        this.f1001f = new ViewPropertyAnimatorCompatSet(this);
        this.f996a = new ArrayList();
    }

    public ViewPropertyAnimatorCompatSet m2226a(au auVar) {
        if (!this.f1000e) {
            this.f996a.add(auVar);
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet m2227a(au auVar, au auVar2) {
        this.f996a.add(auVar);
        auVar2.m1359b(auVar.m1352a());
        this.f996a.add(auVar2);
        return this;
    }

    public void m2230a() {
        if (!this.f1000e) {
            Iterator it = this.f996a.iterator();
            while (it.hasNext()) {
                au auVar = (au) it.next();
                if (this.f997b >= 0) {
                    auVar.m1354a(this.f997b);
                }
                if (this.f998c != null) {
                    auVar.m1357a(this.f998c);
                }
                if (this.f999d != null) {
                    auVar.m1355a(this.f1001f);
                }
                auVar.m1362c();
            }
            this.f1000e = true;
        }
    }

    private void m2224c() {
        this.f1000e = false;
    }

    public void m2231b() {
        if (this.f1000e) {
            Iterator it = this.f996a.iterator();
            while (it.hasNext()) {
                ((au) it.next()).m1360b();
            }
            this.f1000e = false;
        }
    }

    public ViewPropertyAnimatorCompatSet m2225a(long j) {
        if (!this.f1000e) {
            this.f997b = j;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet m2229a(Interpolator interpolator) {
        if (!this.f1000e) {
            this.f998c = interpolator;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet m2228a(ay ayVar) {
        if (!this.f1000e) {
            this.f999d = ayVar;
        }
        return this;
    }
}
