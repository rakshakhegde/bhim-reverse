package android.support.v7.p013a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.p013a.AppCompatDelegateImplBase.AppCompatDelegateImplBase;
import android.support.v7.view.SupportActionModeWrapper.SupportActionModeWrapper;
import android.view.ActionMode;
import android.view.Window;
import android.view.Window.Callback;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.j */
class AppCompatDelegateImplV14 extends AppCompatDelegateImplV11 {
    private static TwilightManager f816r;
    private int f817s;
    private boolean f818t;
    private boolean f819u;

    /* renamed from: android.support.v7.a.j.a */
    class AppCompatDelegateImplV14 extends AppCompatDelegateImplBase {
        final /* synthetic */ AppCompatDelegateImplV14 f815b;

        AppCompatDelegateImplV14(AppCompatDelegateImplV14 appCompatDelegateImplV14, Callback callback) {
            this.f815b = appCompatDelegateImplV14;
            super(appCompatDelegateImplV14, callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (this.f815b.m1999n()) {
                return m1991a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        final ActionMode m1991a(ActionMode.Callback callback) {
            Object supportActionModeWrapper = new SupportActionModeWrapper(this.f815b.a, callback);
            android.support.v7.view.ActionMode b = this.f815b.m1972b((android.support.v7.view.ActionMode.ActionMode) supportActionModeWrapper);
            if (b != null) {
                return supportActionModeWrapper.m2197b(b);
            }
            return null;
        }
    }

    AppCompatDelegateImplV14(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
        this.f817s = -100;
        this.f819u = true;
    }

    public void m1995a(Bundle bundle) {
        super.m1964a(bundle);
        if (bundle != null && this.f817s == -100) {
            this.f817s = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    Callback m1994a(Callback callback) {
        return new AppCompatDelegateImplV14(this, callback);
    }

    public boolean m1999n() {
        return this.f819u;
    }

    public boolean m1998h() {
        this.f818t = true;
        int d = m1997d(this.f817s == -100 ? AppCompatDelegate.m1878i() : this.f817s);
        if (d != -1) {
            return m1992e(d);
        }
        return false;
    }

    int m1997d(int i) {
        switch (i) {
            case -100:
                return -1;
            case R.View_android_theme /*0*/:
                return m1993s().m2070a() ? 2 : 1;
            default:
                return i;
        }
    }

    public void m1996c(Bundle bundle) {
        super.m1909c(bundle);
        if (this.f817s != -100) {
            bundle.putInt("appcompat:local_night_mode", this.f817s);
        }
    }

    private boolean m1992e(int i) {
        Resources resources = this.a.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        Configuration configuration2 = new Configuration(configuration);
        configuration2.uiMode = i3 | (configuration2.uiMode & -49);
        resources.updateConfiguration(configuration2, null);
        return true;
    }

    private TwilightManager m1993s() {
        if (f816r == null) {
            f816r = new TwilightManager(this.a.getApplicationContext());
        }
        return f816r;
    }
}
