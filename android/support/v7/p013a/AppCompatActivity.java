package android.support.v7.p013a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.ap;
import android.support.v4.app.ap.TaskStackBuilder;
import android.support.v4.p006f.KeyEventCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.at;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

/* renamed from: android.support.v7.a.e */
public class AppCompatActivity extends FragmentActivity implements TaskStackBuilder, AppCompatCallback {
    private AppCompatDelegate f659m;
    private int f660n;
    private boolean f661o;
    private Resources f662p;

    public AppCompatActivity() {
        this.f660n = 0;
    }

    protected void onCreate(Bundle bundle) {
        AppCompatDelegate j = m1814j();
        j.m1897g();
        j.m1883a(bundle);
        if (j.m1898h() && this.f660n != 0) {
            if (VERSION.SDK_INT >= 23) {
                onApplyThemeResource(getTheme(), this.f660n, false);
            } else {
                setTheme(this.f660n);
            }
        }
        super.onCreate(bundle);
    }

    public void setTheme(int i) {
        super.setTheme(i);
        this.f660n = i;
    }

    protected void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        m1814j().m1889b(bundle);
    }

    public ActionBar m1811g() {
        return m1814j().m1880a();
    }

    public MenuInflater getMenuInflater() {
        return m1814j().m1887b();
    }

    public void setContentView(int i) {
        m1814j().m1888b(i);
    }

    public void setContentView(View view) {
        m1814j().m1884a(view);
    }

    public void setContentView(View view, LayoutParams layoutParams) {
        m1814j().m1885a(view, layoutParams);
    }

    public void addContentView(View view, LayoutParams layoutParams) {
        m1814j().m1890b(view, layoutParams);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        m1814j().m1882a(configuration);
        if (this.f662p != null) {
            this.f662p.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }

    protected void onStop() {
        super.onStop();
        m1814j().m1891c();
    }

    protected void onPostResume() {
        super.onPostResume();
        m1814j().m1894d();
    }

    public View findViewById(int i) {
        return m1814j().m1881a(i);
    }

    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        ActionBar g = m1811g();
        if (menuItem.getItemId() != 16908332 || g == null || (g.m1781a() & 4) == 0) {
            return false;
        }
        return m1812h();
    }

    protected void onDestroy() {
        super.onDestroy();
        m1814j().m1896f();
    }

    protected void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        m1814j().m1886a(charSequence);
    }

    public void m1810d() {
        m1814j().m1895e();
    }

    public void invalidateOptionsMenu() {
        m1814j().m1895e();
    }

    public void m1805a(ActionMode actionMode) {
    }

    public void m1809b(ActionMode actionMode) {
    }

    public ActionMode m1803a(ActionMode.ActionMode actionMode) {
        return null;
    }

    public void m1804a(ap apVar) {
        apVar.m297a((Activity) this);
    }

    public void m1808b(ap apVar) {
    }

    public boolean m1812h() {
        Intent a = m1802a();
        if (a == null) {
            return false;
        }
        if (m1806a(a)) {
            ap a2 = ap.m296a((Context) this);
            m1804a(a2);
            m1808b(a2);
            a2.m300a();
            try {
                ActivityCompat.m204a(this);
            } catch (IllegalStateException e) {
                finish();
            }
        } else {
            m1807b(a);
        }
        return true;
    }

    public Intent m1802a() {
        return NavUtils.m594a(this);
    }

    public boolean m1806a(Intent intent) {
        return NavUtils.m596a((Activity) this, intent);
    }

    public void m1807b(Intent intent) {
        NavUtils.m599b((Activity) this, intent);
    }

    public void onContentChanged() {
        m1813i();
    }

    @Deprecated
    public void m1813i() {
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return super.onMenuOpened(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
    }

    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        m1814j().m1892c(bundle);
    }

    public AppCompatDelegate m1814j() {
        if (this.f659m == null) {
            this.f659m = AppCompatDelegate.m1875a((Activity) this, (AppCompatCallback) this);
        }
        return this.f659m;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (KeyEventCompat.m1428a(keyEvent, CodedOutputStream.DEFAULT_BUFFER_SIZE) && keyEvent.getUnicodeChar(keyEvent.getMetaState() & -28673) == 60) {
            int action = keyEvent.getAction();
            if (action == 0) {
                ActionBar g = m1811g();
                if (g != null && g.m1789b() && g.m1797g()) {
                    this.f661o = true;
                    return true;
                }
            } else if (action == 1 && this.f661o) {
                this.f661o = false;
                return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public Resources getResources() {
        if (this.f662p == null && at.m2879a()) {
            this.f662p = new at(this, super.getResources());
        }
        return this.f662p == null ? super.getResources() : this.f662p;
    }
}
