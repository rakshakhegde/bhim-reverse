package android.support.v7.p013a;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.view.menu.MenuBuilder;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;

/* renamed from: android.support.v7.a.h */
abstract class AppCompatDelegateImplBase extends AppCompatDelegate {
    final Context f776a;
    final Window f777b;
    final Callback f778c;
    final Callback f779d;
    final AppCompatCallback f780e;
    ActionBar f781f;
    MenuInflater f782g;
    boolean f783h;
    boolean f784i;
    boolean f785j;
    boolean f786k;
    boolean f787l;
    private CharSequence f788m;
    private boolean f789n;

    /* renamed from: android.support.v7.a.h.a */
    class AppCompatDelegateImplBase extends WindowCallbackWrapper {
        final /* synthetic */ AppCompatDelegateImplBase f775a;

        AppCompatDelegateImplBase(AppCompatDelegateImplBase appCompatDelegateImplBase, Callback callback) {
            this.f775a = appCompatDelegateImplBase;
            super(callback);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return this.f775a.m1905a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || this.f775a.m1904a(keyEvent.getKeyCode(), keyEvent);
        }

        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof MenuBuilder)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        public void onContentChanged() {
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            MenuBuilder menuBuilder;
            if (menu instanceof MenuBuilder) {
                menuBuilder = (MenuBuilder) menu;
            } else {
                menuBuilder = null;
            }
            if (i == 0 && menuBuilder == null) {
                return false;
            }
            if (menuBuilder != null) {
                menuBuilder.m2352c(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (menuBuilder == null) {
                return onPreparePanel;
            }
            menuBuilder.m2352c(false);
            return onPreparePanel;
        }

        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            this.f775a.m1908b(i, menu);
            return true;
        }

        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            this.f775a.m1902a(i, menu);
        }
    }

    abstract ActionMode m1900a(ActionMode.ActionMode actionMode);

    abstract void m1902a(int i, Menu menu);

    abstract boolean m1904a(int i, KeyEvent keyEvent);

    abstract boolean m1905a(KeyEvent keyEvent);

    abstract void m1907b(CharSequence charSequence);

    abstract boolean m1908b(int i, Menu menu);

    abstract void m1912k();

    AppCompatDelegateImplBase(Context context, Window window, AppCompatCallback appCompatCallback) {
        this.f776a = context;
        this.f777b = window;
        this.f780e = appCompatCallback;
        this.f778c = this.f777b.getCallback();
        if (this.f778c instanceof AppCompatDelegateImplBase) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        this.f779d = m1901a(this.f778c);
        this.f777b.setCallback(this.f779d);
    }

    Callback m1901a(Callback callback) {
        return new AppCompatDelegateImplBase(this, callback);
    }

    public ActionBar m1899a() {
        m1912k();
        return this.f781f;
    }

    final ActionBar m1913l() {
        return this.f781f;
    }

    public MenuInflater m1906b() {
        if (this.f782g == null) {
            m1912k();
            this.f782g = new SupportMenuInflater(this.f781f != null ? this.f781f.m1790c() : this.f776a);
        }
        return this.f782g;
    }

    final Context m1914m() {
        Context context = null;
        ActionBar a = m1899a();
        if (a != null) {
            context = a.m1790c();
        }
        if (context == null) {
            return this.f776a;
        }
        return context;
    }

    public void m1910f() {
        this.f789n = true;
    }

    public boolean m1915n() {
        return false;
    }

    public boolean m1911h() {
        return false;
    }

    final boolean m1916o() {
        return this.f789n;
    }

    final Callback m1917p() {
        return this.f777b.getCallback();
    }

    public final void m1903a(CharSequence charSequence) {
        this.f788m = charSequence;
        m1907b(charSequence);
    }

    public void m1909c(Bundle bundle) {
    }

    final CharSequence m1918q() {
        if (this.f778c instanceof Activity) {
            return ((Activity) this.f778c).getTitle();
        }
        return this.f788m;
    }
}
