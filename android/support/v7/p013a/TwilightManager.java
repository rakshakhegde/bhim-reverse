package android.support.v7.p013a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.p004a.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

/* renamed from: android.support.v7.a.q */
class TwilightManager {
    private static final TwilightManager f891a;
    private final Context f892b;
    private final LocationManager f893c;

    /* renamed from: android.support.v7.a.q.a */
    private static class TwilightManager {
        boolean f885a;
        long f886b;
        long f887c;
        long f888d;
        long f889e;
        long f890f;

        private TwilightManager() {
        }
    }

    static {
        f891a = new TwilightManager();
    }

    TwilightManager(Context context) {
        this.f892b = context;
        this.f893c = (LocationManager) context.getSystemService("location");
    }

    boolean m2070a() {
        TwilightManager twilightManager = f891a;
        if (m2068a(twilightManager)) {
            return twilightManager.f885a;
        }
        Location b = m2069b();
        if (b != null) {
            m2067a(b);
            return twilightManager.f885a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    private Location m2069b() {
        Location a;
        Location location = null;
        if (PermissionChecker.m112a(this.f892b, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            a = m2066a("network");
        } else {
            a = null;
        }
        if (PermissionChecker.m112a(this.f892b, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = m2066a("gps");
        }
        if (location == null || a == null) {
            if (location == null) {
                location = a;
            }
            return location;
        } else if (location.getTime() > a.getTime()) {
            return location;
        } else {
            return a;
        }
    }

    private Location m2066a(String str) {
        if (this.f893c != null) {
            try {
                if (this.f893c.isProviderEnabled(str)) {
                    return this.f893c.getLastKnownLocation(str);
                }
            } catch (Throwable e) {
                Log.d("TwilightManager", "Failed to get last known location", e);
            }
        }
        return null;
    }

    private boolean m2068a(TwilightManager twilightManager) {
        return twilightManager != null && twilightManager.f890f > System.currentTimeMillis();
    }

    private void m2067a(Location location) {
        long j;
        TwilightManager twilightManager = f891a;
        long currentTimeMillis = System.currentTimeMillis();
        TwilightCalculator a = TwilightCalculator.m2064a();
        a.m2065a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = a.f882a;
        a.m2065a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a.f884c == 1;
        long j3 = a.f883b;
        long j4 = a.f882a;
        a.m2065a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j5 = a.f883b;
        if (j3 == -1 || j4 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            if (currentTimeMillis > j4) {
                j = 0 + j5;
            } else if (currentTimeMillis > j3) {
                j = 0 + j4;
            } else {
                j = 0 + j3;
            }
            j += 60000;
        }
        twilightManager.f885a = z;
        twilightManager.f886b = j2;
        twilightManager.f887c = j3;
        twilightManager.f888d = j4;
        twilightManager.f889e = j5;
        twilightManager.f890f = j;
    }
}
