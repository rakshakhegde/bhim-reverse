package android.support.v7.p013a;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.p006f.af;
import android.support.v7.p013a.ActionBar.ActionBar;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.widget.ac;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window.Callback;
import java.util.ArrayList;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.o */
class ToolbarActionBar extends ActionBar {
    private ac f875a;
    private Callback f876b;
    private boolean f877c;
    private boolean f878d;
    private ArrayList<ActionBar> f879e;
    private final Runnable f880f;

    /* renamed from: android.support.v7.a.o.1 */
    class ToolbarActionBar implements Runnable {
        final /* synthetic */ ToolbarActionBar f871a;

        public void run() {
            this.f871a.m2063i();
        }
    }

    /* renamed from: android.support.v7.a.o.a */
    private final class ToolbarActionBar implements MenuPresenter {
        final /* synthetic */ ToolbarActionBar f872a;
        private boolean f873b;

        private ToolbarActionBar(ToolbarActionBar toolbarActionBar) {
            this.f872a = toolbarActionBar;
        }

        public boolean m2042a(MenuBuilder menuBuilder) {
            if (this.f872a.f876b == null) {
                return false;
            }
            this.f872a.f876b.onMenuOpened(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
            return true;
        }

        public void m2041a(MenuBuilder menuBuilder, boolean z) {
            if (!this.f873b) {
                this.f873b = true;
                this.f872a.f875a.m2701n();
                if (this.f872a.f876b != null) {
                    this.f872a.f876b.onPanelClosed(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
                }
                this.f873b = false;
            }
        }
    }

    /* renamed from: android.support.v7.a.o.b */
    private final class ToolbarActionBar implements MenuBuilder.MenuBuilder {
        final /* synthetic */ ToolbarActionBar f874a;

        private ToolbarActionBar(ToolbarActionBar toolbarActionBar) {
            this.f874a = toolbarActionBar;
        }

        public boolean m2044a(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void m2043a(MenuBuilder menuBuilder) {
            if (this.f874a.f876b == null) {
                return;
            }
            if (this.f874a.f875a.m2696i()) {
                this.f874a.f876b.onPanelClosed(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
            } else if (this.f874a.f876b.onPreparePanel(0, null, menuBuilder)) {
                this.f874a.f876b.onMenuOpened(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
            }
        }
    }

    public void m2052a(boolean z) {
    }

    public void m2049a(float f) {
        af.m1192d(this.f875a.m2677a(), f);
    }

    public Context m2055c() {
        return this.f875a.m2686b();
    }

    public void m2056c(boolean z) {
    }

    public void m2057d(boolean z) {
    }

    public void m2050a(Configuration configuration) {
        super.m1784a(configuration);
    }

    public void m2051a(CharSequence charSequence) {
        this.f875a.m2684a(charSequence);
    }

    public boolean m2061g() {
        ViewGroup a = this.f875a.m2677a();
        if (a == null || a.hasFocus()) {
            return false;
        }
        a.requestFocus();
        return true;
    }

    public int m2048a() {
        return this.f875a.m2702o();
    }

    public boolean m2054b() {
        return this.f875a.m2704q() == 0;
    }

    public boolean m2059e() {
        this.f875a.m2677a().removeCallbacks(this.f880f);
        af.m1182a(this.f875a.m2677a(), this.f880f);
        return true;
    }

    public boolean m2060f() {
        if (!this.f875a.m2690c()) {
            return false;
        }
        this.f875a.m2691d();
        return true;
    }

    void m2063i() {
        Menu j = m2047j();
        MenuBuilder menuBuilder = j instanceof MenuBuilder ? (MenuBuilder) j : null;
        if (menuBuilder != null) {
            menuBuilder.m2359g();
        }
        try {
            j.clear();
            if (!(this.f876b.onCreatePanelMenu(0, j) && this.f876b.onPreparePanel(0, null, j))) {
                j.clear();
            }
            if (menuBuilder != null) {
                menuBuilder.m2360h();
            }
        } catch (Throwable th) {
            if (menuBuilder != null) {
                menuBuilder.m2360h();
            }
        }
    }

    public boolean m2053a(int i, KeyEvent keyEvent) {
        Menu j = m2047j();
        if (j != null) {
            boolean z;
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z = true;
            } else {
                z = false;
            }
            j.setQwertyMode(z);
            j.performShortcut(i, keyEvent, 0);
        }
        return true;
    }

    void m2062h() {
        this.f875a.m2677a().removeCallbacks(this.f880f);
    }

    public void m2058e(boolean z) {
        if (z != this.f878d) {
            this.f878d = z;
            int size = this.f879e.size();
            for (int i = 0; i < size; i++) {
                ((ActionBar) this.f879e.get(i)).m1775a(z);
            }
        }
    }

    private Menu m2047j() {
        if (!this.f877c) {
            this.f875a.m2680a(new ToolbarActionBar(), new ToolbarActionBar());
            this.f877c = true;
        }
        return this.f875a.m2705r();
    }
}
