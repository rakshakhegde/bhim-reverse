package android.support.v7.p013a;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ActionMode;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

/* renamed from: android.support.v7.a.m */
public class AppCompatDialog extends Dialog implements AppCompatCallback {
    private AppCompatDelegate f770a;

    public AppCompatDialog(Context context, int i) {
        super(context, AppCompatDialog.m1867a(context, i));
        m1868a().m1883a(null);
        m1868a().m1898h();
    }

    protected void onCreate(Bundle bundle) {
        m1868a().m1897g();
        super.onCreate(bundle);
        m1868a().m1883a(bundle);
    }

    public void setContentView(int i) {
        m1868a().m1888b(i);
    }

    public void setContentView(View view) {
        m1868a().m1884a(view);
    }

    public void setContentView(View view, LayoutParams layoutParams) {
        m1868a().m1885a(view, layoutParams);
    }

    public View findViewById(int i) {
        return m1868a().m1881a(i);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        m1868a().m1886a(charSequence);
    }

    public void setTitle(int i) {
        super.setTitle(i);
        m1868a().m1886a(getContext().getString(i));
    }

    public void addContentView(View view, LayoutParams layoutParams) {
        m1868a().m1890b(view, layoutParams);
    }

    protected void onStop() {
        super.onStop();
        m1868a().m1891c();
    }

    public boolean m1871a(int i) {
        return m1868a().m1893c(i);
    }

    public void invalidateOptionsMenu() {
        m1868a().m1895e();
    }

    public AppCompatDelegate m1868a() {
        if (this.f770a == null) {
            this.f770a = AppCompatDelegate.m1876a((Dialog) this, (AppCompatCallback) this);
        }
        return this.f770a;
    }

    private static int m1867a(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void m1870a(ActionMode actionMode) {
    }

    public void m1872b(ActionMode actionMode) {
    }

    public ActionMode m1869a(ActionMode.ActionMode actionMode) {
        return null;
    }
}
