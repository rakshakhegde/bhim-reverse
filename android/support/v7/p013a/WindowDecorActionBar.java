package android.support.v7.p013a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.p006f.af;
import android.support.v4.p006f.au;
import android.support.v4.p006f.ay;
import android.support.v4.p006f.az;
import android.support.v4.p006f.ba;
import android.support.v7.p013a.ActionBar.ActionBar;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.ActionBarOverlayLayout.C0005a;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ac;
import android.support.v7.widget.ak;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: android.support.v7.a.r */
public class WindowDecorActionBar extends ActionBar implements C0005a {
    static final /* synthetic */ boolean f904h;
    private static final Interpolator f905i;
    private static final Interpolator f906j;
    private static final boolean f907k;
    private boolean f908A;
    private int f909B;
    private boolean f910C;
    private boolean f911D;
    private boolean f912E;
    private boolean f913F;
    private boolean f914G;
    private ViewPropertyAnimatorCompatSet f915H;
    private boolean f916I;
    WindowDecorActionBar f917a;
    ActionMode f918b;
    ActionMode.ActionMode f919c;
    boolean f920d;
    final ay f921e;
    final ay f922f;
    final ba f923g;
    private Context f924l;
    private Context f925m;
    private Activity f926n;
    private Dialog f927o;
    private ActionBarOverlayLayout f928p;
    private ActionBarContainer f929q;
    private ac f930r;
    private ActionBarContextView f931s;
    private View f932t;
    private ak f933u;
    private ArrayList<Object> f934v;
    private int f935w;
    private boolean f936x;
    private boolean f937y;
    private ArrayList<ActionBar> f938z;

    /* renamed from: android.support.v7.a.r.1 */
    class WindowDecorActionBar extends az {
        final /* synthetic */ WindowDecorActionBar f894a;

        WindowDecorActionBar(WindowDecorActionBar windowDecorActionBar) {
            this.f894a = windowDecorActionBar;
        }

        public void m2071b(View view) {
            if (this.f894a.f910C && this.f894a.f932t != null) {
                af.m1175a(this.f894a.f932t, 0.0f);
                af.m1175a(this.f894a.f929q, 0.0f);
            }
            this.f894a.f929q.setVisibility(8);
            this.f894a.f929q.setTransitioning(false);
            this.f894a.f915H = null;
            this.f894a.m2152i();
            if (this.f894a.f928p != null) {
                af.m1202n(this.f894a.f928p);
            }
        }
    }

    /* renamed from: android.support.v7.a.r.2 */
    class WindowDecorActionBar extends az {
        final /* synthetic */ WindowDecorActionBar f895a;

        WindowDecorActionBar(WindowDecorActionBar windowDecorActionBar) {
            this.f895a = windowDecorActionBar;
        }

        public void m2072b(View view) {
            this.f895a.f915H = null;
            this.f895a.f929q.requestLayout();
        }
    }

    /* renamed from: android.support.v7.a.r.3 */
    class WindowDecorActionBar implements ba {
        final /* synthetic */ WindowDecorActionBar f896a;

        WindowDecorActionBar(WindowDecorActionBar windowDecorActionBar) {
            this.f896a = windowDecorActionBar;
        }

        public void m2073a(View view) {
            ((View) this.f896a.f929q.getParent()).invalidate();
        }
    }

    /* renamed from: android.support.v7.a.r.a */
    public class WindowDecorActionBar extends ActionMode implements MenuBuilder {
        final /* synthetic */ WindowDecorActionBar f899a;
        private final Context f900b;
        private final android.support.v7.view.menu.MenuBuilder f901c;
        private ActionMode.ActionMode f902d;
        private WeakReference<View> f903e;

        public WindowDecorActionBar(WindowDecorActionBar windowDecorActionBar, Context context, ActionMode.ActionMode actionMode) {
            this.f899a = windowDecorActionBar;
            this.f900b = context;
            this.f902d = actionMode;
            this.f901c = new android.support.v7.view.menu.MenuBuilder(context).m2327a(1);
            this.f901c.m2335a((MenuBuilder) this);
        }

        public MenuInflater m2091a() {
            return new SupportMenuInflater(this.f900b);
        }

        public Menu m2098b() {
            return this.f901c;
        }

        public void m2101c() {
            if (this.f899a.f917a == this) {
                if (WindowDecorActionBar.m2120b(this.f899a.f911D, this.f899a.f912E, false)) {
                    this.f902d.m2018a(this);
                } else {
                    this.f899a.f918b = this;
                    this.f899a.f919c = this.f902d;
                }
                this.f902d = null;
                this.f899a.m2155j(false);
                this.f899a.f931s.m2469b();
                this.f899a.f930r.m2677a().sendAccessibilityEvent(32);
                this.f899a.f928p.setHideOnContentScrollEnabled(this.f899a.f920d);
                this.f899a.f917a = null;
            }
        }

        public void m2102d() {
            if (this.f899a.f917a == this) {
                this.f901c.m2359g();
                try {
                    this.f902d.m2021b(this, this.f901c);
                } finally {
                    this.f901c.m2360h();
                }
            }
        }

        public boolean m2103e() {
            this.f901c.m2359g();
            try {
                boolean a = this.f902d.m2019a((ActionMode) this, this.f901c);
                return a;
            } finally {
                this.f901c.m2360h();
            }
        }

        public void m2094a(View view) {
            this.f899a.f931s.setCustomView(view);
            this.f903e = new WeakReference(view);
        }

        public void m2095a(CharSequence charSequence) {
            this.f899a.f931s.setSubtitle(charSequence);
        }

        public void m2100b(CharSequence charSequence) {
            this.f899a.f931s.setTitle(charSequence);
        }

        public void m2092a(int i) {
            m2100b(this.f899a.f924l.getResources().getString(i));
        }

        public void m2099b(int i) {
            m2095a(this.f899a.f924l.getResources().getString(i));
        }

        public CharSequence m2104f() {
            return this.f899a.f931s.getTitle();
        }

        public CharSequence m2105g() {
            return this.f899a.f931s.getSubtitle();
        }

        public void m2096a(boolean z) {
            super.m2079a(z);
            this.f899a.f931s.setTitleOptional(z);
        }

        public boolean m2106h() {
            return this.f899a.f931s.m2471d();
        }

        public View m2107i() {
            return this.f903e != null ? (View) this.f903e.get() : null;
        }

        public boolean m2097a(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItem menuItem) {
            if (this.f902d != null) {
                return this.f902d.m2020a((ActionMode) this, menuItem);
            }
            return false;
        }

        public void m2093a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            if (this.f902d != null) {
                m2102d();
                this.f899a.f931s.m2468a();
            }
        }
    }

    static {
        boolean z = true;
        f904h = !WindowDecorActionBar.class.desiredAssertionStatus();
        f905i = new AccelerateInterpolator();
        f906j = new DecelerateInterpolator();
        if (VERSION.SDK_INT < 14) {
            z = false;
        }
        f907k = z;
    }

    public WindowDecorActionBar(Activity activity, boolean z) {
        this.f934v = new ArrayList();
        this.f935w = -1;
        this.f938z = new ArrayList();
        this.f909B = 0;
        this.f910C = true;
        this.f914G = true;
        this.f921e = new WindowDecorActionBar(this);
        this.f922f = new WindowDecorActionBar(this);
        this.f923g = new WindowDecorActionBar(this);
        this.f926n = activity;
        View decorView = activity.getWindow().getDecorView();
        m2115a(decorView);
        if (!z) {
            this.f932t = decorView.findViewById(16908290);
        }
    }

    public WindowDecorActionBar(Dialog dialog) {
        this.f934v = new ArrayList();
        this.f935w = -1;
        this.f938z = new ArrayList();
        this.f909B = 0;
        this.f910C = true;
        this.f914G = true;
        this.f921e = new WindowDecorActionBar(this);
        this.f922f = new WindowDecorActionBar(this);
        this.f923g = new WindowDecorActionBar(this);
        this.f927o = dialog;
        m2115a(dialog.getWindow().getDecorView());
    }

    private void m2115a(View view) {
        this.f928p = (ActionBarOverlayLayout) view.findViewById(R.decor_content_parent);
        if (this.f928p != null) {
            this.f928p.setActionBarVisibilityCallback(this);
        }
        this.f930r = m2118b(view.findViewById(R.action_bar));
        this.f931s = (ActionBarContextView) view.findViewById(R.action_context_bar);
        this.f929q = (ActionBarContainer) view.findViewById(R.action_bar_container);
        if (this.f930r == null || this.f931s == null || this.f929q == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f924l = this.f930r.m2686b();
        boolean z = (this.f930r.m2702o() & 4) != 0;
        if (z) {
            this.f936x = true;
        }
        ActionBarPolicy a = ActionBarPolicy.m2164a(this.f924l);
        if (a.m2170f() || z) {
            z = true;
        } else {
            z = false;
        }
        m2139a(z);
        m2128k(a.m2168d());
        TypedArray obtainStyledAttributes = this.f924l.obtainStyledAttributes(null, R.ActionBar, R.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(R.ActionBar_hideOnContentScroll, false)) {
            m2140b(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            m2134a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private ac m2118b(View view) {
        if (view instanceof ac) {
            return (ac) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException(new StringBuilder().append("Can't make a decor toolbar out of ").append(view).toString() != null ? view.getClass().getSimpleName() : "null");
    }

    public void m2134a(float f) {
        af.m1192d(this.f929q, f);
    }

    public void m2137a(Configuration configuration) {
        m2128k(ActionBarPolicy.m2164a(this.f924l).m2168d());
    }

    private void m2128k(boolean z) {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        this.f908A = z;
        if (this.f908A) {
            this.f929q.setTabContainer(null);
            this.f930r.m2681a(this.f933u);
        } else {
            this.f930r.m2681a(null);
            this.f929q.setTabContainer(this.f933u);
        }
        if (m2154j() == 2) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.f933u != null) {
            if (z2) {
                this.f933u.setVisibility(0);
                if (this.f928p != null) {
                    af.m1202n(this.f928p);
                }
            } else {
                this.f933u.setVisibility(8);
            }
        }
        ac acVar = this.f930r;
        if (this.f908A || !z2) {
            z3 = false;
        } else {
            z3 = true;
        }
        acVar.m2685a(z3);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f928p;
        if (this.f908A || !z2) {
            z4 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z4);
    }

    void m2152i() {
        if (this.f919c != null) {
            this.f919c.m2018a(this.f918b);
            this.f918b = null;
            this.f919c = null;
        }
    }

    public void m2135a(int i) {
        this.f909B = i;
    }

    public void m2145d(boolean z) {
        this.f916I = z;
        if (!z && this.f915H != null) {
            this.f915H.m2231b();
        }
    }

    public void m2146e(boolean z) {
        if (z != this.f937y) {
            this.f937y = z;
            int size = this.f938z.size();
            for (int i = 0; i < size; i++) {
                ((ActionBar) this.f938z.get(i)).m1775a(z);
            }
        }
    }

    public void m2147f(boolean z) {
        m2136a(z ? 4 : 0, 4);
    }

    public void m2139a(boolean z) {
        this.f930r.m2688b(z);
    }

    public void m2138a(CharSequence charSequence) {
        this.f930r.m2684a(charSequence);
    }

    public boolean m2150g() {
        ViewGroup a = this.f930r.m2677a();
        if (a == null || a.hasFocus()) {
            return false;
        }
        a.requestFocus();
        return true;
    }

    public void m2136a(int i, int i2) {
        int o = this.f930r.m2702o();
        if ((i2 & 4) != 0) {
            this.f936x = true;
        }
        this.f930r.m2689c((o & (i2 ^ -1)) | (i & i2));
    }

    public int m2154j() {
        return this.f930r.m2703p();
    }

    public int m2132a() {
        return this.f930r.m2702o();
    }

    public ActionMode m2133a(ActionMode.ActionMode actionMode) {
        if (this.f917a != null) {
            this.f917a.m2101c();
        }
        this.f928p.setHideOnContentScrollEnabled(false);
        this.f931s.m2470c();
        ActionMode windowDecorActionBar = new WindowDecorActionBar(this, this.f931s.getContext(), actionMode);
        if (!windowDecorActionBar.m2103e()) {
            return null;
        }
        windowDecorActionBar.m2102d();
        this.f931s.m2467a(windowDecorActionBar);
        m2155j(true);
        this.f931s.sendAccessibilityEvent(32);
        this.f917a = windowDecorActionBar;
        return windowDecorActionBar;
    }

    public int m2156k() {
        return this.f929q.getHeight();
    }

    public void m2149g(boolean z) {
        this.f910C = z;
    }

    private void m2130p() {
        if (!this.f913F) {
            this.f913F = true;
            if (this.f928p != null) {
                this.f928p.setShowingForActionMode(true);
            }
            m2129l(false);
        }
    }

    public void m2157l() {
        if (this.f912E) {
            this.f912E = false;
            m2129l(true);
        }
    }

    private void m2131q() {
        if (this.f913F) {
            this.f913F = false;
            if (this.f928p != null) {
                this.f928p.setShowingForActionMode(false);
            }
            m2129l(false);
        }
    }

    public void m2158m() {
        if (!this.f912E) {
            this.f912E = true;
            m2129l(true);
        }
    }

    public void m2140b(boolean z) {
        if (!z || this.f928p.m2500a()) {
            this.f920d = z;
            this.f928p.setHideOnContentScrollEnabled(z);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public int m2144d() {
        return this.f928p.getActionBarHideOffset();
    }

    private static boolean m2120b(boolean z, boolean z2, boolean z3) {
        if (z3) {
            return true;
        }
        if (z || z2) {
            return false;
        }
        return true;
    }

    private void m2129l(boolean z) {
        if (WindowDecorActionBar.m2120b(this.f911D, this.f912E, this.f913F)) {
            if (!this.f914G) {
                this.f914G = true;
                m2151h(z);
            }
        } else if (this.f914G) {
            this.f914G = false;
            m2153i(z);
        }
    }

    public void m2151h(boolean z) {
        if (this.f915H != null) {
            this.f915H.m2231b();
        }
        this.f929q.setVisibility(0);
        if (this.f909B == 0 && f907k && (this.f916I || z)) {
            af.m1175a(this.f929q, 0.0f);
            float f = (float) (-this.f929q.getHeight());
            if (z) {
                int[] iArr = new int[]{0, 0};
                this.f929q.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            af.m1175a(this.f929q, f);
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            au b = af.m1199k(this.f929q).m1358b(0.0f);
            b.m1356a(this.f923g);
            viewPropertyAnimatorCompatSet.m2226a(b);
            if (this.f910C && this.f932t != null) {
                af.m1175a(this.f932t, f);
                viewPropertyAnimatorCompatSet.m2226a(af.m1199k(this.f932t).m1358b(0.0f));
            }
            viewPropertyAnimatorCompatSet.m2229a(f906j);
            viewPropertyAnimatorCompatSet.m2225a(250);
            viewPropertyAnimatorCompatSet.m2228a(this.f922f);
            this.f915H = viewPropertyAnimatorCompatSet;
            viewPropertyAnimatorCompatSet.m2230a();
        } else {
            af.m1187b(this.f929q, 1.0f);
            af.m1175a(this.f929q, 0.0f);
            if (this.f910C && this.f932t != null) {
                af.m1175a(this.f932t, 0.0f);
            }
            this.f922f.m1328b(null);
        }
        if (this.f928p != null) {
            af.m1202n(this.f928p);
        }
    }

    public void m2153i(boolean z) {
        if (this.f915H != null) {
            this.f915H.m2231b();
        }
        if (this.f909B == 0 && f907k && (this.f916I || z)) {
            af.m1187b(this.f929q, 1.0f);
            this.f929q.setTransitioning(true);
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            float f = (float) (-this.f929q.getHeight());
            if (z) {
                int[] iArr = new int[]{0, 0};
                this.f929q.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            au b = af.m1199k(this.f929q).m1358b(f);
            b.m1356a(this.f923g);
            viewPropertyAnimatorCompatSet.m2226a(b);
            if (this.f910C && this.f932t != null) {
                viewPropertyAnimatorCompatSet.m2226a(af.m1199k(this.f932t).m1358b(f));
            }
            viewPropertyAnimatorCompatSet.m2229a(f905i);
            viewPropertyAnimatorCompatSet.m2225a(250);
            viewPropertyAnimatorCompatSet.m2228a(this.f921e);
            this.f915H = viewPropertyAnimatorCompatSet;
            viewPropertyAnimatorCompatSet.m2230a();
            return;
        }
        this.f921e.m1328b(null);
    }

    public boolean m2141b() {
        int k = m2156k();
        return this.f914G && (k == 0 || m2144d() < k);
    }

    public void m2155j(boolean z) {
        au a;
        au a2;
        if (z) {
            m2130p();
        } else {
            m2131q();
        }
        if (z) {
            a = this.f930r.m2676a(4, 100);
            a2 = this.f931s.m2466a(0, 200);
        } else {
            a2 = this.f930r.m2676a(0, 200);
            a = this.f931s.m2466a(8, 100);
        }
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
        viewPropertyAnimatorCompatSet.m2227a(a, a2);
        viewPropertyAnimatorCompatSet.m2230a();
    }

    public Context m2142c() {
        if (this.f925m == null) {
            TypedValue typedValue = new TypedValue();
            this.f924l.getTheme().resolveAttribute(R.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                this.f925m = new ContextThemeWrapper(this.f924l, i);
            } else {
                this.f925m = this.f924l;
            }
        }
        return this.f925m;
    }

    public void m2159n() {
        if (this.f915H != null) {
            this.f915H.m2231b();
            this.f915H = null;
        }
    }

    public void m2160o() {
    }

    public boolean m2148f() {
        if (this.f930r == null || !this.f930r.m2690c()) {
            return false;
        }
        this.f930r.m2691d();
        return true;
    }

    public void m2143c(boolean z) {
        if (!this.f936x) {
            m2147f(z);
        }
    }
}
