package android.support.v7.p013a;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.support.v4.p006f.af;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.NestedScrollView.C0003b;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.lang.ref.WeakReference;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.c */
class AlertController {
    private TextView f728A;
    private TextView f729B;
    private View f730C;
    private ListAdapter f731D;
    private int f732E;
    private int f733F;
    private int f734G;
    private int f735H;
    private int f736I;
    private int f737J;
    private int f738K;
    private int f739L;
    private Handler f740M;
    private final OnClickListener f741N;
    private final Context f742a;
    private final AppCompatDialog f743b;
    private final Window f744c;
    private CharSequence f745d;
    private CharSequence f746e;
    private ListView f747f;
    private View f748g;
    private int f749h;
    private int f750i;
    private int f751j;
    private int f752k;
    private int f753l;
    private boolean f754m;
    private Button f755n;
    private CharSequence f756o;
    private Message f757p;
    private Button f758q;
    private CharSequence f759r;
    private Message f760s;
    private Button f761t;
    private CharSequence f762u;
    private Message f763v;
    private NestedScrollView f764w;
    private int f765x;
    private Drawable f766y;
    private ImageView f767z;

    /* renamed from: android.support.v7.a.c.1 */
    class AlertController implements OnClickListener {
        final /* synthetic */ AlertController f663a;

        AlertController(AlertController alertController) {
            this.f663a = alertController;
        }

        public void onClick(View view) {
            Message obtain;
            if (view == this.f663a.f755n && this.f663a.f757p != null) {
                obtain = Message.obtain(this.f663a.f757p);
            } else if (view == this.f663a.f758q && this.f663a.f760s != null) {
                obtain = Message.obtain(this.f663a.f760s);
            } else if (view != this.f663a.f761t || this.f663a.f763v == null) {
                obtain = null;
            } else {
                obtain = Message.obtain(this.f663a.f763v);
            }
            if (obtain != null) {
                obtain.sendToTarget();
            }
            this.f663a.f740M.obtainMessage(1, this.f663a.f743b).sendToTarget();
        }
    }

    /* renamed from: android.support.v7.a.c.2 */
    class AlertController implements C0003b {
        final /* synthetic */ View f664a;
        final /* synthetic */ View f665b;
        final /* synthetic */ AlertController f666c;

        AlertController(AlertController alertController, View view, View view2) {
            this.f666c = alertController;
            this.f664a = view;
            this.f665b = view2;
        }

        public void m1815a(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
            AlertController.m1830b(nestedScrollView, this.f664a, this.f665b);
        }
    }

    /* renamed from: android.support.v7.a.c.3 */
    class AlertController implements Runnable {
        final /* synthetic */ View f667a;
        final /* synthetic */ View f668b;
        final /* synthetic */ AlertController f669c;

        AlertController(AlertController alertController, View view, View view2) {
            this.f669c = alertController;
            this.f667a = view;
            this.f668b = view2;
        }

        public void run() {
            AlertController.m1830b(this.f669c.f764w, this.f667a, this.f668b);
        }
    }

    /* renamed from: android.support.v7.a.c.4 */
    class AlertController implements OnScrollListener {
        final /* synthetic */ View f670a;
        final /* synthetic */ View f671b;
        final /* synthetic */ AlertController f672c;

        AlertController(AlertController alertController, View view, View view2) {
            this.f672c = alertController;
            this.f670a = view;
            this.f671b = view2;
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            AlertController.m1830b(absListView, this.f670a, this.f671b);
        }
    }

    /* renamed from: android.support.v7.a.c.5 */
    class AlertController implements Runnable {
        final /* synthetic */ View f673a;
        final /* synthetic */ View f674b;
        final /* synthetic */ AlertController f675c;

        AlertController(AlertController alertController, View view, View view2) {
            this.f675c = alertController;
            this.f673a = view;
            this.f674b = view2;
        }

        public void run() {
            AlertController.m1830b(this.f675c.f747f, this.f673a, this.f674b);
        }
    }

    /* renamed from: android.support.v7.a.c.a */
    public static class AlertController {
        public int f688A;
        public boolean f689B;
        public boolean[] f690C;
        public boolean f691D;
        public boolean f692E;
        public int f693F;
        public OnMultiChoiceClickListener f694G;
        public Cursor f695H;
        public String f696I;
        public String f697J;
        public OnItemSelectedListener f698K;
        public AlertController f699L;
        public boolean f700M;
        public final Context f701a;
        public final LayoutInflater f702b;
        public int f703c;
        public Drawable f704d;
        public int f705e;
        public CharSequence f706f;
        public View f707g;
        public CharSequence f708h;
        public CharSequence f709i;
        public DialogInterface.OnClickListener f710j;
        public CharSequence f711k;
        public DialogInterface.OnClickListener f712l;
        public CharSequence f713m;
        public DialogInterface.OnClickListener f714n;
        public boolean f715o;
        public OnCancelListener f716p;
        public OnDismissListener f717q;
        public OnKeyListener f718r;
        public CharSequence[] f719s;
        public ListAdapter f720t;
        public DialogInterface.OnClickListener f721u;
        public int f722v;
        public View f723w;
        public int f724x;
        public int f725y;
        public int f726z;

        /* renamed from: android.support.v7.a.c.a.1 */
        class AlertController extends ArrayAdapter<CharSequence> {
            final /* synthetic */ ListView f676a;
            final /* synthetic */ AlertController f677b;

            AlertController(AlertController alertController, Context context, int i, int i2, CharSequence[] charSequenceArr, ListView listView) {
                this.f677b = alertController;
                this.f676a = listView;
                super(context, i, i2, charSequenceArr);
            }

            public View getView(int i, View view, ViewGroup viewGroup) {
                View view2 = super.getView(i, view, viewGroup);
                if (this.f677b.f690C != null && this.f677b.f690C[i]) {
                    this.f676a.setItemChecked(i, true);
                }
                return view2;
            }
        }

        /* renamed from: android.support.v7.a.c.a.2 */
        class AlertController extends CursorAdapter {
            final /* synthetic */ ListView f678a;
            final /* synthetic */ AlertController f679b;
            final /* synthetic */ AlertController f680c;
            private final int f681d;
            private final int f682e;

            AlertController(AlertController alertController, Context context, Cursor cursor, boolean z, ListView listView, AlertController alertController2) {
                this.f680c = alertController;
                this.f678a = listView;
                this.f679b = alertController2;
                super(context, cursor, z);
                Cursor cursor2 = getCursor();
                this.f681d = cursor2.getColumnIndexOrThrow(this.f680c.f696I);
                this.f682e = cursor2.getColumnIndexOrThrow(this.f680c.f697J);
            }

            public void bindView(View view, Context context, Cursor cursor) {
                ((CheckedTextView) view.findViewById(16908308)).setText(cursor.getString(this.f681d));
                this.f678a.setItemChecked(cursor.getPosition(), cursor.getInt(this.f682e) == 1);
            }

            public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                return this.f680c.f702b.inflate(this.f679b.f736I, viewGroup, false);
            }
        }

        /* renamed from: android.support.v7.a.c.a.3 */
        class AlertController implements OnItemClickListener {
            final /* synthetic */ AlertController f683a;
            final /* synthetic */ AlertController f684b;

            AlertController(AlertController alertController, AlertController alertController2) {
                this.f684b = alertController;
                this.f683a = alertController2;
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                this.f684b.f721u.onClick(this.f683a.f743b, i);
                if (!this.f684b.f692E) {
                    this.f683a.f743b.dismiss();
                }
            }
        }

        /* renamed from: android.support.v7.a.c.a.4 */
        class AlertController implements OnItemClickListener {
            final /* synthetic */ ListView f685a;
            final /* synthetic */ AlertController f686b;
            final /* synthetic */ AlertController f687c;

            AlertController(AlertController alertController, ListView listView, AlertController alertController2) {
                this.f687c = alertController;
                this.f685a = listView;
                this.f686b = alertController2;
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (this.f687c.f690C != null) {
                    this.f687c.f690C[i] = this.f685a.isItemChecked(i);
                }
                this.f687c.f694G.onClick(this.f686b.f743b, i, this.f685a.isItemChecked(i));
            }
        }

        /* renamed from: android.support.v7.a.c.a.a */
        public interface AlertController {
            void m1816a(ListView listView);
        }

        public AlertController(Context context) {
            this.f703c = 0;
            this.f705e = 0;
            this.f689B = false;
            this.f693F = -1;
            this.f700M = true;
            this.f701a = context;
            this.f715o = true;
            this.f702b = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public void m1818a(AlertController alertController) {
            if (this.f707g != null) {
                alertController.m1855b(this.f707g);
            } else {
                if (this.f706f != null) {
                    alertController.m1852a(this.f706f);
                }
                if (this.f704d != null) {
                    alertController.m1850a(this.f704d);
                }
                if (this.f703c != 0) {
                    alertController.m1854b(this.f703c);
                }
                if (this.f705e != 0) {
                    alertController.m1854b(alertController.m1858c(this.f705e));
                }
            }
            if (this.f708h != null) {
                alertController.m1856b(this.f708h);
            }
            if (this.f709i != null) {
                alertController.m1849a(-1, this.f709i, this.f710j, null);
            }
            if (this.f711k != null) {
                alertController.m1849a(-2, this.f711k, this.f712l, null);
            }
            if (this.f713m != null) {
                alertController.m1849a(-3, this.f713m, this.f714n, null);
            }
            if (!(this.f719s == null && this.f695H == null && this.f720t == null)) {
                m1817b(alertController);
            }
            if (this.f723w != null) {
                if (this.f689B) {
                    alertController.m1851a(this.f723w, this.f724x, this.f725y, this.f726z, this.f688A);
                    return;
                }
                alertController.m1859c(this.f723w);
            } else if (this.f722v != 0) {
                alertController.m1848a(this.f722v);
            }
        }

        private void m1817b(AlertController alertController) {
            ListAdapter simpleCursorAdapter;
            ListView listView = (ListView) this.f702b.inflate(alertController.f735H, null);
            if (!this.f691D) {
                int m;
                if (this.f692E) {
                    m = alertController.f737J;
                } else {
                    m = alertController.f738K;
                }
                if (this.f695H != null) {
                    simpleCursorAdapter = new SimpleCursorAdapter(this.f701a, m, this.f695H, new String[]{this.f696I}, new int[]{16908308});
                } else if (this.f720t != null) {
                    simpleCursorAdapter = this.f720t;
                } else {
                    simpleCursorAdapter = new AlertController(this.f701a, m, 16908308, this.f719s);
                }
            } else if (this.f695H == null) {
                simpleCursorAdapter = new AlertController(this, this.f701a, alertController.f736I, 16908308, this.f719s, listView);
            } else {
                Object alertController2 = new AlertController(this, this.f701a, this.f695H, false, listView, alertController);
            }
            if (this.f699L != null) {
                this.f699L.m1816a(listView);
            }
            alertController.f731D = simpleCursorAdapter;
            alertController.f732E = this.f693F;
            if (this.f721u != null) {
                listView.setOnItemClickListener(new AlertController(this, alertController));
            } else if (this.f694G != null) {
                listView.setOnItemClickListener(new AlertController(this, listView, alertController));
            }
            if (this.f698K != null) {
                listView.setOnItemSelectedListener(this.f698K);
            }
            if (this.f692E) {
                listView.setChoiceMode(1);
            } else if (this.f691D) {
                listView.setChoiceMode(2);
            }
            alertController.f747f = listView;
        }
    }

    /* renamed from: android.support.v7.a.c.b */
    private static final class AlertController extends Handler {
        private WeakReference<DialogInterface> f727a;

        public AlertController(DialogInterface dialogInterface) {
            this.f727a = new WeakReference(dialogInterface);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) message.obj).onClick((DialogInterface) this.f727a.get(), message.what);
                case R.View_android_focusable /*1*/:
                    ((DialogInterface) message.obj).dismiss();
                default:
            }
        }
    }

    /* renamed from: android.support.v7.a.c.c */
    private static class AlertController extends ArrayAdapter<CharSequence> {
        public AlertController(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }

        public boolean hasStableIds() {
            return true;
        }

        public long getItemId(int i) {
            return (long) i;
        }
    }

    public AlertController(Context context, AppCompatDialog appCompatDialog, Window window) {
        this.f754m = false;
        this.f765x = 0;
        this.f732E = -1;
        this.f739L = 0;
        this.f741N = new AlertController(this);
        this.f742a = context;
        this.f743b = appCompatDialog;
        this.f744c = window;
        this.f740M = new AlertController(appCompatDialog);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, android.support.v7.p014b.R.R.AlertDialog, android.support.v7.p014b.R.R.alertDialogStyle, 0);
        this.f733F = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_android_layout, 0);
        this.f734G = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_buttonPanelSideLayout, 0);
        this.f735H = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_listLayout, 0);
        this.f736I = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_multiChoiceItemLayout, 0);
        this.f737J = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_singleChoiceItemLayout, 0);
        this.f738K = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AlertDialog_listItemLayout, 0);
        obtainStyledAttributes.recycle();
        appCompatDialog.m1871a(1);
    }

    static boolean m1827a(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (AlertController.m1827a(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public void m1847a() {
        this.f743b.setContentView(m1828b());
        m1833c();
    }

    private int m1828b() {
        if (this.f734G == 0) {
            return this.f733F;
        }
        if (this.f739L == 1) {
            return this.f734G;
        }
        return this.f733F;
    }

    public void m1852a(CharSequence charSequence) {
        this.f745d = charSequence;
        if (this.f728A != null) {
            this.f728A.setText(charSequence);
        }
    }

    public void m1855b(View view) {
        this.f730C = view;
    }

    public void m1856b(CharSequence charSequence) {
        this.f746e = charSequence;
        if (this.f729B != null) {
            this.f729B.setText(charSequence);
        }
    }

    public void m1848a(int i) {
        this.f748g = null;
        this.f749h = i;
        this.f754m = false;
    }

    public void m1859c(View view) {
        this.f748g = view;
        this.f749h = 0;
        this.f754m = false;
    }

    public void m1851a(View view, int i, int i2, int i3, int i4) {
        this.f748g = view;
        this.f749h = 0;
        this.f754m = true;
        this.f750i = i;
        this.f751j = i2;
        this.f752k = i3;
        this.f753l = i4;
    }

    public void m1849a(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (message == null && onClickListener != null) {
            message = this.f740M.obtainMessage(i, onClickListener);
        }
        switch (i) {
            case -3:
                this.f762u = charSequence;
                this.f763v = message;
            case -2:
                this.f759r = charSequence;
                this.f760s = message;
            case -1:
                this.f756o = charSequence;
                this.f757p = message;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    public void m1854b(int i) {
        this.f766y = null;
        this.f765x = i;
        if (this.f767z == null) {
            return;
        }
        if (i != 0) {
            this.f767z.setVisibility(0);
            this.f767z.setImageResource(this.f765x);
            return;
        }
        this.f767z.setVisibility(8);
    }

    public void m1850a(Drawable drawable) {
        this.f766y = drawable;
        this.f765x = 0;
        if (this.f767z == null) {
            return;
        }
        if (drawable != null) {
            this.f767z.setVisibility(0);
            this.f767z.setImageDrawable(drawable);
            return;
        }
        this.f767z.setVisibility(8);
    }

    public int m1858c(int i) {
        TypedValue typedValue = new TypedValue();
        this.f742a.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue.resourceId;
    }

    public boolean m1853a(int i, KeyEvent keyEvent) {
        return this.f764w != null && this.f764w.m1565a(keyEvent);
    }

    public boolean m1857b(int i, KeyEvent keyEvent) {
        return this.f764w != null && this.f764w.m1565a(keyEvent);
    }

    private ViewGroup m1820a(View view, View view2) {
        View inflate;
        if (view == null) {
            if (view2 instanceof ViewStub) {
                inflate = ((ViewStub) view2).inflate();
            } else {
                inflate = view2;
            }
            return (ViewGroup) inflate;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            inflate = ((ViewStub) view).inflate();
        } else {
            inflate = view;
        }
        return (ViewGroup) inflate;
    }

    private void m1833c() {
        boolean z;
        boolean z2;
        View findViewById = this.f744c.findViewById(android.support.v7.p014b.R.R.parentPanel);
        View findViewById2 = findViewById.findViewById(android.support.v7.p014b.R.R.topPanel);
        View findViewById3 = findViewById.findViewById(android.support.v7.p014b.R.R.contentPanel);
        View findViewById4 = findViewById.findViewById(android.support.v7.p014b.R.R.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById.findViewById(android.support.v7.p014b.R.R.customPanel);
        m1825a(viewGroup);
        View findViewById5 = viewGroup.findViewById(android.support.v7.p014b.R.R.topPanel);
        View findViewById6 = viewGroup.findViewById(android.support.v7.p014b.R.R.contentPanel);
        View findViewById7 = viewGroup.findViewById(android.support.v7.p014b.R.R.buttonPanel);
        ViewGroup a = m1820a(findViewById5, findViewById2);
        ViewGroup a2 = m1820a(findViewById6, findViewById3);
        ViewGroup a3 = m1820a(findViewById7, findViewById4);
        m1834c(a2);
        m1836d(a3);
        m1831b(a);
        boolean z3 = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        if (a == null || a.getVisibility() == 8) {
            z = false;
        } else {
            z = true;
        }
        if (a3 == null || a3.getVisibility() == 8) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (!(z2 || a2 == null)) {
            findViewById3 = a2.findViewById(android.support.v7.p014b.R.R.textSpacerNoButtons);
            if (findViewById3 != null) {
                findViewById3.setVisibility(0);
            }
        }
        if (z && this.f764w != null) {
            this.f764w.setClipToPadding(true);
        }
        if (!z3) {
            findViewById3 = this.f747f != null ? this.f747f : this.f764w;
            if (findViewById3 != null) {
                int i;
                if (z) {
                    i = 1;
                } else {
                    i = 0;
                }
                m1826a(a2, findViewById3, (z2 ? 2 : 0) | i, 3);
            }
        }
        ListView listView = this.f747f;
        if (listView != null && this.f731D != null) {
            listView.setAdapter(this.f731D);
            int i2 = this.f732E;
            if (i2 > -1) {
                listView.setItemChecked(i2, true);
                listView.setSelection(i2);
            }
        }
    }

    private void m1826a(ViewGroup viewGroup, View view, int i, int i2) {
        View view2 = null;
        View findViewById = this.f744c.findViewById(android.support.v7.p014b.R.R.scrollIndicatorUp);
        View findViewById2 = this.f744c.findViewById(android.support.v7.p014b.R.R.scrollIndicatorDown);
        if (VERSION.SDK_INT >= 23) {
            af.m1176a(view, i, i2);
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup.removeView(findViewById2);
                return;
            }
            return;
        }
        if (findViewById != null && (i & 1) == 0) {
            viewGroup.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 == null || (i & 2) != 0) {
            view2 = findViewById2;
        } else {
            viewGroup.removeView(findViewById2);
        }
        if (findViewById != null || view2 != null) {
            if (this.f746e != null) {
                this.f764w.setOnScrollChangeListener(new AlertController(this, findViewById, view2));
                this.f764w.post(new AlertController(this, findViewById, view2));
            } else if (this.f747f != null) {
                this.f747f.setOnScrollListener(new AlertController(this, findViewById, view2));
                this.f747f.post(new AlertController(this, findViewById, view2));
            } else {
                if (findViewById != null) {
                    viewGroup.removeView(findViewById);
                }
                if (view2 != null) {
                    viewGroup.removeView(view2);
                }
            }
        }
    }

    private void m1825a(ViewGroup viewGroup) {
        View view;
        boolean z = false;
        if (this.f748g != null) {
            view = this.f748g;
        } else if (this.f749h != 0) {
            view = LayoutInflater.from(this.f742a).inflate(this.f749h, viewGroup, false);
        } else {
            view = null;
        }
        if (view != null) {
            z = true;
        }
        if (!(z && AlertController.m1827a(view))) {
            this.f744c.setFlags(131072, 131072);
        }
        if (z) {
            FrameLayout frameLayout = (FrameLayout) this.f744c.findViewById(android.support.v7.p014b.R.R.custom);
            frameLayout.addView(view, new LayoutParams(-1, -1));
            if (this.f754m) {
                frameLayout.setPadding(this.f750i, this.f751j, this.f752k, this.f753l);
            }
            if (this.f747f != null) {
                ((LinearLayout.LayoutParams) viewGroup.getLayoutParams()).weight = 0.0f;
                return;
            }
            return;
        }
        viewGroup.setVisibility(8);
    }

    private void m1831b(ViewGroup viewGroup) {
        if (this.f730C != null) {
            viewGroup.addView(this.f730C, 0, new LayoutParams(-1, -2));
            this.f744c.findViewById(android.support.v7.p014b.R.R.title_template).setVisibility(8);
            return;
        }
        this.f767z = (ImageView) this.f744c.findViewById(16908294);
        if ((!TextUtils.isEmpty(this.f745d) ? 1 : 0) != 0) {
            this.f728A = (TextView) this.f744c.findViewById(android.support.v7.p014b.R.R.alertTitle);
            this.f728A.setText(this.f745d);
            if (this.f765x != 0) {
                this.f767z.setImageResource(this.f765x);
                return;
            } else if (this.f766y != null) {
                this.f767z.setImageDrawable(this.f766y);
                return;
            } else {
                this.f728A.setPadding(this.f767z.getPaddingLeft(), this.f767z.getPaddingTop(), this.f767z.getPaddingRight(), this.f767z.getPaddingBottom());
                this.f767z.setVisibility(8);
                return;
            }
        }
        this.f744c.findViewById(android.support.v7.p014b.R.R.title_template).setVisibility(8);
        this.f767z.setVisibility(8);
        viewGroup.setVisibility(8);
    }

    private void m1834c(ViewGroup viewGroup) {
        this.f764w = (NestedScrollView) this.f744c.findViewById(android.support.v7.p014b.R.R.scrollView);
        this.f764w.setFocusable(false);
        this.f764w.setNestedScrollingEnabled(false);
        this.f729B = (TextView) viewGroup.findViewById(16908299);
        if (this.f729B != null) {
            if (this.f746e != null) {
                this.f729B.setText(this.f746e);
                return;
            }
            this.f729B.setVisibility(8);
            this.f764w.removeView(this.f729B);
            if (this.f747f != null) {
                ViewGroup viewGroup2 = (ViewGroup) this.f764w.getParent();
                int indexOfChild = viewGroup2.indexOfChild(this.f764w);
                viewGroup2.removeViewAt(indexOfChild);
                viewGroup2.addView(this.f747f, indexOfChild, new LayoutParams(-1, -1));
                return;
            }
            viewGroup.setVisibility(8);
        }
    }

    private static void m1830b(View view, View view2, View view3) {
        int i = 0;
        if (view2 != null) {
            view2.setVisibility(af.m1185a(view, -1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!af.m1185a(view, 1)) {
                i = 4;
            }
            view3.setVisibility(i);
        }
    }

    private void m1836d(ViewGroup viewGroup) {
        int i;
        int i2 = 1;
        this.f755n = (Button) viewGroup.findViewById(16908313);
        this.f755n.setOnClickListener(this.f741N);
        if (TextUtils.isEmpty(this.f756o)) {
            this.f755n.setVisibility(8);
            i = 0;
        } else {
            this.f755n.setText(this.f756o);
            this.f755n.setVisibility(0);
            i = 1;
        }
        this.f758q = (Button) viewGroup.findViewById(16908314);
        this.f758q.setOnClickListener(this.f741N);
        if (TextUtils.isEmpty(this.f759r)) {
            this.f758q.setVisibility(8);
        } else {
            this.f758q.setText(this.f759r);
            this.f758q.setVisibility(0);
            i |= 2;
        }
        this.f761t = (Button) viewGroup.findViewById(16908315);
        this.f761t.setOnClickListener(this.f741N);
        if (TextUtils.isEmpty(this.f762u)) {
            this.f761t.setVisibility(8);
        } else {
            this.f761t.setText(this.f762u);
            this.f761t.setVisibility(0);
            i |= 4;
        }
        if (i == 0) {
            i2 = 0;
        }
        if (i2 == 0) {
            viewGroup.setVisibility(8);
        }
    }
}
