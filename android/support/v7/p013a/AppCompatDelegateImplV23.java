package android.support.v7.p013a;

import android.app.UiModeManager;
import android.content.Context;
import android.support.v7.p013a.AppCompatDelegateImplV14.AppCompatDelegateImplV14;
import android.view.ActionMode;
import android.view.Window;
import android.view.Window.Callback;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.k */
class AppCompatDelegateImplV23 extends AppCompatDelegateImplV14 {
    private final UiModeManager f821r;

    /* renamed from: android.support.v7.a.k.a */
    class AppCompatDelegateImplV23 extends AppCompatDelegateImplV14 {
        final /* synthetic */ AppCompatDelegateImplV23 f820c;

        AppCompatDelegateImplV23(AppCompatDelegateImplV23 appCompatDelegateImplV23, Callback callback) {
            this.f820c = appCompatDelegateImplV23;
            super(appCompatDelegateImplV23, callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (this.f820c.m1999n()) {
                switch (i) {
                    case R.View_android_theme /*0*/:
                        return m1991a(callback);
                }
            }
            return super.onWindowStartingActionMode(callback, i);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return null;
        }
    }

    AppCompatDelegateImplV23(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
        this.f821r = (UiModeManager) context.getSystemService("uimode");
    }

    Callback m2000a(Callback callback) {
        return new AppCompatDelegateImplV23(this, callback);
    }

    int m2001d(int i) {
        if (i == 0 && this.f821r.getNightMode() == 0) {
            return -1;
        }
        return super.m1997d(i);
    }
}
