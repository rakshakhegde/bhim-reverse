package android.support.v7.p013a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;

/* renamed from: android.support.v7.a.g */
public abstract class AppCompatDelegate {
    private static int f772a;
    private static boolean f773b;

    public abstract ActionBar m1880a();

    public abstract View m1881a(int i);

    public abstract void m1882a(Configuration configuration);

    public abstract void m1883a(Bundle bundle);

    public abstract void m1884a(View view);

    public abstract void m1885a(View view, LayoutParams layoutParams);

    public abstract void m1886a(CharSequence charSequence);

    public abstract MenuInflater m1887b();

    public abstract void m1888b(int i);

    public abstract void m1889b(Bundle bundle);

    public abstract void m1890b(View view, LayoutParams layoutParams);

    public abstract void m1891c();

    public abstract void m1892c(Bundle bundle);

    public abstract boolean m1893c(int i);

    public abstract void m1894d();

    public abstract void m1895e();

    public abstract void m1896f();

    public abstract void m1897g();

    public abstract boolean m1898h();

    static {
        f772a = -1;
        f773b = false;
    }

    public static AppCompatDelegate m1875a(Activity activity, AppCompatCallback appCompatCallback) {
        return AppCompatDelegate.m1877a(activity, activity.getWindow(), appCompatCallback);
    }

    public static AppCompatDelegate m1876a(Dialog dialog, AppCompatCallback appCompatCallback) {
        return AppCompatDelegate.m1877a(dialog.getContext(), dialog.getWindow(), appCompatCallback);
    }

    private static AppCompatDelegate m1877a(Context context, Window window, AppCompatCallback appCompatCallback) {
        int i = VERSION.SDK_INT;
        if (i >= 23) {
            return new AppCompatDelegateImplV23(context, window, appCompatCallback);
        }
        if (i >= 14) {
            return new AppCompatDelegateImplV14(context, window, appCompatCallback);
        }
        if (i >= 11) {
            return new AppCompatDelegateImplV11(context, window, appCompatCallback);
        }
        return new AppCompatDelegateImplV7(context, window, appCompatCallback);
    }

    AppCompatDelegate() {
    }

    public static int m1878i() {
        return f772a;
    }

    public static boolean m1879j() {
        return f773b;
    }
}
