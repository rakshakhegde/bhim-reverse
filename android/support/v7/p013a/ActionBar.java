package android.support.v7.p013a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.p014b.R.R;
import android.support.v7.view.ActionMode;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

/* renamed from: android.support.v7.a.a */
public abstract class ActionBar {

    /* renamed from: android.support.v7.a.a.a */
    public static class ActionBar extends MarginLayoutParams {
        public int f658a;

        public ActionBar(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f658a = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.ActionBarLayout);
            this.f658a = obtainStyledAttributes.getInt(R.ActionBarLayout_android_layout_gravity, 0);
            obtainStyledAttributes.recycle();
        }

        public ActionBar(int i, int i2) {
            super(i, i2);
            this.f658a = 0;
            this.f658a = 8388627;
        }

        public ActionBar(ActionBar actionBar) {
            super(actionBar);
            this.f658a = 0;
            this.f658a = actionBar.f658a;
        }

        public ActionBar(LayoutParams layoutParams) {
            super(layoutParams);
            this.f658a = 0;
        }
    }

    /* renamed from: android.support.v7.a.a.b */
    public interface ActionBar {
        void m1775a(boolean z);
    }

    /* renamed from: android.support.v7.a.a.c */
    public static abstract class ActionBar {
        public abstract Drawable m1776a();

        public abstract CharSequence m1777b();

        public abstract View m1778c();

        public abstract void m1779d();

        public abstract CharSequence m1780e();
    }

    public abstract int m1781a();

    public abstract boolean m1789b();

    public void m1786a(boolean z) {
    }

    public Context m1790c() {
        return null;
    }

    public void m1788b(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
        }
    }

    public int m1792d() {
        return 0;
    }

    public void m1783a(float f) {
        if (f != 0.0f) {
            throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
        }
    }

    public void m1791c(boolean z) {
    }

    public void m1793d(boolean z) {
    }

    public void m1784a(Configuration configuration) {
    }

    public void m1794e(boolean z) {
    }

    public ActionMode m1782a(ActionMode.ActionMode actionMode) {
        return null;
    }

    public boolean m1795e() {
        return false;
    }

    public boolean m1787a(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean m1796f() {
        return false;
    }

    public void m1785a(CharSequence charSequence) {
    }

    boolean m1797g() {
        return false;
    }

    void m1798h() {
    }
}
