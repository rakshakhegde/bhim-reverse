package android.support.v7.p013a;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.p006f.af;
import android.support.v4.p010e.ArrayMap;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.aa;
import android.support.v7.widget.ao;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import com.crashlytics.android.core.BuildConfig;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.n */
class AppCompatViewInflater {
    private static final Class<?>[] f866a;
    private static final int[] f867b;
    private static final String[] f868c;
    private static final Map<String, Constructor<? extends View>> f869d;
    private final Object[] f870e;

    /* renamed from: android.support.v7.a.n.a */
    private static class AppCompatViewInflater implements OnClickListener {
        private final View f862a;
        private final String f863b;
        private Method f864c;
        private Context f865d;

        public AppCompatViewInflater(View view, String str) {
            this.f862a = view;
            this.f863b = str;
        }

        public void onClick(View view) {
            if (this.f864c == null) {
                m2035a(this.f862a.getContext(), this.f863b);
            }
            try {
                this.f864c.invoke(this.f865d, new Object[]{view});
            } catch (Throwable e) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", e);
            } catch (Throwable e2) {
                throw new IllegalStateException("Could not execute method for android:onClick", e2);
            }
        }

        private void m2035a(Context context, String str) {
            Context context2 = context;
            while (context2 != null) {
                try {
                    if (!context2.isRestricted()) {
                        Method method = context2.getClass().getMethod(this.f863b, new Class[]{View.class});
                        if (method != null) {
                            this.f864c = method;
                            this.f865d = context2;
                            return;
                        }
                    }
                } catch (NoSuchMethodException e) {
                }
                if (context2 instanceof ContextWrapper) {
                    context2 = ((ContextWrapper) context2).getBaseContext();
                } else {
                    context2 = null;
                }
            }
            int id = this.f862a.getId();
            throw new IllegalStateException("Could not find method " + this.f863b + "(View) in a parent or ancestor Context for android:onClick " + "attribute defined on view " + this.f862a.getClass() + (id == -1 ? BuildConfig.FLAVOR : " with id '" + this.f862a.getContext().getResources().getResourceEntryName(id) + "'"));
        }
    }

    AppCompatViewInflater() {
        this.f870e = new Object[2];
    }

    static {
        f866a = new Class[]{Context.class, AttributeSet.class};
        f867b = new int[]{16843375};
        f868c = new String[]{"android.widget.", "android.view.", "android.webkit."};
        f869d = new ArrayMap();
    }

    public final View m2040a(View view, String str, Context context, AttributeSet attributeSet, boolean z, boolean z2, boolean z3, boolean z4) {
        Context context2;
        View view2;
        if (!z || view == null) {
            context2 = context;
        } else {
            context2 = view.getContext();
        }
        if (z2 || z3) {
            context2 = AppCompatViewInflater.m2036a(context2, attributeSet, z2, z3);
        }
        if (z4) {
            context2 = ao.m2807a(context2);
        }
        View view3 = null;
        Object obj = -1;
        switch (str.hashCode()) {
            case -1946472170:
                if (str.equals("RatingBar")) {
                    obj = 11;
                    break;
                }
                break;
            case -1455429095:
                if (str.equals("CheckedTextView")) {
                    obj = 8;
                    break;
                }
                break;
            case -1346021293:
                if (str.equals("MultiAutoCompleteTextView")) {
                    obj = 10;
                    break;
                }
                break;
            case -938935918:
                if (str.equals("TextView")) {
                    obj = null;
                    break;
                }
                break;
            case -937446323:
                if (str.equals("ImageButton")) {
                    obj = 5;
                    break;
                }
                break;
            case -658531749:
                if (str.equals("SeekBar")) {
                    obj = 12;
                    break;
                }
                break;
            case -339785223:
                if (str.equals("Spinner")) {
                    obj = 4;
                    break;
                }
                break;
            case 776382189:
                if (str.equals("RadioButton")) {
                    obj = 7;
                    break;
                }
                break;
            case 1125864064:
                if (str.equals("ImageView")) {
                    obj = 1;
                    break;
                }
                break;
            case 1413872058:
                if (str.equals("AutoCompleteTextView")) {
                    obj = 9;
                    break;
                }
                break;
            case 1601505219:
                if (str.equals("CheckBox")) {
                    obj = 6;
                    break;
                }
                break;
            case 1666676343:
                if (str.equals("EditText")) {
                    obj = 3;
                    break;
                }
                break;
            case 2001146706:
                if (str.equals("Button")) {
                    obj = 2;
                    break;
                }
                break;
        }
        switch (obj) {
            case R.View_android_theme /*0*/:
                view3 = new aa(context2, attributeSet);
                break;
            case R.View_android_focusable /*1*/:
                view3 = new AppCompatImageView(context2, attributeSet);
                break;
            case R.View_paddingStart /*2*/:
                view3 = new AppCompatButton(context2, attributeSet);
                break;
            case R.View_paddingEnd /*3*/:
                view3 = new AppCompatEditText(context2, attributeSet);
                break;
            case R.View_theme /*4*/:
                view3 = new AppCompatSpinner(context2, attributeSet);
                break;
            case R.Toolbar_contentInsetStart /*5*/:
                view3 = new AppCompatImageButton(context2, attributeSet);
                break;
            case R.Toolbar_contentInsetEnd /*6*/:
                view3 = new AppCompatCheckBox(context2, attributeSet);
                break;
            case R.Toolbar_contentInsetLeft /*7*/:
                view3 = new AppCompatRadioButton(context2, attributeSet);
                break;
            case R.Toolbar_contentInsetRight /*8*/:
                view3 = new AppCompatCheckedTextView(context2, attributeSet);
                break;
            case R.Toolbar_popupTheme /*9*/:
                view3 = new AppCompatAutoCompleteTextView(context2, attributeSet);
                break;
            case R.Toolbar_titleTextAppearance /*10*/:
                view3 = new AppCompatMultiAutoCompleteTextView(context2, attributeSet);
                break;
            case R.Toolbar_subtitleTextAppearance /*11*/:
                view3 = new AppCompatRatingBar(context2, attributeSet);
                break;
            case R.Toolbar_titleMargins /*12*/:
                view3 = new AppCompatSeekBar(context2, attributeSet);
                break;
        }
        if (view3 != null || context == context2) {
            view2 = view3;
        } else {
            view2 = m2037a(context2, str, attributeSet);
        }
        if (view2 != null) {
            m2039a(view2, attributeSet);
        }
        return view2;
    }

    private View m2037a(Context context, String str, AttributeSet attributeSet) {
        if (str.equals("view")) {
            str = attributeSet.getAttributeValue(null, "class");
        }
        try {
            this.f870e[0] = context;
            this.f870e[1] = attributeSet;
            View a;
            if (-1 == str.indexOf(46)) {
                for (String a2 : f868c) {
                    a = m2038a(context, str, a2);
                    if (a != null) {
                        return a;
                    }
                }
                this.f870e[0] = null;
                this.f870e[1] = null;
                return null;
            }
            a = m2038a(context, str, null);
            this.f870e[0] = null;
            this.f870e[1] = null;
            return a;
        } catch (Exception e) {
            return null;
        } finally {
            this.f870e[0] = null;
            this.f870e[1] = null;
        }
    }

    private void m2039a(View view, AttributeSet attributeSet) {
        Context context = view.getContext();
        if (!(context instanceof ContextWrapper)) {
            return;
        }
        if (VERSION.SDK_INT < 15 || af.m1210v(view)) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f867b);
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                view.setOnClickListener(new AppCompatViewInflater(view, string));
            }
            obtainStyledAttributes.recycle();
        }
    }

    private View m2038a(Context context, String str, String str2) {
        Constructor constructor = (Constructor) f869d.get(str);
        if (constructor == null) {
            try {
                constructor = context.getClassLoader().loadClass(str2 != null ? str2 + str : str).asSubclass(View.class).getConstructor(f866a);
                f869d.put(str, constructor);
            } catch (Exception e) {
                return null;
            }
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.f870e);
    }

    private static Context m2036a(Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        int resourceId;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, android.support.v7.p014b.R.R.View, 0, 0);
        if (z) {
            resourceId = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.View_android_theme, 0);
        } else {
            resourceId = 0;
        }
        if (z2 && r0 == 0) {
            resourceId = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.View_theme, 0);
            if (resourceId != 0) {
                Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
            }
        }
        int i = resourceId;
        obtainStyledAttributes.recycle();
        if (i == 0) {
            return context;
        }
        if ((context instanceof ContextThemeWrapper) && ((ContextThemeWrapper) context).m2175a() == i) {
            return context;
        }
        return new ContextThemeWrapper(context, i);
    }
}
