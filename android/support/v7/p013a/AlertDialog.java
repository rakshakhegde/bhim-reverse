package android.support.v7.p013a;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.p013a.AlertController.AlertController;
import android.support.v7.p014b.R.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

/* renamed from: android.support.v7.a.d */
public class AlertDialog extends AppCompatDialog implements DialogInterface {
    private AlertController f771a;

    /* renamed from: android.support.v7.a.d.a */
    public static class AlertDialog {
        private final AlertController f768a;
        private int f769b;

        public AlertDialog(Context context) {
            this(context, AlertDialog.m1873a(context, 0));
        }

        public AlertDialog(Context context, int i) {
            this.f768a = new AlertController(new ContextThemeWrapper(context, AlertDialog.m1873a(context, i)));
            this.f769b = i;
        }

        public Context m1860a() {
            return this.f768a.f701a;
        }

        public AlertDialog m1865a(CharSequence charSequence) {
            this.f768a.f706f = charSequence;
            return this;
        }

        public AlertDialog m1863a(View view) {
            this.f768a.f707g = view;
            return this;
        }

        public AlertDialog m1862a(Drawable drawable) {
            this.f768a.f704d = drawable;
            return this;
        }

        public AlertDialog m1861a(OnKeyListener onKeyListener) {
            this.f768a.f718r = onKeyListener;
            return this;
        }

        public AlertDialog m1864a(ListAdapter listAdapter, OnClickListener onClickListener) {
            this.f768a.f720t = listAdapter;
            this.f768a.f721u = onClickListener;
            return this;
        }

        public AlertDialog m1866b() {
            AlertDialog alertDialog = new AlertDialog(this.f768a.f701a, this.f769b, false);
            this.f768a.m1818a(alertDialog.f771a);
            alertDialog.setCancelable(this.f768a.f715o);
            if (this.f768a.f715o) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.f768a.f716p);
            alertDialog.setOnDismissListener(this.f768a.f717q);
            if (this.f768a.f718r != null) {
                alertDialog.setOnKeyListener(this.f768a.f718r);
            }
            return alertDialog;
        }
    }

    AlertDialog(Context context, int i, boolean z) {
        super(context, AlertDialog.m1873a(context, i));
        this.f771a = new AlertController(getContext(), this, getWindow());
    }

    static int m1873a(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f771a.m1852a(charSequence);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f771a.m1847a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f771a.m1853a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f771a.m1857b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }
}
