package android.support.v7.p013a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.p006f.LayoutInflaterCompat;
import android.support.v4.p006f.LayoutInflaterFactory;
import android.support.v4.p006f.aa;
import android.support.v4.p006f.af;
import android.support.v4.p006f.aq;
import android.support.v4.p006f.au;
import android.support.v4.p006f.az;
import android.support.v4.p006f.bb;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.ContentFrameLayout.C0004a;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ab;
import android.support.v7.widget.ae;
import android.support.v7.widget.ae.FitWindowsViewGroup;
import android.support.v7.widget.at;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v7.a.l */
class AppCompatDelegateImplV7 extends AppCompatDelegateImplBase implements LayoutInflaterFactory, MenuBuilder {
    private boolean f790A;
    private AppCompatDelegateImplV7[] f791B;
    private AppCompatDelegateImplV7 f792C;
    private boolean f793D;
    private boolean f794E;
    private int f795F;
    private final Runnable f796G;
    private boolean f797H;
    private Rect f798I;
    private Rect f799J;
    private AppCompatViewInflater f800K;
    ActionMode f801m;
    ActionBarContextView f802n;
    PopupWindow f803o;
    Runnable f804p;
    au f805q;
    private ab f806r;
    private AppCompatDelegateImplV7 f807s;
    private AppCompatDelegateImplV7 f808t;
    private boolean f809u;
    private ViewGroup f810v;
    private TextView f811w;
    private View f812x;
    private boolean f813y;
    private boolean f814z;

    /* renamed from: android.support.v7.a.l.1 */
    class AppCompatDelegateImplV7 implements Runnable {
        final /* synthetic */ AppCompatDelegateImplV7 f822a;

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f822a = appCompatDelegateImplV7;
        }

        public void run() {
            if ((this.f822a.f795F & 1) != 0) {
                this.f822a.m1950f(0);
            }
            if ((this.f822a.f795F & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0) {
                this.f822a.m1950f(R.AppCompatTheme_ratingBarStyleSmall);
            }
            this.f822a.f794E = false;
            this.f822a.f795F = 0;
        }
    }

    /* renamed from: android.support.v7.a.l.2 */
    class AppCompatDelegateImplV7 implements aa {
        final /* synthetic */ AppCompatDelegateImplV7 f823a;

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f823a = appCompatDelegateImplV7;
        }

        public bb m2002a(View view, bb bbVar) {
            int b = bbVar.m1392b();
            int c = this.f823a.m1951g(b);
            if (b != c) {
                bbVar = bbVar.m1391a(bbVar.m1390a(), c, bbVar.m1393c(), bbVar.m1394d());
            }
            return af.m1174a(view, bbVar);
        }
    }

    /* renamed from: android.support.v7.a.l.3 */
    class AppCompatDelegateImplV7 implements FitWindowsViewGroup {
        final /* synthetic */ AppCompatDelegateImplV7 f824a;

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f824a = appCompatDelegateImplV7;
        }

        public void m2004a(Rect rect) {
            rect.top = this.f824a.m1951g(rect.top);
        }
    }

    /* renamed from: android.support.v7.a.l.4 */
    class AppCompatDelegateImplV7 implements C0004a {
        final /* synthetic */ AppCompatDelegateImplV7 f825a;

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f825a = appCompatDelegateImplV7;
        }

        public void m2007a() {
        }

        public void m2008b() {
            this.f825a.m1958x();
        }
    }

    /* renamed from: android.support.v7.a.l.5 */
    class AppCompatDelegateImplV7 implements Runnable {
        final /* synthetic */ AppCompatDelegateImplV7 f827a;

        /* renamed from: android.support.v7.a.l.5.1 */
        class AppCompatDelegateImplV7 extends az {
            final /* synthetic */ AppCompatDelegateImplV7 f826a;

            AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
                this.f826a = appCompatDelegateImplV7;
            }

            public void m2010b(View view) {
                af.m1187b(this.f826a.f827a.f802n, 1.0f);
                this.f826a.f827a.f805q.m1355a(null);
                this.f826a.f827a.f805q = null;
            }

            public void m2009a(View view) {
                this.f826a.f827a.f802n.setVisibility(0);
            }
        }

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f827a = appCompatDelegateImplV7;
        }

        public void run() {
            this.f827a.f803o.showAtLocation(this.f827a.f802n, 55, 0, 0);
            this.f827a.m1956v();
            af.m1187b(this.f827a.f802n, 0.0f);
            this.f827a.f805q = af.m1199k(this.f827a.f802n).m1353a(1.0f);
            this.f827a.f805q.m1355a(new AppCompatDelegateImplV7(this));
        }
    }

    /* renamed from: android.support.v7.a.l.6 */
    class AppCompatDelegateImplV7 extends az {
        final /* synthetic */ AppCompatDelegateImplV7 f828a;

        AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f828a = appCompatDelegateImplV7;
        }

        public void m2012b(View view) {
            af.m1187b(this.f828a.f802n, 1.0f);
            this.f828a.f805q.m1355a(null);
            this.f828a.f805q = null;
        }

        public void m2011a(View view) {
            this.f828a.f802n.setVisibility(0);
            this.f828a.f802n.sendAccessibilityEvent(32);
            if (this.f828a.f802n.getParent() != null) {
                af.m1202n((View) this.f828a.f802n.getParent());
            }
        }
    }

    /* renamed from: android.support.v7.a.l.a */
    private final class AppCompatDelegateImplV7 implements MenuPresenter {
        final /* synthetic */ AppCompatDelegateImplV7 f829a;

        private AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f829a = appCompatDelegateImplV7;
        }

        public boolean m2016a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            Callback p = this.f829a.m1917p();
            if (p != null) {
                p.onMenuOpened(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
            }
            return true;
        }

        public void m2015a(android.support.v7.view.menu.MenuBuilder menuBuilder, boolean z) {
            this.f829a.m1939b(menuBuilder);
        }
    }

    /* renamed from: android.support.v7.a.l.b */
    class AppCompatDelegateImplV7 implements ActionMode.ActionMode {
        final /* synthetic */ AppCompatDelegateImplV7 f831a;
        private ActionMode.ActionMode f832b;

        /* renamed from: android.support.v7.a.l.b.1 */
        class AppCompatDelegateImplV7 extends az {
            final /* synthetic */ AppCompatDelegateImplV7 f830a;

            AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
                this.f830a = appCompatDelegateImplV7;
            }

            public void m2017b(View view) {
                this.f830a.f831a.f802n.setVisibility(8);
                if (this.f830a.f831a.f803o != null) {
                    this.f830a.f831a.f803o.dismiss();
                } else if (this.f830a.f831a.f802n.getParent() instanceof View) {
                    af.m1202n((View) this.f830a.f831a.f802n.getParent());
                }
                this.f830a.f831a.f802n.removeAllViews();
                this.f830a.f831a.f805q.m1355a(null);
                this.f830a.f831a.f805q = null;
            }
        }

        public AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7, ActionMode.ActionMode actionMode) {
            this.f831a = appCompatDelegateImplV7;
            this.f832b = actionMode;
        }

        public boolean m2023a(ActionMode actionMode, Menu menu) {
            return this.f832b.m2019a(actionMode, menu);
        }

        public boolean m2025b(ActionMode actionMode, Menu menu) {
            return this.f832b.m2021b(actionMode, menu);
        }

        public boolean m2024a(ActionMode actionMode, MenuItem menuItem) {
            return this.f832b.m2020a(actionMode, menuItem);
        }

        public void m2022a(ActionMode actionMode) {
            this.f832b.m2018a(actionMode);
            if (this.f831a.f803o != null) {
                this.f831a.b.getDecorView().removeCallbacks(this.f831a.f804p);
            }
            if (this.f831a.f802n != null) {
                this.f831a.m1956v();
                this.f831a.f805q = af.m1199k(this.f831a.f802n).m1353a(0.0f);
                this.f831a.f805q.m1355a(new AppCompatDelegateImplV7(this));
            }
            if (this.f831a.e != null) {
                this.f831a.e.m1801b(this.f831a.f801m);
            }
            this.f831a.f801m = null;
        }
    }

    /* renamed from: android.support.v7.a.l.c */
    private class AppCompatDelegateImplV7 extends ContentFrameLayout {
        final /* synthetic */ AppCompatDelegateImplV7 f841a;

        public AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7, Context context) {
            this.f841a = appCompatDelegateImplV7;
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return this.f841a.m1971a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !m2028a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            this.f841a.m1945d(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(AppCompatDrawableManager.m2981a().m3004a(getContext(), i));
        }

        private boolean m2028a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }
    }

    /* renamed from: android.support.v7.a.l.d */
    private static final class AppCompatDelegateImplV7 {
        int f842a;
        int f843b;
        int f844c;
        int f845d;
        int f846e;
        int f847f;
        ViewGroup f848g;
        View f849h;
        View f850i;
        android.support.v7.view.menu.MenuBuilder f851j;
        ListMenuPresenter f852k;
        Context f853l;
        boolean f854m;
        boolean f855n;
        boolean f856o;
        public boolean f857p;
        boolean f858q;
        boolean f859r;
        Bundle f860s;

        AppCompatDelegateImplV7(int i) {
            this.f842a = i;
            this.f858q = false;
        }

        public boolean m2032a() {
            if (this.f849h == null) {
                return false;
            }
            if (this.f850i != null || this.f852k.m2309a().getCount() > 0) {
                return true;
            }
            return false;
        }

        void m2030a(Context context) {
            TypedValue typedValue = new TypedValue();
            Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(android.support.v7.p014b.R.R.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(android.support.v7.p014b.R.R.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(android.support.v7.p014b.R.R.Theme_AppCompat_CompactMenu, true);
            }
            Context contextThemeWrapper = new ContextThemeWrapper(context, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            this.f853l = contextThemeWrapper;
            TypedArray obtainStyledAttributes = contextThemeWrapper.obtainStyledAttributes(android.support.v7.p014b.R.R.AppCompatTheme);
            this.f843b = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AppCompatTheme_panelBackground, 0);
            this.f847f = obtainStyledAttributes.getResourceId(android.support.v7.p014b.R.R.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        void m2031a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            if (menuBuilder != this.f851j) {
                if (this.f851j != null) {
                    this.f851j.m2348b(this.f852k);
                }
                this.f851j = menuBuilder;
                if (menuBuilder != null && this.f852k != null) {
                    menuBuilder.m2337a(this.f852k);
                }
            }
        }

        MenuView m2029a(MenuPresenter menuPresenter) {
            if (this.f851j == null) {
                return null;
            }
            if (this.f852k == null) {
                this.f852k = new ListMenuPresenter(this.f853l, android.support.v7.p014b.R.R.abc_list_menu_item_layout);
                this.f852k.m2312a(menuPresenter);
                this.f851j.m2337a(this.f852k);
            }
            return this.f852k.m2308a(this.f848g);
        }
    }

    /* renamed from: android.support.v7.a.l.e */
    private final class AppCompatDelegateImplV7 implements MenuPresenter {
        final /* synthetic */ AppCompatDelegateImplV7 f861a;

        private AppCompatDelegateImplV7(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
            this.f861a = appCompatDelegateImplV7;
        }

        public void m2033a(android.support.v7.view.menu.MenuBuilder menuBuilder, boolean z) {
            Menu menu;
            Menu p = menuBuilder.m2368p();
            boolean z2 = p != menuBuilder;
            AppCompatDelegateImplV7 appCompatDelegateImplV7 = this.f861a;
            if (z2) {
                menu = p;
            }
            AppCompatDelegateImplV7 a = appCompatDelegateImplV7.m1924a(menu);
            if (a == null) {
                return;
            }
            if (z2) {
                this.f861a.m1925a(a.f842a, a, p);
                this.f861a.m1927a(a, true);
                return;
            }
            this.f861a.m1927a(a, z);
        }

        public boolean m2034a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
            if (menuBuilder == null && this.f861a.h) {
                Callback p = this.f861a.m1917p();
                if (!(p == null || this.f861a.m1916o())) {
                    p.onMenuOpened(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
                }
            }
            return true;
        }
    }

    AppCompatDelegateImplV7(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
        this.f805q = null;
        this.f796G = new AppCompatDelegateImplV7(this);
    }

    public void m1964a(Bundle bundle) {
        if ((this.c instanceof Activity) && NavUtils.m597b((Activity) this.c) != null) {
            ActionBar l = m1913l();
            if (l == null) {
                this.f797H = true;
            } else {
                l.m1791c(true);
            }
        }
    }

    public void m1975b(Bundle bundle) {
        m1953s();
    }

    public void m1988k() {
        m1953s();
        if (this.h && this.f == null) {
            if (this.c instanceof Activity) {
                this.f = new WindowDecorActionBar((Activity) this.c, this.i);
            } else if (this.c instanceof Dialog) {
                this.f = new WindowDecorActionBar((Dialog) this.c);
            }
            if (this.f != null) {
                this.f.m1791c(this.f797H);
            }
        }
    }

    public View m1960a(int i) {
        m1953s();
        return this.b.findViewById(i);
    }

    public void m1963a(Configuration configuration) {
        if (this.h && this.f809u) {
            ActionBar a = m1899a();
            if (a != null) {
                a.m1784a(configuration);
            }
        }
        m1911h();
    }

    public void m1981c() {
        ActionBar a = m1899a();
        if (a != null) {
            a.m1793d(false);
        }
    }

    public void m1984d() {
        ActionBar a = m1899a();
        if (a != null) {
            a.m1793d(true);
        }
    }

    public void m1966a(View view) {
        m1953s();
        ViewGroup viewGroup = (ViewGroup) this.f810v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.c.onContentChanged();
    }

    public void m1974b(int i) {
        m1953s();
        ViewGroup viewGroup = (ViewGroup) this.f810v.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.a).inflate(i, viewGroup);
        this.c.onContentChanged();
    }

    public void m1967a(View view, LayoutParams layoutParams) {
        m1953s();
        ViewGroup viewGroup = (ViewGroup) this.f810v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.c.onContentChanged();
    }

    public void m1976b(View view, LayoutParams layoutParams) {
        m1953s();
        ((ViewGroup) this.f810v.findViewById(16908290)).addView(view, layoutParams);
        this.c.onContentChanged();
    }

    public void m1986f() {
        super.m1910f();
        if (this.f != null) {
            this.f.m1798h();
        }
    }

    private void m1953s() {
        if (!this.f809u) {
            this.f810v = m1954t();
            CharSequence q = m1918q();
            if (!TextUtils.isEmpty(q)) {
                m1977b(q);
            }
            m1955u();
            m1968a(this.f810v);
            this.f809u = true;
            AppCompatDelegateImplV7 a = m1922a(0, false);
            if (!m1916o()) {
                if (a == null || a.f851j == null) {
                    m1948e(R.AppCompatTheme_ratingBarStyleSmall);
                }
            }
        }
    }

    private ViewGroup m1954t() {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(android.support.v7.p014b.R.R.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(android.support.v7.p014b.R.R.AppCompatTheme_windowActionBar)) {
            View view;
            if (obtainStyledAttributes.getBoolean(android.support.v7.p014b.R.R.AppCompatTheme_windowNoTitle, false)) {
                m1982c(1);
            } else if (obtainStyledAttributes.getBoolean(android.support.v7.p014b.R.R.AppCompatTheme_windowActionBar, false)) {
                m1982c((int) R.AppCompatTheme_ratingBarStyleSmall);
            }
            if (obtainStyledAttributes.getBoolean(android.support.v7.p014b.R.R.AppCompatTheme_windowActionBarOverlay, false)) {
                m1982c((int) R.AppCompatTheme_seekBarStyle);
            }
            if (obtainStyledAttributes.getBoolean(android.support.v7.p014b.R.R.AppCompatTheme_windowActionModeOverlay, false)) {
                m1982c(10);
            }
            this.k = obtainStyledAttributes.getBoolean(android.support.v7.p014b.R.R.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            this.b.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.a);
            if (this.l) {
                View view2;
                if (this.j) {
                    view2 = (ViewGroup) from.inflate(android.support.v7.p014b.R.R.abc_screen_simple_overlay_action_mode, null);
                } else {
                    view2 = (ViewGroup) from.inflate(android.support.v7.p014b.R.R.abc_screen_simple, null);
                }
                if (VERSION.SDK_INT >= 21) {
                    af.m1181a(view2, new AppCompatDelegateImplV7(this));
                    view = view2;
                } else {
                    ((ae) view2).setOnFitSystemWindowsListener(new AppCompatDelegateImplV7(this));
                    view = view2;
                }
            } else if (this.k) {
                r0 = (ViewGroup) from.inflate(android.support.v7.p014b.R.R.abc_dialog_title_material, null);
                this.i = false;
                this.h = false;
                view = r0;
            } else if (this.h) {
                Context contextThemeWrapper;
                TypedValue typedValue = new TypedValue();
                this.a.getTheme().resolveAttribute(android.support.v7.p014b.R.R.actionBarTheme, typedValue, true);
                if (typedValue.resourceId != 0) {
                    contextThemeWrapper = new ContextThemeWrapper(this.a, typedValue.resourceId);
                } else {
                    contextThemeWrapper = this.a;
                }
                r0 = (ViewGroup) LayoutInflater.from(contextThemeWrapper).inflate(android.support.v7.p014b.R.R.abc_screen_toolbar, null);
                this.f806r = (ab) r0.findViewById(android.support.v7.p014b.R.R.decor_content_parent);
                this.f806r.setWindowCallback(m1917p());
                if (this.i) {
                    this.f806r.m2474a(R.AppCompatTheme_seekBarStyle);
                }
                if (this.f813y) {
                    this.f806r.m2474a(2);
                }
                if (this.f814z) {
                    this.f806r.m2474a(5);
                }
                view = r0;
            } else {
                view = null;
            }
            if (view == null) {
                throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.h + ", windowActionBarOverlay: " + this.i + ", android:windowIsFloating: " + this.k + ", windowActionModeOverlay: " + this.j + ", windowNoTitle: " + this.l + " }");
            }
            if (this.f806r == null) {
                this.f811w = (TextView) view.findViewById(android.support.v7.p014b.R.R.title);
            }
            android.support.v7.widget.au.m2884b(view);
            ContentFrameLayout contentFrameLayout = (ContentFrameLayout) view.findViewById(android.support.v7.p014b.R.R.action_bar_activity_content);
            ViewGroup viewGroup = (ViewGroup) this.b.findViewById(16908290);
            if (viewGroup != null) {
                while (viewGroup.getChildCount() > 0) {
                    View childAt = viewGroup.getChildAt(0);
                    viewGroup.removeViewAt(0);
                    contentFrameLayout.addView(childAt);
                }
                viewGroup.setId(-1);
                contentFrameLayout.setId(16908290);
                if (viewGroup instanceof FrameLayout) {
                    ((FrameLayout) viewGroup).setForeground(null);
                }
            }
            this.b.setContentView(view);
            contentFrameLayout.setAttachListener(new AppCompatDelegateImplV7(this));
            return view;
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    void m1968a(ViewGroup viewGroup) {
    }

    private void m1955u() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.f810v.findViewById(16908290);
        View decorView = this.b.getDecorView();
        contentFrameLayout.m2026a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(android.support.v7.p014b.R.R.AppCompatTheme);
        obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(android.support.v7.p014b.R.R.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    public boolean m1982c(int i) {
        int h = m1952h(i);
        if (this.l && h == R.AppCompatTheme_ratingBarStyleSmall) {
            return false;
        }
        if (this.h && h == 1) {
            this.h = false;
        }
        switch (h) {
            case R.View_android_focusable /*1*/:
                m1957w();
                this.l = true;
                return true;
            case R.View_paddingStart /*2*/:
                m1957w();
                this.f813y = true;
                return true;
            case R.Toolbar_contentInsetStart /*5*/:
                m1957w();
                this.f814z = true;
                return true;
            case R.Toolbar_titleTextAppearance /*10*/:
                m1957w();
                this.j = true;
                return true;
            case R.AppCompatTheme_ratingBarStyleSmall /*108*/:
                m1957w();
                this.h = true;
                return true;
            case R.AppCompatTheme_seekBarStyle /*109*/:
                m1957w();
                this.i = true;
                return true;
            default:
                return this.b.requestFeature(h);
        }
    }

    void m1977b(CharSequence charSequence) {
        if (this.f806r != null) {
            this.f806r.setWindowTitle(charSequence);
        } else if (m1913l() != null) {
            m1913l().m1785a(charSequence);
        } else if (this.f811w != null) {
            this.f811w.setText(charSequence);
        }
    }

    void m1962a(int i, Menu menu) {
        if (i == R.AppCompatTheme_ratingBarStyleSmall) {
            ActionBar a = m1899a();
            if (a != null) {
                a.m1794e(false);
            }
        } else if (i == 0) {
            AppCompatDelegateImplV7 a2 = m1922a(i, true);
            if (a2.f856o) {
                m1927a(a2, false);
            }
        }
    }

    boolean m1979b(int i, Menu menu) {
        if (i != R.AppCompatTheme_ratingBarStyleSmall) {
            return false;
        }
        ActionBar a = m1899a();
        if (a == null) {
            return true;
        }
        a.m1794e(true);
        return true;
    }

    public boolean m1970a(android.support.v7.view.menu.MenuBuilder menuBuilder, MenuItem menuItem) {
        Callback p = m1917p();
        if (!(p == null || m1916o())) {
            AppCompatDelegateImplV7 a = m1924a(menuBuilder.m2368p());
            if (a != null) {
                return p.onMenuItemSelected(a.f842a, menuItem);
            }
        }
        return false;
    }

    public void m1965a(android.support.v7.view.menu.MenuBuilder menuBuilder) {
        m1932a(menuBuilder, true);
    }

    public ActionMode m1972b(ActionMode.ActionMode actionMode) {
        if (actionMode == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.f801m != null) {
            this.f801m.m2083c();
        }
        ActionMode.ActionMode appCompatDelegateImplV7 = new AppCompatDelegateImplV7(this, actionMode);
        ActionBar a = m1899a();
        if (a != null) {
            this.f801m = a.m1782a(appCompatDelegateImplV7);
            if (!(this.f801m == null || this.e == null)) {
                this.e.m1800a(this.f801m);
            }
        }
        if (this.f801m == null) {
            this.f801m = m1959a(appCompatDelegateImplV7);
        }
        return this.f801m;
    }

    public void m1985e() {
        ActionBar a = m1899a();
        if (a == null || !a.m1795e()) {
            m1948e(0);
        }
    }

    ActionMode m1959a(ActionMode.ActionMode actionMode) {
        ActionMode actionMode2;
        m1956v();
        if (this.f801m != null) {
            this.f801m.m2083c();
        }
        ActionMode.ActionMode appCompatDelegateImplV7 = new AppCompatDelegateImplV7(this, actionMode);
        if (this.e == null || m1916o()) {
            actionMode2 = null;
        } else {
            try {
                actionMode2 = this.e.m1799a(appCompatDelegateImplV7);
            } catch (AbstractMethodError e) {
                actionMode2 = null;
            }
        }
        if (actionMode2 != null) {
            this.f801m = actionMode2;
        } else {
            if (this.f802n == null) {
                if (this.k) {
                    Context contextThemeWrapper;
                    TypedValue typedValue = new TypedValue();
                    Theme theme = this.a.getTheme();
                    theme.resolveAttribute(android.support.v7.p014b.R.R.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Theme newTheme = this.a.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        contextThemeWrapper = new ContextThemeWrapper(this.a, 0);
                        contextThemeWrapper.getTheme().setTo(newTheme);
                    } else {
                        contextThemeWrapper = this.a;
                    }
                    this.f802n = new ActionBarContextView(contextThemeWrapper);
                    this.f803o = new PopupWindow(contextThemeWrapper, null, android.support.v7.p014b.R.R.actionModePopupWindowStyle);
                    PopupWindowCompat.m1703a(this.f803o, 2);
                    this.f803o.setContentView(this.f802n);
                    this.f803o.setWidth(-1);
                    contextThemeWrapper.getTheme().resolveAttribute(android.support.v7.p014b.R.R.actionBarSize, typedValue, true);
                    this.f802n.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, contextThemeWrapper.getResources().getDisplayMetrics()));
                    this.f803o.setHeight(-2);
                    this.f804p = new AppCompatDelegateImplV7(this);
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.f810v.findViewById(android.support.v7.p014b.R.R.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(m1914m()));
                        this.f802n = (ActionBarContextView) viewStubCompat.m2671a();
                    }
                }
            }
            if (this.f802n != null) {
                boolean z;
                m1956v();
                this.f802n.m2470c();
                Context context = this.f802n.getContext();
                ActionBarContextView actionBarContextView = this.f802n;
                if (this.f803o == null) {
                    z = true;
                } else {
                    z = false;
                }
                ActionMode standaloneActionMode = new StandaloneActionMode(context, actionBarContextView, appCompatDelegateImplV7, z);
                if (actionMode.m2019a(standaloneActionMode, standaloneActionMode.m2080b())) {
                    standaloneActionMode.m2084d();
                    this.f802n.m2467a(standaloneActionMode);
                    this.f801m = standaloneActionMode;
                    af.m1187b(this.f802n, 0.0f);
                    this.f805q = af.m1199k(this.f802n).m1353a(1.0f);
                    this.f805q.m1355a(new AppCompatDelegateImplV7(this));
                    if (this.f803o != null) {
                        this.b.getDecorView().post(this.f804p);
                    }
                } else {
                    this.f801m = null;
                }
            }
        }
        if (!(this.f801m == null || this.e == null)) {
            this.e.m1800a(this.f801m);
        }
        return this.f801m;
    }

    private void m1956v() {
        if (this.f805q != null) {
            this.f805q.m1360b();
        }
    }

    boolean m1989r() {
        if (this.f801m != null) {
            this.f801m.m2083c();
            return true;
        }
        ActionBar a = m1899a();
        if (a == null || !a.m1796f()) {
            return false;
        }
        return true;
    }

    boolean m1969a(int i, KeyEvent keyEvent) {
        ActionBar a = m1899a();
        if (a != null && a.m1787a(i, keyEvent)) {
            return true;
        }
        if (this.f792C == null || !m1934a(this.f792C, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.f792C == null) {
                AppCompatDelegateImplV7 a2 = m1922a(0, true);
                m1941b(a2, keyEvent);
                boolean a3 = m1934a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.f854m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        } else if (this.f792C == null) {
            return true;
        } else {
            this.f792C.f855n = true;
            return true;
        }
    }

    boolean m1971a(KeyEvent keyEvent) {
        boolean z = true;
        if (keyEvent.getKeyCode() == 82 && this.c.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z = false;
        }
        return z ? m1983c(keyCode, keyEvent) : m1978b(keyCode, keyEvent);
    }

    boolean m1978b(int i, KeyEvent keyEvent) {
        switch (i) {
            case R.View_theme /*4*/:
                boolean z = this.f793D;
                this.f793D = false;
                AppCompatDelegateImplV7 a = m1922a(0, false);
                if (a == null || !a.f856o) {
                    if (m1989r()) {
                        return true;
                    }
                } else if (z) {
                    return true;
                } else {
                    m1927a(a, true);
                    return true;
                }
                break;
            case R.AppCompatTheme_colorPrimary /*82*/:
                m1949e(0, keyEvent);
                return true;
        }
        return false;
    }

    boolean m1983c(int i, KeyEvent keyEvent) {
        boolean z = true;
        switch (i) {
            case R.View_theme /*4*/:
                if ((keyEvent.getFlags() & 128) == 0) {
                    z = false;
                }
                this.f793D = z;
                break;
            case R.AppCompatTheme_colorPrimary /*82*/:
                m1947d(0, keyEvent);
                return true;
        }
        if (VERSION.SDK_INT < 11) {
            m1969a(i, keyEvent);
        }
        return false;
    }

    public View m1980c(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z;
        boolean z2 = VERSION.SDK_INT < 21;
        if (this.f800K == null) {
            this.f800K = new AppCompatViewInflater();
        }
        if (z2 && m1936a((ViewParent) view)) {
            z = true;
        } else {
            z = false;
        }
        return this.f800K.m2040a(view, str, context, attributeSet, z, z2, true, at.m2879a());
    }

    private boolean m1936a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        ViewParent decorView = this.b.getDecorView();
        ViewParent viewParent2 = viewParent;
        while (viewParent2 != null) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || af.m1209u((View) viewParent2)) {
                return false;
            }
            viewParent2 = viewParent2.getParent();
        }
        return true;
    }

    public void m1987g() {
        LayoutInflater from = LayoutInflater.from(this.a);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.m1438a(from, this);
        } else if (!(LayoutInflaterCompat.m1437a(from) instanceof AppCompatDelegateImplV7)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View m1961a(View view, String str, Context context, AttributeSet attributeSet) {
        View b = m1973b(view, str, context, attributeSet);
        return b != null ? b : m1980c(view, str, context, attributeSet);
    }

    View m1973b(View view, String str, Context context, AttributeSet attributeSet) {
        if (this.c instanceof Factory) {
            View onCreateView = ((Factory) this.c).onCreateView(str, context, attributeSet);
            if (onCreateView != null) {
                return onCreateView;
            }
        }
        return null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1926a(android.support.v7.p013a.AppCompatDelegateImplV7.AppCompatDelegateImplV7 r11, android.view.KeyEvent r12) {
        /*
        r10 = this;
        r1 = -1;
        r3 = 0;
        r9 = 1;
        r2 = -2;
        r0 = r11.f856o;
        if (r0 != 0) goto L_0x000e;
    L_0x0008:
        r0 = r10.m1916o();
        if (r0 == 0) goto L_0x000f;
    L_0x000e:
        return;
    L_0x000f:
        r0 = r11.f842a;
        if (r0 != 0) goto L_0x0034;
    L_0x0013:
        r4 = r10.a;
        r0 = r4.getResources();
        r0 = r0.getConfiguration();
        r0 = r0.screenLayout;
        r0 = r0 & 15;
        r5 = 4;
        if (r0 != r5) goto L_0x0048;
    L_0x0024:
        r0 = r9;
    L_0x0025:
        r4 = r4.getApplicationInfo();
        r4 = r4.targetSdkVersion;
        r5 = 11;
        if (r4 < r5) goto L_0x004a;
    L_0x002f:
        r4 = r9;
    L_0x0030:
        if (r0 == 0) goto L_0x0034;
    L_0x0032:
        if (r4 != 0) goto L_0x000e;
    L_0x0034:
        r0 = r10.m1917p();
        if (r0 == 0) goto L_0x004c;
    L_0x003a:
        r4 = r11.f842a;
        r5 = r11.f851j;
        r0 = r0.onMenuOpened(r4, r5);
        if (r0 != 0) goto L_0x004c;
    L_0x0044:
        r10.m1927a(r11, r9);
        goto L_0x000e;
    L_0x0048:
        r0 = r3;
        goto L_0x0025;
    L_0x004a:
        r4 = r3;
        goto L_0x0030;
    L_0x004c:
        r0 = r10.a;
        r4 = "window";
        r0 = r0.getSystemService(r4);
        r8 = r0;
        r8 = (android.view.WindowManager) r8;
        if (r8 == 0) goto L_0x000e;
    L_0x0059:
        r0 = r10.m1941b(r11, r12);
        if (r0 == 0) goto L_0x000e;
    L_0x005f:
        r0 = r11.f848g;
        if (r0 == 0) goto L_0x0067;
    L_0x0063:
        r0 = r11.f858q;
        if (r0 == 0) goto L_0x00f1;
    L_0x0067:
        r0 = r11.f848g;
        if (r0 != 0) goto L_0x00df;
    L_0x006b:
        r0 = r10.m1933a(r11);
        if (r0 == 0) goto L_0x000e;
    L_0x0071:
        r0 = r11.f848g;
        if (r0 == 0) goto L_0x000e;
    L_0x0075:
        r0 = r10.m1944c(r11);
        if (r0 == 0) goto L_0x000e;
    L_0x007b:
        r0 = r11.m2032a();
        if (r0 == 0) goto L_0x000e;
    L_0x0081:
        r0 = r11.f849h;
        r0 = r0.getLayoutParams();
        if (r0 != 0) goto L_0x0103;
    L_0x0089:
        r0 = new android.view.ViewGroup$LayoutParams;
        r0.<init>(r2, r2);
        r1 = r0;
    L_0x008f:
        r0 = r11.f843b;
        r4 = r11.f848g;
        r4.setBackgroundResource(r0);
        r0 = r11.f849h;
        r0 = r0.getParent();
        if (r0 == 0) goto L_0x00a9;
    L_0x009e:
        r4 = r0 instanceof android.view.ViewGroup;
        if (r4 == 0) goto L_0x00a9;
    L_0x00a2:
        r0 = (android.view.ViewGroup) r0;
        r4 = r11.f849h;
        r0.removeView(r4);
    L_0x00a9:
        r0 = r11.f848g;
        r4 = r11.f849h;
        r0.addView(r4, r1);
        r0 = r11.f849h;
        r0 = r0.hasFocus();
        if (r0 != 0) goto L_0x00bd;
    L_0x00b8:
        r0 = r11.f849h;
        r0.requestFocus();
    L_0x00bd:
        r1 = r2;
    L_0x00be:
        r11.f855n = r3;
        r0 = new android.view.WindowManager$LayoutParams;
        r3 = r11.f845d;
        r4 = r11.f846e;
        r5 = 1002; // 0x3ea float:1.404E-42 double:4.95E-321;
        r6 = 8519680; // 0x820000 float:1.1938615E-38 double:4.209281E-317;
        r7 = -3;
        r0.<init>(r1, r2, r3, r4, r5, r6, r7);
        r1 = r11.f844c;
        r0.gravity = r1;
        r1 = r11.f847f;
        r0.windowAnimations = r1;
        r1 = r11.f848g;
        r8.addView(r1, r0);
        r11.f856o = r9;
        goto L_0x000e;
    L_0x00df:
        r0 = r11.f858q;
        if (r0 == 0) goto L_0x0075;
    L_0x00e3:
        r0 = r11.f848g;
        r0 = r0.getChildCount();
        if (r0 <= 0) goto L_0x0075;
    L_0x00eb:
        r0 = r11.f848g;
        r0.removeAllViews();
        goto L_0x0075;
    L_0x00f1:
        r0 = r11.f850i;
        if (r0 == 0) goto L_0x0101;
    L_0x00f5:
        r0 = r11.f850i;
        r0 = r0.getLayoutParams();
        if (r0 == 0) goto L_0x0101;
    L_0x00fd:
        r0 = r0.width;
        if (r0 == r1) goto L_0x00be;
    L_0x0101:
        r1 = r2;
        goto L_0x00be;
    L_0x0103:
        r1 = r0;
        goto L_0x008f;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.a.l.a(android.support.v7.a.l$d, android.view.KeyEvent):void");
    }

    private boolean m1933a(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        appCompatDelegateImplV7.m2030a(m1914m());
        appCompatDelegateImplV7.f848g = new AppCompatDelegateImplV7(this, appCompatDelegateImplV7.f853l);
        appCompatDelegateImplV7.f844c = 81;
        return true;
    }

    private void m1932a(android.support.v7.view.menu.MenuBuilder menuBuilder, boolean z) {
        if (this.f806r == null || !this.f806r.m2476d() || (aq.m1262a(ViewConfiguration.get(this.a)) && !this.f806r.m2478f())) {
            AppCompatDelegateImplV7 a = m1922a(0, true);
            a.f858q = true;
            m1927a(a, false);
            m1926a(a, null);
            return;
        }
        Callback p = m1917p();
        if (this.f806r.m2477e() && z) {
            this.f806r.m2480h();
            if (!m1916o()) {
                p.onPanelClosed(R.AppCompatTheme_ratingBarStyleSmall, m1922a(0, true).f851j);
            }
        } else if (p != null && !m1916o()) {
            if (this.f794E && (this.f795F & 1) != 0) {
                this.b.getDecorView().removeCallbacks(this.f796G);
                this.f796G.run();
            }
            AppCompatDelegateImplV7 a2 = m1922a(0, true);
            if (a2.f851j != null && !a2.f859r && p.onPreparePanel(0, a2.f850i, a2.f851j)) {
                p.onMenuOpened(R.AppCompatTheme_ratingBarStyleSmall, a2.f851j);
                this.f806r.m2479g();
            }
        }
    }

    private boolean m1940b(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        Context contextThemeWrapper;
        android.support.v7.view.menu.MenuBuilder menuBuilder;
        Context context = this.a;
        if ((appCompatDelegateImplV7.f842a == 0 || appCompatDelegateImplV7.f842a == R.AppCompatTheme_ratingBarStyleSmall) && this.f806r != null) {
            TypedValue typedValue = new TypedValue();
            Theme theme = context.getTheme();
            theme.resolveAttribute(android.support.v7.p014b.R.R.actionBarTheme, typedValue, true);
            Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(android.support.v7.p014b.R.R.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(android.support.v7.p014b.R.R.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            Theme theme3 = theme2;
            if (theme3 != null) {
                contextThemeWrapper = new ContextThemeWrapper(context, 0);
                contextThemeWrapper.getTheme().setTo(theme3);
                menuBuilder = new android.support.v7.view.menu.MenuBuilder(contextThemeWrapper);
                menuBuilder.m2335a((MenuBuilder) this);
                appCompatDelegateImplV7.m2031a(menuBuilder);
                return true;
            }
        }
        contextThemeWrapper = context;
        menuBuilder = new android.support.v7.view.menu.MenuBuilder(contextThemeWrapper);
        menuBuilder.m2335a((MenuBuilder) this);
        appCompatDelegateImplV7.m2031a(menuBuilder);
        return true;
    }

    private boolean m1944c(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        if (appCompatDelegateImplV7.f850i != null) {
            appCompatDelegateImplV7.f849h = appCompatDelegateImplV7.f850i;
            return true;
        } else if (appCompatDelegateImplV7.f851j == null) {
            return false;
        } else {
            if (this.f808t == null) {
                this.f808t = new AppCompatDelegateImplV7();
            }
            appCompatDelegateImplV7.f849h = (View) appCompatDelegateImplV7.m2029a(this.f808t);
            return appCompatDelegateImplV7.f849h != null;
        }
    }

    private boolean m1941b(AppCompatDelegateImplV7 appCompatDelegateImplV7, KeyEvent keyEvent) {
        if (m1916o()) {
            return false;
        }
        if (appCompatDelegateImplV7.f854m) {
            return true;
        }
        if (!(this.f792C == null || this.f792C == appCompatDelegateImplV7)) {
            m1927a(this.f792C, false);
        }
        Callback p = m1917p();
        if (p != null) {
            appCompatDelegateImplV7.f850i = p.onCreatePanelView(appCompatDelegateImplV7.f842a);
        }
        boolean z = appCompatDelegateImplV7.f842a == 0 || appCompatDelegateImplV7.f842a == R.AppCompatTheme_ratingBarStyleSmall;
        if (z && this.f806r != null) {
            this.f806r.m2481i();
        }
        if (appCompatDelegateImplV7.f850i == null && !(z && (m1913l() instanceof ToolbarActionBar))) {
            if (appCompatDelegateImplV7.f851j == null || appCompatDelegateImplV7.f859r) {
                if (appCompatDelegateImplV7.f851j == null && (!m1940b(appCompatDelegateImplV7) || appCompatDelegateImplV7.f851j == null)) {
                    return false;
                }
                if (z && this.f806r != null) {
                    if (this.f807s == null) {
                        this.f807s = new AppCompatDelegateImplV7();
                    }
                    this.f806r.m2475a(appCompatDelegateImplV7.f851j, this.f807s);
                }
                appCompatDelegateImplV7.f851j.m2359g();
                if (p.onCreatePanelMenu(appCompatDelegateImplV7.f842a, appCompatDelegateImplV7.f851j)) {
                    appCompatDelegateImplV7.f859r = false;
                } else {
                    appCompatDelegateImplV7.m2031a(null);
                    if (!z || this.f806r == null) {
                        return false;
                    }
                    this.f806r.m2475a(null, this.f807s);
                    return false;
                }
            }
            appCompatDelegateImplV7.f851j.m2359g();
            if (appCompatDelegateImplV7.f860s != null) {
                appCompatDelegateImplV7.f851j.m2346b(appCompatDelegateImplV7.f860s);
                appCompatDelegateImplV7.f860s = null;
            }
            if (p.onPreparePanel(0, appCompatDelegateImplV7.f850i, appCompatDelegateImplV7.f851j)) {
                if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                    z = true;
                } else {
                    z = false;
                }
                appCompatDelegateImplV7.f857p = z;
                appCompatDelegateImplV7.f851j.setQwertyMode(appCompatDelegateImplV7.f857p);
                appCompatDelegateImplV7.f851j.m2360h();
            } else {
                if (z && this.f806r != null) {
                    this.f806r.m2475a(null, this.f807s);
                }
                appCompatDelegateImplV7.f851j.m2360h();
                return false;
            }
        }
        appCompatDelegateImplV7.f854m = true;
        appCompatDelegateImplV7.f855n = false;
        this.f792C = appCompatDelegateImplV7;
        return true;
    }

    private void m1939b(android.support.v7.view.menu.MenuBuilder menuBuilder) {
        if (!this.f790A) {
            this.f790A = true;
            this.f806r.m2482j();
            Callback p = m1917p();
            if (!(p == null || m1916o())) {
                p.onPanelClosed(R.AppCompatTheme_ratingBarStyleSmall, menuBuilder);
            }
            this.f790A = false;
        }
    }

    private void m1945d(int i) {
        m1927a(m1922a(i, true), true);
    }

    private void m1927a(AppCompatDelegateImplV7 appCompatDelegateImplV7, boolean z) {
        if (z && appCompatDelegateImplV7.f842a == 0 && this.f806r != null && this.f806r.m2477e()) {
            m1939b(appCompatDelegateImplV7.f851j);
            return;
        }
        WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
        if (!(windowManager == null || !appCompatDelegateImplV7.f856o || appCompatDelegateImplV7.f848g == null)) {
            windowManager.removeView(appCompatDelegateImplV7.f848g);
            if (z) {
                m1925a(appCompatDelegateImplV7.f842a, appCompatDelegateImplV7, null);
            }
        }
        appCompatDelegateImplV7.f854m = false;
        appCompatDelegateImplV7.f855n = false;
        appCompatDelegateImplV7.f856o = false;
        appCompatDelegateImplV7.f849h = null;
        appCompatDelegateImplV7.f858q = true;
        if (this.f792C == appCompatDelegateImplV7) {
            this.f792C = null;
        }
    }

    private boolean m1947d(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            AppCompatDelegateImplV7 a = m1922a(i, true);
            if (!a.f856o) {
                return m1941b(a, keyEvent);
            }
        }
        return false;
    }

    private boolean m1949e(int i, KeyEvent keyEvent) {
        boolean z = true;
        if (this.f801m != null) {
            return false;
        }
        AppCompatDelegateImplV7 a = m1922a(i, true);
        if (i != 0 || this.f806r == null || !this.f806r.m2476d() || aq.m1262a(ViewConfiguration.get(this.a))) {
            boolean z2;
            if (a.f856o || a.f855n) {
                z2 = a.f856o;
                m1927a(a, true);
                z = z2;
            } else {
                if (a.f854m) {
                    if (a.f859r) {
                        a.f854m = false;
                        z2 = m1941b(a, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        m1926a(a, keyEvent);
                    }
                }
                z = false;
            }
        } else if (this.f806r.m2477e()) {
            z = this.f806r.m2480h();
        } else {
            if (!m1916o() && m1941b(a, keyEvent)) {
                z = this.f806r.m2479g();
            }
            z = false;
        }
        if (z) {
            AudioManager audioManager = (AudioManager) this.a.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z;
    }

    private void m1925a(int i, AppCompatDelegateImplV7 appCompatDelegateImplV7, Menu menu) {
        if (menu == null) {
            if (appCompatDelegateImplV7 == null && i >= 0 && i < this.f791B.length) {
                appCompatDelegateImplV7 = this.f791B[i];
            }
            if (appCompatDelegateImplV7 != null) {
                menu = appCompatDelegateImplV7.f851j;
            }
        }
        if ((appCompatDelegateImplV7 == null || appCompatDelegateImplV7.f856o) && !m1916o()) {
            this.c.onPanelClosed(i, menu);
        }
    }

    private AppCompatDelegateImplV7 m1924a(Menu menu) {
        AppCompatDelegateImplV7[] appCompatDelegateImplV7Arr = this.f791B;
        int length = appCompatDelegateImplV7Arr != null ? appCompatDelegateImplV7Arr.length : 0;
        for (int i = 0; i < length; i++) {
            AppCompatDelegateImplV7 appCompatDelegateImplV7 = appCompatDelegateImplV7Arr[i];
            if (appCompatDelegateImplV7 != null && appCompatDelegateImplV7.f851j == menu) {
                return appCompatDelegateImplV7;
            }
        }
        return null;
    }

    private AppCompatDelegateImplV7 m1922a(int i, boolean z) {
        Object obj = this.f791B;
        if (obj == null || obj.length <= i) {
            Object obj2 = new AppCompatDelegateImplV7[(i + 1)];
            if (obj != null) {
                System.arraycopy(obj, 0, obj2, 0, obj.length);
            }
            this.f791B = obj2;
            obj = obj2;
        }
        AppCompatDelegateImplV7 appCompatDelegateImplV7 = obj[i];
        if (appCompatDelegateImplV7 != null) {
            return appCompatDelegateImplV7;
        }
        appCompatDelegateImplV7 = new AppCompatDelegateImplV7(i);
        obj[i] = appCompatDelegateImplV7;
        return appCompatDelegateImplV7;
    }

    private boolean m1934a(AppCompatDelegateImplV7 appCompatDelegateImplV7, int i, KeyEvent keyEvent, int i2) {
        boolean z = false;
        if (!keyEvent.isSystem()) {
            if ((appCompatDelegateImplV7.f854m || m1941b(appCompatDelegateImplV7, keyEvent)) && appCompatDelegateImplV7.f851j != null) {
                z = appCompatDelegateImplV7.f851j.performShortcut(i, keyEvent, i2);
            }
            if (z && (i2 & 1) == 0 && this.f806r == null) {
                m1927a(appCompatDelegateImplV7, true);
            }
        }
        return z;
    }

    private void m1948e(int i) {
        this.f795F |= 1 << i;
        if (!this.f794E) {
            af.m1182a(this.b.getDecorView(), this.f796G);
            this.f794E = true;
        }
    }

    private void m1950f(int i) {
        AppCompatDelegateImplV7 a = m1922a(i, true);
        if (a.f851j != null) {
            Bundle bundle = new Bundle();
            a.f851j.m2334a(bundle);
            if (bundle.size() > 0) {
                a.f860s = bundle;
            }
            a.f851j.m2359g();
            a.f851j.clear();
        }
        a.f859r = true;
        a.f858q = true;
        if ((i == R.AppCompatTheme_ratingBarStyleSmall || i == 0) && this.f806r != null) {
            a = m1922a(0, false);
            if (a != null) {
                a.f854m = false;
                m1941b(a, null);
            }
        }
    }

    private int m1951g(int i) {
        int i2;
        int i3 = 1;
        int i4 = 0;
        if (this.f802n == null || !(this.f802n.getLayoutParams() instanceof MarginLayoutParams)) {
            i2 = 0;
        } else {
            int i5;
            MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.f802n.getLayoutParams();
            if (this.f802n.isShown()) {
                if (this.f798I == null) {
                    this.f798I = new Rect();
                    this.f799J = new Rect();
                }
                Rect rect = this.f798I;
                Rect rect2 = this.f799J;
                rect.set(0, i, 0, 0);
                android.support.v7.widget.au.m2882a(this.f810v, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    if (this.f812x == null) {
                        this.f812x = new View(this.a);
                        this.f812x.setBackgroundColor(this.a.getResources().getColor(android.support.v7.p014b.R.R.abc_input_method_navigation_guard));
                        this.f810v.addView(this.f812x, -1, new LayoutParams(-1, i));
                        i5 = 1;
                    } else {
                        LayoutParams layoutParams = this.f812x.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.f812x.setLayoutParams(layoutParams);
                        }
                        i5 = 1;
                    }
                } else {
                    i5 = 0;
                }
                if (this.f812x == null) {
                    i3 = 0;
                }
                if (!(this.j || i3 == 0)) {
                    i = 0;
                }
                int i6 = i5;
                i5 = i3;
                i3 = i6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                i5 = 0;
            } else {
                i3 = 0;
                i5 = 0;
            }
            if (i3 != 0) {
                this.f802n.setLayoutParams(marginLayoutParams);
            }
            i2 = i5;
        }
        if (this.f812x != null) {
            View view = this.f812x;
            if (i2 == 0) {
                i4 = 8;
            }
            view.setVisibility(i4);
        }
        return i;
    }

    private void m1957w() {
        if (this.f809u) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private int m1952h(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return R.AppCompatTheme_ratingBarStyleSmall;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return R.AppCompatTheme_seekBarStyle;
        }
    }

    private void m1958x() {
        if (this.f806r != null) {
            this.f806r.m2482j();
        }
        if (this.f803o != null) {
            this.b.getDecorView().removeCallbacks(this.f804p);
            if (this.f803o.isShowing()) {
                try {
                    this.f803o.dismiss();
                } catch (IllegalArgumentException e) {
                }
            }
            this.f803o = null;
        }
        m1956v();
        AppCompatDelegateImplV7 a = m1922a(0, false);
        if (a != null && a.f851j != null) {
            a.f851j.close();
        }
    }
}
