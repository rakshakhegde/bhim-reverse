package android.support.p000a.p001a;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.VectorDrawable;
import android.os.Build.VERSION;
import android.support.p000a.p001a.PathParser.PathParser;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v4.p004a.p005a.ResourcesCompat;
import android.support.v4.p010e.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import java.util.ArrayList;
import java.util.Stack;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@TargetApi(21)
/* renamed from: android.support.a.a.f */
public class VectorDrawableCompat extends VectorDrawableCommon {
    static final Mode f79b;
    private VectorDrawableCompat f80c;
    private PorterDuffColorFilter f81d;
    private ColorFilter f82e;
    private boolean f83f;
    private boolean f84g;
    private ConstantState f85h;
    private final float[] f86i;
    private final Matrix f87j;
    private final Rect f88k;

    /* renamed from: android.support.a.a.f.d */
    private static class VectorDrawableCompat {
        protected PathParser[] f21m;
        String f22n;
        int f23o;

        public VectorDrawableCompat() {
            this.f21m = null;
        }

        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
            this.f21m = null;
            this.f22n = vectorDrawableCompat.f22n;
            this.f23o = vectorDrawableCompat.f23o;
            this.f21m = PathParser.m16a(vectorDrawableCompat.f21m);
        }

        public void m24a(Path path) {
            path.reset();
            if (this.f21m != null) {
                PathParser.m10a(this.f21m, path);
            }
        }

        public String m26b() {
            return this.f22n;
        }

        public boolean m25a() {
            return false;
        }
    }

    /* renamed from: android.support.a.a.f.a */
    private static class VectorDrawableCompat extends VectorDrawableCompat {
        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
            super(vectorDrawableCompat);
        }

        public void m28a(Resources resources, AttributeSet attributeSet, Theme theme, XmlPullParser xmlPullParser) {
            if (TypedArrayUtils.m22a(xmlPullParser, "pathData")) {
                TypedArray b = VectorDrawableCommon.m0b(resources, theme, attributeSet, AndroidResources.f3d);
                m27a(b);
                b.recycle();
            }
        }

        private void m27a(TypedArray typedArray) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.n = string;
            }
            string = typedArray.getString(1);
            if (string != null) {
                this.m = PathParser.m15a(string);
            }
        }

        public boolean m29a() {
            return true;
        }
    }

    /* renamed from: android.support.a.a.f.b */
    private static class VectorDrawableCompat extends VectorDrawableCompat {
        int f24a;
        float f25b;
        int f26c;
        float f27d;
        int f28e;
        float f29f;
        float f30g;
        float f31h;
        float f32i;
        Cap f33j;
        Join f34k;
        float f35l;
        private int[] f36p;

        public VectorDrawableCompat() {
            this.f24a = 0;
            this.f25b = 0.0f;
            this.f26c = 0;
            this.f27d = 1.0f;
            this.f29f = 1.0f;
            this.f30g = 0.0f;
            this.f31h = 1.0f;
            this.f32i = 0.0f;
            this.f33j = Cap.BUTT;
            this.f34k = Join.MITER;
            this.f35l = 4.0f;
        }

        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
            super(vectorDrawableCompat);
            this.f24a = 0;
            this.f25b = 0.0f;
            this.f26c = 0;
            this.f27d = 1.0f;
            this.f29f = 1.0f;
            this.f30g = 0.0f;
            this.f31h = 1.0f;
            this.f32i = 0.0f;
            this.f33j = Cap.BUTT;
            this.f34k = Join.MITER;
            this.f35l = 4.0f;
            this.f36p = vectorDrawableCompat.f36p;
            this.f24a = vectorDrawableCompat.f24a;
            this.f25b = vectorDrawableCompat.f25b;
            this.f27d = vectorDrawableCompat.f27d;
            this.f26c = vectorDrawableCompat.f26c;
            this.f28e = vectorDrawableCompat.f28e;
            this.f29f = vectorDrawableCompat.f29f;
            this.f30g = vectorDrawableCompat.f30g;
            this.f31h = vectorDrawableCompat.f31h;
            this.f32i = vectorDrawableCompat.f32i;
            this.f33j = vectorDrawableCompat.f33j;
            this.f34k = vectorDrawableCompat.f34k;
            this.f35l = vectorDrawableCompat.f35l;
        }

        private Cap m30a(int i, Cap cap) {
            switch (i) {
                case R.View_android_theme /*0*/:
                    return Cap.BUTT;
                case R.View_android_focusable /*1*/:
                    return Cap.ROUND;
                case R.View_paddingStart /*2*/:
                    return Cap.SQUARE;
                default:
                    return cap;
            }
        }

        private Join m31a(int i, Join join) {
            switch (i) {
                case R.View_android_theme /*0*/:
                    return Join.MITER;
                case R.View_android_focusable /*1*/:
                    return Join.ROUND;
                case R.View_paddingStart /*2*/:
                    return Join.BEVEL;
                default:
                    return join;
            }
        }

        public void m33a(Resources resources, AttributeSet attributeSet, Theme theme, XmlPullParser xmlPullParser) {
            TypedArray b = VectorDrawableCommon.m0b(resources, theme, attributeSet, AndroidResources.f2c);
            m32a(b, xmlPullParser);
            b.recycle();
        }

        private void m32a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f36p = null;
            if (TypedArrayUtils.m22a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.n = string;
                }
                string = typedArray.getString(2);
                if (string != null) {
                    this.m = PathParser.m15a(string);
                }
                this.f26c = TypedArrayUtils.m23b(typedArray, xmlPullParser, "fillColor", 1, this.f26c);
                this.f29f = TypedArrayUtils.m19a(typedArray, xmlPullParser, "fillAlpha", 12, this.f29f);
                this.f33j = m30a(TypedArrayUtils.m20a(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.f33j);
                this.f34k = m31a(TypedArrayUtils.m20a(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.f34k);
                this.f35l = TypedArrayUtils.m19a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.f35l);
                this.f24a = TypedArrayUtils.m23b(typedArray, xmlPullParser, "strokeColor", 3, this.f24a);
                this.f27d = TypedArrayUtils.m19a(typedArray, xmlPullParser, "strokeAlpha", 11, this.f27d);
                this.f25b = TypedArrayUtils.m19a(typedArray, xmlPullParser, "strokeWidth", 4, this.f25b);
                this.f31h = TypedArrayUtils.m19a(typedArray, xmlPullParser, "trimPathEnd", 6, this.f31h);
                this.f32i = TypedArrayUtils.m19a(typedArray, xmlPullParser, "trimPathOffset", 7, this.f32i);
                this.f30g = TypedArrayUtils.m19a(typedArray, xmlPullParser, "trimPathStart", 5, this.f30g);
            }
        }
    }

    /* renamed from: android.support.a.a.f.c */
    private static class VectorDrawableCompat {
        final ArrayList<Object> f37a;
        private final Matrix f38b;
        private float f39c;
        private float f40d;
        private float f41e;
        private float f42f;
        private float f43g;
        private float f44h;
        private float f45i;
        private final Matrix f46j;
        private int f47k;
        private int[] f48l;
        private String f49m;

        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat, ArrayMap<String, Object> arrayMap) {
            this.f38b = new Matrix();
            this.f37a = new ArrayList();
            this.f39c = 0.0f;
            this.f40d = 0.0f;
            this.f41e = 0.0f;
            this.f42f = 1.0f;
            this.f43g = 1.0f;
            this.f44h = 0.0f;
            this.f45i = 0.0f;
            this.f46j = new Matrix();
            this.f49m = null;
            this.f39c = vectorDrawableCompat.f39c;
            this.f40d = vectorDrawableCompat.f40d;
            this.f41e = vectorDrawableCompat.f41e;
            this.f42f = vectorDrawableCompat.f42f;
            this.f43g = vectorDrawableCompat.f43g;
            this.f44h = vectorDrawableCompat.f44h;
            this.f45i = vectorDrawableCompat.f45i;
            this.f48l = vectorDrawableCompat.f48l;
            this.f49m = vectorDrawableCompat.f49m;
            this.f47k = vectorDrawableCompat.f47k;
            if (this.f49m != null) {
                arrayMap.put(this.f49m, this);
            }
            this.f46j.set(vectorDrawableCompat.f46j);
            ArrayList arrayList = vectorDrawableCompat.f37a;
            for (int i = 0; i < arrayList.size(); i++) {
                Object obj = arrayList.get(i);
                if (obj instanceof VectorDrawableCompat) {
                    this.f37a.add(new VectorDrawableCompat((VectorDrawableCompat) obj, arrayMap));
                } else {
                    VectorDrawableCompat vectorDrawableCompat2;
                    if (obj instanceof VectorDrawableCompat) {
                        vectorDrawableCompat2 = new VectorDrawableCompat((VectorDrawableCompat) obj);
                    } else if (obj instanceof VectorDrawableCompat) {
                        vectorDrawableCompat2 = new VectorDrawableCompat((VectorDrawableCompat) obj);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.f37a.add(vectorDrawableCompat2);
                    if (vectorDrawableCompat2.f22n != null) {
                        arrayMap.put(vectorDrawableCompat2.f22n, vectorDrawableCompat2);
                    }
                }
            }
        }

        public VectorDrawableCompat() {
            this.f38b = new Matrix();
            this.f37a = new ArrayList();
            this.f39c = 0.0f;
            this.f40d = 0.0f;
            this.f41e = 0.0f;
            this.f42f = 1.0f;
            this.f43g = 1.0f;
            this.f44h = 0.0f;
            this.f45i = 0.0f;
            this.f46j = new Matrix();
            this.f49m = null;
        }

        public String m39a() {
            return this.f49m;
        }

        public void m40a(Resources resources, AttributeSet attributeSet, Theme theme, XmlPullParser xmlPullParser) {
            TypedArray b = VectorDrawableCommon.m0b(resources, theme, attributeSet, AndroidResources.f1b);
            m35a(b, xmlPullParser);
            b.recycle();
        }

        private void m35a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f48l = null;
            this.f39c = TypedArrayUtils.m19a(typedArray, xmlPullParser, "rotation", 5, this.f39c);
            this.f40d = typedArray.getFloat(1, this.f40d);
            this.f41e = typedArray.getFloat(2, this.f41e);
            this.f42f = TypedArrayUtils.m19a(typedArray, xmlPullParser, "scaleX", 3, this.f42f);
            this.f43g = TypedArrayUtils.m19a(typedArray, xmlPullParser, "scaleY", 4, this.f43g);
            this.f44h = TypedArrayUtils.m19a(typedArray, xmlPullParser, "translateX", 6, this.f44h);
            this.f45i = TypedArrayUtils.m19a(typedArray, xmlPullParser, "translateY", 7, this.f45i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.f49m = string;
            }
            m37b();
        }

        private void m37b() {
            this.f46j.reset();
            this.f46j.postTranslate(-this.f40d, -this.f41e);
            this.f46j.postScale(this.f42f, this.f43g);
            this.f46j.postRotate(this.f39c, 0.0f, 0.0f);
            this.f46j.postTranslate(this.f44h + this.f40d, this.f45i + this.f41e);
        }
    }

    /* renamed from: android.support.a.a.f.e */
    private static class VectorDrawableCompat {
        private static final Matrix f50j;
        float f51a;
        float f52b;
        float f53c;
        float f54d;
        int f55e;
        String f56f;
        final ArrayMap<String, Object> f57g;
        private final Path f58h;
        private final Path f59i;
        private final Matrix f60k;
        private Paint f61l;
        private Paint f62m;
        private PathMeasure f63n;
        private int f64o;
        private final VectorDrawableCompat f65p;

        static {
            f50j = new Matrix();
        }

        public VectorDrawableCompat() {
            this.f60k = new Matrix();
            this.f51a = 0.0f;
            this.f52b = 0.0f;
            this.f53c = 0.0f;
            this.f54d = 0.0f;
            this.f55e = 255;
            this.f56f = null;
            this.f57g = new ArrayMap();
            this.f65p = new VectorDrawableCompat();
            this.f58h = new Path();
            this.f59i = new Path();
        }

        public void m52a(int i) {
            this.f55e = i;
        }

        public int m50a() {
            return this.f55e;
        }

        public void m51a(float f) {
            m52a((int) (255.0f * f));
        }

        public float m54b() {
            return ((float) m50a()) / 255.0f;
        }

        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
            this.f60k = new Matrix();
            this.f51a = 0.0f;
            this.f52b = 0.0f;
            this.f53c = 0.0f;
            this.f54d = 0.0f;
            this.f55e = 255;
            this.f56f = null;
            this.f57g = new ArrayMap();
            this.f65p = new VectorDrawableCompat(vectorDrawableCompat.f65p, this.f57g);
            this.f58h = new Path(vectorDrawableCompat.f58h);
            this.f59i = new Path(vectorDrawableCompat.f59i);
            this.f51a = vectorDrawableCompat.f51a;
            this.f52b = vectorDrawableCompat.f52b;
            this.f53c = vectorDrawableCompat.f53c;
            this.f54d = vectorDrawableCompat.f54d;
            this.f64o = vectorDrawableCompat.f64o;
            this.f55e = vectorDrawableCompat.f55e;
            this.f56f = vectorDrawableCompat.f56f;
            if (vectorDrawableCompat.f56f != null) {
                this.f57g.put(vectorDrawableCompat.f56f, this);
            }
        }

        private void m45a(VectorDrawableCompat vectorDrawableCompat, Matrix matrix, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            vectorDrawableCompat.f38b.set(matrix);
            vectorDrawableCompat.f38b.preConcat(vectorDrawableCompat.f46j);
            for (int i3 = 0; i3 < vectorDrawableCompat.f37a.size(); i3++) {
                Object obj = vectorDrawableCompat.f37a.get(i3);
                if (obj instanceof VectorDrawableCompat) {
                    m45a((VectorDrawableCompat) obj, vectorDrawableCompat.f38b, canvas, i, i2, colorFilter);
                } else if (obj instanceof VectorDrawableCompat) {
                    m46a(vectorDrawableCompat, (VectorDrawableCompat) obj, canvas, i, i2, colorFilter);
                }
            }
        }

        public void m53a(Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            m45a(this.f65p, f50j, canvas, i, i2, colorFilter);
        }

        private void m46a(VectorDrawableCompat vectorDrawableCompat, VectorDrawableCompat vectorDrawableCompat2, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            float f = ((float) i) / this.f53c;
            float f2 = ((float) i2) / this.f54d;
            float min = Math.min(f, f2);
            Matrix b = vectorDrawableCompat.f38b;
            this.f60k.set(b);
            this.f60k.postScale(f, f2);
            f = m42a(b);
            if (f != 0.0f) {
                vectorDrawableCompat2.m24a(this.f58h);
                Path path = this.f58h;
                this.f59i.reset();
                if (vectorDrawableCompat2.m25a()) {
                    this.f59i.addPath(path, this.f60k);
                    canvas.clipPath(this.f59i, Op.REPLACE);
                    return;
                }
                Paint paint;
                VectorDrawableCompat vectorDrawableCompat3 = (VectorDrawableCompat) vectorDrawableCompat2;
                if (!(vectorDrawableCompat3.f30g == 0.0f && vectorDrawableCompat3.f31h == 1.0f)) {
                    float f3 = (vectorDrawableCompat3.f30g + vectorDrawableCompat3.f32i) % 1.0f;
                    float f4 = (vectorDrawableCompat3.f31h + vectorDrawableCompat3.f32i) % 1.0f;
                    if (this.f63n == null) {
                        this.f63n = new PathMeasure();
                    }
                    this.f63n.setPath(this.f58h, false);
                    float length = this.f63n.getLength();
                    f3 *= length;
                    f4 *= length;
                    path.reset();
                    if (f3 > f4) {
                        this.f63n.getSegment(f3, length, path, true);
                        this.f63n.getSegment(0.0f, f4, path, true);
                    } else {
                        this.f63n.getSegment(f3, f4, path, true);
                    }
                    path.rLineTo(0.0f, 0.0f);
                }
                this.f59i.addPath(path, this.f60k);
                if (vectorDrawableCompat3.f26c != 0) {
                    if (this.f62m == null) {
                        this.f62m = new Paint();
                        this.f62m.setStyle(Style.FILL);
                        this.f62m.setAntiAlias(true);
                    }
                    paint = this.f62m;
                    paint.setColor(VectorDrawableCompat.m69b(vectorDrawableCompat3.f26c, vectorDrawableCompat3.f29f));
                    paint.setColorFilter(colorFilter);
                    canvas.drawPath(this.f59i, paint);
                }
                if (vectorDrawableCompat3.f24a != 0) {
                    if (this.f61l == null) {
                        this.f61l = new Paint();
                        this.f61l.setStyle(Style.STROKE);
                        this.f61l.setAntiAlias(true);
                    }
                    paint = this.f61l;
                    if (vectorDrawableCompat3.f34k != null) {
                        paint.setStrokeJoin(vectorDrawableCompat3.f34k);
                    }
                    if (vectorDrawableCompat3.f33j != null) {
                        paint.setStrokeCap(vectorDrawableCompat3.f33j);
                    }
                    paint.setStrokeMiter(vectorDrawableCompat3.f35l);
                    paint.setColor(VectorDrawableCompat.m69b(vectorDrawableCompat3.f24a, vectorDrawableCompat3.f27d));
                    paint.setColorFilter(colorFilter);
                    paint.setStrokeWidth((f * min) * vectorDrawableCompat3.f25b);
                    canvas.drawPath(this.f59i, paint);
                }
            }
        }

        private static float m41a(float f, float f2, float f3, float f4) {
            return (f * f4) - (f2 * f3);
        }

        private float m42a(Matrix matrix) {
            float[] fArr = new float[]{0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float hypot = (float) Math.hypot((double) fArr[0], (double) fArr[1]);
            float hypot2 = (float) Math.hypot((double) fArr[2], (double) fArr[3]);
            float a = VectorDrawableCompat.m41a(fArr[0], fArr[1], fArr[2], fArr[3]);
            hypot = Math.max(hypot, hypot2);
            if (hypot > 0.0f) {
                return Math.abs(a) / hypot;
            }
            return 0.0f;
        }
    }

    /* renamed from: android.support.a.a.f.f */
    private static class VectorDrawableCompat extends ConstantState {
        int f66a;
        VectorDrawableCompat f67b;
        ColorStateList f68c;
        Mode f69d;
        boolean f70e;
        Bitmap f71f;
        ColorStateList f72g;
        Mode f73h;
        int f74i;
        boolean f75j;
        boolean f76k;
        Paint f77l;

        public VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
            this.f68c = null;
            this.f69d = VectorDrawableCompat.f79b;
            if (vectorDrawableCompat != null) {
                this.f66a = vectorDrawableCompat.f66a;
                this.f67b = new VectorDrawableCompat(vectorDrawableCompat.f67b);
                if (vectorDrawableCompat.f67b.f62m != null) {
                    this.f67b.f62m = new Paint(vectorDrawableCompat.f67b.f62m);
                }
                if (vectorDrawableCompat.f67b.f61l != null) {
                    this.f67b.f61l = new Paint(vectorDrawableCompat.f67b.f61l);
                }
                this.f68c = vectorDrawableCompat.f68c;
                this.f69d = vectorDrawableCompat.f69d;
                this.f70e = vectorDrawableCompat.f70e;
            }
        }

        public void m57a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f71f, null, rect, m55a(colorFilter));
        }

        public boolean m58a() {
            return this.f67b.m50a() < 255;
        }

        public Paint m55a(ColorFilter colorFilter) {
            if (!m58a() && colorFilter == null) {
                return null;
            }
            if (this.f77l == null) {
                this.f77l = new Paint();
                this.f77l.setFilterBitmap(true);
            }
            this.f77l.setAlpha(this.f67b.m50a());
            this.f77l.setColorFilter(colorFilter);
            return this.f77l;
        }

        public void m56a(int i, int i2) {
            this.f71f.eraseColor(0);
            this.f67b.m53a(new Canvas(this.f71f), i, i2, null);
        }

        public void m59b(int i, int i2) {
            if (this.f71f == null || !m62c(i, i2)) {
                this.f71f = Bitmap.createBitmap(i, i2, Config.ARGB_8888);
                this.f76k = true;
            }
        }

        public boolean m62c(int i, int i2) {
            if (i == this.f71f.getWidth() && i2 == this.f71f.getHeight()) {
                return true;
            }
            return false;
        }

        public boolean m60b() {
            if (!this.f76k && this.f72g == this.f68c && this.f73h == this.f69d && this.f75j == this.f70e && this.f74i == this.f67b.m50a()) {
                return true;
            }
            return false;
        }

        public void m61c() {
            this.f72g = this.f68c;
            this.f73h = this.f69d;
            this.f74i = this.f67b.m50a();
            this.f75j = this.f70e;
            this.f76k = false;
        }

        public VectorDrawableCompat() {
            this.f68c = null;
            this.f69d = VectorDrawableCompat.f79b;
            this.f67b = new VectorDrawableCompat();
        }

        public Drawable newDrawable() {
            return new VectorDrawableCompat();
        }

        public Drawable newDrawable(Resources resources) {
            return new VectorDrawableCompat();
        }

        public int getChangingConfigurations() {
            return this.f66a;
        }
    }

    /* renamed from: android.support.a.a.f.g */
    private static class VectorDrawableCompat extends ConstantState {
        private final ConstantState f78a;

        public VectorDrawableCompat(ConstantState constantState) {
            this.f78a = constantState;
        }

        public Drawable newDrawable() {
            Drawable vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.a = (VectorDrawable) this.f78a.newDrawable();
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            Drawable vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.a = (VectorDrawable) this.f78a.newDrawable(resources);
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Theme theme) {
            Drawable vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.a = (VectorDrawable) this.f78a.newDrawable(resources, theme);
            return vectorDrawableCompat;
        }

        public boolean canApplyTheme() {
            return this.f78a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f78a.getChangingConfigurations();
        }
    }

    public /* bridge */ /* synthetic */ void applyTheme(Theme theme) {
        super.applyTheme(theme);
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getLayoutDirection() {
        return super.getLayoutDirection();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ boolean isAutoMirrored() {
        return super.isAutoMirrored();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setAutoMirrored(boolean z) {
        super.setAutoMirrored(z);
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    static {
        f79b = Mode.SRC_IN;
    }

    private VectorDrawableCompat() {
        this.f84g = true;
        this.f86i = new float[9];
        this.f87j = new Matrix();
        this.f88k = new Rect();
        this.f80c = new VectorDrawableCompat();
    }

    private VectorDrawableCompat(VectorDrawableCompat vectorDrawableCompat) {
        this.f84g = true;
        this.f86i = new float[9];
        this.f87j = new Matrix();
        this.f88k = new Rect();
        this.f80c = vectorDrawableCompat;
        this.f81d = m71a(this.f81d, vectorDrawableCompat.f68c, vectorDrawableCompat.f69d);
    }

    public Drawable mutate() {
        if (this.a != null) {
            this.a.mutate();
        } else if (!this.f83f && super.mutate() == this) {
            this.f80c = new VectorDrawableCompat(this.f80c);
            this.f83f = true;
        }
        return this;
    }

    Object m72a(String str) {
        return this.f80c.f67b.f57g.get(str);
    }

    public ConstantState getConstantState() {
        if (this.a != null) {
            return new VectorDrawableCompat(this.a.getConstantState());
        }
        this.f80c.f66a = getChangingConfigurations();
        return this.f80c;
    }

    public void draw(Canvas canvas) {
        if (this.a != null) {
            this.a.draw(canvas);
            return;
        }
        copyBounds(this.f88k);
        if (this.f88k.width() > 0 && this.f88k.height() > 0) {
            ColorFilter colorFilter = this.f82e == null ? this.f81d : this.f82e;
            canvas.getMatrix(this.f87j);
            this.f87j.getValues(this.f86i);
            float abs = Math.abs(this.f86i[0]);
            float abs2 = Math.abs(this.f86i[4]);
            float abs3 = Math.abs(this.f86i[1]);
            float abs4 = Math.abs(this.f86i[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs2 = 1.0f;
                abs = 1.0f;
            }
            int height = (int) (abs2 * ((float) this.f88k.height()));
            int min = Math.min(2048, (int) (abs * ((float) this.f88k.width())));
            height = Math.min(2048, height);
            if (min > 0 && height > 0) {
                int save = canvas.save();
                canvas.translate((float) this.f88k.left, (float) this.f88k.top);
                if (m68a()) {
                    canvas.translate((float) this.f88k.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.f88k.offsetTo(0, 0);
                this.f80c.m59b(min, height);
                if (!this.f84g) {
                    this.f80c.m56a(min, height);
                } else if (!this.f80c.m60b()) {
                    this.f80c.m56a(min, height);
                    this.f80c.m61c();
                }
                this.f80c.m57a(canvas, colorFilter, this.f88k);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        if (this.a != null) {
            return DrawableCompat.m665c(this.a);
        }
        return this.f80c.f67b.m50a();
    }

    public void setAlpha(int i) {
        if (this.a != null) {
            this.a.setAlpha(i);
        } else if (this.f80c.f67b.m50a() != i) {
            this.f80c.f67b.m52a(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.a != null) {
            this.a.setColorFilter(colorFilter);
            return;
        }
        this.f82e = colorFilter;
        invalidateSelf();
    }

    PorterDuffColorFilter m71a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public void setTint(int i) {
        if (this.a != null) {
            DrawableCompat.m657a(this.a, i);
        } else {
            setTintList(ColorStateList.valueOf(i));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.a != null) {
            DrawableCompat.m659a(this.a, colorStateList);
            return;
        }
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        if (vectorDrawableCompat.f68c != colorStateList) {
            vectorDrawableCompat.f68c = colorStateList;
            this.f81d = m71a(this.f81d, colorStateList, vectorDrawableCompat.f69d);
            invalidateSelf();
        }
    }

    public void setTintMode(Mode mode) {
        if (this.a != null) {
            DrawableCompat.m662a(this.a, mode);
            return;
        }
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        if (vectorDrawableCompat.f69d != mode) {
            vectorDrawableCompat.f69d = mode;
            this.f81d = m71a(this.f81d, vectorDrawableCompat.f68c, mode);
            invalidateSelf();
        }
    }

    public boolean isStateful() {
        if (this.a != null) {
            return this.a.isStateful();
        }
        return super.isStateful() || !(this.f80c == null || this.f80c.f68c == null || !this.f80c.f68c.isStateful());
    }

    protected boolean onStateChange(int[] iArr) {
        if (this.a != null) {
            return this.a.setState(iArr);
        }
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        if (vectorDrawableCompat.f68c == null || vectorDrawableCompat.f69d == null) {
            return false;
        }
        this.f81d = m71a(this.f81d, vectorDrawableCompat.f68c, vectorDrawableCompat.f69d);
        invalidateSelf();
        return true;
    }

    public int getOpacity() {
        if (this.a != null) {
            return this.a.getOpacity();
        }
        return -3;
    }

    public int getIntrinsicWidth() {
        if (this.a != null) {
            return this.a.getIntrinsicWidth();
        }
        return (int) this.f80c.f67b.f51a;
    }

    public int getIntrinsicHeight() {
        if (this.a != null) {
            return this.a.getIntrinsicHeight();
        }
        return (int) this.f80c.f67b.f52b;
    }

    public boolean canApplyTheme() {
        if (this.a != null) {
            DrawableCompat.m666d(this.a);
        }
        return false;
    }

    public static VectorDrawableCompat m65a(Resources resources, int i, Theme theme) {
        if (VERSION.SDK_INT >= 23) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.a = ResourcesCompat.m74a(resources, i, theme);
            vectorDrawableCompat.f85h = new VectorDrawableCompat(vectorDrawableCompat.a.getConstantState());
            return vectorDrawableCompat;
        }
        try {
            int next;
            XmlPullParser xml = resources.getXml(i);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            do {
                next = xml.next();
                if (next == 2) {
                    break;
                }
            } while (next != 1);
            if (next == 2) {
                return VectorDrawableCompat.m66a(resources, xml, asAttributeSet, theme);
            }
            throw new XmlPullParserException("No start tag found");
        } catch (Throwable e) {
            Log.e("VectorDrawableCompat", "parser error", e);
            return null;
        } catch (Throwable e2) {
            Log.e("VectorDrawableCompat", "parser error", e2);
            return null;
        }
    }

    public static VectorDrawableCompat m66a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
        vectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return vectorDrawableCompat;
    }

    private static int m69b(int i, float f) {
        return (((int) (((float) Color.alpha(i)) * f)) << 24) | (16777215 & i);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        if (this.a != null) {
            this.a.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        if (this.a != null) {
            DrawableCompat.m661a(this.a, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        vectorDrawableCompat.f67b = new VectorDrawableCompat();
        TypedArray b = VectorDrawableCommon.m0b(resources, theme, attributeSet, AndroidResources.f0a);
        m67a(b, xmlPullParser);
        b.recycle();
        vectorDrawableCompat.f66a = getChangingConfigurations();
        vectorDrawableCompat.f76k = true;
        m70b(resources, xmlPullParser, attributeSet, theme);
        this.f81d = m71a(this.f81d, vectorDrawableCompat.f68c, vectorDrawableCompat.f69d);
    }

    private static Mode m64a(int i, Mode mode) {
        switch (i) {
            case R.View_paddingEnd /*3*/:
                return Mode.SRC_OVER;
            case R.Toolbar_contentInsetStart /*5*/:
                return Mode.SRC_IN;
            case R.Toolbar_popupTheme /*9*/:
                return Mode.SRC_ATOP;
            case R.Toolbar_titleMarginEnd /*14*/:
                return Mode.MULTIPLY;
            case R.Toolbar_titleMarginTop /*15*/:
                return Mode.SCREEN;
            case R.Toolbar_titleMarginBottom /*16*/:
                return Mode.ADD;
            default:
                return mode;
        }
    }

    private void m67a(TypedArray typedArray, XmlPullParser xmlPullParser) {
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        VectorDrawableCompat vectorDrawableCompat2 = vectorDrawableCompat.f67b;
        vectorDrawableCompat.f69d = VectorDrawableCompat.m64a(TypedArrayUtils.m20a(typedArray, xmlPullParser, "tintMode", 6, -1), Mode.SRC_IN);
        ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            vectorDrawableCompat.f68c = colorStateList;
        }
        vectorDrawableCompat.f70e = TypedArrayUtils.m21a(typedArray, xmlPullParser, "autoMirrored", 5, vectorDrawableCompat.f70e);
        vectorDrawableCompat2.f53c = TypedArrayUtils.m19a(typedArray, xmlPullParser, "viewportWidth", 7, vectorDrawableCompat2.f53c);
        vectorDrawableCompat2.f54d = TypedArrayUtils.m19a(typedArray, xmlPullParser, "viewportHeight", 8, vectorDrawableCompat2.f54d);
        if (vectorDrawableCompat2.f53c <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (vectorDrawableCompat2.f54d <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        } else {
            vectorDrawableCompat2.f51a = typedArray.getDimension(3, vectorDrawableCompat2.f51a);
            vectorDrawableCompat2.f52b = typedArray.getDimension(2, vectorDrawableCompat2.f52b);
            if (vectorDrawableCompat2.f51a <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (vectorDrawableCompat2.f52b <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            } else {
                vectorDrawableCompat2.m51a(TypedArrayUtils.m19a(typedArray, xmlPullParser, "alpha", 4, vectorDrawableCompat2.m54b()));
                String string = typedArray.getString(0);
                if (string != null) {
                    vectorDrawableCompat2.f56f = string;
                    vectorDrawableCompat2.f57g.put(string, vectorDrawableCompat2);
                }
            }
        }
    }

    private void m70b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        VectorDrawableCompat vectorDrawableCompat = this.f80c;
        VectorDrawableCompat vectorDrawableCompat2 = vectorDrawableCompat.f67b;
        Stack stack = new Stack();
        stack.push(vectorDrawableCompat2.f65p);
        int eventType = xmlPullParser.getEventType();
        Object obj = 1;
        while (eventType != 1) {
            if (eventType == 2) {
                Object obj2;
                String name = xmlPullParser.getName();
                VectorDrawableCompat vectorDrawableCompat3 = (VectorDrawableCompat) stack.peek();
                if ("path".equals(name)) {
                    VectorDrawableCompat vectorDrawableCompat4 = new VectorDrawableCompat();
                    vectorDrawableCompat4.m33a(resources, attributeSet, theme, xmlPullParser);
                    vectorDrawableCompat3.f37a.add(vectorDrawableCompat4);
                    if (vectorDrawableCompat4.m26b() != null) {
                        vectorDrawableCompat2.f57g.put(vectorDrawableCompat4.m26b(), vectorDrawableCompat4);
                    }
                    obj2 = null;
                    vectorDrawableCompat.f66a = vectorDrawableCompat4.o | vectorDrawableCompat.f66a;
                } else if ("clip-path".equals(name)) {
                    VectorDrawableCompat vectorDrawableCompat5 = new VectorDrawableCompat();
                    vectorDrawableCompat5.m28a(resources, attributeSet, theme, xmlPullParser);
                    vectorDrawableCompat3.f37a.add(vectorDrawableCompat5);
                    if (vectorDrawableCompat5.m26b() != null) {
                        vectorDrawableCompat2.f57g.put(vectorDrawableCompat5.m26b(), vectorDrawableCompat5);
                    }
                    vectorDrawableCompat.f66a |= vectorDrawableCompat5.o;
                    obj2 = obj;
                } else {
                    if ("group".equals(name)) {
                        VectorDrawableCompat vectorDrawableCompat6 = new VectorDrawableCompat();
                        vectorDrawableCompat6.m40a(resources, attributeSet, theme, xmlPullParser);
                        vectorDrawableCompat3.f37a.add(vectorDrawableCompat6);
                        stack.push(vectorDrawableCompat6);
                        if (vectorDrawableCompat6.m39a() != null) {
                            vectorDrawableCompat2.f57g.put(vectorDrawableCompat6.m39a(), vectorDrawableCompat6);
                        }
                        vectorDrawableCompat.f66a |= vectorDrawableCompat6.f47k;
                    }
                    obj2 = obj;
                }
                obj = obj2;
            } else if (eventType == 3) {
                if ("group".equals(xmlPullParser.getName())) {
                    stack.pop();
                }
            }
            eventType = xmlPullParser.next();
        }
        if (obj != null) {
            StringBuffer stringBuffer = new StringBuffer();
            if (stringBuffer.length() > 0) {
                stringBuffer.append(" or ");
            }
            stringBuffer.append("path");
            throw new XmlPullParserException("no " + stringBuffer + " defined");
        }
    }

    void m73a(boolean z) {
        this.f84g = z;
    }

    private boolean m68a() {
        return false;
    }

    protected void onBoundsChange(Rect rect) {
        if (this.a != null) {
            this.a.setBounds(rect);
        }
    }

    public int getChangingConfigurations() {
        if (this.a != null) {
            return this.a.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f80c.getChangingConfigurations();
    }

    public void invalidateSelf() {
        if (this.a != null) {
            this.a.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public void scheduleSelf(Runnable runnable, long j) {
        if (this.a != null) {
            this.a.scheduleSelf(runnable, j);
        } else {
            super.scheduleSelf(runnable, j);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.a != null) {
            return this.a.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        if (this.a != null) {
            this.a.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }
}
