package android.support.p000a.p001a;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Build.VERSION;
import android.support.v4.p002b.p003a.DrawableCompat;
import android.support.v4.p010e.ArrayMap;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

@TargetApi(21)
/* renamed from: android.support.a.a.b */
public class AnimatedVectorDrawableCompat extends VectorDrawableCommon implements Animatable {
    private AnimatedVectorDrawableCompat f13b;
    private Context f14c;
    private ArgbEvaluator f15d;
    private final Callback f16e;

    /* renamed from: android.support.a.a.b.1 */
    class AnimatedVectorDrawableCompat implements Callback {
        final /* synthetic */ AnimatedVectorDrawableCompat f6a;

        AnimatedVectorDrawableCompat(AnimatedVectorDrawableCompat animatedVectorDrawableCompat) {
            this.f6a = animatedVectorDrawableCompat;
        }

        public void invalidateDrawable(Drawable drawable) {
            this.f6a.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            this.f6a.scheduleSelf(runnable, j);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            this.f6a.unscheduleSelf(runnable);
        }
    }

    /* renamed from: android.support.a.a.b.a */
    private static class AnimatedVectorDrawableCompat extends ConstantState {
        int f7a;
        VectorDrawableCompat f8b;
        ArrayList<Animator> f9c;
        ArrayMap<Animator, String> f10d;

        public AnimatedVectorDrawableCompat(Context context, AnimatedVectorDrawableCompat animatedVectorDrawableCompat, Callback callback, Resources resources) {
            int i = 0;
            if (animatedVectorDrawableCompat != null) {
                this.f7a = animatedVectorDrawableCompat.f7a;
                if (animatedVectorDrawableCompat.f8b != null) {
                    ConstantState constantState = animatedVectorDrawableCompat.f8b.getConstantState();
                    if (resources != null) {
                        this.f8b = (VectorDrawableCompat) constantState.newDrawable(resources);
                    } else {
                        this.f8b = (VectorDrawableCompat) constantState.newDrawable();
                    }
                    this.f8b = (VectorDrawableCompat) this.f8b.mutate();
                    this.f8b.setCallback(callback);
                    this.f8b.setBounds(animatedVectorDrawableCompat.f8b.getBounds());
                    this.f8b.m73a(false);
                }
                if (animatedVectorDrawableCompat.f9c != null) {
                    int size = animatedVectorDrawableCompat.f9c.size();
                    this.f9c = new ArrayList(size);
                    this.f10d = new ArrayMap(size);
                    while (i < size) {
                        Animator animator = (Animator) animatedVectorDrawableCompat.f9c.get(i);
                        Animator clone = animator.clone();
                        String str = (String) animatedVectorDrawableCompat.f10d.get(animator);
                        clone.setTarget(this.f8b.m72a(str));
                        this.f9c.add(clone);
                        this.f10d.put(clone, str);
                        i++;
                    }
                }
            }
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 23.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 23.");
        }

        public int getChangingConfigurations() {
            return this.f7a;
        }
    }

    /* renamed from: android.support.a.a.b.b */
    private static class AnimatedVectorDrawableCompat extends ConstantState {
        private final ConstantState f11a;

        public AnimatedVectorDrawableCompat(ConstantState constantState) {
            this.f11a = constantState;
        }

        public Drawable newDrawable() {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.a = this.f11a.newDrawable();
            animatedVectorDrawableCompat.a.setCallback(animatedVectorDrawableCompat.f16e);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.a = this.f11a.newDrawable(resources);
            animatedVectorDrawableCompat.a.setCallback(animatedVectorDrawableCompat.f16e);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Theme theme) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.a = this.f11a.newDrawable(resources, theme);
            animatedVectorDrawableCompat.a.setCallback(animatedVectorDrawableCompat.f16e);
            return animatedVectorDrawableCompat;
        }

        public boolean canApplyTheme() {
            return this.f11a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f11a.getChangingConfigurations();
        }
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getLayoutDirection() {
        return super.getLayoutDirection();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ boolean isAutoMirrored() {
        return super.isAutoMirrored();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setAutoMirrored(boolean z) {
        super.setAutoMirrored(z);
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    private AnimatedVectorDrawableCompat() {
        this(null, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context) {
        this(context, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context, AnimatedVectorDrawableCompat animatedVectorDrawableCompat, Resources resources) {
        this.f15d = null;
        this.f16e = new AnimatedVectorDrawableCompat(this);
        this.f14c = context;
        if (animatedVectorDrawableCompat != null) {
            this.f13b = animatedVectorDrawableCompat;
        } else {
            this.f13b = new AnimatedVectorDrawableCompat(context, animatedVectorDrawableCompat, this.f16e, resources);
        }
    }

    public Drawable mutate() {
        if (this.a != null) {
            this.a.mutate();
            return this;
        }
        throw new IllegalStateException("Mutate() is not supported for older platform");
    }

    public static AnimatedVectorDrawableCompat m3a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
        animatedVectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return animatedVectorDrawableCompat;
    }

    public ConstantState getConstantState() {
        if (this.a != null) {
            return new AnimatedVectorDrawableCompat(this.a.getConstantState());
        }
        return null;
    }

    public int getChangingConfigurations() {
        if (this.a != null) {
            return this.a.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f13b.f7a;
    }

    public void draw(Canvas canvas) {
        if (this.a != null) {
            this.a.draw(canvas);
            return;
        }
        this.f13b.f8b.draw(canvas);
        if (m6a()) {
            invalidateSelf();
        }
    }

    protected void onBoundsChange(Rect rect) {
        if (this.a != null) {
            this.a.setBounds(rect);
        } else {
            this.f13b.f8b.setBounds(rect);
        }
    }

    protected boolean onStateChange(int[] iArr) {
        if (this.a != null) {
            return this.a.setState(iArr);
        }
        return this.f13b.f8b.setState(iArr);
    }

    protected boolean onLevelChange(int i) {
        if (this.a != null) {
            return this.a.setLevel(i);
        }
        return this.f13b.f8b.setLevel(i);
    }

    public int getAlpha() {
        if (this.a != null) {
            return DrawableCompat.m665c(this.a);
        }
        return this.f13b.f8b.getAlpha();
    }

    public void setAlpha(int i) {
        if (this.a != null) {
            this.a.setAlpha(i);
        } else {
            this.f13b.f8b.setAlpha(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.a != null) {
            this.a.setColorFilter(colorFilter);
        } else {
            this.f13b.f8b.setColorFilter(colorFilter);
        }
    }

    public void setTint(int i) {
        if (this.a != null) {
            DrawableCompat.m657a(this.a, i);
        } else {
            this.f13b.f8b.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.a != null) {
            DrawableCompat.m659a(this.a, colorStateList);
        } else {
            this.f13b.f8b.setTintList(colorStateList);
        }
    }

    public void setTintMode(Mode mode) {
        if (this.a != null) {
            DrawableCompat.m662a(this.a, mode);
        } else {
            this.f13b.f8b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.a != null) {
            return this.a.setVisible(z, z2);
        }
        this.f13b.f8b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public boolean isStateful() {
        if (this.a != null) {
            return this.a.isStateful();
        }
        return this.f13b.f8b.isStateful();
    }

    public int getOpacity() {
        if (this.a != null) {
            return this.a.getOpacity();
        }
        return this.f13b.f8b.getOpacity();
    }

    public int getIntrinsicWidth() {
        if (this.a != null) {
            return this.a.getIntrinsicWidth();
        }
        return this.f13b.f8b.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        if (this.a != null) {
            return this.a.getIntrinsicHeight();
        }
        return this.f13b.f8b.getIntrinsicHeight();
    }

    static TypedArray m1a(Resources resources, Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        if (this.a != null) {
            DrawableCompat.m661a(this.a, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                TypedArray a;
                if ("animated-vector".equals(name)) {
                    a = AnimatedVectorDrawableCompat.m1a(resources, theme, attributeSet, AndroidResources.f4e);
                    int resourceId = a.getResourceId(0, 0);
                    if (resourceId != 0) {
                        VectorDrawableCompat a2 = VectorDrawableCompat.m65a(resources, resourceId, theme);
                        a2.m73a(false);
                        a2.setCallback(this.f16e);
                        if (this.f13b.f8b != null) {
                            this.f13b.f8b.setCallback(null);
                        }
                        this.f13b.f8b = a2;
                    }
                    a.recycle();
                } else if ("target".equals(name)) {
                    a = resources.obtainAttributes(attributeSet, AndroidResources.f5f);
                    String string = a.getString(0);
                    int resourceId2 = a.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        if (this.f14c != null) {
                            m5a(string, AnimatorInflater.loadAnimator(this.f14c, resourceId2));
                        } else {
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    a.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    public void applyTheme(Theme theme) {
        if (this.a != null) {
            DrawableCompat.m660a(this.a, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.a != null) {
            return DrawableCompat.m666d(this.a);
        }
        return false;
    }

    private void m4a(Animator animator) {
        if (animator instanceof AnimatorSet) {
            List childAnimations = ((AnimatorSet) animator).getChildAnimations();
            if (childAnimations != null) {
                for (int i = 0; i < childAnimations.size(); i++) {
                    m4a((Animator) childAnimations.get(i));
                }
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f15d == null) {
                    this.f15d = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f15d);
            }
        }
    }

    private void m5a(String str, Animator animator) {
        animator.setTarget(this.f13b.f8b.m72a(str));
        if (VERSION.SDK_INT < 21) {
            m4a(animator);
        }
        if (this.f13b.f9c == null) {
            this.f13b.f9c = new ArrayList();
            this.f13b.f10d = new ArrayMap();
        }
        this.f13b.f9c.add(animator);
        this.f13b.f10d.put(animator, str);
    }

    public boolean isRunning() {
        if (this.a != null) {
            return ((AnimatedVectorDrawable) this.a).isRunning();
        }
        ArrayList arrayList = this.f13b.f9c;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (((Animator) arrayList.get(i)).isRunning()) {
                return true;
            }
        }
        return false;
    }

    private boolean m6a() {
        ArrayList arrayList = this.f13b.f9c;
        if (arrayList == null) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (((Animator) arrayList.get(i)).isRunning()) {
                return true;
            }
        }
        return false;
    }

    public void start() {
        if (this.a != null) {
            ((AnimatedVectorDrawable) this.a).start();
        } else if (!m6a()) {
            ArrayList arrayList = this.f13b.f9c;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ((Animator) arrayList.get(i)).start();
            }
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.a != null) {
            ((AnimatedVectorDrawable) this.a).stop();
            return;
        }
        ArrayList arrayList = this.f13b.f9c;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((Animator) arrayList.get(i)).end();
        }
    }
}
