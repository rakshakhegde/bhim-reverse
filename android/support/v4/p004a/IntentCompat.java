package android.support.v4.p004a;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build.VERSION;

/* renamed from: android.support.v4.a.f */
public final class IntentCompat {
    private static final IntentCompat f89a;

    /* renamed from: android.support.v4.a.f.a */
    interface IntentCompat {
        Intent m89a(ComponentName componentName);
    }

    /* renamed from: android.support.v4.a.f.b */
    static class IntentCompat implements IntentCompat {
        IntentCompat() {
        }

        public Intent m90a(ComponentName componentName) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setComponent(componentName);
            intent.addCategory("android.intent.category.LAUNCHER");
            return intent;
        }
    }

    /* renamed from: android.support.v4.a.f.c */
    static class IntentCompat extends IntentCompat {
        IntentCompat() {
        }

        public Intent m91a(ComponentName componentName) {
            return IntentCompatHoneycomb.m93a(componentName);
        }
    }

    /* renamed from: android.support.v4.a.f.d */
    static class IntentCompat extends IntentCompat {
        IntentCompat() {
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 15) {
            f89a = new IntentCompat();
        } else if (i >= 11) {
            f89a = new IntentCompat();
        } else {
            f89a = new IntentCompat();
        }
    }

    public static Intent m92a(ComponentName componentName) {
        return f89a.m89a(componentName);
    }
}
