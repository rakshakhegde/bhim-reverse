package android.support.v4.p004a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.io.File;

/* renamed from: android.support.v4.a.b */
class ContextCompatApi21 {
    public static Drawable m83a(Context context, int i) {
        return context.getDrawable(i);
    }

    public static File m84a(Context context) {
        return context.getNoBackupFilesDir();
    }
}
