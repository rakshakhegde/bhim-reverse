package android.support.v4.p004a;

import android.content.Context;
import android.content.res.ColorStateList;

/* renamed from: android.support.v4.a.c */
class ContextCompatApi23 {
    public static ColorStateList m85a(Context context, int i) {
        return context.getColorStateList(i);
    }

    public static int m86b(Context context, int i) {
        return context.getColor(i);
    }
}
