package android.support.v4.p004a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/* renamed from: android.support.v4.a.e */
class ContextCompatJellybean {
    public static void m88a(Context context, Intent[] intentArr, Bundle bundle) {
        context.startActivities(intentArr, bundle);
    }
}
