package android.support.v4.p004a;

import android.content.ComponentName;
import android.content.Intent;

/* renamed from: android.support.v4.a.g */
class IntentCompatHoneycomb {
    public static Intent m93a(ComponentName componentName) {
        return Intent.makeMainActivity(componentName);
    }
}
