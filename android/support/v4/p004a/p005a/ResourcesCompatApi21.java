package android.support.v4.p004a.p005a;

import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;

/* renamed from: android.support.v4.a.a.b */
class ResourcesCompatApi21 {
    public static Drawable m75a(Resources resources, int i, Theme theme) {
        return resources.getDrawable(i, theme);
    }
}
