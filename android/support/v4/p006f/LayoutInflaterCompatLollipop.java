package android.support.v4.p006f;

import android.support.v4.p006f.LayoutInflaterCompatHC.LayoutInflaterCompatHC;
import android.view.LayoutInflater;

/* renamed from: android.support.v4.f.l */
class LayoutInflaterCompatLollipop {
    static void m1443a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        layoutInflater.setFactory2(layoutInflaterFactory != null ? new LayoutInflaterCompatHC(layoutInflaterFactory) : null);
    }
}
