package android.support.v4.p006f;

import android.view.View;

/* compiled from: ViewCompatKitKat */
/* renamed from: android.support.v4.f.an */
class an {
    public static boolean m1246a(View view) {
        return view.isLaidOut();
    }

    public static boolean m1247b(View view) {
        return view.isAttachedToWindow();
    }
}
