package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/* compiled from: ViewPropertyAnimatorCompat */
/* renamed from: android.support.v4.f.au */
public final class au {
    static final ViewPropertyAnimatorCompat f538a;
    private WeakReference<View> f539b;
    private Runnable f540c;
    private Runnable f541d;
    private int f542e;

    /* renamed from: android.support.v4.f.au.g */
    interface ViewPropertyAnimatorCompat {
        long m1299a(au auVar, View view);

        void m1300a(au auVar, View view, float f);

        void m1301a(au auVar, View view, long j);

        void m1302a(au auVar, View view, ay ayVar);

        void m1303a(au auVar, View view, ba baVar);

        void m1304a(au auVar, View view, Interpolator interpolator);

        void m1305b(au auVar, View view);

        void m1306b(au auVar, View view, float f);

        void m1307b(au auVar, View view, long j);

        void m1308c(au auVar, View view);

        void m1309c(au auVar, View view, float f);

        void m1310d(au auVar, View view, float f);
    }

    /* renamed from: android.support.v4.f.au.a */
    static class ViewPropertyAnimatorCompat implements ViewPropertyAnimatorCompat {
        WeakHashMap<View, Runnable> f534a;

        /* renamed from: android.support.v4.f.au.a.a */
        class ViewPropertyAnimatorCompat implements Runnable {
            WeakReference<View> f531a;
            au f532b;
            final /* synthetic */ ViewPropertyAnimatorCompat f533c;

            private ViewPropertyAnimatorCompat(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, au auVar, View view) {
                this.f533c = viewPropertyAnimatorCompat;
                this.f531a = new WeakReference(view);
                this.f532b = auVar;
            }

            public void run() {
                View view = (View) this.f531a.get();
                if (view != null) {
                    this.f533c.m1313d(this.f532b, view);
                }
            }
        }

        ViewPropertyAnimatorCompat() {
            this.f534a = null;
        }

        public void m1317a(au auVar, View view, long j) {
        }

        public void m1316a(au auVar, View view, float f) {
            m1314e(auVar, view);
        }

        public void m1322b(au auVar, View view, float f) {
            m1314e(auVar, view);
        }

        public long m1315a(au auVar, View view) {
            return 0;
        }

        public void m1320a(au auVar, View view, Interpolator interpolator) {
        }

        public void m1323b(au auVar, View view, long j) {
        }

        public void m1325c(au auVar, View view, float f) {
            m1314e(auVar, view);
        }

        public void m1326d(au auVar, View view, float f) {
            m1314e(auVar, view);
        }

        public void m1321b(au auVar, View view) {
            m1314e(auVar, view);
        }

        public void m1324c(au auVar, View view) {
            m1312a(view);
            m1313d(auVar, view);
        }

        public void m1318a(au auVar, View view, ay ayVar) {
            view.setTag(2113929216, ayVar);
        }

        public void m1319a(au auVar, View view, ba baVar) {
        }

        private void m1313d(au auVar, View view) {
            ay ayVar;
            Object tag = view.getTag(2113929216);
            if (tag instanceof ay) {
                ayVar = (ay) tag;
            } else {
                ayVar = null;
            }
            Runnable a = auVar.f540c;
            Runnable b = auVar.f541d;
            auVar.f540c = null;
            auVar.f541d = null;
            if (a != null) {
                a.run();
            }
            if (ayVar != null) {
                ayVar.m1327a(view);
                ayVar.m1328b(view);
            }
            if (b != null) {
                b.run();
            }
            if (this.f534a != null) {
                this.f534a.remove(view);
            }
        }

        private void m1312a(View view) {
            if (this.f534a != null) {
                Runnable runnable = (Runnable) this.f534a.get(view);
                if (runnable != null) {
                    view.removeCallbacks(runnable);
                }
            }
        }

        private void m1314e(au auVar, View view) {
            Runnable runnable;
            if (this.f534a != null) {
                runnable = (Runnable) this.f534a.get(view);
            } else {
                runnable = null;
            }
            if (runnable == null) {
                runnable = new ViewPropertyAnimatorCompat(auVar, view, null);
                if (this.f534a == null) {
                    this.f534a = new WeakHashMap();
                }
                this.f534a.put(view, runnable);
            }
            view.removeCallbacks(runnable);
            view.post(runnable);
        }
    }

    /* renamed from: android.support.v4.f.au.b */
    static class ViewPropertyAnimatorCompat extends ViewPropertyAnimatorCompat {
        WeakHashMap<View, Integer> f537b;

        /* renamed from: android.support.v4.f.au.b.a */
        static class ViewPropertyAnimatorCompat implements ay {
            au f535a;
            boolean f536b;

            ViewPropertyAnimatorCompat(au auVar) {
                this.f535a = auVar;
            }

            public void m1330a(View view) {
                ay ayVar;
                this.f536b = false;
                if (this.f535a.f542e >= 0) {
                    af.m1177a(view, 2, null);
                }
                if (this.f535a.f540c != null) {
                    Runnable a = this.f535a.f540c;
                    this.f535a.f540c = null;
                    a.run();
                }
                Object tag = view.getTag(2113929216);
                if (tag instanceof ay) {
                    ayVar = (ay) tag;
                } else {
                    ayVar = null;
                }
                if (ayVar != null) {
                    ayVar.m1327a(view);
                }
            }

            public void m1331b(View view) {
                if (this.f535a.f542e >= 0) {
                    af.m1177a(view, this.f535a.f542e, null);
                    this.f535a.f542e = -1;
                }
                if (VERSION.SDK_INT >= 16 || !this.f536b) {
                    ay ayVar;
                    if (this.f535a.f541d != null) {
                        Runnable b = this.f535a.f541d;
                        this.f535a.f541d = null;
                        b.run();
                    }
                    Object tag = view.getTag(2113929216);
                    if (tag instanceof ay) {
                        ayVar = (ay) tag;
                    } else {
                        ayVar = null;
                    }
                    if (ayVar != null) {
                        ayVar.m1328b(view);
                    }
                    this.f536b = true;
                }
            }

            public void m1332c(View view) {
                ay ayVar;
                Object tag = view.getTag(2113929216);
                if (tag instanceof ay) {
                    ayVar = (ay) tag;
                } else {
                    ayVar = null;
                }
                if (ayVar != null) {
                    ayVar.m1329c(view);
                }
            }
        }

        ViewPropertyAnimatorCompat() {
            this.f537b = null;
        }

        public void m1335a(au auVar, View view, long j) {
            av.m1366a(view, j);
        }

        public void m1334a(au auVar, View view, float f) {
            av.m1365a(view, f);
        }

        public void m1339b(au auVar, View view, float f) {
            av.m1370b(view, f);
        }

        public long m1333a(au auVar, View view) {
            return av.m1364a(view);
        }

        public void m1337a(au auVar, View view, Interpolator interpolator) {
            av.m1368a(view, interpolator);
        }

        public void m1340b(au auVar, View view, long j) {
            av.m1371b(view, j);
        }

        public void m1342c(au auVar, View view, float f) {
            av.m1373c(view, f);
        }

        public void m1343d(au auVar, View view, float f) {
            av.m1374d(view, f);
        }

        public void m1338b(au auVar, View view) {
            av.m1369b(view);
        }

        public void m1341c(au auVar, View view) {
            av.m1372c(view);
        }

        public void m1336a(au auVar, View view, ay ayVar) {
            view.setTag(2113929216, ayVar);
            av.m1367a(view, new ViewPropertyAnimatorCompat(auVar));
        }
    }

    /* renamed from: android.support.v4.f.au.d */
    static class ViewPropertyAnimatorCompat extends ViewPropertyAnimatorCompat {
        ViewPropertyAnimatorCompat() {
        }

        public void m1344a(au auVar, View view, ay ayVar) {
            aw.m1375a(view, ayVar);
        }
    }

    /* renamed from: android.support.v4.f.au.c */
    static class ViewPropertyAnimatorCompat extends ViewPropertyAnimatorCompat {
        ViewPropertyAnimatorCompat() {
        }
    }

    /* renamed from: android.support.v4.f.au.e */
    static class ViewPropertyAnimatorCompat extends ViewPropertyAnimatorCompat {
        ViewPropertyAnimatorCompat() {
        }

        public void m1345a(au auVar, View view, ba baVar) {
            ax.m1376a(view, baVar);
        }
    }

    /* renamed from: android.support.v4.f.au.f */
    static class ViewPropertyAnimatorCompat extends ViewPropertyAnimatorCompat {
        ViewPropertyAnimatorCompat() {
        }
    }

    au(View view) {
        this.f540c = null;
        this.f541d = null;
        this.f542e = -1;
        this.f539b = new WeakReference(view);
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 21) {
            f538a = new ViewPropertyAnimatorCompat();
        } else if (i >= 19) {
            f538a = new ViewPropertyAnimatorCompat();
        } else if (i >= 18) {
            f538a = new ViewPropertyAnimatorCompat();
        } else if (i >= 16) {
            f538a = new ViewPropertyAnimatorCompat();
        } else if (i >= 14) {
            f538a = new ViewPropertyAnimatorCompat();
        } else {
            f538a = new ViewPropertyAnimatorCompat();
        }
    }

    public au m1354a(long j) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1301a(this, view, j);
        }
        return this;
    }

    public au m1353a(float f) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1300a(this, view, f);
        }
        return this;
    }

    public au m1358b(float f) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1306b(this, view, f);
        }
        return this;
    }

    public long m1352a() {
        View view = (View) this.f539b.get();
        if (view != null) {
            return f538a.m1299a(this, view);
        }
        return 0;
    }

    public au m1357a(Interpolator interpolator) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1304a(this, view, interpolator);
        }
        return this;
    }

    public au m1359b(long j) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1307b(this, view, j);
        }
        return this;
    }

    public au m1361c(float f) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1309c(this, view, f);
        }
        return this;
    }

    public au m1363d(float f) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1310d(this, view, f);
        }
        return this;
    }

    public void m1360b() {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1305b(this, view);
        }
    }

    public void m1362c() {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1308c(this, view);
        }
    }

    public au m1355a(ay ayVar) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1302a(this, view, ayVar);
        }
        return this;
    }

    public au m1356a(ba baVar) {
        View view = (View) this.f539b.get();
        if (view != null) {
            f538a.m1303a(this, view, baVar);
        }
        return this;
    }
}
