package android.support.v4.p006f.p012b;

import android.view.animation.Interpolator;

/* renamed from: android.support.v4.f.b.b */
abstract class LookupTableInterpolator implements Interpolator {
    private final float[] f550a;
    private final float f551b;

    public LookupTableInterpolator(float[] fArr) {
        this.f550a = fArr;
        this.f551b = 1.0f / ((float) (this.f550a.length - 1));
    }

    public float getInterpolation(float f) {
        if (f >= 1.0f) {
            return 1.0f;
        }
        if (f <= 0.0f) {
            return 0.0f;
        }
        int min = Math.min((int) (((float) (this.f550a.length - 1)) * f), this.f550a.length - 2);
        float f2 = (f - (((float) min) * this.f551b)) / this.f551b;
        return ((this.f550a[min + 1] - this.f550a[min]) * f2) + this.f550a[min];
    }
}
