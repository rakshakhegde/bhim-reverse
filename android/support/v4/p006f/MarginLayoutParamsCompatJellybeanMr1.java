package android.support.v4.p006f;

import android.view.ViewGroup.MarginLayoutParams;

/* renamed from: android.support.v4.f.o */
class MarginLayoutParamsCompatJellybeanMr1 {
    public static int m1452a(MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginStart();
    }

    public static int m1453b(MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginEnd();
    }
}
