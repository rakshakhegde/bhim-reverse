package android.support.v4.p006f;

import android.os.Build.VERSION;

/* renamed from: android.support.v4.f.e */
public final class GravityCompat {
    static final GravityCompat f558a;

    /* renamed from: android.support.v4.f.e.a */
    interface GravityCompat {
        int m1417a(int i, int i2);
    }

    /* renamed from: android.support.v4.f.e.b */
    static class GravityCompat implements GravityCompat {
        GravityCompat() {
        }

        public int m1418a(int i, int i2) {
            return -8388609 & i;
        }
    }

    /* renamed from: android.support.v4.f.e.c */
    static class GravityCompat implements GravityCompat {
        GravityCompat() {
        }

        public int m1419a(int i, int i2) {
            return GravityCompatJellybeanMr1.m1421a(i, i2);
        }
    }

    static {
        if (VERSION.SDK_INT >= 17) {
            f558a = new GravityCompat();
        } else {
            f558a = new GravityCompat();
        }
    }

    public static int m1420a(int i, int i2) {
        return f558a.m1417a(i, i2);
    }
}
