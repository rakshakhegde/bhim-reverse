package android.support.v4.p006f;

import android.view.View;
import android.view.View.AccessibilityDelegate;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: android.support.v4.f.b */
class AccessibilityDelegateCompatIcs {

    /* renamed from: android.support.v4.f.b.a */
    public interface AccessibilityDelegateCompatIcs {
        void m811a(View view, int i);

        void m812a(View view, Object obj);

        boolean m813a(View view, AccessibilityEvent accessibilityEvent);

        boolean m814a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent);

        void m815b(View view, AccessibilityEvent accessibilityEvent);

        void m816c(View view, AccessibilityEvent accessibilityEvent);

        void m817d(View view, AccessibilityEvent accessibilityEvent);
    }

    /* renamed from: android.support.v4.f.b.1 */
    static class AccessibilityDelegateCompatIcs extends AccessibilityDelegate {
        final /* synthetic */ AccessibilityDelegateCompatIcs f549a;

        AccessibilityDelegateCompatIcs(AccessibilityDelegateCompatIcs accessibilityDelegateCompatIcs) {
            this.f549a = accessibilityDelegateCompatIcs;
        }

        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.f549a.m813a(view, accessibilityEvent);
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f549a.m815b(view, accessibilityEvent);
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            this.f549a.m812a(view, (Object) accessibilityNodeInfo);
        }

        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f549a.m816c(view, accessibilityEvent);
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.f549a.m814a(viewGroup, view, accessibilityEvent);
        }

        public void sendAccessibilityEvent(View view, int i) {
            this.f549a.m811a(view, i);
        }

        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.f549a.m817d(view, accessibilityEvent);
        }
    }

    public static Object m1380a() {
        return new AccessibilityDelegate();
    }

    public static Object m1381a(AccessibilityDelegateCompatIcs accessibilityDelegateCompatIcs) {
        return new AccessibilityDelegateCompatIcs(accessibilityDelegateCompatIcs);
    }

    public static boolean m1384a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        return ((AccessibilityDelegate) obj).dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public static void m1386b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        ((AccessibilityDelegate) obj).onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    public static void m1383a(Object obj, View view, Object obj2) {
        ((AccessibilityDelegate) obj).onInitializeAccessibilityNodeInfo(view, (AccessibilityNodeInfo) obj2);
    }

    public static void m1387c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        ((AccessibilityDelegate) obj).onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public static boolean m1385a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return ((AccessibilityDelegate) obj).onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    public static void m1382a(Object obj, View view, int i) {
        ((AccessibilityDelegate) obj).sendAccessibilityEvent(view, i);
    }

    public static void m1388d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        ((AccessibilityDelegate) obj).sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
}
