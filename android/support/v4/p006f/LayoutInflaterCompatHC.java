package android.support.v4.p006f;

import android.content.Context;
import android.support.v4.p006f.LayoutInflaterCompatBase.LayoutInflaterCompatBase;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.LayoutInflater.Factory2;
import android.view.View;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.f.k */
class LayoutInflaterCompatHC {
    private static Field f562a;
    private static boolean f563b;

    /* renamed from: android.support.v4.f.k.a */
    static class LayoutInflaterCompatHC extends LayoutInflaterCompatBase implements Factory2 {
        LayoutInflaterCompatHC(LayoutInflaterFactory layoutInflaterFactory) {
            super(layoutInflaterFactory);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.a.m467a(view, str, context, attributeSet);
        }
    }

    static void m1441a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        Factory2 layoutInflaterCompatHC;
        if (layoutInflaterFactory != null) {
            layoutInflaterCompatHC = new LayoutInflaterCompatHC(layoutInflaterFactory);
        } else {
            layoutInflaterCompatHC = null;
        }
        layoutInflater.setFactory2(layoutInflaterCompatHC);
        Factory factory = layoutInflater.getFactory();
        if (factory instanceof Factory2) {
            LayoutInflaterCompatHC.m1442a(layoutInflater, (Factory2) factory);
        } else {
            LayoutInflaterCompatHC.m1442a(layoutInflater, layoutInflaterCompatHC);
        }
    }

    static void m1442a(LayoutInflater layoutInflater, Factory2 factory2) {
        if (!f563b) {
            try {
                f562a = LayoutInflater.class.getDeclaredField("mFactory2");
                f562a.setAccessible(true);
            } catch (Throwable e) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 Could not find field 'mFactory2' on class " + LayoutInflater.class.getName() + "; inflation may have unexpected results.", e);
            }
            f563b = true;
        }
        if (f562a != null) {
            try {
                f562a.set(layoutInflater, factory2);
            } catch (Throwable e2) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 could not set the Factory2 on LayoutInflater " + layoutInflater + "; inflation may have unexpected results.", e2);
            }
        }
    }
}
