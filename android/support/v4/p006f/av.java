package android.support.v4.p006f;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;

/* compiled from: ViewPropertyAnimatorCompatICS */
/* renamed from: android.support.v4.f.av */
class av {

    /* renamed from: android.support.v4.f.av.1 */
    static class ViewPropertyAnimatorCompatICS extends AnimatorListenerAdapter {
        final /* synthetic */ ay f543a;
        final /* synthetic */ View f544b;

        ViewPropertyAnimatorCompatICS(ay ayVar, View view) {
            this.f543a = ayVar;
            this.f544b = view;
        }

        public void onAnimationCancel(Animator animator) {
            this.f543a.m1329c(this.f544b);
        }

        public void onAnimationEnd(Animator animator) {
            this.f543a.m1328b(this.f544b);
        }

        public void onAnimationStart(Animator animator) {
            this.f543a.m1327a(this.f544b);
        }
    }

    public static void m1366a(View view, long j) {
        view.animate().setDuration(j);
    }

    public static void m1365a(View view, float f) {
        view.animate().alpha(f);
    }

    public static void m1370b(View view, float f) {
        view.animate().translationY(f);
    }

    public static long m1364a(View view) {
        return view.animate().getDuration();
    }

    public static void m1368a(View view, Interpolator interpolator) {
        view.animate().setInterpolator(interpolator);
    }

    public static void m1371b(View view, long j) {
        view.animate().setStartDelay(j);
    }

    public static void m1373c(View view, float f) {
        view.animate().scaleX(f);
    }

    public static void m1374d(View view, float f) {
        view.animate().scaleY(f);
    }

    public static void m1369b(View view) {
        view.animate().cancel();
    }

    public static void m1372c(View view) {
        view.animate().start();
    }

    public static void m1367a(View view, ay ayVar) {
        if (ayVar != null) {
            view.animate().setListener(new ViewPropertyAnimatorCompatICS(ayVar, view));
        } else {
            view.animate().setListener(null);
        }
    }
}
