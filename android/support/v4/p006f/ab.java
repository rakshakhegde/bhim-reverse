package android.support.v4.p006f;

/* compiled from: ScrollingView */
/* renamed from: android.support.v4.f.ab */
public interface ab {
    int computeVerticalScrollExtent();

    int computeVerticalScrollOffset();

    int computeVerticalScrollRange();
}
