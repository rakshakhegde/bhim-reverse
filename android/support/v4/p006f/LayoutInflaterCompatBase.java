package android.support.v4.p006f;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.View;

/* renamed from: android.support.v4.f.j */
class LayoutInflaterCompatBase {

    /* renamed from: android.support.v4.f.j.a */
    static class LayoutInflaterCompatBase implements Factory {
        final LayoutInflaterFactory f561a;

        LayoutInflaterCompatBase(LayoutInflaterFactory layoutInflaterFactory) {
            this.f561a = layoutInflaterFactory;
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            return this.f561a.m467a(null, str, context, attributeSet);
        }

        public String toString() {
            return getClass().getName() + "{" + this.f561a + "}";
        }
    }

    static void m1440a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        layoutInflater.setFactory(layoutInflaterFactory != null ? new LayoutInflaterCompatBase(layoutInflaterFactory) : null);
    }

    static LayoutInflaterFactory m1439a(LayoutInflater layoutInflater) {
        Factory factory = layoutInflater.getFactory();
        if (factory instanceof LayoutInflaterCompatBase) {
            return ((LayoutInflaterCompatBase) factory).f561a;
        }
        return null;
    }
}
