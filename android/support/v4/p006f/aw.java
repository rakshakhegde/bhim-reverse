package android.support.v4.p006f;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* compiled from: ViewPropertyAnimatorCompatJB */
/* renamed from: android.support.v4.f.aw */
class aw {

    /* renamed from: android.support.v4.f.aw.1 */
    static class ViewPropertyAnimatorCompatJB extends AnimatorListenerAdapter {
        final /* synthetic */ ay f545a;
        final /* synthetic */ View f546b;

        ViewPropertyAnimatorCompatJB(ay ayVar, View view) {
            this.f545a = ayVar;
            this.f546b = view;
        }

        public void onAnimationCancel(Animator animator) {
            this.f545a.m1329c(this.f546b);
        }

        public void onAnimationEnd(Animator animator) {
            this.f545a.m1328b(this.f546b);
        }

        public void onAnimationStart(Animator animator) {
            this.f545a.m1327a(this.f546b);
        }
    }

    public static void m1375a(View view, ay ayVar) {
        if (ayVar != null) {
            view.animate().setListener(new ViewPropertyAnimatorCompatJB(ayVar, view));
        } else {
            view.animate().setListener(null);
        }
    }
}
