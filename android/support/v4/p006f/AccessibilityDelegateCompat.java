package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.p006f.AccessibilityDelegateCompatIcs.AccessibilityDelegateCompatIcs;
import android.support.v4.p006f.AccessibilityDelegateCompatJellyBean.AccessibilityDelegateCompatJellyBean;
import android.support.v4.p006f.p011a.AccessibilityNodeInfoCompat;
import android.support.v4.p006f.p011a.AccessibilityNodeProviderCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: android.support.v4.f.a */
public class AccessibilityDelegateCompat {
    private static final AccessibilityDelegateCompat f519b;
    private static final Object f520c;
    final Object f521a;

    /* renamed from: android.support.v4.f.a.b */
    interface AccessibilityDelegateCompat {
        AccessibilityNodeProviderCompat m825a(Object obj, View view);

        Object m826a();

        Object m827a(AccessibilityDelegateCompat accessibilityDelegateCompat);

        void m828a(Object obj, View view, int i);

        void m829a(Object obj, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

        boolean m830a(Object obj, View view, int i, Bundle bundle);

        boolean m831a(Object obj, View view, AccessibilityEvent accessibilityEvent);

        boolean m832a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent);

        void m833b(Object obj, View view, AccessibilityEvent accessibilityEvent);

        void m834c(Object obj, View view, AccessibilityEvent accessibilityEvent);

        void m835d(Object obj, View view, AccessibilityEvent accessibilityEvent);
    }

    /* renamed from: android.support.v4.f.a.d */
    static class AccessibilityDelegateCompat implements AccessibilityDelegateCompat {
        AccessibilityDelegateCompat() {
        }

        public Object m837a() {
            return null;
        }

        public Object m838a(AccessibilityDelegateCompat accessibilityDelegateCompat) {
            return null;
        }

        public boolean m842a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            return false;
        }

        public void m844b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public void m840a(Object obj, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        }

        public void m845c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public boolean m843a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return true;
        }

        public void m839a(Object obj, View view, int i) {
        }

        public void m846d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public AccessibilityNodeProviderCompat m836a(Object obj, View view) {
            return null;
        }

        public boolean m841a(Object obj, View view, int i, Bundle bundle) {
            return false;
        }
    }

    /* renamed from: android.support.v4.f.a.a */
    static class AccessibilityDelegateCompat extends AccessibilityDelegateCompat {

        /* renamed from: android.support.v4.f.a.a.1 */
        class AccessibilityDelegateCompat implements AccessibilityDelegateCompatIcs {
            final /* synthetic */ AccessibilityDelegateCompat f502a;
            final /* synthetic */ AccessibilityDelegateCompat f503b;

            AccessibilityDelegateCompat(AccessibilityDelegateCompat accessibilityDelegateCompat, AccessibilityDelegateCompat accessibilityDelegateCompat2) {
                this.f503b = accessibilityDelegateCompat;
                this.f502a = accessibilityDelegateCompat2;
            }

            public boolean m820a(View view, AccessibilityEvent accessibilityEvent) {
                return this.f502a.m1042b(view, accessibilityEvent);
            }

            public void m822b(View view, AccessibilityEvent accessibilityEvent) {
                this.f502a.m1044d(view, accessibilityEvent);
            }

            public void m819a(View view, Object obj) {
                this.f502a.m1038a(view, new AccessibilityNodeInfoCompat(obj));
            }

            public void m823c(View view, AccessibilityEvent accessibilityEvent) {
                this.f502a.m1043c(view, accessibilityEvent);
            }

            public boolean m821a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                return this.f502a.m1041a(viewGroup, view, accessibilityEvent);
            }

            public void m818a(View view, int i) {
                this.f502a.m1037a(view, i);
            }

            public void m824d(View view, AccessibilityEvent accessibilityEvent) {
                this.f502a.m1039a(view, accessibilityEvent);
            }
        }

        AccessibilityDelegateCompat() {
        }

        public Object m847a() {
            return AccessibilityDelegateCompatIcs.m1380a();
        }

        public Object m848a(AccessibilityDelegateCompat accessibilityDelegateCompat) {
            return AccessibilityDelegateCompatIcs.m1381a(new AccessibilityDelegateCompat(this, accessibilityDelegateCompat));
        }

        public boolean m851a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            return AccessibilityDelegateCompatIcs.m1384a(obj, view, accessibilityEvent);
        }

        public void m853b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompatIcs.m1386b(obj, view, accessibilityEvent);
        }

        public void m850a(Object obj, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            AccessibilityDelegateCompatIcs.m1383a(obj, view, accessibilityNodeInfoCompat.m942a());
        }

        public void m854c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompatIcs.m1387c(obj, view, accessibilityEvent);
        }

        public boolean m852a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return AccessibilityDelegateCompatIcs.m1385a(obj, viewGroup, view, accessibilityEvent);
        }

        public void m849a(Object obj, View view, int i) {
            AccessibilityDelegateCompatIcs.m1382a(obj, view, i);
        }

        public void m855d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompatIcs.m1388d(obj, view, accessibilityEvent);
        }
    }

    /* renamed from: android.support.v4.f.a.c */
    static class AccessibilityDelegateCompat extends AccessibilityDelegateCompat {

        /* renamed from: android.support.v4.f.a.c.1 */
        class AccessibilityDelegateCompat implements AccessibilityDelegateCompatJellyBean {
            final /* synthetic */ AccessibilityDelegateCompat f504a;
            final /* synthetic */ AccessibilityDelegateCompat f505b;

            AccessibilityDelegateCompat(AccessibilityDelegateCompat accessibilityDelegateCompat, AccessibilityDelegateCompat accessibilityDelegateCompat2) {
                this.f505b = accessibilityDelegateCompat;
                this.f504a = accessibilityDelegateCompat2;
            }

            public boolean m869a(View view, AccessibilityEvent accessibilityEvent) {
                return this.f504a.m1042b(view, accessibilityEvent);
            }

            public void m871b(View view, AccessibilityEvent accessibilityEvent) {
                this.f504a.m1044d(view, accessibilityEvent);
            }

            public void m867a(View view, Object obj) {
                this.f504a.m1038a(view, new AccessibilityNodeInfoCompat(obj));
            }

            public void m872c(View view, AccessibilityEvent accessibilityEvent) {
                this.f504a.m1043c(view, accessibilityEvent);
            }

            public boolean m870a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                return this.f504a.m1041a(viewGroup, view, accessibilityEvent);
            }

            public void m866a(View view, int i) {
                this.f504a.m1037a(view, i);
            }

            public void m873d(View view, AccessibilityEvent accessibilityEvent) {
                this.f504a.m1039a(view, accessibilityEvent);
            }

            public Object m865a(View view) {
                AccessibilityNodeProviderCompat a = this.f504a.m1035a(view);
                return a != null ? a.m1004a() : null;
            }

            public boolean m868a(View view, int i, Bundle bundle) {
                return this.f504a.m1040a(view, i, bundle);
            }
        }

        AccessibilityDelegateCompat() {
        }

        public Object m875a(AccessibilityDelegateCompat accessibilityDelegateCompat) {
            return AccessibilityDelegateCompatJellyBean.m1401a(new AccessibilityDelegateCompat(this, accessibilityDelegateCompat));
        }

        public AccessibilityNodeProviderCompat m874a(Object obj, View view) {
            Object a = AccessibilityDelegateCompatJellyBean.m1402a(obj, view);
            if (a != null) {
                return new AccessibilityNodeProviderCompat(a);
            }
            return null;
        }

        public boolean m876a(Object obj, View view, int i, Bundle bundle) {
            return AccessibilityDelegateCompatJellyBean.m1403a(obj, view, i, bundle);
        }
    }

    static {
        if (VERSION.SDK_INT >= 16) {
            f519b = new AccessibilityDelegateCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f519b = new AccessibilityDelegateCompat();
        } else {
            f519b = new AccessibilityDelegateCompat();
        }
        f520c = f519b.m826a();
    }

    public AccessibilityDelegateCompat() {
        this.f521a = f519b.m827a(this);
    }

    Object m1036a() {
        return this.f521a;
    }

    public void m1037a(View view, int i) {
        f519b.m828a(f520c, view, i);
    }

    public void m1039a(View view, AccessibilityEvent accessibilityEvent) {
        f519b.m835d(f520c, view, accessibilityEvent);
    }

    public boolean m1042b(View view, AccessibilityEvent accessibilityEvent) {
        return f519b.m831a(f520c, view, accessibilityEvent);
    }

    public void m1043c(View view, AccessibilityEvent accessibilityEvent) {
        f519b.m834c(f520c, view, accessibilityEvent);
    }

    public void m1044d(View view, AccessibilityEvent accessibilityEvent) {
        f519b.m833b(f520c, view, accessibilityEvent);
    }

    public void m1038a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        f519b.m829a(f520c, view, accessibilityNodeInfoCompat);
    }

    public boolean m1041a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return f519b.m832a(f520c, viewGroup, view, accessibilityEvent);
    }

    public AccessibilityNodeProviderCompat m1035a(View view) {
        return f519b.m825a(f520c, view);
    }

    public boolean m1040a(View view, int i, Bundle bundle) {
        return f519b.m830a(f520c, view, i, bundle);
    }
}
