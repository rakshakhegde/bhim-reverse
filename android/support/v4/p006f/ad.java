package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.VelocityTracker;

/* compiled from: VelocityTrackerCompat */
/* renamed from: android.support.v4.f.ad */
public final class ad {
    static final VelocityTrackerCompat f522a;

    /* renamed from: android.support.v4.f.ad.c */
    interface VelocityTrackerCompat {
        float m1046a(VelocityTracker velocityTracker, int i);
    }

    /* renamed from: android.support.v4.f.ad.a */
    static class VelocityTrackerCompat implements VelocityTrackerCompat {
        VelocityTrackerCompat() {
        }

        public float m1047a(VelocityTracker velocityTracker, int i) {
            return velocityTracker.getYVelocity();
        }
    }

    /* renamed from: android.support.v4.f.ad.b */
    static class VelocityTrackerCompat implements VelocityTrackerCompat {
        VelocityTrackerCompat() {
        }

        public float m1048a(VelocityTracker velocityTracker, int i) {
            return ae.m1050a(velocityTracker, i);
        }
    }

    static {
        if (VERSION.SDK_INT >= 11) {
            f522a = new VelocityTrackerCompat();
        } else {
            f522a = new VelocityTrackerCompat();
        }
    }

    public static float m1049a(VelocityTracker velocityTracker, int i) {
        return f522a.m1046a(velocityTracker, i);
    }
}
