package android.support.v4.p006f;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v4.f.z */
public class NestedScrollingParentHelper {
    private final ViewGroup f571a;
    private int f572b;

    public NestedScrollingParentHelper(ViewGroup viewGroup) {
        this.f571a = viewGroup;
    }

    public void m1532a(View view, View view2, int i) {
        this.f572b = i;
    }

    public int m1530a() {
        return this.f572b;
    }

    public void m1531a(View view) {
        this.f572b = 0;
    }
}
