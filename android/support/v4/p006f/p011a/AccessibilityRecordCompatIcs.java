package android.support.v4.p006f.p011a;

import android.view.accessibility.AccessibilityRecord;

/* renamed from: android.support.v4.f.a.i */
class AccessibilityRecordCompatIcs {
    public static void m1030a(Object obj, int i) {
        ((AccessibilityRecord) obj).setScrollX(i);
    }

    public static void m1032b(Object obj, int i) {
        ((AccessibilityRecord) obj).setScrollY(i);
    }

    public static void m1031a(Object obj, boolean z) {
        ((AccessibilityRecord) obj).setScrollable(z);
    }
}
