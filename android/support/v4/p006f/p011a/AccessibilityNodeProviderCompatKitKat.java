package android.support.v4.p006f.p011a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* renamed from: android.support.v4.f.a.g */
class AccessibilityNodeProviderCompatKitKat {

    /* renamed from: android.support.v4.f.a.g.a */
    interface AccessibilityNodeProviderCompatKitKat {
        Object m994a(int i);

        List<Object> m995a(String str, int i);

        boolean m996a(int i, int i2, Bundle bundle);

        Object m997b(int i);
    }

    /* renamed from: android.support.v4.f.a.g.1 */
    static class AccessibilityNodeProviderCompatKitKat extends AccessibilityNodeProvider {
        final /* synthetic */ AccessibilityNodeProviderCompatKitKat f516a;

        AccessibilityNodeProviderCompatKitKat(AccessibilityNodeProviderCompatKitKat accessibilityNodeProviderCompatKitKat) {
            this.f516a = accessibilityNodeProviderCompatKitKat;
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            return (AccessibilityNodeInfo) this.f516a.m994a(i);
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            return this.f516a.m995a(str, i);
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.f516a.m996a(i, i2, bundle);
        }

        public AccessibilityNodeInfo findFocus(int i) {
            return (AccessibilityNodeInfo) this.f516a.m997b(i);
        }
    }

    public static Object m1009a(AccessibilityNodeProviderCompatKitKat accessibilityNodeProviderCompatKitKat) {
        return new AccessibilityNodeProviderCompatKitKat(accessibilityNodeProviderCompatKitKat);
    }
}
