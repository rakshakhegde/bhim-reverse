package android.support.v4.p006f.p011a;

import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: android.support.v4.f.a.d */
class AccessibilityNodeInfoCompatJellybeanMr2 {
    public static String m984a(Object obj) {
        return ((AccessibilityNodeInfo) obj).getViewIdResourceName();
    }
}
