package android.support.v4.p006f.p011a;

import android.os.Build.VERSION;

/* renamed from: android.support.v4.f.a.h */
public class AccessibilityRecordCompat {
    private static final AccessibilityRecordCompat f517a;
    private final Object f518b;

    /* renamed from: android.support.v4.f.a.h.c */
    interface AccessibilityRecordCompat {
        void m1010a(Object obj, int i);

        void m1011a(Object obj, boolean z);

        void m1012b(Object obj, int i);

        void m1013c(Object obj, int i);

        void m1014d(Object obj, int i);
    }

    /* renamed from: android.support.v4.f.a.h.e */
    static class AccessibilityRecordCompat implements AccessibilityRecordCompat {
        AccessibilityRecordCompat() {
        }

        public void m1018c(Object obj, int i) {
        }

        public void m1019d(Object obj, int i) {
        }

        public void m1015a(Object obj, int i) {
        }

        public void m1017b(Object obj, int i) {
        }

        public void m1016a(Object obj, boolean z) {
        }
    }

    /* renamed from: android.support.v4.f.a.h.a */
    static class AccessibilityRecordCompat extends AccessibilityRecordCompat {
        AccessibilityRecordCompat() {
        }

        public void m1020a(Object obj, int i) {
            AccessibilityRecordCompatIcs.m1030a(obj, i);
        }

        public void m1022b(Object obj, int i) {
            AccessibilityRecordCompatIcs.m1032b(obj, i);
        }

        public void m1021a(Object obj, boolean z) {
            AccessibilityRecordCompatIcs.m1031a(obj, z);
        }
    }

    /* renamed from: android.support.v4.f.a.h.b */
    static class AccessibilityRecordCompat extends AccessibilityRecordCompat {
        AccessibilityRecordCompat() {
        }

        public void m1023c(Object obj, int i) {
            AccessibilityRecordCompatIcsMr1.m1033a(obj, i);
        }

        public void m1024d(Object obj, int i) {
            AccessibilityRecordCompatIcsMr1.m1034b(obj, i);
        }
    }

    /* renamed from: android.support.v4.f.a.h.d */
    static class AccessibilityRecordCompat extends AccessibilityRecordCompat {
        AccessibilityRecordCompat() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 16) {
            f517a = new AccessibilityRecordCompat();
        } else if (VERSION.SDK_INT >= 15) {
            f517a = new AccessibilityRecordCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f517a = new AccessibilityRecordCompat();
        } else {
            f517a = new AccessibilityRecordCompat();
        }
    }

    public AccessibilityRecordCompat(Object obj) {
        this.f518b = obj;
    }

    public void m1026a(boolean z) {
        f517a.m1011a(this.f518b, z);
    }

    public void m1025a(int i) {
        f517a.m1010a(this.f518b, i);
    }

    public void m1027b(int i) {
        f517a.m1012b(this.f518b, i);
    }

    public void m1028c(int i) {
        f517a.m1013c(this.f518b, i);
    }

    public void m1029d(int i) {
        f517a.m1014d(this.f518b, i);
    }

    public int hashCode() {
        return this.f518b == null ? 0 : this.f518b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AccessibilityRecordCompat accessibilityRecordCompat = (AccessibilityRecordCompat) obj;
        if (this.f518b == null) {
            if (accessibilityRecordCompat.f518b != null) {
                return false;
            }
            return true;
        } else if (this.f518b.equals(accessibilityRecordCompat.f518b)) {
            return true;
        } else {
            return false;
        }
    }
}
