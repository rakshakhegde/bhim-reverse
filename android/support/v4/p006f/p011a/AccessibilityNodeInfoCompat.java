package android.support.v4.p006f.p011a;

import android.graphics.Rect;
import android.os.Build.VERSION;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v4.f.a.b */
public class AccessibilityNodeInfoCompat {
    private static final AccessibilityNodeInfoCompat f507a;
    private final Object f508b;

    /* renamed from: android.support.v4.f.a.b.d */
    interface AccessibilityNodeInfoCompat {
        int m878a(Object obj);

        void m879a(Object obj, int i);

        void m880a(Object obj, Rect rect);

        void m881a(Object obj, CharSequence charSequence);

        void m882a(Object obj, boolean z);

        CharSequence m883b(Object obj);

        void m884b(Object obj, Rect rect);

        CharSequence m885c(Object obj);

        CharSequence m886d(Object obj);

        CharSequence m887e(Object obj);

        boolean m888f(Object obj);

        boolean m889g(Object obj);

        boolean m890h(Object obj);

        boolean m891i(Object obj);

        boolean m892j(Object obj);

        boolean m893k(Object obj);

        boolean m894l(Object obj);

        boolean m895m(Object obj);

        boolean m896n(Object obj);

        boolean m897o(Object obj);

        String m898p(Object obj);
    }

    /* renamed from: android.support.v4.f.a.b.i */
    static class AccessibilityNodeInfoCompat implements AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }

        public void m900a(Object obj, int i) {
        }

        public int m899a(Object obj) {
            return 0;
        }

        public void m901a(Object obj, Rect rect) {
        }

        public void m905b(Object obj, Rect rect) {
        }

        public CharSequence m904b(Object obj) {
            return null;
        }

        public CharSequence m906c(Object obj) {
            return null;
        }

        public CharSequence m907d(Object obj) {
            return null;
        }

        public CharSequence m908e(Object obj) {
            return null;
        }

        public boolean m909f(Object obj) {
            return false;
        }

        public boolean m910g(Object obj) {
            return false;
        }

        public boolean m911h(Object obj) {
            return false;
        }

        public boolean m912i(Object obj) {
            return false;
        }

        public boolean m913j(Object obj) {
            return false;
        }

        public boolean m914k(Object obj) {
            return false;
        }

        public boolean m915l(Object obj) {
            return false;
        }

        public boolean m916m(Object obj) {
            return false;
        }

        public boolean m917n(Object obj) {
            return false;
        }

        public boolean m918o(Object obj) {
            return false;
        }

        public void m902a(Object obj, CharSequence charSequence) {
        }

        public void m903a(Object obj, boolean z) {
        }

        public String m919p(Object obj) {
            return null;
        }
    }

    /* renamed from: android.support.v4.f.a.b.c */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }

        public void m921a(Object obj, int i) {
            AccessibilityNodeInfoCompatIcs.m965a(obj, i);
        }

        public int m920a(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m964a(obj);
        }

        public void m922a(Object obj, Rect rect) {
            AccessibilityNodeInfoCompatIcs.m966a(obj, rect);
        }

        public void m926b(Object obj, Rect rect) {
            AccessibilityNodeInfoCompatIcs.m970b(obj, rect);
        }

        public CharSequence m925b(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m969b(obj);
        }

        public CharSequence m927c(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m971c(obj);
        }

        public CharSequence m928d(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m972d(obj);
        }

        public CharSequence m929e(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m973e(obj);
        }

        public boolean m930f(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m974f(obj);
        }

        public boolean m931g(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m975g(obj);
        }

        public boolean m932h(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m976h(obj);
        }

        public boolean m933i(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m977i(obj);
        }

        public boolean m934j(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m978j(obj);
        }

        public boolean m935k(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m979k(obj);
        }

        public boolean m936l(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m980l(obj);
        }

        public boolean m937m(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m981m(obj);
        }

        public boolean m938n(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m982n(obj);
        }

        public boolean m939o(Object obj) {
            return AccessibilityNodeInfoCompatIcs.m983o(obj);
        }

        public void m923a(Object obj, CharSequence charSequence) {
            AccessibilityNodeInfoCompatIcs.m967a(obj, charSequence);
        }

        public void m924a(Object obj, boolean z) {
            AccessibilityNodeInfoCompatIcs.m968a(obj, z);
        }
    }

    /* renamed from: android.support.v4.f.a.b.e */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.b.f */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.b.g */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }

        public String m940p(Object obj) {
            return AccessibilityNodeInfoCompatJellybeanMr2.m984a(obj);
        }
    }

    /* renamed from: android.support.v4.f.a.b.h */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.b.a */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.b.b */
    static class AccessibilityNodeInfoCompat extends AccessibilityNodeInfoCompat {
        AccessibilityNodeInfoCompat() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 22) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 21) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 19) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 18) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 17) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 16) {
            f507a = new AccessibilityNodeInfoCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f507a = new AccessibilityNodeInfoCompat();
        } else {
            f507a = new AccessibilityNodeInfoCompat();
        }
    }

    public AccessibilityNodeInfoCompat(Object obj) {
        this.f508b = obj;
    }

    public Object m942a() {
        return this.f508b;
    }

    public int m947b() {
        return f507a.m878a(this.f508b);
    }

    public void m943a(int i) {
        f507a.m879a(this.f508b, i);
    }

    public void m944a(Rect rect) {
        f507a.m880a(this.f508b, rect);
    }

    public void m948b(Rect rect) {
        f507a.m884b(this.f508b, rect);
    }

    public boolean m949c() {
        return f507a.m888f(this.f508b);
    }

    public boolean m950d() {
        return f507a.m889g(this.f508b);
    }

    public boolean m951e() {
        return f507a.m892j(this.f508b);
    }

    public boolean m952f() {
        return f507a.m893k(this.f508b);
    }

    public boolean m953g() {
        return f507a.m897o(this.f508b);
    }

    public boolean m954h() {
        return f507a.m890h(this.f508b);
    }

    public boolean m955i() {
        return f507a.m894l(this.f508b);
    }

    public boolean m956j() {
        return f507a.m891i(this.f508b);
    }

    public boolean m957k() {
        return f507a.m895m(this.f508b);
    }

    public boolean m958l() {
        return f507a.m896n(this.f508b);
    }

    public void m946a(boolean z) {
        f507a.m882a(this.f508b, z);
    }

    public CharSequence m959m() {
        return f507a.m886d(this.f508b);
    }

    public CharSequence m960n() {
        return f507a.m883b(this.f508b);
    }

    public void m945a(CharSequence charSequence) {
        f507a.m881a(this.f508b, charSequence);
    }

    public CharSequence m961o() {
        return f507a.m887e(this.f508b);
    }

    public CharSequence m962p() {
        return f507a.m885c(this.f508b);
    }

    public String m963q() {
        return f507a.m898p(this.f508b);
    }

    public int hashCode() {
        return this.f508b == null ? 0 : this.f508b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = (AccessibilityNodeInfoCompat) obj;
        if (this.f508b == null) {
            if (accessibilityNodeInfoCompat.f508b != null) {
                return false;
            }
            return true;
        } else if (this.f508b.equals(accessibilityNodeInfoCompat.f508b)) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(super.toString());
        Rect rect = new Rect();
        m944a(rect);
        stringBuilder.append("; boundsInParent: " + rect);
        m948b(rect);
        stringBuilder.append("; boundsInScreen: " + rect);
        stringBuilder.append("; packageName: ").append(m959m());
        stringBuilder.append("; className: ").append(m960n());
        stringBuilder.append("; text: ").append(m961o());
        stringBuilder.append("; contentDescription: ").append(m962p());
        stringBuilder.append("; viewId: ").append(m963q());
        stringBuilder.append("; checkable: ").append(m949c());
        stringBuilder.append("; checked: ").append(m950d());
        stringBuilder.append("; focusable: ").append(m951e());
        stringBuilder.append("; focused: ").append(m952f());
        stringBuilder.append("; selected: ").append(m953g());
        stringBuilder.append("; clickable: ").append(m954h());
        stringBuilder.append("; longClickable: ").append(m955i());
        stringBuilder.append("; enabled: ").append(m956j());
        stringBuilder.append("; password: ").append(m957k());
        stringBuilder.append("; scrollable: " + m958l());
        stringBuilder.append("; [");
        int b = m947b();
        while (b != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(b);
            b &= numberOfTrailingZeros ^ -1;
            stringBuilder.append(AccessibilityNodeInfoCompat.m941b(numberOfTrailingZeros));
            if (b != 0) {
                stringBuilder.append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    private static String m941b(int i) {
        switch (i) {
            case R.View_android_focusable /*1*/:
                return "ACTION_FOCUS";
            case R.View_paddingStart /*2*/:
                return "ACTION_CLEAR_FOCUS";
            case R.View_theme /*4*/:
                return "ACTION_SELECT";
            case R.Toolbar_contentInsetRight /*8*/:
                return "ACTION_CLEAR_SELECTION";
            case R.Toolbar_titleMarginBottom /*16*/:
                return "ACTION_CLICK";
            case R.AppCompatTheme_actionModeCutDrawable /*32*/:
                return "ACTION_LONG_CLICK";
            case R.AppCompatTheme_imageButtonStyle /*64*/:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }
}
