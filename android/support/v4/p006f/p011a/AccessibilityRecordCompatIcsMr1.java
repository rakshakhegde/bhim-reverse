package android.support.v4.p006f.p011a;

import android.view.accessibility.AccessibilityRecord;

/* renamed from: android.support.v4.f.a.j */
class AccessibilityRecordCompatIcsMr1 {
    public static void m1033a(Object obj, int i) {
        ((AccessibilityRecord) obj).setMaxScrollX(i);
    }

    public static void m1034b(Object obj, int i) {
        ((AccessibilityRecord) obj).setMaxScrollY(i);
    }
}
