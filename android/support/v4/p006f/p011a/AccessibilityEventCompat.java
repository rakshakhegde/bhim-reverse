package android.support.v4.p006f.p011a;

import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: android.support.v4.f.a.a */
public final class AccessibilityEventCompat {
    private static final AccessibilityEventCompat f506a;

    /* renamed from: android.support.v4.f.a.a.d */
    interface AccessibilityEventCompat {
    }

    /* renamed from: android.support.v4.f.a.a.c */
    static class AccessibilityEventCompat implements AccessibilityEventCompat {
        AccessibilityEventCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.a.a */
    static class AccessibilityEventCompat extends AccessibilityEventCompat {
        AccessibilityEventCompat() {
        }
    }

    /* renamed from: android.support.v4.f.a.a.b */
    static class AccessibilityEventCompat extends AccessibilityEventCompat {
        AccessibilityEventCompat() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 19) {
            f506a = new AccessibilityEventCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f506a = new AccessibilityEventCompat();
        } else {
            f506a = new AccessibilityEventCompat();
        }
    }

    public static AccessibilityRecordCompat m877a(AccessibilityEvent accessibilityEvent) {
        return new AccessibilityRecordCompat(accessibilityEvent);
    }
}
