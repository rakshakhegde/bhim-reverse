package android.support.v4.p006f.p011a;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.p006f.p011a.AccessibilityNodeProviderCompatJellyBean.AccessibilityNodeProviderCompatJellyBean;
import android.support.v4.p006f.p011a.AccessibilityNodeProviderCompatKitKat.AccessibilityNodeProviderCompatKitKat;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.f.a.e */
public class AccessibilityNodeProviderCompat {
    private static final AccessibilityNodeProviderCompat f513a;
    private final Object f514b;

    /* renamed from: android.support.v4.f.a.e.a */
    interface AccessibilityNodeProviderCompat {
        Object m985a(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat);
    }

    /* renamed from: android.support.v4.f.a.e.d */
    static class AccessibilityNodeProviderCompat implements AccessibilityNodeProviderCompat {
        AccessibilityNodeProviderCompat() {
        }

        public Object m992a(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            return null;
        }
    }

    /* renamed from: android.support.v4.f.a.e.b */
    static class AccessibilityNodeProviderCompat extends AccessibilityNodeProviderCompat {

        /* renamed from: android.support.v4.f.a.e.b.1 */
        class AccessibilityNodeProviderCompat implements AccessibilityNodeProviderCompatJellyBean {
            final /* synthetic */ AccessibilityNodeProviderCompat f509a;
            final /* synthetic */ AccessibilityNodeProviderCompat f510b;

            AccessibilityNodeProviderCompat(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat, AccessibilityNodeProviderCompat accessibilityNodeProviderCompat2) {
                this.f510b = accessibilityNodeProviderCompat;
                this.f509a = accessibilityNodeProviderCompat2;
            }

            public boolean m991a(int i, int i2, Bundle bundle) {
                return this.f509a.m1006a(i, i2, bundle);
            }

            public List<Object> m990a(String str, int i) {
                List a = this.f509a.m1005a(str, i);
                List<Object> arrayList = new ArrayList();
                int size = a.size();
                for (int i2 = 0; i2 < size; i2++) {
                    arrayList.add(((AccessibilityNodeInfoCompat) a.get(i2)).m942a());
                }
                return arrayList;
            }

            public Object m989a(int i) {
                AccessibilityNodeInfoCompat a = this.f509a.m1003a(i);
                if (a == null) {
                    return null;
                }
                return a.m942a();
            }
        }

        AccessibilityNodeProviderCompat() {
        }

        public Object m993a(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            return AccessibilityNodeProviderCompatJellyBean.m1008a(new AccessibilityNodeProviderCompat(this, accessibilityNodeProviderCompat));
        }
    }

    /* renamed from: android.support.v4.f.a.e.c */
    static class AccessibilityNodeProviderCompat extends AccessibilityNodeProviderCompat {

        /* renamed from: android.support.v4.f.a.e.c.1 */
        class AccessibilityNodeProviderCompat implements AccessibilityNodeProviderCompatKitKat {
            final /* synthetic */ AccessibilityNodeProviderCompat f511a;
            final /* synthetic */ AccessibilityNodeProviderCompat f512b;

            AccessibilityNodeProviderCompat(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat, AccessibilityNodeProviderCompat accessibilityNodeProviderCompat2) {
                this.f512b = accessibilityNodeProviderCompat;
                this.f511a = accessibilityNodeProviderCompat2;
            }

            public boolean m1000a(int i, int i2, Bundle bundle) {
                return this.f511a.m1006a(i, i2, bundle);
            }

            public List<Object> m999a(String str, int i) {
                List a = this.f511a.m1005a(str, i);
                List<Object> arrayList = new ArrayList();
                int size = a.size();
                for (int i2 = 0; i2 < size; i2++) {
                    arrayList.add(((AccessibilityNodeInfoCompat) a.get(i2)).m942a());
                }
                return arrayList;
            }

            public Object m998a(int i) {
                AccessibilityNodeInfoCompat a = this.f511a.m1003a(i);
                if (a == null) {
                    return null;
                }
                return a.m942a();
            }

            public Object m1001b(int i) {
                AccessibilityNodeInfoCompat b = this.f511a.m1007b(i);
                if (b == null) {
                    return null;
                }
                return b.m942a();
            }
        }

        AccessibilityNodeProviderCompat() {
        }

        public Object m1002a(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            return AccessibilityNodeProviderCompatKitKat.m1009a(new AccessibilityNodeProviderCompat(this, accessibilityNodeProviderCompat));
        }
    }

    static {
        if (VERSION.SDK_INT >= 19) {
            f513a = new AccessibilityNodeProviderCompat();
        } else if (VERSION.SDK_INT >= 16) {
            f513a = new AccessibilityNodeProviderCompat();
        } else {
            f513a = new AccessibilityNodeProviderCompat();
        }
    }

    public AccessibilityNodeProviderCompat() {
        this.f514b = f513a.m985a(this);
    }

    public AccessibilityNodeProviderCompat(Object obj) {
        this.f514b = obj;
    }

    public Object m1004a() {
        return this.f514b;
    }

    public AccessibilityNodeInfoCompat m1003a(int i) {
        return null;
    }

    public boolean m1006a(int i, int i2, Bundle bundle) {
        return false;
    }

    public List<AccessibilityNodeInfoCompat> m1005a(String str, int i) {
        return null;
    }

    public AccessibilityNodeInfoCompat m1007b(int i) {
        return null;
    }
}
