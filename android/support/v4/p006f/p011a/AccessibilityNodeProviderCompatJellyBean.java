package android.support.v4.p006f.p011a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* renamed from: android.support.v4.f.a.f */
class AccessibilityNodeProviderCompatJellyBean {

    /* renamed from: android.support.v4.f.a.f.a */
    interface AccessibilityNodeProviderCompatJellyBean {
        Object m986a(int i);

        List<Object> m987a(String str, int i);

        boolean m988a(int i, int i2, Bundle bundle);
    }

    /* renamed from: android.support.v4.f.a.f.1 */
    static class AccessibilityNodeProviderCompatJellyBean extends AccessibilityNodeProvider {
        final /* synthetic */ AccessibilityNodeProviderCompatJellyBean f515a;

        AccessibilityNodeProviderCompatJellyBean(AccessibilityNodeProviderCompatJellyBean accessibilityNodeProviderCompatJellyBean) {
            this.f515a = accessibilityNodeProviderCompatJellyBean;
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            return (AccessibilityNodeInfo) this.f515a.m986a(i);
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            return this.f515a.m987a(str, i);
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.f515a.m988a(i, i2, bundle);
        }
    }

    public static Object m1008a(AccessibilityNodeProviderCompatJellyBean accessibilityNodeProviderCompatJellyBean) {
        return new AccessibilityNodeProviderCompatJellyBean(accessibilityNodeProviderCompatJellyBean);
    }
}
