package android.support.v4.p006f;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;

/* compiled from: ViewPropertyAnimatorCompatKK */
/* renamed from: android.support.v4.f.ax */
class ax {

    /* renamed from: android.support.v4.f.ax.1 */
    static class ViewPropertyAnimatorCompatKK implements AnimatorUpdateListener {
        final /* synthetic */ ba f547a;
        final /* synthetic */ View f548b;

        ViewPropertyAnimatorCompatKK(ba baVar, View view) {
            this.f547a = baVar;
            this.f548b = view;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.f547a.m1389a(this.f548b);
        }
    }

    public static void m1376a(View view, ba baVar) {
        AnimatorUpdateListener animatorUpdateListener = null;
        if (baVar != null) {
            animatorUpdateListener = new ViewPropertyAnimatorCompatKK(baVar, view);
        }
        view.animate().setUpdateListener(animatorUpdateListener);
    }
}
