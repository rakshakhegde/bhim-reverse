package android.support.v4.p006f;

import android.view.View;

/* compiled from: ViewCompatJB */
/* renamed from: android.support.v4.f.al */
class al {
    public static void m1236a(View view) {
        view.postInvalidateOnAnimation();
    }

    public static void m1237a(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }

    public static void m1238a(View view, Runnable runnable, long j) {
        view.postOnAnimationDelayed(runnable, j);
    }

    public static int m1239b(View view) {
        return view.getMinimumHeight();
    }

    public static void m1240c(View view) {
        view.requestFitSystemWindows();
    }

    public static boolean m1241d(View view) {
        return view.hasOverlappingRendering();
    }
}
