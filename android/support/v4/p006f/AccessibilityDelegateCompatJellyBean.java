package android.support.v4.p006f;

import android.os.Bundle;
import android.view.View;
import android.view.View.AccessibilityDelegate;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;

/* renamed from: android.support.v4.f.c */
class AccessibilityDelegateCompatJellyBean {

    /* renamed from: android.support.v4.f.c.a */
    public interface AccessibilityDelegateCompatJellyBean {
        Object m856a(View view);

        void m857a(View view, int i);

        void m858a(View view, Object obj);

        boolean m859a(View view, int i, Bundle bundle);

        boolean m860a(View view, AccessibilityEvent accessibilityEvent);

        boolean m861a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent);

        void m862b(View view, AccessibilityEvent accessibilityEvent);

        void m863c(View view, AccessibilityEvent accessibilityEvent);

        void m864d(View view, AccessibilityEvent accessibilityEvent);
    }

    /* renamed from: android.support.v4.f.c.1 */
    static class AccessibilityDelegateCompatJellyBean extends AccessibilityDelegate {
        final /* synthetic */ AccessibilityDelegateCompatJellyBean f554a;

        AccessibilityDelegateCompatJellyBean(AccessibilityDelegateCompatJellyBean accessibilityDelegateCompatJellyBean) {
            this.f554a = accessibilityDelegateCompatJellyBean;
        }

        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.f554a.m860a(view, accessibilityEvent);
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f554a.m862b(view, accessibilityEvent);
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            this.f554a.m858a(view, (Object) accessibilityNodeInfo);
        }

        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.f554a.m863c(view, accessibilityEvent);
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.f554a.m861a(viewGroup, view, accessibilityEvent);
        }

        public void sendAccessibilityEvent(View view, int i) {
            this.f554a.m857a(view, i);
        }

        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.f554a.m864d(view, accessibilityEvent);
        }

        public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
            return (AccessibilityNodeProvider) this.f554a.m856a(view);
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            return this.f554a.m859a(view, i, bundle);
        }
    }

    public static Object m1401a(AccessibilityDelegateCompatJellyBean accessibilityDelegateCompatJellyBean) {
        return new AccessibilityDelegateCompatJellyBean(accessibilityDelegateCompatJellyBean);
    }

    public static Object m1402a(Object obj, View view) {
        return ((AccessibilityDelegate) obj).getAccessibilityNodeProvider(view);
    }

    public static boolean m1403a(Object obj, View view, int i, Bundle bundle) {
        return ((AccessibilityDelegate) obj).performAccessibilityAction(view, i, bundle);
    }
}
