package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.LayoutInflater;

/* renamed from: android.support.v4.f.i */
public final class LayoutInflaterCompat {
    static final LayoutInflaterCompat f560a;

    /* renamed from: android.support.v4.f.i.a */
    interface LayoutInflaterCompat {
        LayoutInflaterFactory m1431a(LayoutInflater layoutInflater);

        void m1432a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory);
    }

    /* renamed from: android.support.v4.f.i.b */
    static class LayoutInflaterCompat implements LayoutInflaterCompat {
        LayoutInflaterCompat() {
        }

        public void m1434a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
            LayoutInflaterCompatBase.m1440a(layoutInflater, layoutInflaterFactory);
        }

        public LayoutInflaterFactory m1433a(LayoutInflater layoutInflater) {
            return LayoutInflaterCompatBase.m1439a(layoutInflater);
        }
    }

    /* renamed from: android.support.v4.f.i.c */
    static class LayoutInflaterCompat extends LayoutInflaterCompat {
        LayoutInflaterCompat() {
        }

        public void m1435a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
            LayoutInflaterCompatHC.m1441a(layoutInflater, layoutInflaterFactory);
        }
    }

    /* renamed from: android.support.v4.f.i.d */
    static class LayoutInflaterCompat extends LayoutInflaterCompat {
        LayoutInflaterCompat() {
        }

        public void m1436a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
            LayoutInflaterCompatLollipop.m1443a(layoutInflater, layoutInflaterFactory);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 21) {
            f560a = new LayoutInflaterCompat();
        } else if (i >= 11) {
            f560a = new LayoutInflaterCompat();
        } else {
            f560a = new LayoutInflaterCompat();
        }
    }

    public static void m1438a(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        f560a.m1432a(layoutInflater, layoutInflaterFactory);
    }

    public static LayoutInflaterFactory m1437a(LayoutInflater layoutInflater) {
        return f560a.m1431a(layoutInflater);
    }
}
