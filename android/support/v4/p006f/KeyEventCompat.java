package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.KeyEvent;

/* renamed from: android.support.v4.f.g */
public final class KeyEventCompat {
    static final KeyEventCompat f559a;

    /* renamed from: android.support.v4.f.g.d */
    interface KeyEventCompat {
        boolean m1422a(int i, int i2);
    }

    /* renamed from: android.support.v4.f.g.a */
    static class KeyEventCompat implements KeyEventCompat {
        KeyEventCompat() {
        }

        private static int m1423a(int i, int i2, int i3, int i4, int i5) {
            Object obj = 1;
            Object obj2 = (i2 & i3) != 0 ? 1 : null;
            int i6 = i4 | i5;
            if ((i2 & i6) == 0) {
                obj = null;
            }
            if (obj2 != null) {
                if (obj == null) {
                    return i & (i6 ^ -1);
                }
                throw new IllegalArgumentException("bad arguments");
            } else if (obj != null) {
                return i & (i3 ^ -1);
            } else {
                return i;
            }
        }

        public int m1424a(int i) {
            int i2;
            if ((i & 192) != 0) {
                i2 = i | 1;
            } else {
                i2 = i;
            }
            if ((i2 & 48) != 0) {
                i2 |= 2;
            }
            return i2 & 247;
        }

        public boolean m1425a(int i, int i2) {
            if (KeyEventCompat.m1423a(KeyEventCompat.m1423a(m1424a(i) & 247, i2, 1, 64, 128), i2, 2, 16, 32) == i2) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.f.g.b */
    static class KeyEventCompat extends KeyEventCompat {
        KeyEventCompat() {
        }
    }

    /* renamed from: android.support.v4.f.g.c */
    static class KeyEventCompat extends KeyEventCompat {
        KeyEventCompat() {
        }

        public int m1426a(int i) {
            return KeyEventCompatHoneycomb.m1429a(i);
        }

        public boolean m1427a(int i, int i2) {
            return KeyEventCompatHoneycomb.m1430a(i, i2);
        }
    }

    static {
        if (VERSION.SDK_INT >= 11) {
            f559a = new KeyEventCompat();
        } else {
            f559a = new KeyEventCompat();
        }
    }

    public static boolean m1428a(KeyEvent keyEvent, int i) {
        return f559a.m1422a(keyEvent.getMetaState(), i);
    }
}
