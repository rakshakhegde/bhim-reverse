package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.MotionEvent;

/* renamed from: android.support.v4.f.s */
public final class MotionEventCompat {
    static final MotionEventCompat f566a;

    /* renamed from: android.support.v4.f.s.e */
    interface MotionEventCompat {
        int m1489a(MotionEvent motionEvent);

        int m1490a(MotionEvent motionEvent, int i);

        int m1491b(MotionEvent motionEvent, int i);

        float m1492c(MotionEvent motionEvent, int i);

        float m1493d(MotionEvent motionEvent, int i);

        float m1494e(MotionEvent motionEvent, int i);
    }

    /* renamed from: android.support.v4.f.s.a */
    static class MotionEventCompat implements MotionEventCompat {
        MotionEventCompat() {
        }

        public int m1496a(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return 0;
            }
            return -1;
        }

        public int m1497b(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return 0;
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }

        public float m1498c(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return motionEvent.getX();
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }

        public float m1499d(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return motionEvent.getY();
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }

        public int m1495a(MotionEvent motionEvent) {
            return 0;
        }

        public float m1500e(MotionEvent motionEvent, int i) {
            return 0.0f;
        }
    }

    /* renamed from: android.support.v4.f.s.b */
    static class MotionEventCompat extends MotionEventCompat {
        MotionEventCompat() {
        }

        public int m1501a(MotionEvent motionEvent, int i) {
            return MotionEventCompatEclair.m1515a(motionEvent, i);
        }

        public int m1502b(MotionEvent motionEvent, int i) {
            return MotionEventCompatEclair.m1516b(motionEvent, i);
        }

        public float m1503c(MotionEvent motionEvent, int i) {
            return MotionEventCompatEclair.m1517c(motionEvent, i);
        }

        public float m1504d(MotionEvent motionEvent, int i) {
            return MotionEventCompatEclair.m1518d(motionEvent, i);
        }
    }

    /* renamed from: android.support.v4.f.s.c */
    static class MotionEventCompat extends MotionEventCompat {
        MotionEventCompat() {
        }

        public int m1505a(MotionEvent motionEvent) {
            return MotionEventCompatGingerbread.m1519a(motionEvent);
        }
    }

    /* renamed from: android.support.v4.f.s.d */
    static class MotionEventCompat extends MotionEventCompat {
        MotionEventCompat() {
        }

        public float m1506e(MotionEvent motionEvent, int i) {
            return MotionEventCompatHoneycombMr1.m1520a(motionEvent, i);
        }
    }

    static {
        if (VERSION.SDK_INT >= 12) {
            f566a = new MotionEventCompat();
        } else if (VERSION.SDK_INT >= 9) {
            f566a = new MotionEventCompat();
        } else if (VERSION.SDK_INT >= 5) {
            f566a = new MotionEventCompat();
        } else {
            f566a = new MotionEventCompat();
        }
    }

    public static int m1507a(MotionEvent motionEvent) {
        return motionEvent.getAction() & 255;
    }

    public static int m1509b(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int m1508a(MotionEvent motionEvent, int i) {
        return f566a.m1490a(motionEvent, i);
    }

    public static int m1510b(MotionEvent motionEvent, int i) {
        return f566a.m1491b(motionEvent, i);
    }

    public static float m1511c(MotionEvent motionEvent, int i) {
        return f566a.m1492c(motionEvent, i);
    }

    public static float m1513d(MotionEvent motionEvent, int i) {
        return f566a.m1493d(motionEvent, i);
    }

    public static int m1512c(MotionEvent motionEvent) {
        return f566a.m1489a(motionEvent);
    }

    public static float m1514e(MotionEvent motionEvent, int i) {
        return f566a.m1494e(motionEvent, i);
    }
}
