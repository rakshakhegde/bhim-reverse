package android.support.v4.p006f;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.view.View;
import java.lang.reflect.Field;

/* compiled from: ViewCompatBase */
/* renamed from: android.support.v4.f.ag */
class ag {
    private static Field f526a;
    private static boolean f527b;

    static ColorStateList m1211a(View view) {
        return view instanceof ac ? ((ac) view).getSupportBackgroundTintList() : null;
    }

    static void m1212a(View view, ColorStateList colorStateList) {
        if (view instanceof ac) {
            ((ac) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    static Mode m1214b(View view) {
        return view instanceof ac ? ((ac) view).getSupportBackgroundTintMode() : null;
    }

    static void m1213a(View view, Mode mode) {
        if (view instanceof ac) {
            ((ac) view).setSupportBackgroundTintMode(mode);
        }
    }

    static boolean m1215c(View view) {
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    static int m1216d(View view) {
        if (!f527b) {
            try {
                f526a = View.class.getDeclaredField("mMinHeight");
                f526a.setAccessible(true);
            } catch (NoSuchFieldException e) {
            }
            f527b = true;
        }
        if (f526a != null) {
            try {
                return ((Integer) f526a.get(view)).intValue();
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    static boolean m1217e(View view) {
        return view.getWindowToken() != null;
    }
}
