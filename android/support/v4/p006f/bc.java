package android.support.v4.p006f;

import android.view.WindowInsets;

/* compiled from: WindowInsetsCompatApi21 */
/* renamed from: android.support.v4.f.bc */
class bc extends bb {
    private final WindowInsets f553a;

    bc(WindowInsets windowInsets) {
        this.f553a = windowInsets;
    }

    public int m1395a() {
        return this.f553a.getSystemWindowInsetLeft();
    }

    public int m1397b() {
        return this.f553a.getSystemWindowInsetTop();
    }

    public int m1398c() {
        return this.f553a.getSystemWindowInsetRight();
    }

    public int m1399d() {
        return this.f553a.getSystemWindowInsetBottom();
    }

    public bb m1396a(int i, int i2, int i3, int i4) {
        return new bc(this.f553a.replaceSystemWindowInsets(i, i2, i3, i4));
    }

    WindowInsets m1400e() {
        return this.f553a;
    }
}
