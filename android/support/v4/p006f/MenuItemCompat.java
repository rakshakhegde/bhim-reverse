package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.support.v4.p007c.p008a.SupportMenuItem;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v4.f.p */
public final class MenuItemCompat {
    static final MenuItemCompat f565a;

    /* renamed from: android.support.v4.f.p.d */
    interface MenuItemCompat {
        MenuItem m1454a(MenuItem menuItem, View view);

        View m1455a(MenuItem menuItem);

        void m1456a(MenuItem menuItem, int i);

        MenuItem m1457b(MenuItem menuItem, int i);

        boolean m1458b(MenuItem menuItem);

        boolean m1459c(MenuItem menuItem);
    }

    /* renamed from: android.support.v4.f.p.a */
    static class MenuItemCompat implements MenuItemCompat {
        MenuItemCompat() {
        }

        public void m1462a(MenuItem menuItem, int i) {
        }

        public MenuItem m1460a(MenuItem menuItem, View view) {
            return menuItem;
        }

        public MenuItem m1463b(MenuItem menuItem, int i) {
            return menuItem;
        }

        public View m1461a(MenuItem menuItem) {
            return null;
        }

        public boolean m1464b(MenuItem menuItem) {
            return false;
        }

        public boolean m1465c(MenuItem menuItem) {
            return false;
        }
    }

    /* renamed from: android.support.v4.f.p.b */
    static class MenuItemCompat implements MenuItemCompat {
        MenuItemCompat() {
        }

        public void m1468a(MenuItem menuItem, int i) {
            MenuItemCompatHoneycomb.m1485a(menuItem, i);
        }

        public MenuItem m1466a(MenuItem menuItem, View view) {
            return MenuItemCompatHoneycomb.m1483a(menuItem, view);
        }

        public MenuItem m1469b(MenuItem menuItem, int i) {
            return MenuItemCompatHoneycomb.m1486b(menuItem, i);
        }

        public View m1467a(MenuItem menuItem) {
            return MenuItemCompatHoneycomb.m1484a(menuItem);
        }

        public boolean m1470b(MenuItem menuItem) {
            return false;
        }

        public boolean m1471c(MenuItem menuItem) {
            return false;
        }
    }

    /* renamed from: android.support.v4.f.p.c */
    static class MenuItemCompat extends MenuItemCompat {
        MenuItemCompat() {
        }

        public boolean m1472b(MenuItem menuItem) {
            return MenuItemCompatIcs.m1487a(menuItem);
        }

        public boolean m1473c(MenuItem menuItem) {
            return MenuItemCompatIcs.m1488b(menuItem);
        }
    }

    /* renamed from: android.support.v4.f.p.e */
    public interface MenuItemCompat {
        boolean m1474a(MenuItem menuItem);

        boolean m1475b(MenuItem menuItem);
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 14) {
            f565a = new MenuItemCompat();
        } else if (i >= 11) {
            f565a = new MenuItemCompat();
        } else {
            f565a = new MenuItemCompat();
        }
    }

    public static void m1479a(MenuItem menuItem, int i) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem) menuItem).setShowAsAction(i);
        } else {
            f565a.m1456a(menuItem, i);
        }
    }

    public static MenuItem m1477a(MenuItem menuItem, View view) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).setActionView(view);
        }
        return f565a.m1454a(menuItem, view);
    }

    public static MenuItem m1480b(MenuItem menuItem, int i) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).setActionView(i);
        }
        return f565a.m1457b(menuItem, i);
    }

    public static View m1478a(MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).getActionView();
        }
        return f565a.m1455a(menuItem);
    }

    public static MenuItem m1476a(MenuItem menuItem, ActionProvider actionProvider) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).m714a(actionProvider);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    public static boolean m1481b(MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).expandActionView();
        }
        return f565a.m1458b(menuItem);
    }

    public static boolean m1482c(MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem) menuItem).isActionViewExpanded();
        }
        return f565a.m1459c(menuItem);
    }
}
