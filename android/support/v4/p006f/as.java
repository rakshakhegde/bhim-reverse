package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewParent;

/* compiled from: ViewParentCompat */
/* renamed from: android.support.v4.f.as */
public final class as {
    static final ViewParentCompat f530a;

    /* renamed from: android.support.v4.f.as.b */
    interface ViewParentCompat {
        void m1264a(ViewParent viewParent, View view);

        void m1265a(ViewParent viewParent, View view, int i, int i2, int i3, int i4);

        void m1266a(ViewParent viewParent, View view, int i, int i2, int[] iArr);

        boolean m1267a(ViewParent viewParent, View view, float f, float f2);

        boolean m1268a(ViewParent viewParent, View view, float f, float f2, boolean z);

        boolean m1269a(ViewParent viewParent, View view, View view2, int i);

        void m1270b(ViewParent viewParent, View view, View view2, int i);
    }

    /* renamed from: android.support.v4.f.as.e */
    static class ViewParentCompat implements ViewParentCompat {
        ViewParentCompat() {
        }

        public boolean m1276a(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof NestedScrollingParent) {
                return ((NestedScrollingParent) viewParent).onStartNestedScroll(view, view2, i);
            }
            return false;
        }

        public void m1277b(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof NestedScrollingParent) {
                ((NestedScrollingParent) viewParent).onNestedScrollAccepted(view, view2, i);
            }
        }

        public void m1271a(ViewParent viewParent, View view) {
            if (viewParent instanceof NestedScrollingParent) {
                ((NestedScrollingParent) viewParent).onStopNestedScroll(view);
            }
        }

        public void m1272a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            if (viewParent instanceof NestedScrollingParent) {
                ((NestedScrollingParent) viewParent).onNestedScroll(view, i, i2, i3, i4);
            }
        }

        public void m1273a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            if (viewParent instanceof NestedScrollingParent) {
                ((NestedScrollingParent) viewParent).onNestedPreScroll(view, i, i2, iArr);
            }
        }

        public boolean m1275a(ViewParent viewParent, View view, float f, float f2, boolean z) {
            if (viewParent instanceof NestedScrollingParent) {
                return ((NestedScrollingParent) viewParent).onNestedFling(view, f, f2, z);
            }
            return false;
        }

        public boolean m1274a(ViewParent viewParent, View view, float f, float f2) {
            if (viewParent instanceof NestedScrollingParent) {
                return ((NestedScrollingParent) viewParent).onNestedPreFling(view, f, f2);
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.f.as.a */
    static class ViewParentCompat extends ViewParentCompat {
        ViewParentCompat() {
        }
    }

    /* renamed from: android.support.v4.f.as.c */
    static class ViewParentCompat extends ViewParentCompat {
        ViewParentCompat() {
        }
    }

    /* renamed from: android.support.v4.f.as.d */
    static class ViewParentCompat extends ViewParentCompat {
        ViewParentCompat() {
        }

        public boolean m1283a(ViewParent viewParent, View view, View view2, int i) {
            return at.m1297a(viewParent, view, view2, i);
        }

        public void m1284b(ViewParent viewParent, View view, View view2, int i) {
            at.m1298b(viewParent, view, view2, i);
        }

        public void m1278a(ViewParent viewParent, View view) {
            at.m1292a(viewParent, view);
        }

        public void m1279a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            at.m1293a(viewParent, view, i, i2, i3, i4);
        }

        public void m1280a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            at.m1294a(viewParent, view, i, i2, iArr);
        }

        public boolean m1282a(ViewParent viewParent, View view, float f, float f2, boolean z) {
            return at.m1296a(viewParent, view, f, f2, z);
        }

        public boolean m1281a(ViewParent viewParent, View view, float f, float f2) {
            return at.m1295a(viewParent, view, f, f2);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 21) {
            f530a = new ViewParentCompat();
        } else if (i >= 19) {
            f530a = new ViewParentCompat();
        } else if (i >= 14) {
            f530a = new ViewParentCompat();
        } else {
            f530a = new ViewParentCompat();
        }
    }

    public static boolean m1290a(ViewParent viewParent, View view, View view2, int i) {
        return f530a.m1269a(viewParent, view, view2, i);
    }

    public static void m1291b(ViewParent viewParent, View view, View view2, int i) {
        f530a.m1270b(viewParent, view, view2, i);
    }

    public static void m1285a(ViewParent viewParent, View view) {
        f530a.m1264a(viewParent, view);
    }

    public static void m1286a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
        f530a.m1265a(viewParent, view, i, i2, i3, i4);
    }

    public static void m1287a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
        f530a.m1266a(viewParent, view, i, i2, iArr);
    }

    public static boolean m1289a(ViewParent viewParent, View view, float f, float f2, boolean z) {
        return f530a.m1268a(viewParent, view, f, f2, z);
    }

    public static boolean m1288a(ViewParent viewParent, View view, float f, float f2) {
        return f530a.m1267a(viewParent, view, f, f2);
    }
}
