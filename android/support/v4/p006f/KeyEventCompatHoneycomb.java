package android.support.v4.p006f;

import android.view.KeyEvent;

/* renamed from: android.support.v4.f.h */
class KeyEventCompatHoneycomb {
    public static int m1429a(int i) {
        return KeyEvent.normalizeMetaState(i);
    }

    public static boolean m1430a(int i, int i2) {
        return KeyEvent.metaStateHasModifiers(i, i2);
    }
}
