package android.support.v4.p006f;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: android.support.v4.f.d */
public abstract class ActionProvider {
    private final Context f555a;
    private ActionProvider f556b;
    private ActionProvider f557c;

    /* renamed from: android.support.v4.f.d.a */
    public interface ActionProvider {
        void m1404a(boolean z);
    }

    /* renamed from: android.support.v4.f.d.b */
    public interface ActionProvider {
        void m1405a(boolean z);
    }

    public abstract View m1406a();

    public ActionProvider(Context context) {
        this.f555a = context;
    }

    public View m1407a(MenuItem menuItem) {
        return m1406a();
    }

    public boolean m1412b() {
        return false;
    }

    public boolean m1413c() {
        return true;
    }

    public boolean m1414d() {
        return false;
    }

    public boolean m1415e() {
        return false;
    }

    public void m1410a(SubMenu subMenu) {
    }

    public void m1411a(boolean z) {
        if (this.f556b != null) {
            this.f556b.m1404a(z);
        }
    }

    public void m1408a(ActionProvider actionProvider) {
        this.f556b = actionProvider;
    }

    public void m1409a(ActionProvider actionProvider) {
        if (!(this.f557c == null || actionProvider == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.f557c = actionProvider;
    }

    public void m1416f() {
        this.f557c = null;
        this.f556b = null;
    }
}
