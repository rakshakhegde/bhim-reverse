package android.support.v4.p006f;

import android.view.MenuItem;

/* renamed from: android.support.v4.f.r */
class MenuItemCompatIcs {
    public static boolean m1487a(MenuItem menuItem) {
        return menuItem.expandActionView();
    }

    public static boolean m1488b(MenuItem menuItem) {
        return menuItem.isActionViewExpanded();
    }
}
