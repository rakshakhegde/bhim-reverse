package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.ViewGroup.MarginLayoutParams;

/* renamed from: android.support.v4.f.n */
public final class MarginLayoutParamsCompat {
    static final MarginLayoutParamsCompat f564a;

    /* renamed from: android.support.v4.f.n.a */
    interface MarginLayoutParamsCompat {
        int m1444a(MarginLayoutParams marginLayoutParams);

        int m1445b(MarginLayoutParams marginLayoutParams);
    }

    /* renamed from: android.support.v4.f.n.b */
    static class MarginLayoutParamsCompat implements MarginLayoutParamsCompat {
        MarginLayoutParamsCompat() {
        }

        public int m1446a(MarginLayoutParams marginLayoutParams) {
            return marginLayoutParams.leftMargin;
        }

        public int m1447b(MarginLayoutParams marginLayoutParams) {
            return marginLayoutParams.rightMargin;
        }
    }

    /* renamed from: android.support.v4.f.n.c */
    static class MarginLayoutParamsCompat implements MarginLayoutParamsCompat {
        MarginLayoutParamsCompat() {
        }

        public int m1448a(MarginLayoutParams marginLayoutParams) {
            return MarginLayoutParamsCompatJellybeanMr1.m1452a(marginLayoutParams);
        }

        public int m1449b(MarginLayoutParams marginLayoutParams) {
            return MarginLayoutParamsCompatJellybeanMr1.m1453b(marginLayoutParams);
        }
    }

    static {
        if (VERSION.SDK_INT >= 17) {
            f564a = new MarginLayoutParamsCompat();
        } else {
            f564a = new MarginLayoutParamsCompat();
        }
    }

    public static int m1450a(MarginLayoutParams marginLayoutParams) {
        return f564a.m1444a(marginLayoutParams);
    }

    public static int m1451b(MarginLayoutParams marginLayoutParams) {
        return f564a.m1445b(marginLayoutParams);
    }
}
