package android.support.v4.p006f;

import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.view.View;

/* compiled from: ViewCompatHC */
/* renamed from: android.support.v4.f.ai */
class ai {
    static long m1221a() {
        return ValueAnimator.getFrameDelay();
    }

    public static void m1223a(View view, int i, Paint paint) {
        view.setLayerType(i, paint);
    }

    public static int m1220a(View view) {
        return view.getLayerType();
    }

    public static int m1219a(int i, int i2, int i3) {
        return View.resolveSizeAndState(i, i2, i3);
    }

    public static int m1225b(View view) {
        return view.getMeasuredWidthAndState();
    }

    public static int m1228c(View view) {
        return view.getMeasuredState();
    }

    public static float m1230d(View view) {
        return view.getTranslationY();
    }

    public static float m1231e(View view) {
        return view.getY();
    }

    public static void m1222a(View view, float f) {
        view.setTranslationY(f);
    }

    public static void m1226b(View view, float f) {
        view.setAlpha(f);
    }

    public static void m1229c(View view, float f) {
        view.setX(f);
    }

    public static void m1232f(View view) {
        view.jumpDrawablesToCurrentState();
    }

    public static void m1224a(View view, boolean z) {
        view.setSaveFromParentEnabled(z);
    }

    public static void m1227b(View view, boolean z) {
        view.setActivated(z);
    }
}
