package android.support.v4.p006f;

import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.os.Build.VERSION;
import android.view.View;
import java.util.WeakHashMap;

/* compiled from: ViewCompat */
/* renamed from: android.support.v4.f.af */
public final class af {
    static final ViewCompat f525a;

    /* renamed from: android.support.v4.f.af.m */
    interface ViewCompat {
        int m1051a(int i, int i2, int i3);

        int m1052a(View view);

        bb m1053a(View view, bb bbVar);

        void m1054a(View view, float f);

        void m1055a(View view, int i, int i2);

        void m1056a(View view, int i, Paint paint);

        void m1057a(View view, ColorStateList colorStateList);

        void m1058a(View view, Mode mode);

        void m1059a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat);

        void m1060a(View view, aa aaVar);

        void m1061a(View view, Runnable runnable);

        void m1062a(View view, Runnable runnable, long j);

        void m1063a(View view, boolean z);

        boolean m1064a(View view, int i);

        void m1065b(View view);

        void m1066b(View view, float f);

        void m1067b(View view, boolean z);

        int m1068c(View view);

        void m1069c(View view, float f);

        int m1070d(View view);

        void m1071d(View view, float f);

        int m1072e(View view);

        int m1073f(View view);

        int m1074g(View view);

        int m1075h(View view);

        boolean m1076i(View view);

        float m1077j(View view);

        float m1078k(View view);

        int m1079l(View view);

        au m1080m(View view);

        int m1081n(View view);

        void m1082o(View view);

        void m1083p(View view);

        ColorStateList m1084q(View view);

        Mode m1085r(View view);

        void m1086s(View view);

        boolean m1087t(View view);

        boolean m1088u(View view);

        boolean m1089v(View view);
    }

    /* renamed from: android.support.v4.f.af.a */
    static class ViewCompat implements ViewCompat {
        WeakHashMap<View, au> f523a;

        ViewCompat() {
            this.f523a = null;
        }

        public boolean m1105a(View view, int i) {
            return (view instanceof ab) && m1090a((ab) view, i);
        }

        public int m1092a(View view) {
            return 2;
        }

        public void m1100a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        }

        public void m1106b(View view) {
            view.invalidate();
        }

        public void m1102a(View view, Runnable runnable) {
            view.postDelayed(runnable, m1093a());
        }

        public void m1103a(View view, Runnable runnable, long j) {
            view.postDelayed(runnable, m1093a() + j);
        }

        long m1093a() {
            return 10;
        }

        public void m1097a(View view, int i, Paint paint) {
        }

        public int m1109c(View view) {
            return 0;
        }

        public int m1111d(View view) {
            return 0;
        }

        public int m1091a(int i, int i2, int i3) {
            return View.resolveSize(i, i2);
        }

        public int m1113e(View view) {
            return view.getMeasuredWidth();
        }

        public int m1114f(View view) {
            return 0;
        }

        public int m1115g(View view) {
            return view.getPaddingLeft();
        }

        public int m1116h(View view) {
            return view.getPaddingRight();
        }

        public boolean m1117i(View view) {
            return true;
        }

        public float m1118j(View view) {
            return 0.0f;
        }

        public float m1119k(View view) {
            return 0.0f;
        }

        public int m1120l(View view) {
            return ag.m1216d(view);
        }

        public au m1121m(View view) {
            return new au(view);
        }

        public void m1095a(View view, float f) {
        }

        public void m1107b(View view, float f) {
        }

        public void m1110c(View view, float f) {
        }

        public int m1122n(View view) {
            return 0;
        }

        public void m1123o(View view) {
        }

        public void m1112d(View view, float f) {
        }

        public void m1124p(View view) {
        }

        public void m1101a(View view, aa aaVar) {
        }

        public bb m1094a(View view, bb bbVar) {
            return bbVar;
        }

        public void m1104a(View view, boolean z) {
        }

        public void m1108b(View view, boolean z) {
        }

        public ColorStateList m1125q(View view) {
            return ag.m1211a(view);
        }

        public void m1098a(View view, ColorStateList colorStateList) {
            ag.m1212a(view, colorStateList);
        }

        public void m1099a(View view, Mode mode) {
            ag.m1213a(view, mode);
        }

        public Mode m1126r(View view) {
            return ag.m1214b(view);
        }

        private boolean m1090a(ab abVar, int i) {
            int computeVerticalScrollOffset = abVar.computeVerticalScrollOffset();
            int computeVerticalScrollRange = abVar.computeVerticalScrollRange() - abVar.computeVerticalScrollExtent();
            if (computeVerticalScrollRange == 0) {
                return false;
            }
            if (i < 0) {
                if (computeVerticalScrollOffset <= 0) {
                    return false;
                }
                return true;
            } else if (computeVerticalScrollOffset >= computeVerticalScrollRange - 1) {
                return false;
            } else {
                return true;
            }
        }

        public void m1127s(View view) {
            if (view instanceof NestedScrollingChild) {
                ((NestedScrollingChild) view).stopNestedScroll();
            }
        }

        public boolean m1128t(View view) {
            return ag.m1215c(view);
        }

        public boolean m1129u(View view) {
            return ag.m1217e(view);
        }

        public boolean m1130v(View view) {
            return false;
        }

        public void m1096a(View view, int i, int i2) {
        }
    }

    /* renamed from: android.support.v4.f.af.b */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }
    }

    /* renamed from: android.support.v4.f.af.c */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public int m1131a(View view) {
            return ah.m1218a(view);
        }
    }

    /* renamed from: android.support.v4.f.af.d */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        long m1133a() {
            return ai.m1221a();
        }

        public void m1135a(View view, int i, Paint paint) {
            ai.m1223a(view, i, paint);
        }

        public int m1139c(View view) {
            return ai.m1220a(view);
        }

        public int m1132a(int i, int i2, int i3) {
            return ai.m1219a(i, i2, i3);
        }

        public int m1141e(View view) {
            return ai.m1225b(view);
        }

        public int m1142f(View view) {
            return ai.m1228c(view);
        }

        public float m1143j(View view) {
            return ai.m1230d(view);
        }

        public void m1134a(View view, float f) {
            ai.m1222a(view, f);
        }

        public void m1137b(View view, float f) {
            ai.m1226b(view, f);
        }

        public void m1140c(View view, float f) {
            ai.m1229c(view, f);
        }

        public float m1144k(View view) {
            return ai.m1231e(view);
        }

        public void m1145p(View view) {
            ai.m1232f(view);
        }

        public void m1136a(View view, boolean z) {
            ai.m1224a(view, z);
        }

        public void m1138b(View view, boolean z) {
            ai.m1227b(view, z);
        }
    }

    /* renamed from: android.support.v4.f.af.f */
    static class ViewCompat extends ViewCompat {
        static boolean f524b;

        ViewCompat() {
        }

        static {
            f524b = false;
        }

        public boolean m1147a(View view, int i) {
            return aj.m1234a(view, i);
        }

        public void m1146a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
            aj.m1233a(view, accessibilityDelegateCompat == null ? null : accessibilityDelegateCompat.m1036a());
        }

        public au m1148m(View view) {
            if (this.a == null) {
                this.a = new WeakHashMap();
            }
            au auVar = (au) this.a.get(view);
            if (auVar != null) {
                return auVar;
            }
            auVar = new au(view);
            this.a.put(view, auVar);
            return auVar;
        }
    }

    /* renamed from: android.support.v4.f.af.e */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public boolean m1149v(View view) {
            return ak.m1235a(view);
        }
    }

    /* renamed from: android.support.v4.f.af.g */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public void m1152b(View view) {
            al.m1236a(view);
        }

        public void m1150a(View view, Runnable runnable) {
            al.m1237a(view, runnable);
        }

        public void m1151a(View view, Runnable runnable, long j) {
            al.m1238a(view, runnable, j);
        }

        public int m1154l(View view) {
            return al.m1239b(view);
        }

        public void m1155o(View view) {
            al.m1240c(view);
        }

        public boolean m1153i(View view) {
            return al.m1241d(view);
        }
    }

    /* renamed from: android.support.v4.f.af.h */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public int m1156d(View view) {
            return am.m1242a(view);
        }

        public int m1157g(View view) {
            return am.m1243b(view);
        }

        public int m1158h(View view) {
            return am.m1244c(view);
        }

        public int m1159n(View view) {
            return am.m1245d(view);
        }
    }

    /* renamed from: android.support.v4.f.af.i */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }
    }

    /* renamed from: android.support.v4.f.af.j */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public boolean m1160t(View view) {
            return an.m1246a(view);
        }

        public boolean m1161u(View view) {
            return an.m1247b(view);
        }
    }

    /* renamed from: android.support.v4.f.af.k */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public void m1167o(View view) {
            ao.m1249a(view);
        }

        public void m1166d(View view, float f) {
            ao.m1250a(view, f);
        }

        public void m1165a(View view, aa aaVar) {
            ao.m1253a(view, aaVar);
        }

        public void m1170s(View view) {
            ao.m1256d(view);
        }

        public ColorStateList m1168q(View view) {
            return ao.m1254b(view);
        }

        public void m1163a(View view, ColorStateList colorStateList) {
            ao.m1251a(view, colorStateList);
        }

        public void m1164a(View view, Mode mode) {
            ao.m1252a(view, mode);
        }

        public Mode m1169r(View view) {
            return ao.m1255c(view);
        }

        public bb m1162a(View view, bb bbVar) {
            return ao.m1248a(view, bbVar);
        }
    }

    /* renamed from: android.support.v4.f.af.l */
    static class ViewCompat extends ViewCompat {
        ViewCompat() {
        }

        public void m1171a(View view, int i, int i2) {
            ap.m1257a(view, i, i2);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 23) {
            f525a = new ViewCompat();
        } else if (i >= 21) {
            f525a = new ViewCompat();
        } else if (i >= 19) {
            f525a = new ViewCompat();
        } else if (i >= 17) {
            f525a = new ViewCompat();
        } else if (i >= 16) {
            f525a = new ViewCompat();
        } else if (i >= 15) {
            f525a = new ViewCompat();
        } else if (i >= 14) {
            f525a = new ViewCompat();
        } else if (i >= 11) {
            f525a = new ViewCompat();
        } else if (i >= 9) {
            f525a = new ViewCompat();
        } else if (i >= 7) {
            f525a = new ViewCompat();
        } else {
            f525a = new ViewCompat();
        }
    }

    public static boolean m1185a(View view, int i) {
        return f525a.m1064a(view, i);
    }

    public static int m1173a(View view) {
        return f525a.m1052a(view);
    }

    public static void m1180a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        f525a.m1059a(view, accessibilityDelegateCompat);
    }

    public static void m1186b(View view) {
        f525a.m1065b(view);
    }

    public static void m1182a(View view, Runnable runnable) {
        f525a.m1061a(view, runnable);
    }

    public static void m1183a(View view, Runnable runnable, long j) {
        f525a.m1062a(view, runnable, j);
    }

    public static void m1177a(View view, int i, Paint paint) {
        f525a.m1056a(view, i, paint);
    }

    public static int m1189c(View view) {
        return f525a.m1068c(view);
    }

    public static int m1191d(View view) {
        return f525a.m1070d(view);
    }

    public static int m1172a(int i, int i2, int i3) {
        return f525a.m1051a(i, i2, i3);
    }

    public static int m1193e(View view) {
        return f525a.m1072e(view);
    }

    public static int m1194f(View view) {
        return f525a.m1073f(view);
    }

    public static int m1195g(View view) {
        return f525a.m1074g(view);
    }

    public static int m1196h(View view) {
        return f525a.m1075h(view);
    }

    public static float m1197i(View view) {
        return f525a.m1077j(view);
    }

    public static int m1198j(View view) {
        return f525a.m1079l(view);
    }

    public static au m1199k(View view) {
        return f525a.m1080m(view);
    }

    public static void m1175a(View view, float f) {
        f525a.m1054a(view, f);
    }

    public static void m1187b(View view, float f) {
        f525a.m1066b(view, f);
    }

    public static void m1190c(View view, float f) {
        f525a.m1069c(view, f);
    }

    public static float m1200l(View view) {
        return f525a.m1078k(view);
    }

    public static void m1192d(View view, float f) {
        f525a.m1071d(view, f);
    }

    public static int m1201m(View view) {
        return f525a.m1081n(view);
    }

    public static void m1202n(View view) {
        f525a.m1082o(view);
    }

    public static void m1203o(View view) {
        f525a.m1083p(view);
    }

    public static void m1181a(View view, aa aaVar) {
        f525a.m1060a(view, aaVar);
    }

    public static bb m1174a(View view, bb bbVar) {
        return f525a.m1053a(view, bbVar);
    }

    public static void m1184a(View view, boolean z) {
        f525a.m1063a(view, z);
    }

    public static void m1188b(View view, boolean z) {
        f525a.m1067b(view, z);
    }

    public static boolean m1204p(View view) {
        return f525a.m1076i(view);
    }

    public static ColorStateList m1205q(View view) {
        return f525a.m1084q(view);
    }

    public static void m1178a(View view, ColorStateList colorStateList) {
        f525a.m1057a(view, colorStateList);
    }

    public static Mode m1206r(View view) {
        return f525a.m1085r(view);
    }

    public static void m1179a(View view, Mode mode) {
        f525a.m1058a(view, mode);
    }

    public static void m1207s(View view) {
        f525a.m1086s(view);
    }

    public static boolean m1208t(View view) {
        return f525a.m1087t(view);
    }

    public static boolean m1209u(View view) {
        return f525a.m1088u(view);
    }

    public static boolean m1210v(View view) {
        return f525a.m1089v(view);
    }

    public static void m1176a(View view, int i, int i2) {
        f525a.m1055a(view, i, i2);
    }
}
