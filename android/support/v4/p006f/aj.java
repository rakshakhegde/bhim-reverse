package android.support.v4.p006f;

import android.view.View;
import android.view.View.AccessibilityDelegate;

/* compiled from: ViewCompatICS */
/* renamed from: android.support.v4.f.aj */
class aj {
    public static boolean m1234a(View view, int i) {
        return view.canScrollVertically(i);
    }

    public static void m1233a(View view, Object obj) {
        view.setAccessibilityDelegate((AccessibilityDelegate) obj);
    }
}
