package android.support.v4.p006f;

import android.view.Gravity;

/* renamed from: android.support.v4.f.f */
class GravityCompatJellybeanMr1 {
    public static int m1421a(int i, int i2) {
        return Gravity.getAbsoluteGravity(i, i2);
    }
}
