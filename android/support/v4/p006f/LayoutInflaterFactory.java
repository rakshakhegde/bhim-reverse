package android.support.v4.p006f;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: android.support.v4.f.m */
public interface LayoutInflaterFactory {
    View m467a(View view, String str, Context context, AttributeSet attributeSet);
}
