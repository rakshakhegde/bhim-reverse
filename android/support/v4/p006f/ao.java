package android.support.v4.p006f;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.WindowInsets;

/* compiled from: ViewCompatLollipop */
/* renamed from: android.support.v4.f.ao */
class ao {

    /* renamed from: android.support.v4.f.ao.1 */
    static class ViewCompatLollipop implements OnApplyWindowInsetsListener {
        final /* synthetic */ aa f528a;

        ViewCompatLollipop(aa aaVar) {
            this.f528a = aaVar;
        }

        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            return ((bc) this.f528a.m1045a(view, new bc(windowInsets))).m1400e();
        }
    }

    public static void m1249a(View view) {
        view.requestApplyInsets();
    }

    public static void m1250a(View view, float f) {
        view.setElevation(f);
    }

    public static void m1253a(View view, aa aaVar) {
        if (aaVar == null) {
            view.setOnApplyWindowInsetsListener(null);
        } else {
            view.setOnApplyWindowInsetsListener(new ViewCompatLollipop(aaVar));
        }
    }

    static ColorStateList m1254b(View view) {
        return view.getBackgroundTintList();
    }

    static void m1251a(View view, ColorStateList colorStateList) {
        view.setBackgroundTintList(colorStateList);
        if (VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            Object obj = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? null : 1;
            if (background != null && obj != null) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    static Mode m1255c(View view) {
        return view.getBackgroundTintMode();
    }

    static void m1252a(View view, Mode mode) {
        view.setBackgroundTintMode(mode);
        if (VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            Object obj = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? null : 1;
            if (background != null && obj != null) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    public static bb m1248a(View view, bb bbVar) {
        if (!(bbVar instanceof bc)) {
            return bbVar;
        }
        WindowInsets e = ((bc) bbVar).m1400e();
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(e);
        if (onApplyWindowInsets != e) {
            return new bc(onApplyWindowInsets);
        }
        return bbVar;
    }

    public static void m1256d(View view) {
        view.stopNestedScroll();
    }
}
