package android.support.v4.p006f;

import android.view.View;

/* compiled from: ViewCompatJellybeanMr1 */
/* renamed from: android.support.v4.f.am */
class am {
    public static int m1242a(View view) {
        return view.getLayoutDirection();
    }

    public static int m1243b(View view) {
        return view.getPaddingStart();
    }

    public static int m1244c(View view) {
        return view.getPaddingEnd();
    }

    public static int m1245d(View view) {
        return view.getWindowSystemUiVisibility();
    }
}
