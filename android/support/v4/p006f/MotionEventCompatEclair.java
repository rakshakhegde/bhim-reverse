package android.support.v4.p006f;

import android.view.MotionEvent;

/* renamed from: android.support.v4.f.t */
class MotionEventCompatEclair {
    public static int m1515a(MotionEvent motionEvent, int i) {
        return motionEvent.findPointerIndex(i);
    }

    public static int m1516b(MotionEvent motionEvent, int i) {
        return motionEvent.getPointerId(i);
    }

    public static float m1517c(MotionEvent motionEvent, int i) {
        return motionEvent.getX(i);
    }

    public static float m1518d(MotionEvent motionEvent, int i) {
        return motionEvent.getY(i);
    }
}
