package android.support.v4.p006f;

import android.view.View;
import android.view.ViewParent;

/* renamed from: android.support.v4.f.x */
public class NestedScrollingChildHelper {
    private final View f567a;
    private ViewParent f568b;
    private boolean f569c;
    private int[] f570d;

    public NestedScrollingChildHelper(View view) {
        this.f567a = view;
    }

    public void m1521a(boolean z) {
        if (this.f569c) {
            af.m1207s(this.f567a);
        }
        this.f569c = z;
    }

    public boolean m1522a() {
        return this.f569c;
    }

    public boolean m1528b() {
        return this.f568b != null;
    }

    public boolean m1525a(int i) {
        if (m1528b()) {
            return true;
        }
        if (m1522a()) {
            View view = this.f567a;
            for (ViewParent parent = this.f567a.getParent(); parent != null; parent = parent.getParent()) {
                if (as.m1290a(parent, view, this.f567a, i)) {
                    this.f568b = parent;
                    as.m1291b(parent, view, this.f567a, i);
                    return true;
                }
                if (parent instanceof View) {
                    view = (View) parent;
                }
            }
        }
        return false;
    }

    public void m1529c() {
        if (this.f568b != null) {
            as.m1285a(this.f568b, this.f567a);
            this.f568b = null;
        }
    }

    public boolean m1526a(int i, int i2, int i3, int i4, int[] iArr) {
        if (!m1522a() || this.f568b == null) {
            return false;
        }
        if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
            int i5;
            int i6;
            if (iArr != null) {
                this.f567a.getLocationInWindow(iArr);
                int i7 = iArr[0];
                i5 = iArr[1];
                i6 = i7;
            } else {
                i5 = 0;
                i6 = 0;
            }
            as.m1286a(this.f568b, this.f567a, i, i2, i3, i4);
            if (iArr != null) {
                this.f567a.getLocationInWindow(iArr);
                iArr[0] = iArr[0] - i6;
                iArr[1] = iArr[1] - i5;
            }
            return true;
        } else if (iArr == null) {
            return false;
        } else {
            iArr[0] = 0;
            iArr[1] = 0;
            return false;
        }
    }

    public boolean m1527a(int i, int i2, int[] iArr, int[] iArr2) {
        if (!m1522a() || this.f568b == null) {
            return false;
        }
        if (i != 0 || i2 != 0) {
            int i3;
            int i4;
            if (iArr2 != null) {
                this.f567a.getLocationInWindow(iArr2);
                i3 = iArr2[0];
                i4 = iArr2[1];
            } else {
                i4 = 0;
                i3 = 0;
            }
            if (iArr == null) {
                if (this.f570d == null) {
                    this.f570d = new int[2];
                }
                iArr = this.f570d;
            }
            iArr[0] = 0;
            iArr[1] = 0;
            as.m1287a(this.f568b, this.f567a, i, i2, iArr);
            if (iArr2 != null) {
                this.f567a.getLocationInWindow(iArr2);
                iArr2[0] = iArr2[0] - i3;
                iArr2[1] = iArr2[1] - i4;
            }
            if (iArr[0] == 0 && iArr[1] == 0) {
                return false;
            }
            return true;
        } else if (iArr2 == null) {
            return false;
        } else {
            iArr2[0] = 0;
            iArr2[1] = 0;
            return false;
        }
    }

    public boolean m1524a(float f, float f2, boolean z) {
        if (!m1522a() || this.f568b == null) {
            return false;
        }
        return as.m1289a(this.f568b, this.f567a, f, f2, z);
    }

    public boolean m1523a(float f, float f2) {
        if (!m1522a() || this.f568b == null) {
            return false;
        }
        return as.m1288a(this.f568b, this.f567a, f, f2);
    }
}
