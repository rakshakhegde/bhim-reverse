package android.support.v4.p006f;

import android.os.Build.VERSION;
import android.view.ViewConfiguration;

/* compiled from: ViewConfigurationCompat */
/* renamed from: android.support.v4.f.aq */
public final class aq {
    static final ViewConfigurationCompat f529a;

    /* renamed from: android.support.v4.f.aq.e */
    interface ViewConfigurationCompat {
        boolean m1258a(ViewConfiguration viewConfiguration);
    }

    /* renamed from: android.support.v4.f.aq.a */
    static class ViewConfigurationCompat implements ViewConfigurationCompat {
        ViewConfigurationCompat() {
        }

        public boolean m1259a(ViewConfiguration viewConfiguration) {
            return true;
        }
    }

    /* renamed from: android.support.v4.f.aq.b */
    static class ViewConfigurationCompat extends ViewConfigurationCompat {
        ViewConfigurationCompat() {
        }
    }

    /* renamed from: android.support.v4.f.aq.c */
    static class ViewConfigurationCompat extends ViewConfigurationCompat {
        ViewConfigurationCompat() {
        }

        public boolean m1260a(ViewConfiguration viewConfiguration) {
            return false;
        }
    }

    /* renamed from: android.support.v4.f.aq.d */
    static class ViewConfigurationCompat extends ViewConfigurationCompat {
        ViewConfigurationCompat() {
        }

        public boolean m1261a(ViewConfiguration viewConfiguration) {
            return ar.m1263a(viewConfiguration);
        }
    }

    static {
        if (VERSION.SDK_INT >= 14) {
            f529a = new ViewConfigurationCompat();
        } else if (VERSION.SDK_INT >= 11) {
            f529a = new ViewConfigurationCompat();
        } else if (VERSION.SDK_INT >= 8) {
            f529a = new ViewConfigurationCompat();
        } else {
            f529a = new ViewConfigurationCompat();
        }
    }

    public static boolean m1262a(ViewConfiguration viewConfiguration) {
        return f529a.m1258a(viewConfiguration);
    }
}
