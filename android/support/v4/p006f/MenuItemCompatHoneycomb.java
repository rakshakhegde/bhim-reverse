package android.support.v4.p006f;

import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v4.f.q */
class MenuItemCompatHoneycomb {
    public static void m1485a(MenuItem menuItem, int i) {
        menuItem.setShowAsAction(i);
    }

    public static MenuItem m1483a(MenuItem menuItem, View view) {
        return menuItem.setActionView(view);
    }

    public static MenuItem m1486b(MenuItem menuItem, int i) {
        return menuItem.setActionView(i);
    }

    public static View m1484a(MenuItem menuItem) {
        return menuItem.getActionView();
    }
}
