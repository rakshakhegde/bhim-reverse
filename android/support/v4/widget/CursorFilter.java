package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;
import android.widget.Filter.FilterResults;

/* renamed from: android.support.v4.widget.g */
class CursorFilter extends Filter {
    CursorFilter f645a;

    /* renamed from: android.support.v4.widget.g.a */
    interface CursorFilter {
        Cursor m1636a();

        Cursor m1637a(CharSequence charSequence);

        void m1638a(Cursor cursor);

        CharSequence m1639c(Cursor cursor);
    }

    CursorFilter(CursorFilter cursorFilter) {
        this.f645a = cursorFilter;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f645a.m1639c((Cursor) obj);
    }

    protected FilterResults performFiltering(CharSequence charSequence) {
        Cursor a = this.f645a.m1637a(charSequence);
        FilterResults filterResults = new FilterResults();
        if (a != null) {
            filterResults.count = a.getCount();
            filterResults.values = a;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        Cursor a = this.f645a.m1636a();
        if (filterResults.values != null && filterResults.values != a) {
            this.f645a.m1638a((Cursor) filterResults.values);
        }
    }
}
