package android.support.v4.widget;

import android.widget.PopupWindow;

/* renamed from: android.support.v4.widget.q */
class PopupWindowCompatApi23 {
    static void m1708a(PopupWindow popupWindow, boolean z) {
        popupWindow.setOverlapAnchor(z);
    }

    static void m1707a(PopupWindow popupWindow, int i) {
        popupWindow.setWindowLayoutType(i);
    }
}
