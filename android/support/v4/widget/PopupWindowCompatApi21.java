package android.support.v4.widget;

import android.util.Log;
import android.widget.PopupWindow;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.widget.p */
class PopupWindowCompatApi21 {
    private static Field f650a;

    static {
        try {
            f650a = PopupWindow.class.getDeclaredField("mOverlapAnchor");
            f650a.setAccessible(true);
        } catch (Throwable e) {
            Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
        }
    }

    static void m1706a(PopupWindow popupWindow, boolean z) {
        if (f650a != null) {
            try {
                f650a.set(popupWindow, Boolean.valueOf(z));
            } catch (Throwable e) {
                Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e);
            }
        }
    }
}
