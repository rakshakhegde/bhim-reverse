package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.p006f.AccessibilityDelegateCompat;
import android.support.v4.p006f.MotionEventCompat;
import android.support.v4.p006f.NestedScrollingChild;
import android.support.v4.p006f.NestedScrollingChildHelper;
import android.support.v4.p006f.NestedScrollingParent;
import android.support.v4.p006f.NestedScrollingParentHelper;
import android.support.v4.p006f.ab;
import android.support.v4.p006f.ad;
import android.support.v4.p006f.af;
import android.support.v4.p006f.p011a.AccessibilityEventCompat;
import android.support.v4.p006f.p011a.AccessibilityNodeInfoCompat;
import android.support.v4.p006f.p011a.AccessibilityRecordCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import java.util.List;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class NestedScrollView extends FrameLayout implements ab, NestedScrollingChild, NestedScrollingParent {
    private static final C0002a f574v;
    private static final int[] f575w;
    private C0003b f576A;
    private long f577a;
    private final Rect f578b;
    private ScrollerCompat f579c;
    private EdgeEffectCompat f580d;
    private EdgeEffectCompat f581e;
    private int f582f;
    private boolean f583g;
    private boolean f584h;
    private View f585i;
    private boolean f586j;
    private VelocityTracker f587k;
    private boolean f588l;
    private boolean f589m;
    private int f590n;
    private int f591o;
    private int f592p;
    private int f593q;
    private final int[] f594r;
    private final int[] f595s;
    private int f596t;
    private SavedState f597u;
    private final NestedScrollingParentHelper f598x;
    private final NestedScrollingChildHelper f599y;
    private float f600z;

    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        public int f573a;

        /* renamed from: android.support.v4.widget.NestedScrollView.SavedState.1 */
        static class C00011 implements Creator<SavedState> {
            C00011() {
            }

            public /* synthetic */ Object createFromParcel(Parcel parcel) {
                return m1535a(parcel);
            }

            public /* synthetic */ Object[] newArray(int i) {
                return m1536a(i);
            }

            public SavedState m1535a(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] m1536a(int i) {
                return new SavedState[i];
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f573a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f573a);
        }

        public String toString() {
            return "HorizontalScrollView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " scrollPosition=" + this.f573a + "}";
        }

        static {
            CREATOR = new C00011();
        }
    }

    /* renamed from: android.support.v4.widget.NestedScrollView.a */
    static class C0002a extends AccessibilityDelegateCompat {
        C0002a() {
        }

        public boolean m1538a(View view, int i, Bundle bundle) {
            if (super.m1040a(view, i, bundle)) {
                return true;
            }
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            if (!nestedScrollView.isEnabled()) {
                return false;
            }
            int min;
            switch (i) {
                case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                    min = Math.min(((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()) + nestedScrollView.getScrollY(), nestedScrollView.getScrollRange());
                    if (min == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.m1566b(0, min);
                    return true;
                case 8192:
                    min = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                    if (min == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.m1566b(0, min);
                    return true;
                default:
                    return false;
            }
        }

        public void m1537a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.m1038a(view, accessibilityNodeInfoCompat);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            accessibilityNodeInfoCompat.m945a(ScrollView.class.getName());
            if (nestedScrollView.isEnabled()) {
                int a = nestedScrollView.getScrollRange();
                if (a > 0) {
                    accessibilityNodeInfoCompat.m946a(true);
                    if (nestedScrollView.getScrollY() > 0) {
                        accessibilityNodeInfoCompat.m943a(8192);
                    }
                    if (nestedScrollView.getScrollY() < a) {
                        accessibilityNodeInfoCompat.m943a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                    }
                }
            }
        }

        public void m1539d(View view, AccessibilityEvent accessibilityEvent) {
            super.m1044d(view, accessibilityEvent);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            accessibilityEvent.setClassName(ScrollView.class.getName());
            AccessibilityRecordCompat a = AccessibilityEventCompat.m877a(accessibilityEvent);
            a.m1026a(nestedScrollView.getScrollRange() > 0);
            a.m1025a(nestedScrollView.getScrollX());
            a.m1027b(nestedScrollView.getScrollY());
            a.m1028c(nestedScrollView.getScrollX());
            a.m1029d(nestedScrollView.getScrollRange());
        }
    }

    /* renamed from: android.support.v4.widget.NestedScrollView.b */
    public interface C0003b {
        void m1540a(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4);
    }

    static {
        f574v = new C0002a();
        f575w = new int[]{16843130};
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f578b = new Rect();
        this.f583g = true;
        this.f584h = false;
        this.f585i = null;
        this.f586j = false;
        this.f589m = true;
        this.f593q = -1;
        this.f594r = new int[2];
        this.f595s = new int[2];
        m1543a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f575w, i, 0);
        setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.f598x = new NestedScrollingParentHelper(this);
        this.f599y = new NestedScrollingChildHelper(this);
        setNestedScrollingEnabled(true);
        af.m1180a((View) this, f574v);
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f599y.m1521a(z);
    }

    public boolean isNestedScrollingEnabled() {
        return this.f599y.m1522a();
    }

    public boolean startNestedScroll(int i) {
        return this.f599y.m1525a(i);
    }

    public void stopNestedScroll() {
        this.f599y.m1529c();
    }

    public boolean hasNestedScrollingParent() {
        return this.f599y.m1528b();
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.f599y.m1526a(i, i2, i3, i4, iArr);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.f599y.m1527a(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.f599y.m1524a(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.f599y.m1523a(f, f2);
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return (i & 2) != 0;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f598x.m1532a(view, view2, i);
        startNestedScroll(2);
    }

    public void onStopNestedScroll(View view) {
        this.f598x.m1531a(view);
        stopNestedScroll();
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int scrollY = getScrollY();
        scrollBy(0, i4);
        int scrollY2 = getScrollY() - scrollY;
        dispatchNestedScroll(0, scrollY2, 0, i4 - scrollY2, null);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        dispatchNestedPreScroll(i, i2, iArr, null);
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (z) {
            return false;
        }
        m1559f((int) f2);
        return true;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return dispatchNestedPreFling(f, f2);
    }

    public int getNestedScrollAxes() {
        return this.f598x.m1530a();
    }

    public boolean shouldDelayChildPressedState() {
        return true;
    }

    protected float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    protected float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = (getChildAt(0).getBottom() - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (0.5f * ((float) getHeight()));
    }

    private void m1543a() {
        this.f579c = ScrollerCompat.m1751a(getContext(), null);
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.f590n = viewConfiguration.getScaledTouchSlop();
        this.f591o = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f592p = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void addView(View view) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view);
    }

    public void addView(View view, int i) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i);
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, layoutParams);
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i, layoutParams);
    }

    public void setOnScrollChangeListener(C0003b c0003b) {
        this.f576A = c0003b;
    }

    private boolean m1552b() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        if (getHeight() < (childAt.getHeight() + getPaddingTop()) + getPaddingBottom()) {
            return true;
        }
        return false;
    }

    public void setFillViewport(boolean z) {
        if (z != this.f588l) {
            this.f588l = z;
            requestLayout();
        }
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.f589m = z;
    }

    protected void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.f576A != null) {
            this.f576A.m1540a(this, i, i2, i3, i4);
        }
    }

    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f588l && MeasureSpec.getMode(i2) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            int measuredHeight = getMeasuredHeight();
            if (childAt.getMeasuredHeight() < measuredHeight) {
                childAt.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).width), MeasureSpec.makeMeasureSpec((measuredHeight - getPaddingTop()) - getPaddingBottom(), 1073741824));
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || m1565a(keyEvent);
    }

    public boolean m1565a(KeyEvent keyEvent) {
        int i = 33;
        this.f578b.setEmpty();
        if (m1552b()) {
            if (keyEvent.getAction() != 0) {
                return false;
            }
            switch (keyEvent.getKeyCode()) {
                case R.Toolbar_collapseContentDescription /*19*/:
                    if (keyEvent.isAltPressed()) {
                        return m1567b(33);
                    }
                    return m1568c(33);
                case R.Toolbar_navigationIcon /*20*/:
                    if (keyEvent.isAltPressed()) {
                        return m1567b(130);
                    }
                    return m1568c(130);
                case R.AppCompatTheme_editTextColor /*62*/:
                    if (!keyEvent.isShiftPressed()) {
                        i = 130;
                    }
                    m1563a(i);
                    return false;
                default:
                    return false;
            }
        } else if (!isFocused() || keyEvent.getKeyCode() == 4) {
            return false;
        } else {
            boolean z;
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            findFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, 130);
            if (findFocus == null || findFocus == this || !findFocus.requestFocus(130)) {
                z = false;
            } else {
                z = true;
            }
            return z;
        }
    }

    private boolean m1554c(int i, int i2) {
        if (getChildCount() <= 0) {
            return false;
        }
        int scrollY = getScrollY();
        View childAt = getChildAt(0);
        if (i2 < childAt.getTop() - scrollY || i2 >= childAt.getBottom() - scrollY || i < childAt.getLeft() || i >= childAt.getRight()) {
            return false;
        }
        return true;
    }

    private void m1553c() {
        if (this.f587k == null) {
            this.f587k = VelocityTracker.obtain();
        } else {
            this.f587k.clear();
        }
    }

    private void m1555d() {
        if (this.f587k == null) {
            this.f587k = VelocityTracker.obtain();
        }
    }

    private void m1556e() {
        if (this.f587k != null) {
            this.f587k.recycle();
            this.f587k = null;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (z) {
            m1556e();
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        int action = motionEvent.getAction();
        if (action == 2 && this.f586j) {
            return true;
        }
        switch (action & 255) {
            case R.View_android_theme /*0*/:
                action = (int) motionEvent.getY();
                if (!m1554c((int) motionEvent.getX(), action)) {
                    this.f586j = false;
                    m1556e();
                    break;
                }
                this.f582f = action;
                this.f593q = MotionEventCompat.m1510b(motionEvent, 0);
                m1553c();
                this.f587k.addMovement(motionEvent);
                this.f579c.m1761f();
                if (!this.f579c.m1755a()) {
                    z = true;
                }
                this.f586j = z;
                startNestedScroll(2);
                break;
            case R.View_android_focusable /*1*/:
            case R.View_paddingEnd /*3*/:
                this.f586j = false;
                this.f593q = -1;
                m1556e();
                if (this.f579c.m1756a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    af.m1186b(this);
                }
                stopNestedScroll();
                break;
            case R.View_paddingStart /*2*/:
                action = this.f593q;
                if (action != -1) {
                    int a = MotionEventCompat.m1508a(motionEvent, action);
                    if (a != -1) {
                        action = (int) MotionEventCompat.m1513d(motionEvent, a);
                        if (Math.abs(action - this.f582f) > this.f590n && (getNestedScrollAxes() & 2) == 0) {
                            this.f586j = true;
                            this.f582f = action;
                            m1555d();
                            this.f587k.addMovement(motionEvent);
                            this.f596t = 0;
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                                break;
                            }
                        }
                    }
                    Log.e("NestedScrollView", "Invalid pointerId=" + action + " in onInterceptTouchEvent");
                    break;
                }
                break;
            case R.Toolbar_contentInsetEnd /*6*/:
                m1544a(motionEvent);
                break;
        }
        return this.f586j;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        m1555d();
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        int a = MotionEventCompat.m1507a(motionEvent);
        if (a == 0) {
            this.f596t = 0;
        }
        obtain.offsetLocation(0.0f, (float) this.f596t);
        switch (a) {
            case R.View_android_theme /*0*/:
                if (getChildCount() != 0) {
                    boolean z = !this.f579c.m1755a();
                    this.f586j = z;
                    if (z) {
                        ViewParent parent = getParent();
                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }
                    }
                    if (!this.f579c.m1755a()) {
                        this.f579c.m1762g();
                    }
                    this.f582f = (int) motionEvent.getY();
                    this.f593q = MotionEventCompat.m1510b(motionEvent, 0);
                    startNestedScroll(2);
                    break;
                }
                return false;
            case R.View_android_focusable /*1*/:
                if (this.f586j) {
                    VelocityTracker velocityTracker = this.f587k;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.f592p);
                    a = (int) ad.m1049a(velocityTracker, this.f593q);
                    if (Math.abs(a) > this.f591o) {
                        m1559f(-a);
                    } else if (this.f579c.m1756a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                        af.m1186b(this);
                    }
                }
                this.f593q = -1;
                m1558f();
                break;
            case R.View_paddingStart /*2*/:
                int a2 = MotionEventCompat.m1508a(motionEvent, this.f593q);
                if (a2 != -1) {
                    int i;
                    int d = (int) MotionEventCompat.m1513d(motionEvent, a2);
                    a = this.f582f - d;
                    if (dispatchNestedPreScroll(0, a, this.f595s, this.f594r)) {
                        a -= this.f595s[1];
                        obtain.offsetLocation(0.0f, (float) this.f594r[1]);
                        this.f596t += this.f594r[1];
                    }
                    if (this.f586j || Math.abs(a) <= this.f590n) {
                        i = a;
                    } else {
                        ViewParent parent2 = getParent();
                        if (parent2 != null) {
                            parent2.requestDisallowInterceptTouchEvent(true);
                        }
                        this.f586j = true;
                        if (a > 0) {
                            i = a - this.f590n;
                        } else {
                            i = a + this.f590n;
                        }
                    }
                    if (this.f586j) {
                        Object obj;
                        this.f582f = d - this.f594r[1];
                        int scrollY = getScrollY();
                        int scrollRange = getScrollRange();
                        a = af.m1173a(this);
                        if (a == 0 || (a == 1 && scrollRange > 0)) {
                            obj = 1;
                        } else {
                            obj = null;
                        }
                        if (m1564a(0, i, 0, getScrollY(), 0, scrollRange, 0, 0, true) && !hasNestedScrollingParent()) {
                            this.f587k.clear();
                        }
                        int scrollY2 = getScrollY() - scrollY;
                        if (!dispatchNestedScroll(0, scrollY2, 0, i - scrollY2, this.f594r)) {
                            if (obj != null) {
                                m1560g();
                                a = scrollY + i;
                                if (a < 0) {
                                    this.f580d.m1674a(((float) i) / ((float) getHeight()), MotionEventCompat.m1511c(motionEvent, a2) / ((float) getWidth()));
                                    if (!this.f581e.m1673a()) {
                                        this.f581e.m1677b();
                                    }
                                } else if (a > scrollRange) {
                                    this.f581e.m1674a(((float) i) / ((float) getHeight()), 1.0f - (MotionEventCompat.m1511c(motionEvent, a2) / ((float) getWidth())));
                                    if (!this.f580d.m1673a()) {
                                        this.f580d.m1677b();
                                    }
                                }
                                if (!(this.f580d == null || (this.f580d.m1673a() && this.f581e.m1673a()))) {
                                    af.m1186b(this);
                                    break;
                                }
                            }
                        }
                        this.f582f -= this.f594r[1];
                        obtain.offsetLocation(0.0f, (float) this.f594r[1]);
                        this.f596t += this.f594r[1];
                        break;
                    }
                }
                Log.e("NestedScrollView", "Invalid pointerId=" + this.f593q + " in onTouchEvent");
                break;
                break;
            case R.View_paddingEnd /*3*/:
                if (this.f586j && getChildCount() > 0 && this.f579c.m1756a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    af.m1186b(this);
                }
                this.f593q = -1;
                m1558f();
                break;
            case R.Toolbar_contentInsetStart /*5*/:
                a = MotionEventCompat.m1509b(motionEvent);
                this.f582f = (int) MotionEventCompat.m1513d(motionEvent, a);
                this.f593q = MotionEventCompat.m1510b(motionEvent, a);
                break;
            case R.Toolbar_contentInsetEnd /*6*/:
                m1544a(motionEvent);
                this.f582f = (int) MotionEventCompat.m1513d(motionEvent, MotionEventCompat.m1508a(motionEvent, this.f593q));
                break;
        }
        if (this.f587k != null) {
            this.f587k.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    private void m1544a(MotionEvent motionEvent) {
        int action = (motionEvent.getAction() & 65280) >> 8;
        if (MotionEventCompat.m1510b(motionEvent, action) == this.f593q) {
            action = action == 0 ? 1 : 0;
            this.f582f = (int) MotionEventCompat.m1513d(motionEvent, action);
            this.f593q = MotionEventCompat.m1510b(motionEvent, action);
            if (this.f587k != null) {
                this.f587k.clear();
            }
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((MotionEventCompat.m1512c(motionEvent) & 2) == 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case R.Toolbar_contentInsetRight /*8*/:
                if (this.f586j) {
                    return false;
                }
                float e = MotionEventCompat.m1514e(motionEvent, 9);
                if (e == 0.0f) {
                    return false;
                }
                int verticalScrollFactorCompat = (int) (e * getVerticalScrollFactorCompat());
                int scrollRange = getScrollRange();
                int scrollY = getScrollY();
                verticalScrollFactorCompat = scrollY - verticalScrollFactorCompat;
                if (verticalScrollFactorCompat < 0) {
                    scrollRange = 0;
                } else if (verticalScrollFactorCompat <= scrollRange) {
                    scrollRange = verticalScrollFactorCompat;
                }
                if (scrollRange == scrollY) {
                    return false;
                }
                super.scrollTo(getScrollX(), scrollRange);
                return true;
            default:
                return false;
        }
    }

    private float getVerticalScrollFactorCompat() {
        if (this.f600z == 0.0f) {
            TypedValue typedValue = new TypedValue();
            Context context = getContext();
            if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                this.f600z = typedValue.getDimension(context.getResources().getDisplayMetrics());
            } else {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
        }
        return this.f600z;
    }

    protected void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.scrollTo(i, i2);
    }

    boolean m1564a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
        boolean z2;
        boolean z3;
        int a = af.m1173a(this);
        Object obj = computeHorizontalScrollRange() > computeHorizontalScrollExtent() ? 1 : null;
        Object obj2 = computeVerticalScrollRange() > computeVerticalScrollExtent() ? 1 : null;
        Object obj3 = (a == 0 || (a == 1 && obj != null)) ? 1 : null;
        obj = (a == 0 || (a == 1 && obj2 != null)) ? 1 : null;
        int i9 = i3 + i;
        if (obj3 == null) {
            i7 = 0;
        }
        int i10 = i4 + i2;
        if (obj == null) {
            i8 = 0;
        }
        int i11 = -i7;
        int i12 = i7 + i5;
        a = -i8;
        int i13 = i8 + i6;
        if (i9 > i12) {
            z2 = true;
        } else if (i9 < i11) {
            z2 = true;
            i12 = i11;
        } else {
            z2 = false;
            i12 = i9;
        }
        if (i10 > i13) {
            z3 = true;
        } else if (i10 < a) {
            z3 = true;
            i13 = a;
        } else {
            z3 = false;
            i13 = i10;
        }
        if (z3) {
            this.f579c.m1756a(i12, i13, 0, 0, 0, getScrollRange());
        }
        onOverScrolled(i12, i13, z2, z3);
        if (z2 || z3) {
            return true;
        }
        return false;
    }

    private int getScrollRange() {
        if (getChildCount() > 0) {
            return Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
        }
        return 0;
    }

    private View m1542a(boolean z, int i, int i2) {
        List focusables = getFocusables(2);
        View view = null;
        Object obj = null;
        int size = focusables.size();
        int i3 = 0;
        while (i3 < size) {
            View view2;
            Object obj2;
            View view3 = (View) focusables.get(i3);
            int top = view3.getTop();
            int bottom = view3.getBottom();
            if (i < bottom && top < i2) {
                Object obj3 = (i >= top || bottom >= i2) ? null : 1;
                if (view == null) {
                    Object obj4 = obj3;
                    view2 = view3;
                    obj2 = obj4;
                } else {
                    Object obj5 = ((!z || top >= view.getTop()) && (z || bottom <= view.getBottom())) ? null : 1;
                    if (obj != null) {
                        if (!(obj3 == null || obj5 == null)) {
                            view2 = view3;
                            obj2 = obj;
                        }
                    } else if (obj3 != null) {
                        view2 = view3;
                        int i4 = 1;
                    } else if (obj5 != null) {
                        view2 = view3;
                        obj2 = obj;
                    }
                }
                i3++;
                view = view2;
                obj = obj2;
            }
            obj2 = obj;
            view2 = view;
            i3++;
            view = view2;
            obj = obj2;
        }
        return view;
    }

    public boolean m1563a(int i) {
        int i2 = i == 130 ? 1 : 0;
        int height = getHeight();
        if (i2 != 0) {
            this.f578b.top = getScrollY() + height;
            i2 = getChildCount();
            if (i2 > 0) {
                View childAt = getChildAt(i2 - 1);
                if (this.f578b.top + height > childAt.getBottom()) {
                    this.f578b.top = childAt.getBottom() - height;
                }
            }
        } else {
            this.f578b.top = getScrollY() - height;
            if (this.f578b.top < 0) {
                this.f578b.top = 0;
            }
        }
        this.f578b.bottom = this.f578b.top + height;
        return m1545a(i, this.f578b.top, this.f578b.bottom);
    }

    public boolean m1567b(int i) {
        int i2 = i == 130 ? 1 : 0;
        int height = getHeight();
        this.f578b.top = 0;
        this.f578b.bottom = height;
        if (i2 != 0) {
            i2 = getChildCount();
            if (i2 > 0) {
                this.f578b.bottom = getChildAt(i2 - 1).getBottom() + getPaddingBottom();
                this.f578b.top = this.f578b.bottom - height;
            }
        }
        return m1545a(i, this.f578b.top, this.f578b.bottom);
    }

    private boolean m1545a(int i, int i2, int i3) {
        boolean z = false;
        int height = getHeight();
        int scrollY = getScrollY();
        int i4 = scrollY + height;
        boolean z2 = i == 33;
        View a = m1542a(z2, i2, i3);
        if (a == null) {
            a = this;
        }
        if (i2 < scrollY || i3 > i4) {
            m1557e(z2 ? i2 - scrollY : i3 - i4);
            z = true;
        }
        if (a != findFocus()) {
            a.requestFocus(i);
        }
        return z;
    }

    public boolean m1568c(int i) {
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !m1548a(findNextFocus, maxScrollAmount, getHeight())) {
            if (i == 33 && getScrollY() < maxScrollAmount) {
                maxScrollAmount = getScrollY();
            } else if (i == 130 && getChildCount() > 0) {
                int bottom = getChildAt(0).getBottom();
                int scrollY = (getScrollY() + getHeight()) - getPaddingBottom();
                if (bottom - scrollY < maxScrollAmount) {
                    maxScrollAmount = bottom - scrollY;
                }
            }
            if (maxScrollAmount == 0) {
                return false;
            }
            if (i != 130) {
                maxScrollAmount = -maxScrollAmount;
            }
            m1557e(maxScrollAmount);
        } else {
            findNextFocus.getDrawingRect(this.f578b);
            offsetDescendantRectToMyCoords(findNextFocus, this.f578b);
            m1557e(m1561a(this.f578b));
            findNextFocus.requestFocus(i);
        }
        if (findFocus != null && findFocus.isFocused() && m1547a(findFocus)) {
            int descendantFocusability = getDescendantFocusability();
            setDescendantFocusability(131072);
            requestFocus();
            setDescendantFocusability(descendantFocusability);
        }
        return true;
    }

    private boolean m1547a(View view) {
        return !m1548a(view, 0, getHeight());
    }

    private boolean m1548a(View view, int i, int i2) {
        view.getDrawingRect(this.f578b);
        offsetDescendantRectToMyCoords(view, this.f578b);
        return this.f578b.bottom + i >= getScrollY() && this.f578b.top - i <= getScrollY() + i2;
    }

    private void m1557e(int i) {
        if (i == 0) {
            return;
        }
        if (this.f589m) {
            m1562a(0, i);
        } else {
            scrollBy(0, i);
        }
    }

    public final void m1562a(int i, int i2) {
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.f577a > 250) {
                int max = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
                int scrollY = getScrollY();
                this.f579c.m1752a(getScrollX(), scrollY, 0, Math.max(0, Math.min(scrollY + i2, max)) - scrollY);
                af.m1186b(this);
            } else {
                if (!this.f579c.m1755a()) {
                    this.f579c.m1762g();
                }
                scrollBy(i, i2);
            }
            this.f577a = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    public final void m1566b(int i, int i2) {
        m1562a(i - getScrollX(), i2 - getScrollY());
    }

    public int computeVerticalScrollRange() {
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (getChildCount() == 0) {
            return height;
        }
        int bottom = getChildAt(0).getBottom();
        int scrollY = getScrollY();
        height = Math.max(0, bottom - height);
        if (scrollY < 0) {
            return bottom - scrollY;
        }
        if (scrollY > height) {
            return bottom + (scrollY - height);
        }
        return bottom;
    }

    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    protected void measureChild(View view, int i, int i2) {
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), MeasureSpec.makeMeasureSpec(0, 0));
    }

    protected void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        view.measure(getChildMeasureSpec(i, (((getPaddingLeft() + getPaddingRight()) + marginLayoutParams.leftMargin) + marginLayoutParams.rightMargin) + i2, marginLayoutParams.width), MeasureSpec.makeMeasureSpec(marginLayoutParams.bottomMargin + marginLayoutParams.topMargin, 0));
    }

    public void computeScroll() {
        if (this.f579c.m1761f()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int b = this.f579c.m1757b();
            int c = this.f579c.m1758c();
            if (scrollX != b || scrollY != c) {
                int scrollRange = getScrollRange();
                int a = af.m1173a(this);
                int i = (a == 0 || (a == 1 && scrollRange > 0)) ? 1 : 0;
                m1564a(b - scrollX, c - scrollY, scrollX, scrollY, 0, scrollRange, 0, 0, false);
                if (i != 0) {
                    m1560g();
                    if (c <= 0 && scrollY > 0) {
                        this.f580d.m1675a((int) this.f579c.m1760e());
                    } else if (c >= scrollRange && scrollY < scrollRange) {
                        this.f581e.m1675a((int) this.f579c.m1760e());
                    }
                }
            }
        }
    }

    private void m1551b(View view) {
        view.getDrawingRect(this.f578b);
        offsetDescendantRectToMyCoords(view, this.f578b);
        int a = m1561a(this.f578b);
        if (a != 0) {
            scrollBy(0, a);
        }
    }

    private boolean m1546a(Rect rect, boolean z) {
        int a = m1561a(rect);
        boolean z2 = a != 0;
        if (z2) {
            if (z) {
                scrollBy(0, a);
            } else {
                m1562a(0, a);
            }
        }
        return z2;
    }

    protected int m1561a(Rect rect) {
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        if (rect.bottom < getChildAt(0).getHeight()) {
            i -= verticalFadingEdgeLength;
        }
        if (rect.bottom > i && rect.top > scrollY) {
            if (rect.height() > height) {
                scrollY = (rect.top - scrollY) + 0;
            } else {
                scrollY = (rect.bottom - i) + 0;
            }
            scrollY = Math.min(scrollY, getChildAt(0).getBottom() - i);
        } else if (rect.top >= scrollY || rect.bottom >= i) {
            scrollY = 0;
        } else {
            if (rect.height() > height) {
                scrollY = 0 - (i - rect.bottom);
            } else {
                scrollY = 0 - (scrollY - rect.top);
            }
            scrollY = Math.max(scrollY, -getScrollY());
        }
        return scrollY;
    }

    public void requestChildFocus(View view, View view2) {
        if (this.f583g) {
            this.f585i = view2;
        } else {
            m1551b(view2);
        }
        super.requestChildFocus(view, view2);
    }

    protected boolean onRequestFocusInDescendants(int i, Rect rect) {
        if (i == 2) {
            i = 130;
        } else if (i == 1) {
            i = 33;
        }
        View findNextFocus = rect == null ? FocusFinder.getInstance().findNextFocus(this, null, i) : FocusFinder.getInstance().findNextFocusFromRect(this, rect, i);
        if (findNextFocus == null || m1547a(findNextFocus)) {
            return false;
        }
        return findNextFocus.requestFocus(i, rect);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return m1546a(rect, z);
    }

    public void requestLayout() {
        this.f583g = true;
        super.requestLayout();
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f583g = false;
        if (this.f585i != null && m1549a(this.f585i, (View) this)) {
            m1551b(this.f585i);
        }
        this.f585i = null;
        if (!this.f584h) {
            if (this.f597u != null) {
                scrollTo(getScrollX(), this.f597u.f573a);
                this.f597u = null;
            }
            int max = Math.max(0, (getChildCount() > 0 ? getChildAt(0).getMeasuredHeight() : 0) - (((i4 - i2) - getPaddingBottom()) - getPaddingTop()));
            if (getScrollY() > max) {
                scrollTo(getScrollX(), max);
            } else if (getScrollY() < 0) {
                scrollTo(getScrollX(), 0);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.f584h = true;
    }

    public void onAttachedToWindow() {
        this.f584h = false;
    }

    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && m1548a(findFocus, 0, i4)) {
            findFocus.getDrawingRect(this.f578b);
            offsetDescendantRectToMyCoords(findFocus, this.f578b);
            m1557e(m1561a(this.f578b));
        }
    }

    private static boolean m1549a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        boolean z = (parent instanceof ViewGroup) && m1549a((View) parent, view2);
        return z;
    }

    public void m1569d(int i) {
        if (getChildCount() > 0) {
            int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
            int height2 = getChildAt(0).getHeight();
            this.f579c.m1754a(getScrollX(), getScrollY(), 0, i, 0, 0, 0, Math.max(0, height2 - height), 0, height / 2);
            af.m1186b(this);
        }
    }

    private void m1559f(int i) {
        int scrollY = getScrollY();
        boolean z = (scrollY > 0 || i > 0) && (scrollY < getScrollRange() || i < 0);
        if (!dispatchNestedPreFling(0.0f, (float) i)) {
            dispatchNestedFling(0.0f, (float) i, z);
            if (z) {
                m1569d(i);
            }
        }
    }

    private void m1558f() {
        this.f586j = false;
        m1556e();
        stopNestedScroll();
        if (this.f580d != null) {
            this.f580d.m1677b();
            this.f581e.m1677b();
        }
    }

    public void scrollTo(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int b = m1550b(i, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int b2 = m1550b(i2, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (b != getScrollX() || b2 != getScrollY()) {
                super.scrollTo(b, b2);
            }
        }
    }

    private void m1560g() {
        if (af.m1173a(this) == 2) {
            this.f580d = null;
            this.f581e = null;
        } else if (this.f580d == null) {
            Context context = getContext();
            this.f580d = new EdgeEffectCompat(context);
            this.f581e = new EdgeEffectCompat(context);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f580d != null) {
            int save;
            int width;
            int scrollY = getScrollY();
            if (!this.f580d.m1673a()) {
                save = canvas.save();
                width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                canvas.translate((float) getPaddingLeft(), (float) Math.min(0, scrollY));
                this.f580d.m1672a(width, getHeight());
                if (this.f580d.m1676a(canvas)) {
                    af.m1186b(this);
                }
                canvas.restoreToCount(save);
            }
            if (!this.f581e.m1673a()) {
                save = canvas.save();
                width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height = getHeight();
                canvas.translate((float) ((-width) + getPaddingLeft()), (float) (Math.max(getScrollRange(), scrollY) + height));
                canvas.rotate(180.0f, (float) width, 0.0f);
                this.f581e.m1672a(width, height);
                if (this.f581e.m1676a(canvas)) {
                    af.m1186b(this);
                }
                canvas.restoreToCount(save);
            }
        }
    }

    private static int m1550b(int i, int i2, int i3) {
        if (i2 >= i3 || i < 0) {
            return 0;
        }
        if (i2 + i > i3) {
            return i3 - i2;
        }
        return i;
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.f597u = savedState;
            requestLayout();
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable savedState = new SavedState(super.onSaveInstanceState());
        savedState.f573a = getScrollY();
        return savedState;
    }
}
