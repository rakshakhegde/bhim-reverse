package android.support.v4.widget;

import android.os.Build.VERSION;
import android.widget.ListView;

/* renamed from: android.support.v4.widget.l */
public final class ListViewCompat {
    public static void m1689a(ListView listView, int i) {
        if (VERSION.SDK_INT >= 19) {
            ListViewCompatKitKat.m1691a(listView, i);
        } else {
            ListViewCompatDonut.m1690a(listView, i);
        }
    }
}
