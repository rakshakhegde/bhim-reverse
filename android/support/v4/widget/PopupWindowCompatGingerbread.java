package android.support.v4.widget;

import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* renamed from: android.support.v4.widget.r */
class PopupWindowCompatGingerbread {
    private static Method f651a;
    private static boolean f652b;

    static void m1709a(PopupWindow popupWindow, int i) {
        if (!f652b) {
            try {
                f651a = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", new Class[]{Integer.TYPE});
                f651a.setAccessible(true);
            } catch (Exception e) {
            }
            f652b = true;
        }
        if (f651a != null) {
            try {
                f651a.invoke(popupWindow, new Object[]{Integer.valueOf(i)});
            } catch (Exception e2) {
            }
        }
    }
}
