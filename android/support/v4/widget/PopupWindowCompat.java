package android.support.v4.widget;

import android.os.Build.VERSION;
import android.view.View;
import android.widget.PopupWindow;

/* renamed from: android.support.v4.widget.o */
public final class PopupWindowCompat {
    static final PopupWindowCompat f649a;

    /* renamed from: android.support.v4.widget.o.f */
    interface PopupWindowCompat {
        void m1692a(PopupWindow popupWindow, int i);

        void m1693a(PopupWindow popupWindow, View view, int i, int i2, int i3);

        void m1694a(PopupWindow popupWindow, boolean z);
    }

    /* renamed from: android.support.v4.widget.o.c */
    static class PopupWindowCompat implements PopupWindowCompat {
        PopupWindowCompat() {
        }

        public void m1696a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            popupWindow.showAsDropDown(view, i, i2);
        }

        public void m1697a(PopupWindow popupWindow, boolean z) {
        }

        public void m1695a(PopupWindow popupWindow, int i) {
        }
    }

    /* renamed from: android.support.v4.widget.o.d */
    static class PopupWindowCompat extends PopupWindowCompat {
        PopupWindowCompat() {
        }

        public void m1698a(PopupWindow popupWindow, int i) {
            PopupWindowCompatGingerbread.m1709a(popupWindow, i);
        }
    }

    /* renamed from: android.support.v4.widget.o.e */
    static class PopupWindowCompat extends PopupWindowCompat {
        PopupWindowCompat() {
        }

        public void m1699a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            PopupWindowCompatKitKat.m1710a(popupWindow, view, i, i2, i3);
        }
    }

    /* renamed from: android.support.v4.widget.o.a */
    static class PopupWindowCompat extends PopupWindowCompat {
        PopupWindowCompat() {
        }

        public void m1700a(PopupWindow popupWindow, boolean z) {
            PopupWindowCompatApi21.m1706a(popupWindow, z);
        }
    }

    /* renamed from: android.support.v4.widget.o.b */
    static class PopupWindowCompat extends PopupWindowCompat {
        PopupWindowCompat() {
        }

        public void m1702a(PopupWindow popupWindow, boolean z) {
            PopupWindowCompatApi23.m1708a(popupWindow, z);
        }

        public void m1701a(PopupWindow popupWindow, int i) {
            PopupWindowCompatApi23.m1707a(popupWindow, i);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 23) {
            f649a = new PopupWindowCompat();
        } else if (i >= 21) {
            f649a = new PopupWindowCompat();
        } else if (i >= 19) {
            f649a = new PopupWindowCompat();
        } else if (i >= 9) {
            f649a = new PopupWindowCompat();
        } else {
            f649a = new PopupWindowCompat();
        }
    }

    public static void m1704a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        f649a.m1693a(popupWindow, view, i, i2, i3);
    }

    public static void m1705a(PopupWindow popupWindow, boolean z) {
        f649a.m1694a(popupWindow, z);
    }

    public static void m1703a(PopupWindow popupWindow, int i) {
        f649a.m1692a(popupWindow, i);
    }
}
