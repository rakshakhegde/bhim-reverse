package android.support.v4.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/* renamed from: android.support.v4.widget.u */
public final class ScrollerCompat {
    Object f656a;
    ScrollerCompat f657b;

    /* renamed from: android.support.v4.widget.u.a */
    interface ScrollerCompat {
        Object m1713a(Context context, Interpolator interpolator);

        void m1714a(Object obj, int i, int i2, int i3, int i4);

        void m1715a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8);

        void m1716a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10);

        boolean m1717a(Object obj);

        boolean m1718a(Object obj, int i, int i2, int i3, int i4, int i5, int i6);

        int m1719b(Object obj);

        int m1720c(Object obj);

        float m1721d(Object obj);

        boolean m1722e(Object obj);

        void m1723f(Object obj);

        int m1724g(Object obj);
    }

    /* renamed from: android.support.v4.widget.u.b */
    static class ScrollerCompat implements ScrollerCompat {
        ScrollerCompat() {
        }

        public Object m1725a(Context context, Interpolator interpolator) {
            return interpolator != null ? new Scroller(context, interpolator) : new Scroller(context);
        }

        public boolean m1729a(Object obj) {
            return ((Scroller) obj).isFinished();
        }

        public int m1731b(Object obj) {
            return ((Scroller) obj).getCurrX();
        }

        public int m1732c(Object obj) {
            return ((Scroller) obj).getCurrY();
        }

        public float m1733d(Object obj) {
            return 0.0f;
        }

        public boolean m1734e(Object obj) {
            return ((Scroller) obj).computeScrollOffset();
        }

        public void m1726a(Object obj, int i, int i2, int i3, int i4) {
            ((Scroller) obj).startScroll(i, i2, i3, i4);
        }

        public void m1727a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            ((Scroller) obj).fling(i, i2, i3, i4, i5, i6, i7, i8);
        }

        public void m1728a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
            ((Scroller) obj).fling(i, i2, i3, i4, i5, i6, i7, i8);
        }

        public void m1735f(Object obj) {
            ((Scroller) obj).abortAnimation();
        }

        public int m1736g(Object obj) {
            return ((Scroller) obj).getFinalY();
        }

        public boolean m1730a(Object obj, int i, int i2, int i3, int i4, int i5, int i6) {
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.u.c */
    static class ScrollerCompat implements ScrollerCompat {
        ScrollerCompat() {
        }

        public Object m1737a(Context context, Interpolator interpolator) {
            return ScrollerCompatGingerbread.m1763a(context, interpolator);
        }

        public boolean m1741a(Object obj) {
            return ScrollerCompatGingerbread.m1767a(obj);
        }

        public int m1743b(Object obj) {
            return ScrollerCompatGingerbread.m1769b(obj);
        }

        public int m1744c(Object obj) {
            return ScrollerCompatGingerbread.m1770c(obj);
        }

        public float m1745d(Object obj) {
            return 0.0f;
        }

        public boolean m1746e(Object obj) {
            return ScrollerCompatGingerbread.m1771d(obj);
        }

        public void m1738a(Object obj, int i, int i2, int i3, int i4) {
            ScrollerCompatGingerbread.m1764a(obj, i, i2, i3, i4);
        }

        public void m1739a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            ScrollerCompatGingerbread.m1765a(obj, i, i2, i3, i4, i5, i6, i7, i8);
        }

        public void m1740a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
            ScrollerCompatGingerbread.m1766a(obj, i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
        }

        public void m1747f(Object obj) {
            ScrollerCompatGingerbread.m1772e(obj);
        }

        public int m1748g(Object obj) {
            return ScrollerCompatGingerbread.m1773f(obj);
        }

        public boolean m1742a(Object obj, int i, int i2, int i3, int i4, int i5, int i6) {
            return ScrollerCompatGingerbread.m1768a(obj, i, i2, i3, i4, i5, i6);
        }
    }

    /* renamed from: android.support.v4.widget.u.d */
    static class ScrollerCompat extends ScrollerCompat {
        ScrollerCompat() {
        }

        public float m1749d(Object obj) {
            return ScrollerCompatIcs.m1774a(obj);
        }
    }

    public static ScrollerCompat m1750a(Context context) {
        return ScrollerCompat.m1751a(context, null);
    }

    public static ScrollerCompat m1751a(Context context, Interpolator interpolator) {
        return new ScrollerCompat(VERSION.SDK_INT, context, interpolator);
    }

    private ScrollerCompat(int i, Context context, Interpolator interpolator) {
        if (i >= 14) {
            this.f657b = new ScrollerCompat();
        } else if (i >= 9) {
            this.f657b = new ScrollerCompat();
        } else {
            this.f657b = new ScrollerCompat();
        }
        this.f656a = this.f657b.m1713a(context, interpolator);
    }

    public boolean m1755a() {
        return this.f657b.m1717a(this.f656a);
    }

    public int m1757b() {
        return this.f657b.m1719b(this.f656a);
    }

    public int m1758c() {
        return this.f657b.m1720c(this.f656a);
    }

    public int m1759d() {
        return this.f657b.m1724g(this.f656a);
    }

    public float m1760e() {
        return this.f657b.m1721d(this.f656a);
    }

    public boolean m1761f() {
        return this.f657b.m1722e(this.f656a);
    }

    public void m1752a(int i, int i2, int i3, int i4) {
        this.f657b.m1714a(this.f656a, i, i2, i3, i4);
    }

    public void m1753a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f657b.m1715a(this.f656a, i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void m1754a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f657b.m1716a(this.f656a, i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public boolean m1756a(int i, int i2, int i3, int i4, int i5, int i6) {
        return this.f657b.m1718a(this.f656a, i, i2, i3, i4, i5, i6);
    }

    public void m1762g() {
        this.f657b.m1723f(this.f656a);
    }
}
