package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.p006f.MotionEventCompat;
import android.support.v4.p006f.af;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v4.widget.a */
public abstract class AutoScrollHelper implements OnTouchListener {
    private static final int f613r;
    private final AutoScrollHelper f614a;
    private final Interpolator f615b;
    private final View f616c;
    private Runnable f617d;
    private float[] f618e;
    private float[] f619f;
    private int f620g;
    private int f621h;
    private float[] f622i;
    private float[] f623j;
    private float[] f624k;
    private boolean f625l;
    private boolean f626m;
    private boolean f627n;
    private boolean f628o;
    private boolean f629p;
    private boolean f630q;

    /* renamed from: android.support.v4.widget.a.a */
    private static class AutoScrollHelper {
        private int f601a;
        private int f602b;
        private float f603c;
        private float f604d;
        private long f605e;
        private long f606f;
        private int f607g;
        private int f608h;
        private long f609i;
        private float f610j;
        private int f611k;

        public AutoScrollHelper() {
            this.f605e = Long.MIN_VALUE;
            this.f609i = -1;
            this.f606f = 0;
            this.f607g = 0;
            this.f608h = 0;
        }

        public void m1575a(int i) {
            this.f601a = i;
        }

        public void m1577b(int i) {
            this.f602b = i;
        }

        public void m1573a() {
            this.f605e = AnimationUtils.currentAnimationTimeMillis();
            this.f609i = -1;
            this.f606f = this.f605e;
            this.f610j = 0.5f;
            this.f607g = 0;
            this.f608h = 0;
        }

        public void m1576b() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f611k = AutoScrollHelper.m1592b((int) (currentAnimationTimeMillis - this.f605e), 0, this.f602b);
            this.f610j = m1572a(currentAnimationTimeMillis);
            this.f609i = currentAnimationTimeMillis;
        }

        public boolean m1578c() {
            return this.f609i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.f609i + ((long) this.f611k);
        }

        private float m1572a(long j) {
            if (j < this.f605e) {
                return 0.0f;
            }
            if (this.f609i < 0 || j < this.f609i) {
                return AutoScrollHelper.m1591b(((float) (j - this.f605e)) / ((float) this.f601a), 0.0f, 1.0f) * 0.5f;
            }
            long j2 = j - this.f609i;
            return (AutoScrollHelper.m1591b(((float) j2) / ((float) this.f611k), 0.0f, 1.0f) * this.f610j) + (1.0f - this.f610j);
        }

        private float m1571a(float f) {
            return ((-4.0f * f) * f) + (4.0f * f);
        }

        public void m1579d() {
            if (this.f606f == 0) {
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            float a = m1571a(m1572a(currentAnimationTimeMillis));
            long j = currentAnimationTimeMillis - this.f606f;
            this.f606f = currentAnimationTimeMillis;
            this.f607g = (int) ((((float) j) * a) * this.f603c);
            this.f608h = (int) ((((float) j) * a) * this.f604d);
        }

        public void m1574a(float f, float f2) {
            this.f603c = f;
            this.f604d = f2;
        }

        public int m1580e() {
            return (int) (this.f603c / Math.abs(this.f603c));
        }

        public int m1581f() {
            return (int) (this.f604d / Math.abs(this.f604d));
        }

        public int m1582g() {
            return this.f607g;
        }

        public int m1583h() {
            return this.f608h;
        }
    }

    /* renamed from: android.support.v4.widget.a.b */
    private class AutoScrollHelper implements Runnable {
        final /* synthetic */ AutoScrollHelper f612a;

        private AutoScrollHelper(AutoScrollHelper autoScrollHelper) {
            this.f612a = autoScrollHelper;
        }

        public void run() {
            if (this.f612a.f628o) {
                if (this.f612a.f626m) {
                    this.f612a.f626m = false;
                    this.f612a.f614a.m1573a();
                }
                AutoScrollHelper c = this.f612a.f614a;
                if (c.m1578c() || !this.f612a.m1588a()) {
                    this.f612a.f628o = false;
                    return;
                }
                if (this.f612a.f627n) {
                    this.f612a.f627n = false;
                    this.f612a.m1599d();
                }
                c.m1579d();
                this.f612a.m1608a(c.m1582g(), c.m1583h());
                af.m1182a(this.f612a.f616c, (Runnable) this);
            }
        }
    }

    public abstract void m1608a(int i, int i2);

    public abstract boolean m1616e(int i);

    public abstract boolean m1617f(int i);

    static {
        f613r = ViewConfiguration.getTapTimeout();
    }

    public AutoScrollHelper(View view) {
        this.f614a = new AutoScrollHelper();
        this.f615b = new AccelerateInterpolator();
        this.f618e = new float[]{0.0f, 0.0f};
        this.f619f = new float[]{Float.MAX_VALUE, Float.MAX_VALUE};
        this.f622i = new float[]{0.0f, 0.0f};
        this.f623j = new float[]{0.0f, 0.0f};
        this.f624k = new float[]{Float.MAX_VALUE, Float.MAX_VALUE};
        this.f616c = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i = (int) ((1575.0f * displayMetrics.density) + 0.5f);
        int i2 = (int) ((displayMetrics.density * 315.0f) + 0.5f);
        m1605a((float) i, (float) i);
        m1609b((float) i2, (float) i2);
        m1606a(1);
        m1615e(Float.MAX_VALUE, Float.MAX_VALUE);
        m1613d(0.2f, 0.2f);
        m1611c(1.0f, 1.0f);
        m1610b(f613r);
        m1612c(500);
        m1614d(500);
    }

    public AutoScrollHelper m1607a(boolean z) {
        if (this.f629p && !z) {
            m1597c();
        }
        this.f629p = z;
        return this;
    }

    public AutoScrollHelper m1605a(float f, float f2) {
        this.f624k[0] = f / 1000.0f;
        this.f624k[1] = f2 / 1000.0f;
        return this;
    }

    public AutoScrollHelper m1609b(float f, float f2) {
        this.f623j[0] = f / 1000.0f;
        this.f623j[1] = f2 / 1000.0f;
        return this;
    }

    public AutoScrollHelper m1611c(float f, float f2) {
        this.f622i[0] = f / 1000.0f;
        this.f622i[1] = f2 / 1000.0f;
        return this;
    }

    public AutoScrollHelper m1606a(int i) {
        this.f620g = i;
        return this;
    }

    public AutoScrollHelper m1613d(float f, float f2) {
        this.f618e[0] = f;
        this.f618e[1] = f2;
        return this;
    }

    public AutoScrollHelper m1615e(float f, float f2) {
        this.f619f[0] = f;
        this.f619f[1] = f2;
        return this;
    }

    public AutoScrollHelper m1610b(int i) {
        this.f621h = i;
        return this;
    }

    public AutoScrollHelper m1612c(int i) {
        this.f614a.m1575a(i);
        return this;
    }

    public AutoScrollHelper m1614d(int i) {
        this.f614a.m1577b(i);
        return this;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z = true;
        if (!this.f629p) {
            return false;
        }
        switch (MotionEventCompat.m1507a(motionEvent)) {
            case R.View_android_theme /*0*/:
                this.f627n = true;
                this.f625l = false;
                break;
            case R.View_android_focusable /*1*/:
            case R.View_paddingEnd /*3*/:
                m1597c();
                break;
            case R.View_paddingStart /*2*/:
                break;
        }
        this.f614a.m1574a(m1586a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f616c.getWidth()), m1586a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f616c.getHeight()));
        if (!this.f628o && m1588a()) {
            m1593b();
        }
        if (!(this.f630q && this.f628o)) {
            z = false;
        }
        return z;
    }

    private boolean m1588a() {
        AutoScrollHelper autoScrollHelper = this.f614a;
        int f = autoScrollHelper.m1581f();
        int e = autoScrollHelper.m1580e();
        return (f != 0 && m1617f(f)) || (e != 0 && m1616e(e));
    }

    private void m1593b() {
        if (this.f617d == null) {
            this.f617d = new AutoScrollHelper();
        }
        this.f628o = true;
        this.f626m = true;
        if (this.f625l || this.f621h <= 0) {
            this.f617d.run();
        } else {
            af.m1183a(this.f616c, this.f617d, (long) this.f621h);
        }
        this.f625l = true;
    }

    private void m1597c() {
        if (this.f626m) {
            this.f628o = false;
        } else {
            this.f614a.m1576b();
        }
    }

    private float m1586a(int i, float f, float f2, float f3) {
        float a = m1585a(this.f618e[i], f2, this.f619f[i], f);
        if (a == 0.0f) {
            return 0.0f;
        }
        float f4 = this.f622i[i];
        float f5 = this.f623j[i];
        float f6 = this.f624k[i];
        f4 *= f3;
        if (a > 0.0f) {
            return AutoScrollHelper.m1591b(a * f4, f5, f6);
        }
        return -AutoScrollHelper.m1591b((-a) * f4, f5, f6);
    }

    private float m1585a(float f, float f2, float f3, float f4) {
        float f5;
        float b = AutoScrollHelper.m1591b(f * f2, 0.0f, f3);
        b = m1602f(f2 - f4, b) - m1602f(f4, b);
        if (b < 0.0f) {
            f5 = -this.f615b.getInterpolation(-b);
        } else if (b <= 0.0f) {
            return 0.0f;
        } else {
            f5 = this.f615b.getInterpolation(b);
        }
        return AutoScrollHelper.m1591b(f5, -1.0f, 1.0f);
    }

    private float m1602f(float f, float f2) {
        if (f2 == 0.0f) {
            return 0.0f;
        }
        switch (this.f620g) {
            case R.View_android_theme /*0*/:
            case R.View_android_focusable /*1*/:
                if (f >= f2) {
                    return 0.0f;
                }
                if (f >= 0.0f) {
                    return 1.0f - (f / f2);
                }
                if (this.f628o && this.f620g == 1) {
                    return 1.0f;
                }
                return 0.0f;
            case R.View_paddingStart /*2*/:
                if (f < 0.0f) {
                    return f / (-f2);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    private static int m1592b(int i, int i2, int i3) {
        if (i > i3) {
            return i3;
        }
        if (i < i2) {
            return i2;
        }
        return i;
    }

    private static float m1591b(float f, float f2, float f3) {
        if (f > f3) {
            return f3;
        }
        if (f < f2) {
            return f2;
        }
        return f;
    }

    private void m1599d() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.f616c.onTouchEvent(obtain);
        obtain.recycle();
    }
}
