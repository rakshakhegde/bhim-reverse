package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;

/* renamed from: android.support.v4.widget.c */
class CompoundButtonCompatApi23 {
    static Drawable m1630a(CompoundButton compoundButton) {
        return compoundButton.getButtonDrawable();
    }
}
