package android.support.v4.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.v4.widget.CursorFilter.CursorFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import com.crashlytics.android.core.BuildConfig;

/* renamed from: android.support.v4.widget.f */
public abstract class CursorAdapter extends BaseAdapter implements CursorFilter, Filterable {
    protected boolean f636a;
    protected boolean f637b;
    protected Cursor f638c;
    protected Context f639d;
    protected int f640e;
    protected CursorAdapter f641f;
    protected DataSetObserver f642g;
    protected CursorFilter f643h;
    protected FilterQueryProvider f644i;

    /* renamed from: android.support.v4.widget.f.a */
    private class CursorAdapter extends ContentObserver {
        final /* synthetic */ CursorAdapter f634a;

        public CursorAdapter(CursorAdapter cursorAdapter) {
            this.f634a = cursorAdapter;
            super(new Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            this.f634a.m1648b();
        }
    }

    /* renamed from: android.support.v4.widget.f.b */
    private class CursorAdapter extends DataSetObserver {
        final /* synthetic */ CursorAdapter f635a;

        private CursorAdapter(CursorAdapter cursorAdapter) {
            this.f635a = cursorAdapter;
        }

        public void onChanged() {
            this.f635a.f636a = true;
            this.f635a.notifyDataSetChanged();
        }

        public void onInvalidated() {
            this.f635a.f636a = false;
            this.f635a.notifyDataSetInvalidated();
        }
    }

    public abstract View m1642a(Context context, Cursor cursor, ViewGroup viewGroup);

    public abstract void m1645a(View view, Context context, Cursor cursor);

    public CursorAdapter(Context context, Cursor cursor, boolean z) {
        m1643a(context, cursor, z ? 1 : 2);
    }

    void m1643a(Context context, Cursor cursor, int i) {
        boolean z = true;
        if ((i & 1) == 1) {
            i |= 2;
            this.f637b = true;
        } else {
            this.f637b = false;
        }
        if (cursor == null) {
            z = false;
        }
        this.f638c = cursor;
        this.f636a = z;
        this.f639d = context;
        this.f640e = z ? cursor.getColumnIndexOrThrow("_id") : -1;
        if ((i & 2) == 2) {
            this.f641f = new CursorAdapter(this);
            this.f642g = new CursorAdapter();
        } else {
            this.f641f = null;
            this.f642g = null;
        }
        if (z) {
            if (this.f641f != null) {
                cursor.registerContentObserver(this.f641f);
            }
            if (this.f642g != null) {
                cursor.registerDataSetObserver(this.f642g);
            }
        }
    }

    public Cursor m1640a() {
        return this.f638c;
    }

    public int getCount() {
        if (!this.f636a || this.f638c == null) {
            return 0;
        }
        return this.f638c.getCount();
    }

    public Object getItem(int i) {
        if (!this.f636a || this.f638c == null) {
            return null;
        }
        this.f638c.moveToPosition(i);
        return this.f638c;
    }

    public long getItemId(int i) {
        if (this.f636a && this.f638c != null && this.f638c.moveToPosition(i)) {
            return this.f638c.getLong(this.f640e);
        }
        return 0;
    }

    public boolean hasStableIds() {
        return true;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (!this.f636a) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.f638c.moveToPosition(i)) {
            if (view == null) {
                view = m1642a(this.f639d, this.f638c, viewGroup);
            }
            m1645a(view, this.f639d, this.f638c);
            return view;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i);
        }
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (!this.f636a) {
            return null;
        }
        this.f638c.moveToPosition(i);
        if (view == null) {
            view = m1647b(this.f639d, this.f638c, viewGroup);
        }
        m1645a(view, this.f639d, this.f638c);
        return view;
    }

    public View m1647b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return m1642a(context, cursor, viewGroup);
    }

    public void m1644a(Cursor cursor) {
        Cursor b = m1646b(cursor);
        if (b != null) {
            b.close();
        }
    }

    public Cursor m1646b(Cursor cursor) {
        if (cursor == this.f638c) {
            return null;
        }
        Cursor cursor2 = this.f638c;
        if (cursor2 != null) {
            if (this.f641f != null) {
                cursor2.unregisterContentObserver(this.f641f);
            }
            if (this.f642g != null) {
                cursor2.unregisterDataSetObserver(this.f642g);
            }
        }
        this.f638c = cursor;
        if (cursor != null) {
            if (this.f641f != null) {
                cursor.registerContentObserver(this.f641f);
            }
            if (this.f642g != null) {
                cursor.registerDataSetObserver(this.f642g);
            }
            this.f640e = cursor.getColumnIndexOrThrow("_id");
            this.f636a = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.f640e = -1;
        this.f636a = false;
        notifyDataSetInvalidated();
        return cursor2;
    }

    public CharSequence m1649c(Cursor cursor) {
        return cursor == null ? BuildConfig.FLAVOR : cursor.toString();
    }

    public Cursor m1641a(CharSequence charSequence) {
        if (this.f644i != null) {
            return this.f644i.runQuery(charSequence);
        }
        return this.f638c;
    }

    public Filter getFilter() {
        if (this.f643h == null) {
            this.f643h = new CursorFilter(this);
        }
        return this.f643h;
    }

    protected void m1648b() {
        if (this.f637b && this.f638c != null && !this.f638c.isClosed()) {
            this.f636a = this.f638c.requery();
        }
    }
}
