package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.widget.CompoundButton;

/* renamed from: android.support.v4.widget.b */
public final class CompoundButtonCompat {
    private static final CompoundButtonCompat f631a;

    /* renamed from: android.support.v4.widget.b.c */
    interface CompoundButtonCompat {
        Drawable m1618a(CompoundButton compoundButton);

        void m1619a(CompoundButton compoundButton, ColorStateList colorStateList);

        void m1620a(CompoundButton compoundButton, Mode mode);
    }

    /* renamed from: android.support.v4.widget.b.b */
    static class CompoundButtonCompat implements CompoundButtonCompat {
        CompoundButtonCompat() {
        }

        public void m1622a(CompoundButton compoundButton, ColorStateList colorStateList) {
            CompoundButtonCompatDonut.m1632a(compoundButton, colorStateList);
        }

        public void m1623a(CompoundButton compoundButton, Mode mode) {
            CompoundButtonCompatDonut.m1633a(compoundButton, mode);
        }

        public Drawable m1621a(CompoundButton compoundButton) {
            return CompoundButtonCompatDonut.m1631a(compoundButton);
        }
    }

    /* renamed from: android.support.v4.widget.b.d */
    static class CompoundButtonCompat extends CompoundButtonCompat {
        CompoundButtonCompat() {
        }

        public void m1624a(CompoundButton compoundButton, ColorStateList colorStateList) {
            CompoundButtonCompatLollipop.m1634a(compoundButton, colorStateList);
        }

        public void m1625a(CompoundButton compoundButton, Mode mode) {
            CompoundButtonCompatLollipop.m1635a(compoundButton, mode);
        }
    }

    /* renamed from: android.support.v4.widget.b.a */
    static class CompoundButtonCompat extends CompoundButtonCompat {
        CompoundButtonCompat() {
        }

        public Drawable m1626a(CompoundButton compoundButton) {
            return CompoundButtonCompatApi23.m1630a(compoundButton);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 23) {
            f631a = new CompoundButtonCompat();
        } else if (i >= 21) {
            f631a = new CompoundButtonCompat();
        } else {
            f631a = new CompoundButtonCompat();
        }
    }

    public static void m1628a(CompoundButton compoundButton, ColorStateList colorStateList) {
        f631a.m1619a(compoundButton, colorStateList);
    }

    public static void m1629a(CompoundButton compoundButton, Mode mode) {
        f631a.m1620a(compoundButton, mode);
    }

    public static Drawable m1627a(CompoundButton compoundButton) {
        return f631a.m1618a(compoundButton);
    }
}
