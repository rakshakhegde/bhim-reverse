package android.support.v4.widget;

import android.view.View;
import android.widget.PopupWindow;

/* renamed from: android.support.v4.widget.s */
class PopupWindowCompatKitKat {
    public static void m1710a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        popupWindow.showAsDropDown(view, i, i2, i3);
    }
}
