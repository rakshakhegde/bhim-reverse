package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build.VERSION;

/* renamed from: android.support.v4.widget.h */
public final class EdgeEffectCompat {
    private static final EdgeEffectCompat f646b;
    private Object f647a;

    /* renamed from: android.support.v4.widget.h.c */
    interface EdgeEffectCompat {
        Object m1650a(Context context);

        void m1651a(Object obj, int i, int i2);

        boolean m1652a(Object obj);

        boolean m1653a(Object obj, float f, float f2);

        boolean m1654a(Object obj, int i);

        boolean m1655a(Object obj, Canvas canvas);

        boolean m1656b(Object obj);
    }

    /* renamed from: android.support.v4.widget.h.a */
    static class EdgeEffectCompat implements EdgeEffectCompat {
        EdgeEffectCompat() {
        }

        public Object m1657a(Context context) {
            return null;
        }

        public void m1658a(Object obj, int i, int i2) {
        }

        public boolean m1659a(Object obj) {
            return true;
        }

        public boolean m1663b(Object obj) {
            return false;
        }

        public boolean m1661a(Object obj, int i) {
            return false;
        }

        public boolean m1662a(Object obj, Canvas canvas) {
            return false;
        }

        public boolean m1660a(Object obj, float f, float f2) {
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.h.b */
    static class EdgeEffectCompat implements EdgeEffectCompat {
        EdgeEffectCompat() {
        }

        public Object m1664a(Context context) {
            return EdgeEffectCompatIcs.m1678a(context);
        }

        public void m1665a(Object obj, int i, int i2) {
            EdgeEffectCompatIcs.m1679a(obj, i, i2);
        }

        public boolean m1666a(Object obj) {
            return EdgeEffectCompatIcs.m1680a(obj);
        }

        public boolean m1670b(Object obj) {
            return EdgeEffectCompatIcs.m1684b(obj);
        }

        public boolean m1668a(Object obj, int i) {
            return EdgeEffectCompatIcs.m1682a(obj, i);
        }

        public boolean m1669a(Object obj, Canvas canvas) {
            return EdgeEffectCompatIcs.m1683a(obj, canvas);
        }

        public boolean m1667a(Object obj, float f, float f2) {
            return EdgeEffectCompatIcs.m1681a(obj, f);
        }
    }

    /* renamed from: android.support.v4.widget.h.d */
    static class EdgeEffectCompat extends EdgeEffectCompat {
        EdgeEffectCompat() {
        }

        public boolean m1671a(Object obj, float f, float f2) {
            return EdgeEffectCompatLollipop.m1685a(obj, f, f2);
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f646b = new EdgeEffectCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f646b = new EdgeEffectCompat();
        } else {
            f646b = new EdgeEffectCompat();
        }
    }

    public EdgeEffectCompat(Context context) {
        this.f647a = f646b.m1650a(context);
    }

    public void m1672a(int i, int i2) {
        f646b.m1651a(this.f647a, i, i2);
    }

    public boolean m1673a() {
        return f646b.m1652a(this.f647a);
    }

    public boolean m1674a(float f, float f2) {
        return f646b.m1653a(this.f647a, f, f2);
    }

    public boolean m1677b() {
        return f646b.m1656b(this.f647a);
    }

    public boolean m1675a(int i) {
        return f646b.m1654a(this.f647a, i);
    }

    public boolean m1676a(Canvas canvas) {
        return f646b.m1655a(this.f647a, canvas);
    }
}
