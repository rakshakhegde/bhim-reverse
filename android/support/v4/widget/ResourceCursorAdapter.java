package android.support.v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v4.widget.t */
public abstract class ResourceCursorAdapter extends CursorAdapter {
    private int f653j;
    private int f654k;
    private LayoutInflater f655l;

    public ResourceCursorAdapter(Context context, int i, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.f654k = i;
        this.f653j = i;
        this.f655l = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View m1711a(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f655l.inflate(this.f653j, viewGroup, false);
    }

    public View m1712b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f655l.inflate(this.f654k, viewGroup, false);
    }
}
