package android.support.v4.p007c.p008a;

import android.support.v4.p006f.ActionProvider;
import android.support.v4.p006f.MenuItemCompat.MenuItemCompat;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v4.c.a.b */
public interface SupportMenuItem extends MenuItem {
    SupportMenuItem m714a(ActionProvider actionProvider);

    SupportMenuItem m715a(MenuItemCompat menuItemCompat);

    ActionProvider m716a();

    boolean collapseActionView();

    boolean expandActionView();

    View getActionView();

    boolean isActionViewExpanded();

    MenuItem setActionView(int i);

    MenuItem setActionView(View view);

    void setShowAsAction(int i);

    MenuItem setShowAsActionFlags(int i);
}
