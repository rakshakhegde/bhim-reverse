package android.support.v4.p009d;

import android.os.Build.VERSION;
import java.util.Locale;

/* renamed from: android.support.v4.d.a */
public final class ICUCompat {
    private static final ICUCompat f447a;

    /* renamed from: android.support.v4.d.a.a */
    interface ICUCompat {
        String m717a(Locale locale);
    }

    /* renamed from: android.support.v4.d.a.b */
    static class ICUCompat implements ICUCompat {
        ICUCompat() {
        }

        public String m718a(Locale locale) {
            return null;
        }
    }

    /* renamed from: android.support.v4.d.a.c */
    static class ICUCompat implements ICUCompat {
        ICUCompat() {
        }

        public String m719a(Locale locale) {
            return ICUCompatIcs.m724a(locale);
        }
    }

    /* renamed from: android.support.v4.d.a.d */
    static class ICUCompat implements ICUCompat {
        ICUCompat() {
        }

        public String m720a(Locale locale) {
            return ICUCompatApi23.m722a(locale);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 21) {
            f447a = new ICUCompat();
        } else if (i >= 14) {
            f447a = new ICUCompat();
        } else {
            f447a = new ICUCompat();
        }
    }

    public static String m721a(Locale locale) {
        return f447a.m717a(locale);
    }
}
