package android.support.v4.p009d;

import android.text.TextUtils;
import java.util.Locale;

/* renamed from: android.support.v4.d.e */
class TextUtilsCompatJellybeanMr1 {
    public static int m732a(Locale locale) {
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
