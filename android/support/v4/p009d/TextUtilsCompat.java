package android.support.v4.p009d;

import android.os.Build.VERSION;
import com.crashlytics.android.core.BuildConfig;
import java.util.Locale;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v4.d.d */
public final class TextUtilsCompat {
    public static final Locale f451a;
    private static final TextUtilsCompat f452b;
    private static String f453c;
    private static String f454d;

    /* renamed from: android.support.v4.d.d.a */
    private static class TextUtilsCompat {
        private TextUtilsCompat() {
        }

        public int m727a(Locale locale) {
            if (!(locale == null || locale.equals(TextUtilsCompat.f451a))) {
                String a = ICUCompat.m721a(locale);
                if (a == null) {
                    return TextUtilsCompat.m726b(locale);
                }
                if (a.equalsIgnoreCase(TextUtilsCompat.f453c) || a.equalsIgnoreCase(TextUtilsCompat.f454d)) {
                    return 1;
                }
            }
            return 0;
        }

        private static int m726b(Locale locale) {
            switch (Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
                case R.View_android_focusable /*1*/:
                case R.View_paddingStart /*2*/:
                    return 1;
                default:
                    return 0;
            }
        }
    }

    /* renamed from: android.support.v4.d.d.b */
    private static class TextUtilsCompat extends TextUtilsCompat {
        private TextUtilsCompat() {
            super();
        }

        public int m728a(Locale locale) {
            return TextUtilsCompatJellybeanMr1.m732a(locale);
        }
    }

    static {
        if (VERSION.SDK_INT >= 17) {
            f452b = new TextUtilsCompat();
        } else {
            f452b = new TextUtilsCompat();
        }
        f451a = new Locale(BuildConfig.FLAVOR, BuildConfig.FLAVOR);
        f453c = "Arab";
        f454d = "Hebr";
    }

    public static int m729a(Locale locale) {
        return f452b.m727a(locale);
    }
}
