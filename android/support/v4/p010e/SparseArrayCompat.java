package android.support.v4.p010e;

/* renamed from: android.support.v4.e.i */
public class SparseArrayCompat<E> implements Cloneable {
    private static final Object f497a;
    private boolean f498b;
    private int[] f499c;
    private Object[] f500d;
    private int f501e;

    public /* synthetic */ Object clone() {
        return m800a();
    }

    static {
        f497a = new Object();
    }

    public SparseArrayCompat() {
        this(10);
    }

    public SparseArrayCompat(int i) {
        this.f498b = false;
        if (i == 0) {
            this.f499c = ContainerHelpers.f467a;
            this.f500d = ContainerHelpers.f469c;
        } else {
            int a = ContainerHelpers.m773a(i);
            this.f499c = new int[a];
            this.f500d = new Object[a];
        }
        this.f501e = 0;
    }

    public SparseArrayCompat<E> m800a() {
        try {
            SparseArrayCompat<E> sparseArrayCompat = (SparseArrayCompat) super.clone();
            try {
                sparseArrayCompat.f499c = (int[]) this.f499c.clone();
                sparseArrayCompat.f500d = (Object[]) this.f500d.clone();
                return sparseArrayCompat;
            } catch (CloneNotSupportedException e) {
                return sparseArrayCompat;
            }
        } catch (CloneNotSupportedException e2) {
            return null;
        }
    }

    public E m801a(int i) {
        return m802a(i, null);
    }

    public E m802a(int i, E e) {
        int a = ContainerHelpers.m774a(this.f499c, this.f501e, i);
        return (a < 0 || this.f500d[a] == f497a) ? e : this.f500d[a];
    }

    public void m804b(int i) {
        int a = ContainerHelpers.m774a(this.f499c, this.f501e, i);
        if (a >= 0 && this.f500d[a] != f497a) {
            this.f500d[a] = f497a;
            this.f498b = true;
        }
    }

    public void m807c(int i) {
        m804b(i);
    }

    private void m799d() {
        int i = this.f501e;
        int[] iArr = this.f499c;
        Object[] objArr = this.f500d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f497a) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f498b = false;
        this.f501e = i2;
    }

    public void m805b(int i, E e) {
        int a = ContainerHelpers.m774a(this.f499c, this.f501e, i);
        if (a >= 0) {
            this.f500d[a] = e;
            return;
        }
        a ^= -1;
        if (a >= this.f501e || this.f500d[a] != f497a) {
            if (this.f498b && this.f501e >= this.f499c.length) {
                m799d();
                a = ContainerHelpers.m774a(this.f499c, this.f501e, i) ^ -1;
            }
            if (this.f501e >= this.f499c.length) {
                int a2 = ContainerHelpers.m773a(this.f501e + 1);
                Object obj = new int[a2];
                Object obj2 = new Object[a2];
                System.arraycopy(this.f499c, 0, obj, 0, this.f499c.length);
                System.arraycopy(this.f500d, 0, obj2, 0, this.f500d.length);
                this.f499c = obj;
                this.f500d = obj2;
            }
            if (this.f501e - a != 0) {
                System.arraycopy(this.f499c, a, this.f499c, a + 1, this.f501e - a);
                System.arraycopy(this.f500d, a, this.f500d, a + 1, this.f501e - a);
            }
            this.f499c[a] = i;
            this.f500d[a] = e;
            this.f501e++;
            return;
        }
        this.f499c[a] = i;
        this.f500d[a] = e;
    }

    public int m803b() {
        if (this.f498b) {
            m799d();
        }
        return this.f501e;
    }

    public int m808d(int i) {
        if (this.f498b) {
            m799d();
        }
        return this.f499c[i];
    }

    public E m809e(int i) {
        if (this.f498b) {
            m799d();
        }
        return this.f500d[i];
    }

    public int m810f(int i) {
        if (this.f498b) {
            m799d();
        }
        return ContainerHelpers.m774a(this.f499c, this.f501e, i);
    }

    public void m806c() {
        int i = this.f501e;
        Object[] objArr = this.f500d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f501e = 0;
        this.f498b = false;
    }

    public String toString() {
        if (m803b() <= 0) {
            return "{}";
        }
        StringBuilder stringBuilder = new StringBuilder(this.f501e * 28);
        stringBuilder.append('{');
        for (int i = 0; i < this.f501e; i++) {
            if (i > 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(m808d(i));
            stringBuilder.append('=');
            SparseArrayCompat e = m809e(i);
            if (e != this) {
                stringBuilder.append(e);
            } else {
                stringBuilder.append("(this Map)");
            }
        }
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
