package android.support.v4.p010e;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/* renamed from: android.support.v4.e.a */
public class ArrayMap<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    MapCollections<K, V> f466a;

    /* renamed from: android.support.v4.e.a.1 */
    class ArrayMap extends MapCollections<K, V> {
        final /* synthetic */ ArrayMap f458a;

        ArrayMap(ArrayMap arrayMap) {
            this.f458a = arrayMap;
        }

        protected int m751a() {
            return this.f458a.h;
        }

        protected Object m753a(int i, int i2) {
            return this.f458a.g[(i << 1) + i2];
        }

        protected int m752a(Object obj) {
            return this.f458a.m763a(obj);
        }

        protected int m757b(Object obj) {
            return this.f458a.m767b(obj);
        }

        protected Map<K, V> m758b() {
            return this.f458a;
        }

        protected void m756a(K k, V v) {
            this.f458a.put(k, v);
        }

        protected V m754a(int i, V v) {
            return this.f458a.m765a(i, (Object) v);
        }

        protected void m755a(int i) {
            this.f458a.m770d(i);
        }

        protected void m759c() {
            this.f458a.clear();
        }
    }

    public ArrayMap(int i) {
        super(i);
    }

    private MapCollections<K, V> m771b() {
        if (this.f466a == null) {
            this.f466a = new ArrayMap(this);
        }
        return this.f466a;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        m766a(this.h + map.size());
        for (Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public boolean m772a(Collection<?> collection) {
        return MapCollections.m736c(this, collection);
    }

    public Set<Entry<K, V>> entrySet() {
        return m771b().m748d();
    }

    public Set<K> keySet() {
        return m771b().m749e();
    }

    public Collection<V> values() {
        return m771b().m750f();
    }
}
