package android.support.v4.p010e;

import java.util.LinkedHashMap;

/* renamed from: android.support.v4.e.f */
public class LruCache<K, V> {
    private final LinkedHashMap<K, V> f477a;
    private int f478b;
    private int f479c;
    private int f480d;
    private int f481e;
    private int f482f;
    private int f483g;
    private int f484h;

    public LruCache(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        }
        this.f479c = i;
        this.f477a = new LinkedHashMap(0, 0.75f, true);
    }

    public final V m791a(K k) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        synchronized (this) {
            V v = this.f477a.get(k);
            if (v != null) {
                this.f483g++;
                return v;
            }
            this.f484h++;
            V b = m796b(k);
            if (b == null) {
                return null;
            }
            synchronized (this) {
                this.f481e++;
                v = this.f477a.put(k, b);
                if (v != null) {
                    this.f477a.put(k, v);
                } else {
                    this.f478b += m790c(k, b);
                }
            }
            if (v != null) {
                m794a(false, k, b, v);
                return v;
            }
            m793a(this.f479c);
            return b;
        }
    }

    public final V m792a(K k, V v) {
        if (k == null || v == null) {
            throw new NullPointerException("key == null || value == null");
        }
        V put;
        synchronized (this) {
            this.f480d++;
            this.f478b += m790c(k, v);
            put = this.f477a.put(k, v);
            if (put != null) {
                this.f478b -= m790c(k, put);
            }
        }
        if (put != null) {
            m794a(false, k, put, v);
        }
        m793a(this.f479c);
        return put;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m793a(int r5) {
        /*
        r4 = this;
    L_0x0000:
        monitor-enter(r4);
        r0 = r4.f478b;	 Catch:{ all -> 0x0032 }
        if (r0 < 0) goto L_0x0011;
    L_0x0005:
        r0 = r4.f477a;	 Catch:{ all -> 0x0032 }
        r0 = r0.isEmpty();	 Catch:{ all -> 0x0032 }
        if (r0 == 0) goto L_0x0035;
    L_0x000d:
        r0 = r4.f478b;	 Catch:{ all -> 0x0032 }
        if (r0 == 0) goto L_0x0035;
    L_0x0011:
        r0 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x0032 }
        r1 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0032 }
        r1.<init>();	 Catch:{ all -> 0x0032 }
        r2 = r4.getClass();	 Catch:{ all -> 0x0032 }
        r2 = r2.getName();	 Catch:{ all -> 0x0032 }
        r1 = r1.append(r2);	 Catch:{ all -> 0x0032 }
        r2 = ".sizeOf() is reporting inconsistent results!";
        r1 = r1.append(r2);	 Catch:{ all -> 0x0032 }
        r1 = r1.toString();	 Catch:{ all -> 0x0032 }
        r0.<init>(r1);	 Catch:{ all -> 0x0032 }
        throw r0;	 Catch:{ all -> 0x0032 }
    L_0x0032:
        r0 = move-exception;
        monitor-exit(r4);	 Catch:{ all -> 0x0032 }
        throw r0;
    L_0x0035:
        r0 = r4.f478b;	 Catch:{ all -> 0x0032 }
        if (r0 <= r5) goto L_0x0041;
    L_0x0039:
        r0 = r4.f477a;	 Catch:{ all -> 0x0032 }
        r0 = r0.isEmpty();	 Catch:{ all -> 0x0032 }
        if (r0 == 0) goto L_0x0043;
    L_0x0041:
        monitor-exit(r4);	 Catch:{ all -> 0x0032 }
        return;
    L_0x0043:
        r0 = r4.f477a;	 Catch:{ all -> 0x0032 }
        r0 = r0.entrySet();	 Catch:{ all -> 0x0032 }
        r0 = r0.iterator();	 Catch:{ all -> 0x0032 }
        r0 = r0.next();	 Catch:{ all -> 0x0032 }
        r0 = (java.util.Map.Entry) r0;	 Catch:{ all -> 0x0032 }
        r1 = r0.getKey();	 Catch:{ all -> 0x0032 }
        r0 = r0.getValue();	 Catch:{ all -> 0x0032 }
        r2 = r4.f477a;	 Catch:{ all -> 0x0032 }
        r2.remove(r1);	 Catch:{ all -> 0x0032 }
        r2 = r4.f478b;	 Catch:{ all -> 0x0032 }
        r3 = r4.m790c(r1, r0);	 Catch:{ all -> 0x0032 }
        r2 = r2 - r3;
        r4.f478b = r2;	 Catch:{ all -> 0x0032 }
        r2 = r4.f482f;	 Catch:{ all -> 0x0032 }
        r2 = r2 + 1;
        r4.f482f = r2;	 Catch:{ all -> 0x0032 }
        monitor-exit(r4);	 Catch:{ all -> 0x0032 }
        r2 = 1;
        r3 = 0;
        r4.m794a(r2, r1, r0, r3);
        goto L_0x0000;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.e.f.a(int):void");
    }

    protected void m794a(boolean z, K k, V v, V v2) {
    }

    protected V m796b(K k) {
        return null;
    }

    private int m790c(K k, V v) {
        int b = m795b(k, v);
        if (b >= 0) {
            return b;
        }
        throw new IllegalStateException("Negative size: " + k + "=" + v);
    }

    protected int m795b(K k, V v) {
        return 1;
    }

    public final synchronized String toString() {
        String format;
        int i = 0;
        synchronized (this) {
            int i2 = this.f483g + this.f484h;
            if (i2 != 0) {
                i = (this.f483g * 100) / i2;
            }
            format = String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", new Object[]{Integer.valueOf(this.f479c), Integer.valueOf(this.f483g), Integer.valueOf(this.f484h), Integer.valueOf(i)});
        }
        return format;
    }
}
