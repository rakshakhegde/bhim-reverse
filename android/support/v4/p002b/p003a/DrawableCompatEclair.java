package android.support.v4.p002b.p003a;

import android.graphics.drawable.Drawable;

/* renamed from: android.support.v4.b.a.d */
class DrawableCompatEclair {
    public static Drawable m676a(Drawable drawable) {
        if (drawable instanceof TintAwareDrawable) {
            return drawable;
        }
        return new DrawableWrapperEclair(drawable);
    }
}
