package android.support.v4.p002b.p003a;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.v4.p002b.p003a.DrawableWrapperDonut.DrawableWrapperDonut;

/* renamed from: android.support.v4.b.a.k */
class DrawableWrapperEclair extends DrawableWrapperDonut {

    /* renamed from: android.support.v4.b.a.k.a */
    private static class DrawableWrapperEclair extends DrawableWrapperDonut {
        DrawableWrapperEclair(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
            super(drawableWrapperDonut, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new DrawableWrapperEclair(this, resources);
        }
    }

    DrawableWrapperEclair(Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperEclair(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
        super(drawableWrapperDonut, resources);
    }

    DrawableWrapperDonut m705b() {
        return new DrawableWrapperEclair(this.b, null);
    }

    protected Drawable m704a(ConstantState constantState, Resources resources) {
        return constantState.newDrawable(resources);
    }
}
