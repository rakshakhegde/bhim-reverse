package android.support.v4.p002b.p003a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v4.b.a.a */
public final class DrawableCompat {
    static final DrawableCompat f432a;

    /* renamed from: android.support.v4.b.a.a.b */
    interface DrawableCompat {
        void m605a(Drawable drawable);

        void m606a(Drawable drawable, float f, float f2);

        void m607a(Drawable drawable, int i);

        void m608a(Drawable drawable, int i, int i2, int i3, int i4);

        void m609a(Drawable drawable, ColorStateList colorStateList);

        void m610a(Drawable drawable, Theme theme);

        void m611a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme);

        void m612a(Drawable drawable, Mode mode);

        void m613a(Drawable drawable, boolean z);

        boolean m614b(Drawable drawable);

        Drawable m615c(Drawable drawable);

        int m616d(Drawable drawable);

        int m617e(Drawable drawable);

        boolean m618f(Drawable drawable);

        ColorFilter m619g(Drawable drawable);
    }

    /* renamed from: android.support.v4.b.a.a.a */
    static class DrawableCompat implements DrawableCompat {
        DrawableCompat() {
        }

        public void m620a(Drawable drawable) {
        }

        public void m628a(Drawable drawable, boolean z) {
        }

        public boolean m629b(Drawable drawable) {
            return false;
        }

        public void m621a(Drawable drawable, float f, float f2) {
        }

        public void m623a(Drawable drawable, int i, int i2, int i3, int i4) {
        }

        public void m622a(Drawable drawable, int i) {
            DrawableCompatBase.m672a(drawable, i);
        }

        public void m624a(Drawable drawable, ColorStateList colorStateList) {
            DrawableCompatBase.m673a(drawable, colorStateList);
        }

        public void m627a(Drawable drawable, Mode mode) {
            DrawableCompatBase.m675a(drawable, mode);
        }

        public Drawable m630c(Drawable drawable) {
            return DrawableCompatBase.m671a(drawable);
        }

        public int m631d(Drawable drawable) {
            return 0;
        }

        public int m632e(Drawable drawable) {
            return 0;
        }

        public void m625a(Drawable drawable, Theme theme) {
        }

        public boolean m633f(Drawable drawable) {
            return false;
        }

        public ColorFilter m634g(Drawable drawable) {
            return null;
        }

        public void m626a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            DrawableCompatBase.m674a(drawable, resources, xmlPullParser, attributeSet, theme);
        }
    }

    /* renamed from: android.support.v4.b.a.a.c */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public Drawable m635c(Drawable drawable) {
            return DrawableCompatEclair.m676a(drawable);
        }
    }

    /* renamed from: android.support.v4.b.a.a.d */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public void m636a(Drawable drawable) {
            DrawableCompatHoneycomb.m677a(drawable);
        }

        public Drawable m637c(Drawable drawable) {
            return DrawableCompatHoneycomb.m678b(drawable);
        }
    }

    /* renamed from: android.support.v4.b.a.a.e */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public int m638d(Drawable drawable) {
            int a = DrawableCompatJellybeanMr1.m679a(drawable);
            return a >= 0 ? a : 0;
        }
    }

    /* renamed from: android.support.v4.b.a.a.f */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public void m639a(Drawable drawable, boolean z) {
            DrawableCompatKitKat.m680a(drawable, z);
        }

        public boolean m640b(Drawable drawable) {
            return DrawableCompatKitKat.m681a(drawable);
        }

        public Drawable m641c(Drawable drawable) {
            return DrawableCompatKitKat.m682b(drawable);
        }

        public int m642e(Drawable drawable) {
            return DrawableCompatKitKat.m683c(drawable);
        }
    }

    /* renamed from: android.support.v4.b.a.a.g */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public void m643a(Drawable drawable, float f, float f2) {
            DrawableCompatLollipop.m685a(drawable, f, f2);
        }

        public void m645a(Drawable drawable, int i, int i2, int i3, int i4) {
            DrawableCompatLollipop.m687a(drawable, i, i2, i3, i4);
        }

        public void m644a(Drawable drawable, int i) {
            DrawableCompatLollipop.m686a(drawable, i);
        }

        public void m646a(Drawable drawable, ColorStateList colorStateList) {
            DrawableCompatLollipop.m688a(drawable, colorStateList);
        }

        public void m649a(Drawable drawable, Mode mode) {
            DrawableCompatLollipop.m691a(drawable, mode);
        }

        public Drawable m650c(Drawable drawable) {
            return DrawableCompatLollipop.m684a(drawable);
        }

        public void m647a(Drawable drawable, Theme theme) {
            DrawableCompatLollipop.m689a(drawable, theme);
        }

        public boolean m651f(Drawable drawable) {
            return DrawableCompatLollipop.m692b(drawable);
        }

        public ColorFilter m652g(Drawable drawable) {
            return DrawableCompatLollipop.m693c(drawable);
        }

        public void m648a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            DrawableCompatLollipop.m690a(drawable, resources, xmlPullParser, attributeSet, theme);
        }
    }

    /* renamed from: android.support.v4.b.a.a.h */
    static class DrawableCompat extends DrawableCompat {
        DrawableCompat() {
        }

        public int m654d(Drawable drawable) {
            return DrawableCompatApi23.m670a(drawable);
        }

        public Drawable m653c(Drawable drawable) {
            return drawable;
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 23) {
            f432a = new DrawableCompat();
        } else if (i >= 21) {
            f432a = new DrawableCompat();
        } else if (i >= 19) {
            f432a = new DrawableCompat();
        } else if (i >= 17) {
            f432a = new DrawableCompat();
        } else if (i >= 11) {
            f432a = new DrawableCompat();
        } else if (i >= 5) {
            f432a = new DrawableCompat();
        } else {
            f432a = new DrawableCompat();
        }
    }

    public static void m655a(Drawable drawable) {
        f432a.m605a(drawable);
    }

    public static void m663a(Drawable drawable, boolean z) {
        f432a.m613a(drawable, z);
    }

    public static boolean m664b(Drawable drawable) {
        return f432a.m614b(drawable);
    }

    public static void m656a(Drawable drawable, float f, float f2) {
        f432a.m606a(drawable, f, f2);
    }

    public static void m658a(Drawable drawable, int i, int i2, int i3, int i4) {
        f432a.m608a(drawable, i, i2, i3, i4);
    }

    public static void m657a(Drawable drawable, int i) {
        f432a.m607a(drawable, i);
    }

    public static void m659a(Drawable drawable, ColorStateList colorStateList) {
        f432a.m609a(drawable, colorStateList);
    }

    public static void m662a(Drawable drawable, Mode mode) {
        f432a.m612a(drawable, mode);
    }

    public static int m665c(Drawable drawable) {
        return f432a.m617e(drawable);
    }

    public static void m660a(Drawable drawable, Theme theme) {
        f432a.m610a(drawable, theme);
    }

    public static boolean m666d(Drawable drawable) {
        return f432a.m618f(drawable);
    }

    public static ColorFilter m667e(Drawable drawable) {
        return f432a.m619g(drawable);
    }

    public static void m661a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
        f432a.m611a(drawable, resources, xmlPullParser, attributeSet, theme);
    }

    public static Drawable m668f(Drawable drawable) {
        return f432a.m615c(drawable);
    }

    public static int m669g(Drawable drawable) {
        return f432a.m616d(drawable);
    }
}
