package android.support.v4.p002b.p003a;

import android.graphics.drawable.Drawable;

/* renamed from: android.support.v4.b.a.e */
class DrawableCompatHoneycomb {
    public static void m677a(Drawable drawable) {
        drawable.jumpToCurrentState();
    }

    public static Drawable m678b(Drawable drawable) {
        if (drawable instanceof TintAwareDrawable) {
            return drawable;
        }
        return new DrawableWrapperHoneycomb(drawable);
    }
}
