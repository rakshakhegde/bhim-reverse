package android.support.v4.p002b.p003a;

import android.graphics.drawable.Drawable;

/* renamed from: android.support.v4.b.a.g */
class DrawableCompatKitKat {
    public static void m680a(Drawable drawable, boolean z) {
        drawable.setAutoMirrored(z);
    }

    public static boolean m681a(Drawable drawable) {
        return drawable.isAutoMirrored();
    }

    public static Drawable m682b(Drawable drawable) {
        if (drawable instanceof TintAwareDrawable) {
            return drawable;
        }
        return new DrawableWrapperKitKat(drawable);
    }

    public static int m683c(Drawable drawable) {
        return drawable.getAlpha();
    }
}
