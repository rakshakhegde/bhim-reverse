package android.support.v4.p002b.p003a;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.p002b.p003a.DrawableWrapperDonut.DrawableWrapperDonut;

/* renamed from: android.support.v4.b.a.l */
class DrawableWrapperHoneycomb extends DrawableWrapperDonut {

    /* renamed from: android.support.v4.b.a.l.a */
    private static class DrawableWrapperHoneycomb extends DrawableWrapperDonut {
        DrawableWrapperHoneycomb(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
            super(drawableWrapperDonut, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new DrawableWrapperHoneycomb(this, resources);
        }
    }

    DrawableWrapperHoneycomb(Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperHoneycomb(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
        super(drawableWrapperDonut, resources);
    }

    public void jumpToCurrentState() {
        this.c.jumpToCurrentState();
    }

    DrawableWrapperDonut m706b() {
        return new DrawableWrapperHoneycomb(this.b, null);
    }
}
