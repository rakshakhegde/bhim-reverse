package android.support.v4.p002b.p003a;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.p002b.p003a.DrawableWrapperDonut.DrawableWrapperDonut;

/* renamed from: android.support.v4.b.a.m */
class DrawableWrapperKitKat extends DrawableWrapperHoneycomb {

    /* renamed from: android.support.v4.b.a.m.a */
    private static class DrawableWrapperKitKat extends DrawableWrapperDonut {
        DrawableWrapperKitKat(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
            super(drawableWrapperDonut, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new DrawableWrapperKitKat(this, resources);
        }
    }

    DrawableWrapperKitKat(Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperKitKat(DrawableWrapperDonut drawableWrapperDonut, Resources resources) {
        super(drawableWrapperDonut, resources);
    }

    public void setAutoMirrored(boolean z) {
        this.c.setAutoMirrored(z);
    }

    public boolean isAutoMirrored() {
        return this.c.isAutoMirrored();
    }

    DrawableWrapperDonut m707b() {
        return new DrawableWrapperKitKat(this.b, null);
    }
}
