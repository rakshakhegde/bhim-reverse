package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class BackStackState implements Parcelable {
    public static final Creator<BackStackState> CREATOR;
    final int[] f113a;
    final int f114b;
    final int f115c;
    final String f116d;
    final int f117e;
    final int f118f;
    final CharSequence f119g;
    final int f120h;
    final CharSequence f121i;
    final ArrayList<String> f122j;
    final ArrayList<String> f123k;

    /* renamed from: android.support.v4.app.BackStackState.1 */
    static class BackStackRecord implements Creator<BackStackState> {
        BackStackRecord() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m116a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m117a(i);
        }

        public BackStackState m116a(Parcel parcel) {
            return new BackStackState(parcel);
        }

        public BackStackState[] m117a(int i) {
            return new BackStackState[i];
        }
    }

    public BackStackState(BackStackRecord backStackRecord) {
        int i = 0;
        for (android.support.v4.app.BackStackRecord.BackStackRecord backStackRecord2 = backStackRecord.f288c; backStackRecord2 != null; backStackRecord2 = backStackRecord2.f272a) {
            if (backStackRecord2.f280i != null) {
                i += backStackRecord2.f280i.size();
            }
        }
        this.f113a = new int[(i + (backStackRecord.f290e * 7))];
        if (backStackRecord.f297l) {
            i = 0;
            for (android.support.v4.app.BackStackRecord.BackStackRecord backStackRecord3 = backStackRecord.f288c; backStackRecord3 != null; backStackRecord3 = backStackRecord3.f272a) {
                int i2 = i + 1;
                this.f113a[i] = backStackRecord3.f274c;
                int i3 = i2 + 1;
                this.f113a[i2] = backStackRecord3.f275d != null ? backStackRecord3.f275d.f158p : -1;
                int i4 = i3 + 1;
                this.f113a[i3] = backStackRecord3.f276e;
                i2 = i4 + 1;
                this.f113a[i4] = backStackRecord3.f277f;
                i4 = i2 + 1;
                this.f113a[i2] = backStackRecord3.f278g;
                i2 = i4 + 1;
                this.f113a[i4] = backStackRecord3.f279h;
                if (backStackRecord3.f280i != null) {
                    int size = backStackRecord3.f280i.size();
                    i4 = i2 + 1;
                    this.f113a[i2] = size;
                    i2 = 0;
                    while (i2 < size) {
                        i3 = i4 + 1;
                        this.f113a[i4] = ((Fragment) backStackRecord3.f280i.get(i2)).f158p;
                        i2++;
                        i4 = i3;
                    }
                    i = i4;
                } else {
                    i = i2 + 1;
                    this.f113a[i2] = 0;
                }
            }
            this.f114b = backStackRecord.f295j;
            this.f115c = backStackRecord.f296k;
            this.f116d = backStackRecord.f299n;
            this.f117e = backStackRecord.f301p;
            this.f118f = backStackRecord.f302q;
            this.f119g = backStackRecord.f303r;
            this.f120h = backStackRecord.f304s;
            this.f121i = backStackRecord.f305t;
            this.f122j = backStackRecord.f306u;
            this.f123k = backStackRecord.f307v;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public BackStackState(Parcel parcel) {
        this.f113a = parcel.createIntArray();
        this.f114b = parcel.readInt();
        this.f115c = parcel.readInt();
        this.f116d = parcel.readString();
        this.f117e = parcel.readInt();
        this.f118f = parcel.readInt();
        this.f119g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f120h = parcel.readInt();
        this.f121i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f122j = parcel.createStringArrayList();
        this.f123k = parcel.createStringArrayList();
    }

    public BackStackRecord m118a(FragmentManager fragmentManager) {
        BackStackRecord backStackRecord = new BackStackRecord(fragmentManager);
        int i = 0;
        int i2 = 0;
        while (i2 < this.f113a.length) {
            android.support.v4.app.BackStackRecord.BackStackRecord backStackRecord2 = new android.support.v4.app.BackStackRecord.BackStackRecord();
            int i3 = i2 + 1;
            backStackRecord2.f274c = this.f113a[i2];
            if (FragmentManager.f361a) {
                Log.v("FragmentManager", "Instantiate " + backStackRecord + " op #" + i + " base fragment #" + this.f113a[i3]);
            }
            int i4 = i3 + 1;
            i2 = this.f113a[i3];
            if (i2 >= 0) {
                backStackRecord2.f275d = (Fragment) fragmentManager.f367f.get(i2);
            } else {
                backStackRecord2.f275d = null;
            }
            i3 = i4 + 1;
            backStackRecord2.f276e = this.f113a[i4];
            i4 = i3 + 1;
            backStackRecord2.f277f = this.f113a[i3];
            i3 = i4 + 1;
            backStackRecord2.f278g = this.f113a[i4];
            int i5 = i3 + 1;
            backStackRecord2.f279h = this.f113a[i3];
            i4 = i5 + 1;
            int i6 = this.f113a[i5];
            if (i6 > 0) {
                backStackRecord2.f280i = new ArrayList(i6);
                i3 = 0;
                while (i3 < i6) {
                    if (FragmentManager.f361a) {
                        Log.v("FragmentManager", "Instantiate " + backStackRecord + " set remove fragment #" + this.f113a[i4]);
                    }
                    i5 = i4 + 1;
                    backStackRecord2.f280i.add((Fragment) fragmentManager.f367f.get(this.f113a[i4]));
                    i3++;
                    i4 = i5;
                }
            }
            backStackRecord.f291f = backStackRecord2.f276e;
            backStackRecord.f292g = backStackRecord2.f277f;
            backStackRecord.f293h = backStackRecord2.f278g;
            backStackRecord.f294i = backStackRecord2.f279h;
            backStackRecord.m359a(backStackRecord2);
            i++;
            i2 = i4;
        }
        backStackRecord.f295j = this.f114b;
        backStackRecord.f296k = this.f115c;
        backStackRecord.f299n = this.f116d;
        backStackRecord.f301p = this.f117e;
        backStackRecord.f297l = true;
        backStackRecord.f302q = this.f118f;
        backStackRecord.f303r = this.f119g;
        backStackRecord.f304s = this.f120h;
        backStackRecord.f305t = this.f121i;
        backStackRecord.f306u = this.f122j;
        backStackRecord.f307v = this.f123k;
        backStackRecord.m358a(1);
        return backStackRecord;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.f113a);
        parcel.writeInt(this.f114b);
        parcel.writeInt(this.f115c);
        parcel.writeString(this.f116d);
        parcel.writeInt(this.f117e);
        parcel.writeInt(this.f118f);
        TextUtils.writeToParcel(this.f119g, parcel, 0);
        parcel.writeInt(this.f120h);
        TextUtils.writeToParcel(this.f121i, parcel, 0);
        parcel.writeStringList(this.f122j);
        parcel.writeStringList(this.f123k);
    }

    static {
        CREATOR = new BackStackRecord();
    }
}
