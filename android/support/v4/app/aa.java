package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.ab.NotificationCompatApi20;
import android.support.v4.app.ac.NotificationCompatApi21;
import android.support.v4.app.ad.NotificationCompatBase;
import android.support.v4.app.ag.NotificationCompatIceCreamSandwich;
import android.support.v4.app.ah.NotificationCompatJellybean;
import android.support.v4.app.ai.NotificationCompatKitKat;
import android.support.v4.app.al.RemoteInputCompatBase;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: NotificationCompat */
public class aa {
    private static final NotificationCompat f230a;

    /* renamed from: android.support.v4.app.aa.a */
    public static class NotificationCompat extends NotificationCompatBase {
        public static final NotificationCompatBase.NotificationCompatBase f186d;
        public int f187a;
        public CharSequence f188b;
        public PendingIntent f189c;
        private final Bundle f190e;
        private final aj[] f191f;

        /* renamed from: android.support.v4.app.aa.a.1 */
        static class NotificationCompat implements NotificationCompatBase.NotificationCompatBase {
            NotificationCompat() {
            }
        }

        public /* synthetic */ RemoteInputCompatBase[] m217f() {
            return m216e();
        }

        public int m212a() {
            return this.f187a;
        }

        public CharSequence m213b() {
            return this.f188b;
        }

        public PendingIntent m214c() {
            return this.f189c;
        }

        public Bundle m215d() {
            return this.f190e;
        }

        public aj[] m216e() {
            return this.f191f;
        }

        static {
            f186d = new NotificationCompat();
        }
    }

    /* renamed from: android.support.v4.app.aa.p */
    public static abstract class NotificationCompat {
        NotificationCompat f192d;
        CharSequence f193e;
        CharSequence f194f;
        boolean f195g;

        public NotificationCompat() {
            this.f195g = false;
        }

        public void m218a(NotificationCompat notificationCompat) {
            if (this.f192d != notificationCompat) {
                this.f192d = notificationCompat;
                if (this.f192d != null) {
                    this.f192d.m229a(this);
                }
            }
        }
    }

    /* renamed from: android.support.v4.app.aa.b */
    public static class NotificationCompat extends NotificationCompat {
        Bitmap f196a;
        Bitmap f197b;
        boolean f198c;
    }

    /* renamed from: android.support.v4.app.aa.c */
    public static class NotificationCompat extends NotificationCompat {
        CharSequence f199a;

        public NotificationCompat m219a(CharSequence charSequence) {
            this.e = NotificationCompat.m222d(charSequence);
            return this;
        }

        public NotificationCompat m220b(CharSequence charSequence) {
            this.f199a = NotificationCompat.m222d(charSequence);
            return this;
        }
    }

    /* renamed from: android.support.v4.app.aa.d */
    public static class NotificationCompat {
        Notification f200A;
        public Notification f201B;
        public ArrayList<String> f202C;
        public Context f203a;
        public CharSequence f204b;
        public CharSequence f205c;
        PendingIntent f206d;
        PendingIntent f207e;
        RemoteViews f208f;
        public Bitmap f209g;
        public CharSequence f210h;
        public int f211i;
        int f212j;
        boolean f213k;
        public boolean f214l;
        public NotificationCompat f215m;
        public CharSequence f216n;
        int f217o;
        int f218p;
        boolean f219q;
        String f220r;
        boolean f221s;
        String f222t;
        public ArrayList<NotificationCompat> f223u;
        boolean f224v;
        String f225w;
        Bundle f226x;
        int f227y;
        int f228z;

        public NotificationCompat(Context context) {
            this.f213k = true;
            this.f223u = new ArrayList();
            this.f224v = false;
            this.f227y = 0;
            this.f228z = 0;
            this.f201B = new Notification();
            this.f203a = context;
            this.f201B.when = System.currentTimeMillis();
            this.f201B.audioStreamType = -1;
            this.f212j = 0;
            this.f202C = new ArrayList();
        }

        public NotificationCompat m225a(long j) {
            this.f201B.when = j;
            return this;
        }

        public NotificationCompat m224a(int i) {
            this.f201B.icon = i;
            return this;
        }

        public NotificationCompat m230a(CharSequence charSequence) {
            this.f204b = NotificationCompat.m222d(charSequence);
            return this;
        }

        public NotificationCompat m234b(CharSequence charSequence) {
            this.f205c = NotificationCompat.m222d(charSequence);
            return this;
        }

        public NotificationCompat m226a(PendingIntent pendingIntent) {
            this.f206d = pendingIntent;
            return this;
        }

        public NotificationCompat m233b(PendingIntent pendingIntent) {
            this.f201B.deleteIntent = pendingIntent;
            return this;
        }

        public NotificationCompat m236c(CharSequence charSequence) {
            this.f201B.tickerText = NotificationCompat.m222d(charSequence);
            return this;
        }

        public NotificationCompat m227a(Bitmap bitmap) {
            this.f209g = bitmap;
            return this;
        }

        public NotificationCompat m228a(Uri uri) {
            this.f201B.sound = uri;
            this.f201B.audioStreamType = -1;
            return this;
        }

        public NotificationCompat m231a(boolean z) {
            m221a(16, z);
            return this;
        }

        private void m221a(int i, boolean z) {
            if (z) {
                Notification notification = this.f201B;
                notification.flags |= i;
                return;
            }
            notification = this.f201B;
            notification.flags &= i ^ -1;
        }

        public NotificationCompat m229a(NotificationCompat notificationCompat) {
            if (this.f215m != notificationCompat) {
                this.f215m = notificationCompat;
                if (this.f215m != null) {
                    this.f215m.m218a(this);
                }
            }
            return this;
        }

        public NotificationCompat m232b(int i) {
            this.f227y = i;
            return this;
        }

        public Notification m223a() {
            return aa.f230a.m238a(this, m235b());
        }

        protected NotificationCompat m235b() {
            return new NotificationCompat();
        }

        protected static CharSequence m222d(CharSequence charSequence) {
            if (charSequence != null && charSequence.length() > 5120) {
                return charSequence.subSequence(0, 5120);
            }
            return charSequence;
        }
    }

    /* renamed from: android.support.v4.app.aa.e */
    protected static class NotificationCompat {
        protected NotificationCompat() {
        }

        public Notification m237a(NotificationCompat notificationCompat, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return notificationBuilderWithBuilderAccessor.m254b();
        }
    }

    /* renamed from: android.support.v4.app.aa.f */
    public static class NotificationCompat extends NotificationCompat {
        ArrayList<CharSequence> f229a;

        public NotificationCompat() {
            this.f229a = new ArrayList();
        }
    }

    /* renamed from: android.support.v4.app.aa.g */
    interface NotificationCompat {
        Notification m238a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2);
    }

    /* renamed from: android.support.v4.app.aa.j */
    static class NotificationCompat implements NotificationCompat {
        NotificationCompat() {
        }

        public Notification m239a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            Notification a = ad.m262a(notificationCompat.f201B, notificationCompat.f203a, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f206d);
            if (notificationCompat.f212j > 0) {
                a.flags |= 128;
            }
            return a;
        }
    }

    /* renamed from: android.support.v4.app.aa.n */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m240a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            NotificationBuilderWithBuilderAccessor notificationCompatJellybean = new NotificationCompatJellybean(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g, notificationCompat.f217o, notificationCompat.f218p, notificationCompat.f219q, notificationCompat.f214l, notificationCompat.f212j, notificationCompat.f216n, notificationCompat.f224v, notificationCompat.f226x, notificationCompat.f220r, notificationCompat.f221s, notificationCompat.f222t);
            aa.m250b((NotificationBuilderWithActions) notificationCompatJellybean, notificationCompat.f223u);
            aa.m251b(notificationCompatJellybean, notificationCompat.f215m);
            return notificationCompat2.m237a(notificationCompat, notificationCompatJellybean);
        }
    }

    /* renamed from: android.support.v4.app.aa.o */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m241a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            NotificationBuilderWithBuilderAccessor notificationCompatKitKat = new NotificationCompatKitKat(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g, notificationCompat.f217o, notificationCompat.f218p, notificationCompat.f219q, notificationCompat.f213k, notificationCompat.f214l, notificationCompat.f212j, notificationCompat.f216n, notificationCompat.f224v, notificationCompat.f202C, notificationCompat.f226x, notificationCompat.f220r, notificationCompat.f221s, notificationCompat.f222t);
            aa.m250b((NotificationBuilderWithActions) notificationCompatKitKat, notificationCompat.f223u);
            aa.m251b(notificationCompatKitKat, notificationCompat.f215m);
            return notificationCompat2.m237a(notificationCompat, notificationCompatKitKat);
        }
    }

    /* renamed from: android.support.v4.app.aa.h */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m242a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            NotificationBuilderWithBuilderAccessor notificationCompatApi20 = new NotificationCompatApi20(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g, notificationCompat.f217o, notificationCompat.f218p, notificationCompat.f219q, notificationCompat.f213k, notificationCompat.f214l, notificationCompat.f212j, notificationCompat.f216n, notificationCompat.f224v, notificationCompat.f202C, notificationCompat.f226x, notificationCompat.f220r, notificationCompat.f221s, notificationCompat.f222t);
            aa.m250b((NotificationBuilderWithActions) notificationCompatApi20, notificationCompat.f223u);
            aa.m251b(notificationCompatApi20, notificationCompat.f215m);
            return notificationCompat2.m237a(notificationCompat, notificationCompatApi20);
        }
    }

    /* renamed from: android.support.v4.app.aa.i */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m243a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            NotificationBuilderWithBuilderAccessor notificationCompatApi21 = new NotificationCompatApi21(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g, notificationCompat.f217o, notificationCompat.f218p, notificationCompat.f219q, notificationCompat.f213k, notificationCompat.f214l, notificationCompat.f212j, notificationCompat.f216n, notificationCompat.f224v, notificationCompat.f225w, notificationCompat.f202C, notificationCompat.f226x, notificationCompat.f227y, notificationCompat.f228z, notificationCompat.f200A, notificationCompat.f220r, notificationCompat.f221s, notificationCompat.f222t);
            aa.m250b((NotificationBuilderWithActions) notificationCompatApi21, notificationCompat.f223u);
            aa.m251b(notificationCompatApi21, notificationCompat.f215m);
            return notificationCompat2.m237a(notificationCompat, notificationCompatApi21);
        }
    }

    /* renamed from: android.support.v4.app.aa.k */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m244a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            Notification a = ae.m263a(notificationCompat.f201B, notificationCompat.f203a, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f206d, notificationCompat.f207e);
            if (notificationCompat.f212j > 0) {
                a.flags |= 128;
            }
            return a;
        }
    }

    /* renamed from: android.support.v4.app.aa.l */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m245a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            return af.m264a(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g);
        }
    }

    /* renamed from: android.support.v4.app.aa.m */
    static class NotificationCompat extends NotificationCompat {
        NotificationCompat() {
        }

        public Notification m246a(NotificationCompat notificationCompat, NotificationCompat notificationCompat2) {
            return notificationCompat2.m237a(notificationCompat, new NotificationCompatIceCreamSandwich(notificationCompat.f203a, notificationCompat.f201B, notificationCompat.f204b, notificationCompat.f205c, notificationCompat.f210h, notificationCompat.f208f, notificationCompat.f211i, notificationCompat.f206d, notificationCompat.f207e, notificationCompat.f209g, notificationCompat.f217o, notificationCompat.f218p, notificationCompat.f219q));
        }
    }

    private static void m250b(NotificationBuilderWithActions notificationBuilderWithActions, ArrayList<NotificationCompat> arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            notificationBuilderWithActions.m252a((NotificationCompat) it.next());
        }
    }

    private static void m251b(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat notificationCompat) {
        if (notificationCompat == null) {
            return;
        }
        if (notificationCompat instanceof NotificationCompat) {
            NotificationCompat notificationCompat2 = (NotificationCompat) notificationCompat;
            ah.m274a(notificationBuilderWithBuilderAccessor, notificationCompat2.e, notificationCompat2.g, notificationCompat2.f, notificationCompat2.f199a);
        } else if (notificationCompat instanceof NotificationCompat) {
            NotificationCompat notificationCompat3 = (NotificationCompat) notificationCompat;
            ah.m275a(notificationBuilderWithBuilderAccessor, notificationCompat3.e, notificationCompat3.g, notificationCompat3.f, notificationCompat3.f229a);
        } else if (notificationCompat instanceof NotificationCompat) {
            NotificationCompat notificationCompat4 = (NotificationCompat) notificationCompat;
            ah.m273a(notificationBuilderWithBuilderAccessor, notificationCompat4.e, notificationCompat4.g, notificationCompat4.f, notificationCompat4.f196a, notificationCompat4.f197b, notificationCompat4.f198c);
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 20) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 19) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 16) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 14) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 11) {
            f230a = new NotificationCompat();
        } else if (VERSION.SDK_INT >= 9) {
            f230a = new NotificationCompat();
        } else {
            f230a = new NotificationCompat();
        }
    }
}
