package android.support.v4.app;

/* renamed from: android.support.v4.app.r */
public abstract class FragmentTransaction {
    public abstract int m317a();

    public abstract FragmentTransaction m318a(int i, Fragment fragment);

    public abstract FragmentTransaction m319a(Fragment fragment);

    public abstract FragmentTransaction m320a(Fragment fragment, String str);

    public abstract FragmentTransaction m321a(String str);

    public abstract int m322b();
}
