package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.app.al.RemoteInputCompatBase;

/* compiled from: RemoteInputCompatJellybean */
class am {
    static Bundle m290a(RemoteInputCompatBase remoteInputCompatBase) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", remoteInputCompatBase.m279a());
        bundle.putCharSequence("label", remoteInputCompatBase.m280b());
        bundle.putCharSequenceArray("choices", remoteInputCompatBase.m281c());
        bundle.putBoolean("allowFreeFormInput", remoteInputCompatBase.m282d());
        bundle.putBundle("extras", remoteInputCompatBase.m283e());
        return bundle;
    }

    static Bundle[] m291a(RemoteInputCompatBase[] remoteInputCompatBaseArr) {
        if (remoteInputCompatBaseArr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[remoteInputCompatBaseArr.length];
        for (int i = 0; i < remoteInputCompatBaseArr.length; i++) {
            bundleArr[i] = m290a(remoteInputCompatBaseArr[i]);
        }
        return bundleArr;
    }
}
