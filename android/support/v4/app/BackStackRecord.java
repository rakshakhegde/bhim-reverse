package android.support.v4.app;

import android.os.Build.VERSION;
import android.support.v4.app.FragmentTransitionCompat21.FragmentTransitionCompat21;
import android.support.v4.p010e.ArrayMap;
import android.support.v4.p010e.LogWriter;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v4.app.g */
final class BackStackRecord extends FragmentTransaction implements Runnable {
    static final boolean f286a;
    final FragmentManager f287b;
    BackStackRecord f288c;
    BackStackRecord f289d;
    int f290e;
    int f291f;
    int f292g;
    int f293h;
    int f294i;
    int f295j;
    int f296k;
    boolean f297l;
    boolean f298m;
    String f299n;
    boolean f300o;
    int f301p;
    int f302q;
    CharSequence f303r;
    int f304s;
    CharSequence f305t;
    ArrayList<String> f306u;
    ArrayList<String> f307v;

    /* renamed from: android.support.v4.app.g.1 */
    class BackStackRecord implements FragmentTransitionCompat21 {
        final /* synthetic */ Fragment f257a;
        final /* synthetic */ BackStackRecord f258b;

        BackStackRecord(BackStackRecord backStackRecord, Fragment fragment) {
            this.f258b = backStackRecord;
            this.f257a = fragment;
        }

        public View m316a() {
            return this.f257a.m186n();
        }
    }

    /* renamed from: android.support.v4.app.g.2 */
    class BackStackRecord implements OnPreDrawListener {
        final /* synthetic */ View f259a;
        final /* synthetic */ Object f260b;
        final /* synthetic */ ArrayList f261c;
        final /* synthetic */ BackStackRecord f262d;
        final /* synthetic */ boolean f263e;
        final /* synthetic */ Fragment f264f;
        final /* synthetic */ Fragment f265g;
        final /* synthetic */ BackStackRecord f266h;

        BackStackRecord(BackStackRecord backStackRecord, View view, Object obj, ArrayList arrayList, BackStackRecord backStackRecord2, boolean z, Fragment fragment, Fragment fragment2) {
            this.f266h = backStackRecord;
            this.f259a = view;
            this.f260b = obj;
            this.f261c = arrayList;
            this.f262d = backStackRecord2;
            this.f263e = z;
            this.f264f = fragment;
            this.f265g = fragment2;
        }

        public boolean onPreDraw() {
            this.f259a.getViewTreeObserver().removeOnPreDrawListener(this);
            if (this.f260b != null) {
                FragmentTransitionCompat21.m546a(this.f260b, this.f261c);
                this.f261c.clear();
                ArrayMap a = this.f266h.m325a(this.f262d, this.f263e, this.f264f);
                FragmentTransitionCompat21.m543a(this.f260b, this.f262d.f284d, (Map) a, this.f261c);
                this.f266h.m340a(a, this.f262d);
                this.f266h.m333a(this.f262d, this.f264f, this.f265g, this.f263e, a);
            }
            return true;
        }
    }

    /* renamed from: android.support.v4.app.g.3 */
    class BackStackRecord implements OnPreDrawListener {
        final /* synthetic */ View f267a;
        final /* synthetic */ BackStackRecord f268b;
        final /* synthetic */ int f269c;
        final /* synthetic */ Object f270d;
        final /* synthetic */ BackStackRecord f271e;

        BackStackRecord(BackStackRecord backStackRecord, View view, BackStackRecord backStackRecord2, int i, Object obj) {
            this.f271e = backStackRecord;
            this.f267a = view;
            this.f268b = backStackRecord2;
            this.f269c = i;
            this.f270d = obj;
        }

        public boolean onPreDraw() {
            this.f267a.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f271e.m332a(this.f268b, this.f269c, this.f270d);
            return true;
        }
    }

    /* renamed from: android.support.v4.app.g.a */
    static final class BackStackRecord {
        BackStackRecord f272a;
        BackStackRecord f273b;
        int f274c;
        Fragment f275d;
        int f276e;
        int f277f;
        int f278g;
        int f279h;
        ArrayList<Fragment> f280i;

        BackStackRecord() {
        }
    }

    /* renamed from: android.support.v4.app.g.b */
    public class BackStackRecord {
        public ArrayMap<String, String> f281a;
        public ArrayList<View> f282b;
        public FragmentTransitionCompat21 f283c;
        public View f284d;
        final /* synthetic */ BackStackRecord f285e;

        public BackStackRecord(BackStackRecord backStackRecord) {
            this.f285e = backStackRecord;
            this.f281a = new ArrayMap();
            this.f282b = new ArrayList();
            this.f283c = new FragmentTransitionCompat21();
        }
    }

    static {
        f286a = VERSION.SDK_INT >= 21;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(128);
        stringBuilder.append("BackStackEntry{");
        stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.f301p >= 0) {
            stringBuilder.append(" #");
            stringBuilder.append(this.f301p);
        }
        if (this.f299n != null) {
            stringBuilder.append(" ");
            stringBuilder.append(this.f299n);
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    public void m361a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        m362a(str, printWriter, true);
    }

    public void m362a(String str, PrintWriter printWriter, boolean z) {
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.f299n);
            printWriter.print(" mIndex=");
            printWriter.print(this.f301p);
            printWriter.print(" mCommitted=");
            printWriter.println(this.f300o);
            if (this.f295j != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f295j));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.f296k));
            }
            if (!(this.f291f == 0 && this.f292g == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f291f));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f292g));
            }
            if (!(this.f293h == 0 && this.f294i == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f293h));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f294i));
            }
            if (!(this.f302q == 0 && this.f303r == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.f302q));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.f303r);
            }
            if (!(this.f304s == 0 && this.f305t == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.f304s));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.f305t);
            }
        }
        if (this.f288c != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str2 = str + "    ";
            int i = 0;
            BackStackRecord backStackRecord = this.f288c;
            while (backStackRecord != null) {
                String str3;
                switch (backStackRecord.f274c) {
                    case R.View_android_theme /*0*/:
                        str3 = "NULL";
                        break;
                    case R.View_android_focusable /*1*/:
                        str3 = "ADD";
                        break;
                    case R.View_paddingStart /*2*/:
                        str3 = "REPLACE";
                        break;
                    case R.View_paddingEnd /*3*/:
                        str3 = "REMOVE";
                        break;
                    case R.View_theme /*4*/:
                        str3 = "HIDE";
                        break;
                    case R.Toolbar_contentInsetStart /*5*/:
                        str3 = "SHOW";
                        break;
                    case R.Toolbar_contentInsetEnd /*6*/:
                        str3 = "DETACH";
                        break;
                    case R.Toolbar_contentInsetLeft /*7*/:
                        str3 = "ATTACH";
                        break;
                    default:
                        str3 = "cmd=" + backStackRecord.f274c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str3);
                printWriter.print(" ");
                printWriter.println(backStackRecord.f275d);
                if (z) {
                    if (!(backStackRecord.f276e == 0 && backStackRecord.f277f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(backStackRecord.f276e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(backStackRecord.f277f));
                    }
                    if (!(backStackRecord.f278g == 0 && backStackRecord.f279h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(backStackRecord.f278g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(backStackRecord.f279h));
                    }
                }
                if (backStackRecord.f280i != null && backStackRecord.f280i.size() > 0) {
                    for (int i2 = 0; i2 < backStackRecord.f280i.size(); i2++) {
                        printWriter.print(str2);
                        if (backStackRecord.f280i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i2 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str2);
                            printWriter.print("  #");
                            printWriter.print(i2);
                            printWriter.print(": ");
                        }
                        printWriter.println(backStackRecord.f280i.get(i2));
                    }
                }
                backStackRecord = backStackRecord.f272a;
                i++;
            }
        }
    }

    public BackStackRecord(FragmentManager fragmentManager) {
        this.f298m = true;
        this.f301p = -1;
        this.f287b = fragmentManager;
    }

    void m359a(BackStackRecord backStackRecord) {
        if (this.f288c == null) {
            this.f289d = backStackRecord;
            this.f288c = backStackRecord;
        } else {
            backStackRecord.f273b = this.f289d;
            this.f289d.f272a = backStackRecord;
            this.f289d = backStackRecord;
        }
        backStackRecord.f276e = this.f291f;
        backStackRecord.f277f = this.f292g;
        backStackRecord.f278g = this.f293h;
        backStackRecord.f279h = this.f294i;
        this.f290e++;
    }

    public FragmentTransaction m356a(Fragment fragment, String str) {
        m331a(0, fragment, str, 1);
        return this;
    }

    private void m331a(int i, Fragment fragment, String str, int i2) {
        fragment.f128B = this.f287b;
        if (str != null) {
            if (fragment.f134H == null || str.equals(fragment.f134H)) {
                fragment.f134H = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.f134H + " now " + str);
            }
        }
        if (i != 0) {
            if (fragment.f132F == 0 || fragment.f132F == i) {
                fragment.f132F = i;
                fragment.f133G = i;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.f132F + " now " + i);
            }
        }
        BackStackRecord backStackRecord = new BackStackRecord();
        backStackRecord.f274c = i2;
        backStackRecord.f275d = fragment;
        m359a(backStackRecord);
    }

    public FragmentTransaction m353a(int i, Fragment fragment) {
        return m354a(i, fragment, null);
    }

    public FragmentTransaction m354a(int i, Fragment fragment, String str) {
        if (i == 0) {
            throw new IllegalArgumentException("Must use non-zero containerViewId");
        }
        m331a(i, fragment, str, 2);
        return this;
    }

    public FragmentTransaction m355a(Fragment fragment) {
        BackStackRecord backStackRecord = new BackStackRecord();
        backStackRecord.f274c = 3;
        backStackRecord.f275d = fragment;
        m359a(backStackRecord);
        return this;
    }

    public FragmentTransaction m357a(String str) {
        if (this.f298m) {
            this.f297l = true;
            this.f299n = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    void m358a(int i) {
        if (this.f297l) {
            if (FragmentManager.f361a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            for (BackStackRecord backStackRecord = this.f288c; backStackRecord != null; backStackRecord = backStackRecord.f272a) {
                Fragment fragment;
                if (backStackRecord.f275d != null) {
                    fragment = backStackRecord.f275d;
                    fragment.f127A += i;
                    if (FragmentManager.f361a) {
                        Log.v("FragmentManager", "Bump nesting of " + backStackRecord.f275d + " to " + backStackRecord.f275d.f127A);
                    }
                }
                if (backStackRecord.f280i != null) {
                    for (int size = backStackRecord.f280i.size() - 1; size >= 0; size--) {
                        fragment = (Fragment) backStackRecord.f280i.get(size);
                        fragment.f127A += i;
                        if (FragmentManager.f361a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.f127A);
                        }
                    }
                }
            }
        }
    }

    public int m350a() {
        return m351a(false);
    }

    public int m363b() {
        return m351a(true);
    }

    int m351a(boolean z) {
        if (this.f300o) {
            throw new IllegalStateException("commit already called");
        }
        if (FragmentManager.f361a) {
            Log.v("FragmentManager", "Commit: " + this);
            m361a("  ", null, new PrintWriter(new LogWriter("FragmentManager")), null);
        }
        this.f300o = true;
        if (this.f297l) {
            this.f301p = this.f287b.m477a(this);
        } else {
            this.f301p = -1;
        }
        this.f287b.m496a((Runnable) this, z);
        return this.f301p;
    }

    public void run() {
        if (FragmentManager.f361a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.f297l || this.f301p >= 0) {
            BackStackRecord backStackRecord;
            m358a(1);
            if (!f286a || this.f287b.f375n < 1) {
                backStackRecord = null;
            } else {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                m348b(sparseArray, sparseArray2);
                backStackRecord = m323a(sparseArray, sparseArray2, false);
            }
            int i = backStackRecord != null ? 0 : this.f296k;
            int i2 = backStackRecord != null ? 0 : this.f295j;
            BackStackRecord backStackRecord2 = this.f288c;
            while (backStackRecord2 != null) {
                int i3 = backStackRecord != null ? 0 : backStackRecord2.f276e;
                int i4 = backStackRecord != null ? 0 : backStackRecord2.f277f;
                Fragment fragment;
                switch (backStackRecord2.f274c) {
                    case R.View_android_focusable /*1*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i3;
                        this.f287b.m494a(fragment, false);
                        break;
                    case R.View_paddingStart /*2*/:
                        Fragment fragment2 = backStackRecord2.f275d;
                        int i5 = fragment2.f133G;
                        if (this.f287b.f368g != null) {
                            int size = this.f287b.f368g.size() - 1;
                            while (size >= 0) {
                                fragment = (Fragment) this.f287b.f368g.get(size);
                                if (FragmentManager.f361a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment2 + " old=" + fragment);
                                }
                                if (fragment.f133G == i5) {
                                    if (fragment == fragment2) {
                                        fragment = null;
                                        backStackRecord2.f275d = null;
                                        size--;
                                        fragment2 = fragment;
                                    } else {
                                        if (backStackRecord2.f280i == null) {
                                            backStackRecord2.f280i = new ArrayList();
                                        }
                                        backStackRecord2.f280i.add(fragment);
                                        fragment.f142P = i4;
                                        if (this.f297l) {
                                            fragment.f127A++;
                                            if (FragmentManager.f361a) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.f127A);
                                            }
                                        }
                                        this.f287b.m492a(fragment, i2, i);
                                    }
                                }
                                fragment = fragment2;
                                size--;
                                fragment2 = fragment;
                            }
                        }
                        if (fragment2 == null) {
                            break;
                        }
                        fragment2.f142P = i3;
                        this.f287b.m494a(fragment2, false);
                        break;
                    case R.View_paddingEnd /*3*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i4;
                        this.f287b.m492a(fragment, i2, i);
                        break;
                    case R.View_theme /*4*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i4;
                        this.f287b.m505b(fragment, i2, i);
                        break;
                    case R.Toolbar_contentInsetStart /*5*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i3;
                        this.f287b.m511c(fragment, i2, i);
                        break;
                    case R.Toolbar_contentInsetEnd /*6*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i4;
                        this.f287b.m515d(fragment, i2, i);
                        break;
                    case R.Toolbar_contentInsetLeft /*7*/:
                        fragment = backStackRecord2.f275d;
                        fragment.f142P = i3;
                        this.f287b.m517e(fragment, i2, i);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + backStackRecord2.f274c);
                }
                backStackRecord2 = backStackRecord2.f272a;
            }
            this.f287b.m485a(this.f287b.f375n, i2, i, true);
            if (this.f297l) {
                this.f287b.m506b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    private static void m342a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, Fragment fragment) {
        if (fragment != null) {
            int i = fragment.f133G;
            if (i != 0 && !fragment.m185m()) {
                if (fragment.m184l() && fragment.m186n() != null && sparseArray.get(i) == null) {
                    sparseArray.put(i, fragment);
                }
                if (sparseArray2.get(i) == fragment) {
                    sparseArray2.remove(i);
                }
            }
        }
    }

    private void m349b(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, Fragment fragment) {
        if (fragment != null) {
            int i = fragment.f133G;
            if (i != 0) {
                if (!fragment.m184l()) {
                    sparseArray2.put(i, fragment);
                }
                if (sparseArray.get(i) == fragment) {
                    sparseArray.remove(i);
                }
            }
            if (fragment.f153k < 1 && this.f287b.f375n >= 1) {
                this.f287b.m510c(fragment);
                this.f287b.m493a(fragment, 1, 0, 0, false);
            }
        }
    }

    private void m348b(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (this.f287b.f377p.m120a()) {
            for (BackStackRecord backStackRecord = this.f288c; backStackRecord != null; backStackRecord = backStackRecord.f272a) {
                switch (backStackRecord.f274c) {
                    case R.View_android_focusable /*1*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_paddingStart /*2*/:
                        Fragment fragment = backStackRecord.f275d;
                        if (this.f287b.f368g != null) {
                            Fragment fragment2 = fragment;
                            for (int i = 0; i < this.f287b.f368g.size(); i++) {
                                Fragment fragment3 = (Fragment) this.f287b.f368g.get(i);
                                if (fragment2 == null || fragment3.f133G == fragment2.f133G) {
                                    if (fragment3 == fragment2) {
                                        fragment2 = null;
                                        sparseArray2.remove(fragment3.f133G);
                                    } else {
                                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, fragment3);
                                    }
                                }
                            }
                        }
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_paddingEnd /*3*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_theme /*4*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetStart /*5*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetEnd /*6*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetLeft /*7*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void m360a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (this.f287b.f377p.m120a()) {
            for (BackStackRecord backStackRecord = this.f289d; backStackRecord != null; backStackRecord = backStackRecord.f273b) {
                switch (backStackRecord.f274c) {
                    case R.View_android_focusable /*1*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_paddingStart /*2*/:
                        if (backStackRecord.f280i != null) {
                            for (int size = backStackRecord.f280i.size() - 1; size >= 0; size--) {
                                m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, (Fragment) backStackRecord.f280i.get(size));
                            }
                        }
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_paddingEnd /*3*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.View_theme /*4*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetStart /*5*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetEnd /*6*/:
                        m349b((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    case R.Toolbar_contentInsetLeft /*7*/:
                        BackStackRecord.m342a((SparseArray) sparseArray, (SparseArray) sparseArray2, backStackRecord.f275d);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public BackStackRecord m352a(boolean z, BackStackRecord backStackRecord, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (FragmentManager.f361a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            m361a("  ", null, new PrintWriter(new LogWriter("FragmentManager")), null);
        }
        if (f286a && this.f287b.f375n >= 1) {
            if (backStackRecord == null) {
                if (!(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
                    backStackRecord = m323a((SparseArray) sparseArray, (SparseArray) sparseArray2, true);
                }
            } else if (!z) {
                BackStackRecord.m336a(backStackRecord, this.f307v, this.f306u);
            }
        }
        m358a(-1);
        int i = backStackRecord != null ? 0 : this.f296k;
        int i2 = backStackRecord != null ? 0 : this.f295j;
        BackStackRecord backStackRecord2 = this.f289d;
        while (backStackRecord2 != null) {
            int i3 = backStackRecord != null ? 0 : backStackRecord2.f278g;
            int i4 = backStackRecord != null ? 0 : backStackRecord2.f279h;
            Fragment fragment;
            Fragment fragment2;
            switch (backStackRecord2.f274c) {
                case R.View_android_focusable /*1*/:
                    fragment = backStackRecord2.f275d;
                    fragment.f142P = i4;
                    this.f287b.m492a(fragment, FragmentManager.m475c(i2), i);
                    break;
                case R.View_paddingStart /*2*/:
                    fragment = backStackRecord2.f275d;
                    if (fragment != null) {
                        fragment.f142P = i4;
                        this.f287b.m492a(fragment, FragmentManager.m475c(i2), i);
                    }
                    if (backStackRecord2.f280i == null) {
                        break;
                    }
                    for (int i5 = 0; i5 < backStackRecord2.f280i.size(); i5++) {
                        fragment2 = (Fragment) backStackRecord2.f280i.get(i5);
                        fragment2.f142P = i3;
                        this.f287b.m494a(fragment2, false);
                    }
                    break;
                case R.View_paddingEnd /*3*/:
                    fragment2 = backStackRecord2.f275d;
                    fragment2.f142P = i3;
                    this.f287b.m494a(fragment2, false);
                    break;
                case R.View_theme /*4*/:
                    fragment2 = backStackRecord2.f275d;
                    fragment2.f142P = i3;
                    this.f287b.m511c(fragment2, FragmentManager.m475c(i2), i);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    fragment = backStackRecord2.f275d;
                    fragment.f142P = i4;
                    this.f287b.m505b(fragment, FragmentManager.m475c(i2), i);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    fragment2 = backStackRecord2.f275d;
                    fragment2.f142P = i3;
                    this.f287b.m517e(fragment2, FragmentManager.m475c(i2), i);
                    break;
                case R.Toolbar_contentInsetLeft /*7*/:
                    fragment2 = backStackRecord2.f275d;
                    fragment2.f142P = i3;
                    this.f287b.m515d(fragment2, FragmentManager.m475c(i2), i);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + backStackRecord2.f274c);
            }
            backStackRecord2 = backStackRecord2.f273b;
        }
        if (z) {
            this.f287b.m485a(this.f287b.f375n, FragmentManager.m475c(i2), i, true);
            backStackRecord = null;
        }
        if (this.f301p >= 0) {
            this.f287b.m503b(this.f301p);
            this.f301p = -1;
        }
        return backStackRecord;
    }

    public String m364c() {
        return this.f299n;
    }

    private BackStackRecord m323a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, boolean z) {
        int i;
        int i2 = 0;
        BackStackRecord backStackRecord = new BackStackRecord(this);
        backStackRecord.f284d = new View(this.f287b.f376o.m397g());
        int i3 = 0;
        int i4 = 0;
        while (i3 < sparseArray.size()) {
            if (m344a(sparseArray.keyAt(i3), backStackRecord, z, (SparseArray) sparseArray, (SparseArray) sparseArray2)) {
                i = 1;
            } else {
                i = i4;
            }
            i3++;
            i4 = i;
        }
        while (i2 < sparseArray2.size()) {
            i = sparseArray2.keyAt(i2);
            if (sparseArray.get(i) == null && m344a(i, backStackRecord, z, (SparseArray) sparseArray, (SparseArray) sparseArray2)) {
                i4 = 1;
            }
            i2++;
        }
        if (i4 == 0) {
            return null;
        }
        return backStackRecord;
    }

    private static Object m329a(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return FragmentTransitionCompat21.m535a(z ? fragment.m195w() : fragment.m192t());
    }

    private static Object m346b(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return FragmentTransitionCompat21.m535a(z ? fragment.m193u() : fragment.m194v());
    }

    private static Object m328a(Fragment fragment, Fragment fragment2, boolean z) {
        if (fragment == null || fragment2 == null) {
            return null;
        }
        return FragmentTransitionCompat21.m554b(z ? fragment2.m197y() : fragment.m196x());
    }

    private static Object m330a(Object obj, Fragment fragment, ArrayList<View> arrayList, ArrayMap<String, View> arrayMap, View view) {
        if (obj != null) {
            return FragmentTransitionCompat21.m536a(obj, fragment.m186n(), arrayList, arrayMap, view);
        }
        return obj;
    }

    private ArrayMap<String, View> m324a(BackStackRecord backStackRecord, Fragment fragment, boolean z) {
        ArrayMap arrayMap = new ArrayMap();
        if (this.f306u != null) {
            FragmentTransitionCompat21.m549a((Map) arrayMap, fragment.m186n());
            if (z) {
                arrayMap.m772a(this.f307v);
            } else {
                arrayMap = BackStackRecord.m327a(this.f306u, this.f307v, arrayMap);
            }
        }
        if (z) {
            if (fragment.ag != null) {
                fragment.ag.m293a(this.f307v, arrayMap);
            }
            m334a(backStackRecord, arrayMap, false);
        } else {
            if (fragment.ah != null) {
                fragment.ah.m293a(this.f307v, arrayMap);
            }
            m347b(backStackRecord, arrayMap, false);
        }
        return arrayMap;
    }

    private boolean m344a(int i, BackStackRecord backStackRecord, boolean z, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        View view = (ViewGroup) this.f287b.f377p.m119a(i);
        if (view == null) {
            return false;
        }
        Object obj;
        ArrayList arrayList;
        Object a;
        View view2;
        FragmentTransitionCompat21 backStackRecord2;
        ArrayList arrayList2;
        Map arrayMap;
        boolean z2;
        Object a2;
        Fragment fragment = (Fragment) sparseArray2.get(i);
        Fragment fragment2 = (Fragment) sparseArray.get(i);
        Object a3 = BackStackRecord.m329a(fragment, z);
        Object a4 = BackStackRecord.m328a(fragment, fragment2, z);
        Object b = BackStackRecord.m346b(fragment2, z);
        Map map = null;
        ArrayList arrayList3 = new ArrayList();
        if (a4 != null) {
            map = m324a(backStackRecord, fragment2, z);
            if (map.isEmpty()) {
                map = null;
                obj = null;
                if (a3 != null && obj == null && b == null) {
                    return false;
                }
                arrayList = new ArrayList();
                a = BackStackRecord.m330a(b, fragment2, arrayList, (ArrayMap) map, backStackRecord.f284d);
                if (!(this.f307v == null || map == null)) {
                    view2 = (View) map.get(this.f307v.get(0));
                    if (view2 != null) {
                        if (a != null) {
                            FragmentTransitionCompat21.m542a(a, view2);
                        }
                        if (obj != null) {
                            FragmentTransitionCompat21.m542a(obj, view2);
                        }
                    }
                }
                backStackRecord2 = new BackStackRecord(this, fragment);
                arrayList2 = new ArrayList();
                arrayMap = new ArrayMap();
                z2 = true;
                if (fragment != null) {
                    z2 = z ? fragment.m126A() : fragment.m198z();
                }
                a2 = FragmentTransitionCompat21.m537a(a3, a, obj, z2);
                if (a2 != null) {
                    FragmentTransitionCompat21.m545a(a3, obj, view, backStackRecord2, backStackRecord.f284d, backStackRecord.f283c, backStackRecord.f281a, arrayList2, map, arrayMap, arrayList3);
                    m343a(view, backStackRecord, i, a2);
                    FragmentTransitionCompat21.m544a(a2, backStackRecord.f284d, true);
                    m332a(backStackRecord, i, a2);
                    FragmentTransitionCompat21.m541a((ViewGroup) view, a2);
                    FragmentTransitionCompat21.m540a(view, backStackRecord.f284d, a3, arrayList2, a, arrayList, obj, arrayList3, a2, backStackRecord.f282b, arrayMap);
                }
                if (a2 == null) {
                    return true;
                }
                return false;
            }
            an anVar = z ? fragment2.ag : fragment.ag;
            if (anVar != null) {
                anVar.m292a(new ArrayList(map.keySet()), new ArrayList(map.values()), null);
            }
            m335a(backStackRecord, view, a4, fragment, fragment2, z, arrayList3);
        }
        obj = a4;
        if (a3 != null) {
        }
        arrayList = new ArrayList();
        a = BackStackRecord.m330a(b, fragment2, arrayList, (ArrayMap) map, backStackRecord.f284d);
        view2 = (View) map.get(this.f307v.get(0));
        if (view2 != null) {
            if (a != null) {
                FragmentTransitionCompat21.m542a(a, view2);
            }
            if (obj != null) {
                FragmentTransitionCompat21.m542a(obj, view2);
            }
        }
        backStackRecord2 = new BackStackRecord(this, fragment);
        arrayList2 = new ArrayList();
        arrayMap = new ArrayMap();
        z2 = true;
        if (fragment != null) {
            if (z) {
            }
        }
        a2 = FragmentTransitionCompat21.m537a(a3, a, obj, z2);
        if (a2 != null) {
            FragmentTransitionCompat21.m545a(a3, obj, view, backStackRecord2, backStackRecord.f284d, backStackRecord.f283c, backStackRecord.f281a, arrayList2, map, arrayMap, arrayList3);
            m343a(view, backStackRecord, i, a2);
            FragmentTransitionCompat21.m544a(a2, backStackRecord.f284d, true);
            m332a(backStackRecord, i, a2);
            FragmentTransitionCompat21.m541a((ViewGroup) view, a2);
            FragmentTransitionCompat21.m540a(view, backStackRecord.f284d, a3, arrayList2, a, arrayList, obj, arrayList3, a2, backStackRecord.f282b, arrayMap);
        }
        if (a2 == null) {
            return false;
        }
        return true;
    }

    private void m335a(BackStackRecord backStackRecord, View view, Object obj, Fragment fragment, Fragment fragment2, boolean z, ArrayList<View> arrayList) {
        view.getViewTreeObserver().addOnPreDrawListener(new BackStackRecord(this, view, obj, arrayList, backStackRecord, z, fragment, fragment2));
    }

    private void m333a(BackStackRecord backStackRecord, Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> arrayMap) {
        an anVar = z ? fragment2.ag : fragment.ag;
        if (anVar != null) {
            anVar.m294b(new ArrayList(arrayMap.keySet()), new ArrayList(arrayMap.values()), null);
        }
    }

    private void m340a(ArrayMap<String, View> arrayMap, BackStackRecord backStackRecord) {
        if (this.f307v != null && !arrayMap.isEmpty()) {
            View view = (View) arrayMap.get(this.f307v.get(0));
            if (view != null) {
                backStackRecord.f283c.f408a = view;
            }
        }
    }

    private ArrayMap<String, View> m325a(BackStackRecord backStackRecord, boolean z, Fragment fragment) {
        ArrayMap b = m345b(backStackRecord, fragment, z);
        if (z) {
            if (fragment.ah != null) {
                fragment.ah.m293a(this.f307v, b);
            }
            m334a(backStackRecord, b, true);
        } else {
            if (fragment.ag != null) {
                fragment.ag.m293a(this.f307v, b);
            }
            m347b(backStackRecord, b, true);
        }
        return b;
    }

    private static ArrayMap<String, View> m327a(ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayMap<String, View> arrayMap) {
        if (arrayMap.isEmpty()) {
            return arrayMap;
        }
        ArrayMap<String, View> arrayMap2 = new ArrayMap();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            View view = (View) arrayMap.get(arrayList.get(i));
            if (view != null) {
                arrayMap2.put(arrayList2.get(i), view);
            }
        }
        return arrayMap2;
    }

    private ArrayMap<String, View> m345b(BackStackRecord backStackRecord, Fragment fragment, boolean z) {
        ArrayMap arrayMap = new ArrayMap();
        View n = fragment.m186n();
        if (n == null || this.f306u == null) {
            return arrayMap;
        }
        FragmentTransitionCompat21.m549a((Map) arrayMap, n);
        if (z) {
            return BackStackRecord.m327a(this.f306u, this.f307v, arrayMap);
        }
        arrayMap.m772a(this.f307v);
        return arrayMap;
    }

    private void m343a(View view, BackStackRecord backStackRecord, int i, Object obj) {
        view.getViewTreeObserver().addOnPreDrawListener(new BackStackRecord(this, view, backStackRecord, i, obj));
    }

    private void m332a(BackStackRecord backStackRecord, int i, Object obj) {
        if (this.f287b.f368g != null) {
            for (int i2 = 0; i2 < this.f287b.f368g.size(); i2++) {
                Fragment fragment = (Fragment) this.f287b.f368g.get(i2);
                if (!(fragment.f144R == null || fragment.f143Q == null || fragment.f133G != i)) {
                    if (!fragment.f135I) {
                        FragmentTransitionCompat21.m544a(obj, fragment.f144R, false);
                        backStackRecord.f282b.remove(fragment.f144R);
                    } else if (!backStackRecord.f282b.contains(fragment.f144R)) {
                        FragmentTransitionCompat21.m544a(obj, fragment.f144R, true);
                        backStackRecord.f282b.add(fragment.f144R);
                    }
                }
            }
        }
    }

    private static void m341a(ArrayMap<String, String> arrayMap, String str, String str2) {
        if (str != null && str2 != null) {
            for (int i = 0; i < arrayMap.size(); i++) {
                if (str.equals(arrayMap.m769c(i))) {
                    arrayMap.m765a(i, (Object) str2);
                    return;
                }
            }
            arrayMap.put(str, str2);
        }
    }

    private static void m336a(BackStackRecord backStackRecord, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        if (arrayList != null) {
            for (int i = 0; i < arrayList.size(); i++) {
                BackStackRecord.m341a(backStackRecord.f281a, (String) arrayList.get(i), (String) arrayList2.get(i));
            }
        }
    }

    private void m334a(BackStackRecord backStackRecord, ArrayMap<String, View> arrayMap, boolean z) {
        int size = this.f307v == null ? 0 : this.f307v.size();
        for (int i = 0; i < size; i++) {
            String str = (String) this.f306u.get(i);
            View view = (View) arrayMap.get((String) this.f307v.get(i));
            if (view != null) {
                String a = FragmentTransitionCompat21.m538a(view);
                if (z) {
                    BackStackRecord.m341a(backStackRecord.f281a, str, a);
                } else {
                    BackStackRecord.m341a(backStackRecord.f281a, a, str);
                }
            }
        }
    }

    private void m347b(BackStackRecord backStackRecord, ArrayMap<String, View> arrayMap, boolean z) {
        int size = arrayMap.size();
        for (int i = 0; i < size; i++) {
            String str = (String) arrayMap.m768b(i);
            String a = FragmentTransitionCompat21.m538a((View) arrayMap.m769c(i));
            if (z) {
                BackStackRecord.m341a(backStackRecord.f281a, str, a);
            } else {
                BackStackRecord.m341a(backStackRecord.f281a, a, str);
            }
        }
    }
}
