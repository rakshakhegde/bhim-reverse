package android.support.v4.app;

import android.content.Context;
import android.os.Build.VERSION;

/* renamed from: android.support.v4.app.e */
public final class AppOpsManagerCompat {
    private static final AppOpsManagerCompat f256a;

    /* renamed from: android.support.v4.app.e.b */
    private static class AppOpsManagerCompat {
        private AppOpsManagerCompat() {
        }

        public String m308a(String str) {
            return null;
        }

        public int m307a(Context context, String str, String str2) {
            return 1;
        }
    }

    /* renamed from: android.support.v4.app.e.a */
    private static class AppOpsManagerCompat extends AppOpsManagerCompat {
        private AppOpsManagerCompat() {
            super();
        }

        public String m310a(String str) {
            return AppOpsManagerCompat23.m314a(str);
        }

        public int m309a(Context context, String str, String str2) {
            return AppOpsManagerCompat23.m313a(context, str, str2);
        }
    }

    static {
        if (VERSION.SDK_INT >= 23) {
            f256a = new AppOpsManagerCompat();
        } else {
            f256a = new AppOpsManagerCompat();
        }
    }

    public static String m312a(String str) {
        return f256a.m308a(str);
    }

    public static int m311a(Context context, String str, String str2) {
        return f256a.m307a(context, str, str2);
    }
}
