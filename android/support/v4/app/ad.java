package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.al.RemoteInputCompatBase;

/* compiled from: NotificationCompatBase */
public class ad {

    /* renamed from: android.support.v4.app.ad.a */
    public static abstract class NotificationCompatBase {

        /* renamed from: android.support.v4.app.ad.a.a */
        public interface NotificationCompatBase {
        }

        public abstract int m207a();

        public abstract CharSequence m208b();

        public abstract PendingIntent m209c();

        public abstract Bundle m210d();

        public abstract RemoteInputCompatBase[] m211f();
    }

    public static Notification m262a(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, PendingIntent pendingIntent) {
        notification.setLatestEventInfo(context, charSequence, charSequence2, pendingIntent);
        return notification;
    }
}
