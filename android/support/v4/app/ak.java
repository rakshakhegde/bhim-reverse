package android.support.v4.app;

import android.app.RemoteInput;
import android.app.RemoteInput.Builder;
import android.support.v4.app.al.RemoteInputCompatBase;

/* compiled from: RemoteInputCompatApi20 */
class ak {
    static RemoteInput[] m289a(RemoteInputCompatBase[] remoteInputCompatBaseArr) {
        if (remoteInputCompatBaseArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[remoteInputCompatBaseArr.length];
        for (int i = 0; i < remoteInputCompatBaseArr.length; i++) {
            RemoteInputCompatBase remoteInputCompatBase = remoteInputCompatBaseArr[i];
            remoteInputArr[i] = new Builder(remoteInputCompatBase.m279a()).setLabel(remoteInputCompatBase.m280b()).setChoices(remoteInputCompatBase.m281c()).setAllowFreeFormInput(remoteInputCompatBase.m282d()).addExtras(remoteInputCompatBase.m283e()).build();
        }
        return remoteInputArr;
    }
}
