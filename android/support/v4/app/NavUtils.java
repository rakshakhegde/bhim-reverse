package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.support.v4.p004a.IntentCompat;
import android.util.Log;

/* renamed from: android.support.v4.app.v */
public final class NavUtils {
    private static final NavUtils f431a;

    /* renamed from: android.support.v4.app.v.a */
    interface NavUtils {
        Intent m581a(Activity activity);

        String m582a(Context context, ActivityInfo activityInfo);

        boolean m583a(Activity activity, Intent intent);

        void m584b(Activity activity, Intent intent);
    }

    /* renamed from: android.support.v4.app.v.b */
    static class NavUtils implements NavUtils {
        NavUtils() {
        }

        public Intent m585a(Activity activity) {
            String b = NavUtils.m597b(activity);
            if (b == null) {
                return null;
            }
            ComponentName componentName = new ComponentName(activity, b);
            try {
                return NavUtils.m598b((Context) activity, componentName) == null ? IntentCompat.m92a(componentName) : new Intent().setComponent(componentName);
            } catch (NameNotFoundException e) {
                Log.e("NavUtils", "getParentActivityIntent: bad parentActivityName '" + b + "' in manifest");
                return null;
            }
        }

        public boolean m587a(Activity activity, Intent intent) {
            String action = activity.getIntent().getAction();
            return (action == null || action.equals("android.intent.action.MAIN")) ? false : true;
        }

        public void m588b(Activity activity, Intent intent) {
            intent.addFlags(67108864);
            activity.startActivity(intent);
            activity.finish();
        }

        public String m586a(Context context, ActivityInfo activityInfo) {
            if (activityInfo.metaData == null) {
                return null;
            }
            String string = activityInfo.metaData.getString("android.support.PARENT_ACTIVITY");
            if (string == null) {
                return null;
            }
            if (string.charAt(0) == '.') {
                return context.getPackageName() + string;
            }
            return string;
        }
    }

    /* renamed from: android.support.v4.app.v.c */
    static class NavUtils extends NavUtils {
        NavUtils() {
        }

        public Intent m589a(Activity activity) {
            Intent a = NavUtilsJB.m600a(activity);
            if (a == null) {
                return m592b(activity);
            }
            return a;
        }

        Intent m592b(Activity activity) {
            return super.m585a(activity);
        }

        public boolean m591a(Activity activity, Intent intent) {
            return NavUtilsJB.m602a(activity, intent);
        }

        public void m593b(Activity activity, Intent intent) {
            NavUtilsJB.m603b(activity, intent);
        }

        public String m590a(Context context, ActivityInfo activityInfo) {
            String a = NavUtilsJB.m601a(activityInfo);
            if (a == null) {
                return super.m586a(context, activityInfo);
            }
            return a;
        }
    }

    static {
        if (VERSION.SDK_INT >= 16) {
            f431a = new NavUtils();
        } else {
            f431a = new NavUtils();
        }
    }

    public static boolean m596a(Activity activity, Intent intent) {
        return f431a.m583a(activity, intent);
    }

    public static void m599b(Activity activity, Intent intent) {
        f431a.m584b(activity, intent);
    }

    public static Intent m594a(Activity activity) {
        return f431a.m581a(activity);
    }

    public static Intent m595a(Context context, ComponentName componentName) {
        String b = NavUtils.m598b(context, componentName);
        if (b == null) {
            return null;
        }
        ComponentName componentName2 = new ComponentName(componentName.getPackageName(), b);
        return NavUtils.m598b(context, componentName2) == null ? IntentCompat.m92a(componentName2) : new Intent().setComponent(componentName2);
    }

    public static String m597b(Activity activity) {
        try {
            return NavUtils.m598b((Context) activity, activity.getComponentName());
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String m598b(Context context, ComponentName componentName) {
        return f431a.m582a(context, context.getPackageManager().getActivityInfo(componentName, 128));
    }
}
