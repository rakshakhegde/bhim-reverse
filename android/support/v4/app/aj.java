package android.support.v4.app;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.al.RemoteInputCompatBase;

/* compiled from: RemoteInput */
public final class aj extends RemoteInputCompatBase {
    public static final RemoteInputCompatBase.RemoteInputCompatBase f245a;
    private static final RemoteInput f246g;
    private final String f247b;
    private final CharSequence f248c;
    private final CharSequence[] f249d;
    private final boolean f250e;
    private final Bundle f251f;

    /* renamed from: android.support.v4.app.aj.1 */
    static class RemoteInput implements RemoteInputCompatBase.RemoteInputCompatBase {
        RemoteInput() {
        }
    }

    /* renamed from: android.support.v4.app.aj.a */
    interface RemoteInput {
    }

    /* renamed from: android.support.v4.app.aj.b */
    static class RemoteInput implements RemoteInput {
        RemoteInput() {
        }
    }

    /* renamed from: android.support.v4.app.aj.c */
    static class RemoteInput implements RemoteInput {
        RemoteInput() {
        }
    }

    /* renamed from: android.support.v4.app.aj.d */
    static class RemoteInput implements RemoteInput {
        RemoteInput() {
        }
    }

    public String m284a() {
        return this.f247b;
    }

    public CharSequence m285b() {
        return this.f248c;
    }

    public CharSequence[] m286c() {
        return this.f249d;
    }

    public boolean m287d() {
        return this.f250e;
    }

    public Bundle m288e() {
        return this.f251f;
    }

    static {
        if (VERSION.SDK_INT >= 20) {
            f246g = new RemoteInput();
        } else if (VERSION.SDK_INT >= 16) {
            f246g = new RemoteInput();
        } else {
            f246g = new RemoteInput();
        }
        f245a = new RemoteInput();
    }
}
