package android.support.v4.app;

import android.app.Notification;
import android.app.Notification.Builder;

/* renamed from: android.support.v4.app.z */
public interface NotificationBuilderWithBuilderAccessor {
    Builder m253a();

    Notification m254b();
}
