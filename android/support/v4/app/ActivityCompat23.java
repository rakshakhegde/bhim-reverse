package android.support.v4.app;

import android.app.Activity;

/* renamed from: android.support.v4.app.b */
class ActivityCompat23 {

    /* renamed from: android.support.v4.app.b.a */
    public interface ActivityCompat23 {
        void m302a(int i);
    }

    public static void m303a(Activity activity, String[] strArr, int i) {
        if (activity instanceof ActivityCompat23) {
            ((ActivityCompat23) activity).m302a(i);
        }
        activity.requestPermissions(strArr, i);
    }
}
