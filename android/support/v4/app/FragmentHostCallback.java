package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.p010e.SimpleArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: android.support.v4.app.o */
public abstract class FragmentHostCallback<E> extends FragmentContainer {
    private final Activity f318a;
    final Context f319b;
    final int f320c;
    final FragmentManager f321d;
    private final Handler f322e;
    private SimpleArrayMap<String, LoaderManager> f323f;
    private boolean f324g;
    private LoaderManager f325h;
    private boolean f326i;
    private boolean f327j;

    FragmentHostCallback(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, fragmentActivity.f332a, 0);
    }

    FragmentHostCallback(Activity activity, Context context, Handler handler, int i) {
        this.f321d = new FragmentManager();
        this.f318a = activity;
        this.f319b = context;
        this.f322e = handler;
        this.f320c = i;
    }

    public void m386a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public boolean m389a(Fragment fragment) {
        return true;
    }

    public LayoutInflater m390b() {
        return (LayoutInflater) this.f319b.getSystemService("layout_inflater");
    }

    public void m393c() {
    }

    public void m383a(Fragment fragment, Intent intent, int i, Bundle bundle) {
        if (i != -1) {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        this.f319b.startActivity(intent);
    }

    public boolean m394d() {
        return true;
    }

    public int m395e() {
        return this.f320c;
    }

    public View m382a(int i) {
        return null;
    }

    public boolean m388a() {
        return true;
    }

    Activity m396f() {
        return this.f318a;
    }

    Context m397g() {
        return this.f319b;
    }

    Handler m398h() {
        return this.f322e;
    }

    FragmentManager m399i() {
        return this.f321d;
    }

    void m385a(String str) {
        if (this.f323f != null) {
            LoaderManager loaderManager = (LoaderManager) this.f323f.get(str);
            if (loaderManager != null && !loaderManager.f429f) {
                loaderManager.m580h();
                this.f323f.remove(str);
            }
        }
    }

    void m391b(Fragment fragment) {
    }

    boolean m400j() {
        return this.f324g;
    }

    void m401k() {
        if (!this.f327j) {
            this.f327j = true;
            if (this.f325h != null) {
                this.f325h.m574b();
            } else if (!this.f326i) {
                this.f325h = m381a("(root)", this.f327j, false);
                if (!(this.f325h == null || this.f325h.f428e)) {
                    this.f325h.m574b();
                }
            }
            this.f326i = true;
        }
    }

    void m387a(boolean z) {
        this.f324g = z;
        if (this.f325h != null && this.f327j) {
            this.f327j = false;
            if (z) {
                this.f325h.m576d();
            } else {
                this.f325h.m575c();
            }
        }
    }

    void m402l() {
        if (this.f325h != null) {
            this.f325h.m580h();
        }
    }

    void m403m() {
        if (this.f323f != null) {
            int size = this.f323f.size();
            LoaderManager[] loaderManagerArr = new LoaderManager[size];
            for (int i = size - 1; i >= 0; i--) {
                loaderManagerArr[i] = (LoaderManager) this.f323f.m769c(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                LoaderManager loaderManager = loaderManagerArr[i2];
                loaderManager.m577e();
                loaderManager.m579g();
            }
        }
    }

    LoaderManager m381a(String str, boolean z, boolean z2) {
        if (this.f323f == null) {
            this.f323f = new SimpleArrayMap();
        }
        LoaderManager loaderManager = (LoaderManager) this.f323f.get(str);
        if (loaderManager != null) {
            loaderManager.m571a(this);
            return loaderManager;
        } else if (!z2) {
            return loaderManager;
        } else {
            loaderManager = new LoaderManager(str, this, z);
            this.f323f.put(str, loaderManager);
            return loaderManager;
        }
    }

    SimpleArrayMap<String, LoaderManager> m404n() {
        int i;
        int i2 = 0;
        if (this.f323f != null) {
            int size = this.f323f.size();
            LoaderManager[] loaderManagerArr = new LoaderManager[size];
            for (int i3 = size - 1; i3 >= 0; i3--) {
                loaderManagerArr[i3] = (LoaderManager) this.f323f.m769c(i3);
            }
            i = 0;
            while (i2 < size) {
                LoaderManager loaderManager = loaderManagerArr[i2];
                if (loaderManager.f429f) {
                    i = 1;
                } else {
                    loaderManager.m580h();
                    this.f323f.remove(loaderManager.f427d);
                }
                i2++;
            }
        } else {
            i = 0;
        }
        if (i != 0) {
            return this.f323f;
        }
        return null;
    }

    void m384a(SimpleArrayMap<String, LoaderManager> simpleArrayMap) {
        this.f323f = simpleArrayMap;
    }

    void m392b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.f327j);
        if (this.f325h != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.f325h)));
            printWriter.println(":");
            this.f325h.m572a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }
}
