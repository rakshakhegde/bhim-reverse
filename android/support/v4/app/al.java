package android.support.v4.app;

import android.os.Bundle;

/* compiled from: RemoteInputCompatBase */
class al {

    /* renamed from: android.support.v4.app.al.a */
    public static abstract class RemoteInputCompatBase {

        /* renamed from: android.support.v4.app.al.a.a */
        public interface RemoteInputCompatBase {
        }

        protected abstract String m279a();

        protected abstract CharSequence m280b();

        protected abstract CharSequence[] m281c();

        protected abstract boolean m282d();

        protected abstract Bundle m283e();
    }
}
