package android.support.v4.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: android.support.v4.app.k */
public class DialogFragment extends Fragment implements OnCancelListener, OnDismissListener {
    int f308a;
    int f309b;
    boolean f310c;
    boolean f311d;
    int f312e;
    Dialog f313f;
    boolean f314g;
    boolean f315h;
    boolean f316i;

    public DialogFragment() {
        this.f308a = 0;
        this.f309b = 0;
        this.f310c = true;
        this.f311d = true;
        this.f312e = -1;
    }

    public void m370a(FragmentManager fragmentManager, String str) {
        this.f315h = false;
        this.f316i = true;
        FragmentTransaction a = fragmentManager.m462a();
        a.m320a((Fragment) this, str);
        a.m317a();
    }

    void m371a(boolean z) {
        if (!this.f315h) {
            this.f315h = true;
            this.f316i = false;
            if (this.f313f != null) {
                this.f313f.dismiss();
                this.f313f = null;
            }
            this.f314g = true;
            if (this.f312e >= 0) {
                m180j().m463a(this.f312e, 1);
                this.f312e = -1;
                return;
            }
            FragmentTransaction a = m180j().m462a();
            a.m319a((Fragment) this);
            if (z) {
                a.m322b();
            } else {
                a.m317a();
            }
        }
    }

    public int m366a() {
        return this.f309b;
    }

    public void m374b(boolean z) {
        this.f311d = z;
    }

    public void m367a(Activity activity) {
        super.m143a(activity);
        if (!this.f316i) {
            this.f315h = false;
        }
    }

    public void m373b() {
        super.m158b();
        if (!this.f316i && !this.f315h) {
            this.f315h = true;
        }
    }

    public void m369a(Bundle bundle) {
        super.m150a(bundle);
        this.f311d = this.G == 0;
        if (bundle != null) {
            this.f308a = bundle.getInt("android:style", 0);
            this.f309b = bundle.getInt("android:theme", 0);
            this.f310c = bundle.getBoolean("android:cancelable", true);
            this.f311d = bundle.getBoolean("android:showsDialog", this.f311d);
            this.f312e = bundle.getInt("android:backStackId", -1);
        }
    }

    public LayoutInflater m372b(Bundle bundle) {
        if (!this.f311d) {
            return super.m156b(bundle);
        }
        this.f313f = m375c(bundle);
        if (this.f313f == null) {
            return (LayoutInflater) this.C.m397g().getSystemService("layout_inflater");
        }
        m368a(this.f313f, this.f308a);
        return (LayoutInflater) this.f313f.getContext().getSystemService("layout_inflater");
    }

    public void m368a(Dialog dialog, int i) {
        switch (i) {
            case R.View_android_focusable /*1*/:
            case R.View_paddingStart /*2*/:
                break;
            case R.View_paddingEnd /*3*/:
                dialog.getWindow().addFlags(24);
                break;
            default:
                return;
        }
        dialog.requestWindowFeature(1);
    }

    public Dialog m375c(Bundle bundle) {
        return new Dialog(m176h(), m366a());
    }

    public void onCancel(DialogInterface dialogInterface) {
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.f314g) {
            m371a(true);
        }
    }

    public void m378d(Bundle bundle) {
        super.m167d(bundle);
        if (this.f311d) {
            View n = m186n();
            if (n != null) {
                if (n.getParent() != null) {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                }
                this.f313f.setContentView(n);
            }
            this.f313f.setOwnerActivity(m176h());
            this.f313f.setCancelable(this.f310c);
            this.f313f.setOnCancelListener(this);
            this.f313f.setOnDismissListener(this);
            if (bundle != null) {
                Bundle bundle2 = bundle.getBundle("android:savedDialogState");
                if (bundle2 != null) {
                    this.f313f.onRestoreInstanceState(bundle2);
                }
            }
        }
    }

    public void m376c() {
        super.m162c();
        if (this.f313f != null) {
            this.f314g = false;
            this.f313f.show();
        }
    }

    public void m380e(Bundle bundle) {
        super.m171e(bundle);
        if (this.f313f != null) {
            Bundle onSaveInstanceState = this.f313f.onSaveInstanceState();
            if (onSaveInstanceState != null) {
                bundle.putBundle("android:savedDialogState", onSaveInstanceState);
            }
        }
        if (this.f308a != 0) {
            bundle.putInt("android:style", this.f308a);
        }
        if (this.f309b != 0) {
            bundle.putInt("android:theme", this.f309b);
        }
        if (!this.f310c) {
            bundle.putBoolean("android:cancelable", this.f310c);
        }
        if (!this.f311d) {
            bundle.putBoolean("android:showsDialog", this.f311d);
        }
        if (this.f312e != -1) {
            bundle.putInt("android:backStackId", this.f312e);
        }
    }

    public void m377d() {
        super.m166d();
        if (this.f313f != null) {
            this.f313f.hide();
        }
    }

    public void m379e() {
        super.m170e();
        if (this.f313f != null) {
            this.f314g = true;
            this.f313f.dismiss();
            this.f313f = null;
        }
    }
}
