package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.p004a.ContextCompat;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TaskStackBuilder */
public final class ap implements Iterable<Intent> {
    private static final TaskStackBuilder f253a;
    private final ArrayList<Intent> f254b;
    private final Context f255c;

    /* renamed from: android.support.v4.app.ap.a */
    public interface TaskStackBuilder {
        Intent m295a();
    }

    /* renamed from: android.support.v4.app.ap.b */
    interface TaskStackBuilder {
    }

    /* renamed from: android.support.v4.app.ap.c */
    static class TaskStackBuilder implements TaskStackBuilder {
        TaskStackBuilder() {
        }
    }

    /* renamed from: android.support.v4.app.ap.d */
    static class TaskStackBuilder implements TaskStackBuilder {
        TaskStackBuilder() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 11) {
            f253a = new TaskStackBuilder();
        } else {
            f253a = new TaskStackBuilder();
        }
    }

    private ap(Context context) {
        this.f254b = new ArrayList();
        this.f255c = context;
    }

    public static ap m296a(Context context) {
        return new ap(context);
    }

    public ap m299a(Intent intent) {
        this.f254b.add(intent);
        return this;
    }

    public ap m297a(Activity activity) {
        Intent a;
        Intent intent = null;
        if (activity instanceof TaskStackBuilder) {
            intent = ((TaskStackBuilder) activity).m295a();
        }
        if (intent == null) {
            a = NavUtils.m594a(activity);
        } else {
            a = intent;
        }
        if (a != null) {
            ComponentName component = a.getComponent();
            if (component == null) {
                component = a.resolveActivity(this.f255c.getPackageManager());
            }
            m298a(component);
            m299a(a);
        }
        return this;
    }

    public ap m298a(ComponentName componentName) {
        int size = this.f254b.size();
        try {
            Intent a = NavUtils.m595a(this.f255c, componentName);
            while (a != null) {
                this.f254b.add(size, a);
                a = NavUtils.m595a(this.f255c, a.getComponent());
            }
            return this;
        } catch (Throwable e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    public Iterator<Intent> iterator() {
        return this.f254b.iterator();
    }

    public void m300a() {
        m301a(null);
    }

    public void m301a(Bundle bundle) {
        if (this.f254b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.f254b.toArray(new Intent[this.f254b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!ContextCompat.m79a(this.f255c, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.f255c.startActivity(intent);
        }
    }
}
