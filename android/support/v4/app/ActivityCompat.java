package android.support.v4.app;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.p004a.ContextCompat;

/* renamed from: android.support.v4.app.a */
public class ActivityCompat extends ContextCompat {

    /* renamed from: android.support.v4.app.a.1 */
    static class ActivityCompat implements Runnable {
        final /* synthetic */ String[] f183a;
        final /* synthetic */ Activity f184b;
        final /* synthetic */ int f185c;

        ActivityCompat(String[] strArr, Activity activity, int i) {
            this.f183a = strArr;
            this.f184b = activity;
            this.f185c = i;
        }

        public void run() {
            int[] iArr = new int[this.f183a.length];
            PackageManager packageManager = this.f184b.getPackageManager();
            String packageName = this.f184b.getPackageName();
            int length = this.f183a.length;
            for (int i = 0; i < length; i++) {
                iArr[i] = packageManager.checkPermission(this.f183a[i], packageName);
            }
            ((ActivityCompat) this.f184b).onRequestPermissionsResult(this.f185c, this.f183a, iArr);
        }
    }

    /* renamed from: android.support.v4.app.a.a */
    public interface ActivityCompat {
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    public static void m205a(Activity activity, Intent intent, int i, Bundle bundle) {
        if (VERSION.SDK_INT >= 16) {
            ActivityCompatJB.m306a(activity, intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    public static void m204a(Activity activity) {
        if (VERSION.SDK_INT >= 16) {
            ActivityCompatJB.m305a(activity);
        } else {
            activity.finish();
        }
    }

    public static void m206a(Activity activity, String[] strArr, int i) {
        if (VERSION.SDK_INT >= 23) {
            ActivityCompat23.m303a(activity, strArr, i);
        } else if (activity instanceof ActivityCompat) {
            new Handler(Looper.getMainLooper()).post(new ActivityCompat(strArr, activity, i));
        }
    }
}
