package com.crashlytics.android.beta;

import android.content.Context;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.CurrentTimeProvider;
import io.fabric.sdk.android.services.p021b.IdManager;
import io.fabric.sdk.android.services.p023e.BetaSettingsData;
import io.fabric.sdk.android.services.p038d.PreferenceStore;

interface UpdatesController {
    void initialize(Context context, Beta beta, IdManager idManager, BetaSettingsData betaSettingsData, BuildProperties buildProperties, PreferenceStore preferenceStore, CurrentTimeProvider currentTimeProvider, HttpRequestFactory httpRequestFactory);

    boolean isActivityLifecycleTriggered();
}
