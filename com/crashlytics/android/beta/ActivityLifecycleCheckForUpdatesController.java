package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Activity;
import io.fabric.sdk.android.ActivityLifecycleManager.ActivityLifecycleManager;
import java.util.concurrent.ExecutorService;

@TargetApi(14)
class ActivityLifecycleCheckForUpdatesController extends AbstractCheckForUpdatesController {
    private final ActivityLifecycleManager callbacks;
    private final ExecutorService executorService;

    /* renamed from: com.crashlytics.android.beta.ActivityLifecycleCheckForUpdatesController.1 */
    class C00441 extends ActivityLifecycleManager {

        /* renamed from: com.crashlytics.android.beta.ActivityLifecycleCheckForUpdatesController.1.1 */
        class C00431 implements Runnable {
            C00431() {
            }

            public void run() {
                ActivityLifecycleCheckForUpdatesController.this.checkForUpdates();
            }
        }

        C00441() {
        }

        public void onActivityStarted(Activity activity) {
            if (ActivityLifecycleCheckForUpdatesController.this.signalExternallyReady()) {
                ActivityLifecycleCheckForUpdatesController.this.executorService.submit(new C00431());
            }
        }
    }

    public ActivityLifecycleCheckForUpdatesController(io.fabric.sdk.android.ActivityLifecycleManager activityLifecycleManager, ExecutorService executorService) {
        this.callbacks = new C00441();
        this.executorService = executorService;
        activityLifecycleManager.m5281a(this.callbacks);
    }

    public boolean isActivityLifecycleTriggered() {
        return true;
    }
}
