package com.crashlytics.android.core;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.DeliveryMechanism;
import io.fabric.sdk.android.services.p021b.IdManager;
import io.fabric.sdk.android.services.p023e.SessionSettingsData;
import io.fabric.sdk.android.services.p023e.Settings;
import io.fabric.sdk.android.services.p038d.FileStore;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsUncaughtExceptionHandler implements UncaughtExceptionHandler {
    private static final int ANALYZER_VERSION = 1;
    static final FilenameFilter ANY_SESSION_FILENAME_FILTER;
    private static final String EVENT_TYPE_CRASH = "crash";
    private static final String EVENT_TYPE_LOGGED = "error";
    private static final String GENERATOR_FORMAT = "Crashlytics Android SDK/%s";
    private static final String[] INITIAL_SESSION_PART_TAGS;
    static final String INVALID_CLS_CACHE_DIR = "invalidClsFiles";
    static final Comparator<File> LARGEST_FILE_NAME_FIRST;
    private static final int MAX_COMPLETE_SESSIONS_COUNT = 4;
    static final int MAX_INVALID_SESSIONS = 4;
    private static final int MAX_LOCAL_LOGGED_EXCEPTIONS = 64;
    static final int MAX_OPEN_SESSIONS = 8;
    static final int MAX_STACK_SIZE = 1024;
    static final int NUM_STACK_REPETITIONS_ALLOWED = 10;
    private static final Map<String, String> SEND_AT_CRASHTIME_HEADER;
    static final String SESSION_APP_TAG = "SessionApp";
    static final String SESSION_BEGIN_TAG = "BeginSession";
    static final String SESSION_DEVICE_TAG = "SessionDevice";
    static final String SESSION_EVENT_MISSING_BINARY_IMGS_TAG = "SessionMissingBinaryImages";
    static final String SESSION_FATAL_TAG = "SessionCrash";
    static final FilenameFilter SESSION_FILE_FILTER;
    private static final Pattern SESSION_FILE_PATTERN;
    private static final int SESSION_ID_LENGTH = 35;
    static final String SESSION_NON_FATAL_TAG = "SessionEvent";
    static final String SESSION_OS_TAG = "SessionOS";
    static final String SESSION_USER_TAG = "SessionUser";
    static final Comparator<File> SMALLEST_FILE_NAME_FIRST;
    private final CrashlyticsCore crashlyticsCore;
    private final UncaughtExceptionHandler defaultHandler;
    private final DevicePowerStateListener devicePowerStateListener;
    private final AtomicInteger eventCounter;
    private final CrashlyticsExecutorServiceWrapper executorServiceWrapper;
    private final FileStore fileStore;
    private final IdManager idManager;
    private final AtomicBoolean isHandlingException;
    private final LogFileManager logFileManager;
    private final StackTraceTrimmingStrategy stackTraceTrimmingStrategy;
    private final String unityVersion;

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.13 */
    class AnonymousClass13 implements FilenameFilter {
        final /* synthetic */ Set val$invalidSessionIds;

        AnonymousClass13(Set set) {
            this.val$invalidSessionIds = set;
        }

        public boolean accept(File file, String str) {
            if (str.length() < CrashlyticsUncaughtExceptionHandler.SESSION_ID_LENGTH) {
                return false;
            }
            return this.val$invalidSessionIds.contains(str.substring(0, CrashlyticsUncaughtExceptionHandler.SESSION_ID_LENGTH));
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.14 */
    class AnonymousClass14 implements Callable<Void> {
        final /* synthetic */ SessionEventData val$crashEventData;

        AnonymousClass14(SessionEventData sessionEventData) {
            this.val$crashEventData = sessionEventData;
        }

        public Void call() {
            if (!CrashlyticsUncaughtExceptionHandler.this.isHandlingException.get()) {
                CrashlyticsUncaughtExceptionHandler.this.doWriteExternalCrashEvent(this.val$crashEventData);
            }
            return null;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.1 */
    static class C00611 implements FilenameFilter {
        C00611() {
        }

        public boolean accept(File file, String str) {
            return str.length() == ClsFileOutputStream.SESSION_FILE_EXTENSION.length() + CrashlyticsUncaughtExceptionHandler.SESSION_ID_LENGTH && str.endsWith(ClsFileOutputStream.SESSION_FILE_EXTENSION);
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.2 */
    static class C00622 implements Comparator<File> {
        C00622() {
        }

        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.3 */
    static class C00633 implements Comparator<File> {
        C00633() {
        }

        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.4 */
    static class C00644 implements FilenameFilter {
        C00644() {
        }

        public boolean accept(File file, String str) {
            return CrashlyticsUncaughtExceptionHandler.SESSION_FILE_PATTERN.matcher(str).matches();
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.5 */
    class C00655 implements Callable<Void> {
        final /* synthetic */ Throwable val$ex;
        final /* synthetic */ Date val$now;
        final /* synthetic */ Thread val$thread;

        C00655(Date date, Thread thread, Throwable th) {
            this.val$now = date;
            this.val$thread = thread;
            this.val$ex = th;
        }

        public Void call() {
            CrashlyticsUncaughtExceptionHandler.this.handleUncaughtException(this.val$now, this.val$thread, this.val$ex);
            return null;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.6 */
    class C00666 implements Callable<Void> {
        final /* synthetic */ String val$msg;
        final /* synthetic */ long val$timestamp;

        C00666(long j, String str) {
            this.val$timestamp = j;
            this.val$msg = str;
        }

        public Void call() {
            if (!CrashlyticsUncaughtExceptionHandler.this.isHandlingException.get()) {
                CrashlyticsUncaughtExceptionHandler.this.logFileManager.writeToLog(this.val$timestamp, this.val$msg);
            }
            return null;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.7 */
    class C00677 implements Runnable {
        final /* synthetic */ Throwable val$ex;
        final /* synthetic */ Date val$now;
        final /* synthetic */ Thread val$thread;

        C00677(Date date, Thread thread, Throwable th) {
            this.val$now = date;
            this.val$thread = thread;
            this.val$ex = th;
        }

        public void run() {
            if (!CrashlyticsUncaughtExceptionHandler.this.isHandlingException.get()) {
                CrashlyticsUncaughtExceptionHandler.this.doWriteNonFatal(this.val$now, this.val$thread, this.val$ex);
            }
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.8 */
    class C00688 implements Callable<Void> {
        final /* synthetic */ String val$userEmail;
        final /* synthetic */ String val$userId;
        final /* synthetic */ String val$userName;

        C00688(String str, String str2, String str3) {
            this.val$userId = str;
            this.val$userName = str2;
            this.val$userEmail = str3;
        }

        public Void call() {
            new MetaDataStore(CrashlyticsUncaughtExceptionHandler.this.getFilesDir()).writeUserData(CrashlyticsUncaughtExceptionHandler.this.getCurrentSessionId(), new UserMetaData(this.val$userId, this.val$userName, this.val$userEmail));
            return null;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.9 */
    class C00699 implements Callable<Void> {
        final /* synthetic */ Map val$keyData;

        C00699(Map map) {
            this.val$keyData = map;
        }

        public Void call() {
            new MetaDataStore(CrashlyticsUncaughtExceptionHandler.this.getFilesDir()).writeKeyData(CrashlyticsUncaughtExceptionHandler.this.getCurrentSessionId(), this.val$keyData);
            return null;
        }
    }

    private static class AnySessionPartFileFilter implements FilenameFilter {
        private AnySessionPartFileFilter() {
        }

        public boolean accept(File file, String str) {
            return !CrashlyticsUncaughtExceptionHandler.SESSION_FILE_FILTER.accept(file, str) && CrashlyticsUncaughtExceptionHandler.SESSION_FILE_PATTERN.matcher(str).matches();
        }
    }

    static class FileNameContainsFilter implements FilenameFilter {
        private final String string;

        public FileNameContainsFilter(String str) {
            this.string = str;
        }

        public boolean accept(File file, String str) {
            return str.contains(this.string) && !str.endsWith(ClsFileOutputStream.IN_PROGRESS_SESSION_FILE_EXTENSION);
        }
    }

    static class InvalidPartFileFilter implements FilenameFilter {
        InvalidPartFileFilter() {
        }

        public boolean accept(File file, String str) {
            return ClsFileOutputStream.TEMP_FILENAME_FILTER.accept(file, str) || str.contains(CrashlyticsUncaughtExceptionHandler.SESSION_EVENT_MISSING_BINARY_IMGS_TAG);
        }
    }

    private static final class SendSessionRunnable implements Runnable {
        private final CrashlyticsCore crashlyticsCore;
        private final File fileToSend;

        public SendSessionRunnable(CrashlyticsCore crashlyticsCore, File file) {
            this.crashlyticsCore = crashlyticsCore;
            this.fileToSend = file;
        }

        public void run() {
            if (CommonUtils.m5456n(this.crashlyticsCore.getContext())) {
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Attempting to send crash report at time of crash...");
                CreateReportSpiCall createReportSpiCall = this.crashlyticsCore.getCreateReportSpiCall(Settings.m5615a().m5619b());
                if (createReportSpiCall != null) {
                    new ReportUploader(this.crashlyticsCore.getApiKey(), createReportSpiCall).forceUpload(new SessionReport(this.fileToSend, CrashlyticsUncaughtExceptionHandler.SEND_AT_CRASHTIME_HEADER));
                }
            }
        }
    }

    static class SessionPartFileFilter implements FilenameFilter {
        private final String sessionId;

        public SessionPartFileFilter(String str) {
            this.sessionId = str;
        }

        public boolean accept(File file, String str) {
            if (str.equals(this.sessionId + ClsFileOutputStream.SESSION_FILE_EXTENSION) || !str.contains(this.sessionId) || str.endsWith(ClsFileOutputStream.IN_PROGRESS_SESSION_FILE_EXTENSION)) {
                return false;
            }
            return true;
        }
    }

    static {
        SESSION_FILE_FILTER = new C00611();
        LARGEST_FILE_NAME_FIRST = new C00622();
        SMALLEST_FILE_NAME_FIRST = new C00633();
        ANY_SESSION_FILENAME_FILTER = new C00644();
        SESSION_FILE_PATTERN = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
        SEND_AT_CRASHTIME_HEADER = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
        String[] strArr = new String[MAX_INVALID_SESSIONS];
        strArr[0] = SESSION_USER_TAG;
        strArr[ANALYZER_VERSION] = SESSION_APP_TAG;
        strArr[2] = SESSION_OS_TAG;
        strArr[3] = SESSION_DEVICE_TAG;
        INITIAL_SESSION_PART_TAGS = strArr;
    }

    CrashlyticsUncaughtExceptionHandler(UncaughtExceptionHandler uncaughtExceptionHandler, CrashlyticsExecutorServiceWrapper crashlyticsExecutorServiceWrapper, IdManager idManager, UnityVersionProvider unityVersionProvider, FileStore fileStore, CrashlyticsCore crashlyticsCore) {
        this.eventCounter = new AtomicInteger(0);
        this.defaultHandler = uncaughtExceptionHandler;
        this.executorServiceWrapper = crashlyticsExecutorServiceWrapper;
        this.idManager = idManager;
        this.crashlyticsCore = crashlyticsCore;
        this.unityVersion = unityVersionProvider.getUnityVersion();
        this.fileStore = fileStore;
        this.isHandlingException = new AtomicBoolean(false);
        Context context = crashlyticsCore.getContext();
        this.logFileManager = new LogFileManager(context, fileStore);
        this.devicePowerStateListener = new DevicePowerStateListener(context);
        StackTraceTrimmingStrategy[] stackTraceTrimmingStrategyArr = new StackTraceTrimmingStrategy[ANALYZER_VERSION];
        stackTraceTrimmingStrategyArr[0] = new RemoveRepeatsStrategy(NUM_STACK_REPETITIONS_ALLOWED);
        this.stackTraceTrimmingStrategy = new MiddleOutFallbackStrategy(MAX_STACK_SIZE, stackTraceTrimmingStrategyArr);
    }

    public synchronized void uncaughtException(Thread thread, Throwable th) {
        this.isHandlingException.set(true);
        try {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
            this.devicePowerStateListener.dispose();
            this.executorServiceWrapper.executeSyncLoggingException(new C00655(new Date(), thread, th));
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Crashlytics completed exception processing. Invoking default exception handler.");
            this.defaultHandler.uncaughtException(thread, th);
            this.isHandlingException.set(false);
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the uncaught exception handler", e);
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Crashlytics completed exception processing. Invoking default exception handler.");
            this.defaultHandler.uncaughtException(thread, th);
            this.isHandlingException.set(false);
        } catch (Throwable th2) {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Crashlytics completed exception processing. Invoking default exception handler.");
            this.defaultHandler.uncaughtException(thread, th);
            this.isHandlingException.set(false);
        }
    }

    private void handleUncaughtException(Date date, Thread thread, Throwable th) {
        this.crashlyticsCore.createCrashMarker();
        writeFatal(date, thread, th);
        doCloseSessions();
        doOpenSession();
        trimSessionFiles();
        if (!this.crashlyticsCore.shouldPromptUserBeforeSendingCrashReports()) {
            sendSessionReports();
        }
    }

    boolean isHandlingException() {
        return this.isHandlingException.get();
    }

    File getInvalidFilesDir() {
        return new File(getFilesDir(), INVALID_CLS_CACHE_DIR);
    }

    void writeToLog(long j, String str) {
        this.executorServiceWrapper.executeAsync(new C00666(j, str));
    }

    void writeNonFatalException(Thread thread, Throwable th) {
        this.executorServiceWrapper.executeAsync(new C00677(new Date(), thread, th));
    }

    void cacheUserData(String str, String str2, String str3) {
        this.executorServiceWrapper.executeAsync(new C00688(str, str2, str3));
    }

    void cacheKeyData(Map<String, String> map) {
        this.executorServiceWrapper.executeAsync(new C00699(map));
    }

    void openSession() {
        this.executorServiceWrapper.executeAsync(new Callable<Void>() {
            public Void call() {
                CrashlyticsUncaughtExceptionHandler.this.doOpenSession();
                return null;
            }
        });
    }

    private String getCurrentSessionId() {
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        return listSortedSessionBeginFiles.length > 0 ? getSessionIdFromSessionFile(listSortedSessionBeginFiles[0]) : null;
    }

    private String getPreviousSessionId() {
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        return listSortedSessionBeginFiles.length > ANALYZER_VERSION ? getSessionIdFromSessionFile(listSortedSessionBeginFiles[ANALYZER_VERSION]) : null;
    }

    static String getSessionIdFromSessionFile(File file) {
        return file.getName().substring(0, SESSION_ID_LENGTH);
    }

    boolean hasOpenSession() {
        return listSessionBeginFiles().length > 0;
    }

    boolean finalizeSessions() {
        return ((Boolean) this.executorServiceWrapper.executeSyncLoggingException(new Callable<Boolean>() {
            public Boolean call() {
                if (CrashlyticsUncaughtExceptionHandler.this.isHandlingException.get()) {
                    Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Skipping session finalization because a crash has already occurred.");
                    return Boolean.FALSE;
                }
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Finalizing previously open sessions.");
                CrashlyticsUncaughtExceptionHandler.this.doCloseSessions(true);
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Closed all previously open sessions");
                return Boolean.TRUE;
            }
        })).booleanValue();
    }

    private void doOpenSession() {
        Date date = new Date();
        String clsuuid = new CLSUUID(this.idManager).toString();
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Opening an new session with ID " + clsuuid);
        writeBeginSession(clsuuid, date);
        writeSessionApp(clsuuid);
        writeSessionOS(clsuuid);
        writeSessionDevice(clsuuid);
        this.logFileManager.setCurrentSession(clsuuid);
    }

    void doCloseSessions() {
        doCloseSessions(false);
    }

    private void doCloseSessions(boolean z) {
        int i = z ? ANALYZER_VERSION : 0;
        trimOpenSessions(i + MAX_OPEN_SESSIONS);
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        if (listSortedSessionBeginFiles.length <= i) {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "No open sessions to be closed.");
            return;
        }
        writeSessionUser(getSessionIdFromSessionFile(listSortedSessionBeginFiles[i]));
        CrashlyticsCore crashlyticsCore = this.crashlyticsCore;
        SessionSettingsData sessionSettingsData = CrashlyticsCore.getSessionSettingsData();
        if (sessionSettingsData == null) {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Unable to close session. Settings are not loaded.");
        } else {
            closeOpenSessions(listSortedSessionBeginFiles, i, sessionSettingsData.f3657c);
        }
    }

    private void closeOpenSessions(File[] fileArr, int i, int i2) {
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Closing open sessions.");
        while (i < fileArr.length) {
            File file = fileArr[i];
            String sessionIdFromSessionFile = getSessionIdFromSessionFile(file);
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Closing session: " + sessionIdFromSessionFile);
            writeSessionPartsToSessionFile(file, sessionIdFromSessionFile, i2);
            i += ANALYZER_VERSION;
        }
    }

    private void closeWithoutRenamingOrLog(ClsFileOutputStream clsFileOutputStream) {
        if (clsFileOutputStream != null) {
            try {
                clsFileOutputStream.closeInProgressStream();
            } catch (Throwable e) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Error closing session file stream in the presence of an exception", e);
            }
        }
    }

    private void deleteSessionPartFilesFor(String str) {
        File[] listSessionPartFilesFor = listSessionPartFilesFor(str);
        int length = listSessionPartFilesFor.length;
        for (int i = 0; i < length; i += ANALYZER_VERSION) {
            listSessionPartFilesFor[i].delete();
        }
    }

    private File[] listSessionPartFilesFor(String str) {
        return listFilesMatching(new SessionPartFileFilter(str));
    }

    private File[] listCompleteSessionFiles() {
        return listFilesMatching(SESSION_FILE_FILTER);
    }

    File[] listSessionBeginFiles() {
        return listFilesMatching(new FileNameContainsFilter(SESSION_BEGIN_TAG));
    }

    private File[] listSortedSessionBeginFiles() {
        File[] listSessionBeginFiles = listSessionBeginFiles();
        Arrays.sort(listSessionBeginFiles, LARGEST_FILE_NAME_FIRST);
        return listSessionBeginFiles;
    }

    private File[] listFilesMatching(FilenameFilter filenameFilter) {
        return listFilesMatching(getFilesDir(), filenameFilter);
    }

    private File[] listFilesMatching(File file, FilenameFilter filenameFilter) {
        return ensureFileArrayNotNull(file.listFiles(filenameFilter));
    }

    private File[] listFiles(File file) {
        return ensureFileArrayNotNull(file.listFiles());
    }

    private File[] ensureFileArrayNotNull(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    private void trimSessionEventFiles(String str, int i) {
        Utils.capFileCount(getFilesDir(), new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG), i, SMALLEST_FILE_NAME_FIRST);
    }

    void trimSessionFiles() {
        Utils.capFileCount(getFilesDir(), SESSION_FILE_FILTER, MAX_INVALID_SESSIONS, SMALLEST_FILE_NAME_FIRST);
    }

    private void trimOpenSessions(int i) {
        Set hashSet = new HashSet();
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        int min = Math.min(i, listSortedSessionBeginFiles.length);
        for (int i2 = 0; i2 < min; i2 += ANALYZER_VERSION) {
            hashSet.add(getSessionIdFromSessionFile(listSortedSessionBeginFiles[i2]));
        }
        this.logFileManager.discardOldLogFiles(hashSet);
        retainSessions(listFilesMatching(new AnySessionPartFileFilter()), hashSet);
    }

    private void retainSessions(File[] fileArr, Set<String> set) {
        int length = fileArr.length;
        int i = 0;
        while (i < length) {
            File file = fileArr[i];
            String name = file.getName();
            Matcher matcher = SESSION_FILE_PATTERN.matcher(name);
            if (matcher.matches()) {
                if (!set.contains(matcher.group(ANALYZER_VERSION))) {
                    Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Trimming session file: " + name);
                    file.delete();
                }
                i += ANALYZER_VERSION;
            } else {
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Deleting unknown file: " + name);
                file.delete();
                return;
            }
        }
    }

    private File[] getTrimmedNonFatalFiles(String str, File[] fileArr, int i) {
        if (fileArr.length <= i) {
            return fileArr;
        }
        Logger h = Fabric.m5322h();
        String str2 = CrashlyticsCore.TAG;
        Object[] objArr = new Object[ANALYZER_VERSION];
        objArr[0] = Integer.valueOf(i);
        h.m5284a(str2, String.format(Locale.US, "Trimming down to %d logged exceptions.", objArr));
        trimSessionEventFiles(str, i);
        return listFilesMatching(new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG));
    }

    void cleanInvalidTempFiles() {
        this.executorServiceWrapper.executeAsync(new Runnable() {
            public void run() {
                CrashlyticsUncaughtExceptionHandler.this.doCleanInvalidTempFiles(CrashlyticsUncaughtExceptionHandler.this.listFilesMatching(new InvalidPartFileFilter()));
            }
        });
    }

    void doCleanInvalidTempFiles(File[] fileArr) {
        int i = 0;
        Set hashSet = new HashSet();
        int length = fileArr.length;
        for (int i2 = 0; i2 < length; i2 += ANALYZER_VERSION) {
            File file = fileArr[i2];
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Found invalid session part file: " + file);
            hashSet.add(getSessionIdFromSessionFile(file));
        }
        if (!hashSet.isEmpty()) {
            File invalidFilesDir = getInvalidFilesDir();
            if (!invalidFilesDir.exists()) {
                invalidFilesDir.mkdir();
            }
            File[] listFilesMatching = listFilesMatching(new AnonymousClass13(hashSet));
            length = listFilesMatching.length;
            while (i < length) {
                file = listFilesMatching[i];
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Moving session file: " + file);
                if (!file.renameTo(new File(invalidFilesDir, file.getName()))) {
                    Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Could not move session file. Deleting " + file);
                    file.delete();
                }
                i += ANALYZER_VERSION;
            }
            trimInvalidSessionFiles();
        }
    }

    private void trimInvalidSessionFiles() {
        File invalidFilesDir = getInvalidFilesDir();
        if (invalidFilesDir.exists()) {
            File[] listFilesMatching = listFilesMatching(invalidFilesDir, new InvalidPartFileFilter());
            Arrays.sort(listFilesMatching, Collections.reverseOrder());
            Set hashSet = new HashSet();
            for (int i = 0; i < listFilesMatching.length && hashSet.size() < MAX_INVALID_SESSIONS; i += ANALYZER_VERSION) {
                hashSet.add(getSessionIdFromSessionFile(listFilesMatching[i]));
            }
            retainSessions(listFiles(invalidFilesDir), hashSet);
        }
    }

    private void writeFatal(Date date, Thread thread, Throwable th) {
        Throwable e;
        Closeable closeable;
        Flushable flushable = null;
        try {
            String currentSessionId = getCurrentSessionId();
            if (currentSessionId == null) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Tried to write a fatal exception while no session was open.", null);
                CommonUtils.m5433a(null, "Failed to flush to session begin file.");
                CommonUtils.m5432a(null, "Failed to close fatal exception file output stream.");
                return;
            }
            CrashlyticsCore.recordFatalExceptionEvent(currentSessionId, th.getClass().getName());
            Closeable clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), currentSessionId + SESSION_FATAL_TAG);
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                writeSessionEvent(flushable, date, thread, th, EVENT_TYPE_CRASH, true);
                CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                closeable = clsFileOutputStream;
                try {
                    Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the fatal exception logger", e);
                    CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                    CommonUtils.m5432a(closeable, "Failed to close fatal exception file output stream.");
                } catch (Throwable th2) {
                    e = th2;
                    CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                    CommonUtils.m5432a(closeable, "Failed to close fatal exception file output stream.");
                    throw e;
                }
            } catch (Throwable th3) {
                e = th3;
                closeable = clsFileOutputStream;
                CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                CommonUtils.m5432a(closeable, "Failed to close fatal exception file output stream.");
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            closeable = null;
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the fatal exception logger", e);
            CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
            CommonUtils.m5432a(closeable, "Failed to close fatal exception file output stream.");
        } catch (Throwable th4) {
            e = th4;
            closeable = null;
            CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
            CommonUtils.m5432a(closeable, "Failed to close fatal exception file output stream.");
            throw e;
        }
    }

    void writeExternalCrashEvent(SessionEventData sessionEventData) {
        this.executorServiceWrapper.executeAsync(new AnonymousClass14(sessionEventData));
    }

    private void doWriteExternalCrashEvent(SessionEventData sessionEventData) {
        Throwable e;
        Object obj = ANALYZER_VERSION;
        Flushable flushable = null;
        Closeable clsFileOutputStream;
        try {
            String previousSessionId = getPreviousSessionId();
            if (previousSessionId == null) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Tried to write a native crash while no session was open.", null);
                CommonUtils.m5433a(null, "Failed to flush to session begin file.");
                CommonUtils.m5432a(null, "Failed to close fatal exception file output stream.");
                return;
            }
            CrashlyticsCore.recordFatalExceptionEvent(previousSessionId, String.format(Locale.US, "<native-crash [%s (%s)]>", new Object[]{sessionEventData.signal.code, sessionEventData.signal.name}));
            if (sessionEventData.binaryImages == null || sessionEventData.binaryImages.length <= 0) {
                obj = null;
            }
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), previousSessionId + (obj != null ? SESSION_FATAL_TAG : SESSION_EVENT_MISSING_BINARY_IMGS_TAG));
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                NativeCrashWriter.writeNativeCrash(sessionEventData, new LogFileManager(this.crashlyticsCore.getContext(), this.fileStore, previousSessionId), new MetaDataStore(getFilesDir()).readKeyData(previousSessionId), flushable);
                CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the native crash logger", e);
                    CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                    CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
                } catch (Throwable th) {
                    e = th;
                    CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                    CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
                    throw e;
                }
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the native crash logger", e);
            CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
        } catch (Throwable th2) {
            e = th2;
            clsFileOutputStream = null;
            CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close fatal exception file output stream.");
            throw e;
        }
    }

    private void doWriteNonFatal(Date date, Thread thread, Throwable th) {
        Throwable e;
        Closeable closeable;
        Flushable flushable = null;
        String currentSessionId = getCurrentSessionId();
        if (currentSessionId == null) {
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Tried to write a non-fatal exception while no session was open.", null);
            return;
        }
        CrashlyticsCore.recordLoggedExceptionEvent(currentSessionId, th.getClass().getName());
        try {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Crashlytics is logging non-fatal exception \"" + th + "\" from thread " + thread.getName());
            Closeable clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), currentSessionId + SESSION_NON_FATAL_TAG + CommonUtils.m5419a(this.eventCounter.getAndIncrement()));
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                writeSessionEvent(flushable, date, thread, th, EVENT_TYPE_LOGGED, false);
                CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close non-fatal file output stream.");
            } catch (Exception e2) {
                e = e2;
                closeable = clsFileOutputStream;
                try {
                    Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the non-fatal exception logger", e);
                    CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
                    CommonUtils.m5432a(closeable, "Failed to close non-fatal file output stream.");
                    trimSessionEventFiles(currentSessionId, MAX_LOCAL_LOGGED_EXCEPTIONS);
                } catch (Throwable th2) {
                    e = th2;
                    CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
                    CommonUtils.m5432a(closeable, "Failed to close non-fatal file output stream.");
                    throw e;
                }
            } catch (Throwable th3) {
                e = th3;
                closeable = clsFileOutputStream;
                CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
                CommonUtils.m5432a(closeable, "Failed to close non-fatal file output stream.");
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            closeable = null;
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred in the non-fatal exception logger", e);
            CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
            CommonUtils.m5432a(closeable, "Failed to close non-fatal file output stream.");
            trimSessionEventFiles(currentSessionId, MAX_LOCAL_LOGGED_EXCEPTIONS);
        } catch (Throwable th4) {
            e = th4;
            closeable = null;
            CommonUtils.m5433a(flushable, "Failed to flush to non-fatal file.");
            CommonUtils.m5432a(closeable, "Failed to close non-fatal file output stream.");
            throw e;
        }
        try {
            trimSessionEventFiles(currentSessionId, MAX_LOCAL_LOGGED_EXCEPTIONS);
        } catch (Throwable e4) {
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "An error occurred when trimming non-fatal files.", e4);
        }
    }

    private void writeBeginSession(String str, Date date) {
        Throwable th;
        Flushable flushable = null;
        Closeable clsFileOutputStream;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_BEGIN_TAG);
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                Locale locale = Locale.US;
                String str2 = GENERATOR_FORMAT;
                Object[] objArr = new Object[ANALYZER_VERSION];
                objArr[0] = this.crashlyticsCore.getVersion();
                SessionProtobufHelper.writeBeginSession(flushable, str, String.format(locale, str2, objArr), date.getTime() / 1000);
                CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close begin session file.");
            } catch (Throwable th2) {
                th = th2;
                CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close begin session file.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            clsFileOutputStream = null;
            CommonUtils.m5433a(flushable, "Failed to flush to session begin file.");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close begin session file.");
            throw th;
        }
    }

    private void writeSessionApp(String str) {
        Closeable closeable;
        Throwable th;
        Flushable flushable = null;
        try {
            Closeable clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_APP_TAG);
            try {
                Flushable newInstance = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                try {
                    SessionProtobufHelper.writeSessionApp(newInstance, this.idManager.m5473c(), this.crashlyticsCore.getApiKey(), this.crashlyticsCore.getVersionCode(), this.crashlyticsCore.getVersionName(), this.idManager.m5472b(), DeliveryMechanism.m5460a(this.crashlyticsCore.getInstallerPackageName()).m5461a(), this.unityVersion);
                    CommonUtils.m5433a(newInstance, "Failed to flush to session app file.");
                    CommonUtils.m5432a(clsFileOutputStream, "Failed to close session app file.");
                } catch (Throwable th2) {
                    closeable = clsFileOutputStream;
                    Flushable flushable2 = newInstance;
                    th = th2;
                    flushable = flushable2;
                    CommonUtils.m5433a(flushable, "Failed to flush to session app file.");
                    CommonUtils.m5432a(closeable, "Failed to close session app file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                closeable = clsFileOutputStream;
                CommonUtils.m5433a(flushable, "Failed to flush to session app file.");
                CommonUtils.m5432a(closeable, "Failed to close session app file.");
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            closeable = null;
            CommonUtils.m5433a(flushable, "Failed to flush to session app file.");
            CommonUtils.m5432a(closeable, "Failed to close session app file.");
            throw th;
        }
    }

    private void writeSessionOS(String str) {
        Closeable clsFileOutputStream;
        Throwable th;
        Flushable flushable = null;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_OS_TAG);
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                SessionProtobufHelper.writeSessionOS(flushable, CommonUtils.m5449g(this.crashlyticsCore.getContext()));
                CommonUtils.m5433a(flushable, "Failed to flush to session OS file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close session OS file.");
            } catch (Throwable th2) {
                th = th2;
                CommonUtils.m5433a(flushable, "Failed to flush to session OS file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close session OS file.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            clsFileOutputStream = null;
            CommonUtils.m5433a(flushable, "Failed to flush to session OS file.");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close session OS file.");
            throw th;
        }
    }

    private void writeSessionDevice(String str) {
        Throwable th;
        Closeable closeable = null;
        Flushable flushable = null;
        try {
            OutputStream clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_DEVICE_TAG);
            try {
                flushable = CodedOutputStream.newInstance(clsFileOutputStream);
                Context context = this.crashlyticsCore.getContext();
                StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
                SessionProtobufHelper.writeSessionDevice(flushable, this.idManager.m5478h(), CommonUtils.m5413a(), Build.MODEL, Runtime.getRuntime().availableProcessors(), CommonUtils.m5436b(), ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize()), CommonUtils.m5448f(context), this.idManager.m5479i(), CommonUtils.m5450h(context), Build.MANUFACTURER, Build.PRODUCT);
                CommonUtils.m5433a(flushable, "Failed to flush session device info.");
                CommonUtils.m5432a((Closeable) clsFileOutputStream, "Failed to close session device file.");
            } catch (Throwable th2) {
                th = th2;
                Object obj = clsFileOutputStream;
                CommonUtils.m5433a(flushable, "Failed to flush session device info.");
                CommonUtils.m5432a(closeable, "Failed to close session device file.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            CommonUtils.m5433a(flushable, "Failed to flush session device info.");
            CommonUtils.m5432a(closeable, "Failed to close session device file.");
            throw th;
        }
    }

    private void writeSessionUser(String str) {
        Closeable clsFileOutputStream;
        Throwable th;
        Flushable flushable = null;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_USER_TAG);
            try {
                flushable = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                UserMetaData userMetaData = getUserMetaData(str);
                if (userMetaData.isEmpty()) {
                    CommonUtils.m5433a(flushable, "Failed to flush session user file.");
                    CommonUtils.m5432a(clsFileOutputStream, "Failed to close session user file.");
                    return;
                }
                SessionProtobufHelper.writeSessionUser(flushable, userMetaData.id, userMetaData.name, userMetaData.email);
                CommonUtils.m5433a(flushable, "Failed to flush session user file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close session user file.");
            } catch (Throwable th2) {
                th = th2;
                CommonUtils.m5433a(flushable, "Failed to flush session user file.");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close session user file.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            clsFileOutputStream = null;
            CommonUtils.m5433a(flushable, "Failed to flush session user file.");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close session user file.");
            throw th;
        }
    }

    private void writeSessionEvent(CodedOutputStream codedOutputStream, Date date, Thread thread, Throwable th, String str, boolean z) {
        Thread[] threadArr;
        Map treeMap;
        TrimmedThrowableData trimmedThrowableData = new TrimmedThrowableData(th, this.stackTraceTrimmingStrategy);
        Context context = this.crashlyticsCore.getContext();
        long time = date.getTime() / 1000;
        Float c = CommonUtils.m5442c(context);
        int a = CommonUtils.m5415a(context, this.devicePowerStateListener.isPowerConnected());
        boolean d = CommonUtils.m5446d(context);
        int i = context.getResources().getConfiguration().orientation;
        long b = CommonUtils.m5436b() - CommonUtils.m5437b(context);
        long b2 = CommonUtils.m5438b(Environment.getDataDirectory().getPath());
        RunningAppProcessInfo a2 = CommonUtils.m5417a(context.getPackageName(), context);
        List linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = trimmedThrowableData.stacktrace;
        String buildId = this.crashlyticsCore.getBuildId();
        String c2 = this.idManager.m5473c();
        if (z) {
            Map allStackTraces = Thread.getAllStackTraces();
            threadArr = new Thread[allStackTraces.size()];
            int i2 = 0;
            for (Entry entry : allStackTraces.entrySet()) {
                threadArr[i2] = (Thread) entry.getKey();
                linkedList.add(this.stackTraceTrimmingStrategy.getTrimmedStackTrace((StackTraceElement[]) entry.getValue()));
                i2 += ANALYZER_VERSION;
            }
        } else {
            threadArr = new Thread[0];
        }
        if (CommonUtils.m5435a(context, "com.crashlytics.CollectCustomKeys", true)) {
            Map attributes = this.crashlyticsCore.getAttributes();
            treeMap = (attributes == null || attributes.size() <= ANALYZER_VERSION) ? attributes : new TreeMap(attributes);
        } else {
            treeMap = new TreeMap();
        }
        SessionProtobufHelper.writeSessionEvent(codedOutputStream, time, str, trimmedThrowableData, thread, stackTraceElementArr, threadArr, linkedList, treeMap, this.logFileManager, a2, i, c2, buildId, c, a, d, b, b2);
    }

    private void writeSessionPartsToSessionFile(File file, String str, int i) {
        boolean z;
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Collecting session parts for ID " + str);
        File[] listFilesMatching = listFilesMatching(new FileNameContainsFilter(str + SESSION_FATAL_TAG));
        boolean z2 = listFilesMatching != null && listFilesMatching.length > 0;
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, String.format(Locale.US, "Session %s has fatal exception: %s", new Object[]{str, Boolean.valueOf(z2)}));
        File[] listFilesMatching2 = listFilesMatching(new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG));
        if (listFilesMatching2 == null || listFilesMatching2.length <= 0) {
            z = false;
        } else {
            z = true;
        }
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, String.format(Locale.US, "Session %s has non-fatal exceptions: %s", new Object[]{str, Boolean.valueOf(z)}));
        if (z2 || z) {
            synthesizeSessionFile(file, str, getTrimmedNonFatalFiles(str, listFilesMatching2, i), z2 ? listFilesMatching[0] : null);
        } else {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "No events present for session ID " + str);
        }
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Removing session part files for ID " + str);
        deleteSessionPartFilesFor(str);
    }

    private void synthesizeSessionFile(File file, String str, File[] fileArr, File file2) {
        Closeable clsFileOutputStream;
        Throwable e;
        boolean z = true;
        if (file2 == null) {
            z = false;
        }
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str);
            try {
                Flushable newInstance = CodedOutputStream.newInstance((OutputStream) clsFileOutputStream);
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Collecting SessionStart data for session ID " + str);
                writeToCosFromFile(newInstance, file);
                newInstance.writeUInt64(MAX_INVALID_SESSIONS, new Date().getTime() / 1000);
                newInstance.writeBool(5, z);
                newInstance.writeUInt32(11, ANALYZER_VERSION);
                newInstance.writeEnum(12, 3);
                writeInitialPartsTo(newInstance, str);
                writeNonFatalEventsTo(newInstance, fileArr, str);
                if (z) {
                    writeToCosFromFile(newInstance, file2);
                }
                CommonUtils.m5433a(newInstance, "Error flushing session file stream");
                CommonUtils.m5432a(clsFileOutputStream, "Failed to close CLS file");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Failed to write session file for session ID: " + str, e);
                    CommonUtils.m5433a(null, "Error flushing session file stream");
                    closeWithoutRenamingOrLog(clsFileOutputStream);
                } catch (Throwable th) {
                    e = th;
                    CommonUtils.m5433a(null, "Error flushing session file stream");
                    CommonUtils.m5432a(clsFileOutputStream, "Failed to close CLS file");
                    throw e;
                }
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Failed to write session file for session ID: " + str, e);
            CommonUtils.m5433a(null, "Error flushing session file stream");
            closeWithoutRenamingOrLog(clsFileOutputStream);
        } catch (Throwable th2) {
            e = th2;
            clsFileOutputStream = null;
            CommonUtils.m5433a(null, "Error flushing session file stream");
            CommonUtils.m5432a(clsFileOutputStream, "Failed to close CLS file");
            throw e;
        }
    }

    private static void writeNonFatalEventsTo(CodedOutputStream codedOutputStream, File[] fileArr, String str) {
        Arrays.sort(fileArr, CommonUtils.f3482a);
        int length = fileArr.length;
        for (int i = 0; i < length; i += ANALYZER_VERSION) {
            File file = fileArr[i];
            try {
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[]{str, file.getName()}));
                writeToCosFromFile(codedOutputStream, file);
            } catch (Throwable e) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Error writting non-fatal to session.", e);
            }
        }
    }

    private void writeInitialPartsTo(CodedOutputStream codedOutputStream, String str) {
        String[] strArr = INITIAL_SESSION_PART_TAGS;
        int length = strArr.length;
        for (int i = 0; i < length; i += ANALYZER_VERSION) {
            String str2 = strArr[i];
            File[] listFilesMatching = listFilesMatching(new FileNameContainsFilter(str + str2));
            if (listFilesMatching.length == 0) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Can't find " + str2 + " data for session ID " + str, null);
            } else {
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Collecting " + str2 + " data for session ID " + str);
                writeToCosFromFile(codedOutputStream, listFilesMatching[0]);
            }
        }
    }

    private static void writeToCosFromFile(CodedOutputStream codedOutputStream, File file) {
        Throwable th;
        if (file.exists()) {
            Closeable fileInputStream;
            try {
                fileInputStream = new FileInputStream(file);
                try {
                    copyToCodedOutputStream(fileInputStream, codedOutputStream, (int) file.length());
                    CommonUtils.m5432a(fileInputStream, "Failed to close file input stream.");
                    return;
                } catch (Throwable th2) {
                    th = th2;
                    CommonUtils.m5432a(fileInputStream, "Failed to close file input stream.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fileInputStream = null;
                CommonUtils.m5432a(fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        }
        Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Tried to include a file that doesn't exist: " + file.getName(), null);
    }

    private static void copyToCodedOutputStream(InputStream inputStream, CodedOutputStream codedOutputStream, int i) {
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < bArr.length) {
            int read = inputStream.read(bArr, i2, bArr.length - i2);
            if (read < 0) {
                break;
            }
            i2 += read;
        }
        codedOutputStream.writeRawBytes(bArr);
    }

    private UserMetaData getUserMetaData(String str) {
        return isHandlingException() ? new UserMetaData(this.crashlyticsCore.getUserIdentifier(), this.crashlyticsCore.getUserName(), this.crashlyticsCore.getUserEmail()) : new MetaDataStore(getFilesDir()).readUserData(str);
    }

    private void sendSessionReports() {
        File[] listCompleteSessionFiles = listCompleteSessionFiles();
        int length = listCompleteSessionFiles.length;
        for (int i = 0; i < length; i += ANALYZER_VERSION) {
            this.executorServiceWrapper.executeAsync(new SendSessionRunnable(this.crashlyticsCore, listCompleteSessionFiles[i]));
        }
    }

    private File getFilesDir() {
        return this.fileStore.m5566a();
    }
}
