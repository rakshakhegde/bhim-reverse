package com.crashlytics.android.core;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import io.fabric.sdk.android.services.p023e.PromptSettingsData;
import java.util.concurrent.CountDownLatch;

class CrashPromptDialog {
    private final Builder dialog;
    private final OptInLatch latch;

    /* renamed from: com.crashlytics.android.core.CrashPromptDialog.1 */
    static class C00471 implements OnClickListener {
        final /* synthetic */ OptInLatch val$latch;

        C00471(OptInLatch optInLatch) {
            this.val$latch = optInLatch;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.val$latch.setOptIn(true);
            dialogInterface.dismiss();
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashPromptDialog.2 */
    static class C00482 implements OnClickListener {
        final /* synthetic */ OptInLatch val$latch;

        C00482(OptInLatch optInLatch) {
            this.val$latch = optInLatch;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.val$latch.setOptIn(false);
            dialogInterface.dismiss();
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashPromptDialog.3 */
    static class C00493 implements OnClickListener {
        final /* synthetic */ AlwaysSendCallback val$alwaysSendCallback;
        final /* synthetic */ OptInLatch val$latch;

        C00493(AlwaysSendCallback alwaysSendCallback, OptInLatch optInLatch) {
            this.val$alwaysSendCallback = alwaysSendCallback;
            this.val$latch = optInLatch;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.val$alwaysSendCallback.sendUserReportsWithoutPrompting(true);
            this.val$latch.setOptIn(true);
            dialogInterface.dismiss();
        }
    }

    interface AlwaysSendCallback {
        void sendUserReportsWithoutPrompting(boolean z);
    }

    private static class OptInLatch {
        private final CountDownLatch latch;
        private boolean send;

        private OptInLatch() {
            this.send = false;
            this.latch = new CountDownLatch(1);
        }

        void setOptIn(boolean z) {
            this.send = z;
            this.latch.countDown();
        }

        boolean getOptIn() {
            return this.send;
        }

        void await() {
            try {
                this.latch.await();
            } catch (InterruptedException e) {
            }
        }
    }

    public static CrashPromptDialog create(Activity activity, PromptSettingsData promptSettingsData, AlwaysSendCallback alwaysSendCallback) {
        OptInLatch optInLatch = new OptInLatch();
        DialogStringResolver dialogStringResolver = new DialogStringResolver(activity, promptSettingsData);
        Builder builder = new Builder(activity);
        View createDialogView = createDialogView(activity, dialogStringResolver.getMessage());
        builder.setView(createDialogView).setTitle(dialogStringResolver.getTitle()).setCancelable(false).setNeutralButton(dialogStringResolver.getSendButtonTitle(), new C00471(optInLatch));
        if (promptSettingsData.f3651d) {
            builder.setNegativeButton(dialogStringResolver.getCancelButtonTitle(), new C00482(optInLatch));
        }
        if (promptSettingsData.f3653f) {
            builder.setPositiveButton(dialogStringResolver.getAlwaysSendButtonTitle(), new C00493(alwaysSendCallback, optInLatch));
        }
        return new CrashPromptDialog(builder, optInLatch);
    }

    private static ScrollView createDialogView(Activity activity, String str) {
        float f = activity.getResources().getDisplayMetrics().density;
        int dipsToPixels = dipsToPixels(f, 5);
        View textView = new TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(dipsToPixels, dipsToPixels, dipsToPixels, dipsToPixels);
        textView.setFocusable(false);
        ScrollView scrollView = new ScrollView(activity);
        scrollView.setPadding(dipsToPixels(f, 14), dipsToPixels(f, 2), dipsToPixels(f, 10), dipsToPixels(f, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    private static int dipsToPixels(float f, int i) {
        return (int) (((float) i) * f);
    }

    private CrashPromptDialog(Builder builder, OptInLatch optInLatch) {
        this.latch = optInLatch;
        this.dialog = builder;
    }

    public void show() {
        this.dialog.show();
    }

    public void await() {
        this.latch.await();
    }

    public boolean getOptIn() {
        return this.latch.getOptIn();
    }
}
