package com.crashlytics.android.core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.internal.CrashEventDataProvider;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.concurrency.DependsOn;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.PriorityCallable;
import io.fabric.sdk.android.services.concurrency.Task;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.network.PinningInfoProvider;
import io.fabric.sdk.android.services.p021b.ApiKey;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.Crash.Crash;
import io.fabric.sdk.android.services.p021b.ExecutorUtils;
import io.fabric.sdk.android.services.p023e.PromptSettingsData;
import io.fabric.sdk.android.services.p023e.SessionSettingsData;
import io.fabric.sdk.android.services.p023e.Settings.Settings;
import io.fabric.sdk.android.services.p023e.SettingsData;
import io.fabric.sdk.android.services.p038d.FileStore;
import io.fabric.sdk.android.services.p038d.FileStoreImpl;
import io.fabric.sdk.android.services.p038d.PreferenceStore;
import io.fabric.sdk.android.services.p038d.PreferenceStoreImpl;
import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;

@DependsOn(a = {CrashEventDataProvider.class})
public class CrashlyticsCore extends Kit<Void> {
    static final float CLS_DEFAULT_PROCESS_DELAY = 1.0f;
    static final String COLLECT_CUSTOM_KEYS = "com.crashlytics.CollectCustomKeys";
    static final String COLLECT_CUSTOM_LOGS = "com.crashlytics.CollectCustomLogs";
    static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
    static final String CRASHLYTICS_REQUIRE_BUILD_ID = "com.crashlytics.RequireBuildId";
    static final boolean CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT = true;
    static final String CRASH_MARKER_FILE_NAME = "crash_marker";
    static final int DEFAULT_MAIN_HANDLER_TIMEOUT_SEC = 4;
    private static final String INITIALIZATION_MARKER_FILE_NAME = "initialization_marker";
    static final int MAX_ATTRIBUTES = 64;
    static final int MAX_ATTRIBUTE_SIZE = 1024;
    private static final String MISSING_BUILD_ID_MSG = "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.";
    private static final String PREF_ALWAYS_SEND_REPORTS_KEY = "always_send_reports_opt_in";
    private static final boolean SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT = false;
    public static final String TAG = "CrashlyticsCore";
    private String apiKey;
    private final ConcurrentHashMap<String, String> attributes;
    private String buildId;
    private CrashlyticsFileMarker crashMarker;
    private float delay;
    private boolean disabled;
    private CrashlyticsExecutorServiceWrapper executorServiceWrapper;
    private CrashEventDataProvider externalCrashEventDataProvider;
    private FileStore fileStore;
    private CrashlyticsUncaughtExceptionHandler handler;
    private HttpRequestFactory httpRequestFactory;
    private CrashlyticsFileMarker initializationMarker;
    private String installerPackageName;
    private CrashlyticsListener listener;
    private String packageName;
    private final PinningInfoProvider pinningInfo;
    private File sdkDir;
    private final long startTime;
    private String userEmail;
    private String userId;
    private String userName;
    private String versionCode;
    private String versionName;

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.1 */
    class C00511 extends PriorityCallable<Void> {
        C00511() {
        }

        public Void call() {
            return CrashlyticsCore.this.doInBackground();
        }

        public Priority getPriority() {
            return Priority.IMMEDIATE;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.2 */
    class C00522 implements Callable<Void> {
        C00522() {
        }

        public Void call() {
            CrashlyticsCore.this.initializationMarker.create();
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Initialization marker file created.");
            return null;
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.3 */
    class C00533 implements Callable<Boolean> {
        C00533() {
        }

        public Boolean call() {
            try {
                boolean remove = CrashlyticsCore.this.initializationMarker.remove();
                Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Initialization marker file removed: " + remove);
                return Boolean.valueOf(remove);
            } catch (Throwable e) {
                Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Problem encountered deleting Crashlytics initialization marker.", e);
                return Boolean.valueOf(CrashlyticsCore.SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT);
            }
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.4 */
    class C00544 implements Callable<Boolean> {
        C00544() {
        }

        public Boolean call() {
            return Boolean.valueOf(CrashlyticsCore.this.initializationMarker.isPresent());
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.5 */
    class C00555 implements Settings<Boolean> {
        C00555() {
        }

        public Boolean usingSettings(SettingsData settingsData) {
            boolean z = CrashlyticsCore.SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
            if (!settingsData.f3673d.f3640a) {
                return Boolean.valueOf(CrashlyticsCore.SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT);
            }
            if (!CrashlyticsCore.this.shouldSendReportsWithoutPrompting()) {
                z = CrashlyticsCore.CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
            }
            return Boolean.valueOf(z);
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.6 */
    class C00566 implements Settings<Boolean> {
        C00566() {
        }

        public Boolean usingSettings(SettingsData settingsData) {
            boolean z = CrashlyticsCore.CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
            Activity b = CrashlyticsCore.this.getFabric().m5329b();
            if (!(b == null || b.isFinishing() || !CrashlyticsCore.this.shouldPromptUserBeforeSendingCrashReports())) {
                z = CrashlyticsCore.this.getSendDecisionFromUser(b, settingsData.f3672c);
            }
            return Boolean.valueOf(z);
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.7 */
    class C00577 implements AlwaysSendCallback {
        C00577() {
        }

        public void sendUserReportsWithoutPrompting(boolean z) {
            CrashlyticsCore.this.setShouldSendUserReportsWithoutPrompting(z);
        }
    }

    /* renamed from: com.crashlytics.android.core.CrashlyticsCore.8 */
    class C00588 implements Runnable {
        final /* synthetic */ CrashPromptDialog val$dialog;

        C00588(CrashPromptDialog crashPromptDialog) {
            this.val$dialog = crashPromptDialog;
        }

        public void run() {
            this.val$dialog.show();
        }
    }

    public static class Builder {
        private float delay;
        private boolean disabled;
        private CrashlyticsListener listener;
        private PinningInfoProvider pinningInfoProvider;

        public Builder() {
            this.delay = -1.0f;
            this.disabled = CrashlyticsCore.SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }

        public Builder delay(float f) {
            if (f <= 0.0f) {
                throw new IllegalArgumentException("delay must be greater than 0");
            } else if (this.delay > 0.0f) {
                throw new IllegalStateException("delay already set.");
            } else {
                this.delay = f;
                return this;
            }
        }

        public Builder listener(CrashlyticsListener crashlyticsListener) {
            if (crashlyticsListener == null) {
                throw new IllegalArgumentException("listener must not be null.");
            } else if (this.listener != null) {
                throw new IllegalStateException("listener already set.");
            } else {
                this.listener = crashlyticsListener;
                return this;
            }
        }

        @Deprecated
        public Builder pinningInfo(PinningInfoProvider pinningInfoProvider) {
            if (pinningInfoProvider == null) {
                throw new IllegalArgumentException("pinningInfoProvider must not be null.");
            } else if (this.pinningInfoProvider != null) {
                throw new IllegalStateException("pinningInfoProvider already set.");
            } else {
                this.pinningInfoProvider = pinningInfoProvider;
                return this;
            }
        }

        public Builder disabled(boolean z) {
            this.disabled = z;
            return this;
        }

        public CrashlyticsCore build() {
            if (this.delay < 0.0f) {
                this.delay = CrashlyticsCore.CLS_DEFAULT_PROCESS_DELAY;
            }
            return new CrashlyticsCore(this.delay, this.listener, this.pinningInfoProvider, this.disabled);
        }
    }

    private static final class CrashMarkerCheck implements Callable<Boolean> {
        private final CrashlyticsFileMarker crashMarker;

        public CrashMarkerCheck(CrashlyticsFileMarker crashlyticsFileMarker) {
            this.crashMarker = crashlyticsFileMarker;
        }

        public Boolean call() {
            if (!this.crashMarker.isPresent()) {
                return Boolean.FALSE;
            }
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Found previous crash marker.");
            this.crashMarker.remove();
            return Boolean.TRUE;
        }
    }

    private static final class NoOpListener implements CrashlyticsListener {
        private NoOpListener() {
        }

        public void crashlyticsDidDetectCrashDuringPreviousExecution() {
        }
    }

    public CrashlyticsCore() {
        this(CLS_DEFAULT_PROCESS_DELAY, null, null, SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT);
    }

    CrashlyticsCore(float f, CrashlyticsListener crashlyticsListener, PinningInfoProvider pinningInfoProvider, boolean z) {
        this(f, crashlyticsListener, pinningInfoProvider, z, ExecutorUtils.m5462a("Crashlytics Exception Handler"));
    }

    CrashlyticsCore(float f, CrashlyticsListener crashlyticsListener, PinningInfoProvider pinningInfoProvider, boolean z, ExecutorService executorService) {
        this.userId = null;
        this.userEmail = null;
        this.userName = null;
        this.delay = f;
        if (crashlyticsListener == null) {
            crashlyticsListener = new NoOpListener();
        }
        this.listener = crashlyticsListener;
        this.pinningInfo = pinningInfoProvider;
        this.disabled = z;
        this.executorServiceWrapper = new CrashlyticsExecutorServiceWrapper(executorService);
        this.attributes = new ConcurrentHashMap();
        this.startTime = System.currentTimeMillis();
    }

    protected boolean onPreExecute() {
        return onPreExecute(super.getContext());
    }

    boolean onPreExecute(Context context) {
        if (this.disabled) {
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
        this.apiKey = new ApiKey().m5407a(context);
        if (this.apiKey == null) {
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
        this.buildId = CommonUtils.m5455m(context);
        if (isBuildIdValid(this.buildId, CommonUtils.m5435a(context, CRASHLYTICS_REQUIRE_BUILD_ID, (boolean) CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT))) {
            Fabric.m5322h().m5288c(TAG, "Initializing Crashlytics " + getVersion());
            this.fileStore = new FileStoreImpl(this);
            this.crashMarker = new CrashlyticsFileMarker(CRASH_MARKER_FILE_NAME, this.fileStore);
            this.initializationMarker = new CrashlyticsFileMarker(INITIALIZATION_MARKER_FILE_NAME, this.fileStore);
            try {
                setAndValidateKitProperties(context);
                UnityVersionProvider manifestUnityVersionProvider = new ManifestUnityVersionProvider(context, getPackageName());
                boolean didPreviousInitializationFail = didPreviousInitializationFail();
                checkForPreviousCrash();
                if (!installExceptionHandler(manifestUnityVersionProvider)) {
                    return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
                }
                if (!didPreviousInitializationFail || !CommonUtils.m5456n(context)) {
                    return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
                }
                finishInitSynchronously();
                return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
            } catch (Throwable e) {
                Fabric.m5322h().m5292e(TAG, "Crashlytics was not started due to an exception during initialization", e);
                return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
            }
        }
        throw new UnmetDependencyException(MISSING_BUILD_ID_MSG);
    }

    private void setAndValidateKitProperties(Context context) {
        PinningInfoProvider crashlyticsPinningInfoProvider = this.pinningInfo != null ? new CrashlyticsPinningInfoProvider(this.pinningInfo) : null;
        this.httpRequestFactory = new DefaultHttpRequestFactory(Fabric.m5322h());
        this.httpRequestFactory.m5693a(crashlyticsPinningInfoProvider);
        this.packageName = context.getPackageName();
        this.installerPackageName = getIdManager().m5480j();
        Fabric.m5322h().m5284a(TAG, "Installer package name is: " + this.installerPackageName);
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(this.packageName, 0);
        this.versionCode = Integer.toString(packageInfo.versionCode);
        this.versionName = packageInfo.versionName == null ? "0.0" : packageInfo.versionName;
    }

    private boolean installExceptionHandler(UnityVersionProvider unityVersionProvider) {
        try {
            Fabric.m5322h().m5284a(TAG, "Installing exception handler...");
            this.handler = new CrashlyticsUncaughtExceptionHandler(Thread.getDefaultUncaughtExceptionHandler(), this.executorServiceWrapper, getIdManager(), unityVersionProvider, this.fileStore, this);
            this.handler.openSession();
            Thread.setDefaultUncaughtExceptionHandler(this.handler);
            Fabric.m5322h().m5284a(TAG, "Successfully installed exception handler.");
            return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(TAG, "There was a problem installing the exception handler.", e);
            this.handler = null;
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
    }

    protected Void doInBackground() {
        markInitializationStarted();
        SessionEventData externalCrashEventData = getExternalCrashEventData();
        if (externalCrashEventData != null) {
            this.handler.writeExternalCrashEvent(externalCrashEventData);
        }
        this.handler.cleanInvalidTempFiles();
        try {
            SettingsData b = io.fabric.sdk.android.services.p023e.Settings.m5615a().m5619b();
            if (b == null) {
                Fabric.m5322h().m5289d(TAG, "Received null settings, skipping initialization!");
            } else if (b.f3673d.f3642c) {
                this.handler.finalizeSessions();
                CreateReportSpiCall createReportSpiCall = getCreateReportSpiCall(b);
                if (createReportSpiCall == null) {
                    Fabric.m5322h().m5289d(TAG, "Unable to create a call to upload reports.");
                    markInitializationComplete();
                } else {
                    new ReportUploader(this.apiKey, createReportSpiCall).uploadReports(this.delay);
                    markInitializationComplete();
                }
            } else {
                Fabric.m5322h().m5284a(TAG, "Collection of crash reports disabled in Crashlytics settings.");
                markInitializationComplete();
            }
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(TAG, "Crashlytics encountered a problem during asynchronous initialization.", e);
        } finally {
            markInitializationComplete();
        }
        return null;
    }

    public String getIdentifier() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    public String getVersion() {
        return "2.3.14.151";
    }

    public static CrashlyticsCore getInstance() {
        return (CrashlyticsCore) Fabric.m5314a(CrashlyticsCore.class);
    }

    public PinningInfoProvider getPinningInfoProvider() {
        return !this.disabled ? this.pinningInfo : null;
    }

    public void logException(Throwable th) {
        if (this.disabled || !ensureFabricWithCalled("prior to logging exceptions.")) {
            return;
        }
        if (th == null) {
            Fabric.m5322h().m5282a(5, TAG, "Crashlytics is ignoring a request to log a null exception.");
        } else {
            this.handler.writeNonFatalException(Thread.currentThread(), th);
        }
    }

    public void log(String str) {
        doLog(3, TAG, str);
    }

    private void doLog(int i, String str, String str2) {
        if (!this.disabled && ensureFabricWithCalled("prior to logging messages.")) {
            this.handler.writeToLog(System.currentTimeMillis() - this.startTime, formatLogMessage(i, str, str2));
        }
    }

    public void log(int i, String str, String str2) {
        doLog(i, str, str2);
        Fabric.m5322h().m5283a(i, BuildConfig.FLAVOR + str, BuildConfig.FLAVOR + str2, CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT);
    }

    public void setUserIdentifier(String str) {
        if (!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
            this.userId = sanitizeAttribute(str);
            this.handler.cacheUserData(this.userId, this.userName, this.userEmail);
        }
    }

    public void setUserName(String str) {
        if (!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
            this.userName = sanitizeAttribute(str);
            this.handler.cacheUserData(this.userId, this.userName, this.userEmail);
        }
    }

    public void setUserEmail(String str) {
        if (!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
            this.userEmail = sanitizeAttribute(str);
            this.handler.cacheUserData(this.userId, this.userName, this.userEmail);
        }
    }

    public void setString(String str, String str2) {
        if (this.disabled || !ensureFabricWithCalled("prior to setting keys.")) {
            return;
        }
        if (str == null) {
            Context context = getContext();
            if (context == null || !CommonUtils.m5451i(context)) {
                Fabric.m5322h().m5292e(TAG, "Attempting to set custom attribute with null key, ignoring.", null);
                return;
            }
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
        String sanitizeAttribute = sanitizeAttribute(str);
        if (this.attributes.size() < MAX_ATTRIBUTES || this.attributes.containsKey(sanitizeAttribute)) {
            this.attributes.put(sanitizeAttribute, str2 == null ? BuildConfig.FLAVOR : sanitizeAttribute(str2));
            this.handler.cacheKeyData(this.attributes);
            return;
        }
        Fabric.m5322h().m5284a(TAG, "Exceeded maximum number of custom attributes (64)");
    }

    public void setBool(String str, boolean z) {
        setString(str, Boolean.toString(z));
    }

    public void setDouble(String str, double d) {
        setString(str, Double.toString(d));
    }

    public void setFloat(String str, float f) {
        setString(str, Float.toString(f));
    }

    public void setInt(String str, int i) {
        setString(str, Integer.toString(i));
    }

    public void setLong(String str, long j) {
        setString(str, Long.toString(j));
    }

    public void crash() {
        new CrashTest().indexOutOfBounds();
    }

    public boolean verifyPinning(URL url) {
        try {
            return internalVerifyPinning(url);
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(TAG, "Could not verify SSL pinning", e);
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
    }

    @Deprecated
    public synchronized void setListener(CrashlyticsListener crashlyticsListener) {
        Fabric.m5322h().m5289d(TAG, "Use of setListener is deprecated.");
        if (crashlyticsListener == null) {
            throw new IllegalArgumentException("listener must not be null.");
        }
        this.listener = crashlyticsListener;
    }

    static void recordLoggedExceptionEvent(String str, String str2) {
        Answers answers = (Answers) Fabric.m5314a(Answers.class);
        if (answers != null) {
            answers.onException(new Crash(str, str2));
        }
    }

    static void recordFatalExceptionEvent(String str, String str2) {
        Answers answers = (Answers) Fabric.m5314a(Answers.class);
        if (answers != null) {
            answers.onException(new Crash(str, str2));
        }
    }

    Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(this.attributes);
    }

    String getPackageName() {
        return this.packageName;
    }

    String getApiKey() {
        return this.apiKey;
    }

    String getInstallerPackageName() {
        return this.installerPackageName;
    }

    String getVersionName() {
        return this.versionName;
    }

    String getVersionCode() {
        return this.versionCode;
    }

    String getOverridenSpiEndpoint() {
        return CommonUtils.m5440b(getContext(), CRASHLYTICS_API_ENDPOINT);
    }

    String getBuildId() {
        return this.buildId;
    }

    CrashlyticsUncaughtExceptionHandler getHandler() {
        return this.handler;
    }

    String getUserIdentifier() {
        return getIdManager().m5471a() ? this.userId : null;
    }

    String getUserEmail() {
        return getIdManager().m5471a() ? this.userEmail : null;
    }

    String getUserName() {
        return getIdManager().m5471a() ? this.userName : null;
    }

    private void finishInitSynchronously() {
        Callable c00511 = new C00511();
        for (Task addDependency : getDependencies()) {
            c00511.addDependency(addDependency);
        }
        Future submit = getFabric().m5334f().submit(c00511);
        Fabric.m5322h().m5284a(TAG, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(TAG, "Crashlytics was interrupted during initialization.", e);
        } catch (Throwable e2) {
            Fabric.m5322h().m5292e(TAG, "Problem encountered during Crashlytics initialization.", e2);
        } catch (Throwable e22) {
            Fabric.m5322h().m5292e(TAG, "Crashlytics timed out during initialization.", e22);
        }
    }

    void markInitializationStarted() {
        this.executorServiceWrapper.executeSyncLoggingException(new C00522());
    }

    void markInitializationComplete() {
        this.executorServiceWrapper.executeAsync(new C00533());
    }

    boolean didPreviousInitializationFail() {
        return ((Boolean) this.executorServiceWrapper.executeSyncLoggingException(new C00544())).booleanValue();
    }

    void setExternalCrashEventDataProvider(CrashEventDataProvider crashEventDataProvider) {
        this.externalCrashEventDataProvider = crashEventDataProvider;
    }

    SessionEventData getExternalCrashEventData() {
        if (this.externalCrashEventDataProvider != null) {
            return this.externalCrashEventDataProvider.getCrashEventData();
        }
        return null;
    }

    boolean internalVerifyPinning(URL url) {
        if (getPinningInfoProvider() == null) {
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
        HttpRequest a = this.httpRequestFactory.m5691a(HttpMethod.GET, url.toString());
        ((HttpsURLConnection) a.m5663a()).setInstanceFollowRedirects(SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT);
        a.m5664b();
        return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
    }

    File getSdkDirectory() {
        if (this.sdkDir == null) {
            this.sdkDir = new FileStoreImpl(this).m5567a();
        }
        return this.sdkDir;
    }

    boolean shouldPromptUserBeforeSendingCrashReports() {
        return ((Boolean) io.fabric.sdk.android.services.p023e.Settings.m5615a().m5618a(new C00555(), Boolean.valueOf(SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT))).booleanValue();
    }

    boolean shouldSendReportsWithoutPrompting() {
        return new PreferenceStoreImpl(this).m5569a().getBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT);
    }

    @SuppressLint({"CommitPrefEdits"})
    void setShouldSendUserReportsWithoutPrompting(boolean z) {
        PreferenceStore preferenceStoreImpl = new PreferenceStoreImpl(this);
        preferenceStoreImpl.m5570a(preferenceStoreImpl.m5571b().putBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, z));
    }

    boolean canSendWithUserApproval() {
        return ((Boolean) io.fabric.sdk.android.services.p023e.Settings.m5615a().m5618a(new C00566(), Boolean.valueOf(CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT))).booleanValue();
    }

    CreateReportSpiCall getCreateReportSpiCall(SettingsData settingsData) {
        if (settingsData != null) {
            return new DefaultCreateReportSpiCall(this, getOverridenSpiEndpoint(), settingsData.f3670a.f3627d, this.httpRequestFactory);
        }
        return null;
    }

    private void checkForPreviousCrash() {
        if (Boolean.TRUE.equals((Boolean) this.executorServiceWrapper.executeSyncLoggingException(new CrashMarkerCheck(this.crashMarker)))) {
            try {
                this.listener.crashlyticsDidDetectCrashDuringPreviousExecution();
            } catch (Throwable e) {
                Fabric.m5322h().m5292e(TAG, "Exception thrown by CrashlyticsListener while notifying of previous crash.", e);
            }
        }
    }

    void createCrashMarker() {
        this.crashMarker.create();
    }

    private boolean getSendDecisionFromUser(Activity activity, PromptSettingsData promptSettingsData) {
        CrashPromptDialog create = CrashPromptDialog.create(activity, promptSettingsData, new C00577());
        activity.runOnUiThread(new C00588(create));
        Fabric.m5322h().m5284a(TAG, "Waiting for user opt-in.");
        create.await();
        return create.getOptIn();
    }

    static SessionSettingsData getSessionSettingsData() {
        SettingsData b = io.fabric.sdk.android.services.p023e.Settings.m5615a().m5619b();
        return b == null ? null : b.f3671b;
    }

    private static String formatLogMessage(int i, String str, String str2) {
        return CommonUtils.m5439b(i) + "/" + str + " " + str2;
    }

    private static boolean ensureFabricWithCalled(String str) {
        CrashlyticsCore instance = getInstance();
        if (instance != null && instance.handler != null) {
            return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
        }
        Fabric.m5322h().m5292e(TAG, "Crashlytics must be initialized by calling Fabric.with(Context) " + str, null);
        return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
    }

    private static String sanitizeAttribute(String str) {
        if (str == null) {
            return str;
        }
        str = str.trim();
        if (str.length() > MAX_ATTRIBUTE_SIZE) {
            return str.substring(0, MAX_ATTRIBUTE_SIZE);
        }
        return str;
    }

    static boolean isBuildIdValid(String str, boolean z) {
        if (!z) {
            Fabric.m5322h().m5284a(TAG, "Configured not to require a build ID.");
            return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
        } else if (!CommonUtils.m5445c(str)) {
            return CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT;
        } else {
            Log.e(TAG, ".");
            Log.e(TAG, ".     |  | ");
            Log.e(TAG, ".     |  |");
            Log.e(TAG, ".     |  |");
            Log.e(TAG, ".   \\ |  | /");
            Log.e(TAG, ".    \\    /");
            Log.e(TAG, ".     \\  /");
            Log.e(TAG, ".      \\/");
            Log.e(TAG, ".");
            Log.e(TAG, MISSING_BUILD_ID_MSG);
            Log.e(TAG, ".");
            Log.e(TAG, ".      /\\");
            Log.e(TAG, ".     /  \\");
            Log.e(TAG, ".    /    \\");
            Log.e(TAG, ".   / |  | \\");
            Log.e(TAG, ".     |  |");
            Log.e(TAG, ".     |  |");
            Log.e(TAG, ".     |  |");
            Log.e(TAG, ".");
            return SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT;
        }
    }
}
