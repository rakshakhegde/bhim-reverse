package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import io.fabric.sdk.android.services.p021b.ResponseParser;
import java.io.File;
import java.util.Map.Entry;

class DefaultCreateReportSpiCall extends AbstractSpiCall implements CreateReportSpiCall {
    static final String FILE_CONTENT_TYPE = "application/octet-stream";
    static final String FILE_PARAM = "report[file]";
    static final String IDENTIFIER_PARAM = "report[identifier]";
    static final String MULTI_FILE_PARAM = "report[file";

    public DefaultCreateReportSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
    }

    DefaultCreateReportSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }

    public boolean invoke(CreateReportRequest createReportRequest) {
        HttpRequest applyMultipartDataTo = applyMultipartDataTo(applyHeadersTo(getHttpRequest(), createReportRequest), createReportRequest.report);
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Sending report to: " + getUrl());
        int b = applyMultipartDataTo.m5664b();
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Create report request ID: " + applyMultipartDataTo.m5666b(AbstractSpiCall.HEADER_REQUEST_ID));
        Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Result was: " + b);
        return ResponseParser.m5514a(b) == 0;
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, CreateReportRequest createReportRequest) {
        HttpRequest a = httpRequest.m5654a(AbstractSpiCall.HEADER_API_KEY, createReportRequest.apiKey).m5654a(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE).m5654a(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion());
        HttpRequest httpRequest2 = a;
        for (Entry a2 : createReportRequest.report.getCustomHeaders().entrySet()) {
            httpRequest2 = httpRequest2.m5660a(a2);
        }
        return httpRequest2;
    }

    private HttpRequest applyMultipartDataTo(HttpRequest httpRequest, Report report) {
        int i = 0;
        httpRequest.m5674e(IDENTIFIER_PARAM, report.getIdentifier());
        if (report.getFiles().length == 1) {
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Adding single file " + report.getFileName() + " to report " + report.getIdentifier());
            return httpRequest.m5657a(FILE_PARAM, report.getFileName(), FILE_CONTENT_TYPE, report.getFile());
        }
        File[] files = report.getFiles();
        int length = files.length;
        int i2 = 0;
        while (i < length) {
            File file = files[i];
            Fabric.m5322h().m5284a(CrashlyticsCore.TAG, "Adding file " + file.getName() + " to report " + report.getIdentifier());
            httpRequest.m5657a(MULTI_FILE_PARAM + i2 + "]", file.getName(), FILE_CONTENT_TYPE, file);
            i2++;
            i++;
        }
        return httpRequest;
    }
}
