package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.p038d.FileStore;
import java.io.File;

class CrashlyticsFileMarker {
    private final FileStore fileStore;
    private final String markerName;

    public CrashlyticsFileMarker(String str, FileStore fileStore) {
        this.markerName = str;
        this.fileStore = fileStore;
    }

    public boolean create() {
        boolean z = false;
        try {
            z = getMarkerFile().createNewFile();
        } catch (Throwable e) {
            Fabric.m5322h().m5292e(CrashlyticsCore.TAG, "Error creating marker: " + this.markerName, e);
        }
        return z;
    }

    public boolean isPresent() {
        return getMarkerFile().exists();
    }

    public boolean remove() {
        return getMarkerFile().delete();
    }

    private File getMarkerFile() {
        return new File(this.fileStore.m5566a(), this.markerName);
    }
}
