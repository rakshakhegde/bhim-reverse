package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.p019c.FileRollOverManager;
import io.fabric.sdk.android.services.p023e.AnalyticsSettingsData;

interface SessionAnalyticsManagerStrategy extends FileRollOverManager {
    void deleteAllEvents();

    void processEvent(Builder builder);

    void sendEvents();

    void setAnalyticsSettingsData(AnalyticsSettingsData analyticsSettingsData, String str);
}
