package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.IdManager;
import java.util.Map;
import java.util.UUID;

class SessionMetadataCollector {
    private final Context context;
    private final IdManager idManager;
    private final String versionCode;
    private final String versionName;

    public SessionMetadataCollector(Context context, IdManager idManager, String str, String str2) {
        this.context = context;
        this.idManager = idManager;
        this.versionCode = str;
        this.versionName = str2;
    }

    public SessionEventMetadata getMetadata() {
        Map i = this.idManager.m5479i();
        return new SessionEventMetadata(this.idManager.m5473c(), UUID.randomUUID().toString(), this.idManager.m5472b(), (String) i.get(IdManager.IdManager.ANDROID_ID), (String) i.get(IdManager.IdManager.ANDROID_ADVERTISING_ID), this.idManager.m5482l(), (String) i.get(IdManager.IdManager.FONT_TOKEN), CommonUtils.m5455m(this.context), this.idManager.m5474d(), this.idManager.m5477g(), this.versionCode, this.versionName);
    }
}
