package com.crashlytics.android.answers;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p019c.FilesSender;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import io.fabric.sdk.android.services.p021b.ResponseParser;
import java.io.File;
import java.util.List;

class SessionAnalyticsFilesSender extends AbstractSpiCall implements FilesSender {
    static final String FILE_CONTENT_TYPE = "application/vnd.crashlytics.android.events";
    static final String FILE_PARAM_NAME = "session_analytics_file_";
    private final String apiKey;

    public SessionAnalyticsFilesSender(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, String str3) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
        this.apiKey = str3;
    }

    public boolean send(List<File> list) {
        HttpRequest a = getHttpRequest().m5654a(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE).m5654a(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion()).m5654a(AbstractSpiCall.HEADER_API_KEY, this.apiKey);
        int i = 0;
        for (File file : list) {
            a.m5657a(FILE_PARAM_NAME + i, file.getName(), FILE_CONTENT_TYPE, file);
            i++;
        }
        Fabric.m5322h().m5284a(Answers.TAG, "Sending " + list.size() + " analytics files to " + getUrl());
        int b = a.m5664b();
        Fabric.m5322h().m5284a(Answers.TAG, "Response code for analytics file send is " + b);
        if (ResponseParser.m5514a(b) == 0) {
            return true;
        }
        return false;
    }
}
