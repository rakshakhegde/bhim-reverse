package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.measurement.internal.C0517y;

/* renamed from: com.google.android.gms.measurement.a */
public class C0453a {
    private final C0517y f2216a;

    /* renamed from: com.google.android.gms.measurement.a.a */
    public interface C0452a {
        void m3927a(String str, String str2, Bundle bundle);
    }

    public C0453a(C0517y c0517y) {
        C0385p.m3549a((Object) c0517y);
        this.f2216a = c0517y;
    }

    public static C0453a m3928a(Context context) {
        return C0517y.m4367a(context).m4393j();
    }

    public void m3929a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.f2216a.m4392i().m3992a(str, str2, bundle);
    }
}
