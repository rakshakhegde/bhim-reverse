package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.measurement.internal.C0479f;
import com.google.android.gms.measurement.internal.C0499t;
import com.google.android.gms.measurement.internal.C0517y;

public final class AppMeasurementReceiver extends BroadcastReceiver {
    static final Object f2206a;
    static WakeLock f2207b;
    static Boolean f2208c;

    static {
        f2206a = new Object();
    }

    public static boolean m3922a(Context context) {
        C0385p.m3549a((Object) context);
        if (f2208c != null) {
            return f2208c.booleanValue();
        }
        boolean a = C0479f.m4083a(context, AppMeasurementReceiver.class, false);
        f2208c = Boolean.valueOf(a);
        return a;
    }

    public void onReceive(Context context, Intent intent) {
        C0517y a = C0517y.m4367a(context);
        C0499t f = a.m4389f();
        String action = intent.getAction();
        if (a.m4387d().m4117C()) {
            f.m4288t().m4259a("Device AppMeasurementReceiver got", action);
        } else {
            f.m4288t().m4259a("Local AppMeasurementReceiver got", action);
        }
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            boolean a2 = AppMeasurementService.m3925a(context);
            Intent intent2 = new Intent(context, AppMeasurementService.class);
            intent2.setAction("com.google.android.gms.measurement.UPLOAD");
            synchronized (f2206a) {
                context.startService(intent2);
                if (a2) {
                    try {
                        PowerManager powerManager = (PowerManager) context.getSystemService("power");
                        if (f2207b == null) {
                            f2207b = powerManager.newWakeLock(1, "AppMeasurement WakeLock");
                            f2207b.setReferenceCounted(false);
                        }
                        f2207b.acquire(1000);
                    } catch (SecurityException e) {
                        f.m4283o().m4258a("AppMeasurementService at risk of not starting. For more reliable app measurements, add the WAKE_LOCK permission to your manifest.");
                    }
                    return;
                }
            }
        }
    }
}
