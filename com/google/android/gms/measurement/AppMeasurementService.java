package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager.WakeLock;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.measurement.internal.C0479f;
import com.google.android.gms.measurement.internal.C0499t;
import com.google.android.gms.measurement.internal.C0517y;
import com.google.android.gms.measurement.internal.C0524z;

public final class AppMeasurementService extends Service {
    private static Boolean f2214b;
    private final Handler f2215a;

    /* renamed from: com.google.android.gms.measurement.AppMeasurementService.1 */
    class C04511 implements Runnable {
        final /* synthetic */ C0517y f2210a;
        final /* synthetic */ int f2211b;
        final /* synthetic */ C0499t f2212c;
        final /* synthetic */ AppMeasurementService f2213d;

        /* renamed from: com.google.android.gms.measurement.AppMeasurementService.1.1 */
        class C04501 implements Runnable {
            final /* synthetic */ C04511 f2209a;

            C04501(C04511 c04511) {
                this.f2209a = c04511;
            }

            public void run() {
                if (!this.f2209a.f2213d.stopSelfResult(this.f2209a.f2211b)) {
                    return;
                }
                if (this.f2209a.f2210a.m4387d().m4117C()) {
                    this.f2209a.f2212c.m4288t().m4258a("Device AppMeasurementService processed last upload request");
                } else {
                    this.f2209a.f2212c.m4288t().m4258a("Local AppMeasurementService processed last upload request");
                }
            }
        }

        C04511(AppMeasurementService appMeasurementService, C0517y c0517y, int i, C0499t c0499t) {
            this.f2213d = appMeasurementService;
            this.f2210a = c0517y;
            this.f2211b = i;
            this.f2212c = c0499t;
        }

        public void run() {
            this.f2210a.m4407x();
            this.f2213d.f2215a.post(new C04501(this));
        }
    }

    public AppMeasurementService() {
        this.f2215a = new Handler();
    }

    private void m3924a() {
        try {
            synchronized (AppMeasurementReceiver.f2206a) {
                WakeLock wakeLock = AppMeasurementReceiver.f2207b;
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                }
            }
        } catch (SecurityException e) {
        }
    }

    public static boolean m3925a(Context context) {
        C0385p.m3549a((Object) context);
        if (f2214b != null) {
            return f2214b.booleanValue();
        }
        boolean a = C0479f.m4082a(context, AppMeasurementService.class);
        f2214b = Boolean.valueOf(a);
        return a;
    }

    private C0499t m3926b() {
        return C0517y.m4367a((Context) this).m4389f();
    }

    public IBinder onBind(Intent intent) {
        if (intent == null) {
            m3926b().m4270b().m4258a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new C0524z(C0517y.m4367a((Context) this));
        }
        m3926b().m4283o().m4259a("onBind received unknown action", action);
        return null;
    }

    public void onCreate() {
        super.onCreate();
        C0517y a = C0517y.m4367a((Context) this);
        C0499t f = a.m4389f();
        if (a.m4387d().m4117C()) {
            f.m4288t().m4258a("Device AppMeasurementService is starting up");
        } else {
            f.m4288t().m4258a("Local AppMeasurementService is starting up");
        }
    }

    public void onDestroy() {
        C0517y a = C0517y.m4367a((Context) this);
        C0499t f = a.m4389f();
        if (a.m4387d().m4117C()) {
            f.m4288t().m4258a("Device AppMeasurementService is shutting down");
        } else {
            f.m4288t().m4258a("Local AppMeasurementService is shutting down");
        }
        super.onDestroy();
    }

    public void onRebind(Intent intent) {
        if (intent == null) {
            m3926b().m4270b().m4258a("onRebind called with null intent");
            return;
        }
        m3926b().m4288t().m4259a("onRebind called. action", intent.getAction());
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        m3924a();
        C0517y a = C0517y.m4367a((Context) this);
        C0499t f = a.m4389f();
        String action = intent.getAction();
        if (a.m4387d().m4117C()) {
            f.m4288t().m4260a("Device AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        } else {
            f.m4288t().m4260a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        }
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            a.m4390g().m4349a(new C04511(this, a, i2, f));
        }
        return 2;
    }

    public boolean onUnbind(Intent intent) {
        if (intent == null) {
            m3926b().m4270b().m4258a("onUnbind called with null intent");
        } else {
            m3926b().m4288t().m4259a("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }
}
