package com.google.android.gms.measurement;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.R.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.b */
public final class C0454b {
    private static volatile C0454b f2217d;
    private final String f2218a;
    private final Status f2219b;
    private final boolean f2220c;

    C0454b(Context context) {
        boolean z = true;
        Resources resources = context.getResources();
        String resourcePackageName = resources.getResourcePackageName(R.common_google_play_services_unknown_issue);
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resourcePackageName);
        if (identifier != 0 && resources.getInteger(identifier) == 0) {
            z = false;
        }
        this.f2220c = z;
        int identifier2 = resources.getIdentifier("google_app_id", "string", resourcePackageName);
        if (identifier2 == 0) {
            if (this.f2220c) {
                this.f2219b = new Status(10, "Missing an expected resource: 'R.string.google_app_id' for initializing Google services.  Possible causes are missing google-services.json or com.google.gms.google-services gradle plugin.");
            } else {
                this.f2219b = Status.f1730a;
            }
            this.f2218a = null;
            return;
        }
        String string = resources.getString(identifier2);
        if (TextUtils.isEmpty(string)) {
            if (this.f2220c) {
                this.f2219b = new Status(10, "The resource 'R.string.google_app_id' is invalid, expected an app  identifier and found: '" + string + "'.");
            } else {
                this.f2219b = Status.f1730a;
            }
            this.f2218a = null;
            return;
        }
        this.f2218a = string;
        this.f2219b = Status.f1730a;
    }

    public static Status m3930a(Context context) {
        C0385p.m3550a((Object) context, (Object) "Context must not be null.");
        if (f2217d == null) {
            synchronized (C0454b.class) {
                if (f2217d == null) {
                    f2217d = new C0454b(context);
                }
            }
        }
        return f2217d.f2219b;
    }

    public static String m3931a() {
        if (f2217d == null) {
            synchronized (C0454b.class) {
                if (f2217d == null) {
                    throw new IllegalStateException("Initialize must be called before getGoogleAppId.");
                }
            }
        }
        return f2217d.m3933b();
    }

    public static boolean m3932c() {
        if (f2217d == null) {
            synchronized (C0454b.class) {
                if (f2217d == null) {
                    throw new IllegalStateException("Initialize must be called before isMeasurementEnabled.");
                }
            }
        }
        return f2217d.m3934d();
    }

    String m3933b() {
        if (this.f2219b.m3058d()) {
            return this.f2218a;
        }
        throw new IllegalStateException("Initialize must be successful before calling getGoogleAppId.  The result of the previous call to initialize was: '" + this.f2219b + "'.");
    }

    boolean m3934d() {
        if (this.f2219b.m3058d()) {
            return this.f2220c;
        }
        throw new IllegalStateException("Initialize must be successful before calling isMeasurementEnabledInternal.  The result of the previous call to initialize was: '" + this.f2219b + "'.");
    }
}
