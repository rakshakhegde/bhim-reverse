package com.google.android.gms.measurement.internal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;

/* renamed from: com.google.android.gms.measurement.internal.c */
public class C0476c extends ab {
    private boolean f2318a;
    private final AlarmManager f2319b;

    protected C0476c(C0517y c0517y) {
        super(c0517y);
        this.f2319b = (AlarmManager) m4071i().getSystemService("alarm");
    }

    private PendingIntent m4061o() {
        Intent intent = new Intent(m4071i(), AppMeasurementReceiver.class);
        intent.setAction("com.google.android.gms.measurement.UPLOAD");
        return PendingIntent.getBroadcast(m4071i(), 0, intent, 0);
    }

    protected void m4062a() {
        this.f2319b.cancel(m4061o());
    }

    public void m4063a(long j) {
        m3962y();
        C0385p.m3556b(j > 0);
        C0385p.m3554a(AppMeasurementReceiver.m3922a(m4071i()), (Object) "Receiver not registered/enabled");
        C0385p.m3554a(AppMeasurementService.m3925a(m4071i()), (Object) "Service not registered/enabled");
        m4064b();
        long b = m4070h().m3780b() + j;
        this.f2318a = true;
        this.f2319b.setInexactRepeating(2, b, Math.max(m4076n().m4128N(), j), m4061o());
    }

    public void m4064b() {
        m3962y();
        this.f2318a = false;
        this.f2319b.cancel(m4061o());
    }

    public /* bridge */ /* synthetic */ void m4065c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4066d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4067e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4068f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4069g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4070h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4071i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4072j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4073k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4074l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4075m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4076n() {
        return super.m3958n();
    }
}
