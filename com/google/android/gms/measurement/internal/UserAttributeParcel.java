package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class UserAttributeParcel implements SafeParcelable {
    public static final C0478e CREATOR;
    public final int f2239a;
    public final String f2240b;
    public final long f2241c;
    public final Long f2242d;
    public final Float f2243e;
    public final String f2244f;
    public final String f2245g;

    static {
        CREATOR = new C0478e();
    }

    UserAttributeParcel(int i, String str, long j, Long l, Float f, String str2, String str3) {
        this.f2239a = i;
        this.f2240b = str;
        this.f2241c = j;
        this.f2242d = l;
        this.f2243e = f;
        this.f2244f = str2;
        this.f2245g = str3;
    }

    UserAttributeParcel(String str, long j, Object obj, String str2) {
        C0385p.m3551a(str);
        this.f2239a = 1;
        this.f2240b = str;
        this.f2241c = j;
        this.f2245g = str2;
        if (obj == null) {
            this.f2242d = null;
            this.f2243e = null;
            this.f2244f = null;
        } else if (obj instanceof Long) {
            this.f2242d = (Long) obj;
            this.f2243e = null;
            this.f2244f = null;
        } else if (obj instanceof Float) {
            this.f2242d = null;
            this.f2243e = (Float) obj;
            this.f2244f = null;
        } else if (obj instanceof String) {
            this.f2242d = null;
            this.f2243e = null;
            this.f2244f = (String) obj;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    public Object m3940a() {
        return this.f2242d != null ? this.f2242d : this.f2243e != null ? this.f2243e : this.f2244f != null ? this.f2244f : null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0478e.m4077a(this, parcel, i);
    }
}
