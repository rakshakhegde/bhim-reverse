package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.measurement.internal.C0492q.C0494a;

/* renamed from: com.google.android.gms.measurement.internal.z */
public class C0524z extends C0494a {
    private final C0517y f2483a;
    private final boolean f2484b;

    /* renamed from: com.google.android.gms.measurement.internal.z.1 */
    class C05181 implements Runnable {
        final /* synthetic */ AppMetadata f2466a;
        final /* synthetic */ C0524z f2467b;

        C05181(C0524z c0524z, AppMetadata appMetadata) {
            this.f2467b = c0524z;
            this.f2466a = appMetadata;
        }

        public void run() {
            this.f2467b.m4416a(this.f2466a.f2228h);
            this.f2467b.f2483a.m4377a(this.f2466a);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.z.2 */
    class C05192 implements Runnable {
        final /* synthetic */ AppMetadata f2468a;
        final /* synthetic */ EventParcel f2469b;
        final /* synthetic */ C0524z f2470c;

        C05192(C0524z c0524z, AppMetadata appMetadata, EventParcel eventParcel) {
            this.f2470c = c0524z;
            this.f2468a = appMetadata;
            this.f2469b = eventParcel;
        }

        public void run() {
            this.f2470c.m4416a(this.f2468a.f2228h);
            this.f2470c.f2483a.m4378a(this.f2469b, this.f2468a);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.z.3 */
    class C05203 implements Runnable {
        final /* synthetic */ String f2471a;
        final /* synthetic */ EventParcel f2472b;
        final /* synthetic */ String f2473c;
        final /* synthetic */ C0524z f2474d;

        C05203(C0524z c0524z, String str, EventParcel eventParcel, String str2) {
            this.f2474d = c0524z;
            this.f2471a = str;
            this.f2472b = eventParcel;
            this.f2473c = str2;
        }

        public void run() {
            this.f2474d.m4416a(this.f2471a);
            this.f2474d.f2483a.m4379a(this.f2472b, this.f2473c);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.z.4 */
    class C05214 implements Runnable {
        final /* synthetic */ AppMetadata f2475a;
        final /* synthetic */ UserAttributeParcel f2476b;
        final /* synthetic */ C0524z f2477c;

        C05214(C0524z c0524z, AppMetadata appMetadata, UserAttributeParcel userAttributeParcel) {
            this.f2477c = c0524z;
            this.f2475a = appMetadata;
            this.f2476b = userAttributeParcel;
        }

        public void run() {
            this.f2477c.m4416a(this.f2475a.f2228h);
            this.f2477c.f2483a.m4384b(this.f2476b, this.f2475a);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.z.5 */
    class C05225 implements Runnable {
        final /* synthetic */ AppMetadata f2478a;
        final /* synthetic */ UserAttributeParcel f2479b;
        final /* synthetic */ C0524z f2480c;

        C05225(C0524z c0524z, AppMetadata appMetadata, UserAttributeParcel userAttributeParcel) {
            this.f2480c = c0524z;
            this.f2478a = appMetadata;
            this.f2479b = userAttributeParcel;
        }

        public void run() {
            this.f2480c.m4416a(this.f2478a.f2228h);
            this.f2480c.f2483a.m4380a(this.f2479b, this.f2478a);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.z.6 */
    class C05236 implements Runnable {
        final /* synthetic */ AppMetadata f2481a;
        final /* synthetic */ C0524z f2482b;

        C05236(C0524z c0524z, AppMetadata appMetadata) {
            this.f2482b = c0524z;
            this.f2481a = appMetadata;
        }

        public void run() {
            this.f2482b.m4416a(this.f2481a.f2228h);
            this.f2482b.f2483a.m4383b(this.f2481a);
        }
    }

    public C0524z(C0517y c0517y) {
        C0385p.m3549a((Object) c0517y);
        this.f2483a = c0517y;
        this.f2484b = false;
    }

    public C0524z(C0517y c0517y, boolean z) {
        C0385p.m3549a((Object) c0517y);
        this.f2483a = c0517y;
        this.f2484b = z;
    }

    private void m4410b(String str) {
        if (TextUtils.isEmpty(str)) {
            this.f2483a.m4389f().m4270b().m4258a("Measurement Service called without app package");
            throw new SecurityException("Measurement Service called without app package");
        }
        try {
            m4411c(str);
        } catch (SecurityException e) {
            this.f2483a.m4389f().m4270b().m4259a("Measurement Service called with invalid calling package", str);
            throw e;
        }
    }

    private void m4411c(String str) {
        int myUid = this.f2484b ? Process.myUid() : Binder.getCallingUid();
        if (!GooglePlayServicesUtil.zzb(this.f2483a.m4397n(), myUid, str)) {
            if (!GooglePlayServicesUtil.zze(this.f2483a.m4397n(), myUid) || this.f2483a.m4405v()) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
        }
    }

    public void m4412a(AppMetadata appMetadata) {
        C0385p.m3549a((Object) appMetadata);
        m4410b(appMetadata.f2222b);
        this.f2483a.m4390g().m4349a(new C05236(this, appMetadata));
    }

    public void m4413a(EventParcel eventParcel, AppMetadata appMetadata) {
        C0385p.m3549a((Object) eventParcel);
        C0385p.m3549a((Object) appMetadata);
        m4410b(appMetadata.f2222b);
        this.f2483a.m4390g().m4349a(new C05192(this, appMetadata, eventParcel));
    }

    public void m4414a(EventParcel eventParcel, String str, String str2) {
        C0385p.m3549a((Object) eventParcel);
        C0385p.m3551a(str);
        m4410b(str);
        this.f2483a.m4390g().m4349a(new C05203(this, str2, eventParcel, str));
    }

    public void m4415a(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        C0385p.m3549a((Object) userAttributeParcel);
        C0385p.m3549a((Object) appMetadata);
        m4410b(appMetadata.f2222b);
        if (userAttributeParcel.m3940a() == null) {
            this.f2483a.m4390g().m4349a(new C05214(this, appMetadata, userAttributeParcel));
        } else {
            this.f2483a.m4390g().m4349a(new C05225(this, appMetadata, userAttributeParcel));
        }
    }

    void m4416a(String str) {
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(":", 2);
            if (split.length == 2) {
                try {
                    long longValue = Long.valueOf(split[0]).longValue();
                    if (longValue > 0) {
                        this.f2483a.m4388e().f2415b.m4323a(split[1], longValue);
                    } else {
                        this.f2483a.m4389f().m4283o().m4259a("Combining sample with a non-positive weight", Long.valueOf(longValue));
                    }
                } catch (NumberFormatException e) {
                    this.f2483a.m4389f().m4283o().m4259a("Combining sample with a non-number weight", split[0]);
                }
            }
        }
    }

    public void m4417b(AppMetadata appMetadata) {
        C0385p.m3549a((Object) appMetadata);
        m4410b(appMetadata.f2222b);
        this.f2483a.m4390g().m4349a(new C05181(this, appMetadata));
    }
}
