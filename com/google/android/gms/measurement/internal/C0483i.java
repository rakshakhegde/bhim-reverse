package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.support.v4.p010e.ArrayMap;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0439j.C0437d;
import com.google.android.gms.internal.zztd;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.i */
class C0483i extends ab {
    private static final Map<String, String> f2327a;
    private final C0482a f2328b;
    private final C0475b f2329c;

    /* renamed from: com.google.android.gms.measurement.internal.i.a */
    private class C0482a extends SQLiteOpenHelper {
        final /* synthetic */ C0483i f2326a;

        C0482a(C0483i c0483i, Context context, String str) {
            this.f2326a = c0483i;
            super(context, str, null, 1);
        }

        private void m4158a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, Map<String, String> map) {
            if (!m4160a(sQLiteDatabase, str)) {
                sQLiteDatabase.execSQL(str2);
            }
            try {
                m4159a(sQLiteDatabase, str, str3, map);
            } catch (SQLiteException e) {
                this.f2326a.m3956l().m4270b().m4259a("Failed to verify columns on table that was just created", str);
                throw e;
            }
        }

        private void m4159a(SQLiteDatabase sQLiteDatabase, String str, String str2, Map<String, String> map) {
            Set b = m4161b(sQLiteDatabase, str);
            String[] split = str2.split(",");
            int length = split.length;
            int i = 0;
            while (i < length) {
                String str3 = split[i];
                if (b.remove(str3)) {
                    i++;
                } else {
                    throw new SQLiteException("Database " + str + " is missing required column: " + str3);
                }
            }
            if (map != null) {
                for (Entry entry : map.entrySet()) {
                    if (!b.remove(entry.getKey())) {
                        sQLiteDatabase.execSQL((String) entry.getValue());
                    }
                }
            }
            if (!b.isEmpty()) {
                throw new SQLiteException("Database " + str + " table has extra columns");
            }
        }

        private boolean m4160a(SQLiteDatabase sQLiteDatabase, String str) {
            Object e;
            Throwable th;
            Cursor cursor = null;
            Cursor query;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{CLConstants.FIELD_PAY_INFO_NAME}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        this.f2326a.m3956l().m4283o().m4260a("Error querying for table", str, e);
                        if (query != null) {
                            query.close();
                        }
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = query;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                query = null;
                this.f2326a.m3956l().m4283o().m4260a("Error querying for table", str, e);
                if (query != null) {
                    query.close();
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        private Set<String> m4161b(SQLiteDatabase sQLiteDatabase, String str) {
            Set<String> hashSet = new HashSet();
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM " + str + " LIMIT 0", null);
            try {
                Collections.addAll(hashSet, rawQuery.getColumnNames());
                return hashSet;
            } finally {
                rawQuery.close();
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (this.f2326a.f2329c.m4059a(this.f2326a.m3958n().m4152u())) {
                SQLiteDatabase writableDatabase;
                try {
                    writableDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    this.f2326a.f2329c.m4058a();
                    this.f2326a.m3956l().m4270b().m4258a("Opening the database failed, dropping and recreating it");
                    this.f2326a.m3953i().getDatabasePath(this.f2326a.m4162A()).delete();
                    try {
                        writableDatabase = super.getWritableDatabase();
                        this.f2326a.f2329c.m4060b();
                    } catch (SQLiteException e2) {
                        this.f2326a.m3956l().m4270b().m4259a("Failed to open freshly created database", e2);
                        throw e2;
                    }
                }
                return writableDatabase;
            }
            throw new SQLiteException("Database open failed");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT >= 9) {
                File file = new File(sQLiteDatabase.getPath());
                file.setReadable(false, false);
                file.setWritable(false, false);
                file.setReadable(true, true);
                file.setWritable(true, true);
            }
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            m4158a(sQLiteDatabase, "events", "CREATE TABLE IF NOT EXISTS events ( app_id TEXT NOT NULL, name TEXT NOT NULL, lifetime_count INTEGER NOT NULL, current_bundle_count INTEGER NOT NULL, last_fire_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id, name)) ;", "app_id,name,lifetime_count,current_bundle_count,last_fire_timestamp", null);
            m4158a(sQLiteDatabase, "user_attributes", "CREATE TABLE IF NOT EXISTS user_attributes ( app_id TEXT NOT NULL, name TEXT NOT NULL, set_timestamp INTEGER NOT NULL, value BLOB NOT NULL, PRIMARY KEY (app_id, name)) ;", "app_id,name,set_timestamp,value", null);
            m4158a(sQLiteDatabase, "apps", "CREATE TABLE IF NOT EXISTS apps ( app_id TEXT NOT NULL, app_instance_id TEXT, gmp_app_id TEXT, resettable_device_id_hash TEXT, last_bundle_index INTEGER NOT NULL, last_bundle_end_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id)) ;", "app_id,app_instance_id,gmp_app_id,resettable_device_id_hash,last_bundle_index,last_bundle_end_timestamp", C0483i.f2327a);
            m4158a(sQLiteDatabase, "queue", "CREATE TABLE IF NOT EXISTS queue ( app_id TEXT NOT NULL, bundle_end_timestamp INTEGER NOT NULL, data BLOB NOT NULL);", "app_id,bundle_end_timestamp,data", null);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    static {
        f2327a = new ArrayMap(5);
        f2327a.put("app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;");
        f2327a.put("app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;");
        f2327a.put("gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;");
        f2327a.put("dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;");
        f2327a.put("measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;");
    }

    C0483i(C0517y c0517y) {
        super(c0517y);
        this.f2329c = new C0475b(m3952h());
        this.f2328b = new C0482a(this, m3953i(), m4162A());
    }

    private String m4162A() {
        if (!m3958n().m4117C()) {
            return m3958n().m4157z();
        }
        if (m3958n().m4118D()) {
            return m3958n().m4157z();
        }
        m3956l().m4284p().m4258a("Using secondary database");
        return m3958n().m4115A();
    }

    private boolean m4163B() {
        return m3953i().getDatabasePath(m4162A()).exists();
    }

    static int m4164a(Cursor cursor, int i) {
        if (VERSION.SDK_INT >= 11) {
            return cursor.getType(i);
        }
        CursorWindow window = ((SQLiteCursor) cursor).getWindow();
        int position = cursor.getPosition();
        return window.isNull(position, i) ? 0 : window.isLong(position, i) ? 1 : window.isFloat(position, i) ? 2 : window.isString(position, i) ? 3 : window.isBlob(position, i) ? 4 : -1;
    }

    private long m4165a(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            cursor = m4185q().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
            } else if (cursor != null) {
                cursor.close();
            }
            return j;
        } catch (SQLiteException e) {
            m3956l().m4270b().m4260a("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public C0487m m4169a(String str, String str2) {
        Object e;
        Cursor cursor;
        Throwable th;
        Cursor cursor2 = null;
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        m3949e();
        m3962y();
        try {
            Cursor query = m4185q().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp"}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (query.moveToFirst()) {
                    C0487m c0487m = new C0487m(str, str2, query.getLong(0), query.getLong(1), query.getLong(2));
                    if (query.moveToNext()) {
                        m3956l().m4270b().m4258a("Got multiple records for event aggregates, expected one");
                    }
                    if (query == null) {
                        return c0487m;
                    }
                    query.close();
                    return c0487m;
                }
                if (query != null) {
                    query.close();
                }
                return null;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
                try {
                    m3956l().m4270b().m4261a("Error querying events", str, str2, e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor2 = query;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            m3956l().m4270b().m4261a("Error querying events", str, str2, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    public List<C0477d> m4170a(String str) {
        Object e;
        Cursor cursor;
        Throwable th;
        C0385p.m3551a(str);
        m3949e();
        m3962y();
        List<C0477d> arrayList = new ArrayList();
        Cursor query;
        try {
            query = m4185q().query("user_attributes", new String[]{CLConstants.FIELD_PAY_INFO_NAME, "set_timestamp", CLConstants.FIELD_PAY_INFO_VALUE}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(m3958n().m4151t() + 1));
            try {
                if (query.moveToFirst()) {
                    do {
                        String string = query.getString(0);
                        long j = query.getLong(1);
                        Object b = m4180b(query, 2);
                        if (b == null) {
                            m3956l().m4270b().m4258a("Read invalid user attribute value, ignoring it");
                        } else {
                            arrayList.add(new C0477d(str, string, j, b));
                        }
                    } while (query.moveToNext());
                    if (arrayList.size() > m3958n().m4151t()) {
                        m3956l().m4270b().m4258a("Loaded too many user attributes");
                        arrayList.remove(m3958n().m4151t());
                    }
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                }
                if (query != null) {
                    query.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            try {
                m3956l().m4270b().m4260a("Error querying user attributes", str, e);
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (Throwable th3) {
                th = th3;
                query = cursor;
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            query = null;
            if (query != null) {
                query.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<android.util.Pair<com.google.android.gms.internal.C0439j.C0437d, java.lang.Long>> m4171a(java.lang.String r12, int r13, int r14) {
        /*
        r11 = this;
        r10 = 0;
        r1 = 1;
        r9 = 0;
        r11.m3949e();
        r11.m3962y();
        if (r13 <= 0) goto L_0x004e;
    L_0x000b:
        r0 = r1;
    L_0x000c:
        com.google.android.gms.common.internal.C0385p.m3556b(r0);
        if (r14 <= 0) goto L_0x0050;
    L_0x0011:
        com.google.android.gms.common.internal.C0385p.m3556b(r1);
        com.google.android.gms.common.internal.C0385p.m3551a(r12);
        r0 = r11.m4185q();	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r1 = "queue";
        r2 = 2;
        r2 = new java.lang.String[r2];	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r3 = 0;
        r4 = "rowid";
        r2[r3] = r4;	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r3 = 1;
        r4 = "data";
        r2[r3] = r4;	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r3 = "app_id=?";
        r4 = 1;
        r4 = new java.lang.String[r4];	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r5 = 0;
        r4[r5] = r12;	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r5 = 0;
        r6 = 0;
        r7 = "rowid";
        r8 = java.lang.String.valueOf(r13);	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ SQLiteException -> 0x00e3, all -> 0x00d6 }
        r0 = r2.moveToFirst();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        if (r0 != 0) goto L_0x0052;
    L_0x0044:
        r0 = java.util.Collections.emptyList();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        if (r2 == 0) goto L_0x004d;
    L_0x004a:
        r2.close();
    L_0x004d:
        return r0;
    L_0x004e:
        r0 = r9;
        goto L_0x000c;
    L_0x0050:
        r1 = r9;
        goto L_0x0011;
    L_0x0052:
        r0 = new java.util.ArrayList;	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r0.<init>();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r3 = r9;
    L_0x0058:
        r1 = 0;
        r4 = r2.getLong(r1);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r1 = 1;
        r1 = r2.getBlob(r1);	 Catch:{ IOException -> 0x007a }
        r6 = r11.m3954j();	 Catch:{ IOException -> 0x007a }
        r1 = r6.m4096b(r1);	 Catch:{ IOException -> 0x007a }
        r6 = r0.isEmpty();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        if (r6 != 0) goto L_0x0093;
    L_0x0070:
        r6 = r1.length;	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r6 = r6 + r3;
        if (r6 <= r14) goto L_0x0093;
    L_0x0074:
        if (r2 == 0) goto L_0x004d;
    L_0x0076:
        r2.close();
        goto L_0x004d;
    L_0x007a:
        r1 = move-exception;
        r4 = r11.m3956l();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r4 = r4.m4270b();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r5 = "Failed to unzip queued bundle";
        r4.m4260a(r5, r12, r1);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r1 = r3;
    L_0x0089:
        r3 = r2.moveToNext();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        if (r3 == 0) goto L_0x0074;
    L_0x008f:
        if (r1 > r14) goto L_0x0074;
    L_0x0091:
        r3 = r1;
        goto L_0x0058;
    L_0x0093:
        r6 = com.google.android.gms.internal.C0446n.m3835a(r1);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r7 = new com.google.android.gms.internal.j$d;	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r7.<init>();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r7.m3823a(r6);	 Catch:{ IOException -> 0x00c6 }
        r1 = r1.length;	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r1 = r1 + r3;
        r3 = java.lang.Long.valueOf(r4);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r3 = android.util.Pair.create(r7, r3);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r0.add(r3);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        goto L_0x0089;
    L_0x00ad:
        r0 = move-exception;
        r1 = r2;
    L_0x00af:
        r2 = r11.m3956l();	 Catch:{ all -> 0x00e0 }
        r2 = r2.m4270b();	 Catch:{ all -> 0x00e0 }
        r3 = "Error querying bundles";
        r2.m4260a(r3, r12, r0);	 Catch:{ all -> 0x00e0 }
        r0 = java.util.Collections.emptyList();	 Catch:{ all -> 0x00e0 }
        if (r1 == 0) goto L_0x004d;
    L_0x00c2:
        r1.close();
        goto L_0x004d;
    L_0x00c6:
        r1 = move-exception;
        r4 = r11.m3956l();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r4 = r4.m4270b();	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r5 = "Failed to merge queued bundle";
        r4.m4260a(r5, r12, r1);	 Catch:{ SQLiteException -> 0x00ad, all -> 0x00de }
        r1 = r3;
        goto L_0x0089;
    L_0x00d6:
        r0 = move-exception;
        r2 = r10;
    L_0x00d8:
        if (r2 == 0) goto L_0x00dd;
    L_0x00da:
        r2.close();
    L_0x00dd:
        throw r0;
    L_0x00de:
        r0 = move-exception;
        goto L_0x00d8;
    L_0x00e0:
        r0 = move-exception;
        r2 = r1;
        goto L_0x00d8;
    L_0x00e3:
        r0 = move-exception;
        r1 = r10;
        goto L_0x00af;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.i.a(java.lang.String, int, int):java.util.List<android.util.Pair<com.google.android.gms.internal.j$d, java.lang.Long>>");
    }

    protected void m4172a() {
    }

    public void m4173a(long j) {
        m3949e();
        m3962y();
        if (m4185q().delete("queue", "rowid=?", new String[]{String.valueOf(j)}) != 1) {
            m3956l().m4270b().m4258a("Deleted fewer rows from queue than expected");
        }
    }

    void m4174a(ContentValues contentValues, String str, Object obj) {
        C0385p.m3551a(str);
        C0385p.m3549a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Float) {
            contentValues.put(str, (Float) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    public void m4175a(C0437d c0437d) {
        m3949e();
        m3962y();
        C0385p.m3549a((Object) c0437d);
        C0385p.m3551a(c0437d.f2144o);
        C0385p.m3549a(c0437d.f2135f);
        m4187s();
        long a = m3952h().m3779a();
        if (c0437d.f2135f.longValue() < a - m3958n().m4119E() || c0437d.f2135f.longValue() > m3958n().m4119E() + a) {
            m3956l().m4283o().m4260a("Storing bundle outside of the max uploading time span. now, timestamp", Long.valueOf(a), c0437d.f2135f);
        }
        try {
            byte[] bArr = new byte[c0437d.m3803e()];
            zztd a2 = zztd.m3876a(bArr);
            c0437d.m3824a(a2);
            a2.m3909b();
            bArr = m3954j().m4093a(bArr);
            m3956l().m4288t().m4259a("Saving bundle, size", Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", c0437d.f2144o);
            contentValues.put("bundle_end_timestamp", c0437d.f2135f);
            contentValues.put(CLConstants.FIELD_DATA, bArr);
            try {
                if (m4185q().insert("queue", null, contentValues) == -1) {
                    m3956l().m4270b().m4258a("Failed to insert bundle (got -1)");
                }
            } catch (SQLiteException e) {
                m3956l().m4270b().m4259a("Error storing bundle", e);
            }
        } catch (IOException e2) {
            m3956l().m4270b().m4259a("Data loss. Failed to serialize bundle", e2);
        }
    }

    public void m4176a(C0456a c0456a) {
        C0385p.m3549a((Object) c0456a);
        m3949e();
        m3962y();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", c0456a.f2246a);
        contentValues.put("app_instance_id", c0456a.f2247b);
        contentValues.put("gmp_app_id", c0456a.f2248c);
        contentValues.put("resettable_device_id_hash", c0456a.f2249d);
        contentValues.put("last_bundle_index", Long.valueOf(c0456a.f2250e));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(c0456a.f2251f));
        contentValues.put("app_version", c0456a.f2252g);
        contentValues.put("app_store", c0456a.f2253h);
        contentValues.put("gmp_version", Long.valueOf(c0456a.f2254i));
        contentValues.put("dev_cert_hash", Long.valueOf(c0456a.f2255j));
        contentValues.put("measurement_enabled", Boolean.valueOf(c0456a.f2256k));
        try {
            if (m4185q().insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                m3956l().m4270b().m4258a("Failed to insert/update app (got -1)");
            }
        } catch (SQLiteException e) {
            m3956l().m4270b().m4259a("Error storing app", e);
        }
    }

    public void m4177a(C0477d c0477d) {
        C0385p.m3549a((Object) c0477d);
        m3949e();
        m3962y();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", c0477d.f2320a);
        contentValues.put(CLConstants.FIELD_PAY_INFO_NAME, c0477d.f2321b);
        contentValues.put("set_timestamp", Long.valueOf(c0477d.f2322c));
        m4174a(contentValues, CLConstants.FIELD_PAY_INFO_VALUE, c0477d.f2323d);
        try {
            if (m4185q().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                m3956l().m4270b().m4258a("Failed to insert/update user attribute (got -1)");
            }
        } catch (SQLiteException e) {
            m3956l().m4270b().m4259a("Error storing user attribute", e);
        }
    }

    public void m4178a(C0487m c0487m) {
        C0385p.m3549a((Object) c0487m);
        m3949e();
        m3962y();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", c0487m.f2339a);
        contentValues.put(CLConstants.FIELD_PAY_INFO_NAME, c0487m.f2340b);
        contentValues.put("lifetime_count", Long.valueOf(c0487m.f2341c));
        contentValues.put("current_bundle_count", Long.valueOf(c0487m.f2342d));
        contentValues.put("last_fire_timestamp", Long.valueOf(c0487m.f2343e));
        try {
            if (m4185q().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                m3956l().m4270b().m4258a("Failed to insert/update event aggregates (got -1)");
            }
        } catch (SQLiteException e) {
            m3956l().m4270b().m4259a("Error storing event aggregates", e);
        }
    }

    public C0456a m4179b(String str) {
        C0456a c0456a;
        Object e;
        Cursor cursor;
        Throwable th;
        C0385p.m3551a(str);
        m3949e();
        m3962y();
        Cursor cursor2 = null;
        try {
            Cursor query = m4185q().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (query.moveToFirst()) {
                    c0456a = new C0456a(str, query.getString(0), query.getString(1), query.getString(2), query.getLong(3), query.getLong(4), query.getString(5), query.getString(6), query.getLong(7), query.getLong(8), (query.isNull(9) ? 1 : query.getInt(9)) != 0);
                    if (query.moveToNext()) {
                        m3956l().m4270b().m4258a("Got multiple records for app, expected one");
                    }
                    if (query != null) {
                        query.close();
                    }
                } else {
                    c0456a = null;
                    if (query != null) {
                        query.close();
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
                try {
                    m3956l().m4270b().m4260a("Error querying app", str, e);
                    c0456a = null;
                    if (cursor != null) {
                        cursor.close();
                    }
                    return c0456a;
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor2 = query;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            m3956l().m4270b().m4260a("Error querying app", str, e);
            c0456a = null;
            if (cursor != null) {
                cursor.close();
            }
            return c0456a;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
        return c0456a;
    }

    Object m4180b(Cursor cursor, int i) {
        int a = C0483i.m4164a(cursor, i);
        switch (a) {
            case R.View_android_theme /*0*/:
                m3956l().m4270b().m4258a("Loaded invalid null value from database");
                return null;
            case R.View_android_focusable /*1*/:
                return Long.valueOf(cursor.getLong(i));
            case R.View_paddingStart /*2*/:
                return Float.valueOf(cursor.getFloat(i));
            case R.View_paddingEnd /*3*/:
                return cursor.getString(i);
            case R.View_theme /*4*/:
                m3956l().m4270b().m4258a("Loaded invalid blob type value, ignoring it");
                return null;
            default:
                m3956l().m4270b().m4259a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(a));
                return null;
        }
    }

    public void m4181b() {
        m3962y();
        m4185q().beginTransaction();
    }

    public void m4182b(String str, String str2) {
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        m3949e();
        m3962y();
        try {
            m3956l().m4288t().m4259a("Deleted user attribute rows:", Integer.valueOf(m4185q().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e) {
            m3956l().m4270b().m4261a("Error deleting user attribute", str, str2, e);
        }
    }

    public void m4183o() {
        m3962y();
        m4185q().setTransactionSuccessful();
    }

    public void m4184p() {
        m3962y();
        m4185q().endTransaction();
    }

    SQLiteDatabase m4185q() {
        m3949e();
        try {
            return this.f2328b.getWritableDatabase();
        } catch (SQLiteException e) {
            m3956l().m4283o().m4259a("Error opening database", e);
            throw e;
        }
    }

    public String m4186r() {
        Cursor rawQuery;
        Object e;
        Throwable th;
        String str = null;
        try {
            rawQuery = m4185q().rawQuery("SELECT q.app_id FROM queue q JOIN apps a ON a.app_id=q.app_id WHERE a.measurement_enabled!=0 ORDER BY q.rowid LIMIT 1;", null);
            try {
                if (rawQuery.moveToFirst()) {
                    str = rawQuery.getString(0);
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                } else if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    m3956l().m4270b().m4259a("Database error getting next bundle app id", e);
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            rawQuery = null;
            m3956l().m4270b().m4259a("Database error getting next bundle app id", e);
            if (rawQuery != null) {
                rawQuery.close();
            }
            return str;
        } catch (Throwable th3) {
            rawQuery = null;
            th = th3;
            if (rawQuery != null) {
                rawQuery.close();
            }
            throw th;
        }
        return str;
    }

    void m4187s() {
        m3949e();
        m3962y();
        if (m4163B()) {
            long a = m3957m().f2419f.m4316a();
            long b = m3952h().m3780b();
            if (Math.abs(b - a) > m3958n().m4120F()) {
                m3957m().f2419f.m4317a(b);
                m4188t();
            }
        }
    }

    void m4188t() {
        m3949e();
        m3962y();
        if (m4163B()) {
            int delete = m4185q().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(m3952h().m3779a()), String.valueOf(m3958n().m4119E())});
            if (delete > 0) {
                m3956l().m4288t().m4259a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
            }
        }
    }

    public long m4189u() {
        return m4165a("select max(bundle_end_timestamp) from queue", null, 0);
    }
}
