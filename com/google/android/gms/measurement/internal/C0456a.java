package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.internal.a */
class C0456a {
    final String f2246a;
    final String f2247b;
    final String f2248c;
    final String f2249d;
    final long f2250e;
    final long f2251f;
    final String f2252g;
    final String f2253h;
    final long f2254i;
    final long f2255j;
    final boolean f2256k;

    C0456a(String str, String str2, String str3, String str4, long j, long j2, String str5, String str6, long j3, long j4, boolean z) {
        C0385p.m3551a(str);
        C0385p.m3556b(j >= 0);
        this.f2246a = str;
        this.f2247b = str2;
        if (TextUtils.isEmpty(str3)) {
            str3 = null;
        }
        this.f2248c = str3;
        this.f2249d = str4;
        this.f2250e = j;
        this.f2251f = j2;
        this.f2252g = str5;
        this.f2253h = str6;
        this.f2254i = j3;
        this.f2255j = j4;
        this.f2256k = z;
    }

    public C0456a m3941a(long j) {
        return new C0456a(this.f2246a, this.f2247b, this.f2248c, this.f2249d, this.f2250e, this.f2251f, this.f2252g, this.f2253h, this.f2254i, j, this.f2256k);
    }

    public C0456a m3942a(C0499t c0499t, long j) {
        C0385p.m3549a((Object) c0499t);
        long j2 = this.f2250e + 1;
        if (j2 > 2147483647L) {
            c0499t.m4283o().m4258a("Bundle index overflow");
            j2 = 0;
        }
        return new C0456a(this.f2246a, this.f2247b, this.f2248c, this.f2249d, j2, j, this.f2252g, this.f2253h, this.f2254i, this.f2255j, this.f2256k);
    }

    public C0456a m3943a(String str, long j) {
        return new C0456a(this.f2246a, this.f2247b, str, this.f2249d, this.f2250e, this.f2251f, this.f2252g, this.f2253h, j, this.f2255j, this.f2256k);
    }

    public C0456a m3944a(String str, String str2) {
        return new C0456a(this.f2246a, str, this.f2248c, str2, this.f2250e, this.f2251f, this.f2252g, this.f2253h, this.f2254i, this.f2255j, this.f2256k);
    }

    public C0456a m3945a(boolean z) {
        return new C0456a(this.f2246a, this.f2247b, this.f2248c, this.f2249d, this.f2250e, this.f2251f, this.f2252g, this.f2253h, this.f2254i, this.f2255j, z);
    }

    public C0456a m3946b(String str, String str2) {
        return new C0456a(this.f2246a, this.f2247b, this.f2248c, this.f2249d, this.f2250e, this.f2251f, str, str2, this.f2254i, this.f2255j, this.f2256k);
    }
}
