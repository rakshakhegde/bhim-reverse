package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.e */
public class C0478e implements Creator<UserAttributeParcel> {
    static void m4077a(UserAttributeParcel userAttributeParcel, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, userAttributeParcel.f2239a);
        C0386a.m3568a(parcel, 2, userAttributeParcel.f2240b, false);
        C0386a.m3562a(parcel, 3, userAttributeParcel.f2241c);
        C0386a.m3567a(parcel, 4, userAttributeParcel.f2242d, false);
        C0386a.m3566a(parcel, 5, userAttributeParcel.f2243e, false);
        C0386a.m3568a(parcel, 6, userAttributeParcel.f2244f, false);
        C0386a.m3568a(parcel, 7, userAttributeParcel.f2245g, false);
        C0386a.m3560a(parcel, a);
    }

    public UserAttributeParcel m4078a(Parcel parcel) {
        String str = null;
        int b = zza.m3582b(parcel);
        int i = 0;
        long j = 0;
        String str2 = null;
        Float f = null;
        Long l = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    str3 = zza.m3590h(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    j = zza.m3587e(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    l = zza.m3588f(parcel, a);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    f = zza.m3589g(parcel, a);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    str2 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetLeft /*7*/:
                    str = zza.m3590h(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new UserAttributeParcel(i, str3, j, l, f, str2, str);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public UserAttributeParcel[] m4079a(int i) {
        return new UserAttributeParcel[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m4078a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m4079a(i);
    }
}
