package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Pair;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.internal.C0385p;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.UUID;

/* renamed from: com.google.android.gms.measurement.internal.w */
class C0510w extends ab {
    static final Pair<String, Long> f2414a;
    public final C0509b f2415b;
    public final C0508a f2416c;
    public final C0508a f2417d;
    public final C0508a f2418e;
    public final C0508a f2419f;
    private SharedPreferences f2420h;
    private String f2421i;
    private boolean f2422j;
    private long f2423k;
    private final SecureRandom f2424l;

    /* renamed from: com.google.android.gms.measurement.internal.w.a */
    public final class C0508a {
        final /* synthetic */ C0510w f2404a;
        private final String f2405b;
        private final long f2406c;
        private boolean f2407d;
        private long f2408e;

        public C0508a(C0510w c0510w, String str, long j) {
            this.f2404a = c0510w;
            C0385p.m3551a(str);
            this.f2405b = str;
            this.f2406c = j;
        }

        private void m4315b() {
            if (!this.f2407d) {
                this.f2407d = true;
                this.f2408e = this.f2404a.f2420h.getLong(this.f2405b, this.f2406c);
            }
        }

        public long m4316a() {
            m4315b();
            return this.f2408e;
        }

        public void m4317a(long j) {
            Editor edit = this.f2404a.f2420h.edit();
            edit.putLong(this.f2405b, j);
            edit.apply();
            this.f2408e = j;
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.w.b */
    public final class C0509b {
        final String f2409a;
        final /* synthetic */ C0510w f2410b;
        private final String f2411c;
        private final String f2412d;
        private final long f2413e;

        private C0509b(C0510w c0510w, String str, long j) {
            this.f2410b = c0510w;
            C0385p.m3551a(str);
            C0385p.m3556b(j > 0);
            this.f2409a = str + ":start";
            this.f2411c = str + ":count";
            this.f2412d = str + ":value";
            this.f2413e = j;
        }

        private void m4318b() {
            this.f2410b.m3949e();
            long a = this.f2410b.m3952h().m3779a();
            Editor edit = this.f2410b.f2420h.edit();
            edit.remove(this.f2411c);
            edit.remove(this.f2412d);
            edit.putLong(this.f2409a, a);
            edit.apply();
        }

        private long m4319c() {
            this.f2410b.m3949e();
            long d = m4320d();
            if (d != 0) {
                return Math.abs(d - this.f2410b.m3952h().m3779a());
            }
            m4318b();
            return 0;
        }

        private long m4320d() {
            return this.f2410b.m4328s().getLong(this.f2409a, 0);
        }

        public Pair<String, Long> m4321a() {
            this.f2410b.m3949e();
            long c = m4319c();
            if (c < this.f2413e) {
                return null;
            }
            if (c > this.f2413e * 2) {
                m4318b();
                return null;
            }
            String string = this.f2410b.m4328s().getString(this.f2412d, null);
            c = this.f2410b.m4328s().getLong(this.f2411c, 0);
            m4318b();
            return (string == null || c <= 0) ? C0510w.f2414a : new Pair(string, Long.valueOf(c));
        }

        public void m4322a(String str) {
            m4323a(str, 1);
        }

        public void m4323a(String str, long j) {
            this.f2410b.m3949e();
            if (m4320d() == 0) {
                m4318b();
            }
            if (str == null) {
                str = BuildConfig.FLAVOR;
            }
            long j2 = this.f2410b.f2420h.getLong(this.f2411c, 0);
            if (j2 <= 0) {
                Editor edit = this.f2410b.f2420h.edit();
                edit.putString(this.f2412d, str);
                edit.putLong(this.f2411c, j);
                edit.apply();
                return;
            }
            Object obj = (this.f2410b.f2424l.nextLong() & Long.MAX_VALUE) < (Long.MAX_VALUE / (j2 + j)) * j ? 1 : null;
            Editor edit2 = this.f2410b.f2420h.edit();
            if (obj != null) {
                edit2.putString(this.f2412d, str);
            }
            edit2.putLong(this.f2411c, j2 + j);
            edit2.apply();
        }
    }

    static {
        f2414a = new Pair(BuildConfig.FLAVOR, Long.valueOf(0));
    }

    C0510w(C0517y c0517y) {
        super(c0517y);
        this.f2415b = new C0509b("health_monitor", m3958n().m4122H(), null);
        this.f2416c = new C0508a(this, "last_upload", 0);
        this.f2417d = new C0508a(this, "last_upload_attempt", 0);
        this.f2418e = new C0508a(this, "backoff", 0);
        this.f2419f = new C0508a(this, "last_delete_stale", 0);
        this.f2424l = new SecureRandom();
    }

    static MessageDigest m4325a(String str) {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance(str);
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException e) {
            }
        }
        return null;
    }

    private SharedPreferences m4328s() {
        m3949e();
        m3962y();
        return this.f2420h;
    }

    protected void m4329a() {
        this.f2420h = m3953i().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
    }

    void m4330a(boolean z) {
        m3949e();
        m3956l().m4288t().m4259a("Setting useService", Boolean.valueOf(z));
        Editor edit = m4328s().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    Pair<String, Boolean> m4331b() {
        m3949e();
        long b = m3952h().m3780b();
        if (this.f2421i != null && b < this.f2423k) {
            return new Pair(this.f2421i, Boolean.valueOf(this.f2422j));
        }
        this.f2423k = b + m3958n().m4155x();
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(m3953i());
            this.f2421i = advertisingIdInfo.getId();
            this.f2422j = advertisingIdInfo.isLimitAdTrackingEnabled();
        } catch (Throwable th) {
            m3956l().m4287s().m4259a("Unable to get advertising id", th);
            this.f2421i = BuildConfig.FLAVOR;
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair(this.f2421i, Boolean.valueOf(this.f2422j));
    }

    void m4332b(boolean z) {
        m3949e();
        m3956l().m4288t().m4259a("Setting measurementEnabled", Boolean.valueOf(z));
        Editor edit = m4328s().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    String m4333o() {
        String str = (String) m4331b().first;
        if (C0510w.m4325a("MD5") == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, C0510w.m4325a("MD5").digest(str.getBytes()))});
    }

    String m4334p() {
        return UUID.randomUUID().toString().replaceAll("-", BuildConfig.FLAVOR);
    }

    Boolean m4335q() {
        m3949e();
        return !m4328s().contains("use_service") ? null : Boolean.valueOf(m4328s().getBoolean("use_service", false));
    }

    boolean m4336r() {
        m3949e();
        return m4328s().getBoolean("measurement_enabled", true);
    }
}
