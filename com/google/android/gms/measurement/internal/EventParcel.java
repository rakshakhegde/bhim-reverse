package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class EventParcel implements SafeParcelable {
    public static final C0489o CREATOR;
    public final int f2234a;
    public final String f2235b;
    public final EventParams f2236c;
    public final String f2237d;
    public final long f2238e;

    static {
        CREATOR = new C0489o();
    }

    EventParcel(int i, String str, EventParams eventParams, String str2, long j) {
        this.f2234a = i;
        this.f2235b = str;
        this.f2236c = eventParams;
        this.f2237d = str2;
        this.f2238e = j;
    }

    public EventParcel(String str, EventParams eventParams, String str2, long j) {
        this.f2234a = 1;
        this.f2235b = str;
        this.f2236c = eventParams;
        this.f2237d = str2;
        this.f2238e = j;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "origin=" + this.f2237d + ",name=" + this.f2235b + ",params=" + this.f2236c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0489o.m4213a(this, parcel, i);
    }
}
