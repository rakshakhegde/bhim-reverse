package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.measurement.C0453a.C0452a;

public class ad extends ab {
    private C0461a f2281a;
    private C0452a f2282b;
    private boolean f2283c;

    /* renamed from: com.google.android.gms.measurement.internal.ad.1 */
    class C04571 implements Runnable {
        final /* synthetic */ boolean f2262a;
        final /* synthetic */ ad f2263b;

        public void run() {
            this.f2263b.m3987a(this.f2262a);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ad.2 */
    class C04582 implements Runnable {
        final /* synthetic */ String f2264a;
        final /* synthetic */ String f2265b;
        final /* synthetic */ long f2266c;
        final /* synthetic */ Bundle f2267d;
        final /* synthetic */ boolean f2268e;
        final /* synthetic */ String f2269f;
        final /* synthetic */ ad f2270g;

        C04582(ad adVar, String str, String str2, long j, Bundle bundle, boolean z, String str3) {
            this.f2270g = adVar;
            this.f2264a = str;
            this.f2265b = str2;
            this.f2266c = j;
            this.f2267d = bundle;
            this.f2268e = z;
            this.f2269f = str3;
        }

        public void run() {
            this.f2270g.m3984a(this.f2264a, this.f2265b, this.f2266c, this.f2267d, this.f2268e, this.f2269f);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ad.3 */
    class C04593 implements Runnable {
        final /* synthetic */ String f2271a;
        final /* synthetic */ String f2272b;
        final /* synthetic */ Object f2273c;
        final /* synthetic */ long f2274d;
        final /* synthetic */ ad f2275e;

        C04593(ad adVar, String str, String str2, Object obj, long j) {
            this.f2275e = adVar;
            this.f2271a = str;
            this.f2272b = str2;
            this.f2273c = obj;
            this.f2274d = j;
        }

        public void run() {
            this.f2275e.m3986a(this.f2271a, this.f2272b, this.f2273c, this.f2274d);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ad.4 */
    class C04604 implements Runnable {
        final /* synthetic */ String f2276a;
        final /* synthetic */ String f2277b;
        final /* synthetic */ long f2278c;
        final /* synthetic */ ad f2279d;

        C04604(ad adVar, String str, String str2, long j) {
            this.f2279d = adVar;
            this.f2276a = str;
            this.f2277b = str2;
            this.f2278c = j;
        }

        public void run() {
            this.f2279d.m3986a(this.f2276a, this.f2277b, null, this.f2278c);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ad.a */
    private class C0461a implements ActivityLifecycleCallbacks {
        final /* synthetic */ ad f2280a;

        private C0461a(ad adVar) {
            this.f2280a = adVar;
        }

        private boolean m3980a(String str) {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            this.f2280a.m3993a("auto", "_ldl", (Object) str);
            return true;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            try {
                this.f2280a.m4004l().m4288t().m4258a("onActivityCreated");
                Intent intent = activity.getIntent();
                if (intent != null) {
                    Uri data = intent.getData();
                    if (data != null && data.isHierarchical()) {
                        String queryParameter = data.getQueryParameter("referrer");
                        if (!TextUtils.isEmpty(queryParameter)) {
                            if (queryParameter.contains("gclid")) {
                                this.f2280a.m4004l().m4287s().m4259a("Activity created with referrer", queryParameter);
                                m3980a(queryParameter);
                                return;
                            }
                            this.f2280a.m4004l().m4287s().m4258a("Activity created with data 'referrer' param without gclid");
                        }
                    }
                }
            } catch (Throwable th) {
                this.f2280a.m4004l().m4270b().m4259a("Throwable caught in onActivityCreated", th);
            }
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    protected ad(C0517y c0517y) {
        super(c0517y);
    }

    private void m3984a(String str, String str2, long j, Bundle bundle, boolean z, String str3) {
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        C0385p.m3549a((Object) bundle);
        m3997e();
        m3962y();
        if (m4005m().m4336r()) {
            if (!this.f2283c) {
                this.f2283c = true;
                m3988p();
            }
            if (z && this.f2282b != null) {
                m4004l().m4287s().m4260a("Passing event to registered event handler (FE)", str2, bundle);
                this.f2282b.m3927a(str, str2, bundle);
                return;
            } else if (this.g.m4385b()) {
                m4004l().m4287s().m4260a("Logging event (FE)", str2, bundle);
                m3999g().m4040a(new EventParcel(str2, new EventParams(bundle), str, j), str3);
                return;
            } else {
                return;
            }
        }
        m4004l().m4286r().m4258a("Event not sent since app measurement is disabled");
    }

    private void m3985a(String str, String str2, Bundle bundle, boolean z, String str3) {
        C0385p.m3551a(str);
        long a = m4000h().m3779a();
        m4002j().m4089a(str2);
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str4 : bundle.keySet()) {
                m4002j().m4099c(str4);
                Object a2 = m4002j().m4085a(str4, bundle.get(str4));
                if (a2 != null) {
                    m4002j().m4086a(bundle2, str4, a2);
                }
            }
        }
        m4003k().m4349a(new C04582(this, str, str2, a, bundle2, z, str3));
    }

    private void m3986a(String str, String str2, Object obj, long j) {
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        m3997e();
        m3995c();
        m3962y();
        if (!m4005m().m4336r()) {
            m4004l().m4286r().m4258a("User attribute not set since app measurement is disabled");
        } else if (this.g.m4385b()) {
            m4004l().m4287s().m4260a("Setting user attribute (FE)", str2, obj);
            m3999g().m4041a(new UserAttributeParcel(str2, j, obj, str));
        }
    }

    private void m3987a(boolean z) {
        m3997e();
        m3995c();
        m3962y();
        m4004l().m4287s().m4259a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        m4005m().m4332b(z);
        m3999g().m4055o();
    }

    private void m3988p() {
        try {
            m3991a(Class.forName(m3989q()));
        } catch (ClassNotFoundException e) {
            m4004l().m4286r().m4258a("Tag Manager is not found and thus will not be used");
        }
    }

    private String m3989q() {
        return "com.google.android.gms.tagmanager.TagManagerService";
    }

    protected void m3990a() {
    }

    public void m3991a(Class<?> cls) {
        try {
            cls.getDeclaredMethod("initialize", new Class[]{Context.class}).invoke(null, new Object[]{m4001i()});
        } catch (Exception e) {
            m4004l().m4283o().m4259a("Failed to invoke Tag Manager's initialize() method", e);
        }
    }

    public void m3992a(String str, String str2, Bundle bundle) {
        m3995c();
        m3985a(str, str2, bundle, true, null);
    }

    public void m3993a(String str, String str2, Object obj) {
        C0385p.m3551a(str);
        long a = m4000h().m3779a();
        m4002j().m4094b(str2);
        if (obj != null) {
            m4002j().m4095b(str2, obj);
            Object c = m4002j().m4097c(str2, obj);
            if (c != null) {
                m4003k().m4349a(new C04593(this, str, str2, c, a));
                return;
            }
            return;
        }
        m4003k().m4349a(new C04604(this, str, str2, a));
    }

    public void m3994b() {
        if (m4001i().getApplicationContext() instanceof Application) {
            Application application = (Application) m4001i().getApplicationContext();
            if (this.f2281a == null) {
                this.f2281a = new C0461a();
            }
            application.unregisterActivityLifecycleCallbacks(this.f2281a);
            application.registerActivityLifecycleCallbacks(this.f2281a);
            m4004l().m4288t().m4258a("Registered activity lifecycle callback");
        }
    }

    public /* bridge */ /* synthetic */ void m3995c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m3996d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m3997e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m3998f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m3999g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4000h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4001i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4002j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4003k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4004l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4005m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4006n() {
        return super.m3958n();
    }

    public void m4007o() {
        m3997e();
        m3995c();
        m3962y();
        if (this.g.m4385b()) {
            m3999g().m4056p();
        }
    }
}
