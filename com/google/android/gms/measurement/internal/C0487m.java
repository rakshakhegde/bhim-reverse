package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.internal.m */
class C0487m {
    final String f2339a;
    final String f2340b;
    final long f2341c;
    final long f2342d;
    final long f2343e;

    C0487m(String str, String str2, long j, long j2, long j3) {
        boolean z = true;
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        C0385p.m3556b(j >= 0);
        if (j2 < 0) {
            z = false;
        }
        C0385p.m3556b(z);
        this.f2339a = str;
        this.f2340b = str2;
        this.f2341c = j;
        this.f2342d = j2;
        this.f2343e = j3;
    }

    C0487m m4209a(long j) {
        return new C0487m(this.f2339a, this.f2340b, this.f2341c + 1, this.f2342d + 1, j);
    }
}
