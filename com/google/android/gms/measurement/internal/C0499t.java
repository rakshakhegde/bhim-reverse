package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.measurement.C0453a;

/* renamed from: com.google.android.gms.measurement.internal.t */
public class C0499t extends ab {
    private final String f2378a;
    private final char f2379b;
    private final long f2380c;
    private final C0498a f2381d;
    private final C0498a f2382e;
    private final C0498a f2383f;
    private final C0498a f2384h;
    private final C0498a f2385i;
    private final C0498a f2386j;
    private final C0498a f2387k;
    private final C0498a f2388l;
    private final C0498a f2389m;

    /* renamed from: com.google.android.gms.measurement.internal.t.1 */
    class C04971 implements Runnable {
        final /* synthetic */ String f2372a;
        final /* synthetic */ C0499t f2373b;

        C04971(C0499t c0499t, String str) {
            this.f2373b = c0499t;
            this.f2372a = str;
        }

        public void run() {
            C0510w e = this.f2373b.g.m4388e();
            if (!e.m3960w() || e.m3961x()) {
                this.f2373b.m4266a(6, "Persisted config not initialized . Not logging error/warn.");
            } else {
                e.f2415b.m4322a(this.f2372a);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.t.a */
    public class C0498a {
        final /* synthetic */ C0499t f2374a;
        private final int f2375b;
        private final boolean f2376c;
        private final boolean f2377d;

        C0498a(C0499t c0499t, int i, boolean z, boolean z2) {
            this.f2374a = c0499t;
            this.f2375b = i;
            this.f2376c = z;
            this.f2377d = z2;
        }

        public void m4258a(String str) {
            this.f2374a.m4268a(this.f2375b, this.f2376c, this.f2377d, str, null, null, null);
        }

        public void m4259a(String str, Object obj) {
            this.f2374a.m4268a(this.f2375b, this.f2376c, this.f2377d, str, obj, null, null);
        }

        public void m4260a(String str, Object obj, Object obj2) {
            this.f2374a.m4268a(this.f2375b, this.f2376c, this.f2377d, str, obj, obj2, null);
        }

        public void m4261a(String str, Object obj, Object obj2, Object obj3) {
            this.f2374a.m4268a(this.f2375b, this.f2376c, this.f2377d, str, obj, obj2, obj3);
        }
    }

    C0499t(C0517y c0517y) {
        super(c0517y);
        this.f2378a = m4282n().m4132a();
        this.f2380c = m4282n().m4116B();
        if (m4282n().m4118D()) {
            this.f2379b = m4282n().m4117C() ? 'P' : 'C';
        } else {
            this.f2379b = m4282n().m4117C() ? 'p' : 'c';
        }
        this.f2381d = new C0498a(this, 6, false, false);
        this.f2382e = new C0498a(this, 6, true, false);
        this.f2383f = new C0498a(this, 6, false, true);
        this.f2384h = new C0498a(this, 5, false, false);
        this.f2385i = new C0498a(this, 5, true, false);
        this.f2386j = new C0498a(this, 5, false, true);
        this.f2387k = new C0498a(this, 4, false, false);
        this.f2388l = new C0498a(this, 3, false, false);
        this.f2389m = new C0498a(this, 2, false, false);
    }

    private static String m4262a(String str) {
        if (TextUtils.isEmpty(str)) {
            return BuildConfig.FLAVOR;
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(0, lastIndexOf) : str;
    }

    static String m4263a(boolean z, Object obj) {
        if (obj == null) {
            return BuildConfig.FLAVOR;
        }
        Object valueOf = obj instanceof Integer ? Long.valueOf((long) ((Integer) obj).intValue()) : obj;
        if (valueOf instanceof Long) {
            if (!z) {
                return String.valueOf(valueOf);
            }
            if (Math.abs(((Long) valueOf).longValue()) < 100) {
                return String.valueOf(valueOf);
            }
            String str = String.valueOf(valueOf).charAt(0) == '-' ? "-" : BuildConfig.FLAVOR;
            String valueOf2 = String.valueOf(Math.abs(((Long) valueOf).longValue()));
            return str + Math.round(Math.pow(10.0d, (double) (valueOf2.length() - 1))) + "..." + str + Math.round(Math.pow(10.0d, (double) valueOf2.length()) - 1.0d);
        } else if (valueOf instanceof Boolean) {
            return String.valueOf(valueOf);
        } else {
            if (!(valueOf instanceof Throwable)) {
                return z ? "-" : String.valueOf(valueOf);
            } else {
                Throwable th = (Throwable) valueOf;
                StringBuilder stringBuilder = new StringBuilder(th.toString());
                String a = C0499t.m4262a(C0453a.class.getCanonicalName());
                String a2 = C0499t.m4262a(C0517y.class.getCanonicalName());
                for (StackTraceElement stackTraceElement : th.getStackTrace()) {
                    if (!stackTraceElement.isNativeMethod()) {
                        String className = stackTraceElement.getClassName();
                        if (className != null) {
                            className = C0499t.m4262a(className);
                            if (className.equals(a) || className.equals(a2)) {
                                stringBuilder.append(": ");
                                stringBuilder.append(stackTraceElement);
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                }
                return stringBuilder.toString();
            }
        }
    }

    static String m4264a(boolean z, String str, Object obj, Object obj2, Object obj3) {
        if (str == null) {
            Object obj4 = BuildConfig.FLAVOR;
        }
        Object a = C0499t.m4263a(z, obj);
        Object a2 = C0499t.m4263a(z, obj2);
        Object a3 = C0499t.m4263a(z, obj3);
        StringBuilder stringBuilder = new StringBuilder();
        String str2 = BuildConfig.FLAVOR;
        if (!TextUtils.isEmpty(obj4)) {
            stringBuilder.append(obj4);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(a)) {
            stringBuilder.append(str2);
            stringBuilder.append(a);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a2)) {
            stringBuilder.append(str2);
            stringBuilder.append(a2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a3)) {
            stringBuilder.append(str2);
            stringBuilder.append(a3);
        }
        return stringBuilder.toString();
    }

    protected void m4265a() {
    }

    protected void m4266a(int i, String str) {
        Log.println(i, this.f2378a, str);
    }

    public void m4267a(int i, String str, Object obj, Object obj2, Object obj3) {
        C0385p.m3549a((Object) str);
        C0514x h = this.g.m4391h();
        if (h == null || !h.m3960w() || h.m3961x()) {
            m4266a(6, "Scheduler not initialized or shutdown. Not logging error/warn.");
            return;
        }
        if (i < 0) {
            i = 0;
        }
        if (i >= "01VDIWEA?".length()) {
            i = "01VDIWEA?".length() - 1;
        }
        String str2 = "1" + "01VDIWEA?".charAt(i) + this.f2379b + this.f2380c + ":" + C0499t.m4264a(true, str, obj, obj2, obj3);
        h.m4349a(new C04971(this, str2.length() > 1024 ? str.substring(0, 1024) : str2));
    }

    protected void m4268a(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && m4269a(i)) {
            m4266a(i, C0499t.m4264a(false, str, obj, obj2, obj3));
        }
        if (!z2 && i >= 5) {
            m4267a(i, str, obj, obj2, obj3);
        }
    }

    protected boolean m4269a(int i) {
        return Log.isLoggable(this.f2378a, i);
    }

    public C0498a m4270b() {
        return this.f2381d;
    }

    public /* bridge */ /* synthetic */ void m4271c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4272d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4273e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4274f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4275g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4276h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4277i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4278j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4279k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4280l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4281m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4282n() {
        return super.m3958n();
    }

    public C0498a m4283o() {
        return this.f2384h;
    }

    public C0498a m4284p() {
        return this.f2385i;
    }

    public C0498a m4285q() {
        return this.f2386j;
    }

    public C0498a m4286r() {
        return this.f2387k;
    }

    public C0498a m4287s() {
        return this.f2388l;
    }

    public C0498a m4288t() {
        return this.f2389m;
    }

    public String m4289u() {
        Pair a = m4281m().f2415b.m4321a();
        return a == null ? null : String.valueOf(a.second) + ":" + ((String) a.first);
    }
}
