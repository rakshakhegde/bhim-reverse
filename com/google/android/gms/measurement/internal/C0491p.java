package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C0353c;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0420b;

/* renamed from: com.google.android.gms.measurement.internal.p */
public final class C0491p {
    public static C0490a<Boolean> f2347a;
    public static C0490a<Boolean> f2348b;
    public static C0490a<String> f2349c;
    public static C0490a<Long> f2350d;
    public static C0490a<Long> f2351e;
    public static C0490a<Integer> f2352f;
    public static C0490a<Integer> f2353g;
    public static C0490a<String> f2354h;
    public static C0490a<Long> f2355i;
    public static C0490a<Long> f2356j;
    public static C0490a<Long> f2357k;
    public static C0490a<Long> f2358l;
    public static C0490a<Long> f2359m;
    public static C0490a<Long> f2360n;
    public static C0490a<Integer> f2361o;
    public static C0490a<Long> f2362p;
    public static C0490a<Long> f2363q;

    /* renamed from: com.google.android.gms.measurement.internal.p.a */
    public static final class C0490a<V> {
        private final V f2344a;
        private final C0420b<V> f2345b;
        private V f2346c;

        private C0490a(C0420b<V> c0420b, V v) {
            C0385p.m3549a((Object) c0420b);
            this.f2345b = c0420b;
            this.f2344a = v;
        }

        static C0490a<Integer> m4216a(String str, int i) {
            return C0490a.m4217a(str, i, i);
        }

        static C0490a<Integer> m4217a(String str, int i, int i2) {
            return new C0490a(C0420b.m3754a(str, Integer.valueOf(i2)), Integer.valueOf(i));
        }

        static C0490a<Long> m4218a(String str, long j) {
            return C0490a.m4219a(str, j, j);
        }

        static C0490a<Long> m4219a(String str, long j, long j2) {
            return new C0490a(C0420b.m3755a(str, Long.valueOf(j2)), Long.valueOf(j));
        }

        static C0490a<String> m4220a(String str, String str2) {
            return C0490a.m4221a(str, str2, str2);
        }

        static C0490a<String> m4221a(String str, String str2, String str3) {
            return new C0490a(C0420b.m3756a(str, str3), str2);
        }

        static C0490a<Boolean> m4222a(String str, boolean z) {
            return C0490a.m4223a(str, z, z);
        }

        static C0490a<Boolean> m4223a(String str, boolean z, boolean z2) {
            return new C0490a(C0420b.m3757a(str, z2), Boolean.valueOf(z));
        }

        public V m4224a() {
            return this.f2346c != null ? this.f2346c : (C0353c.f1920a && C0420b.m3758b()) ? this.f2345b.m3762d() : this.f2344a;
        }
    }

    static {
        f2347a = C0490a.m4222a("measurement.service_enabled", true);
        f2348b = C0490a.m4222a("measurement.service_client_enabled", true);
        f2349c = C0490a.m4221a("measurement.log_tag", "GMPM", "GMPM-SVC");
        f2350d = C0490a.m4218a("measurement.ad_id_cache_time", 10000);
        f2351e = C0490a.m4218a("measurement.monitoring.sample_period_millis", 86400000);
        f2352f = C0490a.m4216a("measurement.upload.max_bundles", 100);
        f2353g = C0490a.m4216a("measurement.upload.max_batch_size", 65536);
        f2354h = C0490a.m4220a("measurement.upload.url", "https://app-measurement.com/a");
        f2355i = C0490a.m4218a("measurement.upload.backoff_period", 43200000);
        f2356j = C0490a.m4218a("measurement.upload.window_interval", 3600000);
        f2357k = C0490a.m4218a("measurement.upload.interval", 3600000);
        f2358l = C0490a.m4218a("measurement.upload.stale_data_deletion_interval", 86400000);
        f2359m = C0490a.m4218a("measurement.upload.initial_upload_delay_time", 15000);
        f2360n = C0490a.m4218a("measurement.upload.retry_time", 1800000);
        f2361o = C0490a.m4216a("measurement.upload.retry_count", 6);
        f2362p = C0490a.m4218a("measurement.upload.max_queue_time", 2419200000L);
        f2363q = C0490a.m4218a("measurement.service_client.idle_disconnect_millis", 5000);
    }
}
