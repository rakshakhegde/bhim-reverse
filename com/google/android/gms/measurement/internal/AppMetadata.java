package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class AppMetadata implements SafeParcelable {
    public static final C0480g CREATOR;
    public final int f2221a;
    public final String f2222b;
    public final String f2223c;
    public final String f2224d;
    public final String f2225e;
    public final long f2226f;
    public final long f2227g;
    public final String f2228h;
    public final boolean f2229i;

    static {
        CREATOR = new C0480g();
    }

    AppMetadata(int i, String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z) {
        this.f2221a = i;
        this.f2222b = str;
        this.f2223c = str2;
        this.f2224d = str3;
        this.f2225e = str4;
        this.f2226f = j;
        this.f2227g = j2;
        this.f2228h = str5;
        if (i >= 3) {
            this.f2229i = z;
        } else {
            this.f2229i = true;
        }
    }

    AppMetadata(String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z) {
        C0385p.m3551a(str);
        this.f2221a = 3;
        this.f2222b = str;
        if (TextUtils.isEmpty(str2)) {
            str2 = null;
        }
        this.f2223c = str2;
        this.f2224d = str3;
        this.f2225e = str4;
        this.f2226f = j;
        this.f2227g = j2;
        this.f2228h = str5;
        this.f2229i = z;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0480g.m4112a(this, parcel, i);
    }
}
