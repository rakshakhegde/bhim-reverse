package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.internal.C0429f;
import com.google.android.gms.measurement.C0453a;

class ac {
    final Context f2261a;

    ac(Context context) {
        C0385p.m3549a((Object) context);
        Object applicationContext = context.getApplicationContext();
        C0385p.m3549a(applicationContext);
        this.f2261a = applicationContext;
    }

    C0481h m3964a(C0517y c0517y) {
        return new C0481h(c0517y);
    }

    public C0517y m3965a() {
        return new C0517y(this);
    }

    C0510w m3966b(C0517y c0517y) {
        return new C0510w(c0517y);
    }

    C0499t m3967c(C0517y c0517y) {
        return new C0499t(c0517y);
    }

    C0514x m3968d(C0517y c0517y) {
        return new C0514x(c0517y);
    }

    C0453a m3969e(C0517y c0517y) {
        return new C0453a(c0517y);
    }

    ad m3970f(C0517y c0517y) {
        return new ad(c0517y);
    }

    C0479f m3971g(C0517y c0517y) {
        return new C0479f(c0517y);
    }

    C0483i m3972h(C0517y c0517y) {
        return new C0483i(c0517y);
    }

    C0504u m3973i(C0517y c0517y) {
        return new C0504u(c0517y);
    }

    C0428e m3974j(C0517y c0517y) {
        return C0429f.m3781c();
    }

    ae m3975k(C0517y c0517y) {
        return new ae(c0517y);
    }

    C0485k m3976l(C0517y c0517y) {
        return new C0485k(c0517y);
    }

    C0495r m3977m(C0517y c0517y) {
        return new C0495r(c0517y);
    }

    C0506v m3978n(C0517y c0517y) {
        return new C0506v(c0517y);
    }

    C0476c m3979o(C0517y c0517y) {
        return new C0476c(c0517y);
    }
}
