package com.google.android.gms.measurement.internal;

abstract class ab extends aa {
    private boolean f2258a;
    private boolean f2259b;
    private boolean f2260c;

    ab(C0517y c0517y) {
        super(c0517y);
        this.g.m4381a(this);
    }

    protected abstract void m3959a();

    boolean m3960w() {
        return this.f2258a && !this.f2259b;
    }

    boolean m3961x() {
        return this.f2260c;
    }

    protected void m3962y() {
        if (!m3960w()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void m3963z() {
        if (this.f2258a) {
            throw new IllegalStateException("Can't initialize twice");
        }
        m3959a();
        this.g.m4408y();
        this.f2258a = true;
    }
}
