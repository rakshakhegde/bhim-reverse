package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Process;
import com.google.android.gms.common.C0100b;
import com.google.android.gms.common.internal.C0353c;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.internal.C0432i;

/* renamed from: com.google.android.gms.measurement.internal.h */
public class C0481h extends aa {
    static final String f2324a;
    private Boolean f2325b;

    static {
        f2324a = String.valueOf(C0100b.f1762a / 1000).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
    }

    C0481h(C0517y c0517y) {
        super(c0517y);
    }

    public String m4115A() {
        return "google_app_measurement2.db";
    }

    public long m4116B() {
        return (long) (C0100b.f1762a / 1000);
    }

    public boolean m4117C() {
        return C0353c.f1920a;
    }

    public boolean m4118D() {
        if (this.f2325b == null) {
            synchronized (this) {
                if (this.f2325b == null) {
                    ApplicationInfo applicationInfo = m4140i().getApplicationInfo();
                    String a = C0432i.m3797a(m4140i(), Process.myPid());
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        boolean z = str != null && str.equals(a);
                        this.f2325b = Boolean.valueOf(z);
                    }
                    if (this.f2325b == null) {
                        this.f2325b = Boolean.TRUE;
                        m4143l().m4270b().m4258a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f2325b.booleanValue();
    }

    public long m4119E() {
        return ((Long) C0491p.f2362p.m4224a()).longValue();
    }

    public long m4120F() {
        return ((Long) C0491p.f2358l.m4224a()).longValue();
    }

    public long m4121G() {
        return 20;
    }

    public long m4122H() {
        return Math.max(0, ((Long) C0491p.f2351e.m4224a()).longValue());
    }

    public int m4123I() {
        return ((Integer) C0491p.f2352f.m4224a()).intValue();
    }

    public int m4124J() {
        return Math.max(0, ((Integer) C0491p.f2353g.m4224a()).intValue());
    }

    public String m4125K() {
        return (String) C0491p.f2354h.m4224a();
    }

    public long m4126L() {
        return Math.max(0, ((Long) C0491p.f2355i.m4224a()).longValue());
    }

    public long m4127M() {
        return Math.max(0, ((Long) C0491p.f2357k.m4224a()).longValue());
    }

    public long m4128N() {
        return ((Long) C0491p.f2356j.m4224a()).longValue();
    }

    public long m4129O() {
        return Math.max(0, ((Long) C0491p.f2359m.m4224a()).longValue());
    }

    public long m4130P() {
        return Math.max(0, ((Long) C0491p.f2360n.m4224a()).longValue());
    }

    public int m4131Q() {
        return Math.min(20, Math.max(0, ((Integer) C0491p.f2361o.m4224a()).intValue()));
    }

    String m4132a() {
        return (String) C0491p.f2349c.m4224a();
    }

    int m4133b() {
        return 32;
    }

    public /* bridge */ /* synthetic */ void m4134c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4135d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4136e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4137f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4138g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4139h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4140i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4141j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4142k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4143l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4144m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4145n() {
        return super.m3958n();
    }

    public int m4146o() {
        return 24;
    }

    int m4147p() {
        return 36;
    }

    int m4148q() {
        return 256;
    }

    int m4149r() {
        return 36;
    }

    int m4150s() {
        return 2048;
    }

    int m4151t() {
        return 20;
    }

    long m4152u() {
        return 3600000;
    }

    long m4153v() {
        return 60000;
    }

    long m4154w() {
        return 61000;
    }

    long m4155x() {
        return ((Long) C0491p.f2350d.m4224a()).longValue();
    }

    long m4156y() {
        return ((Long) C0491p.f2363q.m4224a()).longValue();
    }

    public String m4157z() {
        return "google_app_measurement.db";
    }
}
