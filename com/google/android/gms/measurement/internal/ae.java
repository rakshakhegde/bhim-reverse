package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.C0097c.C0093b;
import com.google.android.gms.common.api.C0097c.C0094c;
import com.google.android.gms.common.internal.C0355d;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.stats.C0389b;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.measurement.AppMeasurementService;
import com.google.android.gms.measurement.internal.C0492q.C0494a;
import java.util.ArrayList;
import java.util.List;

public class ae extends ab {
    private final C0474a f2309a;
    private C0492q f2310b;
    private Boolean f2311c;
    private final C0462j f2312d;
    private final C0475b f2313e;
    private final List<Runnable> f2314f;
    private final C0462j f2315h;

    /* renamed from: com.google.android.gms.measurement.internal.ae.1 */
    class C04631 extends C0462j {
        final /* synthetic */ ae f2289a;

        C04631(ae aeVar, C0517y c0517y) {
            this.f2289a = aeVar;
            super(c0517y);
        }

        public void m4016a() {
            this.f2289a.m4038v();
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.2 */
    class C04642 extends C0462j {
        final /* synthetic */ ae f2290a;

        C04642(ae aeVar, C0517y c0517y) {
            this.f2290a = aeVar;
            super(c0517y);
        }

        public void m4017a() {
            this.f2290a.m4052l().m4283o().m4258a("Tasks have been queued for a long time");
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.3 */
    class C04653 implements Runnable {
        final /* synthetic */ ae f2291a;

        C04653(ae aeVar) {
            this.f2291a = aeVar;
        }

        public void run() {
            C0492q c = this.f2291a.f2310b;
            if (c == null) {
                this.f2291a.m4052l().m4270b().m4258a("Failed to send measurementEnabled to service");
                return;
            }
            try {
                c.m4229b(this.f2291a.m4046f().m4237a(this.f2291a.m4052l().m4289u()));
                this.f2291a.m4034r();
            } catch (RemoteException e) {
                this.f2291a.m4052l().m4270b().m4259a("Failed to send measurementEnabled to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.4 */
    class C04664 implements Runnable {
        final /* synthetic */ String f2292a;
        final /* synthetic */ EventParcel f2293b;
        final /* synthetic */ ae f2294c;

        C04664(ae aeVar, String str, EventParcel eventParcel) {
            this.f2294c = aeVar;
            this.f2292a = str;
            this.f2293b = eventParcel;
        }

        public void run() {
            C0492q c = this.f2294c.f2310b;
            if (c == null) {
                this.f2294c.m4052l().m4270b().m4258a("Discarding data. Failed to send event to service");
                return;
            }
            try {
                if (TextUtils.isEmpty(this.f2292a)) {
                    c.m4226a(this.f2293b, this.f2294c.m4046f().m4237a(this.f2294c.m4052l().m4289u()));
                } else {
                    c.m4227a(this.f2293b, this.f2292a, this.f2294c.m4052l().m4289u());
                }
                this.f2294c.m4034r();
            } catch (RemoteException e) {
                this.f2294c.m4052l().m4270b().m4259a("Failed to send event to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.5 */
    class C04675 implements Runnable {
        final /* synthetic */ UserAttributeParcel f2295a;
        final /* synthetic */ ae f2296b;

        C04675(ae aeVar, UserAttributeParcel userAttributeParcel) {
            this.f2296b = aeVar;
            this.f2295a = userAttributeParcel;
        }

        public void run() {
            C0492q c = this.f2296b.f2310b;
            if (c == null) {
                this.f2296b.m4052l().m4270b().m4258a("Discarding data. Failed to set user attribute");
                return;
            }
            try {
                c.m4228a(this.f2295a, this.f2296b.m4046f().m4237a(this.f2296b.m4052l().m4289u()));
                this.f2296b.m4034r();
            } catch (RemoteException e) {
                this.f2296b.m4052l().m4270b().m4259a("Failed to send attribute to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.6 */
    class C04686 implements Runnable {
        final /* synthetic */ ae f2297a;

        C04686(ae aeVar) {
            this.f2297a = aeVar;
        }

        public void run() {
            C0492q c = this.f2297a.f2310b;
            if (c == null) {
                this.f2297a.m4052l().m4270b().m4258a("Discarding data. Failed to send app launch");
                return;
            }
            try {
                c.m4225a(this.f2297a.m4046f().m4237a(this.f2297a.m4052l().m4289u()));
                this.f2297a.m4034r();
            } catch (RemoteException e) {
                this.f2297a.m4052l().m4270b().m4259a("Failed to send app launch to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.7 */
    class C04697 implements ServiceConnection {
        final /* synthetic */ ae f2298a;

        C04697(ae aeVar) {
            this.f2298a = aeVar;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.ae.a */
    protected class C0474a implements ServiceConnection, C0093b, C0094c {
        final /* synthetic */ ae f2306a;
        private volatile boolean f2307b;
        private volatile C0496s f2308c;

        /* renamed from: com.google.android.gms.measurement.internal.ae.a.1 */
        class C04701 implements Runnable {
            final /* synthetic */ C0492q f2299a;
            final /* synthetic */ C0474a f2300b;

            C04701(C0474a c0474a, C0492q c0492q) {
                this.f2300b = c0474a;
                this.f2299a = c0492q;
            }

            public void run() {
                if (!this.f2300b.f2306a.m4042b()) {
                    this.f2300b.f2306a.m4052l().m4287s().m4258a("Connected to service");
                    this.f2300b.f2306a.m4029a(this.f2299a);
                }
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.ae.a.2 */
        class C04712 implements Runnable {
            final /* synthetic */ ComponentName f2301a;
            final /* synthetic */ C0474a f2302b;

            C04712(C0474a c0474a, ComponentName componentName) {
                this.f2302b = c0474a;
                this.f2301a = componentName;
            }

            public void run() {
                this.f2302b.f2306a.m4026a(this.f2301a);
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.ae.a.3 */
        class C04723 implements Runnable {
            final /* synthetic */ C0492q f2303a;
            final /* synthetic */ C0474a f2304b;

            C04723(C0474a c0474a, C0492q c0492q) {
                this.f2304b = c0474a;
                this.f2303a = c0492q;
            }

            public void run() {
                if (!this.f2304b.f2306a.m4042b()) {
                    this.f2304b.f2306a.m4052l().m4287s().m4258a("Connected to remote service");
                    this.f2304b.f2306a.m4029a(this.f2303a);
                }
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.ae.a.4 */
        class C04734 implements Runnable {
            final /* synthetic */ C0474a f2305a;

            C04734(C0474a c0474a) {
                this.f2305a = c0474a;
            }

            public void run() {
                this.f2305a.f2306a.m4026a(new ComponentName(this.f2305a.f2306a.m4049i(), AppMeasurementService.class));
            }
        }

        protected C0474a(ae aeVar) {
            this.f2306a = aeVar;
        }

        public void m4018a() {
            this.f2306a.m4045e();
            Context i = this.f2306a.m4049i();
            synchronized (this) {
                if (this.f2307b) {
                    this.f2306a.m4052l().m4288t().m4258a("Connection attempt already in progress");
                } else if (this.f2308c != null) {
                    this.f2306a.m4052l().m4288t().m4258a("Already awaiting connection attempt");
                } else {
                    this.f2308c = new C0496s(i, Looper.getMainLooper(), C0355d.m3345a(i), this, this);
                    this.f2306a.m4052l().m4288t().m4258a("Connecting to remote service");
                    this.f2307b = true;
                    this.f2308c.m3411e();
                }
            }
        }

        public void m4019a(int i) {
            C0385p.m3555b("MeasurementServiceConnection.onConnectionSuspended");
            this.f2306a.m4052l().m4287s().m4258a("Service connection suspended");
            this.f2306a.m4051k().m4349a(new C04734(this));
        }

        public void m4020a(Intent intent) {
            this.f2306a.m4045e();
            Context i = this.f2306a.m4049i();
            C0389b a = C0389b.m3613a();
            synchronized (this) {
                if (this.f2307b) {
                    this.f2306a.m4052l().m4288t().m4258a("Connection attempt already in progress");
                    return;
                }
                this.f2307b = true;
                a.m3624a(i, intent, this.f2306a.f2309a, 129);
            }
        }

        public void m4021a(Bundle bundle) {
            C0385p.m3555b("MeasurementServiceConnection.onConnected");
            synchronized (this) {
                this.f2307b = false;
                try {
                    C0492q c0492q = (C0492q) this.f2308c.m3418l();
                    this.f2308c = null;
                    this.f2306a.m4051k().m4349a(new C04723(this, c0492q));
                } catch (DeadObjectException e) {
                    this.f2308c = null;
                } catch (IllegalStateException e2) {
                    this.f2308c = null;
                }
            }
        }

        public void m4022a(ConnectionResult connectionResult) {
            C0385p.m3555b("MeasurementServiceConnection.onConnectionFailed");
            this.f2306a.m4052l().m4283o().m4259a("Service connection failed", connectionResult);
            synchronized (this) {
                this.f2307b = false;
                this.f2308c = null;
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            C0385p.m3555b("MeasurementServiceConnection.onServiceConnected");
            synchronized (this) {
                this.f2307b = false;
                if (iBinder == null) {
                    this.f2306a.m4052l().m4270b().m4258a("Service connected with null binder");
                    return;
                }
                C0492q c0492q = null;
                try {
                    String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                    if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                        c0492q = C0494a.m4235a(iBinder);
                        this.f2306a.m4052l().m4288t().m4258a("Bound to IMeasurementService interface");
                    } else {
                        this.f2306a.m4052l().m4270b().m4259a("Got binder with a wrong descriptor", interfaceDescriptor);
                    }
                } catch (RemoteException e) {
                    this.f2306a.m4052l().m4270b().m4258a("Service connect failed to get IMeasurementService");
                }
                if (c0492q == null) {
                    try {
                        C0389b.m3613a().m3622a(this.f2306a.m4049i(), this.f2306a.f2309a);
                    } catch (IllegalArgumentException e2) {
                    }
                } else {
                    this.f2306a.m4051k().m4349a(new C04701(this, c0492q));
                }
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            C0385p.m3555b("MeasurementServiceConnection.onServiceDisconnected");
            this.f2306a.m4052l().m4287s().m4258a("Service disconnected");
            this.f2306a.m4051k().m4349a(new C04712(this, componentName));
        }
    }

    protected ae(C0517y c0517y) {
        super(c0517y);
        this.f2314f = new ArrayList();
        this.f2313e = new C0475b(c0517y.m4398o());
        this.f2309a = new C0474a(this);
        this.f2312d = new C04631(this, c0517y);
        this.f2315h = new C04642(this, c0517y);
    }

    private void m4023A() {
        m4045e();
        m4035s();
    }

    private void m4024B() {
        m4045e();
        m4052l().m4288t().m4259a("Processing queued up service tasks", Integer.valueOf(this.f2314f.size()));
        for (Runnable a : this.f2314f) {
            m4051k().m4349a(a);
        }
        this.f2314f.clear();
        this.f2315h.m4015c();
    }

    private void m4026a(ComponentName componentName) {
        m4045e();
        if (this.f2310b != null) {
            this.f2310b = null;
            m4052l().m4288t().m4259a("Disconnected from device MeasurementService", componentName);
            m4023A();
        }
    }

    private void m4029a(C0492q c0492q) {
        m4045e();
        C0385p.m3549a((Object) c0492q);
        this.f2310b = c0492q;
        m4034r();
        m4024B();
    }

    private void m4030a(Runnable runnable) {
        m4045e();
        if (m4042b()) {
            runnable.run();
        } else if (((long) this.f2314f.size()) >= m4054n().m4121G()) {
            m4052l().m4270b().m4258a("Discarding data. Max runnable queue size reached");
        } else {
            this.f2314f.add(runnable);
            if (!this.g.m4405v()) {
                this.f2315h.m4013a(60000);
            }
            m4035s();
        }
    }

    private void m4034r() {
        m4045e();
        this.f2313e.m4058a();
        if (!this.g.m4405v()) {
            this.f2312d.m4013a(m4054n().m4156y());
        }
    }

    private void m4035s() {
        m4045e();
        m3962y();
        if (!m4042b()) {
            if (this.f2311c == null) {
                this.f2311c = m4053m().m4335q();
                if (this.f2311c == null) {
                    m4052l().m4288t().m4258a("State of service unknown");
                    this.f2311c = Boolean.valueOf(m4037u());
                    m4053m().m4330a(this.f2311c.booleanValue());
                }
            }
            if (this.f2311c.booleanValue()) {
                m4052l().m4288t().m4258a("Using measurement service");
                this.f2309a.m4018a();
            } else if (m4036t() && !this.g.m4405v()) {
                m4052l().m4288t().m4258a("Using local app measurement service");
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                intent.setComponent(new ComponentName(m4049i(), AppMeasurementService.class));
                this.f2309a.m4020a(intent);
            } else if (m4054n().m4118D()) {
                m4052l().m4288t().m4258a("Using direct local measurement implementation");
                m4029a(new C0524z(this.g, true));
            } else {
                m4052l().m4270b().m4258a("Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    private boolean m4036t() {
        List queryIntentServices = m4049i().getPackageManager().queryIntentServices(new Intent(m4049i(), AppMeasurementService.class), 65536);
        return queryIntentServices != null && queryIntentServices.size() > 0;
    }

    private boolean m4037u() {
        m4045e();
        m3962y();
        if (m4054n().m4117C()) {
            return true;
        }
        Intent intent = new Intent("com.google.android.gms.measurement.START");
        intent.setComponent(new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.measurement.service.MeasurementBrokerService"));
        C0389b a = C0389b.m3613a();
        m4052l().m4288t().m4258a("Checking service availability");
        if (!a.m3624a(m4049i(), intent, new C04697(this), 0)) {
            return false;
        }
        m4052l().m4288t().m4258a("Service available");
        return true;
    }

    private void m4038v() {
        m4045e();
        if (m4042b()) {
            m4052l().m4288t().m4258a("Inactivity, disconnecting from AppMeasurementService");
            m4057q();
        }
    }

    protected void m4039a() {
    }

    protected void m4040a(EventParcel eventParcel, String str) {
        C0385p.m3549a((Object) eventParcel);
        m4045e();
        m3962y();
        m4030a(new C04664(this, str, eventParcel));
    }

    protected void m4041a(UserAttributeParcel userAttributeParcel) {
        m4045e();
        m3962y();
        m4030a(new C04675(this, userAttributeParcel));
    }

    public boolean m4042b() {
        m4045e();
        m3962y();
        return this.f2310b != null;
    }

    public /* bridge */ /* synthetic */ void m4043c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4044d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4045e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4046f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4047g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4048h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4049i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4050j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4051k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4052l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4053m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4054n() {
        return super.m3958n();
    }

    protected void m4055o() {
        m4045e();
        m3962y();
        m4030a(new C04653(this));
    }

    protected void m4056p() {
        m4045e();
        m3962y();
        m4030a(new C04686(this));
    }

    public void m4057q() {
        m4045e();
        m3962y();
        try {
            C0389b.m3613a().m3622a(m4049i(), this.f2309a);
        } catch (IllegalStateException e) {
        } catch (IllegalArgumentException e2) {
        }
        this.f2310b = null;
    }
}
