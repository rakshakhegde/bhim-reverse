package com.google.android.gms.measurement.internal;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.internal.C0439j.C0435b;
import com.google.android.gms.internal.C0439j.C0436c;
import com.google.android.gms.internal.C0439j.C0438e;
import com.google.android.gms.internal.zztd;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/* renamed from: com.google.android.gms.measurement.internal.f */
public class C0479f extends aa {
    C0479f(C0517y c0517y) {
        super(c0517y);
    }

    private Object m4080a(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Float)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Double) {
            return Float.valueOf((float) ((Double) obj).doubleValue());
        } else {
            if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
                return null;
            }
            String valueOf = String.valueOf(obj);
            return valueOf.length() > i ? z ? valueOf.substring(0, i) : null : valueOf;
        }
    }

    private void m4081a(String str, String str2, int i, Object obj) {
        if (obj == null) {
            m4109l().m4285q().m4259a(str + " value can't be null. Ignoring " + str, str2);
        } else if (!(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                String valueOf = String.valueOf(obj);
                if (valueOf.length() > i) {
                    m4109l().m4285q().m4260a("Ignoring " + str + ". Value is too long. name, value length", str2, Integer.valueOf(valueOf.length()));
                }
            }
        }
    }

    public static boolean m4082a(Context context, Class<? extends Service> cls) {
        try {
            ServiceInfo serviceInfo = context.getPackageManager().getServiceInfo(new ComponentName(context, cls), 4);
            if (serviceInfo != null && serviceInfo.enabled) {
                return true;
            }
        } catch (NameNotFoundException e) {
        }
        return false;
    }

    public static boolean m4083a(Context context, Class<? extends BroadcastReceiver> cls, boolean z) {
        try {
            ActivityInfo receiverInfo = context.getPackageManager().getReceiverInfo(new ComponentName(context, cls), 2);
            if (receiverInfo != null && receiverInfo.enabled && (!z || receiverInfo.exported)) {
                return true;
            }
        } catch (NameNotFoundException e) {
        }
        return false;
    }

    private int m4084e(String str) {
        return "_ldl".equals(str) ? m4111n().m4150s() : m4111n().m4149r();
    }

    public Object m4085a(String str, Object obj) {
        int p = (TextUtils.isEmpty(str) || !str.startsWith(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)) ? m4111n().m4147p() : m4111n().m4148q();
        return m4080a(p, obj, false);
    }

    public void m4086a(Bundle bundle, String str, Object obj) {
        if (obj instanceof Long) {
            bundle.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof Float) {
            bundle.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof String) {
            bundle.putString(str, String.valueOf(obj));
        } else if (str != null) {
            m4109l().m4285q().m4260a("Not putting event parameter. Invalid value type. name, type", str, obj.getClass().getSimpleName());
        }
    }

    public void m4087a(C0435b c0435b, Object obj) {
        C0385p.m3549a(obj);
        c0435b.f2125b = null;
        c0435b.f2126c = null;
        c0435b.f2127d = null;
        if (obj instanceof String) {
            c0435b.f2125b = (String) obj;
        } else if (obj instanceof Long) {
            c0435b.f2126c = (Long) obj;
        } else if (obj instanceof Float) {
            c0435b.f2127d = (Float) obj;
        } else {
            m4109l().m4270b().m4259a("Ignoring invalid (type) event param value", obj);
        }
    }

    public void m4088a(C0438e c0438e, Object obj) {
        C0385p.m3549a(obj);
        c0438e.f2159c = null;
        c0438e.f2160d = null;
        c0438e.f2161e = null;
        if (obj instanceof String) {
            c0438e.f2159c = (String) obj;
        } else if (obj instanceof Long) {
            c0438e.f2160d = (Long) obj;
        } else if (obj instanceof Float) {
            c0438e.f2161e = (Float) obj;
        } else {
            m4109l().m4270b().m4259a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    public void m4089a(String str) {
        m4090a("event", m4111n().m4133b(), str);
    }

    void m4090a(String str, int i, String str2) {
        if (str2 == null) {
            throw new IllegalArgumentException(str + " name is required and can't be null");
        } else if (str2.length() == 0) {
            throw new IllegalArgumentException(str + " name is required and can't be empty");
        } else {
            char charAt = str2.charAt(0);
            if (Character.isLetter(charAt) || charAt == '_') {
                int i2 = 1;
                while (i2 < str2.length()) {
                    char charAt2 = str2.charAt(i2);
                    if (charAt2 == '_' || Character.isLetterOrDigit(charAt2)) {
                        i2++;
                    } else {
                        throw new IllegalArgumentException(str + " name must consist of letters, digits or _ (underscores)");
                    }
                }
                if (str2.length() > i) {
                    throw new IllegalArgumentException(str + " name is too long. The maximum supported length is " + i);
                }
                return;
            }
            throw new IllegalArgumentException(str + " name must start with a letter or _");
        }
    }

    public boolean m4091a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(m4105h().m3779a() - j) > j2;
    }

    public byte[] m4092a(C0436c c0436c) {
        try {
            byte[] bArr = new byte[c0436c.m3803e()];
            zztd a = zztd.m3876a(bArr);
            c0436c.m3819a(a);
            a.m3909b();
            return bArr;
        } catch (IOException e) {
            m4109l().m4270b().m4259a("Data loss. Failed to serialize batch", e);
            return null;
        }
    }

    public byte[] m4093a(byte[] bArr) {
        try {
            OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            m4109l().m4270b().m4259a("Failed to gzip content", e);
            throw e;
        }
    }

    public void m4094b(String str) {
        m4090a("user attribute", m4111n().m4146o(), str);
    }

    public void m4095b(String str, Object obj) {
        if ("_ldl".equals(str)) {
            m4081a("user attribute referrer", str, m4084e(str), obj);
        } else {
            m4081a("user attribute", str, m4084e(str), obj);
        }
    }

    public byte[] m4096b(byte[] bArr) {
        try {
            InputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read <= 0) {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
                byteArrayOutputStream.write(bArr2, 0, read);
            }
        } catch (IOException e) {
            m4109l().m4270b().m4259a("Failed to ungzip content", e);
            throw e;
        }
    }

    public Object m4097c(String str, Object obj) {
        return "_ldl".equals(str) ? m4080a(m4084e(str), obj, true) : m4080a(m4084e(str), obj, false);
    }

    public /* bridge */ /* synthetic */ void m4098c() {
        super.m3947c();
    }

    public void m4099c(String str) {
        m4090a("event param", m4111n().m4146o(), str);
    }

    public /* bridge */ /* synthetic */ void m4100d() {
        super.m3948d();
    }

    public boolean m4101d(String str) {
        m4102e();
        return m4106i().checkCallingOrSelfPermission(str) == 0;
    }

    public /* bridge */ /* synthetic */ void m4102e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4103f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4104g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4105h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4106i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4107j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4108k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4109l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4110m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4111n() {
        return super.m3958n();
    }
}
