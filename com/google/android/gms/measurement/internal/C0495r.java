package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.measurement.C0454b;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;

/* renamed from: com.google.android.gms.measurement.internal.r */
public class C0495r extends ab {
    private static final X500Principal f2365a;
    private String f2366b;
    private String f2367c;
    private String f2368d;
    private String f2369e;
    private long f2370f;
    private String f2371h;

    static {
        f2365a = new X500Principal("CN=Android Debug,O=Android,C=US");
    }

    C0495r(C0517y c0517y) {
        super(c0517y);
    }

    static long m4236a(byte[] bArr) {
        long j = null;
        C0385p.m3549a((Object) bArr);
        C0385p.m3553a(bArr.length > 0);
        long j2 = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j2 += (((long) bArr[length]) & 255) << j;
            j += 8;
            length--;
        }
        return j2;
    }

    AppMetadata m4237a(String str) {
        return new AppMetadata(this.f2366b, m4239b(), this.f2367c, this.f2368d, m4251n().m4116B(), m4252o(), str, m4250m().m4336r());
    }

    protected void m4238a() {
        String str = "Unknown";
        String str2 = "Unknown";
        PackageManager packageManager = m4246i().getPackageManager();
        String packageName = m4246i().getPackageName();
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        if (installerPackageName == null) {
            installerPackageName = "manual_install";
        } else if (GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE.equals(installerPackageName)) {
            installerPackageName = BuildConfig.FLAVOR;
        }
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(m4246i().getPackageName(), 0);
            if (packageInfo != null) {
                CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    str2 = applicationLabel.toString();
                }
                str = packageInfo.versionName;
            }
        } catch (NameNotFoundException e) {
            m4249l().m4270b().m4259a("Error retrieving package info: appName", str2);
        }
        this.f2366b = packageName;
        this.f2368d = installerPackageName;
        this.f2367c = str;
        this.f2369e = str2;
        long j = 0;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            if (!m4253p()) {
                PackageInfo packageInfo2 = packageManager.getPackageInfo(m4246i().getPackageName(), 64);
                if (!(instance == null || packageInfo2.signatures == null || packageInfo2.signatures.length <= 0)) {
                    j = C0495r.m4236a(instance.digest(packageInfo2.signatures[0].toByteArray()));
                }
            }
        } catch (NoSuchAlgorithmException e2) {
            m4249l().m4270b().m4259a("Could not get MD5 instance", e2);
        } catch (NameNotFoundException e3) {
            m4249l().m4270b().m4259a("Package name not found", e3);
        }
        this.f2370f = j;
    }

    String m4239b() {
        m3962y();
        if (m4251n().m4117C()) {
            return BuildConfig.FLAVOR;
        }
        if (this.f2371h == null) {
            Status a = C0454b.m3930a(m4246i());
            if (a == null || !a.m3058d()) {
                this.f2371h = BuildConfig.FLAVOR;
                m4249l().m4270b().m4259a("getGoogleAppId failed with status", Integer.valueOf(a == null ? 0 : a.m3059e()));
                if (!(a == null || a.m3056b() == null)) {
                    m4249l().m4287s().m4258a(a.m3056b());
                }
            } else {
                try {
                    if (C0454b.m3932c()) {
                        String a2 = C0454b.m3931a();
                        if (TextUtils.isEmpty(a2)) {
                            a2 = BuildConfig.FLAVOR;
                        }
                        this.f2371h = a2;
                    } else {
                        this.f2371h = BuildConfig.FLAVOR;
                    }
                } catch (IllegalStateException e) {
                    this.f2371h = BuildConfig.FLAVOR;
                    m4249l().m4270b().m4259a("getGoogleAppId or isMeasurementEnabled failed with exception", e);
                }
            }
        }
        return this.f2371h;
    }

    public /* bridge */ /* synthetic */ void m4240c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4241d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4242e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4243f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4244g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4245h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4246i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4247j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4248k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4249l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4250m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4251n() {
        return super.m3958n();
    }

    long m4252o() {
        m3962y();
        return this.f2370f;
    }

    boolean m4253p() {
        try {
            PackageInfo packageInfo = m4246i().getPackageManager().getPackageInfo(m4246i().getPackageName(), 64);
            if (!(packageInfo == null || packageInfo.signatures == null || packageInfo.signatures.length <= 0)) {
                return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(packageInfo.signatures[0].toByteArray()))).getSubjectX500Principal().equals(f2365a);
            }
        } catch (CertificateException e) {
            m4249l().m4270b().m4259a("Error obtaining certificate", e);
        } catch (NameNotFoundException e2) {
            m4249l().m4270b().m4259a("Package name not found", e2);
        }
        return true;
    }
}
