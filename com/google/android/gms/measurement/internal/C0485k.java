package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.google.android.gms.internal.C0428e;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.measurement.internal.k */
public class C0485k extends ab {
    private long f2331a;
    private String f2332b;

    C0485k(C0517y c0517y) {
        super(c0517y);
    }

    protected void m4190a() {
        Calendar instance = Calendar.getInstance();
        this.f2331a = TimeUnit.MINUTES.convert((long) (instance.get(16) + instance.get(15)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        this.f2332b = locale.getLanguage().toLowerCase(Locale.ENGLISH) + "-" + locale.getCountry().toLowerCase(Locale.ENGLISH);
    }

    public String m4191b() {
        m3962y();
        return Build.MODEL;
    }

    public /* bridge */ /* synthetic */ void m4192c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4193d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4194e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4195f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4196g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4197h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4198i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4199j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4200k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4201l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4202m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4203n() {
        return super.m3958n();
    }

    public String m4204o() {
        m3962y();
        return VERSION.RELEASE;
    }

    public long m4205p() {
        m3962y();
        return this.f2331a;
    }

    public String m4206q() {
        m3962y();
        return this.f2332b;
    }
}
