package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.C0097c.C0093b;
import com.google.android.gms.common.api.C0097c.C0094c;
import com.google.android.gms.common.internal.C0355d;
import com.google.android.gms.common.internal.C0370h;
import com.google.android.gms.measurement.internal.C0492q.C0494a;

/* renamed from: com.google.android.gms.measurement.internal.s */
public class C0496s extends C0370h<C0492q> {
    public C0496s(Context context, Looper looper, C0355d c0355d, C0093b c0093b, C0094c c0094c) {
        super(context, looper, 93, c0355d, c0093b, c0094c);
    }

    public /* synthetic */ IInterface m4254a(IBinder iBinder) {
        return m4256b(iBinder);
    }

    protected String m4255a() {
        return "com.google.android.gms.measurement.START";
    }

    public C0492q m4256b(IBinder iBinder) {
        return C0494a.m4235a(iBinder);
    }

    protected String m4257b() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }
}
