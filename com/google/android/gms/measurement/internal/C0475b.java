package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;

/* renamed from: com.google.android.gms.measurement.internal.b */
class C0475b {
    private final C0428e f2316a;
    private long f2317b;

    public C0475b(C0428e c0428e) {
        C0385p.m3549a((Object) c0428e);
        this.f2316a = c0428e;
    }

    public void m4058a() {
        this.f2317b = this.f2316a.m3780b();
    }

    public boolean m4059a(long j) {
        return this.f2317b == 0 || this.f2316a.m3780b() - this.f2317b >= j;
    }

    public void m4060b() {
        this.f2317b = 0;
    }
}
