package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

/* renamed from: com.google.android.gms.measurement.internal.x */
public class C0514x extends ab {
    private C0513c f2432a;
    private C0513c f2433b;
    private final BlockingQueue<FutureTask<?>> f2434c;
    private final BlockingQueue<FutureTask<?>> f2435d;
    private final UncaughtExceptionHandler f2436e;
    private final UncaughtExceptionHandler f2437f;
    private final Object f2438h;
    private final Semaphore f2439i;
    private volatile boolean f2440j;

    /* renamed from: com.google.android.gms.measurement.internal.x.a */
    private final class C0511a<V> extends FutureTask<V> {
        final /* synthetic */ C0514x f2425a;
        private final String f2426b;

        C0511a(C0514x c0514x, Runnable runnable, String str) {
            this.f2425a = c0514x;
            super(runnable, null);
            C0385p.m3549a((Object) str);
            this.f2426b = str;
        }

        protected void setException(Throwable th) {
            this.f2425a.m4360l().m4270b().m4259a(this.f2426b, th);
            super.setException(th);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.x.b */
    private final class C0512b implements UncaughtExceptionHandler {
        final /* synthetic */ C0514x f2427a;
        private final String f2428b;

        public C0512b(C0514x c0514x, String str) {
            this.f2427a = c0514x;
            C0385p.m3549a((Object) str);
            this.f2428b = str;
        }

        public synchronized void uncaughtException(Thread thread, Throwable th) {
            this.f2427a.m4360l().m4270b().m4259a(this.f2428b, th);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.x.c */
    private final class C0513c extends Thread {
        final /* synthetic */ C0514x f2429a;
        private final Object f2430b;
        private final BlockingQueue<FutureTask<?>> f2431c;

        public C0513c(C0514x c0514x, String str, BlockingQueue<FutureTask<?>> blockingQueue) {
            this.f2429a = c0514x;
            C0385p.m3549a((Object) str);
            this.f2430b = new Object();
            this.f2431c = blockingQueue;
            setName(str);
        }

        private void m4337a(InterruptedException interruptedException) {
            this.f2429a.m4360l().m4283o().m4259a(getName() + " was interrupted", interruptedException);
        }

        public void m4338a() {
            synchronized (this.f2430b) {
                this.f2430b.notifyAll();
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r4 = this;
        L_0x0000:
            r0 = r4.f2431c;
            r0 = r0.poll();
            r0 = (java.util.concurrent.FutureTask) r0;
            if (r0 == 0) goto L_0x000e;
        L_0x000a:
            r0.run();
            goto L_0x0000;
        L_0x000e:
            r1 = r4.f2430b;
            monitor-enter(r1);
            r0 = r4.f2431c;	 Catch:{ all -> 0x005f }
            r0 = r0.peek();	 Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0028;
        L_0x0019:
            r0 = r4.f2429a;	 Catch:{ all -> 0x005f }
            r0 = r0.f2440j;	 Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0028;
        L_0x0021:
            r0 = r4.f2430b;	 Catch:{ InterruptedException -> 0x005a }
            r2 = 30000; // 0x7530 float:4.2039E-41 double:1.4822E-319;
            r0.wait(r2);	 Catch:{ InterruptedException -> 0x005a }
        L_0x0028:
            monitor-exit(r1);	 Catch:{ all -> 0x005f }
            r0 = r4.f2429a;
            r1 = r0.f2438h;
            monitor-enter(r1);
            r0 = r4.f2431c;	 Catch:{ all -> 0x0071 }
            r0 = r0.peek();	 Catch:{ all -> 0x0071 }
            if (r0 != 0) goto L_0x0084;
        L_0x0038:
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r0 = r0.f2439i;	 Catch:{ all -> 0x0071 }
            r0.release();	 Catch:{ all -> 0x0071 }
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r0 = r0.f2438h;	 Catch:{ all -> 0x0071 }
            r0.notifyAll();	 Catch:{ all -> 0x0071 }
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r0 = r0.f2432a;	 Catch:{ all -> 0x0071 }
            if (r4 != r0) goto L_0x0062;
        L_0x0052:
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r2 = 0;
            r0.f2432a = r2;	 Catch:{ all -> 0x0071 }
        L_0x0058:
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            return;
        L_0x005a:
            r0 = move-exception;
            r4.m4337a(r0);	 Catch:{ all -> 0x005f }
            goto L_0x0028;
        L_0x005f:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x005f }
            throw r0;
        L_0x0062:
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r0 = r0.f2433b;	 Catch:{ all -> 0x0071 }
            if (r4 != r0) goto L_0x0074;
        L_0x006a:
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r2 = 0;
            r0.f2433b = r2;	 Catch:{ all -> 0x0071 }
            goto L_0x0058;
        L_0x0071:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            throw r0;
        L_0x0074:
            r0 = r4.f2429a;	 Catch:{ all -> 0x0071 }
            r0 = r0.m4360l();	 Catch:{ all -> 0x0071 }
            r0 = r0.m4270b();	 Catch:{ all -> 0x0071 }
            r2 = "Current scheduler thread is neither worker nor network";
            r0.m4258a(r2);	 Catch:{ all -> 0x0071 }
            goto L_0x0058;
        L_0x0084:
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            goto L_0x0000;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.x.c.run():void");
        }
    }

    C0514x(C0517y c0517y) {
        super(c0517y);
        this.f2438h = new Object();
        this.f2439i = new Semaphore(2);
        this.f2434c = new LinkedBlockingQueue();
        this.f2435d = new LinkedBlockingQueue();
        this.f2436e = new C0512b(this, "Thread death: Uncaught exception on worker thread");
        this.f2437f = new C0512b(this, "Thread death: Uncaught exception on network thread");
    }

    private void m4340a(FutureTask<?> futureTask) {
        synchronized (this.f2438h) {
            this.f2434c.add(futureTask);
            if (this.f2432a == null) {
                this.f2432a = new C0513c(this, "Measurement Worker", this.f2434c);
                this.f2432a.setUncaughtExceptionHandler(this.f2436e);
                this.f2432a.start();
            } else {
                this.f2432a.m4338a();
            }
        }
    }

    private void m4344b(FutureTask<?> futureTask) {
        synchronized (this.f2438h) {
            this.f2435d.add(futureTask);
            if (this.f2433b == null) {
                this.f2433b = new C0513c(this, "Measurement Network", this.f2435d);
                this.f2433b.setUncaughtExceptionHandler(this.f2437f);
                this.f2433b.start();
            } else {
                this.f2433b.m4338a();
            }
        }
    }

    protected void m4348a() {
    }

    public void m4349a(Runnable runnable) {
        m3962y();
        C0385p.m3549a((Object) runnable);
        m4340a(new C0511a(this, runnable, "Task exception on worker thread"));
    }

    public void m4350b(Runnable runnable) {
        m3962y();
        C0385p.m3549a((Object) runnable);
        m4344b(new C0511a(this, runnable, "Task exception on network thread"));
    }

    public /* bridge */ /* synthetic */ void m4351c() {
        super.m3947c();
    }

    public void m4352d() {
        if (Thread.currentThread() != this.f2433b) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public void m4353e() {
        if (Thread.currentThread() != this.f2432a) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public /* bridge */ /* synthetic */ C0495r m4354f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4355g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4356h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4357i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4358j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4359k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4360l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4361m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4362n() {
        return super.m3958n();
    }
}
