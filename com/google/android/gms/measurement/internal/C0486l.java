package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C0385p;
import java.util.Iterator;

/* renamed from: com.google.android.gms.measurement.internal.l */
public class C0486l {
    final String f2333a;
    final String f2334b;
    final String f2335c;
    final long f2336d;
    final long f2337e;
    final EventParams f2338f;

    C0486l(C0517y c0517y, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        C0385p.m3551a(str2);
        C0385p.m3551a(str3);
        this.f2333a = str2;
        this.f2334b = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f2335c = str;
        this.f2336d = j;
        this.f2337e = j2;
        if (this.f2337e != 0 && this.f2337e > this.f2336d) {
            c0517y.m4389f().m4283o().m4258a("Event created with reverse previous/current timestamps");
        }
        this.f2338f = m4207a(c0517y, bundle);
    }

    private C0486l(C0517y c0517y, String str, String str2, String str3, long j, long j2, EventParams eventParams) {
        C0385p.m3551a(str2);
        C0385p.m3551a(str3);
        C0385p.m3549a((Object) eventParams);
        this.f2333a = str2;
        this.f2334b = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f2335c = str;
        this.f2336d = j;
        this.f2337e = j2;
        if (this.f2337e != 0 && this.f2337e > this.f2336d) {
            c0517y.m4389f().m4283o().m4258a("Event created with reverse previous/current timestamps");
        }
        this.f2338f = eventParams;
    }

    private EventParams m4207a(C0517y c0517y, Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return new EventParams(new Bundle());
        }
        Bundle bundle2 = new Bundle(bundle);
        Iterator it = bundle2.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str == null) {
                it.remove();
            } else {
                Object a = c0517y.m4394k().m4085a(str, bundle2.get(str));
                if (a == null) {
                    it.remove();
                } else {
                    c0517y.m4394k().m4086a(bundle2, str, a);
                }
            }
        }
        return new EventParams(bundle2);
    }

    C0486l m4208a(C0517y c0517y, long j) {
        return new C0486l(c0517y, this.f2335c, this.f2333a, this.f2334b, this.f2336d, j, this.f2338f);
    }

    public String toString() {
        return "Event{appId='" + this.f2333a + '\'' + ", name='" + this.f2334b + '\'' + ", params=" + this.f2338f + '}';
    }
}
