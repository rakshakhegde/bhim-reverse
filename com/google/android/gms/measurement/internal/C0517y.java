package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import com.google.android.gms.internal.C0439j.C0434a;
import com.google.android.gms.internal.C0439j.C0435b;
import com.google.android.gms.internal.C0439j.C0436c;
import com.google.android.gms.internal.C0439j.C0437d;
import com.google.android.gms.internal.C0439j.C0438e;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;
import com.google.android.gms.measurement.C0453a;
import com.google.android.gms.measurement.internal.C0504u.C0501a;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.y */
public class C0517y {
    private static ac f2443a;
    private static volatile C0517y f2444b;
    private final Context f2445c;
    private final C0481h f2446d;
    private final C0510w f2447e;
    private final C0499t f2448f;
    private final C0514x f2449g;
    private final C0453a f2450h;
    private final C0479f f2451i;
    private final C0483i f2452j;
    private final C0504u f2453k;
    private final C0428e f2454l;
    private final ae f2455m;
    private final C0485k f2456n;
    private final ad f2457o;
    private final C0495r f2458p;
    private final C0506v f2459q;
    private final C0476c f2460r;
    private final boolean f2461s;
    private Boolean f2462t;
    private List<Long> f2463u;
    private int f2464v;
    private int f2465w;

    /* renamed from: com.google.android.gms.measurement.internal.y.1 */
    class C05151 implements Runnable {
        final /* synthetic */ C0517y f2441a;

        C05151(C0517y c0517y) {
            this.f2441a = c0517y;
        }

        public void run() {
            this.f2441a.m4386c();
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.y.2 */
    class C05162 implements C0501a {
        final /* synthetic */ C0517y f2442a;

        C05162(C0517y c0517y) {
            this.f2442a = c0517y;
        }

        public void m4363a(int i, Throwable th, byte[] bArr) {
            this.f2442a.m4368a(i, th, bArr);
        }
    }

    C0517y(ac acVar) {
        C0385p.m3549a((Object) acVar);
        this.f2445c = acVar.f2261a;
        this.f2454l = acVar.m3974j(this);
        this.f2446d = acVar.m3964a(this);
        C0510w b = acVar.m3966b(this);
        b.m3963z();
        this.f2447e = b;
        C0499t c = acVar.m3967c(this);
        c.m3963z();
        this.f2448f = c;
        this.f2451i = acVar.m3971g(this);
        C0485k l = acVar.m3976l(this);
        l.m3963z();
        this.f2456n = l;
        C0495r m = acVar.m3977m(this);
        m.m3963z();
        this.f2458p = m;
        C0483i h = acVar.m3972h(this);
        h.m3963z();
        this.f2452j = h;
        C0504u i = acVar.m3973i(this);
        i.m3963z();
        this.f2453k = i;
        ae k = acVar.m3975k(this);
        k.m3963z();
        this.f2455m = k;
        ad f = acVar.m3970f(this);
        f.m3963z();
        this.f2457o = f;
        C0476c o = acVar.m3979o(this);
        o.m3963z();
        this.f2460r = o;
        this.f2459q = acVar.m3978n(this);
        this.f2450h = acVar.m3969e(this);
        C0514x d = acVar.m3968d(this);
        d.m3963z();
        this.f2449g = d;
        if (this.f2464v != this.f2465w) {
            m4389f().m4270b().m4260a("Not all components initialized", Integer.valueOf(this.f2464v), Integer.valueOf(this.f2465w));
        }
        this.f2461s = true;
        if (!(this.f2446d.m4117C() || m4405v())) {
            if (!(this.f2445c.getApplicationContext() instanceof Application)) {
                m4389f().m4283o().m4258a("Application context is not an Application");
            } else if (VERSION.SDK_INT >= 14) {
                m4392i().m3994b();
            } else {
                m4389f().m4287s().m4258a("Not tracking deep linking pre-ICS");
            }
        }
        this.f2449g.m4349a(new C05151(this));
    }

    private boolean m4364A() {
        return !TextUtils.isEmpty(m4395l().m4186r());
    }

    private void m4365B() {
        m4404u();
        m4376a();
        if (m4385b() && m4364A()) {
            long C = m4366C();
            if (C == 0) {
                m4402s().m4313b();
                m4403t().m4064b();
                return;
            } else if (m4396m().m4296b()) {
                long a = m4388e().f2418e.m4316a();
                long L = m4387d().m4126L();
                if (!m4394k().m4091a(a, L)) {
                    C = Math.max(C, a + L);
                }
                m4402s().m4313b();
                C -= m4398o().m3779a();
                if (C <= 0) {
                    m4403t().m4063a(1);
                    return;
                }
                m4389f().m4288t().m4259a("Upload scheduled in approximately ms", Long.valueOf(C));
                m4403t().m4063a(C);
                return;
            } else {
                m4402s().m4312a();
                m4403t().m4064b();
                return;
            }
        }
        m4402s().m4313b();
        m4403t().m4064b();
    }

    private long m4366C() {
        long a = m4398o().m3779a();
        long O = m4387d().m4129O();
        long M = m4387d().m4127M();
        long a2 = m4388e().f2416c.m4316a();
        long a3 = m4388e().f2417d.m4316a();
        long u = m4395l().m4189u();
        if (u == 0) {
            return 0;
        }
        a -= Math.abs(u - a);
        O += a;
        if (!m4394k().m4091a(a2, M)) {
            O = a2 + M;
        }
        if (a3 == 0 || a3 < a) {
            return O;
        }
        for (int i = 0; i < m4387d().m4131Q(); i++) {
            O += ((long) (1 << i)) * m4387d().m4130P();
            if (O > a3) {
                return O;
            }
        }
        return 0;
    }

    public static C0517y m4367a(Context context) {
        C0385p.m3549a((Object) context);
        C0385p.m3549a(context.getApplicationContext());
        if (f2444b == null) {
            synchronized (C0517y.class) {
                if (f2444b == null) {
                    f2444b = (f2443a != null ? f2443a : new ac(context)).m3965a();
                }
            }
        }
        return f2444b;
    }

    private void m4368a(int i, Throwable th, byte[] bArr) {
        int i2 = 0;
        m4404u();
        m4376a();
        if (bArr == null) {
            bArr = new byte[0];
        }
        List<Long> list = this.f2463u;
        this.f2463u = null;
        if ((i == 200 || i == 204) && th == null) {
            m4388e().f2416c.m4317a(m4398o().m3779a());
            m4388e().f2417d.m4317a(0);
            m4365B();
            m4389f().m4288t().m4260a("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
            m4395l().m4181b();
            try {
                for (Long longValue : list) {
                    m4395l().m4173a(longValue.longValue());
                }
                m4395l().m4183o();
                if (m4396m().m4296b() && m4364A()) {
                    m4407x();
                } else {
                    m4365B();
                }
            } finally {
                m4395l().m4184p();
            }
        } else {
            m4389f().m4288t().m4260a("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            m4388e().f2417d.m4317a(m4398o().m3779a());
            if (i == 503 || i == 429) {
                i2 = 1;
            }
            if (i2 != 0) {
                m4388e().f2418e.m4317a(m4398o().m3779a());
            }
            m4365B();
        }
    }

    private void m4369a(aa aaVar) {
        if (aaVar == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    private void m4371a(List<Long> list) {
        C0385p.m3556b(!list.isEmpty());
        if (this.f2463u != null) {
            m4389f().m4270b().m4258a("Set uploading progress before finishing the previous upload");
        } else {
            this.f2463u = new ArrayList(list);
        }
    }

    private void m4372b(ab abVar) {
        if (abVar == null) {
            throw new IllegalStateException("Component not created");
        } else if (!abVar.m3960w()) {
            throw new IllegalStateException("Component not initialized");
        }
    }

    private void m4373c(AppMetadata appMetadata) {
        m4404u();
        m4376a();
        C0385p.m3549a((Object) appMetadata);
        C0385p.m3551a(appMetadata.f2222b);
        C0456a b = m4395l().m4179b(appMetadata.f2222b);
        String o = m4388e().m4333o();
        Object obj = null;
        if (b == null) {
            b = new C0456a(appMetadata.f2222b, m4388e().m4334p(), appMetadata.f2223c, o, 0, 0, appMetadata.f2224d, appMetadata.f2225e, appMetadata.f2226f, appMetadata.f2227g, appMetadata.f2229i);
            obj = 1;
        } else if (!o.equals(b.f2249d)) {
            b = b.m3944a(m4388e().m4334p(), o);
            obj = 1;
        }
        if (!(TextUtils.isEmpty(appMetadata.f2223c) || (appMetadata.f2223c.equals(b.f2248c) && appMetadata.f2226f == b.f2254i))) {
            b = b.m3943a(appMetadata.f2223c, appMetadata.f2226f);
            obj = 1;
        }
        if (!(TextUtils.isEmpty(appMetadata.f2224d) || (appMetadata.f2224d.equals(b.f2252g) && appMetadata.f2225e.equals(b.f2253h)))) {
            b = b.m3946b(appMetadata.f2224d, appMetadata.f2225e);
            obj = 1;
        }
        if (appMetadata.f2227g != b.f2255j) {
            b = b.m3941a(appMetadata.f2227g);
            obj = 1;
        }
        if (appMetadata.f2229i != b.f2256k) {
            b = b.m3945a(appMetadata.f2229i);
            obj = 1;
        }
        if (obj != null) {
            m4395l().m4176a(b);
        }
    }

    private boolean m4374z() {
        m4404u();
        return this.f2463u != null;
    }

    C0437d m4375a(C0486l[] c0486lArr, AppMetadata appMetadata) {
        int i;
        C0385p.m3549a((Object) appMetadata);
        C0385p.m3549a((Object) c0486lArr);
        m4404u();
        C0437d c0437d = new C0437d();
        c0437d.f2130a = Integer.valueOf(1);
        c0437d.f2138i = AbstractSpiCall.ANDROID_CLIENT_TYPE;
        c0437d.f2144o = appMetadata.f2222b;
        c0437d.f2143n = appMetadata.f2225e;
        c0437d.f2145p = appMetadata.f2224d;
        c0437d.f2146q = Long.valueOf(appMetadata.f2226f);
        c0437d.f2154y = appMetadata.f2223c;
        c0437d.f2151v = appMetadata.f2227g == 0 ? null : Long.valueOf(appMetadata.f2227g);
        Pair b = m4388e().m4331b();
        if (!(b == null || b.first == null || b.second == null)) {
            c0437d.f2148s = (String) b.first;
            c0437d.f2149t = (Boolean) b.second;
        }
        c0437d.f2140k = m4400q().m4191b();
        c0437d.f2139j = m4400q().m4204o();
        c0437d.f2142m = Integer.valueOf((int) m4400q().m4205p());
        c0437d.f2141l = m4400q().m4206q();
        c0437d.f2147r = null;
        c0437d.f2133d = null;
        c0437d.f2134e = Long.valueOf(c0486lArr[0].f2336d);
        c0437d.f2135f = Long.valueOf(c0486lArr[0].f2336d);
        for (int i2 = 1; i2 < c0486lArr.length; i2++) {
            c0437d.f2134e = Long.valueOf(Math.min(c0437d.f2134e.longValue(), c0486lArr[i2].f2336d));
            c0437d.f2135f = Long.valueOf(Math.max(c0437d.f2135f.longValue(), c0486lArr[i2].f2336d));
        }
        C0456a b2 = m4395l().m4179b(appMetadata.f2222b);
        if (b2 == null) {
            b2 = new C0456a(appMetadata.f2222b, m4388e().m4334p(), appMetadata.f2223c, m4388e().m4333o(), 0, 0, appMetadata.f2224d, appMetadata.f2225e, appMetadata.f2226f, appMetadata.f2227g, appMetadata.f2229i);
        }
        C0456a a = b2.m3942a(m4389f(), c0437d.f2135f.longValue());
        m4395l().m4176a(a);
        c0437d.f2150u = a.f2247b;
        c0437d.f2152w = Integer.valueOf((int) a.f2250e);
        c0437d.f2137h = b2.f2251f == 0 ? null : Long.valueOf(b2.f2251f);
        c0437d.f2136g = c0437d.f2137h;
        List a2 = m4395l().m4170a(appMetadata.f2222b);
        c0437d.f2132c = new C0438e[a2.size()];
        for (i = 0; i < a2.size(); i++) {
            C0438e c0438e = new C0438e();
            c0437d.f2132c[i] = c0438e;
            c0438e.f2158b = ((C0477d) a2.get(i)).f2321b;
            c0438e.f2157a = Long.valueOf(((C0477d) a2.get(i)).f2322c);
            m4394k().m4088a(c0438e, ((C0477d) a2.get(i)).f2323d);
        }
        c0437d.f2131b = new C0434a[c0486lArr.length];
        for (i = 0; i < c0486lArr.length; i++) {
            C0434a c0434a = new C0434a();
            c0437d.f2131b[i] = c0434a;
            c0434a.f2119b = c0486lArr[i].f2334b;
            c0434a.f2120c = Long.valueOf(c0486lArr[i].f2336d);
            c0434a.f2118a = new C0435b[c0486lArr[i].f2338f.m3937a()];
            Iterator it = c0486lArr[i].f2338f.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                String str = (String) it.next();
                C0435b c0435b = new C0435b();
                int i4 = i3 + 1;
                c0434a.f2118a[i3] = c0435b;
                c0435b.f2124a = str;
                m4394k().m4087a(c0435b, c0486lArr[i].f2338f.m3938a(str));
                i3 = i4;
            }
        }
        c0437d.f2153x = m4389f().m4289u();
        return c0437d;
    }

    void m4376a() {
        if (!this.f2461s) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    void m4377a(AppMetadata appMetadata) {
        m4404u();
        m4376a();
        C0385p.m3551a(appMetadata.f2222b);
        m4373c(appMetadata);
    }

    void m4378a(EventParcel eventParcel, AppMetadata appMetadata) {
        m4404u();
        m4376a();
        C0385p.m3551a(appMetadata.f2222b);
        if (!TextUtils.isEmpty(appMetadata.f2223c)) {
            m4389f().m4288t().m4259a("Logging event", eventParcel);
            Object c0486l = new C0486l(this, eventParcel.f2237d, appMetadata.f2222b, eventParcel.f2235b, eventParcel.f2238e, 0, eventParcel.f2236c.m3939b());
            m4395l().m4181b();
            try {
                C0487m c0487m;
                m4373c(appMetadata);
                C0487m a = m4395l().m4169a(appMetadata.f2222b, c0486l.f2334b);
                if (a == null) {
                    c0487m = new C0487m(appMetadata.f2222b, c0486l.f2334b, 1, 1, c0486l.f2336d);
                } else {
                    c0486l = c0486l.m4208a(this, a.f2343e);
                    c0487m = a.m4209a(c0486l.f2336d);
                }
                m4395l().m4178a(c0487m);
                m4395l().m4175a(m4375a(new C0486l[]{c0486l}, appMetadata));
                m4395l().m4183o();
                m4389f().m4287s().m4259a("Event logged", c0486l);
                m4365B();
            } finally {
                m4395l().m4184p();
            }
        }
    }

    void m4379a(EventParcel eventParcel, String str) {
        C0456a b = m4395l().m4179b(str);
        if (b == null || TextUtils.isEmpty(b.f2252g)) {
            m4389f().m4287s().m4259a("No app data available; dropping event", str);
            return;
        }
        m4378a(eventParcel, new AppMetadata(str, b.f2248c, b.f2252g, b.f2253h, b.f2254i, b.f2255j, null, b.f2256k));
    }

    void m4380a(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        m4404u();
        m4376a();
        if (!TextUtils.isEmpty(appMetadata.f2223c)) {
            m4394k().m4094b(userAttributeParcel.f2240b);
            Object c = m4394k().m4097c(userAttributeParcel.f2240b, userAttributeParcel.m3940a());
            if (c != null) {
                C0477d c0477d = new C0477d(appMetadata.f2222b, userAttributeParcel.f2240b, userAttributeParcel.f2241c, c);
                m4389f().m4287s().m4260a("Setting user attribute", c0477d.f2321b, c);
                m4395l().m4181b();
                try {
                    m4373c(appMetadata);
                    m4395l().m4177a(c0477d);
                    m4395l().m4183o();
                    m4389f().m4287s().m4260a("User attribute set", c0477d.f2321b, c0477d.f2323d);
                } finally {
                    m4395l().m4184p();
                }
            }
        }
    }

    void m4381a(ab abVar) {
        this.f2464v++;
    }

    public void m4382a(boolean z) {
        m4365B();
    }

    public void m4383b(AppMetadata appMetadata) {
        m4404u();
        m4376a();
        C0385p.m3549a((Object) appMetadata);
        C0385p.m3551a(appMetadata.f2222b);
        if (!TextUtils.isEmpty(appMetadata.f2223c)) {
            m4373c(appMetadata);
            if (m4395l().m4169a(appMetadata.f2222b, "_f") == null) {
                long a = m4398o().m3779a();
                m4380a(new UserAttributeParcel("_fot", a, Long.valueOf(3600000 * ((a / 3600000) + 1)), "auto"), appMetadata);
                Bundle bundle = new Bundle();
                bundle.putLong("_c", 1);
                m4378a(new EventParcel("_f", new EventParams(bundle), "auto", a), appMetadata);
            }
        }
    }

    void m4384b(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        m4404u();
        m4376a();
        if (!TextUtils.isEmpty(appMetadata.f2223c)) {
            m4389f().m4287s().m4259a("Removing user attribute", userAttributeParcel.f2240b);
            m4395l().m4181b();
            try {
                m4373c(appMetadata);
                m4395l().m4182b(appMetadata.f2222b, userAttributeParcel.f2240b);
                m4395l().m4183o();
                m4389f().m4287s().m4259a("User attribute removed", userAttributeParcel.f2240b);
            } finally {
                m4395l().m4184p();
            }
        }
    }

    protected boolean m4385b() {
        boolean z = true;
        m4376a();
        m4404u();
        if (this.f2462t == null) {
            boolean z2 = m4394k().m4101d("android.permission.INTERNET") && m4394k().m4101d("android.permission.ACCESS_NETWORK_STATE") && AppMeasurementReceiver.m3922a(m4397n()) && AppMeasurementService.m3925a(m4397n());
            this.f2462t = Boolean.valueOf(z2);
            if (this.f2462t.booleanValue() && !m4387d().m4117C()) {
                if (TextUtils.isEmpty(m4401r().m4239b())) {
                    z = false;
                }
                this.f2462t = Boolean.valueOf(z);
            }
        }
        return this.f2462t.booleanValue();
    }

    protected void m4386c() {
        m4404u();
        m4389f().m4286r().m4258a("App measurement is starting up");
        m4389f().m4287s().m4258a("Debug logging enabled");
        if (!m4405v() || (this.f2449g.m3960w() && !this.f2449g.m3961x())) {
            m4395l().m4187s();
            if (!m4385b()) {
                if (!m4394k().m4101d("android.permission.INTERNET")) {
                    m4389f().m4270b().m4258a("App is missing INTERNET permission");
                }
                if (!m4394k().m4101d("android.permission.ACCESS_NETWORK_STATE")) {
                    m4389f().m4270b().m4258a("App is missing ACCESS_NETWORK_STATE permission");
                }
                if (!AppMeasurementReceiver.m3922a(m4397n())) {
                    m4389f().m4270b().m4258a("AppMeasurementReceiver not registered/enabled");
                }
                if (!AppMeasurementService.m3925a(m4397n())) {
                    m4389f().m4270b().m4258a("AppMeasurementService not registered/enabled");
                }
                m4389f().m4270b().m4258a("Uploading is not possible. App measurement disabled");
            } else if (!(m4387d().m4117C() || m4405v() || TextUtils.isEmpty(m4401r().m4239b()))) {
                m4392i().m4007o();
            }
            m4365B();
            return;
        }
        m4389f().m4270b().m4258a("Scheduler shutting down before Scion.start() called");
    }

    public C0481h m4387d() {
        return this.f2446d;
    }

    public C0510w m4388e() {
        m4369a(this.f2447e);
        return this.f2447e;
    }

    public C0499t m4389f() {
        m4372b(this.f2448f);
        return this.f2448f;
    }

    public C0514x m4390g() {
        m4372b(this.f2449g);
        return this.f2449g;
    }

    C0514x m4391h() {
        return this.f2449g;
    }

    public ad m4392i() {
        m4372b(this.f2457o);
        return this.f2457o;
    }

    public C0453a m4393j() {
        return this.f2450h;
    }

    public C0479f m4394k() {
        m4369a(this.f2451i);
        return this.f2451i;
    }

    public C0483i m4395l() {
        m4372b(this.f2452j);
        return this.f2452j;
    }

    public C0504u m4396m() {
        m4372b(this.f2453k);
        return this.f2453k;
    }

    public Context m4397n() {
        return this.f2445c;
    }

    public C0428e m4398o() {
        return this.f2454l;
    }

    public ae m4399p() {
        m4372b(this.f2455m);
        return this.f2455m;
    }

    public C0485k m4400q() {
        m4372b(this.f2456n);
        return this.f2456n;
    }

    public C0495r m4401r() {
        m4372b(this.f2458p);
        return this.f2458p;
    }

    public C0506v m4402s() {
        if (this.f2459q != null) {
            return this.f2459q;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public C0476c m4403t() {
        m4372b(this.f2460r);
        return this.f2460r;
    }

    public void m4404u() {
        m4390g().m4353e();
    }

    protected boolean m4405v() {
        return false;
    }

    void m4406w() {
        if (m4387d().m4117C()) {
            throw new IllegalStateException("Unexpected call on package side");
        }
    }

    public void m4407x() {
        int i = 0;
        m4404u();
        m4376a();
        if (!m4387d().m4117C()) {
            Boolean q = m4388e().m4335q();
            if (q == null) {
                m4389f().m4283o().m4258a("Upload data called on the client side before use of service was decided");
                return;
            } else if (q.booleanValue()) {
                m4389f().m4270b().m4258a("Upload called in the client side when service should be used");
                return;
            }
        }
        if (m4374z()) {
            m4389f().m4283o().m4258a("Uploading requested multiple times");
        } else if (m4396m().m4296b()) {
            long a = m4388e().f2416c.m4316a();
            if (a != 0) {
                m4389f().m4287s().m4259a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(m4398o().m3779a() - a)));
            }
            String r = m4395l().m4186r();
            if (!TextUtils.isEmpty(r)) {
                List<Pair> a2 = m4395l().m4171a(r, m4387d().m4123I(), m4387d().m4124J());
                if (!a2.isEmpty()) {
                    C0437d c0437d;
                    Object obj;
                    List subList;
                    for (Pair pair : a2) {
                        c0437d = (C0437d) pair.first;
                        if (!TextUtils.isEmpty(c0437d.f2148s)) {
                            obj = c0437d.f2148s;
                            break;
                        }
                    }
                    obj = null;
                    if (obj != null) {
                        for (int i2 = 0; i2 < a2.size(); i2++) {
                            c0437d = (C0437d) ((Pair) a2.get(i2)).first;
                            if (!TextUtils.isEmpty(c0437d.f2148s) && !c0437d.f2148s.equals(obj)) {
                                subList = a2.subList(0, i2);
                                break;
                            }
                        }
                    }
                    subList = a2;
                    C0436c c0436c = new C0436c();
                    c0436c.f2128a = new C0437d[subList.size()];
                    List arrayList = new ArrayList(subList.size());
                    long a3 = m4398o().m3779a();
                    while (i < c0436c.f2128a.length) {
                        c0436c.f2128a[i] = (C0437d) ((Pair) subList.get(i)).first;
                        arrayList.add(((Pair) subList.get(i)).second);
                        c0436c.f2128a[i].f2147r = Long.valueOf(m4387d().m4116B());
                        c0436c.f2128a[i].f2133d = Long.valueOf(a3);
                        c0436c.f2128a[i].f2155z = Boolean.valueOf(m4387d().m4117C());
                        i++;
                    }
                    byte[] a4 = m4394k().m4092a(c0436c);
                    String K = m4387d().m4125K();
                    try {
                        URL url = new URL(K);
                        m4371a(arrayList);
                        m4388e().f2417d.m4317a(m4398o().m3779a());
                        m4396m().m4295a(url, a4, new C05162(this));
                    } catch (MalformedURLException e) {
                        m4389f().m4270b().m4259a("Failed to parse upload URL. Not uploading", K);
                    }
                }
            }
        } else {
            m4389f().m4283o().m4258a("Network not connected, ignoring upload request");
            m4365B();
        }
    }

    void m4408y() {
        this.f2465w++;
    }
}
