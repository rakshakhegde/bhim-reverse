package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.o */
public class C0489o implements Creator<EventParcel> {
    static void m4213a(EventParcel eventParcel, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, eventParcel.f2234a);
        C0386a.m3568a(parcel, 2, eventParcel.f2235b, false);
        C0386a.m3565a(parcel, 3, eventParcel.f2236c, i, false);
        C0386a.m3568a(parcel, 4, eventParcel.f2237d, false);
        C0386a.m3562a(parcel, 5, eventParcel.f2238e);
        C0386a.m3560a(parcel, a);
    }

    public EventParcel m4214a(Parcel parcel) {
        String str = null;
        int b = zza.m3582b(parcel);
        int i = 0;
        long j = 0;
        EventParams eventParams = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    str2 = zza.m3590h(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    eventParams = (EventParams) zza.m3579a(parcel, a, EventParams.CREATOR);
                    break;
                case R.View_theme /*4*/:
                    str = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    j = zza.m3587e(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new EventParcel(i, str2, eventParams, str, j);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public EventParcel[] m4215a(int i) {
        return new EventParcel[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m4214a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m4215a(i);
    }
}
