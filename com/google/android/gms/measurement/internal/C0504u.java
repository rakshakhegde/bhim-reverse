package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/* renamed from: com.google.android.gms.measurement.internal.u */
public class C0504u extends ab {

    /* renamed from: com.google.android.gms.measurement.internal.u.a */
    interface C0501a {
        void m4290a(int i, Throwable th, byte[] bArr);
    }

    /* renamed from: com.google.android.gms.measurement.internal.u.b */
    private static class C0502b implements Runnable {
        private final C0501a f2390a;
        private final int f2391b;
        private final Throwable f2392c;
        private final byte[] f2393d;

        private C0502b(C0501a c0501a, int i, Throwable th, byte[] bArr) {
            this.f2390a = c0501a;
            this.f2391b = i;
            this.f2392c = th;
            this.f2393d = bArr;
        }

        public void run() {
            this.f2390a.m4290a(this.f2391b, this.f2392c, this.f2393d);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.u.c */
    private class C0503c implements Runnable {
        final /* synthetic */ C0504u f2394a;
        private final URL f2395b;
        private final byte[] f2396c;
        private final C0501a f2397d;

        public C0503c(C0504u c0504u, URL url, byte[] bArr, C0501a c0501a) {
            this.f2394a = c0504u;
            this.f2395b = url;
            this.f2396c = bArr;
            this.f2397d = c0501a;
        }

        public void run() {
            Throwable e;
            int i;
            Throwable th;
            this.f2394a.m4298d();
            int i2 = 0;
            HttpURLConnection a;
            OutputStream outputStream;
            try {
                byte[] a2 = this.f2394a.m4304j().m4093a(this.f2396c);
                a = this.f2394a.m4293a(this.f2395b);
                try {
                    a.setDoOutput(true);
                    a.addRequestProperty("Content-Encoding", "gzip");
                    a.setFixedLengthStreamingMode(a2.length);
                    a.connect();
                    outputStream = a.getOutputStream();
                } catch (IOException e2) {
                    e = e2;
                    i = i2;
                    outputStream = null;
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e3) {
                            this.f2394a.m4306l().m4270b().m4259a("Error closing HTTP compressed POST connection output stream", e3);
                        }
                    }
                    if (a != null) {
                        a.disconnect();
                    }
                    this.f2394a.m4305k().m4349a(new C0502b(i, e, null, null));
                } catch (Throwable th2) {
                    th = th2;
                    outputStream = null;
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e32) {
                            this.f2394a.m4306l().m4270b().m4259a("Error closing HTTP compressed POST connection output stream", e32);
                        }
                    }
                    if (a != null) {
                        a.disconnect();
                    }
                    this.f2394a.m4305k().m4349a(new C0502b(i2, null, null, null));
                    throw th;
                }
                try {
                    outputStream.write(a2);
                    outputStream.close();
                    outputStream = null;
                    i2 = a.getResponseCode();
                    a2 = this.f2394a.m4292a(a);
                    if (null != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e322) {
                            this.f2394a.m4306l().m4270b().m4259a("Error closing HTTP compressed POST connection output stream", e322);
                        }
                    }
                    if (a != null) {
                        a.disconnect();
                    }
                    this.f2394a.m4305k().m4349a(new C0502b(i2, null, a2, null));
                } catch (IOException e4) {
                    e = e4;
                    i = 0;
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (a != null) {
                        a.disconnect();
                    }
                    this.f2394a.m4305k().m4349a(new C0502b(i, e, null, null));
                } catch (Throwable th3) {
                    th = th3;
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (a != null) {
                        a.disconnect();
                    }
                    this.f2394a.m4305k().m4349a(new C0502b(i2, null, null, null));
                    throw th;
                }
            } catch (IOException e5) {
                e = e5;
                i = 0;
                outputStream = null;
                a = null;
                if (outputStream != null) {
                    outputStream.close();
                }
                if (a != null) {
                    a.disconnect();
                }
                this.f2394a.m4305k().m4349a(new C0502b(i, e, null, null));
            } catch (Throwable th22) {
                th = th22;
                a = null;
                outputStream = null;
                if (outputStream != null) {
                    outputStream.close();
                }
                if (a != null) {
                    a.disconnect();
                }
                this.f2394a.m4305k().m4349a(new C0502b(i2, null, null, null));
                throw th;
            }
        }
    }

    public C0504u(C0517y c0517y) {
        super(c0517y);
    }

    private byte[] m4292a(HttpURLConnection httpURLConnection) {
        InputStream inputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] toByteArray = byteArrayOutputStream.toByteArray();
            return toByteArray;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    protected HttpURLConnection m4293a(URL url) {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.setConnectTimeout((int) m4308n().m4153v());
            httpURLConnection.setReadTimeout((int) m4308n().m4154w());
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setDoInput(true);
            return httpURLConnection;
        }
        throw new IOException("Failed to obtain HTTP connection");
    }

    protected void m4294a() {
    }

    public void m4295a(URL url, byte[] bArr, C0501a c0501a) {
        m4299e();
        m3962y();
        C0385p.m3549a((Object) url);
        C0385p.m3549a((Object) bArr);
        C0385p.m3549a((Object) c0501a);
        m4305k().m4350b(new C0503c(this, url, bArr, c0501a));
    }

    public boolean m4296b() {
        NetworkInfo activeNetworkInfo;
        m3962y();
        try {
            activeNetworkInfo = ((ConnectivityManager) m4303i().getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException e) {
            activeNetworkInfo = null;
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public /* bridge */ /* synthetic */ void m4297c() {
        super.m3947c();
    }

    public /* bridge */ /* synthetic */ void m4298d() {
        super.m3948d();
    }

    public /* bridge */ /* synthetic */ void m4299e() {
        super.m3949e();
    }

    public /* bridge */ /* synthetic */ C0495r m4300f() {
        return super.m3950f();
    }

    public /* bridge */ /* synthetic */ ae m4301g() {
        return super.m3951g();
    }

    public /* bridge */ /* synthetic */ C0428e m4302h() {
        return super.m3952h();
    }

    public /* bridge */ /* synthetic */ Context m4303i() {
        return super.m3953i();
    }

    public /* bridge */ /* synthetic */ C0479f m4304j() {
        return super.m3954j();
    }

    public /* bridge */ /* synthetic */ C0514x m4305k() {
        return super.m3955k();
    }

    public /* bridge */ /* synthetic */ C0499t m4306l() {
        return super.m3956l();
    }

    public /* bridge */ /* synthetic */ C0510w m4307m() {
        return super.m3957m();
    }

    public /* bridge */ /* synthetic */ C0481h m4308n() {
        return super.m3958n();
    }
}
