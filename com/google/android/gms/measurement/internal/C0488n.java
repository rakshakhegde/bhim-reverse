package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.n */
public class C0488n implements Creator<EventParams> {
    static void m4210a(EventParams eventParams, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, eventParams.f2232a);
        C0386a.m3563a(parcel, 2, eventParams.m3939b(), false);
        C0386a.m3560a(parcel, a);
    }

    public EventParams m4211a(Parcel parcel) {
        int b = zza.m3582b(parcel);
        int i = 0;
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    bundle = zza.m3592j(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new EventParams(i, bundle);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public EventParams[] m4212a(int i) {
        return new EventParams[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m4211a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m4212a(i);
    }
}
