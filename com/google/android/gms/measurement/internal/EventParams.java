package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Iterator;

public class EventParams implements SafeParcelable, Iterable<String> {
    public static final C0488n CREATOR;
    public final int f2232a;
    private final Bundle f2233b;

    /* renamed from: com.google.android.gms.measurement.internal.EventParams.1 */
    class C04551 implements Iterator<String> {
        Iterator<String> f2230a;
        final /* synthetic */ EventParams f2231b;

        C04551(EventParams eventParams) {
            this.f2231b = eventParams;
            this.f2230a = this.f2231b.f2233b.keySet().iterator();
        }

        public String m3935a() {
            return (String) this.f2230a.next();
        }

        public boolean hasNext() {
            return this.f2230a.hasNext();
        }

        public /* synthetic */ Object next() {
            return m3935a();
        }

        public void remove() {
            throw new UnsupportedOperationException("Remove not supported");
        }
    }

    static {
        CREATOR = new C0488n();
    }

    EventParams(int i, Bundle bundle) {
        this.f2232a = i;
        this.f2233b = bundle;
    }

    EventParams(Bundle bundle) {
        C0385p.m3549a((Object) bundle);
        this.f2233b = bundle;
        this.f2232a = 1;
    }

    public int m3937a() {
        return this.f2233b.size();
    }

    Object m3938a(String str) {
        return this.f2233b.get(str);
    }

    public Bundle m3939b() {
        return new Bundle(this.f2233b);
    }

    public int describeContents() {
        return 0;
    }

    public Iterator<String> iterator() {
        return new C04551(this);
    }

    public String toString() {
        return this.f2233b.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0488n.m4210a(this, parcel, i);
    }
}
