package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.q */
public interface C0492q extends IInterface {

    /* renamed from: com.google.android.gms.measurement.internal.q.a */
    public static abstract class C0494a extends Binder implements C0492q {

        /* renamed from: com.google.android.gms.measurement.internal.q.a.a */
        private static class C0493a implements C0492q {
            private IBinder f2364a;

            C0493a(IBinder iBinder) {
                this.f2364a = iBinder;
            }

            public void m4230a(AppMetadata appMetadata) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2364a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m4231a(EventParcel eventParcel, AppMetadata appMetadata) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (eventParcel != null) {
                        obtain.writeInt(1);
                        eventParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2364a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m4232a(EventParcel eventParcel, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (eventParcel != null) {
                        obtain.writeInt(1);
                        eventParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f2364a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m4233a(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (userAttributeParcel != null) {
                        obtain.writeInt(1);
                        userAttributeParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2364a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f2364a;
            }

            public void m4234b(AppMetadata appMetadata) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2364a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public C0494a() {
            attachInterface(this, "com.google.android.gms.measurement.internal.IMeasurementService");
        }

        public static C0492q m4235a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C0492q)) ? new C0493a(iBinder) : (C0492q) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            AppMetadata appMetadata = null;
            switch (i) {
                case R.View_android_focusable /*1*/:
                    parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    EventParcel a = parcel.readInt() != 0 ? EventParcel.CREATOR.m4214a(parcel) : null;
                    if (parcel.readInt() != 0) {
                        appMetadata = AppMetadata.CREATOR.m4113a(parcel);
                    }
                    m4226a(a, appMetadata);
                    parcel2.writeNoException();
                    return true;
                case R.View_paddingStart /*2*/:
                    parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    UserAttributeParcel a2 = parcel.readInt() != 0 ? UserAttributeParcel.CREATOR.m4078a(parcel) : null;
                    if (parcel.readInt() != 0) {
                        appMetadata = AppMetadata.CREATOR.m4113a(parcel);
                    }
                    m4228a(a2, appMetadata);
                    parcel2.writeNoException();
                    return true;
                case R.View_theme /*4*/:
                    parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (parcel.readInt() != 0) {
                        appMetadata = AppMetadata.CREATOR.m4113a(parcel);
                    }
                    m4225a(appMetadata);
                    parcel2.writeNoException();
                    return true;
                case R.Toolbar_contentInsetStart /*5*/:
                    EventParcel a3;
                    parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (parcel.readInt() != 0) {
                        a3 = EventParcel.CREATOR.m4214a(parcel);
                    }
                    m4227a(a3, parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case R.Toolbar_contentInsetEnd /*6*/:
                    parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (parcel.readInt() != 0) {
                        appMetadata = AppMetadata.CREATOR.m4113a(parcel);
                    }
                    m4229b(appMetadata);
                    parcel2.writeNoException();
                    return true;
                case 1598968902:
                    parcel2.writeString("com.google.android.gms.measurement.internal.IMeasurementService");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    void m4225a(AppMetadata appMetadata);

    void m4226a(EventParcel eventParcel, AppMetadata appMetadata);

    void m4227a(EventParcel eventParcel, String str, String str2);

    void m4228a(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata);

    void m4229b(AppMetadata appMetadata);
}
