package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.internal.d */
class C0477d {
    final String f2320a;
    final String f2321b;
    final long f2322c;
    final Object f2323d;

    C0477d(String str, String str2, long j, Object obj) {
        C0385p.m3551a(str);
        C0385p.m3551a(str2);
        C0385p.m3549a(obj);
        this.f2320a = str;
        this.f2321b = str2;
        this.f2322c = j;
        this.f2323d = obj;
    }
}
