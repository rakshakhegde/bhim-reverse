package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0428e;

class aa {
    protected final C0517y f2257g;

    aa(C0517y c0517y) {
        C0385p.m3549a((Object) c0517y);
        this.f2257g = c0517y;
    }

    public void m3947c() {
        this.f2257g.m4406w();
    }

    public void m3948d() {
        this.f2257g.m4390g().m4352d();
    }

    public void m3949e() {
        this.f2257g.m4390g().m4353e();
    }

    public C0495r m3950f() {
        return this.f2257g.m4401r();
    }

    public ae m3951g() {
        return this.f2257g.m4399p();
    }

    public C0428e m3952h() {
        return this.f2257g.m4398o();
    }

    public Context m3953i() {
        return this.f2257g.m4397n();
    }

    public C0479f m3954j() {
        return this.f2257g.m4394k();
    }

    public C0514x m3955k() {
        return this.f2257g.m4390g();
    }

    public C0499t m3956l() {
        return this.f2257g.m4389f();
    }

    public C0510w m3957m() {
        return this.f2257g.m4388e();
    }

    public C0481h m3958n() {
        return this.f2257g.m4387d();
    }
}
