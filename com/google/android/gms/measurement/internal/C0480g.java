package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.measurement.internal.g */
public class C0480g implements Creator<AppMetadata> {
    static void m4112a(AppMetadata appMetadata, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, appMetadata.f2221a);
        C0386a.m3568a(parcel, 2, appMetadata.f2222b, false);
        C0386a.m3568a(parcel, 3, appMetadata.f2223c, false);
        C0386a.m3568a(parcel, 4, appMetadata.f2224d, false);
        C0386a.m3568a(parcel, 5, appMetadata.f2225e, false);
        C0386a.m3562a(parcel, 6, appMetadata.f2226f);
        C0386a.m3562a(parcel, 7, appMetadata.f2227g);
        C0386a.m3568a(parcel, 8, appMetadata.f2228h, false);
        C0386a.m3569a(parcel, 9, appMetadata.f2229i);
        C0386a.m3560a(parcel, a);
    }

    public AppMetadata m4113a(Parcel parcel) {
        long j = 0;
        boolean z = false;
        String str = null;
        int b = zza.m3582b(parcel);
        long j2 = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    str5 = zza.m3590h(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    str4 = zza.m3590h(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    str3 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    str2 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    j2 = zza.m3587e(parcel, a);
                    break;
                case R.Toolbar_contentInsetLeft /*7*/:
                    j = zza.m3587e(parcel, a);
                    break;
                case R.Toolbar_contentInsetRight /*8*/:
                    str = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_popupTheme /*9*/:
                    z = zza.m3585c(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new AppMetadata(i, str5, str4, str3, str2, j2, j, str, z);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public AppMetadata[] m4114a(int i) {
        return new AppMetadata[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m4113a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m4114a(i);
    }
}
