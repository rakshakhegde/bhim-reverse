package com.google.android.gms.measurement.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.internal.j */
abstract class C0462j {
    private static volatile Handler f2284b;
    private final C0517y f2285a;
    private final Runnable f2286c;
    private volatile long f2287d;
    private boolean f2288e;

    /* renamed from: com.google.android.gms.measurement.internal.j.1 */
    class C04841 implements Runnable {
        final /* synthetic */ C0462j f2330a;

        C04841(C0462j c0462j) {
            this.f2330a = c0462j;
        }

        public void run() {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.f2330a.f2285a.m4390g().m4349a((Runnable) this);
                return;
            }
            boolean b = this.f2330a.m4014b();
            this.f2330a.f2287d = 0;
            if (b && this.f2330a.f2288e) {
                this.f2330a.m4012a();
            }
        }
    }

    C0462j(C0517y c0517y) {
        C0385p.m3549a((Object) c0517y);
        this.f2285a = c0517y;
        this.f2288e = true;
        this.f2286c = new C04841(this);
    }

    private Handler m4011d() {
        if (f2284b != null) {
            return f2284b;
        }
        Handler handler;
        synchronized (C0462j.class) {
            if (f2284b == null) {
                f2284b = new Handler(this.f2285a.m4397n().getMainLooper());
            }
            handler = f2284b;
        }
        return handler;
    }

    public abstract void m4012a();

    public void m4013a(long j) {
        m4015c();
        if (j >= 0) {
            this.f2287d = this.f2285a.m4398o().m3779a();
            if (!m4011d().postDelayed(this.f2286c, j)) {
                this.f2285a.m4389f().m4270b().m4259a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    public boolean m4014b() {
        return this.f2287d != 0;
    }

    public void m4015c() {
        this.f2287d = 0;
        m4011d().removeCallbacks(this.f2286c);
    }
}
