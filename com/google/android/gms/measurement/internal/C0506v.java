package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.measurement.internal.v */
class C0506v extends BroadcastReceiver {
    static final String f2400a;
    private final C0517y f2401b;
    private boolean f2402c;
    private boolean f2403d;

    /* renamed from: com.google.android.gms.measurement.internal.v.1 */
    class C05051 implements Runnable {
        final /* synthetic */ boolean f2398a;
        final /* synthetic */ C0506v f2399b;

        C05051(C0506v c0506v, boolean z) {
            this.f2399b = c0506v;
            this.f2398a = z;
        }

        public void run() {
            this.f2399b.f2401b.m4382a(this.f2398a);
        }
    }

    static {
        f2400a = C0506v.class.getName();
    }

    C0506v(C0517y c0517y) {
        C0385p.m3549a((Object) c0517y);
        this.f2401b = c0517y;
    }

    private Context m4310d() {
        return this.f2401b.m4397n();
    }

    private C0499t m4311e() {
        return this.f2401b.m4389f();
    }

    public void m4312a() {
        this.f2401b.m4376a();
        this.f2401b.m4404u();
        if (!this.f2402c) {
            m4310d().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.f2403d = this.f2401b.m4396m().m4296b();
            m4311e().m4288t().m4259a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.f2403d));
            this.f2402c = true;
        }
    }

    public void m4313b() {
        this.f2401b.m4376a();
        this.f2401b.m4404u();
        if (m4314c()) {
            m4311e().m4288t().m4258a("Unregistering connectivity change receiver");
            this.f2402c = false;
            this.f2403d = false;
            try {
                m4310d().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                m4311e().m4270b().m4259a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    public boolean m4314c() {
        this.f2401b.m4404u();
        return this.f2402c;
    }

    public void onReceive(Context context, Intent intent) {
        this.f2401b.m4376a();
        String action = intent.getAction();
        m4311e().m4288t().m4259a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean b = this.f2401b.m4396m().m4296b();
            if (this.f2403d != b) {
                this.f2403d = b;
                this.f2401b.m4390g().m4349a(new C05051(this, b));
                return;
            }
            return;
        }
        m4311e().m4283o().m4259a("NetworkBroadcastReceiver received unknown action", action);
    }
}
