package com.google.android.gms.internal;

/* renamed from: com.google.android.gms.internal.p */
public abstract class C0433p {
    protected volatile int f2116A;

    public C0433p() {
        this.f2116A = -1;
    }

    public void m3799a(zztd com_google_android_gms_internal_zztd) {
    }

    public abstract C0433p m3800b(C0446n c0446n);

    protected int m3801c() {
        return 0;
    }

    public /* synthetic */ Object clone() {
        return m3804f();
    }

    public int m3802d() {
        if (this.f2116A < 0) {
            m3803e();
        }
        return this.f2116A;
    }

    public int m3803e() {
        int c = m3801c();
        this.f2116A = c;
        return c;
    }

    public C0433p m3804f() {
        return (C0433p) super.clone();
    }

    public String toString() {
        return C0448q.m3862a(this);
    }
}
