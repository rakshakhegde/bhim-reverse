package com.google.android.gms.internal;

import android.os.Binder;

/* renamed from: com.google.android.gms.internal.b */
public abstract class C0420b<T> {
    private static final Object f2107c;
    private static C0425a f2108d;
    private static int f2109e;
    private static String f2110f;
    protected final String f2111a;
    protected final T f2112b;
    private T f2113g;

    /* renamed from: com.google.android.gms.internal.b.1 */
    static class C04211 extends C0420b<Boolean> {
        C04211(String str, Boolean bool) {
            super(str, bool);
        }

        protected /* synthetic */ Object m3763a(String str) {
            return m3764b(str);
        }

        protected Boolean m3764b(String str) {
            return C0420b.f2108d.m3771a(this.a, (Boolean) this.b);
        }
    }

    /* renamed from: com.google.android.gms.internal.b.2 */
    static class C04222 extends C0420b<Long> {
        C04222(String str, Long l) {
            super(str, l);
        }

        protected /* synthetic */ Object m3765a(String str) {
            return m3766b(str);
        }

        protected Long m3766b(String str) {
            return C0420b.f2108d.m3773a(this.a, (Long) this.b);
        }
    }

    /* renamed from: com.google.android.gms.internal.b.3 */
    static class C04233 extends C0420b<Integer> {
        C04233(String str, Integer num) {
            super(str, num);
        }

        protected /* synthetic */ Object m3767a(String str) {
            return m3768b(str);
        }

        protected Integer m3768b(String str) {
            return C0420b.f2108d.m3772a(this.a, (Integer) this.b);
        }
    }

    /* renamed from: com.google.android.gms.internal.b.4 */
    static class C04244 extends C0420b<String> {
        C04244(String str, String str2) {
            super(str, str2);
        }

        protected /* synthetic */ Object m3769a(String str) {
            return m3770b(str);
        }

        protected String m3770b(String str) {
            return C0420b.f2108d.m3774a(this.a, (String) this.b);
        }
    }

    /* renamed from: com.google.android.gms.internal.b.a */
    private interface C0425a {
        Boolean m3771a(String str, Boolean bool);

        Integer m3772a(String str, Integer num);

        Long m3773a(String str, Long l);

        String m3774a(String str, String str2);
    }

    static {
        f2107c = new Object();
        f2108d = null;
        f2109e = 0;
        f2110f = "com.google.android.providers.gsf.permission.READ_GSERVICES";
    }

    protected C0420b(String str, T t) {
        this.f2113g = null;
        this.f2111a = str;
        this.f2112b = t;
    }

    public static int m3753a() {
        return f2109e;
    }

    public static C0420b<Integer> m3754a(String str, Integer num) {
        return new C04233(str, num);
    }

    public static C0420b<Long> m3755a(String str, Long l) {
        return new C04222(str, l);
    }

    public static C0420b<String> m3756a(String str, String str2) {
        return new C04244(str, str2);
    }

    public static C0420b<Boolean> m3757a(String str, boolean z) {
        return new C04211(str, Boolean.valueOf(z));
    }

    public static boolean m3758b() {
        return f2108d != null;
    }

    protected abstract T m3760a(String str);

    public final T m3761c() {
        return this.f2113g != null ? this.f2113g : m3760a(this.f2111a);
    }

    public final T m3762d() {
        long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            T c = m3761c();
            return c;
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }
}
