package com.google.android.gms.internal;

import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.internal.n */
public final class C0446n {
    private final byte[] f2186a;
    private int f2187b;
    private int f2188c;
    private int f2189d;
    private int f2190e;
    private int f2191f;
    private int f2192g;
    private int f2193h;
    private int f2194i;
    private int f2195j;

    private C0446n(byte[] bArr, int i, int i2) {
        this.f2192g = Integer.MAX_VALUE;
        this.f2194i = 64;
        this.f2195j = 67108864;
        this.f2186a = bArr;
        this.f2187b = i;
        this.f2188c = i + i2;
        this.f2190e = i;
    }

    public static C0446n m3835a(byte[] bArr) {
        return C0446n.m3836a(bArr, 0, bArr.length);
    }

    public static C0446n m3836a(byte[] bArr, int i, int i2) {
        return new C0446n(bArr, i, i2);
    }

    private void m3837o() {
        this.f2188c += this.f2189d;
        int i = this.f2188c;
        if (i > this.f2192g) {
            this.f2189d = i - this.f2192g;
            this.f2188c -= this.f2189d;
            return;
        }
        this.f2189d = 0;
    }

    public int m3838a() {
        if (m3857l()) {
            this.f2191f = 0;
            return 0;
        }
        this.f2191f = m3853h();
        if (this.f2191f != 0) {
            return this.f2191f;
        }
        throw zztj.m3918d();
    }

    public void m3839a(int i) {
        if (this.f2191f != i) {
            throw zztj.m3919e();
        }
    }

    public void m3840a(C0433p c0433p) {
        int h = m3853h();
        if (this.f2193h >= this.f2194i) {
            throw zztj.m3921g();
        }
        h = m3844c(h);
        this.f2193h++;
        c0433p.m3800b(this);
        m3839a(0);
        this.f2193h--;
        m3846d(h);
    }

    public void m3841b() {
        int a;
        do {
            a = m3838a();
            if (a == 0) {
                return;
            }
        } while (m3842b(a));
    }

    public boolean m3842b(int i) {
        switch (C0449r.m3868a(i)) {
            case R.View_android_theme /*0*/:
                m3847e();
                return true;
            case R.View_android_focusable /*1*/:
                m3856k();
                return true;
            case R.View_paddingStart /*2*/:
                m3852g(m3853h());
                return true;
            case R.View_paddingEnd /*3*/:
                m3841b();
                m3839a(C0449r.m3869a(C0449r.m3871b(i), 4));
                return true;
            case R.View_theme /*4*/:
                return false;
            case R.Toolbar_contentInsetStart /*5*/:
                m3855j();
                return true;
            default:
                throw zztj.m3920f();
        }
    }

    public float m3843c() {
        return Float.intBitsToFloat(m3855j());
    }

    public int m3844c(int i) {
        if (i < 0) {
            throw zztj.m3916b();
        }
        int i2 = this.f2190e + i;
        int i3 = this.f2192g;
        if (i2 > i3) {
            throw zztj.m3915a();
        }
        this.f2192g = i2;
        m3837o();
        return i3;
    }

    public long m3845d() {
        return m3854i();
    }

    public void m3846d(int i) {
        this.f2192g = i;
        m3837o();
    }

    public int m3847e() {
        return m3853h();
    }

    public void m3848e(int i) {
        if (i > this.f2190e - this.f2187b) {
            throw new IllegalArgumentException("Position " + i + " is beyond current " + (this.f2190e - this.f2187b));
        } else if (i < 0) {
            throw new IllegalArgumentException("Bad position " + i);
        } else {
            this.f2190e = this.f2187b + i;
        }
    }

    public boolean m3849f() {
        return m3853h() != 0;
    }

    public byte[] m3850f(int i) {
        if (i < 0) {
            throw zztj.m3916b();
        } else if (this.f2190e + i > this.f2192g) {
            m3852g(this.f2192g - this.f2190e);
            throw zztj.m3915a();
        } else if (i <= this.f2188c - this.f2190e) {
            Object obj = new byte[i];
            System.arraycopy(this.f2186a, this.f2190e, obj, 0, i);
            this.f2190e += i;
            return obj;
        } else {
            throw zztj.m3915a();
        }
    }

    public String m3851g() {
        int h = m3853h();
        if (h > this.f2188c - this.f2190e || h <= 0) {
            return new String(m3850f(h), "UTF-8");
        }
        String str = new String(this.f2186a, this.f2190e, h, "UTF-8");
        this.f2190e = h + this.f2190e;
        return str;
    }

    public void m3852g(int i) {
        if (i < 0) {
            throw zztj.m3916b();
        } else if (this.f2190e + i > this.f2192g) {
            m3852g(this.f2192g - this.f2190e);
            throw zztj.m3915a();
        } else if (i <= this.f2188c - this.f2190e) {
            this.f2190e += i;
        } else {
            throw zztj.m3915a();
        }
    }

    public int m3853h() {
        byte n = m3859n();
        if (n >= null) {
            return n;
        }
        int i = n & 127;
        byte n2 = m3859n();
        if (n2 >= null) {
            return i | (n2 << 7);
        }
        i |= (n2 & 127) << 7;
        n2 = m3859n();
        if (n2 >= null) {
            return i | (n2 << 14);
        }
        i |= (n2 & 127) << 14;
        n2 = m3859n();
        if (n2 >= null) {
            return i | (n2 << 21);
        }
        i |= (n2 & 127) << 21;
        n2 = m3859n();
        i |= n2 << 28;
        if (n2 >= null) {
            return i;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (m3859n() >= null) {
                return i;
            }
        }
        throw zztj.m3917c();
    }

    public long m3854i() {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte n = m3859n();
            j |= ((long) (n & 127)) << i;
            if ((n & 128) == 0) {
                return j;
            }
        }
        throw zztj.m3917c();
    }

    public int m3855j() {
        return (((m3859n() & 255) | ((m3859n() & 255) << 8)) | ((m3859n() & 255) << 16)) | ((m3859n() & 255) << 24);
    }

    public long m3856k() {
        byte n = m3859n();
        byte n2 = m3859n();
        return ((((((((((long) n2) & 255) << 8) | (((long) n) & 255)) | ((((long) m3859n()) & 255) << 16)) | ((((long) m3859n()) & 255) << 24)) | ((((long) m3859n()) & 255) << 32)) | ((((long) m3859n()) & 255) << 40)) | ((((long) m3859n()) & 255) << 48)) | ((((long) m3859n()) & 255) << 56);
    }

    public boolean m3857l() {
        return this.f2190e == this.f2188c;
    }

    public int m3858m() {
        return this.f2190e - this.f2187b;
    }

    public byte m3859n() {
        if (this.f2190e == this.f2188c) {
            throw zztj.m3915a();
        }
        byte[] bArr = this.f2186a;
        int i = this.f2190e;
        this.f2190e = i + 1;
        return bArr[i];
    }
}
