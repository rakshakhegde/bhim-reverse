package com.google.android.gms.internal;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zztd {
    private final ByteBuffer f2205a;

    public static class zza extends IOException {
        zza(int i, int i2) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space (pos " + i + " limit " + i2 + ").");
        }
    }

    private zztd(ByteBuffer byteBuffer) {
        this.f2205a = byteBuffer;
        this.f2205a.order(ByteOrder.LITTLE_ENDIAN);
    }

    private zztd(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    private static int m3873a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) < '\u0080') {
            i++;
        }
        int i2 = i;
        i = length;
        while (i2 < length) {
            char charAt = charSequence.charAt(i2);
            if (charAt >= '\u0800') {
                i += m3874a(charSequence, i2);
                break;
            }
            i2++;
            i = ((127 - charAt) >>> 31) + i;
        }
        if (i >= length) {
            return i;
        }
        throw new IllegalArgumentException("UTF-8 length does not fit in int: " + (((long) i) + 4294967296L));
    }

    private static int m3874a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        int i2 = 0;
        int i3 = i;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            if (charAt < '\u0800') {
                i2 += (127 - charAt) >>> 31;
            } else {
                i2 += 2;
                if ('\ud800' <= charAt && charAt <= '\udfff') {
                    if (Character.codePointAt(charSequence, i3) < 65536) {
                        throw new IllegalArgumentException("Unpaired surrogate at index " + i3);
                    }
                    i3++;
                }
            }
            i3++;
        }
        return i2;
    }

    private static int m3875a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int length = charSequence.length();
        int i3 = 0;
        int i4 = i + i2;
        while (i3 < length && i3 + i < i4) {
            char charAt = charSequence.charAt(i3);
            if (charAt >= '\u0080') {
                break;
            }
            bArr[i + i3] = (byte) charAt;
            i3++;
        }
        if (i3 == length) {
            return i + length;
        }
        int i5 = i + i3;
        while (i3 < length) {
            int i6;
            char charAt2 = charSequence.charAt(i3);
            if (charAt2 < '\u0080' && i5 < i4) {
                i6 = i5 + 1;
                bArr[i5] = (byte) charAt2;
            } else if (charAt2 < '\u0800' && i5 <= i4 - 2) {
                r6 = i5 + 1;
                bArr[i5] = (byte) ((charAt2 >>> 6) | 960);
                i6 = r6 + 1;
                bArr[r6] = (byte) ((charAt2 & 63) | 128);
            } else if ((charAt2 < '\ud800' || '\udfff' < charAt2) && i5 <= i4 - 3) {
                i6 = i5 + 1;
                bArr[i5] = (byte) ((charAt2 >>> 12) | 480);
                i5 = i6 + 1;
                bArr[i6] = (byte) (((charAt2 >>> 6) & 63) | 128);
                i6 = i5 + 1;
                bArr[i5] = (byte) ((charAt2 & 63) | 128);
            } else if (i5 <= i4 - 4) {
                if (i3 + 1 != charSequence.length()) {
                    i3++;
                    charAt = charSequence.charAt(i3);
                    if (Character.isSurrogatePair(charAt2, charAt)) {
                        int toCodePoint = Character.toCodePoint(charAt2, charAt);
                        i6 = i5 + 1;
                        bArr[i5] = (byte) ((toCodePoint >>> 18) | 240);
                        i5 = i6 + 1;
                        bArr[i6] = (byte) (((toCodePoint >>> 12) & 63) | 128);
                        r6 = i5 + 1;
                        bArr[i5] = (byte) (((toCodePoint >>> 6) & 63) | 128);
                        i6 = r6 + 1;
                        bArr[r6] = (byte) ((toCodePoint & 63) | 128);
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i3 - 1));
            } else if ('\ud800' > charAt2 || charAt2 > '\udfff' || (i3 + 1 != charSequence.length() && Character.isSurrogatePair(charAt2, charSequence.charAt(i3 + 1)))) {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + i5);
            } else {
                throw new IllegalArgumentException("Unpaired surrogate at index " + i3);
            }
            i3++;
            i5 = i6;
        }
        return i5;
    }

    public static zztd m3876a(byte[] bArr) {
        return m3877a(bArr, 0, bArr.length);
    }

    public static zztd m3877a(byte[] bArr, int i, int i2) {
        return new zztd(bArr, i, i2);
    }

    private static void m3878a(CharSequence charSequence, ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        } else if (byteBuffer.hasArray()) {
            try {
                byteBuffer.position(m3875a(charSequence, byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining()) - byteBuffer.arrayOffset());
            } catch (Throwable e) {
                BufferOverflowException bufferOverflowException = new BufferOverflowException();
                bufferOverflowException.initCause(e);
                throw bufferOverflowException;
            }
        } else {
            m3891b(charSequence, byteBuffer);
        }
    }

    public static int m3879b(float f) {
        return 4;
    }

    public static int m3880b(int i) {
        return i >= 0 ? m3894f(i) : 10;
    }

    public static int m3881b(int i, float f) {
        return m3892d(i) + m3879b(f);
    }

    public static int m3882b(int i, int i2) {
        return m3892d(i) + m3880b(i2);
    }

    public static int m3883b(int i, long j) {
        return m3892d(i) + m3887b(j);
    }

    public static int m3884b(int i, C0433p c0433p) {
        return m3892d(i) + m3888b(c0433p);
    }

    public static int m3885b(int i, String str) {
        return m3892d(i) + m3889b(str);
    }

    public static int m3886b(int i, boolean z) {
        return m3892d(i) + m3890b(z);
    }

    public static int m3887b(long j) {
        return m3893d(j);
    }

    public static int m3888b(C0433p c0433p) {
        int e = c0433p.m3803e();
        return e + m3894f(e);
    }

    public static int m3889b(String str) {
        int a = m3873a((CharSequence) str);
        return a + m3894f(a);
    }

    public static int m3890b(boolean z) {
        return 1;
    }

    private static void m3891b(CharSequence charSequence, ByteBuffer byteBuffer) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            char charAt = charSequence.charAt(i);
            if (charAt < '\u0080') {
                byteBuffer.put((byte) charAt);
            } else if (charAt < '\u0800') {
                byteBuffer.put((byte) ((charAt >>> 6) | 960));
                byteBuffer.put((byte) ((charAt & 63) | 128));
            } else if (charAt < '\ud800' || '\udfff' < charAt) {
                byteBuffer.put((byte) ((charAt >>> 12) | 480));
                byteBuffer.put((byte) (((charAt >>> 6) & 63) | 128));
                byteBuffer.put((byte) ((charAt & 63) | 128));
            } else {
                if (i + 1 != charSequence.length()) {
                    i++;
                    char charAt2 = charSequence.charAt(i);
                    if (Character.isSurrogatePair(charAt, charAt2)) {
                        int toCodePoint = Character.toCodePoint(charAt, charAt2);
                        byteBuffer.put((byte) ((toCodePoint >>> 18) | 240));
                        byteBuffer.put((byte) (((toCodePoint >>> 12) & 63) | 128));
                        byteBuffer.put((byte) (((toCodePoint >>> 6) & 63) | 128));
                        byteBuffer.put((byte) ((toCodePoint & 63) | 128));
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i - 1));
            }
            i++;
        }
    }

    public static int m3892d(int i) {
        return m3894f(C0449r.m3869a(i, 0));
    }

    public static int m3893d(long j) {
        return (-128 & j) == 0 ? 1 : (-16384 & j) == 0 ? 2 : (-2097152 & j) == 0 ? 3 : (-268435456 & j) == 0 ? 4 : (-34359738368L & j) == 0 ? 5 : (-4398046511104L & j) == 0 ? 6 : (-562949953421312L & j) == 0 ? 7 : (-72057594037927936L & j) == 0 ? 8 : (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    public static int m3894f(int i) {
        return (i & -128) == 0 ? 1 : (i & -16384) == 0 ? 2 : (-2097152 & i) == 0 ? 3 : (-268435456 & i) == 0 ? 4 : 5;
    }

    public int m3895a() {
        return this.f2205a.remaining();
    }

    public void m3896a(byte b) {
        if (this.f2205a.hasRemaining()) {
            this.f2205a.put(b);
            return;
        }
        throw new zza(this.f2205a.position(), this.f2205a.limit());
    }

    public void m3897a(float f) {
        m3914g(Float.floatToIntBits(f));
    }

    public void m3898a(int i) {
        if (i >= 0) {
            m3913e(i);
        } else {
            m3912c((long) i);
        }
    }

    public void m3899a(int i, float f) {
        m3911c(i, 5);
        m3897a(f);
    }

    public void m3900a(int i, int i2) {
        m3911c(i, 0);
        m3898a(i2);
    }

    public void m3901a(int i, long j) {
        m3911c(i, 0);
        m3905a(j);
    }

    public void m3902a(int i, C0433p c0433p) {
        m3911c(i, 2);
        m3906a(c0433p);
    }

    public void m3903a(int i, String str) {
        m3911c(i, 2);
        m3907a(str);
    }

    public void m3904a(int i, boolean z) {
        m3911c(i, 0);
        m3908a(z);
    }

    public void m3905a(long j) {
        m3912c(j);
    }

    public void m3906a(C0433p c0433p) {
        m3913e(c0433p.m3802d());
        c0433p.m3799a(this);
    }

    public void m3907a(String str) {
        try {
            int f = m3894f(str.length());
            if (f == m3894f(str.length() * 3)) {
                int position = this.f2205a.position();
                if (this.f2205a.remaining() < f) {
                    throw new zza(f + position, this.f2205a.limit());
                }
                this.f2205a.position(position + f);
                m3878a((CharSequence) str, this.f2205a);
                int position2 = this.f2205a.position();
                this.f2205a.position(position);
                m3913e((position2 - position) - f);
                this.f2205a.position(position2);
                return;
            }
            m3913e(m3873a((CharSequence) str));
            m3878a((CharSequence) str, this.f2205a);
        } catch (Throwable e) {
            zza com_google_android_gms_internal_zztd_zza = new zza(this.f2205a.position(), this.f2205a.limit());
            com_google_android_gms_internal_zztd_zza.initCause(e);
            throw com_google_android_gms_internal_zztd_zza;
        }
    }

    public void m3908a(boolean z) {
        m3910c(z ? 1 : 0);
    }

    public void m3909b() {
        if (m3895a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public void m3910c(int i) {
        m3896a((byte) i);
    }

    public void m3911c(int i, int i2) {
        m3913e(C0449r.m3869a(i, i2));
    }

    public void m3912c(long j) {
        while ((-128 & j) != 0) {
            m3910c((((int) j) & 127) | 128);
            j >>>= 7;
        }
        m3910c((int) j);
    }

    public void m3913e(int i) {
        while ((i & -128) != 0) {
            m3910c((i & 127) | 128);
            i >>>= 7;
        }
        m3910c(i);
    }

    public void m3914g(int i) {
        if (this.f2205a.remaining() < 4) {
            throw new zza(this.f2205a.position(), this.f2205a.limit());
        }
        this.f2205a.putInt(i);
    }
}
