package com.google.android.gms.internal;

import com.google.android.gms.common.api.C0090a;
import com.google.android.gms.common.api.C0090a.C0086a;
import com.google.android.gms.common.api.C0090a.C0087b;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.p024a.p025a.C0082a;

/* renamed from: com.google.android.gms.internal.k */
public final class C0442k {
    public static final C0087b<Object> f2162a;
    public static final C0087b<Object> f2163b;
    public static final C0086a<Object, C0445m> f2164c;
    static final C0086a<Object, Object> f2165d;
    public static final Scope f2166e;
    public static final Scope f2167f;
    public static final C0090a<C0445m> f2168g;
    public static final C0090a<Object> f2169h;
    public static final C0081l f2170i;

    /* renamed from: com.google.android.gms.internal.k.1 */
    static class C04401 extends C0086a<Object, C0445m> {
        C04401() {
        }
    }

    /* renamed from: com.google.android.gms.internal.k.2 */
    static class C04412 extends C0086a<Object, Object> {
        C04412() {
        }
    }

    static {
        f2162a = new C0087b();
        f2163b = new C0087b();
        f2164c = new C04401();
        f2165d = new C04412();
        f2166e = new Scope("profile");
        f2167f = new Scope("email");
        f2168g = new C0090a("SignIn.API", f2164c, f2162a);
        f2169h = new C0090a("SignIn.INTERNAL_API", f2165d, f2163b);
        f2170i = new C0082a();
    }
}
