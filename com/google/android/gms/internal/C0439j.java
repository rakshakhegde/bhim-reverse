package com.google.android.gms.internal;

import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.internal.j */
public interface C0439j {

    /* renamed from: com.google.android.gms.internal.j.a */
    public static final class C0434a extends C0433p {
        private static volatile C0434a[] f2117f;
        public C0435b[] f2118a;
        public String f2119b;
        public Long f2120c;
        public Long f2121d;
        public Integer f2122e;

        public C0434a() {
            m3808b();
        }

        public static C0434a[] m3805a() {
            if (f2117f == null) {
                synchronized (C0447o.f2196a) {
                    if (f2117f == null) {
                        f2117f = new C0434a[0];
                    }
                }
            }
            return f2117f;
        }

        public C0434a m3806a(C0446n c0446n) {
            while (true) {
                int a = c0446n.m3838a();
                switch (a) {
                    case R.View_android_theme /*0*/:
                        break;
                    case R.Toolbar_titleTextAppearance /*10*/:
                        int b = C0449r.m3872b(c0446n, 10);
                        a = this.f2118a == null ? 0 : this.f2118a.length;
                        Object obj = new C0435b[(b + a)];
                        if (a != 0) {
                            System.arraycopy(this.f2118a, 0, obj, 0, a);
                        }
                        while (a < obj.length - 1) {
                            obj[a] = new C0435b();
                            c0446n.m3840a(obj[a]);
                            c0446n.m3838a();
                            a++;
                        }
                        obj[a] = new C0435b();
                        c0446n.m3840a(obj[a]);
                        this.f2118a = obj;
                        continue;
                    case R.Toolbar_collapseIcon /*18*/:
                        this.f2119b = c0446n.m3851g();
                        continue;
                    case R.Toolbar_subtitleTextColor /*24*/:
                        this.f2120c = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_actionModeCutDrawable /*32*/:
                        this.f2121d = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_textAppearanceLargePopupMenu /*40*/:
                        this.f2122e = Integer.valueOf(c0446n.m3847e());
                        continue;
                    default:
                        if (!C0449r.m3870a(c0446n, a)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public void m3807a(zztd com_google_android_gms_internal_zztd) {
            if (this.f2118a != null && this.f2118a.length > 0) {
                for (C0433p c0433p : this.f2118a) {
                    if (c0433p != null) {
                        com_google_android_gms_internal_zztd.m3902a(1, c0433p);
                    }
                }
            }
            if (this.f2119b != null) {
                com_google_android_gms_internal_zztd.m3903a(2, this.f2119b);
            }
            if (this.f2120c != null) {
                com_google_android_gms_internal_zztd.m3901a(3, this.f2120c.longValue());
            }
            if (this.f2121d != null) {
                com_google_android_gms_internal_zztd.m3901a(4, this.f2121d.longValue());
            }
            if (this.f2122e != null) {
                com_google_android_gms_internal_zztd.m3900a(5, this.f2122e.intValue());
            }
            super.m3799a(com_google_android_gms_internal_zztd);
        }

        public C0434a m3808b() {
            this.f2118a = C0435b.m3811a();
            this.f2119b = null;
            this.f2120c = null;
            this.f2121d = null;
            this.f2122e = null;
            this.A = -1;
            return this;
        }

        public /* synthetic */ C0433p m3809b(C0446n c0446n) {
            return m3806a(c0446n);
        }

        protected int m3810c() {
            int c = super.m3801c();
            if (this.f2118a != null && this.f2118a.length > 0) {
                for (C0433p c0433p : this.f2118a) {
                    if (c0433p != null) {
                        c += zztd.m3884b(1, c0433p);
                    }
                }
            }
            if (this.f2119b != null) {
                c += zztd.m3885b(2, this.f2119b);
            }
            if (this.f2120c != null) {
                c += zztd.m3883b(3, this.f2120c.longValue());
            }
            if (this.f2121d != null) {
                c += zztd.m3883b(4, this.f2121d.longValue());
            }
            return this.f2122e != null ? c + zztd.m3882b(5, this.f2122e.intValue()) : c;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0434a)) {
                return false;
            }
            C0434a c0434a = (C0434a) obj;
            if (!C0447o.m3861a(this.f2118a, c0434a.f2118a)) {
                return false;
            }
            if (this.f2119b == null) {
                if (c0434a.f2119b != null) {
                    return false;
                }
            } else if (!this.f2119b.equals(c0434a.f2119b)) {
                return false;
            }
            if (this.f2120c == null) {
                if (c0434a.f2120c != null) {
                    return false;
                }
            } else if (!this.f2120c.equals(c0434a.f2120c)) {
                return false;
            }
            if (this.f2121d == null) {
                if (c0434a.f2121d != null) {
                    return false;
                }
            } else if (!this.f2121d.equals(c0434a.f2121d)) {
                return false;
            }
            return this.f2122e == null ? c0434a.f2122e == null : this.f2122e.equals(c0434a.f2122e);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.f2121d == null ? 0 : this.f2121d.hashCode()) + (((this.f2120c == null ? 0 : this.f2120c.hashCode()) + (((this.f2119b == null ? 0 : this.f2119b.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + C0447o.m3860a(this.f2118a)) * 31)) * 31)) * 31)) * 31;
            if (this.f2122e != null) {
                i = this.f2122e.hashCode();
            }
            return hashCode + i;
        }
    }

    /* renamed from: com.google.android.gms.internal.j.b */
    public static final class C0435b extends C0433p {
        private static volatile C0435b[] f2123e;
        public String f2124a;
        public String f2125b;
        public Long f2126c;
        public Float f2127d;

        public C0435b() {
            m3814b();
        }

        public static C0435b[] m3811a() {
            if (f2123e == null) {
                synchronized (C0447o.f2196a) {
                    if (f2123e == null) {
                        f2123e = new C0435b[0];
                    }
                }
            }
            return f2123e;
        }

        public C0435b m3812a(C0446n c0446n) {
            while (true) {
                int a = c0446n.m3838a();
                switch (a) {
                    case R.View_android_theme /*0*/:
                        break;
                    case R.Toolbar_titleTextAppearance /*10*/:
                        this.f2124a = c0446n.m3851g();
                        continue;
                    case R.Toolbar_collapseIcon /*18*/:
                        this.f2125b = c0446n.m3851g();
                        continue;
                    case R.Toolbar_subtitleTextColor /*24*/:
                        this.f2126c = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_actionModeFindDrawable /*37*/:
                        this.f2127d = Float.valueOf(c0446n.m3843c());
                        continue;
                    default:
                        if (!C0449r.m3870a(c0446n, a)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public void m3813a(zztd com_google_android_gms_internal_zztd) {
            if (this.f2124a != null) {
                com_google_android_gms_internal_zztd.m3903a(1, this.f2124a);
            }
            if (this.f2125b != null) {
                com_google_android_gms_internal_zztd.m3903a(2, this.f2125b);
            }
            if (this.f2126c != null) {
                com_google_android_gms_internal_zztd.m3901a(3, this.f2126c.longValue());
            }
            if (this.f2127d != null) {
                com_google_android_gms_internal_zztd.m3899a(4, this.f2127d.floatValue());
            }
            super.m3799a(com_google_android_gms_internal_zztd);
        }

        public C0435b m3814b() {
            this.f2124a = null;
            this.f2125b = null;
            this.f2126c = null;
            this.f2127d = null;
            this.A = -1;
            return this;
        }

        public /* synthetic */ C0433p m3815b(C0446n c0446n) {
            return m3812a(c0446n);
        }

        protected int m3816c() {
            int c = super.m3801c();
            if (this.f2124a != null) {
                c += zztd.m3885b(1, this.f2124a);
            }
            if (this.f2125b != null) {
                c += zztd.m3885b(2, this.f2125b);
            }
            if (this.f2126c != null) {
                c += zztd.m3883b(3, this.f2126c.longValue());
            }
            return this.f2127d != null ? c + zztd.m3881b(4, this.f2127d.floatValue()) : c;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0435b)) {
                return false;
            }
            C0435b c0435b = (C0435b) obj;
            if (this.f2124a == null) {
                if (c0435b.f2124a != null) {
                    return false;
                }
            } else if (!this.f2124a.equals(c0435b.f2124a)) {
                return false;
            }
            if (this.f2125b == null) {
                if (c0435b.f2125b != null) {
                    return false;
                }
            } else if (!this.f2125b.equals(c0435b.f2125b)) {
                return false;
            }
            if (this.f2126c == null) {
                if (c0435b.f2126c != null) {
                    return false;
                }
            } else if (!this.f2126c.equals(c0435b.f2126c)) {
                return false;
            }
            return this.f2127d == null ? c0435b.f2127d == null : this.f2127d.equals(c0435b.f2127d);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.f2126c == null ? 0 : this.f2126c.hashCode()) + (((this.f2125b == null ? 0 : this.f2125b.hashCode()) + (((this.f2124a == null ? 0 : this.f2124a.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31;
            if (this.f2127d != null) {
                i = this.f2127d.hashCode();
            }
            return hashCode + i;
        }
    }

    /* renamed from: com.google.android.gms.internal.j.c */
    public static final class C0436c extends C0433p {
        public C0437d[] f2128a;

        public C0436c() {
            m3817a();
        }

        public C0436c m3817a() {
            this.f2128a = C0437d.m3822a();
            this.A = -1;
            return this;
        }

        public C0436c m3818a(C0446n c0446n) {
            while (true) {
                int a = c0446n.m3838a();
                switch (a) {
                    case R.View_android_theme /*0*/:
                        break;
                    case R.Toolbar_titleTextAppearance /*10*/:
                        int b = C0449r.m3872b(c0446n, 10);
                        a = this.f2128a == null ? 0 : this.f2128a.length;
                        Object obj = new C0437d[(b + a)];
                        if (a != 0) {
                            System.arraycopy(this.f2128a, 0, obj, 0, a);
                        }
                        while (a < obj.length - 1) {
                            obj[a] = new C0437d();
                            c0446n.m3840a(obj[a]);
                            c0446n.m3838a();
                            a++;
                        }
                        obj[a] = new C0437d();
                        c0446n.m3840a(obj[a]);
                        this.f2128a = obj;
                        continue;
                    default:
                        if (!C0449r.m3870a(c0446n, a)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public void m3819a(zztd com_google_android_gms_internal_zztd) {
            if (this.f2128a != null && this.f2128a.length > 0) {
                for (C0433p c0433p : this.f2128a) {
                    if (c0433p != null) {
                        com_google_android_gms_internal_zztd.m3902a(1, c0433p);
                    }
                }
            }
            super.m3799a(com_google_android_gms_internal_zztd);
        }

        public /* synthetic */ C0433p m3820b(C0446n c0446n) {
            return m3818a(c0446n);
        }

        protected int m3821c() {
            int c = super.m3801c();
            if (this.f2128a != null && this.f2128a.length > 0) {
                for (C0433p c0433p : this.f2128a) {
                    if (c0433p != null) {
                        c += zztd.m3884b(1, c0433p);
                    }
                }
            }
            return c;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0436c)) {
                return false;
            }
            return C0447o.m3861a(this.f2128a, ((C0436c) obj).f2128a);
        }

        public int hashCode() {
            return ((getClass().getName().hashCode() + 527) * 31) + C0447o.m3860a(this.f2128a);
        }
    }

    /* renamed from: com.google.android.gms.internal.j.d */
    public static final class C0437d extends C0433p {
        private static volatile C0437d[] f2129B;
        public Integer f2130a;
        public C0434a[] f2131b;
        public C0438e[] f2132c;
        public Long f2133d;
        public Long f2134e;
        public Long f2135f;
        public Long f2136g;
        public Long f2137h;
        public String f2138i;
        public String f2139j;
        public String f2140k;
        public String f2141l;
        public Integer f2142m;
        public String f2143n;
        public String f2144o;
        public String f2145p;
        public Long f2146q;
        public Long f2147r;
        public String f2148s;
        public Boolean f2149t;
        public String f2150u;
        public Long f2151v;
        public Integer f2152w;
        public String f2153x;
        public String f2154y;
        public Boolean f2155z;

        public C0437d() {
            m3825b();
        }

        public static C0437d[] m3822a() {
            if (f2129B == null) {
                synchronized (C0447o.f2196a) {
                    if (f2129B == null) {
                        f2129B = new C0437d[0];
                    }
                }
            }
            return f2129B;
        }

        public C0437d m3823a(C0446n c0446n) {
            while (true) {
                int a = c0446n.m3838a();
                int b;
                Object obj;
                switch (a) {
                    case R.View_android_theme /*0*/:
                        break;
                    case R.Toolbar_contentInsetRight /*8*/:
                        this.f2130a = Integer.valueOf(c0446n.m3847e());
                        continue;
                    case R.Toolbar_collapseIcon /*18*/:
                        b = C0449r.m3872b(c0446n, 18);
                        a = this.f2131b == null ? 0 : this.f2131b.length;
                        obj = new C0434a[(b + a)];
                        if (a != 0) {
                            System.arraycopy(this.f2131b, 0, obj, 0, a);
                        }
                        while (a < obj.length - 1) {
                            obj[a] = new C0434a();
                            c0446n.m3840a(obj[a]);
                            c0446n.m3838a();
                            a++;
                        }
                        obj[a] = new C0434a();
                        c0446n.m3840a(obj[a]);
                        this.f2131b = obj;
                        continue;
                    case R.AppCompatTheme_actionMenuTextColor /*26*/:
                        b = C0449r.m3872b(c0446n, 26);
                        a = this.f2132c == null ? 0 : this.f2132c.length;
                        obj = new C0438e[(b + a)];
                        if (a != 0) {
                            System.arraycopy(this.f2132c, 0, obj, 0, a);
                        }
                        while (a < obj.length - 1) {
                            obj[a] = new C0438e();
                            c0446n.m3840a(obj[a]);
                            c0446n.m3838a();
                            a++;
                        }
                        obj[a] = new C0438e();
                        c0446n.m3840a(obj[a]);
                        this.f2132c = obj;
                        continue;
                    case R.AppCompatTheme_actionModeCutDrawable /*32*/:
                        this.f2133d = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_textAppearanceLargePopupMenu /*40*/:
                        this.f2134e = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_homeAsUpIndicator /*48*/:
                        this.f2135f = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_dividerHorizontal /*56*/:
                        this.f2137h = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_textAppearanceSearchResultSubtitle /*66*/:
                        this.f2138i = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_dropDownListViewStyle /*74*/:
                        this.f2139j = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_colorPrimary /*82*/:
                        this.f2140k = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_controlBackground /*90*/:
                        this.f2141l = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_buttonBarPositiveButtonStyle /*96*/:
                        this.f2142m = Integer.valueOf(c0446n.m3847e());
                        continue;
                    case R.AppCompatTheme_ratingBarStyle /*106*/:
                        this.f2143n = c0446n.m3851g();
                        continue;
                    case 114:
                        this.f2144o = c0446n.m3851g();
                        continue;
                    case 130:
                        this.f2145p = c0446n.m3851g();
                        continue;
                    case 136:
                        this.f2146q = Long.valueOf(c0446n.m3845d());
                        continue;
                    case 144:
                        this.f2147r = Long.valueOf(c0446n.m3845d());
                        continue;
                    case 154:
                        this.f2148s = c0446n.m3851g();
                        continue;
                    case 160:
                        this.f2149t = Boolean.valueOf(c0446n.m3849f());
                        continue;
                    case 170:
                        this.f2150u = c0446n.m3851g();
                        continue;
                    case 176:
                        this.f2151v = Long.valueOf(c0446n.m3845d());
                        continue;
                    case 184:
                        this.f2152w = Integer.valueOf(c0446n.m3847e());
                        continue;
                    case 194:
                        this.f2153x = c0446n.m3851g();
                        continue;
                    case 202:
                        this.f2154y = c0446n.m3851g();
                        continue;
                    case 208:
                        this.f2136g = Long.valueOf(c0446n.m3845d());
                        continue;
                    case 224:
                        this.f2155z = Boolean.valueOf(c0446n.m3849f());
                        continue;
                    default:
                        if (!C0449r.m3870a(c0446n, a)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public void m3824a(zztd com_google_android_gms_internal_zztd) {
            int i = 0;
            if (this.f2130a != null) {
                com_google_android_gms_internal_zztd.m3900a(1, this.f2130a.intValue());
            }
            if (this.f2131b != null && this.f2131b.length > 0) {
                for (C0433p c0433p : this.f2131b) {
                    if (c0433p != null) {
                        com_google_android_gms_internal_zztd.m3902a(2, c0433p);
                    }
                }
            }
            if (this.f2132c != null && this.f2132c.length > 0) {
                while (i < this.f2132c.length) {
                    C0433p c0433p2 = this.f2132c[i];
                    if (c0433p2 != null) {
                        com_google_android_gms_internal_zztd.m3902a(3, c0433p2);
                    }
                    i++;
                }
            }
            if (this.f2133d != null) {
                com_google_android_gms_internal_zztd.m3901a(4, this.f2133d.longValue());
            }
            if (this.f2134e != null) {
                com_google_android_gms_internal_zztd.m3901a(5, this.f2134e.longValue());
            }
            if (this.f2135f != null) {
                com_google_android_gms_internal_zztd.m3901a(6, this.f2135f.longValue());
            }
            if (this.f2137h != null) {
                com_google_android_gms_internal_zztd.m3901a(7, this.f2137h.longValue());
            }
            if (this.f2138i != null) {
                com_google_android_gms_internal_zztd.m3903a(8, this.f2138i);
            }
            if (this.f2139j != null) {
                com_google_android_gms_internal_zztd.m3903a(9, this.f2139j);
            }
            if (this.f2140k != null) {
                com_google_android_gms_internal_zztd.m3903a(10, this.f2140k);
            }
            if (this.f2141l != null) {
                com_google_android_gms_internal_zztd.m3903a(11, this.f2141l);
            }
            if (this.f2142m != null) {
                com_google_android_gms_internal_zztd.m3900a(12, this.f2142m.intValue());
            }
            if (this.f2143n != null) {
                com_google_android_gms_internal_zztd.m3903a(13, this.f2143n);
            }
            if (this.f2144o != null) {
                com_google_android_gms_internal_zztd.m3903a(14, this.f2144o);
            }
            if (this.f2145p != null) {
                com_google_android_gms_internal_zztd.m3903a(16, this.f2145p);
            }
            if (this.f2146q != null) {
                com_google_android_gms_internal_zztd.m3901a(17, this.f2146q.longValue());
            }
            if (this.f2147r != null) {
                com_google_android_gms_internal_zztd.m3901a(18, this.f2147r.longValue());
            }
            if (this.f2148s != null) {
                com_google_android_gms_internal_zztd.m3903a(19, this.f2148s);
            }
            if (this.f2149t != null) {
                com_google_android_gms_internal_zztd.m3904a(20, this.f2149t.booleanValue());
            }
            if (this.f2150u != null) {
                com_google_android_gms_internal_zztd.m3903a(21, this.f2150u);
            }
            if (this.f2151v != null) {
                com_google_android_gms_internal_zztd.m3901a(22, this.f2151v.longValue());
            }
            if (this.f2152w != null) {
                com_google_android_gms_internal_zztd.m3900a(23, this.f2152w.intValue());
            }
            if (this.f2153x != null) {
                com_google_android_gms_internal_zztd.m3903a(24, this.f2153x);
            }
            if (this.f2154y != null) {
                com_google_android_gms_internal_zztd.m3903a(25, this.f2154y);
            }
            if (this.f2136g != null) {
                com_google_android_gms_internal_zztd.m3901a(26, this.f2136g.longValue());
            }
            if (this.f2155z != null) {
                com_google_android_gms_internal_zztd.m3904a(28, this.f2155z.booleanValue());
            }
            super.m3799a(com_google_android_gms_internal_zztd);
        }

        public C0437d m3825b() {
            this.f2130a = null;
            this.f2131b = C0434a.m3805a();
            this.f2132c = C0438e.m3828a();
            this.f2133d = null;
            this.f2134e = null;
            this.f2135f = null;
            this.f2136g = null;
            this.f2137h = null;
            this.f2138i = null;
            this.f2139j = null;
            this.f2140k = null;
            this.f2141l = null;
            this.f2142m = null;
            this.f2143n = null;
            this.f2144o = null;
            this.f2145p = null;
            this.f2146q = null;
            this.f2147r = null;
            this.f2148s = null;
            this.f2149t = null;
            this.f2150u = null;
            this.f2151v = null;
            this.f2152w = null;
            this.f2153x = null;
            this.f2154y = null;
            this.f2155z = null;
            this.A = -1;
            return this;
        }

        public /* synthetic */ C0433p m3826b(C0446n c0446n) {
            return m3823a(c0446n);
        }

        protected int m3827c() {
            int i = 0;
            int c = super.m3801c();
            if (this.f2130a != null) {
                c += zztd.m3882b(1, this.f2130a.intValue());
            }
            if (this.f2131b != null && this.f2131b.length > 0) {
                int i2 = c;
                for (C0433p c0433p : this.f2131b) {
                    if (c0433p != null) {
                        i2 += zztd.m3884b(2, c0433p);
                    }
                }
                c = i2;
            }
            if (this.f2132c != null && this.f2132c.length > 0) {
                while (i < this.f2132c.length) {
                    C0433p c0433p2 = this.f2132c[i];
                    if (c0433p2 != null) {
                        c += zztd.m3884b(3, c0433p2);
                    }
                    i++;
                }
            }
            if (this.f2133d != null) {
                c += zztd.m3883b(4, this.f2133d.longValue());
            }
            if (this.f2134e != null) {
                c += zztd.m3883b(5, this.f2134e.longValue());
            }
            if (this.f2135f != null) {
                c += zztd.m3883b(6, this.f2135f.longValue());
            }
            if (this.f2137h != null) {
                c += zztd.m3883b(7, this.f2137h.longValue());
            }
            if (this.f2138i != null) {
                c += zztd.m3885b(8, this.f2138i);
            }
            if (this.f2139j != null) {
                c += zztd.m3885b(9, this.f2139j);
            }
            if (this.f2140k != null) {
                c += zztd.m3885b(10, this.f2140k);
            }
            if (this.f2141l != null) {
                c += zztd.m3885b(11, this.f2141l);
            }
            if (this.f2142m != null) {
                c += zztd.m3882b(12, this.f2142m.intValue());
            }
            if (this.f2143n != null) {
                c += zztd.m3885b(13, this.f2143n);
            }
            if (this.f2144o != null) {
                c += zztd.m3885b(14, this.f2144o);
            }
            if (this.f2145p != null) {
                c += zztd.m3885b(16, this.f2145p);
            }
            if (this.f2146q != null) {
                c += zztd.m3883b(17, this.f2146q.longValue());
            }
            if (this.f2147r != null) {
                c += zztd.m3883b(18, this.f2147r.longValue());
            }
            if (this.f2148s != null) {
                c += zztd.m3885b(19, this.f2148s);
            }
            if (this.f2149t != null) {
                c += zztd.m3886b(20, this.f2149t.booleanValue());
            }
            if (this.f2150u != null) {
                c += zztd.m3885b(21, this.f2150u);
            }
            if (this.f2151v != null) {
                c += zztd.m3883b(22, this.f2151v.longValue());
            }
            if (this.f2152w != null) {
                c += zztd.m3882b(23, this.f2152w.intValue());
            }
            if (this.f2153x != null) {
                c += zztd.m3885b(24, this.f2153x);
            }
            if (this.f2154y != null) {
                c += zztd.m3885b(25, this.f2154y);
            }
            if (this.f2136g != null) {
                c += zztd.m3883b(26, this.f2136g.longValue());
            }
            return this.f2155z != null ? c + zztd.m3886b(28, this.f2155z.booleanValue()) : c;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0437d)) {
                return false;
            }
            C0437d c0437d = (C0437d) obj;
            if (this.f2130a == null) {
                if (c0437d.f2130a != null) {
                    return false;
                }
            } else if (!this.f2130a.equals(c0437d.f2130a)) {
                return false;
            }
            if (!C0447o.m3861a(this.f2131b, c0437d.f2131b)) {
                return false;
            }
            if (!C0447o.m3861a(this.f2132c, c0437d.f2132c)) {
                return false;
            }
            if (this.f2133d == null) {
                if (c0437d.f2133d != null) {
                    return false;
                }
            } else if (!this.f2133d.equals(c0437d.f2133d)) {
                return false;
            }
            if (this.f2134e == null) {
                if (c0437d.f2134e != null) {
                    return false;
                }
            } else if (!this.f2134e.equals(c0437d.f2134e)) {
                return false;
            }
            if (this.f2135f == null) {
                if (c0437d.f2135f != null) {
                    return false;
                }
            } else if (!this.f2135f.equals(c0437d.f2135f)) {
                return false;
            }
            if (this.f2136g == null) {
                if (c0437d.f2136g != null) {
                    return false;
                }
            } else if (!this.f2136g.equals(c0437d.f2136g)) {
                return false;
            }
            if (this.f2137h == null) {
                if (c0437d.f2137h != null) {
                    return false;
                }
            } else if (!this.f2137h.equals(c0437d.f2137h)) {
                return false;
            }
            if (this.f2138i == null) {
                if (c0437d.f2138i != null) {
                    return false;
                }
            } else if (!this.f2138i.equals(c0437d.f2138i)) {
                return false;
            }
            if (this.f2139j == null) {
                if (c0437d.f2139j != null) {
                    return false;
                }
            } else if (!this.f2139j.equals(c0437d.f2139j)) {
                return false;
            }
            if (this.f2140k == null) {
                if (c0437d.f2140k != null) {
                    return false;
                }
            } else if (!this.f2140k.equals(c0437d.f2140k)) {
                return false;
            }
            if (this.f2141l == null) {
                if (c0437d.f2141l != null) {
                    return false;
                }
            } else if (!this.f2141l.equals(c0437d.f2141l)) {
                return false;
            }
            if (this.f2142m == null) {
                if (c0437d.f2142m != null) {
                    return false;
                }
            } else if (!this.f2142m.equals(c0437d.f2142m)) {
                return false;
            }
            if (this.f2143n == null) {
                if (c0437d.f2143n != null) {
                    return false;
                }
            } else if (!this.f2143n.equals(c0437d.f2143n)) {
                return false;
            }
            if (this.f2144o == null) {
                if (c0437d.f2144o != null) {
                    return false;
                }
            } else if (!this.f2144o.equals(c0437d.f2144o)) {
                return false;
            }
            if (this.f2145p == null) {
                if (c0437d.f2145p != null) {
                    return false;
                }
            } else if (!this.f2145p.equals(c0437d.f2145p)) {
                return false;
            }
            if (this.f2146q == null) {
                if (c0437d.f2146q != null) {
                    return false;
                }
            } else if (!this.f2146q.equals(c0437d.f2146q)) {
                return false;
            }
            if (this.f2147r == null) {
                if (c0437d.f2147r != null) {
                    return false;
                }
            } else if (!this.f2147r.equals(c0437d.f2147r)) {
                return false;
            }
            if (this.f2148s == null) {
                if (c0437d.f2148s != null) {
                    return false;
                }
            } else if (!this.f2148s.equals(c0437d.f2148s)) {
                return false;
            }
            if (this.f2149t == null) {
                if (c0437d.f2149t != null) {
                    return false;
                }
            } else if (!this.f2149t.equals(c0437d.f2149t)) {
                return false;
            }
            if (this.f2150u == null) {
                if (c0437d.f2150u != null) {
                    return false;
                }
            } else if (!this.f2150u.equals(c0437d.f2150u)) {
                return false;
            }
            if (this.f2151v == null) {
                if (c0437d.f2151v != null) {
                    return false;
                }
            } else if (!this.f2151v.equals(c0437d.f2151v)) {
                return false;
            }
            if (this.f2152w == null) {
                if (c0437d.f2152w != null) {
                    return false;
                }
            } else if (!this.f2152w.equals(c0437d.f2152w)) {
                return false;
            }
            if (this.f2153x == null) {
                if (c0437d.f2153x != null) {
                    return false;
                }
            } else if (!this.f2153x.equals(c0437d.f2153x)) {
                return false;
            }
            if (this.f2154y == null) {
                if (c0437d.f2154y != null) {
                    return false;
                }
            } else if (!this.f2154y.equals(c0437d.f2154y)) {
                return false;
            }
            return this.f2155z == null ? c0437d.f2155z == null : this.f2155z.equals(c0437d.f2155z);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.f2154y == null ? 0 : this.f2154y.hashCode()) + (((this.f2153x == null ? 0 : this.f2153x.hashCode()) + (((this.f2152w == null ? 0 : this.f2152w.hashCode()) + (((this.f2151v == null ? 0 : this.f2151v.hashCode()) + (((this.f2150u == null ? 0 : this.f2150u.hashCode()) + (((this.f2149t == null ? 0 : this.f2149t.hashCode()) + (((this.f2148s == null ? 0 : this.f2148s.hashCode()) + (((this.f2147r == null ? 0 : this.f2147r.hashCode()) + (((this.f2146q == null ? 0 : this.f2146q.hashCode()) + (((this.f2145p == null ? 0 : this.f2145p.hashCode()) + (((this.f2144o == null ? 0 : this.f2144o.hashCode()) + (((this.f2143n == null ? 0 : this.f2143n.hashCode()) + (((this.f2142m == null ? 0 : this.f2142m.hashCode()) + (((this.f2141l == null ? 0 : this.f2141l.hashCode()) + (((this.f2140k == null ? 0 : this.f2140k.hashCode()) + (((this.f2139j == null ? 0 : this.f2139j.hashCode()) + (((this.f2138i == null ? 0 : this.f2138i.hashCode()) + (((this.f2137h == null ? 0 : this.f2137h.hashCode()) + (((this.f2136g == null ? 0 : this.f2136g.hashCode()) + (((this.f2135f == null ? 0 : this.f2135f.hashCode()) + (((this.f2134e == null ? 0 : this.f2134e.hashCode()) + (((this.f2133d == null ? 0 : this.f2133d.hashCode()) + (((((((this.f2130a == null ? 0 : this.f2130a.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + C0447o.m3860a(this.f2131b)) * 31) + C0447o.m3860a(this.f2132c)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.f2155z != null) {
                i = this.f2155z.hashCode();
            }
            return hashCode + i;
        }
    }

    /* renamed from: com.google.android.gms.internal.j.e */
    public static final class C0438e extends C0433p {
        private static volatile C0438e[] f2156f;
        public Long f2157a;
        public String f2158b;
        public String f2159c;
        public Long f2160d;
        public Float f2161e;

        public C0438e() {
            m3831b();
        }

        public static C0438e[] m3828a() {
            if (f2156f == null) {
                synchronized (C0447o.f2196a) {
                    if (f2156f == null) {
                        f2156f = new C0438e[0];
                    }
                }
            }
            return f2156f;
        }

        public C0438e m3829a(C0446n c0446n) {
            while (true) {
                int a = c0446n.m3838a();
                switch (a) {
                    case R.View_android_theme /*0*/:
                        break;
                    case R.Toolbar_contentInsetRight /*8*/:
                        this.f2157a = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.Toolbar_collapseIcon /*18*/:
                        this.f2158b = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_actionMenuTextColor /*26*/:
                        this.f2159c = c0446n.m3851g();
                        continue;
                    case R.AppCompatTheme_actionModeCutDrawable /*32*/:
                        this.f2160d = Long.valueOf(c0446n.m3845d());
                        continue;
                    case R.AppCompatTheme_actionDropDownStyle /*45*/:
                        this.f2161e = Float.valueOf(c0446n.m3843c());
                        continue;
                    default:
                        if (!C0449r.m3870a(c0446n, a)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public void m3830a(zztd com_google_android_gms_internal_zztd) {
            if (this.f2157a != null) {
                com_google_android_gms_internal_zztd.m3901a(1, this.f2157a.longValue());
            }
            if (this.f2158b != null) {
                com_google_android_gms_internal_zztd.m3903a(2, this.f2158b);
            }
            if (this.f2159c != null) {
                com_google_android_gms_internal_zztd.m3903a(3, this.f2159c);
            }
            if (this.f2160d != null) {
                com_google_android_gms_internal_zztd.m3901a(4, this.f2160d.longValue());
            }
            if (this.f2161e != null) {
                com_google_android_gms_internal_zztd.m3899a(5, this.f2161e.floatValue());
            }
            super.m3799a(com_google_android_gms_internal_zztd);
        }

        public C0438e m3831b() {
            this.f2157a = null;
            this.f2158b = null;
            this.f2159c = null;
            this.f2160d = null;
            this.f2161e = null;
            this.A = -1;
            return this;
        }

        public /* synthetic */ C0433p m3832b(C0446n c0446n) {
            return m3829a(c0446n);
        }

        protected int m3833c() {
            int c = super.m3801c();
            if (this.f2157a != null) {
                c += zztd.m3883b(1, this.f2157a.longValue());
            }
            if (this.f2158b != null) {
                c += zztd.m3885b(2, this.f2158b);
            }
            if (this.f2159c != null) {
                c += zztd.m3885b(3, this.f2159c);
            }
            if (this.f2160d != null) {
                c += zztd.m3883b(4, this.f2160d.longValue());
            }
            return this.f2161e != null ? c + zztd.m3881b(5, this.f2161e.floatValue()) : c;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0438e)) {
                return false;
            }
            C0438e c0438e = (C0438e) obj;
            if (this.f2157a == null) {
                if (c0438e.f2157a != null) {
                    return false;
                }
            } else if (!this.f2157a.equals(c0438e.f2157a)) {
                return false;
            }
            if (this.f2158b == null) {
                if (c0438e.f2158b != null) {
                    return false;
                }
            } else if (!this.f2158b.equals(c0438e.f2158b)) {
                return false;
            }
            if (this.f2159c == null) {
                if (c0438e.f2159c != null) {
                    return false;
                }
            } else if (!this.f2159c.equals(c0438e.f2159c)) {
                return false;
            }
            if (this.f2160d == null) {
                if (c0438e.f2160d != null) {
                    return false;
                }
            } else if (!this.f2160d.equals(c0438e.f2160d)) {
                return false;
            }
            return this.f2161e == null ? c0438e.f2161e == null : this.f2161e.equals(c0438e.f2161e);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.f2160d == null ? 0 : this.f2160d.hashCode()) + (((this.f2159c == null ? 0 : this.f2159c.hashCode()) + (((this.f2158b == null ? 0 : this.f2158b.hashCode()) + (((this.f2157a == null ? 0 : this.f2157a.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.f2161e != null) {
                i = this.f2161e.hashCode();
            }
            return hashCode + i;
        }
    }
}
