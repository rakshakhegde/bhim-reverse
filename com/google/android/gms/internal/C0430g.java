package com.google.android.gms.internal;

import android.content.res.Configuration;
import android.content.res.Resources;

/* renamed from: com.google.android.gms.internal.g */
public final class C0430g {
    public static boolean m3784a(Resources resources) {
        if (resources == null) {
            return false;
        }
        return (C0431h.m3786a() && ((resources.getConfiguration().screenLayout & 15) > 3)) || C0430g.m3785b(resources);
    }

    private static boolean m3785b(Resources resources) {
        Configuration configuration = resources.getConfiguration();
        return C0431h.m3788b() && (configuration.screenLayout & 15) <= 3 && configuration.smallestScreenWidthDp >= 600;
    }
}
