package com.google.android.gms.internal;

import com.google.android.gms.common.api.C0097c.C0095d;

/* renamed from: com.google.android.gms.internal.m */
public final class C0445m {
    public static final C0445m f2178a;
    private final boolean f2179b;
    private final boolean f2180c;
    private final String f2181d;
    private final C0095d f2182e;
    private final boolean f2183f;
    private final boolean f2184g;
    private final boolean f2185h;

    /* renamed from: com.google.android.gms.internal.m.a */
    public static final class C0444a {
        private boolean f2171a;
        private boolean f2172b;
        private String f2173c;
        private C0095d f2174d;
        private boolean f2175e;
        private boolean f2176f;
        private boolean f2177g;

        public C0445m m3834a() {
            return new C0445m(this.f2172b, this.f2173c, this.f2174d, this.f2175e, this.f2176f, this.f2177g, null);
        }
    }

    static {
        f2178a = new C0444a().m3834a();
    }

    private C0445m(boolean z, boolean z2, String str, C0095d c0095d, boolean z3, boolean z4, boolean z5) {
        this.f2179b = z;
        this.f2180c = z2;
        this.f2181d = str;
        this.f2182e = c0095d;
        this.f2183f = z3;
        this.f2184g = z4;
        this.f2185h = z5;
    }
}
