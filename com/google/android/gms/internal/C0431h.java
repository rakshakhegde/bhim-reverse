package com.google.android.gms.internal;

import android.os.Build.VERSION;

/* renamed from: com.google.android.gms.internal.h */
public final class C0431h {
    public static boolean m3786a() {
        return C0431h.m3787a(11);
    }

    private static boolean m3787a(int i) {
        return VERSION.SDK_INT >= i;
    }

    public static boolean m3788b() {
        return C0431h.m3787a(13);
    }

    public static boolean m3789c() {
        return C0431h.m3787a(14);
    }

    public static boolean m3790d() {
        return C0431h.m3787a(16);
    }

    public static boolean m3791e() {
        return C0431h.m3787a(18);
    }

    public static boolean m3792f() {
        return C0431h.m3787a(19);
    }

    public static boolean m3793g() {
        return C0431h.m3787a(20);
    }

    public static boolean m3794h() {
        return C0431h.m3787a(21);
    }
}
