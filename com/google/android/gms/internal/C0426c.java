package com.google.android.gms.internal;

import android.content.Context;
import java.util.regex.Pattern;

/* renamed from: com.google.android.gms.internal.c */
public final class C0426c {
    private static Pattern f2114a;

    static {
        f2114a = null;
    }

    public static int m3775a(int i) {
        return i / 1000;
    }

    public static boolean m3776a(Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.type.watch");
    }
}
