package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Process;
import com.google.android.gms.common.internal.C0353c;

/* renamed from: com.google.android.gms.internal.d */
public class C0427d {
    public static boolean m3777a() {
        return C0353c.f1920a && C0420b.m3758b() && C0420b.m3753a() == Process.myUid();
    }

    public static boolean m3778a(Context context, String str) {
        try {
            return (context.getPackageManager().getApplicationInfo(str, 0).flags & 2097152) != 0;
        } catch (NameNotFoundException e) {
            return false;
        }
    }
}
