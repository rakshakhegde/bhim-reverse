package com.google.android.gms.internal;

import android.os.SystemClock;

/* renamed from: com.google.android.gms.internal.f */
public final class C0429f implements C0428e {
    private static C0429f f2115a;

    public static synchronized C0428e m3781c() {
        C0428e c0428e;
        synchronized (C0429f.class) {
            if (f2115a == null) {
                f2115a = new C0429f();
            }
            c0428e = f2115a;
        }
        return c0428e;
    }

    public long m3782a() {
        return System.currentTimeMillis();
    }

    public long m3783b() {
        return SystemClock.elapsedRealtime();
    }
}
