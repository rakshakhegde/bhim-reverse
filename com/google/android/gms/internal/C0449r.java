package com.google.android.gms.internal;

/* renamed from: com.google.android.gms.internal.r */
public final class C0449r {
    public static final int[] f2197a;
    public static final long[] f2198b;
    public static final float[] f2199c;
    public static final double[] f2200d;
    public static final boolean[] f2201e;
    public static final String[] f2202f;
    public static final byte[][] f2203g;
    public static final byte[] f2204h;

    static {
        f2197a = new int[0];
        f2198b = new long[0];
        f2199c = new float[0];
        f2200d = new double[0];
        f2201e = new boolean[0];
        f2202f = new String[0];
        f2203g = new byte[0][];
        f2204h = new byte[0];
    }

    static int m3868a(int i) {
        return i & 7;
    }

    static int m3869a(int i, int i2) {
        return (i << 3) | i2;
    }

    public static boolean m3870a(C0446n c0446n, int i) {
        return c0446n.m3842b(i);
    }

    public static int m3871b(int i) {
        return i >>> 3;
    }

    public static final int m3872b(C0446n c0446n, int i) {
        int i2 = 1;
        int m = c0446n.m3858m();
        c0446n.m3842b(i);
        while (c0446n.m3838a() == i) {
            c0446n.m3842b(i);
            i2++;
        }
        c0446n.m3848e(m);
        return i2;
    }
}
