package com.google.android.gms.iid;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.p004a.WakefulBroadcastReceiver;
import android.util.Log;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.gcm.C0399c;
import java.io.IOException;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: com.google.android.gms.iid.b */
public class C0411b extends Service {
    static String f2079a;
    private static String f2080f;
    private static String f2081g;
    private static String f2082h;
    MessengerCompat f2083b;
    BroadcastReceiver f2084c;
    int f2085d;
    int f2086e;

    /* renamed from: com.google.android.gms.iid.b.1 */
    class C04091 extends Handler {
        final /* synthetic */ C0411b f2077a;

        C04091(C0411b c0411b, Looper looper) {
            this.f2077a = c0411b;
            super(looper);
        }

        public void handleMessage(Message message) {
            this.f2077a.m3703a(message, MessengerCompat.m3685a(message));
        }
    }

    /* renamed from: com.google.android.gms.iid.b.2 */
    class C04102 extends BroadcastReceiver {
        final /* synthetic */ C0411b f2078a;

        C04102(C0411b c0411b) {
            this.f2078a = c0411b;
        }

        public void onReceive(Context context, Intent intent) {
            if (Log.isLoggable("InstanceID", 3)) {
                intent.getStringExtra("registration_id");
                Log.d("InstanceID", "Received GSF callback using dynamic receiver: " + intent.getExtras());
            }
            this.f2078a.m3707a(intent);
            this.f2078a.m3705a();
        }
    }

    static {
        f2079a = CLConstants.OUTPUT_KEY_ACTION;
        f2080f = "google.com/iid";
        f2081g = "CMD";
        f2082h = "gcm.googleapis.com/refresh";
    }

    public C0411b() {
        this.f2083b = new MessengerCompat(new C04091(this, Looper.getMainLooper()));
        this.f2084c = new C04102(this);
    }

    static void m3701a(Context context) {
        Intent intent = new Intent("com.google.android.gms.iid.InstanceID");
        intent.setPackage(context.getPackageName());
        intent.putExtra(f2081g, "SYNC");
        context.startService(intent);
    }

    static void m3702a(Context context, C0416f c0416f) {
        c0416f.m3738b();
        Intent intent = new Intent("com.google.android.gms.iid.InstanceID");
        intent.putExtra(f2081g, "RST");
        intent.setPackage(context.getPackageName());
        context.startService(intent);
    }

    private void m3703a(Message message, int i) {
        C0415e.m3712a((Context) this);
        getPackageManager();
        if (i == C0415e.f2091c || i == C0415e.f2090b) {
            m3707a((Intent) message.obj);
        } else {
            Log.w("InstanceID", "Message from unexpected caller " + i + " mine=" + C0415e.f2090b + " appid=" + C0415e.f2091c);
        }
    }

    void m3705a() {
        synchronized (this) {
            this.f2085d--;
            if (this.f2085d == 0) {
                stopSelf(this.f2086e);
            }
            if (Log.isLoggable("InstanceID", 3)) {
                Log.d("InstanceID", "Stop " + this.f2085d + " " + this.f2086e);
            }
        }
    }

    void m3706a(int i) {
        synchronized (this) {
            this.f2085d++;
            if (i > this.f2086e) {
                this.f2086e = i;
            }
        }
    }

    public void m3707a(Intent intent) {
        C0408a b;
        String stringExtra = intent.getStringExtra(CLConstants.FIELD_SUBTYPE);
        if (stringExtra == null) {
            b = C0408a.m3693b(this);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(CLConstants.FIELD_SUBTYPE, stringExtra);
            b = C0408a.m3690a(this, bundle);
        }
        String stringExtra2 = intent.getStringExtra(f2081g);
        if (intent.getStringExtra(CLConstants.OUTPUT_KEY_ERROR) == null && intent.getStringExtra("registration_id") == null) {
            if (Log.isLoggable("InstanceID", 3)) {
                Log.d("InstanceID", "Service command " + stringExtra + " " + stringExtra2 + " " + intent.getExtras());
            }
            if (intent.getStringExtra("unregistered") != null) {
                C0416f c = b.m3698c();
                if (stringExtra == null) {
                    stringExtra = BuildConfig.FLAVOR;
                }
                c.m3742e(stringExtra);
                b.m3699d().m3728d(intent);
                return;
            } else if (f2082h.equals(intent.getStringExtra("from"))) {
                b.m3698c().m3742e(stringExtra);
                m3708a(false);
                return;
            } else if ("RST".equals(stringExtra2)) {
                b.m3697b();
                m3708a(true);
                return;
            } else if ("RST_FULL".equals(stringExtra2)) {
                if (!b.m3698c().m3737a()) {
                    b.m3698c().m3738b();
                    m3708a(true);
                    return;
                }
                return;
            } else if ("SYNC".equals(stringExtra2)) {
                b.m3698c().m3742e(stringExtra);
                m3708a(false);
                return;
            } else if ("PING".equals(stringExtra2)) {
                try {
                    C0399c.m3648a((Context) this).m3657a(f2080f, C0415e.m3719b(), 0, intent.getExtras());
                    return;
                } catch (IOException e) {
                    Log.w("InstanceID", "Failed to send ping response");
                    return;
                }
            } else {
                return;
            }
        }
        if (Log.isLoggable("InstanceID", 3)) {
            Log.d("InstanceID", "Register result in service " + stringExtra);
        }
        b.m3699d().m3728d(intent);
    }

    public void m3708a(boolean z) {
        m3709b();
    }

    public void m3709b() {
    }

    public IBinder onBind(Intent intent) {
        return (intent == null || !"com.google.android.gms.iid.InstanceID".equals(intent.getAction())) ? null : this.f2083b.m3687a();
    }

    public void onCreate() {
        IntentFilter intentFilter = new IntentFilter("com.google.android.c2dm.intent.REGISTRATION");
        intentFilter.addCategory(getPackageName());
        registerReceiver(this.f2084c, intentFilter, "com.google.android.c2dm.permission.RECEIVE", null);
    }

    public void onDestroy() {
        unregisterReceiver(this.f2084c);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        m3706a(i2);
        if (intent == null) {
            m3705a();
            return 2;
        }
        try {
            if ("com.google.android.gms.iid.InstanceID".equals(intent.getAction())) {
                if (VERSION.SDK_INT <= 18) {
                    Intent intent2 = (Intent) intent.getParcelableExtra("GSF");
                    if (intent2 != null) {
                        startService(intent2);
                        return 1;
                    }
                }
                m3707a(intent);
            }
            m3705a();
            if (intent.getStringExtra("from") != null) {
                WakefulBroadcastReceiver.m115a(intent);
            }
            return 2;
        } finally {
            m3705a();
        }
    }
}
