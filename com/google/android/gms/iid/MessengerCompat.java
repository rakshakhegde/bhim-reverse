package com.google.android.gms.iid;

import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.iid.C0405d.C0406a;

public class MessengerCompat implements Parcelable {
    public static final Creator<MessengerCompat> CREATOR;
    Messenger f2067a;
    C0405d f2068b;

    /* renamed from: com.google.android.gms.iid.MessengerCompat.1 */
    static class C04041 implements Creator<MessengerCompat> {
        C04041() {
        }

        public MessengerCompat m3680a(Parcel parcel) {
            IBinder readStrongBinder = parcel.readStrongBinder();
            return readStrongBinder != null ? new MessengerCompat(readStrongBinder) : null;
        }

        public MessengerCompat[] m3681a(int i) {
            return new MessengerCompat[i];
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m3680a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m3681a(i);
        }
    }

    /* renamed from: com.google.android.gms.iid.MessengerCompat.a */
    private final class C0407a extends C0406a {
        Handler f2065a;
        final /* synthetic */ MessengerCompat f2066b;

        C0407a(MessengerCompat messengerCompat, Handler handler) {
            this.f2066b = messengerCompat;
            this.f2065a = handler;
        }

        public void m3684a(Message message) {
            message.arg2 = Binder.getCallingUid();
            this.f2065a.dispatchMessage(message);
        }
    }

    static {
        CREATOR = new C04041();
    }

    public MessengerCompat(Handler handler) {
        if (VERSION.SDK_INT >= 21) {
            this.f2067a = new Messenger(handler);
        } else {
            this.f2068b = new C0407a(this, handler);
        }
    }

    public MessengerCompat(IBinder iBinder) {
        if (VERSION.SDK_INT >= 21) {
            this.f2067a = new Messenger(iBinder);
        } else {
            this.f2068b = C0406a.m3683a(iBinder);
        }
    }

    public static int m3685a(Message message) {
        return VERSION.SDK_INT >= 21 ? m3686c(message) : message.arg2;
    }

    private static int m3686c(Message message) {
        return message.sendingUid;
    }

    public IBinder m3687a() {
        return this.f2067a != null ? this.f2067a.getBinder() : this.f2068b.asBinder();
    }

    public void m3688b(Message message) {
        if (this.f2067a != null) {
            this.f2067a.send(message);
        } else {
            this.f2068b.m3682a(message);
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj != null) {
            try {
                z = m3687a().equals(((MessengerCompat) obj).m3687a());
            } catch (ClassCastException e) {
            }
        }
        return z;
    }

    public int hashCode() {
        return m3687a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.f2067a != null) {
            parcel.writeStrongBinder(this.f2067a.getBinder());
        } else {
            parcel.writeStrongBinder(this.f2068b.asBinder());
        }
    }
}
