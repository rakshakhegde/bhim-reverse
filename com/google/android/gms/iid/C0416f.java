package com.google.android.gms.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.p004a.ContextCompat;
import android.util.Base64;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: com.google.android.gms.iid.f */
public class C0416f {
    SharedPreferences f2104a;
    Context f2105b;

    public C0416f(Context context) {
        this(context, "com.google.android.gms.appid");
    }

    public C0416f(Context context, String str) {
        this.f2105b = context;
        this.f2104a = context.getSharedPreferences(str, 4);
        m3730g(str + "-no-backup");
    }

    private String m3729b(String str, String str2, String str3) {
        return str + "|T|" + str2 + CLConstants.SALT_DELIMETER + str3;
    }

    private void m3730g(String str) {
        File file = new File(new ContextCompat().m82a(this.f2105b), str);
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !m3737a()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    C0411b.m3702a(this.f2105b, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    Log.d("InstanceID/Store", "Error creating file in no backup dir: " + e.getMessage());
                }
            }
        }
    }

    synchronized String m3731a(String str) {
        return this.f2104a.getString(str, null);
    }

    synchronized String m3732a(String str, String str2) {
        return this.f2104a.getString(str + "|S|" + str2, null);
    }

    public synchronized String m3733a(String str, String str2, String str3) {
        return this.f2104a.getString(m3729b(str, str2, str3), null);
    }

    synchronized KeyPair m3734a(String str, long j) {
        KeyPair a;
        a = C0412c.m3710a();
        Editor edit = this.f2104a.edit();
        m3735a(edit, str, "|P|", C0408a.m3692a(a.getPublic().getEncoded()));
        m3735a(edit, str, "|K|", C0408a.m3692a(a.getPrivate().getEncoded()));
        m3735a(edit, str, "cre", Long.toString(j));
        edit.commit();
        return a;
    }

    synchronized void m3735a(Editor editor, String str, String str2, String str3) {
        editor.putString(str + "|S|" + str2, str3);
    }

    public synchronized void m3736a(String str, String str2, String str3, String str4, String str5) {
        String b = m3729b(str, str2, str3);
        Editor edit = this.f2104a.edit();
        edit.putString(b, str4);
        edit.putString("appVersion", str5);
        edit.putString("lastToken", Long.toString(System.currentTimeMillis() / 1000));
        edit.commit();
    }

    boolean m3737a() {
        return this.f2104a.getAll().isEmpty();
    }

    public synchronized void m3738b() {
        this.f2104a.edit().clear().commit();
    }

    public synchronized void m3739b(String str) {
        Editor edit = this.f2104a.edit();
        for (String str2 : this.f2104a.getAll().keySet()) {
            if (str2.startsWith(str)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    public KeyPair m3740c(String str) {
        return m3743f(str);
    }

    void m3741d(String str) {
        m3739b(str + CLConstants.SALT_DELIMETER);
    }

    public void m3742e(String str) {
        m3739b(str + "|T|");
    }

    KeyPair m3743f(String str) {
        Object e;
        String a = m3732a(str, "|P|");
        String a2 = m3732a(str, "|K|");
        if (a2 == null) {
            return null;
        }
        try {
            byte[] decode = Base64.decode(a, 8);
            byte[] decode2 = Base64.decode(a2, 8);
            KeyFactory instance = KeyFactory.getInstance("RSA");
            return new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
        } catch (InvalidKeySpecException e2) {
            e = e2;
            Log.w("InstanceID/Store", "Invalid key stored " + e);
            C0411b.m3702a(this.f2105b, this);
            return null;
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            Log.w("InstanceID/Store", "Invalid key stored " + e);
            C0411b.m3702a(this.f2105b, this);
            return null;
        }
    }
}
