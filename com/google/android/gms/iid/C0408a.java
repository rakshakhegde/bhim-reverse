package com.google.android.gms.iid;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import com.crashlytics.android.core.BuildConfig;
import java.io.IOException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: com.google.android.gms.iid.a */
public class C0408a {
    static Map<String, C0408a> f2069a;
    static String f2070f;
    private static C0416f f2071g;
    private static C0415e f2072h;
    Context f2073b;
    KeyPair f2074c;
    String f2075d;
    long f2076e;

    static {
        f2069a = new HashMap();
    }

    protected C0408a(Context context, String str, Bundle bundle) {
        this.f2075d = BuildConfig.FLAVOR;
        this.f2073b = context.getApplicationContext();
        this.f2075d = str;
    }

    static int m3689a(Context context) {
        int i = 0;
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.w("InstanceID", "Never happens: can't find own package " + e);
            return i;
        }
    }

    public static synchronized C0408a m3690a(Context context, Bundle bundle) {
        C0408a c0408a;
        synchronized (C0408a.class) {
            String string = bundle == null ? BuildConfig.FLAVOR : bundle.getString(CLConstants.FIELD_SUBTYPE);
            String str = string == null ? BuildConfig.FLAVOR : string;
            Context applicationContext = context.getApplicationContext();
            if (f2071g == null) {
                f2071g = new C0416f(applicationContext);
                f2072h = new C0415e(applicationContext);
            }
            f2070f = Integer.toString(C0408a.m3689a(applicationContext));
            c0408a = (C0408a) f2069a.get(str);
            if (c0408a == null) {
                c0408a = new C0408a(applicationContext, str, bundle);
                f2069a.put(str, c0408a);
            }
        }
        return c0408a;
    }

    static String m3691a(KeyPair keyPair) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(keyPair.getPublic().getEncoded());
            digest[0] = (byte) (((digest[0] & 15) + 112) & 255);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException e) {
            Log.w("InstanceID", "Unexpected error, device missing required alghorithms");
            return null;
        }
    }

    static String m3692a(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    public static C0408a m3693b(Context context) {
        return C0408a.m3690a(context, null);
    }

    public String m3694a(String str, String str2, Bundle bundle) {
        Object obj = null;
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        Object obj2 = 1;
        String a = m3700e() ? null : f2071g.m3733a(this.f2075d, str, str2);
        if (a == null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            if (bundle.getString("ttl") != null) {
                obj2 = null;
            }
            if (!"jwt".equals(bundle.getString(CLConstants.FIELD_TYPE))) {
                obj = obj2;
            }
            a = m3696b(str, str2, bundle);
            Log.w("InstanceID", "token: " + a);
            if (!(a == null || r1 == null)) {
                f2071g.m3736a(this.f2075d, str, str2, a, f2070f);
            }
        }
        return a;
    }

    KeyPair m3695a() {
        if (this.f2074c == null) {
            this.f2074c = f2071g.m3740c(this.f2075d);
        }
        if (this.f2074c == null) {
            this.f2076e = System.currentTimeMillis();
            this.f2074c = f2071g.m3734a(this.f2075d, this.f2076e);
        }
        return this.f2074c;
    }

    public String m3696b(String str, String str2, Bundle bundle) {
        if (str2 != null) {
            bundle.putString("scope", str2);
        }
        bundle.putString("sender", str);
        String str3 = BuildConfig.FLAVOR.equals(this.f2075d) ? str : this.f2075d;
        if (!bundle.containsKey("legacy.register")) {
            bundle.putString("subscription", str);
            bundle.putString(CLConstants.FIELD_SUBTYPE, str3);
            bundle.putString("X-subscription", str);
            bundle.putString("X-subtype", str3);
        }
        return f2072h.m3726b(f2072h.m3720a(bundle, m3695a()));
    }

    void m3697b() {
        this.f2076e = 0;
        f2071g.m3741d(this.f2075d);
        this.f2074c = null;
    }

    C0416f m3698c() {
        return f2071g;
    }

    C0415e m3699d() {
        return f2072h;
    }

    boolean m3700e() {
        String a = f2071g.m3731a("appVersion");
        if (a == null || !a.equals(f2070f)) {
            return true;
        }
        a = f2071g.m3731a("lastToken");
        if (a == null) {
            return true;
        }
        return (System.currentTimeMillis() / 1000) - Long.valueOf(Long.parseLong(a)).longValue() > 604800;
    }
}
