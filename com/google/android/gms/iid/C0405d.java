package com.google.android.gms.iid;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.iid.d */
public interface C0405d extends IInterface {

    /* renamed from: com.google.android.gms.iid.d.a */
    public static abstract class C0406a extends Binder implements C0405d {

        /* renamed from: com.google.android.gms.iid.d.a.a */
        private static class C0413a implements C0405d {
            private IBinder f2087a;

            C0413a(IBinder iBinder) {
                this.f2087a = iBinder;
            }

            public void m3711a(Message message) {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
                    if (message != null) {
                        obtain.writeInt(1);
                        message.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2087a.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f2087a;
            }
        }

        public C0406a() {
            attachInterface(this, "com.google.android.gms.iid.IMessengerCompat");
        }

        public static C0405d m3683a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.iid.IMessengerCompat");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C0405d)) ? new C0413a(iBinder) : (C0405d) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            switch (i) {
                case R.View_android_focusable /*1*/:
                    parcel.enforceInterface("com.google.android.gms.iid.IMessengerCompat");
                    m3682a(parcel.readInt() != 0 ? (Message) Message.CREATOR.createFromParcel(parcel) : null);
                    return true;
                case 1598968902:
                    parcel2.writeString("com.google.android.gms.iid.IMessengerCompat");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    void m3682a(Message message);
}
