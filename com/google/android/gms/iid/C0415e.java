package com.google.android.gms.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.C0399c;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: com.google.android.gms.iid.e */
public class C0415e {
    static String f2089a;
    static int f2090b;
    static int f2091c;
    static int f2092d;
    Context f2093e;
    Map<String, Object> f2094f;
    Messenger f2095g;
    Messenger f2096h;
    MessengerCompat f2097i;
    PendingIntent f2098j;
    long f2099k;
    long f2100l;
    int f2101m;
    int f2102n;
    long f2103o;

    /* renamed from: com.google.android.gms.iid.e.1 */
    class C04141 extends Handler {
        final /* synthetic */ C0415e f2088a;

        C04141(C0415e c0415e, Looper looper) {
            this.f2088a = c0415e;
            super(looper);
        }

        public void handleMessage(Message message) {
            this.f2088a.m3725a(message);
        }
    }

    static {
        f2089a = null;
        f2090b = 0;
        f2091c = 0;
        f2092d = 0;
    }

    public C0415e(Context context) {
        this.f2094f = new HashMap();
        this.f2093e = context;
    }

    public static String m3712a(Context context) {
        ApplicationInfo applicationInfo;
        if (f2089a != null) {
            return f2089a;
        }
        f2090b = Process.myUid();
        PackageManager packageManager = context.getPackageManager();
        for (ResolveInfo resolveInfo : packageManager.queryIntentServices(new Intent("com.google.android.c2dm.intent.REGISTER"), 0)) {
            if (packageManager.checkPermission("com.google.android.c2dm.permission.RECEIVE", resolveInfo.serviceInfo.packageName) == 0) {
                try {
                    ApplicationInfo applicationInfo2 = packageManager.getApplicationInfo(resolveInfo.serviceInfo.packageName, 0);
                    Log.w("InstanceID/Rpc", "Found " + applicationInfo2.uid);
                    f2091c = applicationInfo2.uid;
                    f2089a = resolveInfo.serviceInfo.packageName;
                    return f2089a;
                } catch (NameNotFoundException e) {
                }
            } else {
                Log.w("InstanceID/Rpc", "Possible malicious package " + resolveInfo.serviceInfo.packageName + " declares " + "com.google.android.c2dm.intent.REGISTER" + " without permission");
            }
        }
        Log.w("InstanceID/Rpc", "Failed to resolve REGISTER intent, falling back");
        try {
            applicationInfo = packageManager.getApplicationInfo(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, 0);
            f2089a = applicationInfo.packageName;
            f2091c = applicationInfo.uid;
            return f2089a;
        } catch (NameNotFoundException e2) {
            try {
                applicationInfo = packageManager.getApplicationInfo("com.google.android.gsf", 0);
                f2089a = applicationInfo.packageName;
                f2091c = applicationInfo.uid;
                return f2089a;
            } catch (NameNotFoundException e3) {
                Log.w("InstanceID/Rpc", "Both Google Play Services and legacy GSF package are missing");
                return null;
            }
        }
    }

    static String m3713a(KeyPair keyPair, String... strArr) {
        String str = null;
        try {
            byte[] bytes = TextUtils.join("\n", strArr).getBytes("UTF-8");
            try {
                PrivateKey privateKey = keyPair.getPrivate();
                Signature instance = Signature.getInstance(privateKey instanceof RSAPrivateKey ? "SHA256withRSA" : "SHA256withECDSA");
                instance.initSign(privateKey);
                instance.update(bytes);
                str = C0408a.m3692a(instance.sign());
            } catch (Throwable e) {
                Log.e("InstanceID/Rpc", "Unable to sign registration request", e);
            }
        } catch (Throwable e2) {
            Log.e("InstanceID/Rpc", "Unable to encode string", e2);
        }
        return str;
    }

    private void m3714a(Object obj) {
        synchronized (getClass()) {
            for (String str : this.f2094f.keySet()) {
                Object obj2 = this.f2094f.get(str);
                this.f2094f.put(str, obj);
                m3715a(obj2, obj);
            }
        }
    }

    private void m3715a(Object obj, Object obj2) {
        if (obj instanceof ConditionVariable) {
            ((ConditionVariable) obj).open();
        }
        if (obj instanceof Messenger) {
            Messenger messenger = (Messenger) obj;
            Message obtain = Message.obtain();
            obtain.obj = obj2;
            try {
                messenger.send(obtain);
            } catch (RemoteException e) {
                Log.w("InstanceID/Rpc", "Failed to send response " + e);
            }
        }
    }

    private void m3716a(String str) {
        if ("com.google.android.gsf".equals(f2089a)) {
            this.f2101m++;
            if (this.f2101m >= 3) {
                if (this.f2101m == 3) {
                    this.f2102n = new Random().nextInt(1000) + 1000;
                }
                this.f2102n *= 2;
                this.f2103o = SystemClock.elapsedRealtime() + ((long) this.f2102n);
                Log.w("InstanceID/Rpc", "Backoff due to " + str + " for " + this.f2102n);
            }
        }
    }

    private void m3717a(String str, Object obj) {
        synchronized (getClass()) {
            Object obj2 = this.f2094f.get(str);
            this.f2094f.put(str, obj);
            m3715a(obj2, obj);
        }
    }

    private Intent m3718b(Bundle bundle, KeyPair keyPair) {
        Intent intent;
        ConditionVariable conditionVariable = new ConditionVariable();
        String b = C0415e.m3719b();
        synchronized (getClass()) {
            this.f2094f.put(b, conditionVariable);
        }
        m3724a(bundle, keyPair, b);
        conditionVariable.block(30000);
        synchronized (getClass()) {
            Object remove = this.f2094f.remove(b);
            if (remove instanceof Intent) {
                intent = (Intent) remove;
            } else if (remove instanceof String) {
                throw new IOException((String) remove);
            } else {
                Log.w("InstanceID/Rpc", "No response " + remove);
                throw new IOException("TIMEOUT");
            }
        }
        return intent;
    }

    public static synchronized String m3719b() {
        String num;
        synchronized (C0415e.class) {
            int i = f2092d;
            f2092d = i + 1;
            num = Integer.toString(i);
        }
        return num;
    }

    Intent m3720a(Bundle bundle, KeyPair keyPair) {
        Intent b = m3718b(bundle, keyPair);
        return (b == null || !b.hasExtra("google.messenger")) ? b : m3718b(bundle, keyPair);
    }

    void m3721a() {
        if (this.f2095g == null) {
            C0415e.m3712a(this.f2093e);
            this.f2095g = new Messenger(new C04141(this, Looper.getMainLooper()));
        }
    }

    synchronized void m3722a(Intent intent) {
        if (this.f2098j == null) {
            Intent intent2 = new Intent();
            intent2.setPackage("com.google.example.invalidpackage");
            this.f2098j = PendingIntent.getBroadcast(this.f2093e, 0, intent2, 0);
        }
        intent.putExtra("app", this.f2098j);
    }

    protected void m3723a(Intent intent, String str) {
        this.f2099k = SystemClock.elapsedRealtime();
        intent.putExtra("kid", "|ID|" + str + CLConstants.SALT_DELIMETER);
        intent.putExtra("X-kid", "|ID|" + str + CLConstants.SALT_DELIMETER);
        boolean equals = "com.google.android.gsf".equals(f2089a);
        String stringExtra = intent.getStringExtra("useGsf");
        if (stringExtra != null) {
            equals = "1".equals(stringExtra);
        }
        if (Log.isLoggable("InstanceID/Rpc", 3)) {
            Log.d("InstanceID/Rpc", "Sending " + intent.getExtras());
        }
        if (this.f2096h != null) {
            intent.putExtra("google.messenger", this.f2095g);
            Message obtain = Message.obtain();
            obtain.obj = intent;
            try {
                this.f2096h.send(obtain);
                return;
            } catch (RemoteException e) {
                if (Log.isLoggable("InstanceID/Rpc", 3)) {
                    Log.d("InstanceID/Rpc", "Messenger failed, fallback to startService");
                }
            }
        }
        if (equals) {
            Intent intent2 = new Intent("com.google.android.gms.iid.InstanceID");
            intent2.setPackage(this.f2093e.getPackageName());
            intent2.putExtra("GSF", intent);
            this.f2093e.startService(intent2);
            return;
        }
        intent.putExtra("google.messenger", this.f2095g);
        intent.putExtra("messenger2", "1");
        if (this.f2097i != null) {
            Message obtain2 = Message.obtain();
            obtain2.obj = intent;
            try {
                this.f2097i.m3688b(obtain2);
                return;
            } catch (RemoteException e2) {
                if (Log.isLoggable("InstanceID/Rpc", 3)) {
                    Log.d("InstanceID/Rpc", "Messenger failed, fallback to startService");
                }
            }
        }
        this.f2093e.startService(intent);
    }

    void m3724a(Bundle bundle, KeyPair keyPair, String str) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.f2103o == 0 || elapsedRealtime > this.f2103o) {
            m3721a();
            if (f2089a == null) {
                throw new IOException("MISSING_INSTANCEID_SERVICE");
            }
            this.f2099k = SystemClock.elapsedRealtime();
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage(f2089a);
            bundle.putString("gmsv", Integer.toString(C0399c.m3655c(this.f2093e)));
            bundle.putString("osv", Integer.toString(VERSION.SDK_INT));
            bundle.putString("app_ver", Integer.toString(C0408a.m3689a(this.f2093e)));
            bundle.putString("cliv", "1");
            bundle.putString("appid", C0408a.m3691a(keyPair));
            bundle.putString("pub2", C0408a.m3692a(keyPair.getPublic().getEncoded()));
            bundle.putString("sig", C0415e.m3713a(keyPair, this.f2093e.getPackageName(), r1));
            intent.putExtras(bundle);
            m3722a(intent);
            m3723a(intent, str);
            return;
        }
        Log.w("InstanceID/Rpc", "Backoff mode, next request attempt: " + (this.f2103o - elapsedRealtime) + " interval: " + this.f2102n);
        throw new IOException("RETRY_LATER");
    }

    public void m3725a(Message message) {
        if (message != null) {
            if (message.obj instanceof Intent) {
                Intent intent = (Intent) message.obj;
                intent.setExtrasClassLoader(MessengerCompat.class.getClassLoader());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof MessengerCompat) {
                        this.f2097i = (MessengerCompat) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f2096h = (Messenger) parcelableExtra;
                    }
                }
                m3728d((Intent) message.obj);
                return;
            }
            Log.w("InstanceID/Rpc", "Dropping invalid message");
        }
    }

    String m3726b(Intent intent) {
        if (intent == null) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
        String stringExtra = intent.getStringExtra("registration_id");
        if (stringExtra == null) {
            stringExtra = intent.getStringExtra("unregistered");
        }
        intent.getLongExtra("Retry-After", 0);
        if (stringExtra != null) {
            if (stringExtra == null) {
                return stringExtra;
            }
            stringExtra = intent.getStringExtra(CLConstants.OUTPUT_KEY_ERROR);
            if (stringExtra == null) {
                throw new IOException(stringExtra);
            }
            Log.w("InstanceID/Rpc", "Unexpected response from GCM " + intent.getExtras(), new Throwable());
            throw new IOException("SERVICE_NOT_AVAILABLE");
        } else if (stringExtra == null) {
            return stringExtra;
        } else {
            stringExtra = intent.getStringExtra(CLConstants.OUTPUT_KEY_ERROR);
            if (stringExtra == null) {
                Log.w("InstanceID/Rpc", "Unexpected response from GCM " + intent.getExtras(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
            throw new IOException(stringExtra);
        }
    }

    void m3727c(Intent intent) {
        String stringExtra = intent.getStringExtra(CLConstants.OUTPUT_KEY_ERROR);
        if (stringExtra == null) {
            Log.w("InstanceID/Rpc", "Unexpected response, no error or registration id " + intent.getExtras());
            return;
        }
        if (Log.isLoggable("InstanceID/Rpc", 3)) {
            Log.d("InstanceID/Rpc", "Received InstanceID error " + stringExtra);
        }
        String str = null;
        if (stringExtra.startsWith(CLConstants.SALT_DELIMETER)) {
            String[] split = stringExtra.split("\\|");
            if (!"ID".equals(split[1])) {
                Log.w("InstanceID/Rpc", "Unexpected structured response " + stringExtra);
            }
            if (split.length > 2) {
                str = split[2];
                stringExtra = split[3];
                if (stringExtra.startsWith(":")) {
                    stringExtra = stringExtra.substring(1);
                }
            } else {
                stringExtra = "UNKNOWN";
            }
            intent.putExtra(CLConstants.OUTPUT_KEY_ERROR, stringExtra);
        }
        if (str == null) {
            m3714a((Object) stringExtra);
        } else {
            m3717a(str, (Object) stringExtra);
        }
        long longExtra = intent.getLongExtra("Retry-After", 0);
        if (longExtra > 0) {
            this.f2100l = SystemClock.elapsedRealtime();
            this.f2102n = ((int) longExtra) * 1000;
            this.f2103o = SystemClock.elapsedRealtime() + ((long) this.f2102n);
            Log.w("InstanceID/Rpc", "Explicit request from server to backoff: " + this.f2102n);
        } else if ("SERVICE_NOT_AVAILABLE".equals(stringExtra) || "AUTHENTICATION_FAILED".equals(stringExtra)) {
            m3716a(stringExtra);
        }
    }

    void m3728d(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if ("com.google.android.c2dm.intent.REGISTRATION".equals(action) || "com.google.android.gms.iid.InstanceID".equals(action)) {
                action = intent.getStringExtra("registration_id");
                String stringExtra = action == null ? intent.getStringExtra("unregistered") : action;
                if (stringExtra == null) {
                    m3727c(intent);
                    return;
                }
                this.f2099k = SystemClock.elapsedRealtime();
                this.f2103o = 0;
                this.f2101m = 0;
                this.f2102n = 0;
                if (Log.isLoggable("InstanceID/Rpc", 3)) {
                    Log.d("InstanceID/Rpc", "AppIDResponse: " + stringExtra + " " + intent.getExtras());
                }
                action = null;
                if (stringExtra.startsWith(CLConstants.SALT_DELIMETER)) {
                    String[] split = stringExtra.split("\\|");
                    if (!"ID".equals(split[1])) {
                        Log.w("InstanceID/Rpc", "Unexpected structured response " + stringExtra);
                    }
                    stringExtra = split[2];
                    if (split.length > 4) {
                        if ("SYNC".equals(split[3])) {
                            C0411b.m3701a(this.f2093e);
                        } else if ("RST".equals(split[3])) {
                            C0411b.m3702a(this.f2093e, C0408a.m3693b(this.f2093e).m3698c());
                            intent.removeExtra("registration_id");
                            m3717a(stringExtra, (Object) intent);
                            return;
                        }
                    }
                    action = split[split.length - 1];
                    if (action.startsWith(":")) {
                        action = action.substring(1);
                    }
                    intent.putExtra("registration_id", action);
                    action = stringExtra;
                }
                if (action == null) {
                    m3714a((Object) intent);
                } else {
                    m3717a(action, (Object) intent);
                }
            } else if (Log.isLoggable("InstanceID/Rpc", 3)) {
                Log.d("InstanceID/Rpc", "Unexpected response " + intent.getAction());
            }
        } else if (Log.isLoggable("InstanceID/Rpc", 3)) {
            Log.d("InstanceID/Rpc", "Unexpected response: null");
        }
    }
}
