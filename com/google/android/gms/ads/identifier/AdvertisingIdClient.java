package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.google.android.gms.common.C0100b;
import com.google.android.gms.common.C0102d;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.stats.C0389b;
import com.google.android.gms.internal.C0417a;
import com.google.android.gms.internal.C0417a.C0419a;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class AdvertisingIdClient {
    private static boolean zzoN;
    private final Context mContext;
    C0102d zzoH;
    C0417a zzoI;
    boolean zzoJ;
    Object zzoK;
    C0083a zzoL;
    final long zzoM;

    public static final class Info {
        private final String zzoS;
        private final boolean zzoT;

        public Info(String str, boolean z) {
            this.zzoS = str;
            this.zzoT = z;
        }

        public String getId() {
            return this.zzoS;
        }

        public boolean isLimitAdTrackingEnabled() {
            return this.zzoT;
        }

        public String toString() {
            return "{" + this.zzoS + "}" + this.zzoT;
        }
    }

    /* renamed from: com.google.android.gms.ads.identifier.AdvertisingIdClient.a */
    static class C0083a extends Thread {
        CountDownLatch f1713a;
        boolean f1714b;
        private WeakReference<AdvertisingIdClient> f1715c;
        private long f1716d;

        public C0083a(AdvertisingIdClient advertisingIdClient, long j) {
            this.f1715c = new WeakReference(advertisingIdClient);
            this.f1716d = j;
            this.f1713a = new CountDownLatch(1);
            this.f1714b = false;
            start();
        }

        private void m3044c() {
            AdvertisingIdClient advertisingIdClient = (AdvertisingIdClient) this.f1715c.get();
            if (advertisingIdClient != null) {
                advertisingIdClient.finish();
                this.f1714b = true;
            }
        }

        public void m3045a() {
            this.f1713a.countDown();
        }

        public boolean m3046b() {
            return this.f1714b;
        }

        public void run() {
            try {
                if (!this.f1713a.await(this.f1716d, TimeUnit.MILLISECONDS)) {
                    m3044c();
                }
            } catch (InterruptedException e) {
                m3044c();
            }
        }
    }

    static {
        zzoN = false;
    }

    public AdvertisingIdClient(Context context) {
        this(context, 30000);
    }

    public AdvertisingIdClient(Context context, long j) {
        this.zzoK = new Object();
        C0385p.m3549a((Object) context);
        this.mContext = context;
        this.zzoJ = false;
        this.zzoM = j;
    }

    public static Info getAdvertisingIdInfo(Context context) {
        AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(context, -1);
        try {
            advertisingIdClient.zzb(false);
            Info info = advertisingIdClient.getInfo();
            return info;
        } finally {
            advertisingIdClient.finish();
        }
    }

    public static void setShouldSkipGmsCoreVersionCheck(boolean z) {
        zzoN = z;
    }

    static C0417a zza(Context context, C0102d c0102d) {
        try {
            return C0419a.m3752a(c0102d.m3084a());
        } catch (InterruptedException e) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            IOException iOException = new IOException(th);
        }
    }

    private void zzaL() {
        synchronized (this.zzoK) {
            if (this.zzoL != null) {
                this.zzoL.m3045a();
                try {
                    this.zzoL.join();
                } catch (InterruptedException e) {
                }
            }
            if (this.zzoM > 0) {
                this.zzoL = new C0083a(this, this.zzoM);
            }
        }
    }

    static C0102d zzp(Context context) {
        try {
            context.getPackageManager().getPackageInfo(GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE, 0);
            if (zzoN) {
                Log.d("Ads", "Skipping gmscore version check");
                switch (C0100b.m3073a().m3075a(context)) {
                    case R.View_android_theme /*0*/:
                    case R.View_paddingStart /*2*/:
                        break;
                    default:
                        throw new IOException("Google Play services not available");
                }
            }
            try {
                GooglePlayServicesUtil.zzac(context);
            } catch (Throwable e) {
                throw new IOException(e);
            }
            ServiceConnection c0102d = new C0102d();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE);
            try {
                if (C0389b.m3613a().m3624a(context, intent, c0102d, 1)) {
                    return c0102d;
                }
                throw new IOException("Connection failure");
            } catch (Throwable e2) {
                IOException iOException = new IOException(e2);
            }
        } catch (NameNotFoundException e3) {
            throw new GooglePlayServicesNotAvailableException(9);
        }
    }

    protected void finalize() {
        finish();
        super.finalize();
    }

    public void finish() {
        C0385p.m3558c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.mContext == null || this.zzoH == null) {
                return;
            }
            try {
                if (this.zzoJ) {
                    C0389b.m3613a().m3622a(this.mContext, this.zzoH);
                }
            } catch (Throwable e) {
                Log.i("AdvertisingIdClient", "AdvertisingIdClient unbindService failed.", e);
            }
            this.zzoJ = false;
            this.zzoI = null;
            this.zzoH = null;
        }
    }

    public Info getInfo() {
        Info info;
        C0385p.m3558c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.zzoJ) {
                synchronized (this.zzoK) {
                    if (this.zzoL == null || !this.zzoL.m3046b()) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    zzb(false);
                    if (!this.zzoJ) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (Throwable e) {
                    Log.i("AdvertisingIdClient", "GMS remote exception ", e);
                    throw new IOException("Remote exception");
                } catch (Throwable e2) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e2);
                }
            }
            C0385p.m3549a(this.zzoH);
            C0385p.m3549a(this.zzoI);
            info = new Info(this.zzoI.m3744a(), this.zzoI.m3747a(true));
        }
        zzaL();
        return info;
    }

    public void start() {
        zzb(true);
    }

    protected void zzb(boolean z) {
        C0385p.m3558c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.zzoJ) {
                finish();
            }
            this.zzoH = zzp(this.mContext);
            this.zzoI = zza(this.mContext, this.zzoH);
            this.zzoJ = true;
            if (z) {
                zzaL();
            }
        }
    }
}
