package com.google.android.gms.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.measurement.C0453a;

/* renamed from: com.google.android.gms.gcm.d */
class C0400d {
    static C0453a f2060a;

    public static void m3658a(Context context, Intent intent) {
        C0400d.m3659a(context, "_nr", intent);
    }

    private static void m3659a(Context context, String str, Intent intent) {
        String stringExtra = intent.getStringExtra("gcm.a.campaign");
        if (Log.isLoggable("GcmAnalytics", 3)) {
            Log.d("GcmAnalytics", "Sending event=" + str + " campaign=" + stringExtra);
        }
        Bundle bundle = new Bundle();
        bundle.putString("nc", stringExtra);
        stringExtra = intent.getStringExtra("from");
        if (!TextUtils.isEmpty(stringExtra)) {
            if (stringExtra.startsWith("/topics/")) {
                bundle.putString("nt", stringExtra);
            } else {
                try {
                    Long.parseLong(stringExtra);
                    bundle.putString("nsid", stringExtra);
                } catch (NumberFormatException e) {
                    Log.d("GcmAnalytics", "Unrecognised from address: " + stringExtra);
                }
            }
        }
        try {
            (f2060a == null ? C0453a.m3928a(context) : f2060a).m3929a("gcm", str, bundle);
        } catch (NoClassDefFoundError e2) {
            Log.e("GcmAnalytics", "Unable to log event, missing GMS measurement library");
        }
    }

    public static void m3660b(Context context, Intent intent) {
        C0400d.m3659a(context, "_no", intent);
    }

    public static void m3661c(Context context, Intent intent) {
        C0400d.m3659a(context, "_nd", intent);
    }

    public static void m3662d(Context context, Intent intent) {
        C0400d.m3659a(context, "_nf", intent);
    }
}
