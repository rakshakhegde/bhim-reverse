package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.p004a.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.gcm.a */
public abstract class C0396a extends Service {
    private static boolean f2042d;
    private final Object f2043a;
    private int f2044b;
    private int f2045c;

    /* renamed from: com.google.android.gms.gcm.a.1 */
    class C03941 implements Runnable {
        final /* synthetic */ Intent f2038a;
        final /* synthetic */ C0396a f2039b;

        C03941(C0396a c0396a, Intent intent) {
            this.f2039b = c0396a;
            this.f2038a = intent;
        }

        public void run() {
            this.f2039b.m3638d(this.f2038a);
        }
    }

    /* renamed from: com.google.android.gms.gcm.a.2 */
    class C03952 extends AsyncTask<Void, Void, Void> {
        final /* synthetic */ Intent f2040a;
        final /* synthetic */ C0396a f2041b;

        C03952(C0396a c0396a, Intent intent) {
            this.f2041b = c0396a;
            this.f2040a = intent;
        }

        protected Void m3631a(Void... voidArr) {
            this.f2041b.m3638d(this.f2040a);
            return null;
        }

        protected /* synthetic */ Object doInBackground(Object[] objArr) {
            return m3631a((Void[]) objArr);
        }
    }

    static {
        f2042d = false;
    }

    public C0396a() {
        this.f2043a = new Object();
        this.f2045c = 0;
    }

    static boolean m3633a(Intent intent) {
        return f2042d && !TextUtils.isEmpty(intent.getStringExtra("gcm.a.campaign"));
    }

    static boolean m3634a(Bundle bundle) {
        return f2042d && !TextUtils.isEmpty(bundle.getString("gcm.a.campaign"));
    }

    private void m3635b() {
        synchronized (this.f2043a) {
            this.f2045c--;
            if (this.f2045c == 0) {
                m3645a(this.f2044b);
            }
        }
    }

    private void m3636b(Intent intent) {
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("com.google.android.gms.gcm.PENDING_INTENT");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (CanceledException e) {
                Log.e("GcmListenerService", "Notification pending intent canceled");
            }
        }
        C0400d.m3660b(this, intent);
    }

    private void m3637c(Intent intent) {
        if (VERSION.SDK_INT >= 11) {
            AsyncTask.THREAD_POOL_EXECUTOR.execute(new C03941(this, intent));
        } else {
            new C03952(this, intent).execute(new Void[0]);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3638d(android.content.Intent r4) {
        /*
        r3 = this;
        r1 = r4.getAction();	 Catch:{ all -> 0x004a }
        r0 = -1;
        r2 = r1.hashCode();	 Catch:{ all -> 0x004a }
        switch(r2) {
            case 214175003: goto L_0x003c;
            case 366519424: goto L_0x0032;
            default: goto L_0x000c;
        };	 Catch:{ all -> 0x004a }
    L_0x000c:
        switch(r0) {
            case 0: goto L_0x0046;
            case 1: goto L_0x004f;
            default: goto L_0x000f;
        };	 Catch:{ all -> 0x004a }
    L_0x000f:
        r0 = "GcmListenerService";
        r1 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004a }
        r1.<init>();	 Catch:{ all -> 0x004a }
        r2 = "Unknown intent action: ";
        r1 = r1.append(r2);	 Catch:{ all -> 0x004a }
        r2 = r4.getAction();	 Catch:{ all -> 0x004a }
        r1 = r1.append(r2);	 Catch:{ all -> 0x004a }
        r1 = r1.toString();	 Catch:{ all -> 0x004a }
        android.util.Log.d(r0, r1);	 Catch:{ all -> 0x004a }
    L_0x002b:
        r3.m3635b();	 Catch:{ all -> 0x004a }
        android.support.v4.p004a.WakefulBroadcastReceiver.m115a(r4);
        return;
    L_0x0032:
        r2 = "com.google.android.c2dm.intent.RECEIVE";
        r1 = r1.equals(r2);	 Catch:{ all -> 0x004a }
        if (r1 == 0) goto L_0x000c;
    L_0x003a:
        r0 = 0;
        goto L_0x000c;
    L_0x003c:
        r2 = "com.google.android.gms.gcm.NOTIFICATION_DISMISS";
        r1 = r1.equals(r2);	 Catch:{ all -> 0x004a }
        if (r1 == 0) goto L_0x000c;
    L_0x0044:
        r0 = 1;
        goto L_0x000c;
    L_0x0046:
        r3.m3639e(r4);	 Catch:{ all -> 0x004a }
        goto L_0x002b;
    L_0x004a:
        r0 = move-exception;
        android.support.v4.p004a.WakefulBroadcastReceiver.m115a(r4);
        throw r0;
    L_0x004f:
        com.google.android.gms.gcm.C0400d.m3661c(r3, r4);	 Catch:{ all -> 0x004a }
        goto L_0x002b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gcm.a.d(android.content.Intent):void");
    }

    private void m3639e(Intent intent) {
        String stringExtra = intent.getStringExtra("message_type");
        if (stringExtra == null) {
            stringExtra = "gcm";
        }
        Object obj = -1;
        switch (stringExtra.hashCode()) {
            case -2062414158:
                if (stringExtra.equals("deleted_messages")) {
                    obj = 1;
                    break;
                }
                break;
            case 102161:
                if (stringExtra.equals("gcm")) {
                    obj = null;
                    break;
                }
                break;
            case 814694033:
                if (stringExtra.equals("send_error")) {
                    obj = 3;
                    break;
                }
                break;
            case 814800675:
                if (stringExtra.equals("send_event")) {
                    obj = 2;
                    break;
                }
                break;
        }
        switch (obj) {
            case R.View_android_theme /*0*/:
                if (C0396a.m3633a(intent)) {
                    C0400d.m3658a(this, intent);
                }
                m3640f(intent);
            case R.View_android_focusable /*1*/:
                m3641a();
            case R.View_paddingStart /*2*/:
                m3642a(intent.getStringExtra("google.message_id"));
            case R.View_paddingEnd /*3*/:
                m3644a(intent.getStringExtra("google.message_id"), intent.getStringExtra(CLConstants.OUTPUT_KEY_ERROR));
            default:
                Log.w("GcmListenerService", "Received message with unknown type: " + stringExtra);
        }
    }

    private void m3640f(Intent intent) {
        Bundle extras = intent.getExtras();
        extras.remove("message_type");
        extras.remove("android.support.content.wakelockid");
        if (C0403e.m3671a(extras)) {
            if (C0403e.m3670a((Context) this)) {
                C0403e.m3674b(extras);
                if (C0396a.m3633a(intent)) {
                    C0400d.m3662d(this, intent);
                }
            } else {
                C0403e.m3665a((Context) this, getClass()).m3679c(extras);
                return;
            }
        }
        String string = extras.getString("from");
        extras.remove("from");
        m3643a(string, extras);
    }

    public void m3641a() {
    }

    public void m3642a(String str) {
    }

    public void m3643a(String str, Bundle bundle) {
    }

    public void m3644a(String str, String str2) {
    }

    boolean m3645a(int i) {
        return stopSelfResult(i);
    }

    public final IBinder onBind(Intent intent) {
        return null;
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.f2043a) {
            this.f2044b = i2;
            this.f2045c++;
        }
        if ("com.google.android.gms.gcm.NOTIFICATION_OPEN".equals(intent.getAction())) {
            m3636b(intent);
            m3635b();
            WakefulBroadcastReceiver.m115a(intent);
        } else {
            m3637c(intent);
        }
        return 3;
    }
}
