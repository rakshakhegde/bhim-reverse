package com.google.android.gms.gcm;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.support.v4.app.aa.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.MissingFormatArgumentException;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: com.google.android.gms.gcm.e */
class C0403e {
    static C0403e f2062a;
    private final Context f2063b;
    private final Class<? extends C0396a> f2064c;

    /* renamed from: com.google.android.gms.gcm.e.a */
    private class C0402a extends IllegalArgumentException {
        final /* synthetic */ C0403e f2061a;

        private C0402a(C0403e c0403e, String str) {
            this.f2061a = c0403e;
            super(str);
        }
    }

    private C0403e(Context context, Class<? extends C0396a> cls) {
        this.f2063b = context.getApplicationContext();
        this.f2064c = cls;
    }

    private int m3663a() {
        return (int) SystemClock.uptimeMillis();
    }

    private PendingIntent m3664a(Bundle bundle, PendingIntent pendingIntent) {
        Intent intent = new Intent("com.google.android.gms.gcm.NOTIFICATION_OPEN");
        m3668a(intent, bundle);
        intent.putExtra("com.google.android.gms.gcm.PENDING_INTENT", pendingIntent);
        return PendingIntent.getService(this.f2063b, m3663a(), intent, 1073741824);
    }

    static synchronized C0403e m3665a(Context context, Class<? extends C0396a> cls) {
        C0403e c0403e;
        synchronized (C0403e.class) {
            if (f2062a == null) {
                f2062a = new C0403e(context, cls);
            }
            c0403e = f2062a;
        }
        return c0403e;
    }

    static String m3666a(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    private String m3667a(String str) {
        return str.substring("gcm.n.".length());
    }

    private void m3668a(Intent intent, Bundle bundle) {
        intent.setClass(this.f2063b, this.f2064c);
        for (String str : bundle.keySet()) {
            if (str.startsWith("gcm.a.") || str.equals("from")) {
                intent.putExtra(str, bundle.getString(str));
            }
        }
    }

    private void m3669a(String str, Notification notification) {
        if (Log.isLoggable("GcmNotification", 3)) {
            Log.d("GcmNotification", "Showing notification");
        }
        NotificationManager notificationManager = (NotificationManager) this.f2063b.getSystemService("notification");
        if (TextUtils.isEmpty(str)) {
            str = "GCM-Notification:" + SystemClock.uptimeMillis();
        }
        notificationManager.notify(str, 0, notification);
    }

    static boolean m3670a(Context context) {
        if (((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return false;
        }
        int myPid = Process.myPid();
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (runningAppProcessInfo.pid == myPid) {
                return runningAppProcessInfo.importance == 100;
            }
        }
        return false;
    }

    static boolean m3671a(Bundle bundle) {
        return C0403e.m3666a(bundle, "gcm.n.icon") != null;
    }

    private int m3672b(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new C0402a("Missing icon", null);
        }
        Resources resources = this.f2063b.getResources();
        int identifier = resources.getIdentifier(str, "drawable", this.f2063b.getPackageName());
        if (identifier == 0) {
            identifier = resources.getIdentifier(str, "mipmap", this.f2063b.getPackageName());
            if (identifier == 0) {
                throw new C0402a("Icon resource not found: " + str, null);
            }
        }
        return identifier;
    }

    private String m3673b(Bundle bundle, String str) {
        Object a = C0403e.m3666a(bundle, str);
        if (!TextUtils.isEmpty(a)) {
            return a;
        }
        String a2 = C0403e.m3666a(bundle, str + "_loc_key");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        Resources resources = this.f2063b.getResources();
        int identifier = resources.getIdentifier(a2, "string", this.f2063b.getPackageName());
        if (identifier == 0) {
            throw new C0402a(m3667a(str + "_loc_key") + " resource not found: " + a2, null);
        }
        String a3 = C0403e.m3666a(bundle, str + "_loc_args");
        if (TextUtils.isEmpty(a3)) {
            return resources.getString(identifier);
        }
        try {
            JSONArray jSONArray = new JSONArray(a3);
            String[] strArr = new String[jSONArray.length()];
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = jSONArray.opt(i);
            }
            try {
                return resources.getString(identifier, strArr);
            } catch (MissingFormatArgumentException e) {
                throw new C0402a("Missing format argument for " + a2 + ": " + e, null);
            }
        } catch (JSONException e2) {
            throw new C0402a("Malformed " + m3667a(str + "_loc_args") + ": " + a3, null);
        }
    }

    static void m3674b(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        Iterator it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str.startsWith("gcm.n.")) {
                bundle2.putString(str.substring("gcm.n.".length()), bundle.getString(str));
                it.remove();
            } else if (str.startsWith("gcm.notification.")) {
                bundle2.putString(str.substring("gcm.notification.".length()), bundle.getString(str));
                it.remove();
            }
        }
        if (!bundle2.isEmpty()) {
            bundle.putBundle("notification", bundle2);
        }
    }

    private Uri m3675c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if ("default".equals(str)) {
            return RingtoneManager.getDefaultUri(2);
        }
        throw new C0402a("Invalid sound: " + str, null);
    }

    private Notification m3676d(Bundle bundle) {
        PendingIntent pendingIntent = null;
        CharSequence b = m3673b(bundle, "gcm.n.title");
        if (TextUtils.isEmpty(b)) {
            throw new C0402a("Missing title", null);
        }
        CharSequence b2 = m3673b(bundle, "gcm.n.body");
        int b3 = m3672b(C0403e.m3666a(bundle, "gcm.n.icon"));
        Object a = C0403e.m3666a(bundle, "gcm.n.color");
        Uri c = m3675c(C0403e.m3666a(bundle, "gcm.n.sound"));
        PendingIntent e = m3677e(bundle);
        if (C0396a.m3634a(bundle)) {
            e = m3664a(bundle, e);
            pendingIntent = m3678f(bundle);
        }
        NotificationCompat b4 = new NotificationCompat(this.f2063b).m231a(true).m224a(b3).m230a(b).m234b(b2);
        if (!TextUtils.isEmpty(a)) {
            b4.m232b(Color.parseColor(a));
        }
        if (c != null) {
            b4.m228a(c);
        }
        if (e != null) {
            b4.m226a(e);
        }
        if (pendingIntent != null) {
            b4.m233b(pendingIntent);
        }
        return b4.m223a();
    }

    private PendingIntent m3677e(Bundle bundle) {
        Object a = C0403e.m3666a(bundle, "gcm.n.click_action");
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        Intent intent = new Intent(a);
        intent.setPackage(this.f2063b.getPackageName());
        intent.setFlags(268435456);
        intent.putExtras(bundle);
        for (String str : bundle.keySet()) {
            if (str.startsWith("gcm.n.") || str.startsWith("gcm.notification.")) {
                intent.removeExtra(str);
            }
        }
        return PendingIntent.getActivity(this.f2063b, m3663a(), intent, 1073741824);
    }

    private PendingIntent m3678f(Bundle bundle) {
        Intent intent = new Intent("com.google.android.gms.gcm.NOTIFICATION_DISMISS");
        m3668a(intent, bundle);
        return PendingIntent.getService(this.f2063b, m3663a(), intent, 1073741824);
    }

    boolean m3679c(Bundle bundle) {
        try {
            m3669a(C0403e.m3666a(bundle, "gcm.n.tag"), m3676d(bundle));
            return true;
        } catch (C0402a e) {
            Log.w("GcmNotification", "Failed to show notification: " + e.getMessage());
            return false;
        }
    }
}
