package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import com.google.android.gms.iid.C0408a;
import com.google.android.gms.iid.C0415e;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: com.google.android.gms.gcm.c */
public class C0399c {
    public static int f2050a;
    public static int f2051b;
    public static int f2052c;
    static C0399c f2053d;
    private static final AtomicInteger f2054i;
    final Messenger f2055e;
    private Context f2056f;
    private PendingIntent f2057g;
    private Map<String, Handler> f2058h;
    private final BlockingQueue<Intent> f2059j;

    /* renamed from: com.google.android.gms.gcm.c.1 */
    class C03981 extends Handler {
        final /* synthetic */ C0399c f2049a;

        C03981(C0399c c0399c, Looper looper) {
            this.f2049a = c0399c;
            super(looper);
        }

        public void handleMessage(Message message) {
            if (message == null || !(message.obj instanceof Intent)) {
                Log.w("GCM", "Dropping invalid message");
            }
            Intent intent = (Intent) message.obj;
            if ("com.google.android.c2dm.intent.REGISTRATION".equals(intent.getAction())) {
                this.f2049a.f2059j.add(intent);
            } else if (!this.f2049a.m3654b(intent)) {
                intent.setPackage(this.f2049a.f2056f.getPackageName());
                this.f2049a.f2056f.sendBroadcast(intent);
            }
        }
    }

    static {
        f2050a = 5000000;
        f2051b = 6500000;
        f2052c = 7000000;
        f2054i = new AtomicInteger(1);
    }

    public C0399c() {
        this.f2059j = new LinkedBlockingQueue();
        this.f2058h = Collections.synchronizedMap(new HashMap());
        this.f2055e = new Messenger(new C03981(this, Looper.getMainLooper()));
    }

    public static synchronized C0399c m3648a(Context context) {
        C0399c c0399c;
        synchronized (C0399c.class) {
            if (f2053d == null) {
                f2053d = new C0399c();
                f2053d.f2056f = context.getApplicationContext();
            }
            c0399c = f2053d;
        }
        return c0399c;
    }

    private void m3650a(String str, String str2, long j, int i, Bundle bundle) {
        if (str == null) {
            throw new IllegalArgumentException("Missing 'to'");
        }
        Intent intent = new Intent("com.google.android.gcm.intent.SEND");
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        m3656a(intent);
        intent.setPackage(C0399c.m3653b(this.f2056f));
        intent.putExtra("google.to", str);
        intent.putExtra("google.message_id", str2);
        intent.putExtra("google.ttl", Long.toString(j));
        intent.putExtra("google.delay", Integer.toString(i));
        if (C0399c.m3653b(this.f2056f).contains(".gsf")) {
            Bundle bundle2 = new Bundle();
            for (String str3 : bundle.keySet()) {
                Object obj = bundle.get(str3);
                if (obj instanceof String) {
                    bundle2.putString("gcm." + str3, (String) obj);
                }
            }
            bundle2.putString("google.to", str);
            bundle2.putString("google.message_id", str2);
            C0408a.m3693b(this.f2056f).m3696b("GCM", "upstream", bundle2);
            return;
        }
        this.f2056f.sendOrderedBroadcast(intent, "com.google.android.gtalkservice.permission.GTALK_SERVICE");
    }

    public static String m3653b(Context context) {
        return C0415e.m3712a(context);
    }

    private boolean m3654b(Intent intent) {
        Object stringExtra = intent.getStringExtra("In-Reply-To");
        if (stringExtra == null && intent.hasExtra(CLConstants.OUTPUT_KEY_ERROR)) {
            stringExtra = intent.getStringExtra("google.message_id");
        }
        if (stringExtra != null) {
            Handler handler = (Handler) this.f2058h.remove(stringExtra);
            if (handler != null) {
                Message obtain = Message.obtain();
                obtain.obj = intent;
                return handler.sendMessage(obtain);
            }
        }
        return false;
    }

    public static int m3655c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(C0399c.m3653b(context), 0).versionCode;
        } catch (NameNotFoundException e) {
            return -1;
        }
    }

    synchronized void m3656a(Intent intent) {
        if (this.f2057g == null) {
            Intent intent2 = new Intent();
            intent2.setPackage("com.google.example.invalidpackage");
            this.f2057g = PendingIntent.getBroadcast(this.f2056f, 0, intent2, 0);
        }
        intent.putExtra("app", this.f2057g);
    }

    public void m3657a(String str, String str2, long j, Bundle bundle) {
        m3650a(str, str2, j, -1, bundle);
    }
}
