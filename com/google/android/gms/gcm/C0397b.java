package com.google.android.gms.gcm;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.iid.C0408a;
import java.util.regex.Pattern;

/* renamed from: com.google.android.gms.gcm.b */
public class C0397b {
    private static C0397b f2046a;
    private static final Pattern f2047c;
    private C0408a f2048b;

    static {
        f2047c = Pattern.compile("/topics/[a-zA-Z0-9-_.~%]{1,900}");
    }

    private C0397b(Context context) {
        this.f2048b = C0408a.m3693b(context);
    }

    public static synchronized C0397b m3646a(Context context) {
        C0397b c0397b;
        synchronized (C0397b.class) {
            if (f2046a == null) {
                f2046a = new C0397b(context);
            }
            c0397b = f2046a;
        }
        return c0397b;
    }

    public void m3647a(String str, String str2, Bundle bundle) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Invalid appInstanceToken: " + str);
        } else if (str2 == null || !f2047c.matcher(str2).matches()) {
            throw new IllegalArgumentException("Invalid topic name: " + str2);
        } else {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putString("gcm.topic", str2);
            this.f2048b.m3694a(str, str2, bundle);
        }
    }
}
