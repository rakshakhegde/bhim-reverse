package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.C0384o;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import org.npci.upi.security.pinactivitycomponent.R.R;

public final class ConnectionResult implements SafeParcelable {
    public static final Creator<ConnectionResult> CREATOR;
    public static final ConnectionResult f1717a;
    final int f1718b;
    private final int f1719c;
    private final PendingIntent f1720d;
    private final String f1721e;

    static {
        f1717a = new ConnectionResult(0);
        CREATOR = new C0106e();
    }

    public ConnectionResult(int i) {
        this(i, null, null);
    }

    ConnectionResult(int i, int i2, PendingIntent pendingIntent, String str) {
        this.f1718b = i;
        this.f1719c = i2;
        this.f1720d = pendingIntent;
        this.f1721e = str;
    }

    public ConnectionResult(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, null);
    }

    public ConnectionResult(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }

    static String m3047a(int i) {
        switch (i) {
            case R.View_android_theme /*0*/:
                return "SUCCESS";
            case R.View_android_focusable /*1*/:
                return "SERVICE_MISSING";
            case R.View_paddingStart /*2*/:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case R.View_paddingEnd /*3*/:
                return "SERVICE_DISABLED";
            case R.View_theme /*4*/:
                return "SIGN_IN_REQUIRED";
            case R.Toolbar_contentInsetStart /*5*/:
                return "INVALID_ACCOUNT";
            case R.Toolbar_contentInsetEnd /*6*/:
                return "RESOLUTION_REQUIRED";
            case R.Toolbar_contentInsetLeft /*7*/:
                return "NETWORK_ERROR";
            case R.Toolbar_contentInsetRight /*8*/:
                return "INTERNAL_ERROR";
            case R.Toolbar_popupTheme /*9*/:
                return "SERVICE_INVALID";
            case R.Toolbar_titleTextAppearance /*10*/:
                return "DEVELOPER_ERROR";
            case R.Toolbar_subtitleTextAppearance /*11*/:
                return "LICENSE_CHECK_FAILED";
            case R.Toolbar_titleMarginStart /*13*/:
                return "CANCELED";
            case R.Toolbar_titleMarginEnd /*14*/:
                return "TIMEOUT";
            case R.Toolbar_titleMarginTop /*15*/:
                return "INTERRUPTED";
            case R.Toolbar_titleMarginBottom /*16*/:
                return "API_UNAVAILABLE";
            case R.Toolbar_maxButtonHeight /*17*/:
                return "SIGN_IN_FAILED";
            case R.Toolbar_collapseIcon /*18*/:
                return "SERVICE_UPDATING";
            case R.Toolbar_collapseContentDescription /*19*/:
                return "SERVICE_MISSING_PERMISSION";
            default:
                return "UNKNOWN_ERROR_CODE(" + i + ")";
        }
    }

    public boolean m3048a() {
        return this.f1719c == 0;
    }

    public int m3049b() {
        return this.f1719c;
    }

    public PendingIntent m3050c() {
        return this.f1720d;
    }

    public String m3051d() {
        return this.f1721e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ConnectionResult)) {
            return false;
        }
        ConnectionResult connectionResult = (ConnectionResult) obj;
        return this.f1719c == connectionResult.f1719c && C0384o.m3548a(this.f1720d, connectionResult.f1720d) && C0384o.m3548a(this.f1721e, connectionResult.f1721e);
    }

    public int hashCode() {
        return C0384o.m3546a(Integer.valueOf(this.f1719c), this.f1720d, this.f1721e);
    }

    public String toString() {
        return C0384o.m3547a((Object) this).m3545a("statusCode", m3047a(this.f1719c)).m3545a("resolution", this.f1720d).m3545a("message", this.f1721e).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0106e.m3096a(this, parcel, i);
    }
}
