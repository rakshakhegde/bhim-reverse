package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.api.e */
public class C0099e implements Creator<Status> {
    static void m3070a(Status status, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, status.m3059e());
        C0386a.m3561a(parcel, 1000, status.m3057c());
        C0386a.m3568a(parcel, 2, status.m3056b(), false);
        C0386a.m3565a(parcel, 3, status.m3055a(), i, false);
        C0386a.m3560a(parcel, a);
    }

    public Status m3071a(Parcel parcel) {
        PendingIntent pendingIntent = null;
        int i = 0;
        int b = zza.m3582b(parcel);
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    str = zza.m3590h(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    pendingIntent = (PendingIntent) zza.m3579a(parcel, a, PendingIntent.CREATOR);
                    break;
                case 1000:
                    i2 = zza.m3586d(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new Status(i2, i, str, pendingIntent);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public Status[] m3072a(int i) {
        return new Status[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3071a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3072a(i);
    }
}
