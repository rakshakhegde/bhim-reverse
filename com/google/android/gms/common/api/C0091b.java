package com.google.android.gms.common.api;

import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.api.b */
public class C0091b {
    public static String m3060a(int i) {
        switch (i) {
            case -1:
                return "SUCCESS_CACHE";
            case R.View_android_theme /*0*/:
                return "SUCCESS";
            case R.View_android_focusable /*1*/:
                return "SERVICE_MISSING";
            case R.View_paddingStart /*2*/:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case R.View_paddingEnd /*3*/:
                return "SERVICE_DISABLED";
            case R.View_theme /*4*/:
                return "SIGN_IN_REQUIRED";
            case R.Toolbar_contentInsetStart /*5*/:
                return "INVALID_ACCOUNT";
            case R.Toolbar_contentInsetEnd /*6*/:
                return "RESOLUTION_REQUIRED";
            case R.Toolbar_contentInsetLeft /*7*/:
                return "NETWORK_ERROR";
            case R.Toolbar_contentInsetRight /*8*/:
                return "INTERNAL_ERROR";
            case R.Toolbar_popupTheme /*9*/:
                return "SERVICE_INVALID";
            case R.Toolbar_titleTextAppearance /*10*/:
                return "DEVELOPER_ERROR";
            case R.Toolbar_subtitleTextAppearance /*11*/:
                return "LICENSE_CHECK_FAILED";
            case R.Toolbar_titleMarginStart /*13*/:
                return "ERROR";
            case R.Toolbar_titleMarginEnd /*14*/:
                return "INTERRUPTED";
            case R.Toolbar_titleMarginTop /*15*/:
                return "TIMEOUT";
            case R.Toolbar_titleMarginBottom /*16*/:
                return "CANCELED";
            case R.Toolbar_maxButtonHeight /*17*/:
                return "API_NOT_CONNECTED";
            case 3000:
                return "AUTH_API_INVALID_CREDENTIALS";
            case 3001:
                return "AUTH_API_ACCESS_FORBIDDEN";
            case 3002:
                return "AUTH_API_CLIENT_ERROR";
            case 3003:
                return "AUTH_API_SERVER_ERROR";
            case 3004:
                return "AUTH_TOKEN_ERROR";
            case 3005:
                return "AUTH_URL_RESOLUTION";
            default:
                return "unknown status code: " + i;
        }
    }
}
