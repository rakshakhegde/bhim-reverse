package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.p010e.ArrayMap;
import android.view.View;
import com.google.android.gms.common.C0100b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C0090a.C0086a;
import com.google.android.gms.common.internal.C0355d;
import com.google.android.gms.common.internal.C0355d.C0354a;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.internal.C0442k;
import com.google.android.gms.internal.C0445m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.google.android.gms.common.api.c */
public abstract class C0097c {
    private static final Set<C0097c> f1761a;

    /* renamed from: com.google.android.gms.common.api.c.a */
    public static final class C0092a {
        private Account f1744a;
        private final Set<Scope> f1745b;
        private final Set<Scope> f1746c;
        private int f1747d;
        private View f1748e;
        private String f1749f;
        private String f1750g;
        private final Map<C0090a<?>, C0354a> f1751h;
        private final Context f1752i;
        private final Map<C0090a<?>, Object> f1753j;
        private int f1754k;
        private Looper f1755l;
        private C0100b f1756m;
        private C0086a<? extends Object, C0445m> f1757n;
        private final ArrayList<C0093b> f1758o;
        private final ArrayList<C0094c> f1759p;
        private C0445m f1760q;

        public C0092a(Context context) {
            this.f1745b = new HashSet();
            this.f1746c = new HashSet();
            this.f1751h = new ArrayMap();
            this.f1753j = new ArrayMap();
            this.f1754k = -1;
            this.f1756m = C0100b.m3073a();
            this.f1757n = C0442k.f2164c;
            this.f1758o = new ArrayList();
            this.f1759p = new ArrayList();
            this.f1752i = context;
            this.f1755l = context.getMainLooper();
            this.f1749f = context.getPackageName();
            this.f1750g = context.getClass().getName();
        }

        public C0355d m3061a() {
            if (this.f1753j.containsKey(C0442k.f2168g)) {
                C0385p.m3554a(this.f1760q == null, (Object) "SignIn.API can't be used in conjunction with requestServerAuthCode.");
                this.f1760q = (C0445m) this.f1753j.get(C0442k.f2168g);
            }
            return new C0355d(this.f1744a, this.f1745b, this.f1751h, this.f1747d, this.f1748e, this.f1749f, this.f1750g, this.f1760q != null ? this.f1760q : C0445m.f2178a);
        }
    }

    /* renamed from: com.google.android.gms.common.api.c.b */
    public interface C0093b {
        void m3062a(int i);

        void m3063a(Bundle bundle);
    }

    /* renamed from: com.google.android.gms.common.api.c.c */
    public interface C0094c {
        void m3064a(ConnectionResult connectionResult);
    }

    /* renamed from: com.google.android.gms.common.api.c.d */
    public interface C0095d {
    }

    /* renamed from: com.google.android.gms.common.api.c.e */
    public interface C0096e {
        void m3065a(ConnectionResult connectionResult);

        void m3066b(ConnectionResult connectionResult);
    }

    static {
        f1761a = Collections.newSetFromMap(new WeakHashMap());
    }
}
