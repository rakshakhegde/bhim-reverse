package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class Scope implements SafeParcelable {
    public static final Creator<Scope> CREATOR;
    final int f1728a;
    private final String f1729b;

    static {
        CREATOR = new C0098d();
    }

    Scope(int i, String str) {
        C0385p.m3552a(str, (Object) "scopeUri must not be null or empty");
        this.f1728a = i;
        this.f1729b = str;
    }

    public Scope(String str) {
        this(1, str);
    }

    public String m3053a() {
        return this.f1729b;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return this == obj ? true : !(obj instanceof Scope) ? false : this.f1729b.equals(((Scope) obj).f1729b);
    }

    public int hashCode() {
        return this.f1729b.hashCode();
    }

    public String toString() {
        return this.f1729b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0098d.m3067a(this, parcel, i);
    }
}
