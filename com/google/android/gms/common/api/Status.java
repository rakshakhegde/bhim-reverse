package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.C0384o;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class Status implements SafeParcelable {
    public static final Creator<Status> CREATOR;
    public static final Status f1730a;
    public static final Status f1731b;
    public static final Status f1732c;
    public static final Status f1733d;
    public static final Status f1734e;
    private final int f1735f;
    private final int f1736g;
    private final String f1737h;
    private final PendingIntent f1738i;

    static {
        f1730a = new Status(0);
        f1731b = new Status(14);
        f1732c = new Status(8);
        f1733d = new Status(15);
        f1734e = new Status(16);
        CREATOR = new C0099e();
    }

    public Status(int i) {
        this(i, null);
    }

    Status(int i, int i2, String str, PendingIntent pendingIntent) {
        this.f1735f = i;
        this.f1736g = i2;
        this.f1737h = str;
        this.f1738i = pendingIntent;
    }

    public Status(int i, String str) {
        this(1, i, str, null);
    }

    private String m3054f() {
        return this.f1737h != null ? this.f1737h : C0091b.m3060a(this.f1736g);
    }

    PendingIntent m3055a() {
        return this.f1738i;
    }

    public String m3056b() {
        return this.f1737h;
    }

    int m3057c() {
        return this.f1735f;
    }

    public boolean m3058d() {
        return this.f1736g <= 0;
    }

    public int describeContents() {
        return 0;
    }

    public int m3059e() {
        return this.f1736g;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.f1735f == status.f1735f && this.f1736g == status.f1736g && C0384o.m3548a(this.f1737h, status.f1737h) && C0384o.m3548a(this.f1738i, status.f1738i);
    }

    public int hashCode() {
        return C0384o.m3546a(Integer.valueOf(this.f1735f), Integer.valueOf(this.f1736g), this.f1737h, this.f1738i);
    }

    public String toString() {
        return C0384o.m3547a((Object) this).m3545a("statusCode", m3054f()).m3545a("resolution", this.f1738i).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0099e.m3070a(this, parcel, i);
    }
}
