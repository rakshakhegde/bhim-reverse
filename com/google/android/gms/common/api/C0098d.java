package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.api.d */
public class C0098d implements Creator<Scope> {
    static void m3067a(Scope scope, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, scope.f1728a);
        C0386a.m3568a(parcel, 2, scope.m3053a(), false);
        C0386a.m3560a(parcel, a);
    }

    public Scope m3068a(Parcel parcel) {
        int b = zza.m3582b(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    str = zza.m3590h(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new Scope(i, str);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public Scope[] m3069a(int i) {
        return new Scope[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3068a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3069a(i);
    }
}
