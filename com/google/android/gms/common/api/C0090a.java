package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.common.api.a */
public final class C0090a<O> {
    private final C0086a<?, O> f1739a;
    private final C0088c<?, O> f1740b;
    private final C0087b<?> f1741c;
    private final C0089d<?> f1742d;
    private final String f1743e;

    /* renamed from: com.google.android.gms.common.api.a.a */
    public static abstract class C0086a<T, O> {
    }

    /* renamed from: com.google.android.gms.common.api.a.b */
    public static final class C0087b<C> {
    }

    /* renamed from: com.google.android.gms.common.api.a.c */
    public interface C0088c<T, O> {
    }

    /* renamed from: com.google.android.gms.common.api.a.d */
    public static final class C0089d<C> {
    }

    public <C> C0090a(String str, C0086a<C, O> c0086a, C0087b<C> c0087b) {
        C0385p.m3550a((Object) c0086a, (Object) "Cannot construct an Api with a null ClientBuilder");
        C0385p.m3550a((Object) c0087b, (Object) "Cannot construct an Api with a null ClientKey");
        this.f1743e = str;
        this.f1739a = c0086a;
        this.f1740b = null;
        this.f1741c = c0087b;
        this.f1742d = null;
    }
}
