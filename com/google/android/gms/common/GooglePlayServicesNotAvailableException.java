package com.google.android.gms.common;

public final class GooglePlayServicesNotAvailableException extends Exception {
    public final int f1722a;

    public GooglePlayServicesNotAvailableException(int i) {
        this.f1722a = i;
    }
}
