package com.google.android.gms.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C0376k;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.b */
public class C0100b {
    public static final int f1762a;
    private static final C0100b f1763b;

    static {
        f1762a = GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        f1763b = new C0100b();
    }

    C0100b() {
    }

    public static C0100b m3073a() {
        return f1763b;
    }

    private String m3074a(Context context, String str) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("gcore_");
        stringBuilder.append(f1762a);
        stringBuilder.append("-");
        if (!TextUtils.isEmpty(str)) {
            stringBuilder.append(str);
        }
        stringBuilder.append("-");
        if (context != null) {
            stringBuilder.append(context.getPackageName());
        }
        stringBuilder.append("-");
        if (context != null) {
            try {
                stringBuilder.append(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
            } catch (NameNotFoundException e) {
            }
        }
        return stringBuilder.toString();
    }

    public int m3075a(Context context) {
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        return GooglePlayServicesUtil.zzd(context, isGooglePlayServicesAvailable) ? 18 : isGooglePlayServicesAvailable;
    }

    public Dialog m3076a(Activity activity, int i, int i2) {
        return GooglePlayServicesUtil.getErrorDialog(i, activity, i2);
    }

    public PendingIntent m3077a(Context context, int i, int i2) {
        return m3078a(context, i, i2, null);
    }

    public PendingIntent m3078a(Context context, int i, int i2, String str) {
        Intent a = m3079a(context, i, str);
        return a == null ? null : PendingIntent.getActivity(context, i2, a, 268435456);
    }

    public Intent m3079a(Context context, int i, String str) {
        switch (i) {
            case R.View_android_focusable /*1*/:
            case R.View_paddingStart /*2*/:
                return C0376k.m3449a(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, m3074a(context, str));
            case R.View_paddingEnd /*3*/:
                return C0376k.m3448a(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE);
            case R.AppCompatTheme_dialogTheme /*42*/:
                return C0376k.m3447a();
            default:
                return null;
        }
    }

    public final boolean m3080a(int i) {
        return GooglePlayServicesUtil.isUserRecoverableError(i);
    }
}
