package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.common.c */
public class C0101c extends DialogFragment {
    private Dialog ai;
    private OnCancelListener aj;

    public C0101c() {
        this.ai = null;
        this.aj = null;
    }

    public static C0101c m3081a(Dialog dialog, OnCancelListener onCancelListener) {
        C0101c c0101c = new C0101c();
        Dialog dialog2 = (Dialog) C0385p.m3550a((Object) dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        c0101c.ai = dialog2;
        if (onCancelListener != null) {
            c0101c.aj = onCancelListener;
        }
        return c0101c;
    }

    public void m3082a(FragmentManager fragmentManager, String str) {
        super.m370a(fragmentManager, str);
    }

    public Dialog m3083c(Bundle bundle) {
        if (this.ai == null) {
            m374b(false);
        }
        return this.ai;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.aj != null) {
            this.aj.onCancel(dialogInterface);
        }
    }
}
