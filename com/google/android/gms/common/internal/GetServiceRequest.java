package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.C0100b;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C0349l.C0350a;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collection;

public class GetServiceRequest implements SafeParcelable {
    public static final Creator<GetServiceRequest> CREATOR;
    final int f1903a;
    final int f1904b;
    int f1905c;
    String f1906d;
    IBinder f1907e;
    Scope[] f1908f;
    Bundle f1909g;
    Account f1910h;

    static {
        CREATOR = new C0358g();
    }

    public GetServiceRequest(int i) {
        this.f1903a = 2;
        this.f1905c = C0100b.f1762a;
        this.f1904b = i;
    }

    GetServiceRequest(int i, int i2, int i3, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account) {
        this.f1903a = i;
        this.f1904b = i2;
        this.f1905c = i3;
        this.f1906d = str;
        if (i < 2) {
            this.f1910h = m3327a(iBinder);
        } else {
            this.f1907e = iBinder;
            this.f1910h = account;
        }
        this.f1908f = scopeArr;
        this.f1909g = bundle;
    }

    private Account m3327a(IBinder iBinder) {
        return iBinder != null ? C0351a.m3339a(C0350a.m3338a(iBinder)) : null;
    }

    public GetServiceRequest m3328a(Account account) {
        this.f1910h = account;
        return this;
    }

    public GetServiceRequest m3329a(Bundle bundle) {
        this.f1909g = bundle;
        return this;
    }

    public GetServiceRequest m3330a(C0349l c0349l) {
        if (c0349l != null) {
            this.f1907e = c0349l.asBinder();
        }
        return this;
    }

    public GetServiceRequest m3331a(String str) {
        this.f1906d = str;
        return this;
    }

    public GetServiceRequest m3332a(Collection<Scope> collection) {
        this.f1908f = (Scope[]) collection.toArray(new Scope[collection.size()]);
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0358g.m3354a(this, parcel, i);
    }
}
