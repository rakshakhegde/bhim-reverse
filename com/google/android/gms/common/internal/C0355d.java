package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.view.View;
import com.google.android.gms.common.api.C0090a;
import com.google.android.gms.common.api.C0097c.C0092a;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.C0445m;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: com.google.android.gms.common.internal.d */
public final class C0355d {
    private final Account f1922a;
    private final Set<Scope> f1923b;
    private final Set<Scope> f1924c;
    private final Map<C0090a<?>, C0354a> f1925d;
    private final int f1926e;
    private final View f1927f;
    private final String f1928g;
    private final String f1929h;
    private final C0445m f1930i;

    /* renamed from: com.google.android.gms.common.internal.d.a */
    public static final class C0354a {
        public final Set<Scope> f1921a;
    }

    public C0355d(Account account, Set<Scope> set, Map<C0090a<?>, C0354a> map, int i, View view, String str, String str2, C0445m c0445m) {
        Map map2;
        this.f1922a = account;
        this.f1923b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        if (map == null) {
            map2 = Collections.EMPTY_MAP;
        }
        this.f1925d = map2;
        this.f1927f = view;
        this.f1926e = i;
        this.f1928g = str;
        this.f1929h = str2;
        this.f1930i = c0445m;
        Set hashSet = new HashSet(this.f1923b);
        for (C0354a c0354a : this.f1925d.values()) {
            hashSet.addAll(c0354a.f1921a);
        }
        this.f1924c = Collections.unmodifiableSet(hashSet);
    }

    public static C0355d m3345a(Context context) {
        return new C0092a(context).m3061a();
    }

    public Account m3346a() {
        return this.f1922a;
    }

    public Set<Scope> m3347b() {
        return this.f1924c;
    }

    public String m3348c() {
        return this.f1929h;
    }
}
