package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.google.android.gms.internal.C0430g;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.e */
public final class C0356e {
    public static final String m3349a(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case R.View_android_focusable /*1*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_title);
            case R.View_paddingStart /*2*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_update_title);
            case R.View_paddingEnd /*3*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_enable_title);
            case R.View_theme /*4*/:
            case R.Toolbar_contentInsetEnd /*6*/:
                return null;
            case R.Toolbar_contentInsetStart /*5*/:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_invalid_account_title);
            case R.Toolbar_contentInsetLeft /*7*/:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_network_error_title);
            case R.Toolbar_contentInsetRight /*8*/:
                Log.e("GoogleApiAvailability", "Internal error occurred. Please see logs for detailed information");
                return null;
            case R.Toolbar_popupTheme /*9*/:
                Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unsupported_title);
            case R.Toolbar_titleTextAppearance /*10*/:
                Log.e("GoogleApiAvailability", "Developer error occurred. Please see logs for detailed information");
                return null;
            case R.Toolbar_subtitleTextAppearance /*11*/:
                Log.e("GoogleApiAvailability", "The application is not licensed to the user.");
                return null;
            case R.Toolbar_titleMarginBottom /*16*/:
                Log.e("GoogleApiAvailability", "One of the API components you attempted to connect to is not available.");
                return null;
            case R.Toolbar_maxButtonHeight /*17*/:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_sign_in_failed_title);
            case R.Toolbar_collapseIcon /*18*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_updating_title);
            case R.AppCompatTheme_dialogTheme /*42*/:
                return resources.getString(com.google.android.gms.R.R.common_android_wear_update_title);
            default:
                Log.e("GoogleApiAvailability", "Unexpected error code " + i);
                return null;
        }
    }

    public static String m3350a(Context context, int i, String str) {
        Resources resources = context.getResources();
        switch (i) {
            case R.View_android_focusable /*1*/:
                if (C0430g.m3784a(resources)) {
                    return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_text_tablet, new Object[]{str});
                }
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_text_phone, new Object[]{str});
            case R.View_paddingStart /*2*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_update_text, new Object[]{str});
            case R.View_paddingEnd /*3*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_enable_text, new Object[]{str});
            case R.Toolbar_contentInsetStart /*5*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_invalid_account_text);
            case R.Toolbar_contentInsetLeft /*7*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_network_error_text);
            case R.Toolbar_popupTheme /*9*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unsupported_text, new Object[]{str});
            case R.Toolbar_titleMarginBottom /*16*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_api_unavailable_text, new Object[]{str});
            case R.Toolbar_maxButtonHeight /*17*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_sign_in_failed_text);
            case R.Toolbar_collapseIcon /*18*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_updating_text, new Object[]{str});
            case R.AppCompatTheme_dialogTheme /*42*/:
                return resources.getString(com.google.android.gms.R.R.common_android_wear_update_text, new Object[]{str});
            default:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unknown_issue);
        }
    }

    public static String m3351b(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case R.View_android_focusable /*1*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_button);
            case R.View_paddingStart /*2*/:
            case R.AppCompatTheme_dialogTheme /*42*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_update_button);
            case R.View_paddingEnd /*3*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_enable_button);
            default:
                return resources.getString(17039370);
        }
    }

    public static String m3352b(Context context, int i, String str) {
        Resources resources = context.getResources();
        switch (i) {
            case R.View_android_focusable /*1*/:
                if (C0430g.m3784a(resources)) {
                    return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_text_tablet, new Object[]{str});
                }
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_text_phone, new Object[]{str});
            case R.View_paddingStart /*2*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_update_text, new Object[]{str});
            case R.View_paddingEnd /*3*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_enable_text, new Object[]{str});
            case R.Toolbar_contentInsetStart /*5*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_invalid_account_text);
            case R.Toolbar_contentInsetLeft /*7*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_network_error_text);
            case R.Toolbar_popupTheme /*9*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unsupported_text, new Object[]{str});
            case R.Toolbar_titleMarginBottom /*16*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_api_unavailable_text, new Object[]{str});
            case R.Toolbar_maxButtonHeight /*17*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_sign_in_failed_text);
            case R.Toolbar_collapseIcon /*18*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_updating_text, new Object[]{str});
            case R.AppCompatTheme_dialogTheme /*42*/:
                return resources.getString(com.google.android.gms.R.R.common_android_wear_notification_needs_update_text, new Object[]{str});
            default:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unknown_issue);
        }
    }

    public static final String m3353c(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case R.View_android_focusable /*1*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_install_title);
            case R.View_paddingStart /*2*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_update_title);
            case R.View_paddingEnd /*3*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_enable_title);
            case R.View_theme /*4*/:
            case R.Toolbar_contentInsetEnd /*6*/:
                return null;
            case R.Toolbar_contentInsetStart /*5*/:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_invalid_account_title);
            case R.Toolbar_contentInsetLeft /*7*/:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_network_error_title);
            case R.Toolbar_contentInsetRight /*8*/:
                Log.e("GoogleApiAvailability", "Internal error occurred. Please see logs for detailed information");
                return null;
            case R.Toolbar_popupTheme /*9*/:
                Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_unsupported_title);
            case R.Toolbar_titleTextAppearance /*10*/:
                Log.e("GoogleApiAvailability", "Developer error occurred. Please see logs for detailed information");
                return null;
            case R.Toolbar_subtitleTextAppearance /*11*/:
                Log.e("GoogleApiAvailability", "The application is not licensed to the user.");
                return null;
            case R.Toolbar_titleMarginBottom /*16*/:
                Log.e("GoogleApiAvailability", "One of the API components you attempted to connect to is not available.");
                return null;
            case R.Toolbar_maxButtonHeight /*17*/:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_sign_in_failed_title);
            case R.Toolbar_collapseIcon /*18*/:
                return resources.getString(com.google.android.gms.R.R.common_google_play_services_updating_title);
            case R.AppCompatTheme_dialogTheme /*42*/:
                return resources.getString(com.google.android.gms.R.R.common_android_wear_update_title);
            default:
                Log.e("GoogleApiAvailability", "Unexpected error code " + i);
                return null;
        }
    }
}
