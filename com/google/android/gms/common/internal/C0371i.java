package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.ServiceConnection;

/* renamed from: com.google.android.gms.common.internal.i */
public abstract class C0371i {
    private static final Object f1971a;
    private static C0371i f1972b;

    static {
        f1971a = new Object();
    }

    public static C0371i m3421a(Context context) {
        synchronized (f1971a) {
            if (f1972b == null) {
                f1972b = new C0375j(context.getApplicationContext());
            }
        }
        return f1972b;
    }

    public abstract boolean m3422a(String str, ServiceConnection serviceConnection, String str2);

    public abstract void m3423b(String str, ServiceConnection serviceConnection, String str2);
}
