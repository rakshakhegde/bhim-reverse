package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.l */
public interface C0349l extends IInterface {

    /* renamed from: com.google.android.gms.common.internal.l.a */
    public static abstract class C0350a extends Binder implements C0349l {

        /* renamed from: com.google.android.gms.common.internal.l.a.a */
        private static class C0377a implements C0349l {
            private IBinder f1991a;

            C0377a(IBinder iBinder) {
                this.f1991a = iBinder;
            }

            public Account m3451a() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IAccountAccessor");
                    this.f1991a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    Account account = obtain2.readInt() != 0 ? (Account) Account.CREATOR.createFromParcel(obtain2) : null;
                    obtain2.recycle();
                    obtain.recycle();
                    return account;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f1991a;
            }
        }

        public static C0349l m3338a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C0349l)) ? new C0377a(iBinder) : (C0349l) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            switch (i) {
                case R.View_paddingStart /*2*/:
                    parcel.enforceInterface("com.google.android.gms.common.internal.IAccountAccessor");
                    Account a = m3337a();
                    parcel2.writeNoException();
                    if (a != null) {
                        parcel2.writeInt(1);
                        a.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 1598968902:
                    parcel2.writeString("com.google.android.gms.common.internal.IAccountAccessor");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    Account m3337a();
}
