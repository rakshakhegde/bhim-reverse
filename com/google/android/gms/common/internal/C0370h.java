package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.C0100b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C0097c.C0093b;
import com.google.android.gms.common.api.C0097c.C0094c;
import com.google.android.gms.common.api.C0097c.C0096e;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C0362m.C0363a;
import com.google.android.gms.common.internal.C0379n.C0381a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.h */
public abstract class C0370h<T extends IInterface> {
    public static final String[] f1951c;
    final Handler f1952a;
    protected AtomicInteger f1953b;
    private final Context f1954d;
    private final C0355d f1955e;
    private final Looper f1956f;
    private final C0371i f1957g;
    private final C0100b f1958h;
    private final Object f1959i;
    private C0379n f1960j;
    private C0096e f1961k;
    private T f1962l;
    private final ArrayList<C0359c<?>> f1963m;
    private C0365e f1964n;
    private int f1965o;
    private final Set<Scope> f1966p;
    private final Account f1967q;
    private final C0093b f1968r;
    private final C0094c f1969s;
    private final int f1970t;

    /* renamed from: com.google.android.gms.common.internal.h.c */
    protected abstract class C0359c<TListener> {
        private TListener f1935a;
        private boolean f1936b;
        final /* synthetic */ C0370h f1937d;

        public C0359c(C0370h c0370h, TListener tListener) {
            this.f1937d = c0370h;
            this.f1935a = tListener;
            this.f1936b = false;
        }

        protected abstract void m3357a(TListener tListener);

        protected abstract void m3358b();

        public void m3359c() {
            synchronized (this) {
                Object obj = this.f1935a;
                if (this.f1936b) {
                    Log.w("GmsClient", "Callback proxy " + this + " being reused. This is not safe.");
                }
            }
            if (obj != null) {
                try {
                    m3357a(obj);
                } catch (RuntimeException e) {
                    m3358b();
                    throw e;
                }
            }
            m3358b();
            synchronized (this) {
                this.f1936b = true;
            }
            m3360d();
        }

        public void m3360d() {
            m3361e();
            synchronized (this.f1937d.f1963m) {
                this.f1937d.f1963m.remove(this);
            }
        }

        public void m3361e() {
            synchronized (this) {
                this.f1935a = null;
            }
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.a */
    private abstract class C0360a extends C0359c<Boolean> {
        public final int f1938a;
        public final Bundle f1939b;
        final /* synthetic */ C0370h f1940c;

        protected C0360a(C0370h c0370h, int i, Bundle bundle) {
            this.f1940c = c0370h;
            super(c0370h, Boolean.valueOf(true));
            this.f1938a = i;
            this.f1939b = bundle;
        }

        protected abstract void m3362a(ConnectionResult connectionResult);

        protected void m3363a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool == null) {
                this.f1940c.m3390b(1, null);
                return;
            }
            switch (this.f1938a) {
                case R.View_android_theme /*0*/:
                    if (!m3365a()) {
                        this.f1940c.m3390b(1, null);
                        m3362a(new ConnectionResult(8, null));
                    }
                case R.Toolbar_titleTextAppearance /*10*/:
                    this.f1940c.m3390b(1, null);
                    throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                default:
                    this.f1940c.m3390b(1, null);
                    if (this.f1939b != null) {
                        pendingIntent = (PendingIntent) this.f1939b.getParcelable("pendingIntent");
                    }
                    m3362a(new ConnectionResult(this.f1938a, pendingIntent));
            }
        }

        protected /* synthetic */ void m3364a(Object obj) {
            m3363a((Boolean) obj);
        }

        protected abstract boolean m3365a();

        protected void m3366b() {
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.b */
    final class C0361b extends Handler {
        final /* synthetic */ C0370h f1941a;

        public C0361b(C0370h c0370h, Looper looper) {
            this.f1941a = c0370h;
            super(looper);
        }

        private void m3367a(Message message) {
            C0359c c0359c = (C0359c) message.obj;
            c0359c.m3358b();
            c0359c.m3360d();
        }

        private boolean m3368b(Message message) {
            return message.what == 2 || message.what == 1 || message.what == 5 || message.what == 6;
        }

        public void handleMessage(Message message) {
            if (this.f1941a.f1953b.get() != message.arg1) {
                if (m3368b(message)) {
                    m3367a(message);
                }
            } else if ((message.what == 1 || message.what == 5 || message.what == 6) && !this.f1941a.m3413g()) {
                m3367a(message);
            } else if (message.what == 3) {
                ConnectionResult connectionResult = new ConnectionResult(message.arg2, null);
                this.f1941a.f1961k.m3065a(connectionResult);
                this.f1941a.m3403a(connectionResult);
            } else if (message.what == 4) {
                this.f1941a.m3390b(4, null);
                if (this.f1941a.f1968r != null) {
                    this.f1941a.f1968r.m3062a(message.arg2);
                }
                this.f1941a.m3399a(message.arg2);
                this.f1941a.m3386a(4, 1, null);
            } else if (message.what == 2 && !this.f1941a.m3412f()) {
                m3367a(message);
            } else if (m3368b(message)) {
                ((C0359c) message.obj).m3359c();
            } else {
                Log.wtf("GmsClient", "Don't know how to handle message: " + message.what, new Exception());
            }
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.d */
    public static final class C0364d extends C0363a {
        private C0370h f1942a;
        private final int f1943b;

        public C0364d(C0370h c0370h, int i) {
            this.f1942a = c0370h;
            this.f1943b = i;
        }

        private void m3372a() {
            this.f1942a = null;
        }

        public void m3373a(int i, Bundle bundle) {
            C0385p.m3550a(this.f1942a, (Object) "onAccountValidationComplete can be called only once per call to validateAccount");
            this.f1942a.m3400a(i, bundle, this.f1943b);
            m3372a();
        }

        public void m3374a(int i, IBinder iBinder, Bundle bundle) {
            C0385p.m3550a(this.f1942a, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
            this.f1942a.m3401a(i, iBinder, bundle, this.f1943b);
            m3372a();
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.e */
    public final class C0365e implements ServiceConnection {
        final /* synthetic */ C0370h f1944a;
        private final int f1945b;

        public C0365e(C0370h c0370h, int i) {
            this.f1944a = c0370h;
            this.f1945b = i;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            C0385p.m3550a((Object) iBinder, (Object) "Expecting a valid IBinder");
            this.f1944a.f1960j = C0381a.m3544a(iBinder);
            this.f1944a.m3409c(this.f1945b);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.f1944a.f1952a.sendMessage(this.f1944a.f1952a.obtainMessage(4, this.f1945b, 1));
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.f */
    protected class C0366f implements C0096e {
        final /* synthetic */ C0370h f1946a;

        public C0366f(C0370h c0370h) {
            this.f1946a = c0370h;
        }

        public void m3375a(ConnectionResult connectionResult) {
            if (connectionResult.m3048a()) {
                this.f1946a.m3405a(null, this.f1946a.f1966p);
            } else if (this.f1946a.f1969s != null) {
                this.f1946a.f1969s.m3064a(connectionResult);
            }
        }

        public void m3376b(ConnectionResult connectionResult) {
            throw new IllegalStateException("Legacy GmsClient received onReportAccountValidation callback.");
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.g */
    protected final class C0367g extends C0360a {
        public final IBinder f1947e;
        final /* synthetic */ C0370h f1948f;

        public C0367g(C0370h c0370h, int i, IBinder iBinder, Bundle bundle) {
            this.f1948f = c0370h;
            super(c0370h, i, bundle);
            this.f1947e = iBinder;
        }

        protected void m3377a(ConnectionResult connectionResult) {
            if (this.f1948f.f1969s != null) {
                this.f1948f.f1969s.m3064a(connectionResult);
            }
            this.f1948f.m3403a(connectionResult);
        }

        protected boolean m3378a() {
            try {
                String interfaceDescriptor = this.f1947e.getInterfaceDescriptor();
                if (this.f1948f.m3406b().equals(interfaceDescriptor)) {
                    IInterface a = this.f1948f.m3396a(this.f1947e);
                    if (a == null || !this.f1948f.m3386a(2, 3, a)) {
                        return false;
                    }
                    Bundle k = this.f1948f.m3417k();
                    if (this.f1948f.f1968r != null) {
                        this.f1948f.f1968r.m3063a(k);
                    }
                    return true;
                }
                Log.e("GmsClient", "service descriptor mismatch: " + this.f1948f.m3406b() + " vs. " + interfaceDescriptor);
                return false;
            } catch (RemoteException e) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.h */
    protected final class C0368h extends C0360a {
        final /* synthetic */ C0370h f1949e;

        public C0368h(C0370h c0370h) {
            this.f1949e = c0370h;
            super(c0370h, 0, null);
        }

        protected void m3379a(ConnectionResult connectionResult) {
            this.f1949e.f1961k.m3065a(connectionResult);
            this.f1949e.m3403a(connectionResult);
        }

        protected boolean m3380a() {
            this.f1949e.f1961k.m3065a(ConnectionResult.f1717a);
            return true;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.h.i */
    protected final class C0369i extends C0360a {
        final /* synthetic */ C0370h f1950e;

        public C0369i(C0370h c0370h, int i, Bundle bundle) {
            this.f1950e = c0370h;
            super(c0370h, i, bundle);
        }

        protected void m3381a(ConnectionResult connectionResult) {
            this.f1950e.f1961k.m3066b(connectionResult);
            this.f1950e.m3403a(connectionResult);
        }

        protected boolean m3382a() {
            this.f1950e.f1961k.m3066b(ConnectionResult.f1717a);
            return true;
        }
    }

    static {
        f1951c = new String[]{"service_esmobile", "service_googleme"};
    }

    protected C0370h(Context context, Looper looper, int i, C0355d c0355d, C0093b c0093b, C0094c c0094c) {
        this(context, looper, C0371i.m3421a(context), C0100b.m3073a(), i, c0355d, (C0093b) C0385p.m3549a((Object) c0093b), (C0094c) C0385p.m3549a((Object) c0094c));
    }

    protected C0370h(Context context, Looper looper, C0371i c0371i, C0100b c0100b, int i, C0355d c0355d, C0093b c0093b, C0094c c0094c) {
        this.f1959i = new Object();
        this.f1963m = new ArrayList();
        this.f1965o = 1;
        this.f1953b = new AtomicInteger(0);
        this.f1954d = (Context) C0385p.m3550a((Object) context, (Object) "Context must not be null");
        this.f1956f = (Looper) C0385p.m3550a((Object) looper, (Object) "Looper must not be null");
        this.f1957g = (C0371i) C0385p.m3550a((Object) c0371i, (Object) "Supervisor must not be null");
        this.f1958h = (C0100b) C0385p.m3550a((Object) c0100b, (Object) "API availability must not be null");
        this.f1952a = new C0361b(this, looper);
        this.f1970t = i;
        this.f1955e = (C0355d) C0385p.m3549a((Object) c0355d);
        this.f1967q = c0355d.m3346a();
        this.f1966p = m3389b(c0355d.m3347b());
        this.f1968r = c0093b;
        this.f1969s = c0094c;
    }

    private boolean m3386a(int i, int i2, T t) {
        boolean z;
        synchronized (this.f1959i) {
            if (this.f1965o != i) {
                z = false;
            } else {
                m3390b(i2, t);
                z = true;
            }
        }
        return z;
    }

    private Set<Scope> m3389b(Set<Scope> set) {
        Set<Scope> a = m3398a((Set) set);
        if (a == null) {
            return a;
        }
        for (Scope contains : a) {
            if (!set.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3390b(int r5, T r6) {
        /*
        r4 = this;
        r0 = 1;
        r1 = 0;
        r2 = 3;
        if (r5 != r2) goto L_0x001d;
    L_0x0005:
        r3 = r0;
    L_0x0006:
        if (r6 == 0) goto L_0x001f;
    L_0x0008:
        r2 = r0;
    L_0x0009:
        if (r3 != r2) goto L_0x0021;
    L_0x000b:
        com.google.android.gms.common.internal.C0385p.m3556b(r0);
        r1 = r4.f1959i;
        monitor-enter(r1);
        r4.f1965o = r5;	 Catch:{ all -> 0x0027 }
        r4.f1962l = r6;	 Catch:{ all -> 0x0027 }
        r4.m3402a(r5, r6);	 Catch:{ all -> 0x0027 }
        switch(r5) {
            case 1: goto L_0x002e;
            case 2: goto L_0x0023;
            case 3: goto L_0x002a;
            default: goto L_0x001b;
        };	 Catch:{ all -> 0x0027 }
    L_0x001b:
        monitor-exit(r1);	 Catch:{ all -> 0x0027 }
        return;
    L_0x001d:
        r3 = r1;
        goto L_0x0006;
    L_0x001f:
        r2 = r1;
        goto L_0x0009;
    L_0x0021:
        r0 = r1;
        goto L_0x000b;
    L_0x0023:
        r4.m3394o();	 Catch:{ all -> 0x0027 }
        goto L_0x001b;
    L_0x0027:
        r0 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x0027 }
        throw r0;
    L_0x002a:
        r4.m3410d();	 Catch:{ all -> 0x0027 }
        goto L_0x001b;
    L_0x002e:
        r4.m3395p();	 Catch:{ all -> 0x0027 }
        goto L_0x001b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.h.b(int, android.os.IInterface):void");
    }

    private void m3394o() {
        if (this.f1964n != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect() for " + m3397a());
            this.f1957g.m3423b(m3397a(), this.f1964n, m3408c());
            this.f1953b.incrementAndGet();
        }
        this.f1964n = new C0365e(this, this.f1953b.get());
        if (!this.f1957g.m3422a(m3397a(), this.f1964n, m3408c())) {
            Log.e("GmsClient", "unable to connect to service: " + m3397a());
            this.f1952a.sendMessage(this.f1952a.obtainMessage(3, this.f1953b.get(), 9));
        }
    }

    private void m3395p() {
        if (this.f1964n != null) {
            this.f1957g.m3423b(m3397a(), this.f1964n, m3408c());
            this.f1964n = null;
        }
    }

    protected abstract T m3396a(IBinder iBinder);

    protected abstract String m3397a();

    protected Set<Scope> m3398a(Set<Scope> set) {
        return set;
    }

    protected void m3399a(int i) {
    }

    protected void m3400a(int i, Bundle bundle, int i2) {
        this.f1952a.sendMessage(this.f1952a.obtainMessage(5, i2, -1, new C0369i(this, i, bundle)));
    }

    protected void m3401a(int i, IBinder iBinder, Bundle bundle, int i2) {
        this.f1952a.sendMessage(this.f1952a.obtainMessage(1, i2, -1, new C0367g(this, i, iBinder, bundle)));
    }

    protected void m3402a(int i, T t) {
    }

    protected void m3403a(ConnectionResult connectionResult) {
    }

    public void m3404a(C0096e c0096e) {
        this.f1961k = (C0096e) C0385p.m3550a((Object) c0096e, (Object) "Connection progress callbacks cannot be null.");
        m3390b(2, null);
    }

    public void m3405a(C0349l c0349l, Set<Scope> set) {
        try {
            GetServiceRequest a = new GetServiceRequest(this.f1970t).m3331a(this.f1954d.getPackageName()).m3329a(m3415i());
            if (set != null) {
                a.m3332a((Collection) set);
            }
            if (m3419m()) {
                a.m3328a(m3414h()).m3330a(c0349l);
            } else if (m3420n()) {
                a.m3328a(this.f1967q);
            }
            this.f1960j.m3466a(new C0364d(this, this.f1953b.get()), a);
        } catch (DeadObjectException e) {
            Log.w("GmsClient", "service died");
            m3407b(1);
        } catch (Throwable e2) {
            Log.w("GmsClient", "Remote exception occurred", e2);
        }
    }

    protected abstract String m3406b();

    public void m3407b(int i) {
        this.f1952a.sendMessage(this.f1952a.obtainMessage(4, this.f1953b.get(), i));
    }

    protected final String m3408c() {
        return this.f1955e.m3348c();
    }

    protected void m3409c(int i) {
        this.f1952a.sendMessage(this.f1952a.obtainMessage(6, i, -1, new C0368h(this)));
    }

    protected void m3410d() {
    }

    public void m3411e() {
        int a = this.f1958h.m3075a(this.f1954d);
        if (a != 0) {
            m3390b(1, null);
            this.f1961k = new C0366f(this);
            this.f1952a.sendMessage(this.f1952a.obtainMessage(3, this.f1953b.get(), a));
            return;
        }
        m3404a(new C0366f(this));
    }

    public boolean m3412f() {
        boolean z;
        synchronized (this.f1959i) {
            z = this.f1965o == 3;
        }
        return z;
    }

    public boolean m3413g() {
        boolean z;
        synchronized (this.f1959i) {
            z = this.f1965o == 2;
        }
        return z;
    }

    public final Account m3414h() {
        return this.f1967q != null ? this.f1967q : new Account("<<default account>>", "com.google");
    }

    protected Bundle m3415i() {
        return new Bundle();
    }

    protected final void m3416j() {
        if (!m3412f()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public Bundle m3417k() {
        return null;
    }

    public final T m3418l() {
        T t;
        synchronized (this.f1959i) {
            if (this.f1965o == 4) {
                throw new DeadObjectException();
            }
            m3416j();
            C0385p.m3554a(this.f1962l != null, (Object) "Client is connected but service is null");
            t = this.f1962l;
        }
        return t;
    }

    public boolean m3419m() {
        return false;
    }

    public boolean m3420n() {
        return false;
    }
}
