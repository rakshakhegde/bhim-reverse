package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.b */
public class C0352b implements Creator<ValidateAccountRequest> {
    static void m3341a(ValidateAccountRequest validateAccountRequest, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, validateAccountRequest.f1911a);
        C0386a.m3561a(parcel, 2, validateAccountRequest.m3333a());
        C0386a.m3564a(parcel, 3, validateAccountRequest.f1912b, false);
        C0386a.m3570a(parcel, 4, validateAccountRequest.m3334b(), i, false);
        C0386a.m3563a(parcel, 5, validateAccountRequest.m3336d(), false);
        C0386a.m3568a(parcel, 6, validateAccountRequest.m3335c(), false);
        C0386a.m3560a(parcel, a);
    }

    public ValidateAccountRequest m3342a(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = zza.m3582b(parcel);
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i2 = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    iBinder = zza.m3591i(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    scopeArr = (Scope[]) zza.m3584b(parcel, a, Scope.CREATOR);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    bundle = zza.m3592j(parcel, a);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    str = zza.m3590h(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ValidateAccountRequest(i2, i, iBinder, scopeArr, bundle, str);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public ValidateAccountRequest[] m3343a(int i) {
        return new ValidateAccountRequest[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3342a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3343a(i);
    }
}
