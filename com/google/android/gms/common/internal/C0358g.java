package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.g */
public class C0358g implements Creator<GetServiceRequest> {
    static void m3354a(GetServiceRequest getServiceRequest, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, getServiceRequest.f1903a);
        C0386a.m3561a(parcel, 2, getServiceRequest.f1904b);
        C0386a.m3561a(parcel, 3, getServiceRequest.f1905c);
        C0386a.m3568a(parcel, 4, getServiceRequest.f1906d, false);
        C0386a.m3564a(parcel, 5, getServiceRequest.f1907e, false);
        C0386a.m3570a(parcel, 6, getServiceRequest.f1908f, i, false);
        C0386a.m3563a(parcel, 7, getServiceRequest.f1909g, false);
        C0386a.m3565a(parcel, 8, getServiceRequest.f1910h, i, false);
        C0386a.m3560a(parcel, a);
    }

    public GetServiceRequest m3355a(Parcel parcel) {
        int i = 0;
        Account account = null;
        int b = zza.m3582b(parcel);
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i3 = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    i2 = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingEnd /*3*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    str = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    iBinder = zza.m3591i(parcel, a);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    scopeArr = (Scope[]) zza.m3584b(parcel, a, Scope.CREATOR);
                    break;
                case R.Toolbar_contentInsetLeft /*7*/:
                    bundle = zza.m3592j(parcel, a);
                    break;
                case R.Toolbar_contentInsetRight /*8*/:
                    account = (Account) zza.m3579a(parcel, a, Account.CREATOR);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new GetServiceRequest(i3, i2, i, str, iBinder, scopeArr, bundle, account);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public GetServiceRequest[] m3356a(int i) {
        return new GetServiceRequest[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3355a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3356a(i);
    }
}
