package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.stats.C0389b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.internal.j */
final class C0375j extends C0371i implements Callback {
    private final HashMap<C0372a, C0374b> f1984a;
    private final Context f1985b;
    private final Handler f1986c;
    private final C0389b f1987d;
    private final long f1988e;

    /* renamed from: com.google.android.gms.common.internal.j.a */
    private static final class C0372a {
        private final String f1973a;
        private final ComponentName f1974b;

        public C0372a(String str) {
            this.f1973a = C0385p.m3551a(str);
            this.f1974b = null;
        }

        public Intent m3424a() {
            return this.f1973a != null ? new Intent(this.f1973a).setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE) : new Intent().setComponent(this.f1974b);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0372a)) {
                return false;
            }
            C0372a c0372a = (C0372a) obj;
            return C0384o.m3548a(this.f1973a, c0372a.f1973a) && C0384o.m3548a(this.f1974b, c0372a.f1974b);
        }

        public int hashCode() {
            return C0384o.m3546a(this.f1973a, this.f1974b);
        }

        public String toString() {
            return this.f1973a == null ? this.f1974b.flattenToString() : this.f1973a;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.j.b */
    private final class C0374b {
        final /* synthetic */ C0375j f1976a;
        private final C0373a f1977b;
        private final Set<ServiceConnection> f1978c;
        private int f1979d;
        private boolean f1980e;
        private IBinder f1981f;
        private final C0372a f1982g;
        private ComponentName f1983h;

        /* renamed from: com.google.android.gms.common.internal.j.b.a */
        public class C0373a implements ServiceConnection {
            final /* synthetic */ C0374b f1975a;

            public C0373a(C0374b c0374b) {
                this.f1975a = c0374b;
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                synchronized (this.f1975a.f1976a.f1984a) {
                    this.f1975a.f1981f = iBinder;
                    this.f1975a.f1983h = componentName;
                    for (ServiceConnection onServiceConnected : this.f1975a.f1978c) {
                        onServiceConnected.onServiceConnected(componentName, iBinder);
                    }
                    this.f1975a.f1979d = 1;
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                synchronized (this.f1975a.f1976a.f1984a) {
                    this.f1975a.f1981f = null;
                    this.f1975a.f1983h = componentName;
                    for (ServiceConnection onServiceDisconnected : this.f1975a.f1978c) {
                        onServiceDisconnected.onServiceDisconnected(componentName);
                    }
                    this.f1975a.f1979d = 2;
                }
            }
        }

        public C0374b(C0375j c0375j, C0372a c0372a) {
            this.f1976a = c0375j;
            this.f1982g = c0372a;
            this.f1977b = new C0373a(this);
            this.f1978c = new HashSet();
            this.f1979d = 2;
        }

        public void m3430a(ServiceConnection serviceConnection, String str) {
            this.f1976a.f1987d.m3623a(this.f1976a.f1985b, serviceConnection, str, this.f1982g.m3424a());
            this.f1978c.add(serviceConnection);
        }

        public void m3431a(String str) {
            this.f1979d = 3;
            this.f1980e = this.f1976a.f1987d.m3625a(this.f1976a.f1985b, str, this.f1982g.m3424a(), this.f1977b, 129);
            if (!this.f1980e) {
                this.f1979d = 2;
                try {
                    this.f1976a.f1987d.m3622a(this.f1976a.f1985b, this.f1977b);
                } catch (IllegalArgumentException e) {
                }
            }
        }

        public boolean m3432a() {
            return this.f1980e;
        }

        public boolean m3433a(ServiceConnection serviceConnection) {
            return this.f1978c.contains(serviceConnection);
        }

        public int m3434b() {
            return this.f1979d;
        }

        public void m3435b(ServiceConnection serviceConnection, String str) {
            this.f1976a.f1987d.m3626b(this.f1976a.f1985b, serviceConnection);
            this.f1978c.remove(serviceConnection);
        }

        public void m3436b(String str) {
            this.f1976a.f1987d.m3622a(this.f1976a.f1985b, this.f1977b);
            this.f1980e = false;
            this.f1979d = 2;
        }

        public boolean m3437c() {
            return this.f1978c.isEmpty();
        }

        public IBinder m3438d() {
            return this.f1981f;
        }

        public ComponentName m3439e() {
            return this.f1983h;
        }
    }

    C0375j(Context context) {
        this.f1984a = new HashMap();
        this.f1985b = context.getApplicationContext();
        this.f1986c = new Handler(context.getMainLooper(), this);
        this.f1987d = C0389b.m3613a();
        this.f1988e = 5000;
    }

    private boolean m3441a(C0372a c0372a, ServiceConnection serviceConnection, String str) {
        boolean a;
        C0385p.m3550a((Object) serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f1984a) {
            C0374b c0374b = (C0374b) this.f1984a.get(c0372a);
            if (c0374b != null) {
                this.f1986c.removeMessages(0, c0374b);
                if (!c0374b.m3433a(serviceConnection)) {
                    c0374b.m3430a(serviceConnection, str);
                    switch (c0374b.m3434b()) {
                        case R.View_android_focusable /*1*/:
                            serviceConnection.onServiceConnected(c0374b.m3439e(), c0374b.m3438d());
                            break;
                        case R.View_paddingStart /*2*/:
                            c0374b.m3431a(str);
                            break;
                        default:
                            break;
                    }
                }
                throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  config=" + c0372a);
            }
            c0374b = new C0374b(this, c0372a);
            c0374b.m3430a(serviceConnection, str);
            c0374b.m3431a(str);
            this.f1984a.put(c0372a, c0374b);
            a = c0374b.m3432a();
        }
        return a;
    }

    private void m3443b(C0372a c0372a, ServiceConnection serviceConnection, String str) {
        C0385p.m3550a((Object) serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f1984a) {
            C0374b c0374b = (C0374b) this.f1984a.get(c0372a);
            if (c0374b == null) {
                throw new IllegalStateException("Nonexistent connection status for service config: " + c0372a);
            } else if (c0374b.m3433a(serviceConnection)) {
                c0374b.m3435b(serviceConnection, str);
                if (c0374b.m3437c()) {
                    this.f1986c.sendMessageDelayed(this.f1986c.obtainMessage(0, c0374b), this.f1988e);
                }
            } else {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  config=" + c0372a);
            }
        }
    }

    public boolean m3445a(String str, ServiceConnection serviceConnection, String str2) {
        return m3441a(new C0372a(str), serviceConnection, str2);
    }

    public void m3446b(String str, ServiceConnection serviceConnection, String str2) {
        m3443b(new C0372a(str), serviceConnection, str2);
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case R.View_android_theme /*0*/:
                C0374b c0374b = (C0374b) message.obj;
                synchronized (this.f1984a) {
                    if (c0374b.m3437c()) {
                        if (c0374b.m3432a()) {
                            c0374b.m3436b("GmsClientSupervisor");
                        }
                        this.f1984a.remove(c0374b.f1982g);
                    }
                    break;
                }
                return true;
            default:
                return false;
        }
    }
}
