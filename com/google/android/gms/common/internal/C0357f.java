package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

/* renamed from: com.google.android.gms.common.internal.f */
public class C0357f implements OnClickListener {
    private final Activity f1931a;
    private final Fragment f1932b;
    private final Intent f1933c;
    private final int f1934d;

    public C0357f(Activity activity, Intent intent, int i) {
        this.f1931a = activity;
        this.f1932b = null;
        this.f1933c = intent;
        this.f1934d = i;
    }

    public C0357f(Fragment fragment, Intent intent, int i) {
        this.f1931a = null;
        this.f1932b = fragment;
        this.f1933c = intent;
        this.f1934d = i;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            if (this.f1933c != null && this.f1932b != null) {
                this.f1932b.m147a(this.f1933c, this.f1934d);
            } else if (this.f1933c != null) {
                this.f1931a.startActivityForResult(this.f1933c, this.f1934d);
            }
            dialogInterface.dismiss();
        } catch (ActivityNotFoundException e) {
            Log.e("SettingsRedirect", "Can't redirect to app settings for Google Play services");
        }
    }
}
