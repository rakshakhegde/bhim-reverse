package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.google.android.gms.common.internal.safeparcel.a */
public class C0386a {
    public static int m3559a(Parcel parcel) {
        return C0386a.m3573b(parcel, 20293);
    }

    public static void m3560a(Parcel parcel, int i) {
        C0386a.m3575c(parcel, i);
    }

    public static void m3561a(Parcel parcel, int i, int i2) {
        C0386a.m3574b(parcel, i, 4);
        parcel.writeInt(i2);
    }

    public static void m3562a(Parcel parcel, int i, long j) {
        C0386a.m3574b(parcel, i, 8);
        parcel.writeLong(j);
    }

    public static void m3563a(Parcel parcel, int i, Bundle bundle, boolean z) {
        if (bundle != null) {
            int b = C0386a.m3573b(parcel, i);
            parcel.writeBundle(bundle);
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3564a(Parcel parcel, int i, IBinder iBinder, boolean z) {
        if (iBinder != null) {
            int b = C0386a.m3573b(parcel, i);
            parcel.writeStrongBinder(iBinder);
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3565a(Parcel parcel, int i, Parcelable parcelable, int i2, boolean z) {
        if (parcelable != null) {
            int b = C0386a.m3573b(parcel, i);
            parcelable.writeToParcel(parcel, i2);
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3566a(Parcel parcel, int i, Float f, boolean z) {
        if (f != null) {
            C0386a.m3574b(parcel, i, 4);
            parcel.writeFloat(f.floatValue());
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3567a(Parcel parcel, int i, Long l, boolean z) {
        if (l != null) {
            C0386a.m3574b(parcel, i, 8);
            parcel.writeLong(l.longValue());
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3568a(Parcel parcel, int i, String str, boolean z) {
        if (str != null) {
            int b = C0386a.m3573b(parcel, i);
            parcel.writeString(str);
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3569a(Parcel parcel, int i, boolean z) {
        C0386a.m3574b(parcel, i, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    public static <T extends Parcelable> void m3570a(Parcel parcel, int i, T[] tArr, int i2, boolean z) {
        if (tArr != null) {
            int b = C0386a.m3573b(parcel, i);
            parcel.writeInt(r3);
            for (Parcelable parcelable : tArr) {
                if (parcelable == null) {
                    parcel.writeInt(0);
                } else {
                    C0386a.m3572a(parcel, parcelable, i2);
                }
            }
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    public static void m3571a(Parcel parcel, int i, String[] strArr, boolean z) {
        if (strArr != null) {
            int b = C0386a.m3573b(parcel, i);
            parcel.writeStringArray(strArr);
            C0386a.m3575c(parcel, b);
        } else if (z) {
            C0386a.m3574b(parcel, i, 0);
        }
    }

    private static <T extends Parcelable> void m3572a(Parcel parcel, T t, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        t.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }

    private static int m3573b(Parcel parcel, int i) {
        parcel.writeInt(-65536 | i);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    private static void m3574b(Parcel parcel, int i, int i2) {
        if (i2 >= 65535) {
            parcel.writeInt(-65536 | i);
            parcel.writeInt(i2);
            return;
        }
        parcel.writeInt((i2 << 16) | i);
    }

    private static void m3575c(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        int i2 = dataPosition - i;
        parcel.setDataPosition(i - 4);
        parcel.writeInt(i2);
        parcel.setDataPosition(dataPosition);
    }
}
