package com.google.android.gms.common.internal;

import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtil;

/* renamed from: com.google.android.gms.common.internal.k */
public class C0376k {
    private static final Uri f1989a;
    private static final Uri f1990b;

    static {
        f1989a = Uri.parse("http://plus.google.com/");
        f1990b = f1989a.buildUpon().appendPath("circles").appendPath("find").build();
    }

    public static Intent m3447a() {
        Intent intent = new Intent("com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION");
        intent.setPackage("com.google.android.wearable.app");
        return intent;
    }

    public static Intent m3448a(String str) {
        Uri fromParts = Uri.fromParts("package", str, null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        return intent;
    }

    public static Intent m3449a(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(C0376k.m3450b(str, str2));
        intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE);
        intent.addFlags(524288);
        return intent;
    }

    private static Uri m3450b(String str, String str2) {
        Builder appendQueryParameter = Uri.parse("market://details").buildUpon().appendQueryParameter("id", str);
        if (!TextUtils.isEmpty(str2)) {
            appendQueryParameter.appendQueryParameter("pcampaignid", str2);
        }
        return appendQueryParameter.build();
    }
}
