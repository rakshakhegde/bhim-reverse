package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class ValidateAccountRequest implements SafeParcelable {
    public static final Creator<ValidateAccountRequest> CREATOR;
    final int f1911a;
    final IBinder f1912b;
    private final int f1913c;
    private final Scope[] f1914d;
    private final Bundle f1915e;
    private final String f1916f;

    static {
        CREATOR = new C0352b();
    }

    ValidateAccountRequest(int i, int i2, IBinder iBinder, Scope[] scopeArr, Bundle bundle, String str) {
        this.f1911a = i;
        this.f1913c = i2;
        this.f1912b = iBinder;
        this.f1914d = scopeArr;
        this.f1915e = bundle;
        this.f1916f = str;
    }

    public int m3333a() {
        return this.f1913c;
    }

    public Scope[] m3334b() {
        return this.f1914d;
    }

    public String m3335c() {
        return this.f1916f;
    }

    public Bundle m3336d() {
        return this.f1915e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0352b.m3341a(this, parcel, i);
    }
}
