package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: com.google.android.gms.common.internal.o */
public final class C0384o {

    /* renamed from: com.google.android.gms.common.internal.o.a */
    public static final class C0383a {
        private final List<String> f1994a;
        private final Object f1995b;

        private C0383a(Object obj) {
            this.f1995b = C0385p.m3549a(obj);
            this.f1994a = new ArrayList();
        }

        public C0383a m3545a(String str, Object obj) {
            this.f1994a.add(((String) C0385p.m3549a((Object) str)) + "=" + String.valueOf(obj));
            return this;
        }

        public String toString() {
            StringBuilder append = new StringBuilder(100).append(this.f1995b.getClass().getSimpleName()).append('{');
            int size = this.f1994a.size();
            for (int i = 0; i < size; i++) {
                append.append((String) this.f1994a.get(i));
                if (i < size - 1) {
                    append.append(", ");
                }
            }
            return append.append('}').toString();
        }
    }

    public static int m3546a(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static C0383a m3547a(Object obj) {
        return new C0383a(null);
    }

    public static boolean m3548a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }
}
