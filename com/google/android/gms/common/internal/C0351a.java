package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.C0349l.C0350a;

/* renamed from: com.google.android.gms.common.internal.a */
public class C0351a extends C0350a {
    int f1917a;
    private Account f1918b;
    private Context f1919c;

    public static Account m3339a(C0349l c0349l) {
        Account account = null;
        if (c0349l != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                account = c0349l.m3337a();
            } catch (RemoteException e) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return account;
    }

    public Account m3340a() {
        int callingUid = Binder.getCallingUid();
        if (callingUid == this.f1917a) {
            return this.f1918b;
        }
        if (GooglePlayServicesUtil.zze(this.f1919c, callingUid)) {
            this.f1917a = callingUid;
            return this.f1918b;
        }
        throw new SecurityException("Caller is not GooglePlayServices");
    }

    public boolean equals(Object obj) {
        return this == obj ? true : !(obj instanceof C0351a) ? false : this.f1918b.equals(((C0351a) obj).f1918b);
    }
}
