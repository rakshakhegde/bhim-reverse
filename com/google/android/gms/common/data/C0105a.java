package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.data.a */
public class C0105a implements Creator<DataHolder> {
    static void m3093a(DataHolder dataHolder, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3571a(parcel, 1, dataHolder.m3087c(), false);
        C0386a.m3561a(parcel, 1000, dataHolder.m3086b());
        C0386a.m3570a(parcel, 2, dataHolder.m3088d(), i, false);
        C0386a.m3561a(parcel, 3, dataHolder.m3089e());
        C0386a.m3563a(parcel, 4, dataHolder.m3090f(), false);
        C0386a.m3560a(parcel, a);
    }

    public DataHolder m3094a(Parcel parcel) {
        int i = 0;
        Bundle bundle = null;
        int b = zza.m3582b(parcel);
        CursorWindow[] cursorWindowArr = null;
        String[] strArr = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    strArr = zza.m3593k(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    cursorWindowArr = (CursorWindow[]) zza.m3584b(parcel, a, CursorWindow.CREATOR);
                    break;
                case R.View_paddingEnd /*3*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    bundle = zza.m3592j(parcel, a);
                    break;
                case 1000:
                    i2 = zza.m3586d(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() != b) {
            throw new zza.zza("Overread allowed size end=" + b, parcel);
        }
        DataHolder dataHolder = new DataHolder(i2, strArr, cursorWindowArr, i, bundle);
        dataHolder.m3085a();
        return dataHolder;
    }

    public DataHolder[] m3095a(int i) {
        return new DataHolder[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3094a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3095a(i);
    }
}
