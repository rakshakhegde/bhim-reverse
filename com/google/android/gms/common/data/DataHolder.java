package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import com.google.android.gms.common.internal.C0385p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.HashMap;

public final class DataHolder implements SafeParcelable {
    public static final C0105a CREATOR;
    private static final C0103a f1772l;
    Bundle f1773a;
    int[] f1774b;
    int f1775c;
    boolean f1776d;
    private final int f1777e;
    private final String[] f1778f;
    private final CursorWindow[] f1779g;
    private final int f1780h;
    private final Bundle f1781i;
    private Object f1782j;
    private boolean f1783k;

    /* renamed from: com.google.android.gms.common.data.DataHolder.a */
    public static class C0103a {
        private final String[] f1766a;
        private final ArrayList<HashMap<String, Object>> f1767b;
        private final String f1768c;
        private final HashMap<Object, Integer> f1769d;
        private boolean f1770e;
        private String f1771f;

        private C0103a(String[] strArr, String str) {
            this.f1766a = (String[]) C0385p.m3549a((Object) strArr);
            this.f1767b = new ArrayList();
            this.f1768c = str;
            this.f1769d = new HashMap();
            this.f1770e = false;
            this.f1771f = null;
        }
    }

    /* renamed from: com.google.android.gms.common.data.DataHolder.1 */
    static class C01041 extends C0103a {
        C01041(String[] strArr, String str) {
            super(str, null);
        }
    }

    public static class zzb extends RuntimeException {
    }

    static {
        CREATOR = new C0105a();
        f1772l = new C01041(new String[0], null);
    }

    DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.f1776d = false;
        this.f1783k = true;
        this.f1777e = i;
        this.f1778f = strArr;
        this.f1779g = cursorWindowArr;
        this.f1780h = i2;
        this.f1781i = bundle;
    }

    public void m3085a() {
        int i;
        int i2 = 0;
        this.f1773a = new Bundle();
        for (i = 0; i < this.f1778f.length; i++) {
            this.f1773a.putInt(this.f1778f[i], i);
        }
        this.f1774b = new int[this.f1779g.length];
        i = 0;
        while (i2 < this.f1779g.length) {
            this.f1774b[i2] = i;
            i += this.f1779g[i2].getNumRows() - (i - this.f1779g[i2].getStartPosition());
            i2++;
        }
        this.f1775c = i;
    }

    int m3086b() {
        return this.f1777e;
    }

    String[] m3087c() {
        return this.f1778f;
    }

    CursorWindow[] m3088d() {
        return this.f1779g;
    }

    public int describeContents() {
        return 0;
    }

    public int m3089e() {
        return this.f1780h;
    }

    public Bundle m3090f() {
        return this.f1781i;
    }

    protected void finalize() {
        try {
            if (this.f1783k && this.f1779g.length > 0 && !m3091g()) {
                Log.e("DataBuffer", "Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (" + (this.f1782j == null ? "internal object: " + toString() : this.f1782j.toString()) + ")");
                m3092h();
            }
            super.finalize();
        } catch (Throwable th) {
            super.finalize();
        }
    }

    public boolean m3091g() {
        boolean z;
        synchronized (this) {
            z = this.f1776d;
        }
        return z;
    }

    public void m3092h() {
        synchronized (this) {
            if (!this.f1776d) {
                this.f1776d = true;
                for (CursorWindow close : this.f1779g) {
                    close.close();
                }
            }
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0105a.m3093a(this, parcel, i);
    }
}
