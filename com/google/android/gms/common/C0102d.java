package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.google.android.gms.common.d */
public class C0102d implements ServiceConnection {
    boolean f1764a;
    private final BlockingQueue<IBinder> f1765b;

    public C0102d() {
        this.f1764a = false;
        this.f1765b = new LinkedBlockingQueue();
    }

    public IBinder m3084a() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("BlockingServiceConnection.getService() called on main thread");
        } else if (this.f1764a) {
            throw new IllegalStateException();
        } else {
            this.f1764a = true;
            return (IBinder) this.f1765b.take();
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f1765b.add(iBinder);
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
