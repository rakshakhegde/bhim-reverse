package com.google.android.gms.common;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.C0347f.C0107a;
import com.google.android.gms.common.C0347f.ab;

/* renamed from: com.google.android.gms.common.g */
public class C0348g {
    private static final C0348g f1902a;

    static {
        f1902a = new C0348g();
    }

    private C0348g() {
    }

    public static C0348g m3322a() {
        return f1902a;
    }

    private boolean m3323a(PackageInfo packageInfo, boolean z) {
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return false;
        }
        C0107a abVar = new ab(packageInfo.signatures[0].toByteArray());
        if ((z ? C0347f.m3318a() : C0347f.m3321b()).contains(abVar)) {
            return true;
        }
        if (Log.isLoggable("GoogleSignatureVerifier", 2)) {
            Log.v("GoogleSignatureVerifier", "Signature not valid.  Found: \n" + Base64.encodeToString(abVar.m3100a(), 0));
        }
        return false;
    }

    C0107a m3324a(PackageInfo packageInfo, C0107a... c0107aArr) {
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        C0107a abVar = new ab(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < c0107aArr.length; i++) {
            if (c0107aArr[i].equals(abVar)) {
                return c0107aArr[i];
            }
        }
        if (Log.isLoggable("GoogleSignatureVerifier", 2)) {
            Log.v("GoogleSignatureVerifier", "Signature not valid.  Found: \n" + Base64.encodeToString(abVar.m3100a(), 0));
        }
        return null;
    }

    public boolean m3325a(PackageManager packageManager, PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (GooglePlayServicesUtil.zzc(packageManager)) {
            return m3323a(packageInfo, true);
        }
        boolean a = m3323a(packageInfo, false);
        if (a || !m3323a(packageInfo, true)) {
            return a;
        }
        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        return a;
    }

    public boolean m3326a(PackageManager packageManager, String str) {
        try {
            return m3325a(packageManager, packageManager.getPackageInfo(str, 64));
        } catch (NameNotFoundException e) {
            if (Log.isLoggable("GoogleSignatureVerifier", 3)) {
                Log.d("GoogleSignatureVerifier", "Package manager can't find package " + str + ", defaulting to false");
            }
            return false;
        }
    }
}
