package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.e */
public class C0106e implements Creator<ConnectionResult> {
    static void m3096a(ConnectionResult connectionResult, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, connectionResult.f1718b);
        C0386a.m3561a(parcel, 2, connectionResult.m3049b());
        C0386a.m3565a(parcel, 3, connectionResult.m3050c(), i, false);
        C0386a.m3568a(parcel, 4, connectionResult.m3051d(), false);
        C0386a.m3560a(parcel, a);
    }

    public ConnectionResult m3097a(Parcel parcel) {
        String str = null;
        int i = 0;
        int b = zza.m3582b(parcel);
        PendingIntent pendingIntent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            PendingIntent pendingIntent2;
            int i3;
            String str2;
            int a = zza.m3577a(parcel);
            String str3;
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    str3 = str;
                    pendingIntent2 = pendingIntent;
                    i3 = i;
                    i = zza.m3586d(parcel, a);
                    str2 = str3;
                    break;
                case R.View_paddingStart /*2*/:
                    i = i2;
                    PendingIntent pendingIntent3 = pendingIntent;
                    i3 = zza.m3586d(parcel, a);
                    str2 = str;
                    pendingIntent2 = pendingIntent3;
                    break;
                case R.View_paddingEnd /*3*/:
                    i3 = i;
                    i = i2;
                    str3 = str;
                    pendingIntent2 = (PendingIntent) zza.m3579a(parcel, a, PendingIntent.CREATOR);
                    str2 = str3;
                    break;
                case R.View_theme /*4*/:
                    str2 = zza.m3590h(parcel, a);
                    pendingIntent2 = pendingIntent;
                    i3 = i;
                    i = i2;
                    break;
                default:
                    zza.m3583b(parcel, a);
                    str2 = str;
                    pendingIntent2 = pendingIntent;
                    i3 = i;
                    i = i2;
                    break;
            }
            i2 = i;
            i = i3;
            pendingIntent = pendingIntent2;
            str = str2;
        }
        if (parcel.dataPosition() == b) {
            return new ConnectionResult(i2, i, pendingIntent, str);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public ConnectionResult[] m3098a(int i) {
        return new ConnectionResult[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3097a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3098a(i);
    }
}
