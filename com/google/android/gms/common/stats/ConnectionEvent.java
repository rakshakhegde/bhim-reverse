package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ConnectionEvent extends C0387f implements SafeParcelable {
    public static final Creator<ConnectionEvent> CREATOR;
    final int f1996a;
    private final long f1997b;
    private int f1998c;
    private final String f1999d;
    private final String f2000e;
    private final String f2001f;
    private final String f2002g;
    private final String f2003h;
    private final String f2004i;
    private final long f2005j;
    private final long f2006k;
    private long f2007l;

    static {
        CREATOR = new C0388a();
    }

    ConnectionEvent(int i, long j, int i2, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this.f1996a = i;
        this.f1997b = j;
        this.f1998c = i2;
        this.f1999d = str;
        this.f2000e = str2;
        this.f2001f = str3;
        this.f2002g = str4;
        this.f2007l = -1;
        this.f2003h = str5;
        this.f2004i = str6;
        this.f2005j = j2;
        this.f2006k = j3;
    }

    public ConnectionEvent(long j, int i, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this(1, j, i, str, str2, str3, str4, str5, str6, j2, j3);
    }

    public long m3598a() {
        return this.f1997b;
    }

    public int m3599b() {
        return this.f1998c;
    }

    public String m3600c() {
        return this.f1999d;
    }

    public String m3601d() {
        return this.f2000e;
    }

    public int describeContents() {
        return 0;
    }

    public String m3602e() {
        return this.f2001f;
    }

    public String m3603f() {
        return this.f2002g;
    }

    public String m3604g() {
        return this.f2003h;
    }

    public String m3605h() {
        return this.f2004i;
    }

    public long m3606i() {
        return this.f2007l;
    }

    public long m3607j() {
        return this.f2006k;
    }

    public long m3608k() {
        return this.f2005j;
    }

    public String m3609l() {
        return "\t" + m3600c() + "/" + m3601d() + "\t" + m3602e() + "/" + m3603f() + "\t" + (this.f2003h == null ? BuildConfig.FLAVOR : this.f2003h) + "\t" + m3607j();
    }

    public void writeToParcel(Parcel parcel, int i) {
        C0388a.m3610a(this, parcel, i);
    }
}
