package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Debug;
import android.os.Parcelable;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.C0353c;
import com.google.android.gms.common.stats.C0391c.C0390a;
import com.google.android.gms.internal.C0427d;
import com.google.android.gms.internal.C0432i;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.google.android.gms.common.stats.b */
public class C0389b {
    private static final Object f2008a;
    private static C0389b f2009b;
    private static Integer f2010h;
    private final List<String> f2011c;
    private final List<String> f2012d;
    private final List<String> f2013e;
    private final List<String> f2014f;
    private C0393e f2015g;
    private C0393e f2016i;

    static {
        f2008a = new Object();
    }

    private C0389b() {
        if (C0389b.m3621c() == C0392d.f2026b) {
            this.f2011c = Collections.EMPTY_LIST;
            this.f2012d = Collections.EMPTY_LIST;
            this.f2013e = Collections.EMPTY_LIST;
            this.f2014f = Collections.EMPTY_LIST;
            return;
        }
        String str = (String) C0390a.f2018b.m3761c();
        this.f2011c = str == null ? Collections.EMPTY_LIST : Arrays.asList(str.split(","));
        str = (String) C0390a.f2019c.m3761c();
        this.f2012d = str == null ? Collections.EMPTY_LIST : Arrays.asList(str.split(","));
        str = (String) C0390a.f2020d.m3761c();
        this.f2013e = str == null ? Collections.EMPTY_LIST : Arrays.asList(str.split(","));
        str = (String) C0390a.f2021e.m3761c();
        this.f2014f = str == null ? Collections.EMPTY_LIST : Arrays.asList(str.split(","));
        this.f2015g = new C0393e(1024, ((Long) C0390a.f2022f.m3761c()).longValue());
        this.f2016i = new C0393e(1024, ((Long) C0390a.f2022f.m3761c()).longValue());
    }

    public static C0389b m3613a() {
        synchronized (f2008a) {
            if (f2009b == null) {
                f2009b = new C0389b();
            }
        }
        return f2009b;
    }

    private String m3614a(ServiceConnection serviceConnection) {
        return String.valueOf((((long) Process.myPid()) << 32) | ((long) System.identityHashCode(serviceConnection)));
    }

    private void m3615a(Context context, String str, int i, String str2, String str3, String str4, String str5) {
        Parcelable connectionEvent;
        long currentTimeMillis = System.currentTimeMillis();
        String str6 = null;
        if (!((C0389b.m3621c() & C0392d.f2030f) == 0 || i == 13)) {
            str6 = C0432i.m3795a(3, 5);
        }
        long j = 0;
        if ((C0389b.m3621c() & C0392d.f2032h) != 0) {
            j = Debug.getNativeHeapAllocatedSize();
        }
        if (i == 1 || i == 4 || i == 14) {
            connectionEvent = new ConnectionEvent(currentTimeMillis, i, null, null, null, null, str6, str, SystemClock.elapsedRealtime(), j);
        } else {
            connectionEvent = new ConnectionEvent(currentTimeMillis, i, str2, str3, str4, str5, str6, str, SystemClock.elapsedRealtime(), j);
        }
        context.startService(new Intent().setComponent(C0392d.f2025a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", connectionEvent));
    }

    private void m3616a(Context context, String str, String str2, Intent intent, int i) {
        String str3 = null;
        if (m3620b() && this.f2015g != null) {
            String str4;
            String str5;
            if (i != 4 && i != 1) {
                ServiceInfo b = C0389b.m3619b(context, intent);
                if (b == null) {
                    Log.w("ConnectionTracker", String.format("Client %s made an invalid request %s", new Object[]{str2, intent.toUri(0)}));
                    return;
                }
                str4 = b.processName;
                str5 = b.name;
                str3 = C0432i.m3796a(context);
                if (m3618a(str3, str2, str4, str5)) {
                    this.f2015g.m3628a(str);
                } else {
                    return;
                }
            } else if (this.f2015g.m3629b(str)) {
                str5 = null;
                str4 = null;
            } else {
                return;
            }
            m3615a(context, str, i, str3, str2, str4, str5);
        }
    }

    private boolean m3617a(Context context, Intent intent) {
        ComponentName component = intent.getComponent();
        return (component == null || (C0353c.f1920a && GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE.equals(component.getPackageName()))) ? false : C0427d.m3778a(context, component.getPackageName());
    }

    private boolean m3618a(String str, String str2, String str3, String str4) {
        return (this.f2011c.contains(str) || this.f2012d.contains(str2) || this.f2013e.contains(str3) || this.f2014f.contains(str4) || (str3.equals(str) && (C0389b.m3621c() & C0392d.f2031g) != 0)) ? false : true;
    }

    private static ServiceInfo m3619b(Context context, Intent intent) {
        List queryIntentServices = context.getPackageManager().queryIntentServices(intent, 128);
        if (queryIntentServices == null || queryIntentServices.size() == 0) {
            Log.w("ConnectionTracker", String.format("There are no handler of this intent: %s\n Stack trace: %s", new Object[]{intent.toUri(0), C0432i.m3795a(3, 20)}));
            return null;
        }
        if (queryIntentServices.size() > 1) {
            Log.w("ConnectionTracker", String.format("Multiple handlers found for this intent: %s\n Stack trace: %s", new Object[]{intent.toUri(0), C0432i.m3795a(3, 20)}));
            Iterator it = queryIntentServices.iterator();
            if (it.hasNext()) {
                Log.w("ConnectionTracker", ((ResolveInfo) it.next()).serviceInfo.name);
                return null;
            }
        }
        return ((ResolveInfo) queryIntentServices.get(0)).serviceInfo;
    }

    private boolean m3620b() {
        return C0353c.f1920a && C0389b.m3621c() != C0392d.f2026b;
    }

    private static int m3621c() {
        if (f2010h == null) {
            try {
                f2010h = Integer.valueOf(C0427d.m3777a() ? ((Integer) C0390a.f2017a.m3761c()).intValue() : C0392d.f2026b);
            } catch (SecurityException e) {
                f2010h = Integer.valueOf(C0392d.f2026b);
            }
        }
        return f2010h.intValue();
    }

    public void m3622a(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
        m3616a(context, m3614a(serviceConnection), null, null, 1);
    }

    public void m3623a(Context context, ServiceConnection serviceConnection, String str, Intent intent) {
        m3616a(context, m3614a(serviceConnection), str, intent, 3);
    }

    public boolean m3624a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        return m3625a(context, context.getClass().getName(), intent, serviceConnection, i);
    }

    public boolean m3625a(Context context, String str, Intent intent, ServiceConnection serviceConnection, int i) {
        if (m3617a(context, intent)) {
            Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
            return false;
        }
        boolean bindService = context.bindService(intent, serviceConnection, i);
        if (bindService) {
            m3616a(context, m3614a(serviceConnection), str, intent, 2);
        }
        return bindService;
    }

    public void m3626b(Context context, ServiceConnection serviceConnection) {
        m3616a(context, m3614a(serviceConnection), null, null, 4);
    }
}
