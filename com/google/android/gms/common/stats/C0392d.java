package com.google.android.gms.common.stats;

import android.content.ComponentName;
import com.google.android.gms.common.GooglePlayServicesUtil;

/* renamed from: com.google.android.gms.common.stats.d */
public final class C0392d {
    public static final ComponentName f2025a;
    public static int f2026b;
    public static int f2027c;
    public static int f2028d;
    public static int f2029e;
    public static int f2030f;
    public static int f2031g;
    public static int f2032h;
    public static int f2033i;

    static {
        f2025a = new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.common.stats.GmsCoreStatsService");
        f2026b = 0;
        f2027c = 1;
        f2028d = 2;
        f2029e = 4;
        f2030f = 8;
        f2031g = 16;
        f2032h = 32;
        f2033i = 1;
    }
}
