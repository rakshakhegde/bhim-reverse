package com.google.android.gms.common.stats;

import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.internal.C0420b;

/* renamed from: com.google.android.gms.common.stats.c */
public final class C0391c {
    public static C0420b<Integer> f2023a;
    public static C0420b<Integer> f2024b;

    /* renamed from: com.google.android.gms.common.stats.c.a */
    public static final class C0390a {
        public static C0420b<Integer> f2017a;
        public static C0420b<String> f2018b;
        public static C0420b<String> f2019c;
        public static C0420b<String> f2020d;
        public static C0420b<String> f2021e;
        public static C0420b<Long> f2022f;

        static {
            f2017a = C0420b.m3754a("gms:common:stats:connections:level", Integer.valueOf(C0392d.f2026b));
            f2018b = C0420b.m3756a("gms:common:stats:connections:ignored_calling_processes", BuildConfig.FLAVOR);
            f2019c = C0420b.m3756a("gms:common:stats:connections:ignored_calling_services", BuildConfig.FLAVOR);
            f2020d = C0420b.m3756a("gms:common:stats:connections:ignored_target_processes", BuildConfig.FLAVOR);
            f2021e = C0420b.m3756a("gms:common:stats:connections:ignored_target_services", "com.google.android.gms.auth.GetToken");
            f2022f = C0420b.m3755a("gms:common:stats:connections:time_out_duration", Long.valueOf(600000));
        }
    }

    static {
        f2023a = C0420b.m3754a("gms:common:stats:max_num_of_events", Integer.valueOf(100));
        f2024b = C0420b.m3754a("gms:common:stats:max_chunk_size", Integer.valueOf(100));
    }
}
