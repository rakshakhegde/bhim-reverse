package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.C0386a;
import com.google.android.gms.common.internal.safeparcel.zza;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: com.google.android.gms.common.stats.a */
public class C0388a implements Creator<ConnectionEvent> {
    static void m3610a(ConnectionEvent connectionEvent, Parcel parcel, int i) {
        int a = C0386a.m3559a(parcel);
        C0386a.m3561a(parcel, 1, connectionEvent.f1996a);
        C0386a.m3562a(parcel, 2, connectionEvent.m3598a());
        C0386a.m3568a(parcel, 4, connectionEvent.m3600c(), false);
        C0386a.m3568a(parcel, 5, connectionEvent.m3601d(), false);
        C0386a.m3568a(parcel, 6, connectionEvent.m3602e(), false);
        C0386a.m3568a(parcel, 7, connectionEvent.m3603f(), false);
        C0386a.m3568a(parcel, 8, connectionEvent.m3604g(), false);
        C0386a.m3562a(parcel, 10, connectionEvent.m3608k());
        C0386a.m3562a(parcel, 11, connectionEvent.m3607j());
        C0386a.m3561a(parcel, 12, connectionEvent.m3599b());
        C0386a.m3568a(parcel, 13, connectionEvent.m3605h(), false);
        C0386a.m3560a(parcel, a);
    }

    public ConnectionEvent m3611a(Parcel parcel) {
        int b = zza.m3582b(parcel);
        int i = 0;
        long j = 0;
        int i2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < b) {
            int a = zza.m3577a(parcel);
            switch (zza.m3576a(a)) {
                case R.View_android_focusable /*1*/:
                    i = zza.m3586d(parcel, a);
                    break;
                case R.View_paddingStart /*2*/:
                    j = zza.m3587e(parcel, a);
                    break;
                case R.View_theme /*4*/:
                    str = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetStart /*5*/:
                    str2 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetEnd /*6*/:
                    str3 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetLeft /*7*/:
                    str4 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_contentInsetRight /*8*/:
                    str5 = zza.m3590h(parcel, a);
                    break;
                case R.Toolbar_titleTextAppearance /*10*/:
                    j2 = zza.m3587e(parcel, a);
                    break;
                case R.Toolbar_subtitleTextAppearance /*11*/:
                    j3 = zza.m3587e(parcel, a);
                    break;
                case R.Toolbar_titleMargins /*12*/:
                    i2 = zza.m3586d(parcel, a);
                    break;
                case R.Toolbar_titleMarginStart /*13*/:
                    str6 = zza.m3590h(parcel, a);
                    break;
                default:
                    zza.m3583b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ConnectionEvent(i, j, i2, str, str2, str3, str4, str5, str6, j2, j3);
        }
        throw new zza.zza("Overread allowed size end=" + b, parcel);
    }

    public ConnectionEvent[] m3612a(int i) {
        return new ConnectionEvent[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return m3611a(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return m3612a(i);
    }
}
