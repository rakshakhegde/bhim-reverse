package com.google.android.gms.common.stats;

/* renamed from: com.google.android.gms.common.stats.f */
public abstract class C0387f {
    public abstract long m3594a();

    public abstract int m3595b();

    public abstract long m3596i();

    public abstract String m3597l();

    public String toString() {
        return m3594a() + "\t" + m3595b() + "\t" + m3596i() + m3597l();
    }
}
