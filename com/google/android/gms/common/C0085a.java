package com.google.android.gms.common;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import com.google.android.gms.common.internal.C0385p;

/* renamed from: com.google.android.gms.common.a */
public class C0085a extends DialogFragment {
    private Dialog f1726a;
    private OnCancelListener f1727b;

    public C0085a() {
        this.f1726a = null;
        this.f1727b = null;
    }

    public static C0085a m3052a(Dialog dialog, OnCancelListener onCancelListener) {
        C0085a c0085a = new C0085a();
        Dialog dialog2 = (Dialog) C0385p.m3550a((Object) dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        c0085a.f1726a = dialog2;
        if (onCancelListener != null) {
            c0085a.f1727b = onCancelListener;
        }
        return c0085a;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f1727b != null) {
            this.f1727b.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f1726a == null) {
            setShowsDialog(false);
        }
        return this.f1726a;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
