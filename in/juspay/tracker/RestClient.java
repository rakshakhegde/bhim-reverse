package in.juspay.tracker;

import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class RestClient {
    private static final String f2613a;
    private static DefaultHttpClient f2614b;
    private static DefaultHttpClient f2615c;

    public static class UnsuccessfulRestCall extends RuntimeException {
        private final HttpResponse f2611a;
        private final HttpRequestBase f2612b;

        public UnsuccessfulRestCall(HttpRequestBase httpRequestBase, HttpResponse httpResponse) {
            super(httpRequestBase.getURI() + " returned " + httpResponse.getStatusLine().getStatusCode());
            this.f2611a = httpResponse;
            this.f2612b = httpRequestBase;
        }
    }

    static {
        f2613a = RestClient.class.getName();
        m4502a();
    }

    private static DefaultHttpClient m4501a(HttpParams httpParams) {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        SocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier(SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        schemeRegistry.register(new Scheme("https", socketFactory, 443));
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
    }

    public static void m4502a() {
        JuspayLogger.m4566a(f2613a, "Default http client");
        HttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, AbstractSpiCall.DEFAULT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        f2614b = m4501a(basicHttpParams);
        basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, AbstractSpiCall.DEFAULT_TIMEOUT);
        f2615c = m4501a(basicHttpParams);
    }

    private static byte[] m4506a(HttpRequestBase httpRequestBase, HttpResponse httpResponse) {
        GZIPInputStream gZIPInputStream;
        try {
            if (m4504a(httpResponse)) {
                Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                if (contentEncoding == null || !contentEncoding.getValue().equals("gzip")) {
                    return EntityUtils.toByteArray(httpResponse.getEntity());
                }
                JuspayLogger.m4566a(f2613a, "GZIP Header Present");
                gZIPInputStream = new GZIPInputStream(httpResponse.getEntity().getContent());
                byte[] bArr = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = gZIPInputStream.read(bArr);
                    if (read < 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bArr = byteArrayOutputStream.toByteArray();
                if (gZIPInputStream == null) {
                    return bArr;
                }
                JuspayLogger.m4566a(f2613a, "CLOSING GZIP STREAM");
                gZIPInputStream.close();
                return bArr;
            }
            throw new UnsuccessfulRestCall(httpRequestBase, httpResponse);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        } catch (ClientProtocolException e2) {
            throw new RuntimeException(e2.getMessage());
        } catch (Throwable th) {
            if (gZIPInputStream != null) {
                JuspayLogger.m4566a(f2613a, "CLOSING GZIP STREAM");
                gZIPInputStream.close();
            }
        }
    }

    private static byte[] m4505a(HttpRequestBase httpRequestBase) {
        JuspayLogger.m4568b(f2613a, "Executing " + httpRequestBase.getMethod() + " " + httpRequestBase.getURI());
        try {
            httpRequestBase.setHeader("Accept-Encoding", "gzip");
            HttpResponse execute = f2614b.execute(httpRequestBase);
            JuspayLogger.m4568b(f2613a, "Got response for " + httpRequestBase.getURI() + ", response code " + execute.getStatusLine().getStatusCode());
            return m4506a(httpRequestBase, execute);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static boolean m4504a(HttpResponse httpResponse) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        return statusCode >= 200 && statusCode < 300;
    }

    public static void m4503a(String str, String str2) {
        HttpRequestBase httpPost = new HttpPost(str);
        httpPost.setHeader("Content-Type", "application/x-godel-gzip-encrypted");
        httpPost.setEntity(new ByteArrayEntity(EncryptionHelper.m4509a().m4517d(str2.getBytes("UTF-8"))));
        m4505a(httpPost);
    }

    public static void m4507b(String str, String str2) {
        HttpRequestBase httpPost = new HttpPost(str);
        httpPost.setHeader("Content-Type", "application/x-godel-gzip-encoded");
        httpPost.setEntity(new ByteArrayEntity(EncryptionHelper.m4509a().m4518e(str2.getBytes("UTF-8"))));
        m4505a(httpPost);
    }

    public static void m4508c(String str, String str2) {
        HttpRequestBase httpPost = new HttpPost(str);
        httpPost.setHeader("Content-Type", "application/x-godel-gzip");
        httpPost.setEntity(new ByteArrayEntity(FileUtil.m4533a(str2.getBytes("UTF-8"))));
        m4505a(httpPost);
    }
}
