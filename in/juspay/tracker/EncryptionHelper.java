package in.juspay.tracker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: in.juspay.tracker.a */
public class EncryptionHelper {
    private static String f2616a;
    private static byte[] f2617b;
    private static EncryptionHelper f2618c;
    private static final String f2619d;

    static {
        f2616a = "AES";
        f2617b = new byte[]{(byte) -52, (byte) 51, (byte) -68, (byte) -121, (byte) -44, (byte) -114, (byte) -59, (byte) -20, (byte) -79, (byte) 22, (byte) 34, (byte) -77, (byte) -48, (byte) -75, (byte) 45, (byte) 93};
        f2619d = EncryptionHelper.class.getName();
    }

    public static EncryptionHelper m4509a() {
        EncryptionHelper encryptionHelper;
        synchronized (EncryptionHelper.class) {
            if (f2618c == null) {
                f2618c = new EncryptionHelper();
            }
            encryptionHelper = f2618c;
        }
        return encryptionHelper;
    }

    public String m4512a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            String f = m4511f(instance.digest());
            JuspayLogger.m4568b(f2619d, "result is " + f);
            return f;
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2619d, "Exception caught trying to SHA-256 hash", e);
            return null;
        }
    }

    private String m4511f(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            String toHexString = Integer.toHexString(b & 255);
            if (toHexString.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(toHexString);
        }
        return stringBuffer.toString();
    }

    public String m4514b(String str) {
        return m4513a(str.getBytes());
    }

    public String m4513a(byte[] bArr) {
        String str = "MD5";
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : digest) {
                str = Integer.toHexString(b & 255);
                while (str.length() < 2) {
                    str = "0" + str;
                }
                stringBuilder.append(str);
            }
            return stringBuilder.toString();
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2619d, "Exception trying to calculate md5sum from given string", e);
            return null;
        }
    }

    private Key m4510b() {
        return new SecretKeySpec(f2617b, f2616a);
    }

    public byte[] m4515b(byte[] bArr) {
        Key b = m4510b();
        Cipher instance = Cipher.getInstance(f2616a);
        instance.init(1, b);
        return instance.doFinal(bArr);
    }

    public byte[] m4516c(byte[] bArr) {
        int i = 0;
        byte[] bArr2 = new byte[8];
        new SecureRandom().nextBytes(bArr2);
        int length = bArr.length;
        byte[] bArr3 = new byte[(length + 8)];
        int i2 = 0;
        int i3 = 0;
        while (i < length && i2 < length + 8) {
            if (i2 <= 0 || i2 % 10 != 9 || i3 >= 8) {
                bArr3[i2] = (byte) (bArr[i] ^ bArr2[i % 8]);
                i++;
            } else {
                bArr3[i2] = bArr2[i3];
                i3++;
            }
            i2++;
        }
        return bArr3;
    }

    public byte[] m4517d(byte[] bArr) {
        try {
            return m4515b(FileUtil.m4533a(bArr));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public byte[] m4518e(byte[] bArr) {
        try {
            return m4516c(FileUtil.m4533a(bArr));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }
}
