package in.juspay.tracker;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.crashlytics.android.core.BuildConfig;
import in.juspay.tracker.Event.Event;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: in.juspay.tracker.e */
public class GodelTracker {
    private static final String f2659a;
    private static long f2660b;
    private static long f2661c;
    private static long f2662d;
    private static long f2663e;
    private static List<Map<String, String>> f2664f;
    private static Map<String, String> f2665g;
    private static boolean f2666p;
    private static GodelTracker f2667w;
    private Timer f2668h;
    private TimerTask f2669i;
    private GodelTracker f2670j;
    private int f2671k;
    private String f2672l;
    private long f2673m;
    private String f2674n;
    private String f2675o;
    private boolean f2676q;
    private long f2677r;
    private Timer f2678s;
    private volatile boolean f2679t;
    private GodelTracker f2680u;
    private String f2681v;
    private Context f2682x;
    private String f2683y;

    /* renamed from: in.juspay.tracker.e.a */
    public enum GodelTracker {
        ENCRYPT,
        OBFUSCATE,
        GZIP
    }

    /* renamed from: in.juspay.tracker.e.b */
    class GodelTracker extends TimerTask {
        final /* synthetic */ GodelTracker f2657a;

        /* renamed from: in.juspay.tracker.e.b.1 */
        class GodelTracker implements Runnable {
            final /* synthetic */ GodelTracker f2656a;

            /* renamed from: in.juspay.tracker.e.b.1.1 */
            class GodelTracker implements FilenameFilter {
                final /* synthetic */ GodelTracker f2652a;

                GodelTracker(GodelTracker godelTracker) {
                    this.f2652a = godelTracker;
                }

                public boolean accept(File file, String str) {
                    return str.contains("sendingLogs");
                }
            }

            /* renamed from: in.juspay.tracker.e.b.1.2 */
            class GodelTracker extends AsyncTask {
                final /* synthetic */ String f2653a;
                final /* synthetic */ File f2654b;
                final /* synthetic */ GodelTracker f2655c;

                GodelTracker(GodelTracker godelTracker, String str, File file) {
                    this.f2655c = godelTracker;
                    this.f2653a = str;
                    this.f2654b = file;
                }

                protected Object doInBackground(Object[] objArr) {
                    try {
                        if (this.f2655c.f2656a.f2657a.f2680u == GodelTracker.ENCRYPT) {
                            RestClient.m4503a(Properties.m4574a().m4579b(), this.f2653a);
                        } else if (this.f2655c.f2656a.f2657a.f2680u == GodelTracker.OBFUSCATE) {
                            RestClient.m4507b(Properties.m4574a().m4579b(), this.f2653a);
                        } else if (this.f2655c.f2656a.f2657a.f2680u == GodelTracker.GZIP) {
                            RestClient.m4508c(Properties.m4574a().m4579b(), this.f2653a);
                        }
                        this.f2654b.delete();
                    } catch (Throwable th) {
                        JuspayLogger.m4567a(GodelTracker.f2659a, "Exception trying to post analytics data as JSON ", th);
                    }
                    GodelTracker.f2666p = false;
                    return null;
                }
            }

            GodelTracker(GodelTracker godelTracker) {
                this.f2656a = godelTracker;
            }

            public void run() {
                try {
                    File file;
                    StringBuilder a;
                    Object jSONArray;
                    JSONObject jSONObject;
                    String jSONObject2;
                    GodelTracker.f2666p = true;
                    this.f2656a.f2657a.m4558a(true);
                    File file2 = new File(this.f2656a.f2657a.f2682x.getFilesDir() + "/residueLogs.log");
                    File file3 = new File(this.f2656a.f2657a.f2682x.getFilesDir() + "/sendingLogs" + new Date().getTime() + ".log");
                    if (file2.exists()) {
                        file2.renameTo(file3);
                    }
                    this.f2656a.f2657a.m4558a(false);
                    File[] listFiles = new File(this.f2656a.f2657a.f2682x.getFilesDir() + "/").listFiles(new GodelTracker(this));
                    StringBuilder stringBuilder = new StringBuilder(BuildConfig.FLAVOR);
                    if (listFiles.length > 0) {
                        file3 = listFiles[0];
                        if (file3.exists()) {
                            file = file3;
                            a = this.f2656a.m4535a(file3);
                        } else {
                            file = file3;
                            a = stringBuilder;
                        }
                    } else {
                        a = stringBuilder;
                        file = null;
                    }
                    a.insert(0, "[");
                    a.append("{}]");
                    try {
                        if (!a.toString().equals("[{}]")) {
                            jSONArray = new JSONArray(a.toString());
                            jSONObject = new JSONObject();
                            jSONObject.put(CLConstants.FIELD_DATA, jSONArray);
                            jSONObject2 = jSONObject.toString();
                            if (jSONArray != null || file == null) {
                                GodelTracker.f2666p = false;
                            } else {
                                new GodelTracker(this, jSONObject2, file).execute(new Object[0]);
                            }
                            JuspayLogger.m4568b(GodelTracker.f2659a, "Post data: " + jSONObject2);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jSONArray = null;
                    try {
                        jSONObject = new JSONObject();
                        jSONObject.put(CLConstants.FIELD_DATA, jSONArray);
                        jSONObject2 = jSONObject.toString();
                        if (jSONArray != null) {
                        }
                        GodelTracker.f2666p = false;
                        JuspayLogger.m4568b(GodelTracker.f2659a, "Post data: " + jSONObject2);
                    } catch (Throwable e2) {
                        JuspayLogger.m4567a(GodelTracker.f2659a, e2.getMessage(), e2);
                        GodelTracker.f2666p = false;
                    }
                } catch (Throwable e22) {
                    Log.e(GodelTracker.f2659a, "Exception posting logs", e22);
                    GodelTracker.f2666p = false;
                }
            }
        }

        GodelTracker(GodelTracker godelTracker) {
            this.f2657a = godelTracker;
        }

        public void run() {
            if (!GodelTracker.f2666p && !this.f2657a.m4562c()) {
                new Handler(Looper.getMainLooper()).post(new GodelTracker(this));
            }
        }

        private StringBuilder m4535a(File file) {
            StringBuilder stringBuilder = new StringBuilder(BuildConfig.FLAVOR);
            if (file.exists()) {
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    while (true) {
                        int read = fileInputStream.read();
                        if (read == -1) {
                            break;
                        }
                        stringBuilder.append((char) read);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return stringBuilder;
        }
    }

    /* renamed from: in.juspay.tracker.e.c */
    class GodelTracker extends TimerTask {
        final /* synthetic */ GodelTracker f2658a;

        GodelTracker(GodelTracker godelTracker) {
            this.f2658a = godelTracker;
        }

        public void run() {
            if (GodelTracker.f2664f != null && GodelTracker.f2664f.size() > 0 && !this.f2658a.m4562c()) {
                this.f2658a.m4558a(true);
                try {
                    int size = GodelTracker.f2664f.size();
                    StringBuilder stringBuilder = new StringBuilder(BuildConfig.FLAVOR);
                    for (int i = 0; i < size; i++) {
                        if (!GodelTracker.f2664f.isEmpty()) {
                            stringBuilder.append(new JSONObject((Map) GodelTracker.f2664f.get(0)).toString() + ",");
                            GodelTracker.f2664f.remove(0);
                        }
                    }
                    FileOutputStream openFileOutput = this.f2658a.f2682x.openFileOutput("residueLogs.log", 32768);
                    openFileOutput.write(stringBuilder.toString().getBytes());
                    openFileOutput.flush();
                    openFileOutput.close();
                } catch (Throwable e) {
                    Log.e(GodelTracker.f2659a, "Exception writing logs to a file", e);
                }
                this.f2658a.m4558a(false);
                if (this.f2658a.f2679t) {
                    this.f2658a.f2669i.run();
                }
            }
        }
    }

    static {
        f2659a = GodelTracker.class.getName();
        f2660b = 3000;
        f2661c = 5000;
        f2662d = 10000;
        f2663e = 3000;
        f2664f = new CopyOnWriteArrayList();
        f2665g = new HashMap();
        f2666p = false;
        f2667w = null;
    }

    public static void m4538a(GodelTracker godelTracker) {
        f2667w = null;
        f2667w = GodelTracker.m4537a();
        f2667w.f2680u = godelTracker;
    }

    private GodelTracker() {
        this.f2671k = 0;
        this.f2676q = true;
        this.f2677r = 0;
        this.f2678s = null;
        this.f2679t = false;
        this.f2680u = GodelTracker.ENCRYPT;
        this.f2672l = UUID.randomUUID().toString();
        Log.d(f2659a, "Godel Session Id - " + this.f2672l);
        this.f2673m = new Date().getTime();
        this.f2669i = new GodelTracker(this);
        this.f2670j = new GodelTracker(this);
    }

    private void m4550k() {
        if (this.f2668h == null) {
            this.f2668h = new Timer();
            this.f2668h.schedule(this.f2669i, f2660b, f2661c);
        }
        if (this.f2678s == null) {
            this.f2678s = new Timer();
            this.f2678s.schedule(this.f2670j, f2660b, f2663e);
        }
    }

    public static GodelTracker m4537a() {
        GodelTracker godelTracker;
        synchronized (GodelTracker.class) {
            if (f2667w == null) {
                f2667w = new GodelTracker();
                f2667w.m4551l();
            }
            godelTracker = f2667w;
        }
        return godelTracker;
    }

    public String m4559b() {
        return this.f2672l;
    }

    public boolean m4562c() {
        if (this.f2677r + f2662d > System.currentTimeMillis()) {
            return this.f2676q;
        }
        m4558a(false);
        return false;
    }

    public void m4558a(boolean z) {
        this.f2677r = System.currentTimeMillis();
        this.f2676q = z;
    }

    private void m4551l() {
        if (f2667w != null) {
            for (Entry entry : f2665g.entrySet()) {
                GodelTracker.m4537a().m4553a(new Event().m4522a(Event.GODEL).m4521a(Event.INFO).m4525b((String) entry.getKey()).m4527c((String) entry.getValue()));
            }
            f2665g.clear();
        }
    }

    public void m4552a(Context context) {
        this.f2682x = context;
    }

    public void m4563d() {
        try {
            JSONObject f = Properties.m4574a().m4585f();
            f2660b = f.optLong("interval_start", f2660b);
            f2661c = f.optLong("interval_batch", f2661c);
            GodelTracker.m4537a().m4553a(new Event().m4522a(Event.CONFIG).m4521a(Event.INFO).m4525b("log_push_config").m4527c("START_INTERVAL = " + f2660b + " BATCH_INTERVAL = " + f2661c));
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2659a, "Exception while setting timer interval", e);
        } finally {
            m4550k();
        }
    }

    public void m4556a(Date date, String str) {
        Map hashMap = new HashMap();
        hashMap.put(CLConstants.FIELD_TYPE, "jsError");
        hashMap.put("at", String.valueOf(date.getTime()));
        hashMap.put("stackTrace", str);
        hashMap.put("pageId", String.valueOf(PageView.f2689h));
        m4540b(hashMap);
    }

    public void m4553a(Event event) {
        Event a = LoggingUtil.m4569a().m4573a(event);
        if (a != null) {
            Map hashMap = new HashMap();
            hashMap.put(CLConstants.FIELD_TYPE, "event");
            hashMap.put("at", String.valueOf(a.f2639a.getTime()));
            hashMap.put("category", a.f2640b);
            hashMap.put(CLConstants.OUTPUT_KEY_ACTION, a.f2641c);
            hashMap.put("label", a.f2642d);
            hashMap.put(CLConstants.FIELD_PAY_INFO_VALUE, a.f2643e);
            hashMap.put("pageId", String.valueOf(PageView.f2689h));
            m4540b(hashMap);
        }
    }

    public void m4554a(ExceptionTracker exceptionTracker) {
        Map hashMap = new HashMap();
        hashMap.put(CLConstants.FIELD_TYPE, "Exception");
        hashMap.put("at", String.valueOf(exceptionTracker.m4532c().getTime()));
        hashMap.put("message", exceptionTracker.m4531b().getLocalizedMessage());
        hashMap.put("stackTrace", Log.getStackTraceString(exceptionTracker.m4531b()));
        hashMap.put("description", exceptionTracker.m4530a());
        hashMap.put("pageId", String.valueOf(PageView.f2689h));
        hashMap.put("log_level", String.valueOf(2));
        m4540b(hashMap);
    }

    public static String m4546e() {
        try {
            for (NetworkInterface inetAddresses : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                for (InetAddress inetAddress : Collections.list(inetAddresses.getInetAddresses())) {
                    if (!inetAddress.isLoopbackAddress()) {
                        String toUpperCase = inetAddress.getHostAddress().toUpperCase();
                        if (InetAddressUtils.isIPv4Address(toUpperCase)) {
                            return toUpperCase;
                        }
                    }
                }
            }
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2659a, "Failed to Retreive IP address", e);
        }
        return BuildConfig.FLAVOR;
    }

    public Map<String, String> m4564f() {
        Map<String, String> hashMap = new HashMap();
        try {
            hashMap.put("at", String.valueOf(System.currentTimeMillis()));
            hashMap.put("brand", String.valueOf(Build.BRAND));
            hashMap.put("model", String.valueOf(Build.MODEL));
            hashMap.put("manufacturer", Build.MANUFACTURER);
            hashMap.put("os", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            hashMap.put("os_version", String.valueOf(VERSION.RELEASE));
            hashMap.put("locale", Locale.getDefault().getDisplayLanguage());
            hashMap.put("app_name", m4565g());
            hashMap.put("client_id", this.f2681v);
            hashMap.put("godel_version", SessionInfo.m4587a().m4594b());
            hashMap.put("godel_build_version", SessionInfo.m4587a().m4599d());
            hashMap.put("godel_remotes_version", SessionInfo.m4587a().m4596c());
            hashMap.put("invocation_type", this.f2683y);
            hashMap.put("ip_address", GodelTracker.m4546e());
            if (this.f2682x != null) {
                hashMap.put("device_id", SessionInfo.m4587a().m4604g(this.f2682x));
                hashMap.put("screen_width", SessionInfo.m4587a().m4602e(this.f2682x));
                hashMap.put("screen_height", SessionInfo.m4587a().m4600d(this.f2682x));
                hashMap.put("screen_ppi", SessionInfo.m4587a().m4603f(this.f2682x));
                hashMap.put("network_info", SessionInfo.m4587a().m4590a(this.f2682x));
                hashMap.put("network_type", String.valueOf(SessionInfo.m4587a().m4593b(this.f2682x)));
                hashMap.put("app_version", SessionInfo.m4587a().m4597c(this.f2682x));
                hashMap.put("app_debuggable", String.valueOf((this.f2682x.getApplicationInfo().flags & 2) != 0));
                hashMap.put("dev_options_enabled", String.valueOf(SessionInfo.m4587a().m4605h(this.f2682x)));
            }
            SessionInfo.m4587a();
            hashMap.put("is_rooted", String.valueOf(SessionInfo.m4588e()));
            hashMap.put("log_level", String.valueOf(2));
        } catch (Throwable th) {
            JuspayLogger.m4567a(f2659a, "Exception while creatingSession Data Map", th);
        }
        return hashMap;
    }

    public void m4557a(Map<String, String> map) {
        m4540b((Map) map);
    }

    private void m4540b(Map<String, String> map) {
        map.put("session_id", this.f2672l);
        map.put("bank", this.f2674n);
        int i = this.f2671k + 1;
        this.f2671k = i;
        map.put("sn", String.valueOf(i));
        JuspayLogger.m4568b(f2659a, "Analytics: " + map.toString());
        if (m4544c((Map) map)) {
            f2664f.add(map);
        }
    }

    private boolean m4544c(Map<String, String> map) {
        int intValue;
        boolean z;
        boolean z2 = false;
        if (map.containsKey("log_level")) {
            intValue = Integer.valueOf((String) map.get("log_level")).intValue();
        } else {
            intValue = 0;
        }
        if (intValue < Properties.m4574a().m4584e()) {
            z = false;
        } else {
            z = true;
        }
        if (!m4539a((Map) map, Properties.m4574a().m4583d())) {
            z2 = z;
        }
        if (m4539a((Map) map, Properties.m4574a().m4582c())) {
            return true;
        }
        return z2;
    }

    private boolean m4539a(Map<String, String> map, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (map.containsKey(str) && ((String) map.get(str)).equals(jSONObject.get(str))) {
                        return true;
                    }
                }
                continue;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void m4555a(String str) {
        this.f2681v = str;
    }

    public void m4560b(String str) {
        this.f2674n = str;
    }

    public void m4561c(String str) {
        GodelTracker.m4537a().m4553a(new Event().m4522a(Event.GODEL).m4521a(Event.INFO).m4525b(str));
    }

    public String m4565g() {
        if (this.f2675o == null) {
            this.f2675o = String.valueOf(this.f2682x.getApplicationInfo().loadLabel(this.f2682x.getPackageManager()));
        }
        return this.f2675o;
    }
}
