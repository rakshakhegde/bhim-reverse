package in.juspay.tracker;

import android.util.Log;

/* renamed from: in.juspay.tracker.f */
public class JuspayLogger {
    public static boolean f2684a;
    private static boolean f2685b;

    static {
        f2684a = false;
        f2685b = false;
    }

    public static void m4566a(String str, String str2) {
        if (f2684a) {
            Log.d(str, str2);
        }
    }

    public static void m4568b(String str, String str2) {
        if (f2684a && f2685b) {
            Log.d(str, str2);
        }
    }

    public static void m4567a(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
        GodelTracker.m4537a().m4554a(new ExceptionTracker().m4528a(str2).m4529a(th));
    }
}
