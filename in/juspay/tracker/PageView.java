package in.juspay.tracker;

import java.util.Date;

/* renamed from: in.juspay.tracker.h */
public class PageView {
    public static int f2689h;
    Date f2690a;
    String f2691b;
    String f2692c;
    String f2693d;
    int f2694e;
    long f2695f;
    long f2696g;

    public String toString() {
        return "PageView{at=" + this.f2690a + ", url='" + this.f2691b + '\'' + ", title='" + this.f2692c + '\'' + ", loadTime='" + this.f2693d + '\'' + ", statusCode=" + this.f2694e + ", pageLoadStart=" + this.f2695f + ", pageLoadEnd=" + this.f2696g + ", pageId=" + f2689h + '}';
    }
}
