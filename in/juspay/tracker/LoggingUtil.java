package in.juspay.tracker;

import java.security.SecureRandom;
import java.util.Iterator;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: in.juspay.tracker.g */
public class LoggingUtil {
    private static LoggingUtil f2686b;
    private final String f2687a;
    private JSONObject f2688c;

    public static LoggingUtil m4569a() {
        if (f2686b == null) {
            f2686b = new LoggingUtil();
        }
        return f2686b;
    }

    LoggingUtil() {
        this.f2687a = LoggingUtil.class.getName();
        this.f2688c = null;
    }

    private JSONObject m4570a(String str) {
        try {
            JSONObject g = Properties.m4574a().m4586g();
            if (g == null) {
                return null;
            }
            Iterator keys = g.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                if (str.contains(str2)) {
                    return g.getJSONObject(str2);
                }
            }
            return m4572b(str);
        } catch (Throwable e) {
            JuspayLogger.m4567a(this.f2687a, "Error while getting rules for event " + str, e);
            return null;
        }
    }

    private JSONObject m4572b(String str) {
        try {
            JSONObject jSONObject;
            if (str.contains("account_info")) {
                jSONObject = new JSONObject();
                jSONObject.put("logHashed", true);
                jSONObject.put(CLConstants.FIELD_PAY_INFO_VALUE, 0.0d);
                return jSONObject;
            }
            jSONObject = new JSONObject();
            jSONObject.put("logHashed", false);
            jSONObject.put(CLConstants.FIELD_PAY_INFO_VALUE, 1);
            return jSONObject;
        } catch (Throwable e) {
            JuspayLogger.m4567a(this.f2687a, "Error while getting default rules ", e);
            return null;
        }
    }

    private boolean m4571a(double d) {
        if (d == 1.0d) {
            return true;
        }
        if (d == 0.0d) {
            return false;
        }
        if (((double) new SecureRandom().nextInt(100)) >= 100.0d * d) {
            return false;
        }
        return true;
    }

    public Event m4573a(Event event) {
        try {
            if (event.m4524a() == null) {
                return event;
            }
            JSONObject a = m4570a(event.m4524a());
            if (a == null) {
                return event;
            }
            if (a.optBoolean("logHashed", false) && event.m4526b() != null) {
                event.m4527c(EncryptionHelper.m4509a().m4514b(event.m4526b()));
            }
            if (m4571a(a.optDouble(CLConstants.FIELD_PAY_INFO_VALUE, 0.0d))) {
                return event;
            }
            return null;
        } catch (Throwable e) {
            JuspayLogger.m4567a(this.f2687a, "Error in getLogEvent ", e);
            return null;
        }
    }
}
