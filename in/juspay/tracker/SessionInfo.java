package in.juspay.tracker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* renamed from: in.juspay.tracker.j */
public class SessionInfo {
    private static String f2704a;
    private static SessionInfo f2705f;
    private String f2706b;
    private String f2707c;
    private DisplayMetrics f2708d;
    private String f2709e;
    private Map<String, String> f2710g;
    private String f2711h;

    public SessionInfo() {
        this.f2710g = new HashMap();
    }

    static {
        f2704a = SessionInfo.class.getName();
        f2705f = null;
    }

    public static SessionInfo m4587a() {
        SessionInfo sessionInfo;
        synchronized (SessionInfo.class) {
            if (f2705f == null) {
                f2705f = new SessionInfo();
            }
            sessionInfo = f2705f;
        }
        return sessionInfo;
    }

    public String m4594b() {
        return this.f2706b;
    }

    public void m4591a(String str) {
        this.f2706b = str;
    }

    public String m4596c() {
        return this.f2707c;
    }

    public void m4595b(String str) {
        this.f2707c = str;
    }

    public String m4599d() {
        return this.f2709e;
    }

    public void m4598c(String str) {
        this.f2709e = str;
    }

    public void m4601d(String str) {
        this.f2711h = str;
    }

    public String m4590a(Context context) {
        try {
            context.getSystemService("wifi");
            if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
                return "wifi";
            }
            return "cellular";
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2704a, "Exception trying to get network info", e);
            return null;
        }
    }

    public int m4593b(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkType();
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2704a, "Exception trying to get network type", e);
            return -1;
        }
    }

    public String m4597c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2704a, "Exception trying to getVersionName", e);
            return null;
        }
    }

    private DisplayMetrics m4589i(Context context) {
        try {
            if (this.f2708d == null) {
                this.f2708d = context.getResources().getDisplayMetrics();
            }
            return this.f2708d;
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2704a, "Exception caught trying to get display metrics", e);
            return null;
        }
    }

    public String m4600d(Context context) {
        DisplayMetrics i = m4589i(context);
        if (i != null) {
            return String.valueOf(i.heightPixels);
        }
        return null;
    }

    public String m4602e(Context context) {
        DisplayMetrics i = m4589i(context);
        if (i != null) {
            return String.valueOf(i.widthPixels);
        }
        return null;
    }

    public String m4603f(Context context) {
        DisplayMetrics i = m4589i(context);
        if (i != null) {
            return String.valueOf(i.xdpi);
        }
        return null;
    }

    public String m4604g(Context context) {
        try {
            if (!m4592a(context, "android.permission.READ_PHONE_STATE")) {
                return null;
            }
            return EncryptionHelper.m4509a().m4512a(((TelephonyManager) context.getSystemService("phone")).getDeviceId());
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2704a, "Exception trying to get device id", e);
            return null;
        }
    }

    public boolean m4592a(Context context, String str) {
        try {
            if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
                return true;
            }
            JuspayLogger.m4566a(f2704a, "Permission not found: " + str);
            return false;
        } catch (Throwable th) {
            JuspayLogger.m4567a(f2704a, "Exception trying to fetch permission info: " + str + " returning FALSE", th);
            return false;
        }
    }

    public static boolean m4588e() {
        String str = Build.TAGS;
        if (str != null && str.contains("test-keys")) {
            return true;
        }
        try {
            if (new File("/system/app/Superuser.apk").exists()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean m4605h(Context context) {
        if (context != null) {
            try {
                if (Secure.getInt(context.getContentResolver(), "development_settings_enabled", 0) == 1) {
                    return true;
                }
                return false;
            } catch (Throwable e) {
                JuspayLogger.m4567a(f2704a, "Exception while getting dev options enabled", e);
            }
        }
        return false;
    }
}
