package in.juspay.tracker;

import com.crashlytics.android.core.BuildConfig;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: in.juspay.tracker.i */
public class Properties {
    private static Properties f2697a;
    private JSONObject f2698b;
    private JSONObject f2699c;
    private int f2700d;
    private JSONArray f2701e;
    private JSONArray f2702f;
    private String f2703g;

    public Properties() {
        this.f2698b = new JSONObject();
        this.f2699c = new JSONObject();
        this.f2700d = 0;
        this.f2701e = new JSONArray();
        this.f2702f = new JSONArray();
        this.f2703g = BuildConfig.FLAVOR;
        f2697a = this;
    }

    public static Properties m4574a() {
        if (f2697a == null) {
            f2697a = new Properties();
        }
        return f2697a;
    }

    public String m4579b() {
        return this.f2703g;
    }

    public void m4576a(String str) {
        this.f2703g = str;
    }

    public JSONArray m4582c() {
        return this.f2702f;
    }

    public void m4577a(JSONArray jSONArray) {
        this.f2702f = jSONArray;
    }

    public JSONArray m4583d() {
        return this.f2701e;
    }

    public void m4580b(JSONArray jSONArray) {
        this.f2701e = jSONArray;
    }

    public int m4584e() {
        return this.f2700d;
    }

    public void m4575a(int i) {
        this.f2700d = i;
    }

    public JSONObject m4585f() {
        return this.f2699c;
    }

    public void m4578a(JSONObject jSONObject) {
        this.f2699c = jSONObject;
    }

    public JSONObject m4586g() {
        return this.f2698b;
    }

    public void m4581b(JSONObject jSONObject) {
        this.f2698b = jSONObject;
    }
}
