package in.juspay.tracker;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

/* renamed from: in.juspay.tracker.d */
public class FileUtil {
    private static String f2647a;

    static {
        f2647a = FileUtil.class.toString();
    }

    public static byte[] m4533a(byte[] bArr) {
        try {
            OutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length);
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            byteArrayOutputStream.close();
            gZIPOutputStream.close();
            JuspayLogger.m4566a(f2647a, "Gzipping complete");
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable e) {
            JuspayLogger.m4567a(f2647a, "Could not gzip", e);
            throw new RuntimeException(e);
        }
    }
}
