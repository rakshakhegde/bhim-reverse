package in.juspay.tracker;

import java.util.Date;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: in.juspay.tracker.b */
public class Event {
    Date f2639a;
    String f2640b;
    String f2641c;
    String f2642d;
    String f2643e;

    /* renamed from: in.juspay.tracker.b.a */
    public enum Event {
        CLICK("click"),
        ASYNC("async"),
        PREFERENCES("prefs"),
        CHECK("check"),
        WEBLAB("weblab"),
        FALLBACK("fallback"),
        ERROR(CLConstants.OUTPUT_KEY_ERROR),
        INFO("info"),
        OTHER("other");
        
        private final String f2630j;

        public String m4519a() {
            return this.f2630j;
        }

        private Event(String str) {
            this.f2630j = str;
        }
    }

    /* renamed from: in.juspay.tracker.b.b */
    public enum Event {
        UI("ui"),
        GODEL("godel"),
        ACS("acs"),
        CONFIG("config"),
        UBER("uber"),
        DUI("dui");
        
        private final String f2638g;

        private Event(String str) {
            this.f2638g = str;
        }
    }

    public Event() {
        this.f2639a = new Date();
    }

    public Event m4522a(Event event) {
        this.f2640b = event.f2638g;
        return this;
    }

    public Event m4523a(String str) {
        this.f2641c = str;
        return this;
    }

    public String m4524a() {
        return this.f2642d;
    }

    public Event m4525b(String str) {
        this.f2642d = str;
        return this;
    }

    public String m4526b() {
        return this.f2643e;
    }

    public Event m4527c(String str) {
        this.f2643e = str;
        return this;
    }

    public Event m4521a(Event event) {
        m4523a(event.m4519a());
        return this;
    }
}
