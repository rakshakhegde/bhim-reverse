package in.juspay.tracker;

import java.util.Date;

/* renamed from: in.juspay.tracker.c */
public class ExceptionTracker {
    Date f2644a;
    String f2645b;
    Throwable f2646c;

    public ExceptionTracker() {
        this.f2644a = new Date();
    }

    public String m4530a() {
        return this.f2645b;
    }

    public ExceptionTracker m4528a(String str) {
        this.f2645b = str;
        return this;
    }

    public Throwable m4531b() {
        return this.f2646c;
    }

    public ExceptionTracker m4529a(Throwable th) {
        this.f2646c = th;
        return this;
    }

    public Date m4532c() {
        return this.f2644a;
    }
}
