package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.Rect;
import android.util.Log;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.g */
public class CenterCropStrategy extends PreviewScalingStrategy {
    private static final String f3125a;

    static {
        f3125a = CenterCropStrategy.class.getSimpleName();
    }

    protected float m5058a(Size size, Size size2) {
        if (size.f3185a <= 0 || size.f3186b <= 0) {
            return 0.0f;
        }
        Size b = size.m5123b(size2);
        float f = (((float) b.f3185a) * 1.0f) / ((float) size.f3185a);
        if (f > 1.0f) {
            f = (float) Math.pow((double) (1.0f / f), 1.1d);
        }
        float f2 = ((((float) b.f3186b) * 1.0f) / ((float) size2.f3186b)) + ((((float) b.f3185a) * 1.0f) / ((float) size2.f3185a));
        return f * ((1.0f / f2) / f2);
    }

    public Rect m5059b(Size size, Size size2) {
        Size b = size.m5123b(size2);
        Log.i(f3125a, "Preview: " + size + "; Scaled: " + b + "; Want: " + size2);
        int i = (b.f3185a - size2.f3185a) / 2;
        int i2 = (b.f3186b - size2.f3186b) / 2;
        return new Rect(-i, -i2, b.f3185a - i, b.f3186b - i2);
    }
}
