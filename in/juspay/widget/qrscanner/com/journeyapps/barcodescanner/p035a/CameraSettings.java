package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.d */
public class CameraSettings {
    private int f3107a;
    private boolean f3108b;
    private boolean f3109c;
    private boolean f3110d;
    private boolean f3111e;
    private boolean f3112f;
    private boolean f3113g;
    private boolean f3114h;
    private CameraSettings f3115i;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.d.a */
    public enum CameraSettings {
        AUTO,
        CONTINUOUS,
        INFINITY,
        MACRO
    }

    public CameraSettings() {
        this.f3107a = -1;
        this.f3108b = false;
        this.f3109c = false;
        this.f3110d = false;
        this.f3111e = true;
        this.f3112f = false;
        this.f3113g = false;
        this.f3114h = false;
        this.f3115i = CameraSettings.AUTO;
    }

    public int m5039a() {
        return this.f3107a;
    }

    public boolean m5040b() {
        return this.f3108b;
    }

    public boolean m5041c() {
        return this.f3109c;
    }

    public boolean m5042d() {
        return this.f3113g;
    }

    public boolean m5043e() {
        return this.f3110d;
    }

    public boolean m5044f() {
        return this.f3111e;
    }

    public CameraSettings m5045g() {
        return this.f3115i;
    }

    public boolean m5046h() {
        return this.f3114h;
    }
}
