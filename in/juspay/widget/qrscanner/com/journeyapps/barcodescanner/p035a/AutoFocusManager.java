package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.a */
public final class AutoFocusManager {
    private static final String f3055a;
    private static final Collection<String> f3056h;
    private boolean f3057b;
    private boolean f3058c;
    private final boolean f3059d;
    private final Camera f3060e;
    private Handler f3061f;
    private int f3062g;
    private final Callback f3063i;
    private final AutoFocusCallback f3064j;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.a.1 */
    class AutoFocusManager implements Callback {
        final /* synthetic */ AutoFocusManager f3052a;

        AutoFocusManager(AutoFocusManager autoFocusManager) {
            this.f3052a = autoFocusManager;
        }

        public boolean handleMessage(Message message) {
            if (message.what != this.f3052a.f3062g) {
                return false;
            }
            this.f3052a.m4990d();
            return true;
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.a.2 */
    class AutoFocusManager implements AutoFocusCallback {
        final /* synthetic */ AutoFocusManager f3054a;

        /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.a.2.1 */
        class AutoFocusManager implements Runnable {
            final /* synthetic */ AutoFocusManager f3053a;

            AutoFocusManager(AutoFocusManager autoFocusManager) {
                this.f3053a = autoFocusManager;
            }

            public void run() {
                this.f3053a.f3054a.f3058c = false;
                this.f3053a.f3054a.m4987c();
            }
        }

        AutoFocusManager(AutoFocusManager autoFocusManager) {
            this.f3054a = autoFocusManager;
        }

        public void onAutoFocus(boolean z, Camera camera) {
            this.f3054a.f3061f.post(new AutoFocusManager(this));
        }
    }

    static {
        f3055a = AutoFocusManager.class.getSimpleName();
        f3056h = new ArrayList(2);
        f3056h.add("auto");
        f3056h.add("macro");
    }

    public AutoFocusManager(Camera camera, CameraSettings cameraSettings) {
        boolean z = true;
        this.f3062g = 1;
        this.f3063i = new AutoFocusManager(this);
        this.f3064j = new AutoFocusManager(this);
        this.f3061f = new Handler(this.f3063i);
        this.f3060e = camera;
        String focusMode = camera.getParameters().getFocusMode();
        if (!(cameraSettings.m5044f() && f3056h.contains(focusMode))) {
            z = false;
        }
        this.f3059d = z;
        Log.i(f3055a, "Current focus mode '" + focusMode + "'; use auto focus? " + this.f3059d);
        m4992a();
    }

    private synchronized void m4987c() {
        if (!(this.f3057b || this.f3061f.hasMessages(this.f3062g))) {
            this.f3061f.sendMessageDelayed(this.f3061f.obtainMessage(this.f3062g), 2000);
        }
    }

    public void m4992a() {
        this.f3057b = false;
        m4990d();
    }

    private void m4990d() {
        if (this.f3059d && !this.f3057b && !this.f3058c) {
            try {
                this.f3060e.autoFocus(this.f3064j);
                this.f3058c = true;
            } catch (Throwable e) {
                Log.w(f3055a, "Unexpected exception while focusing", e);
                m4987c();
            }
        }
    }

    private void m4991e() {
        this.f3061f.removeMessages(this.f3062g);
    }

    public void m4993b() {
        this.f3057b = true;
        this.f3058c = false;
        m4991e();
        if (this.f3059d) {
            try {
                this.f3060e.cancelAutoFocus();
            } catch (Throwable e) {
                Log.w(f3055a, "Unexpected exception while cancelling focusing", e);
            }
        }
    }
}
