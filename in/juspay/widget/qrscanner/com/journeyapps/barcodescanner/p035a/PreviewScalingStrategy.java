package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.Rect;
import android.util.Log;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l */
public abstract class PreviewScalingStrategy {
    private static final String f3124a;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l.1 */
    class PreviewScalingStrategy implements Comparator<Size> {
        final /* synthetic */ Size f3133a;
        final /* synthetic */ PreviewScalingStrategy f3134b;

        PreviewScalingStrategy(PreviewScalingStrategy previewScalingStrategy, Size size) {
            this.f3134b = previewScalingStrategy;
            this.f3133a = size;
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m5072a((Size) obj, (Size) obj2);
        }

        public int m5072a(Size size, Size size2) {
            return Float.compare(this.f3134b.m5054a(size2, this.f3133a), this.f3134b.m5054a(size, this.f3133a));
        }
    }

    public abstract Rect m5056b(Size size, Size size2);

    static {
        f3124a = PreviewScalingStrategy.class.getSimpleName();
    }

    public Size m5055a(List<Size> list, Size size) {
        List b = m5057b((List) list, size);
        Log.i(f3124a, "Viewfinder size: " + size);
        Log.i(f3124a, "Preview in order of preference: " + b);
        return (Size) b.get(0);
    }

    public List<Size> m5057b(List<Size> list, Size size) {
        if (size != null) {
            Collections.sort(list, new PreviewScalingStrategy(this, size));
        }
        return list;
    }

    protected float m5054a(Size size, Size size2) {
        return 0.5f;
    }
}
