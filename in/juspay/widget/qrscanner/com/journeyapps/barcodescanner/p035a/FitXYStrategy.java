package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.Rect;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.j */
public class FitXYStrategy extends PreviewScalingStrategy {
    private static final String f3132a;

    static {
        f3132a = FitXYStrategy.class.getSimpleName();
    }

    private static float m5067a(float f) {
        if (f < 1.0f) {
            return 1.0f / f;
        }
        return f;
    }

    protected float m5068a(Size size, Size size2) {
        if (size.f3185a <= 0 || size.f3186b <= 0) {
            return 0.0f;
        }
        float a = (1.0f / FitXYStrategy.m5067a((((float) size.f3185a) * 1.0f) / ((float) size2.f3185a))) / FitXYStrategy.m5067a((((float) size.f3186b) * 1.0f) / ((float) size2.f3186b));
        float a2 = FitXYStrategy.m5067a(((((float) size.f3185a) * 1.0f) / ((float) size.f3186b)) / ((((float) size2.f3185a) * 1.0f) / ((float) size2.f3186b)));
        return a * (((1.0f / a2) / a2) / a2);
    }

    public Rect m5069b(Size size, Size size2) {
        return new Rect(0, 0, size2.f3185a, size2.f3186b);
    }
}
