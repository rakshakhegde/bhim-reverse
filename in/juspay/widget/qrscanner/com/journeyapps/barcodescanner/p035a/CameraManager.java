package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.AmbientLightManager;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.p028a.CameraConfigurationUtils;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.p028a.p029a.OpenCameraInterface;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.SourceData;
import java.util.ArrayList;
import java.util.List;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.c */
public final class CameraManager {
    private static final String f3088a;
    private Camera f3089b;
    private CameraInfo f3090c;
    private AutoFocusManager f3091d;
    private AmbientLightManager f3092e;
    private boolean f3093f;
    private String f3094g;
    private CameraSettings f3095h;
    private DisplayConfiguration f3096i;
    private Size f3097j;
    private Size f3098k;
    private int f3099l;
    private Context f3100m;
    private final CameraManager f3101n;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.c.a */
    private final class CameraManager implements PreviewCallback {
        final /* synthetic */ CameraManager f3085a;
        private PreviewCallback f3086b;
        private Size f3087c;

        public CameraManager(CameraManager cameraManager) {
            this.f3085a = cameraManager;
        }

        public void m5017a(Size size) {
            this.f3087c = size;
        }

        public void m5016a(PreviewCallback previewCallback) {
            this.f3086b = previewCallback;
        }

        public void onPreviewFrame(byte[] bArr, Camera camera) {
            Size size = this.f3087c;
            PreviewCallback previewCallback = this.f3086b;
            if (size == null || previewCallback == null) {
                Log.d(CameraManager.f3088a, "Got preview callback, but no handler or resolution available");
                if (previewCallback != null) {
                    previewCallback.m5071a(new Exception("No resolution available"));
                    return;
                }
                return;
            }
            int previewFormat = camera.getParameters().getPreviewFormat();
            try {
                previewCallback.m5070a(new SourceData(bArr, size.f3185a, size.f3186b, previewFormat, this.f3085a.m5036g()));
            } catch (Exception e) {
                Log.e(CameraManager.f3088a, "Camera preview failed", e);
                previewCallback.m5071a(e);
            }
        }
    }

    static {
        f3088a = CameraManager.class.getSimpleName();
    }

    public CameraManager(Context context) {
        this.f3095h = new CameraSettings();
        this.f3099l = -1;
        this.f3100m = context;
        this.f3101n = new CameraManager(this);
    }

    public void m5025a() {
        this.f3089b = OpenCameraInterface.m4610b(this.f3095h.m5039a());
        Log.d(getClass().getSimpleName(), String.valueOf(this.f3089b));
        if (this.f3089b == null) {
            throw new RuntimeException("Failed to open camera");
        }
        int a = OpenCameraInterface.m4609a(this.f3095h.m5039a());
        this.f3090c = new CameraInfo();
        Camera.getCameraInfo(a, this.f3090c);
    }

    public void m5031b() {
        if (this.f3089b == null) {
            throw new RuntimeException("Camera not open");
        }
        m5024m();
    }

    public void m5027a(CameraSurface cameraSurface) {
        cameraSurface.m5047a(this.f3089b);
    }

    public void m5032c() {
        Camera camera = this.f3089b;
        if (camera != null && !this.f3093f) {
            camera.startPreview();
            this.f3093f = true;
            this.f3091d = new AutoFocusManager(this.f3089b, this.f3095h);
            this.f3092e = new AmbientLightManager(this.f3100m, this, this.f3095h);
            this.f3092e.m4627a();
        }
    }

    public void m5033d() {
        if (this.f3091d != null) {
            this.f3091d.m4993b();
            this.f3091d = null;
        }
        if (this.f3092e != null) {
            this.f3092e.m4628b();
            this.f3092e = null;
        }
        if (this.f3089b != null && this.f3093f) {
            this.f3089b.stopPreview();
            this.f3101n.m5016a(null);
            this.f3093f = false;
        }
    }

    public void m5034e() {
        if (this.f3089b != null) {
            Log.d(f3088a, "before Release");
            this.f3089b.release();
            Log.d(f3088a, "after Release");
            this.f3089b = null;
        }
    }

    public boolean m5035f() {
        if (this.f3099l != -1) {
            return this.f3099l % 180 != 0;
        } else {
            throw new IllegalStateException("Rotation not calculated yet. Call configure() first.");
        }
    }

    public int m5036g() {
        return this.f3099l;
    }

    private Parameters m5022k() {
        Parameters parameters = this.f3089b.getParameters();
        if (this.f3094g == null) {
            this.f3094g = parameters.flatten();
        } else {
            parameters.unflatten(this.f3094g);
        }
        return parameters;
    }

    private void m5020b(boolean z) {
        Parameters k = m5022k();
        if (k == null) {
            Log.w(f3088a, "Device error: no camera parameters are available. Proceeding without configuration.");
            return;
        }
        Log.i(f3088a, "Initial camera parameters: " + k.flatten());
        if (z) {
            Log.w(f3088a, "In camera config safe mode -- most settings will not be honored");
        }
        CameraConfigurationUtils.m4617a(k, this.f3095h.m5045g(), z);
        if (!z) {
            CameraConfigurationUtils.m4618a(k, false);
            if (this.f3095h.m5040b()) {
                CameraConfigurationUtils.m4624f(k);
            }
            if (this.f3095h.m5041c()) {
                CameraConfigurationUtils.m4623e(k);
            }
            if (this.f3095h.m5043e() && VERSION.SDK_INT >= 15) {
                CameraConfigurationUtils.m4622d(k);
                CameraConfigurationUtils.m4619b(k);
                CameraConfigurationUtils.m4621c(k);
            }
        }
        List a = CameraManager.m5018a(k);
        if (a.size() == 0) {
            this.f3097j = null;
        } else {
            this.f3097j = this.f3096i.m5062a(a, m5035f());
            k.setPreviewSize(this.f3097j.f3185a, this.f3097j.f3186b);
        }
        if (Build.DEVICE.equals("glass-1")) {
            CameraConfigurationUtils.m4615a(k);
        }
        Log.i(f3088a, "Final camera parameters: " + k.flatten());
        this.f3089b.setParameters(k);
    }

    private static List<Size> m5018a(Parameters parameters) {
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Size> arrayList = new ArrayList();
        Camera.Size previewSize;
        if (supportedPreviewSizes == null) {
            previewSize = parameters.getPreviewSize();
            if (previewSize != null) {
                arrayList.add(new Size(previewSize.width, previewSize.height));
            }
            return arrayList;
        }
        for (Camera.Size previewSize2 : supportedPreviewSizes) {
            arrayList.add(new Size(previewSize2.width, previewSize2.height));
        }
        return arrayList;
    }

    private int m5023l() {
        int i = 0;
        switch (this.f3096i.m5060a()) {
            case R.View_android_focusable /*1*/:
                i = 90;
                break;
            case R.View_paddingStart /*2*/:
                i = 180;
                break;
            case R.View_paddingEnd /*3*/:
                i = 270;
                break;
        }
        if (this.f3090c.facing == 1) {
            i = (360 - ((i + this.f3090c.orientation) % 360)) % 360;
        } else {
            i = ((this.f3090c.orientation - i) + 360) % 360;
        }
        Log.i(f3088a, "Camera Display Orientation: " + i);
        return i;
    }

    private void m5019a(int i) {
        this.f3089b.setDisplayOrientation(i);
    }

    private void m5024m() {
        try {
            this.f3099l = m5023l();
            m5019a(this.f3099l);
        } catch (Exception e) {
            Log.w(f3088a, "Failed to set rotation.");
        }
        try {
            m5020b(false);
        } catch (Exception e2) {
            try {
                m5020b(true);
            } catch (Exception e3) {
                Log.w(f3088a, "Camera rejected even safe-mode parameters! No configuration");
            }
        }
        Camera.Size previewSize = this.f3089b.getParameters().getPreviewSize();
        if (previewSize == null) {
            this.f3098k = this.f3097j;
        } else {
            this.f3098k = new Size(previewSize.width, previewSize.height);
        }
        this.f3101n.m5017a(this.f3098k);
    }

    public Size m5037h() {
        if (this.f3098k == null) {
            return null;
        }
        if (m5035f()) {
            return this.f3098k.m5121a();
        }
        return this.f3098k;
    }

    public void m5029a(PreviewCallback previewCallback) {
        Camera camera = this.f3089b;
        if (camera != null && this.f3093f) {
            this.f3101n.m5016a(previewCallback);
            camera.setOneShotPreviewCallback(this.f3101n);
        }
    }

    public void m5026a(CameraSettings cameraSettings) {
        this.f3095h = cameraSettings;
    }

    public void m5028a(DisplayConfiguration displayConfiguration) {
        this.f3096i = displayConfiguration;
    }

    public void m5030a(boolean z) {
        if (this.f3089b != null && z != m5038i()) {
            if (this.f3091d != null) {
                this.f3091d.m4993b();
            }
            Parameters parameters = this.f3089b.getParameters();
            CameraConfigurationUtils.m4618a(parameters, z);
            if (this.f3095h.m5042d()) {
                CameraConfigurationUtils.m4620b(parameters, z);
            }
            this.f3089b.setParameters(parameters);
            if (this.f3091d != null) {
                this.f3091d.m4992a();
            }
        }
    }

    public boolean m5038i() {
        Parameters parameters = this.f3089b.getParameters();
        if (parameters == null) {
            return false;
        }
        String flashMode = parameters.getFlashMode();
        if (flashMode == null) {
            return false;
        }
        if ("on".equals(flashMode) || "torch".equals(flashMode)) {
            return true;
        }
        return false;
    }
}
