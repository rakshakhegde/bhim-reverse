package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.Rect;
import android.util.Log;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.i */
public class FitCenterStrategy extends PreviewScalingStrategy {
    private static final String f3131a;

    static {
        f3131a = FitCenterStrategy.class.getSimpleName();
    }

    protected float m5065a(Size size, Size size2) {
        if (size.f3185a <= 0 || size.f3186b <= 0) {
            return 0.0f;
        }
        Size a = size.m5122a(size2);
        float f = (((float) a.f3185a) * 1.0f) / ((float) size.f3185a);
        if (f > 1.0f) {
            f = (float) Math.pow((double) (1.0f / f), 1.1d);
        }
        float f2 = ((((float) size2.f3186b) * 1.0f) / ((float) a.f3186b)) * ((((float) size2.f3185a) * 1.0f) / ((float) a.f3185a));
        return f * (((1.0f / f2) / f2) / f2);
    }

    public Rect m5066b(Size size, Size size2) {
        Size a = size.m5122a(size2);
        Log.i(f3131a, "Preview: " + size + "; Scaled: " + a + "; Want: " + size2);
        int i = (a.f3185a - size2.f3185a) / 2;
        int i2 = (a.f3186b - size2.f3186b) / 2;
        return new Rect(-i, -i2, a.f3185a - i, a.f3186b - i2);
    }
}
