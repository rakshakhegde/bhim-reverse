package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.Rect;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.h */
public class DisplayConfiguration {
    private static final String f3126a;
    private Size f3127b;
    private int f3128c;
    private boolean f3129d;
    private PreviewScalingStrategy f3130e;

    static {
        f3126a = DisplayConfiguration.class.getSimpleName();
    }

    public DisplayConfiguration(int i, Size size) {
        this.f3129d = false;
        this.f3130e = new FitCenterStrategy();
        this.f3128c = i;
        this.f3127b = size;
    }

    public int m5060a() {
        return this.f3128c;
    }

    public void m5064a(PreviewScalingStrategy previewScalingStrategy) {
        this.f3130e = previewScalingStrategy;
    }

    public Size m5063a(boolean z) {
        if (this.f3127b == null) {
            return null;
        }
        if (z) {
            return this.f3127b.m5121a();
        }
        return this.f3127b;
    }

    public Size m5062a(List<Size> list, boolean z) {
        return this.f3130e.m5055a((List) list, m5063a(z));
    }

    public Rect m5061a(Size size) {
        return this.f3130e.m5056b(size, this.f3127b);
    }
}
