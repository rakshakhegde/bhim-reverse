package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Size;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.Util;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b */
public class CameraInstance {
    private static final String f3073a;
    private CameraThread f3074b;
    private CameraSurface f3075c;
    private CameraManager f3076d;
    private Handler f3077e;
    private DisplayConfiguration f3078f;
    private boolean f3079g;
    private CameraSettings f3080h;
    private Runnable f3081i;
    private Runnable f3082j;
    private Runnable f3083k;
    private Runnable f3084l;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.1 */
    class CameraInstance implements Runnable {
        final /* synthetic */ boolean f3065a;
        final /* synthetic */ CameraInstance f3066b;

        CameraInstance(CameraInstance cameraInstance, boolean z) {
            this.f3066b = cameraInstance;
            this.f3065a = z;
        }

        public void run() {
            this.f3066b.f3076d.m5030a(this.f3065a);
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.2 */
    class CameraInstance implements Runnable {
        final /* synthetic */ PreviewCallback f3067a;
        final /* synthetic */ CameraInstance f3068b;

        CameraInstance(CameraInstance cameraInstance, PreviewCallback previewCallback) {
            this.f3068b = cameraInstance;
            this.f3067a = previewCallback;
        }

        public void run() {
            this.f3068b.f3076d.m5029a(this.f3067a);
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.3 */
    class CameraInstance implements Runnable {
        final /* synthetic */ CameraInstance f3069a;

        CameraInstance(CameraInstance cameraInstance) {
            this.f3069a = cameraInstance;
        }

        public void run() {
            try {
                Log.d(CameraInstance.f3073a, "Opening camera");
                this.f3069a.f3076d.m5025a();
            } catch (Throwable e) {
                this.f3069a.m4996a((Exception) e);
                Log.e(CameraInstance.f3073a, "Failed to open camera", e);
            }
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.4 */
    class CameraInstance implements Runnable {
        final /* synthetic */ CameraInstance f3070a;

        CameraInstance(CameraInstance cameraInstance) {
            this.f3070a = cameraInstance;
        }

        public void run() {
            try {
                Log.d(CameraInstance.f3073a, "Configuring camera");
                this.f3070a.f3076d.m5031b();
                if (this.f3070a.f3077e != null) {
                    this.f3070a.f3077e.obtainMessage(R.zxing_prewiew_size_ready, this.f3070a.m5002h()).sendToTarget();
                }
            } catch (Throwable e) {
                this.f3070a.m4996a((Exception) e);
                Log.e(CameraInstance.f3073a, "Failed to configure camera", e);
            }
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.5 */
    class CameraInstance implements Runnable {
        final /* synthetic */ CameraInstance f3071a;

        CameraInstance(CameraInstance cameraInstance) {
            this.f3071a = cameraInstance;
        }

        public void run() {
            try {
                Log.d(CameraInstance.f3073a, "Starting preview");
                this.f3071a.f3076d.m5027a(this.f3071a.f3075c);
                this.f3071a.f3076d.m5032c();
            } catch (Throwable e) {
                this.f3071a.m4996a((Exception) e);
                Log.e(CameraInstance.f3073a, "Failed to start preview", e);
            }
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b.6 */
    class CameraInstance implements Runnable {
        final /* synthetic */ CameraInstance f3072a;

        CameraInstance(CameraInstance cameraInstance) {
            this.f3072a = cameraInstance;
        }

        public void run() {
            try {
                Log.d(CameraInstance.f3073a, "Closing camera");
                this.f3072a.f3076d.m5033d();
                this.f3072a.f3076d.m5034e();
            } catch (Throwable e) {
                Log.e(CameraInstance.f3073a, "Failed to close camera", e);
            }
            this.f3072a.f3074b.m5052b();
        }
    }

    static {
        f3073a = CameraInstance.class.getSimpleName();
    }

    public CameraInstance(Context context) {
        this.f3079g = false;
        this.f3080h = new CameraSettings();
        this.f3081i = new CameraInstance(this);
        this.f3082j = new CameraInstance(this);
        this.f3083k = new CameraInstance(this);
        this.f3084l = new CameraInstance(this);
        Util.m5132a();
        this.f3074b = CameraThread.m5048a();
        this.f3076d = new CameraManager(context);
        this.f3076d.m5026a(this.f3080h);
    }

    public void m5008a(DisplayConfiguration displayConfiguration) {
        this.f3078f = displayConfiguration;
        this.f3076d.m5028a(displayConfiguration);
    }

    public DisplayConfiguration m5004a() {
        return this.f3078f;
    }

    public void m5005a(Handler handler) {
        this.f3077e = handler;
    }

    public void m5007a(CameraSurface cameraSurface) {
        this.f3075c = cameraSurface;
    }

    public void m5006a(CameraSettings cameraSettings) {
        if (!this.f3079g) {
            this.f3080h = cameraSettings;
            this.f3076d.m5026a(cameraSettings);
        }
    }

    private Size m5002h() {
        return this.f3076d.m5037h();
    }

    public void m5011b() {
        Util.m5132a();
        this.f3079g = true;
        this.f3074b.m5053b(this.f3081i);
    }

    public void m5012c() {
        Util.m5132a();
        m5003i();
        this.f3074b.m5051a(this.f3082j);
    }

    public void m5013d() {
        Util.m5132a();
        m5003i();
        this.f3074b.m5051a(this.f3083k);
    }

    public void m5010a(boolean z) {
        Util.m5132a();
        if (this.f3079g) {
            this.f3074b.m5051a(new CameraInstance(this, z));
        }
    }

    public void m5014e() {
        Util.m5132a();
        Log.d(f3073a, "Inside CameraInstance Close");
        if (this.f3079g) {
            this.f3074b.m5051a(this.f3084l);
        }
        this.f3079g = false;
    }

    public boolean m5015f() {
        return this.f3079g;
    }

    public void m5009a(PreviewCallback previewCallback) {
        m5003i();
        this.f3074b.m5051a(new CameraInstance(this, previewCallback));
    }

    private void m5003i() {
        if (!this.f3079g) {
            throw new IllegalStateException("CameraInstance is not open");
        }
    }

    private void m4996a(Exception exception) {
        if (this.f3077e != null) {
            this.f3077e.obtainMessage(R.zxing_camera_error, exception).sendToTarget();
        }
    }
}
