package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build.VERSION;
import android.view.SurfaceHolder;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.e */
public class CameraSurface {
    private SurfaceHolder f3116a;
    private SurfaceTexture f3117b;

    public CameraSurface(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalArgumentException("surfaceHolder may not be null");
        }
        this.f3116a = surfaceHolder;
    }

    public CameraSurface(SurfaceTexture surfaceTexture) {
        if (surfaceTexture == null) {
            throw new IllegalArgumentException("surfaceTexture may not be null");
        }
        this.f3117b = surfaceTexture;
    }

    public void m5047a(Camera camera) {
        if (this.f3116a != null) {
            camera.setPreviewDisplay(this.f3116a);
        } else if (VERSION.SDK_INT >= 11) {
            camera.setPreviewTexture(this.f3117b);
        } else {
            throw new IllegalStateException("SurfaceTexture not supported.");
        }
    }
}
