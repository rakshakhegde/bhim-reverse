package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import android.os.Handler;
import android.os.HandlerThread;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.f */
class CameraThread {
    private static final String f3118a;
    private static CameraThread f3119b;
    private Handler f3120c;
    private HandlerThread f3121d;
    private int f3122e;
    private final Object f3123f;

    static {
        f3118a = CameraThread.class.getSimpleName();
    }

    public static CameraThread m5048a() {
        if (f3119b == null) {
            f3119b = new CameraThread();
        }
        return f3119b;
    }

    private CameraThread() {
        this.f3122e = 0;
        this.f3123f = new Object();
    }

    protected void m5051a(Runnable runnable) {
        synchronized (this.f3123f) {
            m5049c();
            this.f3120c.post(runnable);
        }
    }

    private void m5049c() {
        synchronized (this.f3123f) {
            if (this.f3120c == null) {
                if (this.f3122e <= 0) {
                    throw new IllegalStateException("CameraThread is not open");
                }
                this.f3121d = new HandlerThread("CameraThread");
                this.f3121d.start();
                this.f3120c = new Handler(this.f3121d.getLooper());
            }
        }
    }

    private void m5050d() {
        synchronized (this.f3123f) {
            this.f3121d.quit();
            this.f3121d = null;
            this.f3120c = null;
        }
    }

    protected void m5052b() {
        synchronized (this.f3123f) {
            this.f3122e--;
            if (this.f3122e == 0) {
                m5050d();
            }
        }
    }

    protected void m5053b(Runnable runnable) {
        synchronized (this.f3123f) {
            this.f3122e++;
            m5051a(runnable);
        }
    }
}
