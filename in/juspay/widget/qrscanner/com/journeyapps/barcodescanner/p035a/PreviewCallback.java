package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a;

import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.SourceData;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.k */
public interface PreviewCallback {
    void m5070a(SourceData sourceData);

    void m5071a(Exception exception);
}
