package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.f */
public interface DecoderFactory {
    Decoder m5097a(Map<DecodeHintType, ?> map);
}
