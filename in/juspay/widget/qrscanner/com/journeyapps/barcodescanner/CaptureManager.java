package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.util.Log;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.BeepManager;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.InactivityTimer;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.CameraPreview.CameraPreview;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d */
public class CaptureManager {
    private static final String f3148a;
    private static int f3149b;
    private Activity f3150c;
    private DecoratedBarcodeView f3151d;
    private int f3152e;
    private boolean f3153f;
    private boolean f3154g;
    private InactivityTimer f3155h;
    private BeepManager f3156i;
    private Handler f3157j;
    private BarcodeCallback f3158k;
    private final CameraPreview f3159l;
    private boolean f3160m;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d.1 */
    class CaptureManager implements CameraPreview {
        final /* synthetic */ CaptureManager f3144a;

        CaptureManager(CaptureManager captureManager) {
            this.f3144a = captureManager;
        }

        public void m5079a() {
        }

        public void m5081b() {
        }

        public void m5082c() {
        }

        public void m5080a(Exception exception) {
            this.f3144a.m5091e();
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d.2 */
    class CaptureManager implements Runnable {
        final /* synthetic */ CaptureManager f3145a;

        CaptureManager(CaptureManager captureManager) {
            this.f3145a = captureManager;
        }

        public void run() {
            Log.d(CaptureManager.f3148a, "Finishing due to inactivity");
            this.f3145a.m5085g();
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d.3 */
    class CaptureManager implements OnClickListener {
        final /* synthetic */ CaptureManager f3146a;

        CaptureManager(CaptureManager captureManager) {
            this.f3146a = captureManager;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.f3146a.m5085g();
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d.4 */
    class CaptureManager implements OnCancelListener {
        final /* synthetic */ CaptureManager f3147a;

        CaptureManager(CaptureManager captureManager) {
            this.f3147a = captureManager;
        }

        public void onCancel(DialogInterface dialogInterface) {
            this.f3147a.m5085g();
        }
    }

    static {
        f3148a = CaptureManager.class.getSimpleName();
        f3149b = 250;
    }

    public void m5087a(BarcodeCallback barcodeCallback) {
        this.f3158k = barcodeCallback;
    }

    public CaptureManager(Activity activity, DecoratedBarcodeView decoratedBarcodeView) {
        this.f3152e = -1;
        this.f3153f = false;
        this.f3154g = false;
        this.f3158k = null;
        this.f3159l = new CaptureManager(this);
        this.f3160m = false;
        this.f3150c = activity;
        this.f3151d = decoratedBarcodeView;
        decoratedBarcodeView.getBarcodeView().m4942a(this.f3159l);
        this.f3157j = new Handler();
        this.f3155h = new InactivityTimer(activity, new CaptureManager(this));
        this.f3156i = new BeepManager(activity);
    }

    public void m5086a() {
        this.f3151d.m4970a(this.f3158k);
    }

    public void m5088b() {
        this.f3151d.m4971b();
        this.f3155h.m4639b();
    }

    public void m5089c() {
        this.f3151d.m4969a();
        this.f3155h.m4640c();
    }

    public void m5090d() {
        this.f3154g = true;
        this.f3155h.m4640c();
    }

    private void m5085g() {
        this.f3150c.finish();
    }

    protected void m5091e() {
        if (!this.f3150c.isFinishing() && !this.f3154g) {
            Builder builder = new Builder(this.f3150c);
            builder.setTitle(this.f3150c.getString(R.zxing_app_name));
            builder.setMessage(this.f3150c.getString(R.zxing_msg_camera_framework_bug));
            builder.setPositiveButton(R.zxing_button_ok, new CaptureManager(this));
            builder.setOnCancelListener(new CaptureManager(this));
            builder.show();
        }
    }
}
