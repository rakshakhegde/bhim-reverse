package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.os.Looper;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.n */
public class Util {
    public static void m5132a() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new IllegalStateException("Must be called from the main thread.");
        }
    }
}
