package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.CameraPreview.CameraPreview;
import java.util.ArrayList;
import java.util.List;

public class ViewfinderView extends View {
    protected static final String f3038a;
    protected static final int[] f3039b;
    protected final Paint f3040c;
    protected Bitmap f3041d;
    protected final int f3042e;
    protected final int f3043f;
    protected final int f3044g;
    protected final int f3045h;
    protected int f3046i;
    protected List<ResultPoint> f3047j;
    protected List<ResultPoint> f3048k;
    protected CameraPreview f3049l;
    protected Rect f3050m;
    protected Rect f3051n;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.ViewfinderView.1 */
    class C05611 implements CameraPreview {
        final /* synthetic */ ViewfinderView f3037a;

        C05611(ViewfinderView viewfinderView) {
            this.f3037a = viewfinderView;
        }

        public void m4978a() {
            this.f3037a.m4982a();
            this.f3037a.invalidate();
        }

        public void m4980b() {
        }

        public void m4981c() {
        }

        public void m4979a(Exception exception) {
        }
    }

    static {
        f3038a = ViewfinderView.class.getSimpleName();
        f3039b = new int[]{0, 64, 128, 192, 255, 192, 128, 64};
    }

    public ViewfinderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3040c = new Paint(1);
        Resources resources = getResources();
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.zxing_finder);
        this.f3042e = obtainStyledAttributes.getColor(R.zxing_finder_zxing_viewfinder_mask, resources.getColor(R.zxing_viewfinder_mask));
        this.f3043f = obtainStyledAttributes.getColor(R.zxing_finder_zxing_result_view, resources.getColor(R.zxing_result_view));
        this.f3044g = obtainStyledAttributes.getColor(R.zxing_finder_zxing_viewfinder_laser, resources.getColor(R.zxing_viewfinder_laser));
        this.f3045h = obtainStyledAttributes.getColor(R.zxing_finder_zxing_possible_result_points, resources.getColor(R.zxing_possible_result_points));
        obtainStyledAttributes.recycle();
        this.f3046i = 0;
        this.f3047j = new ArrayList(5);
        this.f3048k = null;
    }

    public void setCameraPreview(CameraPreview cameraPreview) {
        this.f3049l = cameraPreview;
        cameraPreview.m4942a(new C05611(this));
    }

    protected void m4982a() {
        if (this.f3049l != null) {
            Rect framingRect = this.f3049l.getFramingRect();
            Rect previewFramingRect = this.f3049l.getPreviewFramingRect();
            if (framingRect != null && previewFramingRect != null) {
                this.f3050m = framingRect;
                this.f3051n = previewFramingRect;
            }
        }
    }

    @SuppressLint({"DrawAllocation"})
    public void onDraw(Canvas canvas) {
        m4982a();
        if (this.f3050m != null && this.f3051n != null) {
            Rect rect = this.f3050m;
            Rect rect2 = this.f3051n;
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            this.f3040c.setColor(this.f3041d != null ? this.f3043f : this.f3042e);
            canvas.drawRect(0.0f, 0.0f, (float) width, (float) rect.top, this.f3040c);
            canvas.drawRect(0.0f, (float) rect.top, (float) rect.left, (float) (rect.bottom + 1), this.f3040c);
            canvas.drawRect((float) (rect.right + 1), (float) rect.top, (float) width, (float) (rect.bottom + 1), this.f3040c);
            canvas.drawRect(0.0f, (float) (rect.bottom + 1), (float) width, (float) height, this.f3040c);
            if (this.f3041d != null) {
                this.f3040c.setAlpha(160);
                canvas.drawBitmap(this.f3041d, null, rect, this.f3040c);
                return;
            }
            this.f3040c.setColor(this.f3044g);
            this.f3040c.setAlpha(f3039b[this.f3046i]);
            this.f3046i = (this.f3046i + 1) % f3039b.length;
            int height2 = (rect.height() / 2) + rect.top;
            canvas.drawRect((float) (rect.left + 2), (float) (height2 - 1), (float) (rect.right - 1), (float) (height2 + 2), this.f3040c);
            float width2 = ((float) rect.width()) / ((float) rect2.width());
            float height3 = ((float) rect.height()) / ((float) rect2.height());
            List<ResultPoint> list = this.f3047j;
            List<ResultPoint> list2 = this.f3048k;
            int i = rect.left;
            int i2 = rect.top;
            if (list.isEmpty()) {
                this.f3048k = null;
            } else {
                this.f3047j = new ArrayList(5);
                this.f3048k = list;
                this.f3040c.setAlpha(160);
                this.f3040c.setColor(this.f3045h);
                for (ResultPoint resultPoint : list) {
                    canvas.drawCircle((float) (((int) (resultPoint.m4710a() * width2)) + i), (float) (((int) (resultPoint.m4711b() * height3)) + i2), 6.0f, this.f3040c);
                }
            }
            if (list2 != null) {
                this.f3040c.setAlpha(80);
                this.f3040c.setColor(this.f3045h);
                for (ResultPoint resultPoint2 : list2) {
                    canvas.drawCircle((float) (((int) (resultPoint2.m4710a() * width2)) + i), (float) (((int) (resultPoint2.m4711b() * height3)) + i2), 3.0f, this.f3040c);
                }
            }
            postInvalidateDelayed(80, rect.left - 6, rect.top - 6, rect.right + 6, rect.bottom + 6);
        }
    }

    public void m4983a(ResultPoint resultPoint) {
        List list = this.f3047j;
        list.add(resultPoint);
        int size = list.size();
        if (size > 20) {
            list.subList(0, size - 10).clear();
        }
    }
}
