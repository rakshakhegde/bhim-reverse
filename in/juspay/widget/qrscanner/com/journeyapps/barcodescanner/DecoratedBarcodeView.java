package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.TextView;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import java.util.List;

public class DecoratedBarcodeView extends FrameLayout {
    private BarcodeView f3033a;
    private ViewfinderView f3034b;
    private TextView f3035c;
    private C0559a f3036d;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.DecoratedBarcodeView.a */
    public interface C0559a {
        void m4960a();

        void m4961b();
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.DecoratedBarcodeView.b */
    private class C0560b implements BarcodeCallback {
        final /* synthetic */ DecoratedBarcodeView f3031a;
        private BarcodeCallback f3032b;

        public C0560b(DecoratedBarcodeView decoratedBarcodeView, BarcodeCallback barcodeCallback) {
            this.f3031a = decoratedBarcodeView;
            this.f3032b = barcodeCallback;
        }

        public void m4964a(BarcodeResult barcodeResult) {
            this.f3032b.m4962a(barcodeResult);
        }

        public void m4965a(List<ResultPoint> list) {
            for (ResultPoint a : list) {
                this.f3031a.f3034b.m4983a(a);
            }
            this.f3032b.m4963a((List) list);
        }
    }

    public DecoratedBarcodeView(Context context) {
        super(context);
        m4968e();
    }

    public DecoratedBarcodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m4967a(attributeSet);
    }

    public DecoratedBarcodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m4967a(attributeSet);
    }

    private void m4967a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.zxing_view);
        int resourceId = obtainStyledAttributes.getResourceId(R.zxing_view_zxing_scanner_layout, R.zxing_barcode_scanner);
        obtainStyledAttributes.recycle();
        inflate(getContext(), resourceId, this);
        this.f3033a = (BarcodeView) findViewById(R.zxing_barcode_surface);
        if (this.f3033a == null) {
            throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.BarcodeView on provided layout with the id \"zxing_barcode_surface\".");
        }
        this.f3033a.m4941a(attributeSet);
        this.f3034b = (ViewfinderView) findViewById(R.zxing_viewfinder_view);
        if (this.f3034b == null) {
            throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.ViewfinderView on provided layout with the id \"zxing_viewfinder_view\".");
        }
        this.f3034b.setCameraPreview(this.f3033a);
        this.f3035c = (TextView) findViewById(R.zxing_status_view);
    }

    private void m4968e() {
        m4967a(null);
    }

    public void setStatusText(String str) {
        if (this.f3035c != null) {
            this.f3035c.setText(str);
        }
    }

    public void m4969a() {
        this.f3033a.m4959d();
    }

    public void m4971b() {
        this.f3033a.m4945e();
    }

    public BarcodeView getBarcodeView() {
        return (BarcodeView) findViewById(R.zxing_barcode_surface);
    }

    public ViewfinderView getViewFinder() {
        return this.f3034b;
    }

    public TextView getStatusView() {
        return this.f3035c;
    }

    public void m4970a(BarcodeCallback barcodeCallback) {
        this.f3033a.m4956a(new C0560b(this, barcodeCallback));
    }

    public void m4972c() {
        this.f3033a.setTorch(true);
        if (this.f3036d != null) {
            this.f3036d.m4960a();
        }
    }

    public void m4973d() {
        this.f3033a.setTorch(false);
        if (this.f3036d != null) {
            this.f3036d.m4961b();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case org.npci.upi.security.pinactivitycomponent.R.R.Toolbar_subtitleTextColor /*24*/:
                m4972c();
                return true;
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_actionMenuTextAppearance /*25*/:
                m4973d();
                return true;
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_actionModeStyle /*27*/:
            case org.npci.upi.security.pinactivitycomponent.R.R.AppCompatTheme_panelMenuListTheme /*80*/:
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public void setTorchListener(C0559a c0559a) {
        this.f3036d = c0559a;
    }
}
