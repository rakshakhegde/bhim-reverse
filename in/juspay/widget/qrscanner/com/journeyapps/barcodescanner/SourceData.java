package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.graphics.Rect;
import in.juspay.widget.qrscanner.com.google.zxing.PlanarYUVLuminanceSource;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.m */
public class SourceData {
    private byte[] f3187a;
    private int f3188b;
    private int f3189c;
    private int f3190d;
    private int f3191e;
    private Rect f3192f;

    public SourceData(byte[] bArr, int i, int i2, int i3, int i4) {
        this.f3187a = bArr;
        this.f3188b = i;
        this.f3189c = i2;
        this.f3191e = i4;
        this.f3190d = i3;
        if (i * i2 > bArr.length) {
            throw new IllegalArgumentException("Image data does not match the resolution. " + i + "x" + i2 + " > " + bArr.length);
        }
    }

    public void m5129a(Rect rect) {
        this.f3192f = rect;
    }

    public boolean m5130a() {
        return this.f3191e % 180 != 0;
    }

    public PlanarYUVLuminanceSource m5131b() {
        byte[] a = SourceData.m5125a(this.f3191e, this.f3187a, this.f3188b, this.f3189c);
        if (m5130a()) {
            return new PlanarYUVLuminanceSource(a, this.f3189c, this.f3188b, this.f3192f.left, this.f3192f.top, this.f3192f.width(), this.f3192f.height(), false);
        }
        return new PlanarYUVLuminanceSource(a, this.f3188b, this.f3189c, this.f3192f.left, this.f3192f.top, this.f3192f.width(), this.f3192f.height(), false);
    }

    public static byte[] m5125a(int i, byte[] bArr, int i2, int i3) {
        switch (i) {
            case R.AppCompatTheme_controlBackground /*90*/:
                return SourceData.m5126a(bArr, i2, i3);
            case 180:
                return SourceData.m5127b(bArr, i2, i3);
            case 270:
                return SourceData.m5128c(bArr, i2, i3);
            default:
                return bArr;
        }
    }

    public static byte[] m5126a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[(i * i2)];
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            for (int i5 = i2 - 1; i5 >= 0; i5--) {
                bArr2[i3] = bArr[(i5 * i) + i4];
                i3++;
            }
        }
        return bArr2;
    }

    public static byte[] m5127b(byte[] bArr, int i, int i2) {
        int i3 = i * i2;
        byte[] bArr2 = new byte[i3];
        int i4 = i3 - 1;
        for (int i5 = 0; i5 < i3; i5++) {
            bArr2[i4] = bArr[i5];
            i4--;
        }
        return bArr2;
    }

    public static byte[] m5128c(byte[] bArr, int i, int i2) {
        int i3 = i * i2;
        byte[] bArr2 = new byte[i3];
        int i4 = i3 - 1;
        for (int i5 = 0; i5 < i; i5++) {
            for (i3 = i2 - 1; i3 >= 0; i3--) {
                bArr2[i4] = bArr[(i3 * i) + i5];
                i4--;
            }
        }
        return bArr2;
    }
}
