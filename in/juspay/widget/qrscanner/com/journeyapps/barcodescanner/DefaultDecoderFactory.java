package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.BarcodeFormat;
import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.MultiFormatReader;
import in.juspay.widget.qrscanner.com.google.zxing.Reader;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.i */
public class DefaultDecoderFactory implements DecoderFactory {
    private Collection<BarcodeFormat> f3177a;
    private Map<DecodeHintType, ?> f3178b;
    private String f3179c;

    public Decoder m5114a(Map<DecodeHintType, ?> map) {
        Map enumMap = new EnumMap(DecodeHintType.class);
        enumMap.putAll(map);
        if (this.f3178b != null) {
            enumMap.putAll(this.f3178b);
        }
        if (this.f3177a != null) {
            enumMap.put(DecodeHintType.POSSIBLE_FORMATS, this.f3177a);
        }
        if (this.f3179c != null) {
            enumMap.put(DecodeHintType.CHARACTER_SET, this.f3179c);
        }
        Reader multiFormatReader = new MultiFormatReader();
        multiFormatReader.m4913a(enumMap);
        return new Decoder(multiFormatReader);
    }
}
