package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.BinaryBitmap;
import in.juspay.widget.qrscanner.com.google.zxing.LuminanceSource;
import in.juspay.widget.qrscanner.com.google.zxing.MultiFormatReader;
import in.juspay.widget.qrscanner.com.google.zxing.Reader;
import in.juspay.widget.qrscanner.com.google.zxing.Result;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPointCallback;
import in.juspay.widget.qrscanner.com.google.zxing.common.HybridBinarizer;
import java.util.ArrayList;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.e */
public class Decoder implements ResultPointCallback {
    private Reader f3161a;
    private List<ResultPoint> f3162b;

    public Decoder(Reader reader) {
        this.f3162b = new ArrayList();
        this.f3161a = reader;
    }

    public Result m5093a(LuminanceSource luminanceSource) {
        return m5092a(m5096b(luminanceSource));
    }

    protected BinaryBitmap m5096b(LuminanceSource luminanceSource) {
        return new BinaryBitmap(new HybridBinarizer(luminanceSource));
    }

    protected Result m5092a(BinaryBitmap binaryBitmap) {
        this.f3162b.clear();
        Result b;
        try {
            if (this.f3161a instanceof MultiFormatReader) {
                b = ((MultiFormatReader) this.f3161a).m4914b(binaryBitmap);
                return b;
            }
            b = this.f3161a.m4699a(binaryBitmap);
            this.f3161a.m4701a();
            return b;
        } catch (Exception e) {
            b = null;
        } finally {
            this.f3161a.m4701a();
        }
    }

    public List<ResultPoint> m5094a() {
        return new ArrayList(this.f3162b);
    }

    public void m5095a(ResultPoint resultPoint) {
        this.f3162b.add(resultPoint);
    }
}
