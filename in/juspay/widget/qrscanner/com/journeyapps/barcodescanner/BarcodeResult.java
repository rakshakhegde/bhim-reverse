package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.Result;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.b */
public class BarcodeResult {
    protected Result f3135a;
    protected SourceData f3136b;
    private final int f3137c;

    public BarcodeResult(Result result, SourceData sourceData) {
        this.f3137c = 2;
        this.f3135a = result;
        this.f3136b = sourceData;
    }

    public String toString() {
        return this.f3135a.m4918a();
    }
}
