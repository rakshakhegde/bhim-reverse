package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPointCallback;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.g */
public class DecoderResultPointCallback implements ResultPointCallback {
    private Decoder f3163a;

    public void m5099a(Decoder decoder) {
        this.f3163a = decoder;
    }

    public void m5098a(ResultPoint resultPoint) {
        if (this.f3163a != null) {
            this.f3163a.m5095a(resultPoint);
        }
    }
}
