package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l */
public class Size implements Comparable<Size> {
    public final int f3185a;
    public final int f3186b;

    public /* synthetic */ int compareTo(Object obj) {
        return m5124c((Size) obj);
    }

    public Size(int i, int i2) {
        this.f3185a = i;
        this.f3186b = i2;
    }

    public Size m5121a() {
        return new Size(this.f3186b, this.f3185a);
    }

    public Size m5122a(Size size) {
        if (this.f3185a * size.f3186b >= size.f3185a * this.f3186b) {
            return new Size(size.f3185a, (this.f3186b * size.f3185a) / this.f3185a);
        }
        return new Size((this.f3185a * size.f3186b) / this.f3186b, size.f3186b);
    }

    public Size m5123b(Size size) {
        if (this.f3185a * size.f3186b <= size.f3185a * this.f3186b) {
            return new Size(size.f3185a, (this.f3186b * size.f3185a) / this.f3185a);
        }
        return new Size((this.f3185a * size.f3186b) / this.f3186b, size.f3186b);
    }

    public int m5124c(Size size) {
        int i = this.f3186b * this.f3185a;
        int i2 = size.f3186b * size.f3185a;
        if (i2 < i) {
            return 1;
        }
        if (i2 > i) {
            return -1;
        }
        return 0;
    }

    public String toString() {
        return this.f3185a + "x" + this.f3186b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Size size = (Size) obj;
        if (this.f3185a == size.f3185a && this.f3186b == size.f3186b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.f3185a * 31) + this.f3186b;
    }
}
