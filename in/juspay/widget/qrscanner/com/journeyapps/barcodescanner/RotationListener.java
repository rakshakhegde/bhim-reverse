package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.OrientationEventListener;
import android.view.WindowManager;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.k */
public class RotationListener {
    private int f3181a;
    private WindowManager f3182b;
    private OrientationEventListener f3183c;
    private RotationCallback f3184d;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.k.1 */
    class RotationListener extends OrientationEventListener {
        final /* synthetic */ RotationListener f3180a;

        RotationListener(RotationListener rotationListener, Context context, int i) {
            this.f3180a = rotationListener;
            super(context, i);
        }

        public void onOrientationChanged(int i) {
            WindowManager a = this.f3180a.f3182b;
            RotationCallback b = this.f3180a.f3184d;
            if (this.f3180a.f3182b != null && b != null) {
                int rotation = a.getDefaultDisplay().getRotation();
                if (rotation != this.f3180a.f3181a) {
                    this.f3180a.f3181a = rotation;
                    b.m5073a(rotation);
                }
            }
        }
    }

    public void m5120a(Context context, RotationCallback rotationCallback) {
        m5119a();
        Context applicationContext = context.getApplicationContext();
        this.f3184d = rotationCallback;
        this.f3182b = (WindowManager) applicationContext.getSystemService("window");
        this.f3183c = new RotationListener(this, applicationContext, 3);
        this.f3183c.enable();
        this.f3181a = this.f3182b.getDefaultDisplay().getRotation();
    }

    public void m5119a() {
        if (this.f3183c != null) {
            this.f3183c.disable();
        }
        this.f3183c = null;
        this.f3182b = null;
        this.f3184d = null;
    }
}
