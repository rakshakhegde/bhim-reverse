package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.google.zxing.LuminanceSource;
import in.juspay.widget.qrscanner.com.google.zxing.Result;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraInstance;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.PreviewCallback;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.h */
public class DecoderThread {
    private static final String f3166a;
    private CameraInstance f3167b;
    private HandlerThread f3168c;
    private Handler f3169d;
    private Decoder f3170e;
    private Handler f3171f;
    private Rect f3172g;
    private boolean f3173h;
    private final Object f3174i;
    private final Callback f3175j;
    private final PreviewCallback f3176k;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.h.1 */
    class DecoderThread implements Callback {
        final /* synthetic */ DecoderThread f3164a;

        DecoderThread(DecoderThread decoderThread) {
            this.f3164a = decoderThread;
        }

        public boolean handleMessage(Message message) {
            if (message.what == R.zxing_decode) {
                this.f3164a.m5105b((SourceData) message.obj);
            } else if (message.what == R.zxing_preview_failed) {
                this.f3164a.m5106c();
            }
            return true;
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.h.2 */
    class DecoderThread implements PreviewCallback {
        final /* synthetic */ DecoderThread f3165a;

        DecoderThread(DecoderThread decoderThread) {
            this.f3165a = decoderThread;
        }

        public void m5100a(SourceData sourceData) {
            synchronized (this.f3165a.f3174i) {
                if (this.f3165a.f3173h) {
                    this.f3165a.f3169d.obtainMessage(R.zxing_decode, sourceData).sendToTarget();
                }
            }
        }

        public void m5101a(Exception exception) {
            synchronized (this.f3165a.f3174i) {
                if (this.f3165a.f3173h) {
                    this.f3165a.f3169d.obtainMessage(R.zxing_preview_failed).sendToTarget();
                }
            }
        }
    }

    static {
        f3166a = DecoderThread.class.getSimpleName();
    }

    public DecoderThread(CameraInstance cameraInstance, Decoder decoder, Handler handler) {
        this.f3173h = false;
        this.f3174i = new Object();
        this.f3175j = new DecoderThread(this);
        this.f3176k = new DecoderThread(this);
        Util.m5132a();
        this.f3167b = cameraInstance;
        this.f3170e = decoder;
        this.f3171f = handler;
    }

    public void m5112a(Decoder decoder) {
        this.f3170e = decoder;
    }

    public void m5111a(Rect rect) {
        this.f3172g = rect;
    }

    public void m5110a() {
        Util.m5132a();
        this.f3168c = new HandlerThread(f3166a);
        this.f3168c.start();
        this.f3169d = new Handler(this.f3168c.getLooper(), this.f3175j);
        this.f3173h = true;
        m5106c();
    }

    public void m5113b() {
        Util.m5132a();
        synchronized (this.f3174i) {
            this.f3173h = false;
            this.f3169d.removeCallbacksAndMessages(null);
            this.f3168c.quit();
        }
    }

    private void m5106c() {
        if (this.f3167b.m5015f()) {
            this.f3167b.m5009a(this.f3176k);
        }
    }

    protected LuminanceSource m5109a(SourceData sourceData) {
        if (this.f3172g == null) {
            return null;
        }
        return sourceData.m5131b();
    }

    private void m5105b(SourceData sourceData) {
        long currentTimeMillis = System.currentTimeMillis();
        Result result = null;
        sourceData.m5129a(this.f3172g);
        LuminanceSource a = m5109a(sourceData);
        if (a != null) {
            result = this.f3170e.m5093a(a);
        }
        if (result != null) {
            Log.d(f3166a, "Found barcode in " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            if (this.f3171f != null) {
                Message obtain = Message.obtain(this.f3171f, R.zxing_decode_succeeded, new BarcodeResult(result, sourceData));
                obtain.setData(new Bundle());
                obtain.sendToTarget();
            }
        } else if (this.f3171f != null) {
            Message.obtain(this.f3171f, R.zxing_decode_failed).sendToTarget();
        }
        if (this.f3171f != null) {
            Message.obtain(this.f3171f, R.zxing_possible_result_points, this.f3170e.m5094a()).sendToTarget();
        }
        m5106c();
    }
}
