package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraInstance;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraSettings;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraSurface;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CenterCropStrategy;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.DisplayConfiguration;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.FitCenterStrategy;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.FitXYStrategy;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.PreviewScalingStrategy;
import java.util.ArrayList;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c */
public class CameraPreview extends ViewGroup {
    private static final String f2998a;
    private final CameraPreview f2999A;
    private CameraInstance f3000b;
    private WindowManager f3001c;
    private Handler f3002d;
    private boolean f3003e;
    private SurfaceView f3004f;
    private TextureView f3005g;
    private boolean f3006h;
    private RotationListener f3007i;
    private int f3008j;
    private List<CameraPreview> f3009k;
    private DisplayConfiguration f3010l;
    private CameraSettings f3011m;
    private Size f3012n;
    private Size f3013o;
    private Rect f3014p;
    private Size f3015q;
    private Rect f3016r;
    private Rect f3017s;
    private Size f3018t;
    private double f3019u;
    private PreviewScalingStrategy f3020v;
    private boolean f3021w;
    private final Callback f3022x;
    private final Handler.Callback f3023y;
    private RotationCallback f3024z;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.a */
    public interface CameraPreview {
        void m4974a();

        void m4975a(Exception exception);

        void m4976b();

        void m4977c();
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.1 */
    class CameraPreview implements SurfaceTextureListener {
        final /* synthetic */ CameraPreview f3138a;

        CameraPreview(CameraPreview cameraPreview) {
            this.f3138a = cameraPreview;
        }

        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            onSurfaceTextureSizeChanged(surfaceTexture, i, i2);
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
            this.f3138a.f3015q = new Size(i, i2);
            this.f3138a.m4937l();
        }

        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.2 */
    class CameraPreview implements Callback {
        final /* synthetic */ CameraPreview f3139a;

        CameraPreview(CameraPreview cameraPreview) {
            this.f3139a = cameraPreview;
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            this.f3139a.f3015q = null;
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            if (surfaceHolder == null) {
                Log.e(CameraPreview.f2998a, "*** WARNING *** surfaceChanged() gave us a null surface!");
                return;
            }
            this.f3139a.f3015q = new Size(i2, i3);
            this.f3139a.m4937l();
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.3 */
    class CameraPreview implements Handler.Callback {
        final /* synthetic */ CameraPreview f3140a;

        CameraPreview(CameraPreview cameraPreview) {
            this.f3140a = cameraPreview;
        }

        public boolean handleMessage(Message message) {
            if (message.what == R.zxing_prewiew_size_ready) {
                this.f3140a.m4930b((Size) message.obj);
                return true;
            }
            if (message.what == R.zxing_camera_error) {
                Exception exception = (Exception) message.obj;
                if (this.f3140a.m4946f()) {
                    this.f3140a.m4944d();
                    this.f3140a.f2999A.m4975a(exception);
                }
            }
            return false;
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.4 */
    class CameraPreview implements RotationCallback {
        final /* synthetic */ CameraPreview f3142a;

        /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.4.1 */
        class CameraPreview implements Runnable {
            final /* synthetic */ CameraPreview f3141a;

            CameraPreview(CameraPreview cameraPreview) {
                this.f3141a = cameraPreview;
            }

            public void run() {
                this.f3141a.f3142a.m4928b();
            }
        }

        CameraPreview(CameraPreview cameraPreview) {
            this.f3142a = cameraPreview;
        }

        public void m5074a(int i) {
            this.f3142a.f3002d.postDelayed(new CameraPreview(this), 250);
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.c.5 */
    class CameraPreview implements CameraPreview {
        final /* synthetic */ CameraPreview f3143a;

        CameraPreview(CameraPreview cameraPreview) {
            this.f3143a = cameraPreview;
        }

        public void m5075a() {
            for (CameraPreview a : this.f3143a.f3009k) {
                a.m4974a();
            }
        }

        public void m5077b() {
            for (CameraPreview b : this.f3143a.f3009k) {
                b.m4976b();
            }
        }

        public void m5078c() {
            for (CameraPreview c : this.f3143a.f3009k) {
                c.m4977c();
            }
        }

        public void m5076a(Exception exception) {
            for (CameraPreview a : this.f3143a.f3009k) {
                a.m4975a(exception);
            }
        }
    }

    static {
        f2998a = CameraPreview.class.getSimpleName();
    }

    @TargetApi(14)
    private SurfaceTextureListener m4921a() {
        return new CameraPreview(this);
    }

    public CameraPreview(Context context) {
        super(context);
        this.f3003e = false;
        this.f3006h = false;
        this.f3008j = -1;
        this.f3009k = new ArrayList();
        this.f3011m = new CameraSettings();
        this.f3016r = null;
        this.f3017s = null;
        this.f3018t = null;
        this.f3019u = 0.1d;
        this.f3020v = null;
        this.f3021w = false;
        this.f3022x = new CameraPreview(this);
        this.f3023y = new CameraPreview(this);
        this.f3024z = new CameraPreview(this);
        this.f2999A = new CameraPreview(this);
        m4923a(context, null, 0, 0);
    }

    public CameraPreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3003e = false;
        this.f3006h = false;
        this.f3008j = -1;
        this.f3009k = new ArrayList();
        this.f3011m = new CameraSettings();
        this.f3016r = null;
        this.f3017s = null;
        this.f3018t = null;
        this.f3019u = 0.1d;
        this.f3020v = null;
        this.f3021w = false;
        this.f3022x = new CameraPreview(this);
        this.f3023y = new CameraPreview(this);
        this.f3024z = new CameraPreview(this);
        this.f2999A = new CameraPreview(this);
        m4923a(context, attributeSet, 0, 0);
    }

    public CameraPreview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3003e = false;
        this.f3006h = false;
        this.f3008j = -1;
        this.f3009k = new ArrayList();
        this.f3011m = new CameraSettings();
        this.f3016r = null;
        this.f3017s = null;
        this.f3018t = null;
        this.f3019u = 0.1d;
        this.f3020v = null;
        this.f3021w = false;
        this.f3022x = new CameraPreview(this);
        this.f3023y = new CameraPreview(this);
        this.f3024z = new CameraPreview(this);
        this.f2999A = new CameraPreview(this);
        m4923a(context, attributeSet, i, 0);
    }

    private void m4923a(Context context, AttributeSet attributeSet, int i, int i2) {
        if (getBackground() == null) {
            setBackgroundColor(-16777216);
        }
        m4941a(attributeSet);
        this.f3001c = (WindowManager) context.getSystemService("window");
        this.f3002d = new Handler(this.f3023y);
        this.f3007i = new RotationListener();
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        m4935j();
    }

    protected void m4941a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.zxing_camera_preview);
        int dimension = (int) obtainStyledAttributes.getDimension(R.zxing_camera_preview_zxing_framing_rect_width, -1.0f);
        int dimension2 = (int) obtainStyledAttributes.getDimension(R.zxing_camera_preview_zxing_framing_rect_height, -1.0f);
        if (dimension > 0 && dimension2 > 0) {
            this.f3018t = new Size(dimension, dimension2);
        }
        this.f3003e = obtainStyledAttributes.getBoolean(R.zxing_camera_preview_zxing_use_texture_view, true);
        dimension = obtainStyledAttributes.getInteger(R.zxing_camera_preview_zxing_preview_scaling_strategy, -1);
        if (dimension == 1) {
            this.f3020v = new CenterCropStrategy();
        } else if (dimension == 2) {
            this.f3020v = new FitCenterStrategy();
        } else if (dimension == 3) {
            this.f3020v = new FitXYStrategy();
        }
        obtainStyledAttributes.recycle();
    }

    private void m4928b() {
        if (m4946f() && getDisplayRotation() != this.f3008j) {
            m4944d();
            m4945e();
        }
    }

    @SuppressLint({"NewAPI"})
    private void m4935j() {
        if (!this.f3003e || VERSION.SDK_INT < 14) {
            this.f3004f = new SurfaceView(getContext());
            if (VERSION.SDK_INT < 11) {
                this.f3004f.getHolder().setType(3);
            }
            this.f3004f.getHolder().addCallback(this.f3022x);
            addView(this.f3004f);
            return;
        }
        this.f3005g = new TextureView(getContext());
        this.f3005g.setSurfaceTextureListener(m4921a());
        addView(this.f3005g);
    }

    public void m4942a(CameraPreview cameraPreview) {
        this.f3009k.add(cameraPreview);
    }

    private void m4936k() {
        if (this.f3012n == null || this.f3013o == null || this.f3010l == null) {
            this.f3017s = null;
            this.f3016r = null;
            this.f3014p = null;
            throw new IllegalStateException("containerSize or previewSize is not set yet");
        }
        int i = this.f3013o.f3185a;
        int i2 = this.f3013o.f3186b;
        int i3 = this.f3012n.f3185a;
        int i4 = this.f3012n.f3186b;
        this.f3014p = this.f3010l.m5061a(this.f3013o);
        this.f3016r = m4940a(new Rect(0, 0, i3, i4), this.f3014p);
        Rect rect = new Rect(this.f3016r);
        rect.offset(-this.f3014p.left, -this.f3014p.top);
        this.f3017s = new Rect((rect.left * i) / this.f3014p.width(), (rect.top * i2) / this.f3014p.height(), (i * rect.right) / this.f3014p.width(), (i2 * rect.bottom) / this.f3014p.height());
        if (this.f3017s.width() <= 0 || this.f3017s.height() <= 0) {
            this.f3017s = null;
            this.f3016r = null;
            Log.w(f2998a, "Preview frame is too small");
            return;
        }
        this.f2999A.m4974a();
    }

    public void setTorch(boolean z) {
        this.f3021w = z;
        if (this.f3000b != null) {
            this.f3000b.m5010a(z);
        }
    }

    private void m4926a(Size size) {
        this.f3012n = size;
        if (this.f3000b != null && this.f3000b.m5004a() == null) {
            this.f3010l = new DisplayConfiguration(getDisplayRotation(), size);
            this.f3010l.m5064a(getPreviewScalingStrategy());
            this.f3000b.m5008a(this.f3010l);
            this.f3000b.m5012c();
            if (this.f3021w) {
                this.f3000b.m5010a(this.f3021w);
            }
        }
    }

    public void setPreviewScalingStrategy(PreviewScalingStrategy previewScalingStrategy) {
        this.f3020v = previewScalingStrategy;
    }

    public PreviewScalingStrategy getPreviewScalingStrategy() {
        if (this.f3020v != null) {
            return this.f3020v;
        }
        if (this.f3005g != null) {
            return new CenterCropStrategy();
        }
        return new FitCenterStrategy();
    }

    private void m4930b(Size size) {
        this.f3013o = size;
        if (this.f3012n != null) {
            m4936k();
            requestLayout();
            m4937l();
        }
    }

    protected Matrix m4939a(Size size, Size size2) {
        float f = 1.0f;
        float f2 = ((float) size.f3185a) / ((float) size.f3186b);
        float f3 = ((float) size2.f3185a) / ((float) size2.f3186b);
        if (f2 < f3) {
            f2 = f3 / f2;
        } else {
            float f4 = f2 / f3;
            f2 = 1.0f;
            f = f4;
        }
        Matrix matrix = new Matrix();
        matrix.setScale(f2, f);
        matrix.postTranslate((((float) size.f3185a) - (f2 * ((float) size.f3185a))) / 2.0f, (((float) size.f3186b) - (f * ((float) size.f3186b))) / 2.0f);
        return matrix;
    }

    private void m4937l() {
        if (this.f3015q != null && this.f3013o != null && this.f3014p != null) {
            if (this.f3004f != null && this.f3015q.equals(new Size(this.f3014p.width(), this.f3014p.height()))) {
                m4924a(new CameraSurface(this.f3004f.getHolder()));
            } else if (this.f3005g != null && VERSION.SDK_INT >= 14 && this.f3005g.getSurfaceTexture() != null) {
                if (this.f3013o != null) {
                    this.f3005g.setTransform(m4939a(new Size(this.f3005g.getWidth(), this.f3005g.getHeight()), this.f3013o));
                }
                m4924a(new CameraSurface(this.f3005g.getSurfaceTexture()));
            }
        }
    }

    @SuppressLint({"DrawAllocation"})
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        m4926a(new Size(i3 - i, i4 - i2));
        if (this.f3004f != null) {
            if (this.f3014p == null) {
                this.f3004f.layout(0, 0, getWidth(), getHeight());
            } else {
                this.f3004f.layout(this.f3014p.left, this.f3014p.top, this.f3014p.right, this.f3014p.bottom);
            }
        } else if (this.f3005g != null && VERSION.SDK_INT >= 14) {
            this.f3005g.layout(0, 0, getWidth(), getHeight());
        }
    }

    public Rect getFramingRect() {
        return this.f3016r;
    }

    public Rect getPreviewFramingRect() {
        return this.f3017s;
    }

    public CameraSettings getCameraSettings() {
        return this.f3011m;
    }

    public void setCameraSettings(CameraSettings cameraSettings) {
        this.f3011m = cameraSettings;
    }

    public void m4945e() {
        Util.m5132a();
        Log.d(f2998a, "resume()");
        m4938m();
        if (this.f3015q != null) {
            m4937l();
        } else if (this.f3004f != null) {
            this.f3004f.getHolder().addCallback(this.f3022x);
        } else if (this.f3005g != null && VERSION.SDK_INT >= 14) {
            if (this.f3005g.isAvailable()) {
                m4921a().onSurfaceTextureAvailable(this.f3005g.getSurfaceTexture(), this.f3005g.getWidth(), this.f3005g.getHeight());
            } else {
                this.f3005g.setSurfaceTextureListener(m4921a());
            }
        }
        requestLayout();
        this.f3007i.m5120a(getContext(), this.f3024z);
    }

    public void m4944d() {
        Util.m5132a();
        Log.d(f2998a, "pause()");
        this.f3008j = -1;
        if (this.f3000b != null) {
            Log.d(f2998a, "cameraInstance NOT NULL");
            this.f3000b.m5014e();
            this.f3000b = null;
            this.f3006h = false;
        }
        if (this.f3015q == null && this.f3004f != null) {
            this.f3004f.getHolder().removeCallback(this.f3022x);
        }
        if (this.f3015q == null && this.f3005g != null && VERSION.SDK_INT >= 14) {
            this.f3005g.setSurfaceTextureListener(null);
        }
        this.f3012n = null;
        this.f3013o = null;
        this.f3017s = null;
        this.f3007i.m5119a();
        this.f2999A.m4977c();
    }

    public Size getFramingRectSize() {
        return this.f3018t;
    }

    public void setFramingRectSize(Size size) {
        this.f3018t = size;
    }

    public double getMarginFraction() {
        return this.f3019u;
    }

    public void setMarginFraction(double d) {
        if (d >= 0.5d) {
            throw new IllegalArgumentException("The margin fraction must be less than 0.5");
        }
        this.f3019u = d;
    }

    public void setUseTextureView(boolean z) {
        this.f3003e = z;
    }

    protected boolean m4946f() {
        return this.f3000b != null;
    }

    private int getDisplayRotation() {
        return this.f3001c.getDefaultDisplay().getRotation();
    }

    private void m4938m() {
        if (this.f3000b != null) {
            Log.w(f2998a, "initCamera called twice");
            return;
        }
        this.f3000b = m4947g();
        this.f3000b.m5005a(this.f3002d);
        this.f3000b.m5011b();
        this.f3008j = getDisplayRotation();
    }

    protected CameraInstance m4947g() {
        CameraInstance cameraInstance = new CameraInstance(getContext());
        cameraInstance.m5006a(this.f3011m);
        return cameraInstance;
    }

    private void m4924a(CameraSurface cameraSurface) {
        if (!this.f3006h && this.f3000b != null) {
            Log.i(f2998a, "Starting preview");
            this.f3000b.m5007a(cameraSurface);
            this.f3000b.m5013d();
            this.f3006h = true;
            m4943c();
            this.f2999A.m4976b();
        }
    }

    protected void m4943c() {
    }

    public CameraInstance getCameraInstance() {
        return this.f3000b;
    }

    public boolean m4948h() {
        return this.f3006h;
    }

    protected Rect m4940a(Rect rect, Rect rect2) {
        Rect rect3 = new Rect(rect);
        rect3.intersect(rect2);
        if (this.f3018t != null) {
            rect3.inset(Math.max(0, (rect3.width() - this.f3018t.f3185a) / 2), Math.max(0, (rect3.height() - this.f3018t.f3186b) / 2));
        } else {
            int min = (int) Math.min(((double) rect3.width()) * this.f3019u, ((double) rect3.height()) * this.f3019u);
            rect3.inset(min, min);
            if (rect3.height() > rect3.width()) {
                rect3.inset(0, (rect3.height() - rect3.width()) / 2);
            }
        }
        return rect3;
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Parcelable bundle = new Bundle();
        bundle.putParcelable("super", onSaveInstanceState);
        bundle.putBoolean("torch", this.f3021w);
        return bundle;
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable("super"));
            setTorch(bundle.getBoolean("torch"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }
}
