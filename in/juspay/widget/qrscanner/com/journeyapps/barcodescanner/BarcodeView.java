package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.AttributeSet;
import in.juspay.widget.qrscanner.R.R;
import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BarcodeView extends CameraPreview {
    private C0558a f3025a;
    private BarcodeCallback f3026b;
    private DecoderThread f3027c;
    private DecoderFactory f3028d;
    private Handler f3029e;
    private final Callback f3030f;

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.BarcodeView.1 */
    class C05571 implements Callback {
        final /* synthetic */ BarcodeView f2993a;

        C05571(BarcodeView barcodeView) {
            this.f2993a = barcodeView;
        }

        public boolean handleMessage(Message message) {
            if (message.what == R.zxing_decode_succeeded) {
                BarcodeResult barcodeResult = (BarcodeResult) message.obj;
                if (!(barcodeResult == null || this.f2993a.f3026b == null || this.f2993a.f3025a == C0558a.NONE)) {
                    this.f2993a.f3026b.m4962a(barcodeResult);
                    if (this.f2993a.f3025a == C0558a.SINGLE) {
                        this.f2993a.m4955a();
                    }
                }
                return true;
            } else if (message.what == R.zxing_decode_failed) {
                return true;
            } else {
                if (message.what != R.zxing_possible_result_points) {
                    return false;
                }
                List list = (List) message.obj;
                if (!(this.f2993a.f3026b == null || this.f2993a.f3025a == C0558a.NONE)) {
                    this.f2993a.f3026b.m4963a(list);
                }
                return true;
            }
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.BarcodeView.a */
    private enum C0558a {
        NONE,
        SINGLE,
        CONTINUOUS
    }

    public BarcodeView(Context context) {
        super(context);
        this.f3025a = C0558a.NONE;
        this.f3026b = null;
        this.f3030f = new C05571(this);
        m4950a(context, null);
    }

    public BarcodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3025a = C0558a.NONE;
        this.f3026b = null;
        this.f3030f = new C05571(this);
        m4950a(context, attributeSet);
    }

    public BarcodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3025a = C0558a.NONE;
        this.f3026b = null;
        this.f3030f = new C05571(this);
        m4950a(context, attributeSet);
    }

    private void m4950a(Context context, AttributeSet attributeSet) {
        this.f3028d = new DefaultDecoderFactory();
        this.f3029e = new Handler(this.f3030f);
    }

    public void setDecoderFactory(DecoderFactory decoderFactory) {
        Util.m5132a();
        this.f3028d = decoderFactory;
        if (this.f3027c != null) {
            this.f3027c.m5112a(m4952j());
        }
    }

    private Decoder m4952j() {
        if (this.f3028d == null) {
            this.f3028d = m4957b();
        }
        DecoderResultPointCallback decoderResultPointCallback = new DecoderResultPointCallback();
        Map hashMap = new HashMap();
        hashMap.put(DecodeHintType.NEED_RESULT_POINT_CALLBACK, decoderResultPointCallback);
        Decoder a = this.f3028d.m5097a(hashMap);
        decoderResultPointCallback.m5099a(a);
        return a;
    }

    public DecoderFactory getDecoderFactory() {
        return this.f3028d;
    }

    public void m4956a(BarcodeCallback barcodeCallback) {
        this.f3025a = C0558a.SINGLE;
        this.f3026b = barcodeCallback;
        m4953k();
    }

    public void m4955a() {
        this.f3025a = C0558a.NONE;
        this.f3026b = null;
        m4954l();
    }

    protected DecoderFactory m4957b() {
        return new DefaultDecoderFactory();
    }

    private void m4953k() {
        m4954l();
        if (this.f3025a != C0558a.NONE && m4948h()) {
            this.f3027c = new DecoderThread(getCameraInstance(), m4952j(), this.f3029e);
            this.f3027c.m5111a(getPreviewFramingRect());
            this.f3027c.m5110a();
        }
    }

    protected void m4958c() {
        super.m4943c();
        m4953k();
    }

    private void m4954l() {
        if (this.f3027c != null) {
            this.f3027c.m5113b();
            this.f3027c = null;
        }
    }

    public void m4959d() {
        m4954l();
        super.m4944d();
    }
}
