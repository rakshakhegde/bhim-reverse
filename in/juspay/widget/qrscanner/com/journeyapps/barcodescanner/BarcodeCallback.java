package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a */
public interface BarcodeCallback {
    void m4962a(BarcodeResult barcodeResult);

    void m4963a(List<ResultPoint> list);
}
