package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.i */
public interface Reader {
    Result m4699a(BinaryBitmap binaryBitmap);

    Result m4700a(BinaryBitmap binaryBitmap, Map<DecodeHintType, ?> map);

    void m4701a();
}
