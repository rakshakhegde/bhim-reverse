package in.juspay.widget.qrscanner.com.google.zxing;

import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b */
public abstract class Binarizer {
    private final LuminanceSource f2855a;

    public abstract BitMatrix m4817b();

    protected Binarizer(LuminanceSource luminanceSource) {
        this.f2855a = luminanceSource;
    }

    public final LuminanceSource m4816a() {
        return this.f2855a;
    }
}
