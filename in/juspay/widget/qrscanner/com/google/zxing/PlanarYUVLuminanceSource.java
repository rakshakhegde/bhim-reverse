package in.juspay.widget.qrscanner.com.google.zxing;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.h */
public final class PlanarYUVLuminanceSource extends LuminanceSource {
    private final byte[] f2969a;
    private final int f2970b;
    private final int f2971c;
    private final int f2972d;
    private final int f2973e;

    public PlanarYUVLuminanceSource(byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        super(i5, i6);
        if (i3 + i5 > i || i4 + i6 > i2) {
            throw new IllegalArgumentException("Crop rectangle does not fit within image data.");
        }
        this.f2969a = bArr;
        this.f2970b = i;
        this.f2971c = i2;
        this.f2972d = i3;
        this.f2973e = i4;
        if (z) {
            m4915a(i5, i6);
        }
    }

    public byte[] m4917a(int i, byte[] bArr) {
        if (i < 0 || i >= m4908c()) {
            throw new IllegalArgumentException("Requested row is outside the image: " + i);
        }
        int b = m4907b();
        if (bArr == null || bArr.length < b) {
            bArr = new byte[b];
        }
        System.arraycopy(this.f2969a, ((this.f2973e + i) * this.f2970b) + this.f2972d, bArr, 0, b);
        return bArr;
    }

    public byte[] m4916a() {
        int i = 0;
        int b = m4907b();
        int c = m4908c();
        if (b == this.f2970b && c == this.f2971c) {
            return this.f2969a;
        }
        int i2 = b * c;
        byte[] bArr = new byte[i2];
        int i3 = (this.f2973e * this.f2970b) + this.f2972d;
        if (b == this.f2970b) {
            System.arraycopy(this.f2969a, i3, bArr, 0, i2);
            return bArr;
        }
        while (i < c) {
            System.arraycopy(this.f2969a, i3, bArr, i * b, b);
            i3 += this.f2970b;
            i++;
        }
        return bArr;
    }

    private void m4915a(int i, int i2) {
        byte[] bArr = this.f2969a;
        int i3 = this.f2972d + (this.f2973e * this.f2970b);
        for (int i4 = 0; i4 < i2; i4++) {
            int i5 = i3 + (i / 2);
            int i6 = (i3 + i) - 1;
            int i7 = i3;
            while (i7 < i5) {
                byte b = bArr[i7];
                bArr[i7] = bArr[i6];
                bArr[i6] = b;
                i7++;
                i6--;
            }
            i3 += this.f2970b;
        }
    }
}
