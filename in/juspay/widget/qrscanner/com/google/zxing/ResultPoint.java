package in.juspay.widget.qrscanner.com.google.zxing;

import in.juspay.widget.qrscanner.com.google.zxing.common.p034a.MathUtils;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.l */
public class ResultPoint {
    private final float f2813a;
    private final float f2814b;

    public ResultPoint(float f, float f2) {
        this.f2813a = f;
        this.f2814b = f2;
    }

    public final float m4710a() {
        return this.f2813a;
    }

    public final float m4711b() {
        return this.f2814b;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ResultPoint)) {
            return false;
        }
        ResultPoint resultPoint = (ResultPoint) obj;
        if (this.f2813a == resultPoint.f2813a && this.f2814b == resultPoint.f2814b) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return (Float.floatToIntBits(this.f2813a) * 31) + Float.floatToIntBits(this.f2814b);
    }

    public final String toString() {
        return "(" + this.f2813a + ',' + this.f2814b + ')';
    }

    public static void m4709a(ResultPoint[] resultPointArr) {
        ResultPoint resultPoint;
        ResultPoint resultPoint2;
        ResultPoint resultPoint3;
        float a = ResultPoint.m4707a(resultPointArr[0], resultPointArr[1]);
        float a2 = ResultPoint.m4707a(resultPointArr[1], resultPointArr[2]);
        float a3 = ResultPoint.m4707a(resultPointArr[0], resultPointArr[2]);
        if (a2 >= a && a2 >= a3) {
            resultPoint = resultPointArr[0];
            resultPoint2 = resultPointArr[1];
            resultPoint3 = resultPointArr[2];
        } else if (a3 < a2 || a3 < a) {
            resultPoint = resultPointArr[2];
            resultPoint2 = resultPointArr[0];
            resultPoint3 = resultPointArr[1];
        } else {
            resultPoint = resultPointArr[1];
            resultPoint2 = resultPointArr[0];
            resultPoint3 = resultPointArr[2];
        }
        if (ResultPoint.m4708a(resultPoint2, resultPoint, resultPoint3) >= 0.0f) {
            ResultPoint resultPoint4 = resultPoint3;
            resultPoint3 = resultPoint2;
            resultPoint2 = resultPoint4;
        }
        resultPointArr[0] = resultPoint3;
        resultPointArr[1] = resultPoint;
        resultPointArr[2] = resultPoint2;
    }

    public static float m4707a(ResultPoint resultPoint, ResultPoint resultPoint2) {
        return MathUtils.m4819a(resultPoint.f2813a, resultPoint.f2814b, resultPoint2.f2813a, resultPoint2.f2814b);
    }

    private static float m4708a(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3) {
        float f = resultPoint2.f2813a;
        float f2 = resultPoint2.f2814b;
        return ((resultPoint3.f2813a - f) * (resultPoint.f2814b - f2)) - ((resultPoint.f2813a - f) * (resultPoint3.f2814b - f2));
    }
}
