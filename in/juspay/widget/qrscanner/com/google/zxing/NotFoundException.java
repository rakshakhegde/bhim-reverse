package in.juspay.widget.qrscanner.com.google.zxing;

public final class NotFoundException extends ReaderException {
    private static final NotFoundException f2716c;

    static {
        f2716c = new NotFoundException();
        f2716c.setStackTrace(b);
    }

    private NotFoundException() {
    }

    public static NotFoundException m4608a() {
        return f2716c;
    }
}
