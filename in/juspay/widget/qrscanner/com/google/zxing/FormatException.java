package in.juspay.widget.qrscanner.com.google.zxing;

public final class FormatException extends ReaderException {
    private static final FormatException f2715c;

    static {
        f2715c = new FormatException();
        f2715c.setStackTrace(b);
    }

    private FormatException() {
    }

    public static FormatException m4607a() {
        return a ? new FormatException() : f2715c;
    }
}
