package in.juspay.widget.qrscanner.com.google.zxing;

public abstract class ReaderException extends Exception {
    protected static final boolean f2712a;
    protected static final StackTraceElement[] f2713b;

    static {
        boolean z;
        if (System.getProperty("surefire.test.class.path") != null) {
            z = true;
        } else {
            z = false;
        }
        f2712a = z;
        f2713b = new StackTraceElement[0];
    }

    ReaderException() {
    }

    public final synchronized Throwable fillInStackTrace() {
        return null;
    }
}
