package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.f */
public final class QRCode {
    private Mode f2850a;
    private ErrorCorrectionLevel f2851b;
    private Version f2852c;
    private int f2853d;
    private ByteMatrix f2854e;

    public QRCode() {
        this.f2853d = -1;
    }

    public ByteMatrix m4810a() {
        return this.f2854e;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(200);
        stringBuilder.append("<<\n");
        stringBuilder.append(" mode: ");
        stringBuilder.append(this.f2850a);
        stringBuilder.append("\n ecLevel: ");
        stringBuilder.append(this.f2851b);
        stringBuilder.append("\n version: ");
        stringBuilder.append(this.f2852c);
        stringBuilder.append("\n maskPattern: ");
        stringBuilder.append(this.f2853d);
        if (this.f2854e == null) {
            stringBuilder.append("\n matrix: null\n");
        } else {
            stringBuilder.append("\n matrix:\n");
            stringBuilder.append(this.f2854e);
        }
        stringBuilder.append(">>\n");
        return stringBuilder.toString();
    }

    public void m4813a(Mode mode) {
        this.f2850a = mode;
    }

    public void m4812a(ErrorCorrectionLevel errorCorrectionLevel) {
        this.f2851b = errorCorrectionLevel;
    }

    public void m4814a(Version version) {
        this.f2852c = version;
    }

    public void m4811a(int i) {
        this.f2853d = i;
    }

    public void m4815a(ByteMatrix byteMatrix) {
        this.f2854e = byteMatrix;
    }

    public static boolean m4809b(int i) {
        return i >= 0 && i < 8;
    }
}
