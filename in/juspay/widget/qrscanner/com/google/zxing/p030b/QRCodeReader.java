package in.juspay.widget.qrscanner.com.google.zxing.p030b;

import in.juspay.widget.qrscanner.com.google.zxing.BarcodeFormat;
import in.juspay.widget.qrscanner.com.google.zxing.BinaryBitmap;
import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.Reader;
import in.juspay.widget.qrscanner.com.google.zxing.Result;
import in.juspay.widget.qrscanner.com.google.zxing.ResultMetadataType;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.common.DecoderResult;
import in.juspay.widget.qrscanner.com.google.zxing.common.DetectorResult;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Decoder;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.QRCodeDecoderMetaData;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b.Detector;
import java.util.List;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a */
public class QRCodeReader implements Reader {
    private static final ResultPoint[] f2811a;
    private final Decoder f2812b;

    public QRCodeReader() {
        this.f2812b = new Decoder();
    }

    static {
        f2811a = new ResultPoint[0];
    }

    public Result m4704a(BinaryBitmap binaryBitmap) {
        return m4705a(binaryBitmap, null);
    }

    public final Result m4705a(BinaryBitmap binaryBitmap, Map<DecodeHintType, ?> map) {
        DecoderResult a;
        ResultPoint[] b;
        if (map == null || !map.containsKey(DecodeHintType.PURE_BARCODE)) {
            DetectorResult a2 = new Detector(binaryBitmap.m4818a()).m4728a((Map) map);
            a = this.f2812b.m4671a(a2.m4862a(), (Map) map);
            b = a2.m4863b();
        } else {
            a = this.f2812b.m4671a(QRCodeReader.m4703a(binaryBitmap.m4818a()), (Map) map);
            b = f2811a;
        }
        if (a.m4854e() instanceof QRCodeDecoderMetaData) {
            ((QRCodeDecoderMetaData) a.m4854e()).m4682a(b);
        }
        Result result = new Result(a.m4851b(), a.m4850a(), b, BarcodeFormat.QR_CODE);
        List c = a.m4852c();
        if (c != null) {
            result.m4919a(ResultMetadataType.BYTE_SEGMENTS, c);
        }
        String d = a.m4853d();
        if (d != null) {
            result.m4919a(ResultMetadataType.ERROR_CORRECTION_LEVEL, d);
        }
        if (a.m4855f()) {
            result.m4919a(ResultMetadataType.STRUCTURED_APPEND_SEQUENCE, Integer.valueOf(a.m4857h()));
            result.m4919a(ResultMetadataType.STRUCTURED_APPEND_PARITY, Integer.valueOf(a.m4856g()));
        }
        return result;
    }

    public void m4706a() {
    }

    private static BitMatrix m4703a(BitMatrix bitMatrix) {
        int[] a = bitMatrix.m4837a();
        int[] b = bitMatrix.m4839b();
        if (a == null || b == null) {
            throw NotFoundException.m4608a();
        }
        float a2 = QRCodeReader.m4702a(a, bitMatrix);
        int i = a[1];
        int i2 = b[1];
        int i3 = a[0];
        int i4 = b[0];
        if (i3 >= i4 || i >= i2) {
            throw NotFoundException.m4608a();
        }
        if (i2 - i != i4 - i3) {
            i4 = (i2 - i) + i3;
            if (i4 >= bitMatrix.m4840c()) {
                throw NotFoundException.m4608a();
            }
        }
        int round = Math.round(((float) ((i4 - i3) + 1)) / a2);
        int round2 = Math.round(((float) ((i2 - i) + 1)) / a2);
        if (round <= 0 || round2 <= 0) {
            throw NotFoundException.m4608a();
        } else if (round2 != round) {
            throw NotFoundException.m4608a();
        } else {
            int i5 = (int) (a2 / 2.0f);
            int i6 = i + i5;
            i = i3 + i5;
            i4 = (((int) (((float) (round - 1)) * a2)) + i) - i4;
            if (i4 <= 0) {
                i3 = i;
            } else if (i4 > i5) {
                throw NotFoundException.m4608a();
            } else {
                i3 = i - i4;
            }
            i4 = (((int) (((float) (round2 - 1)) * a2)) + i6) - i2;
            if (i4 <= 0) {
                i4 = i6;
            } else if (i4 > i5) {
                throw NotFoundException.m4608a();
            } else {
                i4 = i6 - i4;
            }
            BitMatrix bitMatrix2 = new BitMatrix(round, round2);
            for (i = 0; i < round2; i++) {
                i5 = i4 + ((int) (((float) i) * a2));
                for (i6 = 0; i6 < round; i6++) {
                    if (bitMatrix.m4836a(((int) (((float) i6) * a2)) + i3, i5)) {
                        bitMatrix2.m4838b(i6, i);
                    }
                }
            }
            return bitMatrix2;
        }
    }

    private static float m4702a(int[] iArr, BitMatrix bitMatrix) {
        int d = bitMatrix.m4842d();
        int c = bitMatrix.m4840c();
        int i = iArr[0];
        boolean z = true;
        int i2 = iArr[1];
        int i3 = i;
        int i4 = 0;
        while (i3 < c && i2 < d) {
            boolean z2;
            if (z != bitMatrix.m4836a(i3, i2)) {
                i = i4 + 1;
                if (i == 5) {
                    break;
                }
                boolean z3;
                if (z) {
                    z3 = false;
                } else {
                    z3 = true;
                }
                int i5 = i;
                z2 = z3;
                i4 = i5;
            } else {
                z2 = z;
            }
            i3++;
            i2++;
            z = z2;
        }
        if (i3 != c && i2 != d) {
            return ((float) (i3 - iArr[0])) / 7.0f;
        }
        throw NotFoundException.m4608a();
    }
}
