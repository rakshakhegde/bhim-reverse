package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.h */
public enum Mode {
    TERMINATOR(new int[]{0, 0, 0}, 0),
    NUMERIC(new int[]{10, 12, 14}, 1),
    ALPHANUMERIC(new int[]{9, 11, 13}, 2),
    STRUCTURED_APPEND(new int[]{0, 0, 0}, 3),
    BYTE(new int[]{8, 16, 16}, 4),
    ECI(new int[]{0, 0, 0}, 7),
    KANJI(new int[]{8, 10, 12}, 8),
    FNC1_FIRST_POSITION(new int[]{0, 0, 0}, 5),
    FNC1_SECOND_POSITION(new int[]{0, 0, 0}, 9),
    HANZI(new int[]{8, 10, 12}, 13);
    
    private final int[] f2798k;
    private final int f2799l;

    private Mode(int[] iArr, int i) {
        this.f2798k = iArr;
        this.f2799l = i;
    }

    public static Mode m4679a(int i) {
        switch (i) {
            case R.View_android_theme /*0*/:
                return TERMINATOR;
            case R.View_android_focusable /*1*/:
                return NUMERIC;
            case R.View_paddingStart /*2*/:
                return ALPHANUMERIC;
            case R.View_paddingEnd /*3*/:
                return STRUCTURED_APPEND;
            case R.View_theme /*4*/:
                return BYTE;
            case R.Toolbar_contentInsetStart /*5*/:
                return FNC1_FIRST_POSITION;
            case R.Toolbar_contentInsetLeft /*7*/:
                return ECI;
            case R.Toolbar_contentInsetRight /*8*/:
                return KANJI;
            case R.Toolbar_popupTheme /*9*/:
                return FNC1_SECOND_POSITION;
            case R.Toolbar_titleMarginStart /*13*/:
                return HANZI;
            default:
                throw new IllegalArgumentException();
        }
    }

    public int m4681a(Version version) {
        int a = version.m4693a();
        if (a <= 9) {
            a = 0;
        } else if (a <= 26) {
            a = 1;
        } else {
            a = 2;
        }
        return this.f2798k[a];
    }

    public int m4680a() {
        return this.f2799l;
    }
}
