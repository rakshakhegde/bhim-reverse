package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.g */
final class FormatInformation {
    private static final int[][] f2784a;
    private final ErrorCorrectionLevel f2785b;
    private final byte f2786c;

    static {
        f2784a = new int[][]{new int[]{21522, 0}, new int[]{20773, 1}, new int[]{24188, 2}, new int[]{23371, 3}, new int[]{17913, 4}, new int[]{16590, 5}, new int[]{20375, 6}, new int[]{19104, 7}, new int[]{30660, 8}, new int[]{29427, 9}, new int[]{32170, 10}, new int[]{30877, 11}, new int[]{26159, 12}, new int[]{25368, 13}, new int[]{27713, 14}, new int[]{26998, 15}, new int[]{5769, 16}, new int[]{5054, 17}, new int[]{7399, 18}, new int[]{6608, 19}, new int[]{1890, 20}, new int[]{597, 21}, new int[]{3340, 22}, new int[]{2107, 23}, new int[]{13663, 24}, new int[]{12392, 25}, new int[]{16177, 26}, new int[]{14854, 27}, new int[]{9396, 28}, new int[]{8579, 29}, new int[]{11994, 30}, new int[]{11245, 31}};
    }

    private FormatInformation(int i) {
        this.f2785b = ErrorCorrectionLevel.m4672a((i >> 3) & 3);
        this.f2786c = (byte) (i & 7);
    }

    static int m4674a(int i, int i2) {
        return Integer.bitCount(i ^ i2);
    }

    static FormatInformation m4675b(int i, int i2) {
        FormatInformation c = FormatInformation.m4676c(i, i2);
        return c != null ? c : FormatInformation.m4676c(i ^ 21522, i2 ^ 21522);
    }

    private static FormatInformation m4676c(int i, int i2) {
        int i3 = Integer.MAX_VALUE;
        int[][] iArr = f2784a;
        int length = iArr.length;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            int[] iArr2 = iArr[i4];
            int i6 = iArr2[0];
            if (i6 == i || i6 == i2) {
                return new FormatInformation(iArr2[1]);
            }
            int i7;
            int a = FormatInformation.m4674a(i, i6);
            if (a < i3) {
                i3 = iArr2[1];
            } else {
                a = i3;
                i3 = i5;
            }
            if (i != i2) {
                i5 = FormatInformation.m4674a(i2, i6);
                if (i5 < a) {
                    i3 = iArr2[1];
                    i4++;
                    i7 = i3;
                    i3 = i5;
                    i5 = i7;
                }
            }
            i5 = a;
            i4++;
            i7 = i3;
            i3 = i5;
            i5 = i7;
        }
        if (i3 <= 3) {
            return new FormatInformation(i5);
        }
        return null;
    }

    ErrorCorrectionLevel m4677a() {
        return this.f2785b;
    }

    byte m4678b() {
        return this.f2786c;
    }

    public int hashCode() {
        return (this.f2785b.ordinal() << 3) | this.f2786c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FormatInformation)) {
            return false;
        }
        FormatInformation formatInformation = (FormatInformation) obj;
        if (this.f2785b == formatInformation.f2785b && this.f2786c == formatInformation.f2786c) {
            return true;
        }
        return false;
    }
}
