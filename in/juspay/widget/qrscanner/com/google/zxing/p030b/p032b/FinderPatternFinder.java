package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPointCallback;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.e */
public class FinderPatternFinder {
    private final BitMatrix f2831a;
    private final List<FinderPattern> f2832b;
    private boolean f2833c;
    private final int[] f2834d;
    private final ResultPointCallback f2835e;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.e.a */
    private static final class FinderPatternFinder implements Serializable, Comparator<FinderPattern> {
        private final float f2829a;

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m4733a((FinderPattern) obj, (FinderPattern) obj2);
        }

        private FinderPatternFinder(float f) {
            this.f2829a = f;
        }

        public int m4733a(FinderPattern finderPattern, FinderPattern finderPattern2) {
            if (finderPattern2.m4732d() != finderPattern.m4732d()) {
                return finderPattern2.m4732d() - finderPattern.m4732d();
            }
            float abs = Math.abs(finderPattern2.m4731c() - this.f2829a);
            float abs2 = Math.abs(finderPattern.m4731c() - this.f2829a);
            if (abs < abs2) {
                return 1;
            }
            return abs == abs2 ? 0 : -1;
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.e.b */
    private static final class FinderPatternFinder implements Serializable, Comparator<FinderPattern> {
        private final float f2830a;

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m4734a((FinderPattern) obj, (FinderPattern) obj2);
        }

        private FinderPatternFinder(float f) {
            this.f2830a = f;
        }

        public int m4734a(FinderPattern finderPattern, FinderPattern finderPattern2) {
            float abs = Math.abs(finderPattern2.m4731c() - this.f2830a);
            float abs2 = Math.abs(finderPattern.m4731c() - this.f2830a);
            if (abs < abs2) {
                return -1;
            }
            return abs == abs2 ? 0 : 1;
        }
    }

    public FinderPatternFinder(BitMatrix bitMatrix, ResultPointCallback resultPointCallback) {
        this.f2831a = bitMatrix;
        this.f2832b = new ArrayList();
        this.f2834d = new int[5];
        this.f2835e = resultPointCallback;
    }

    final FinderPatternInfo m4744a(Map<DecodeHintType, ?> map) {
        Object obj = (map == null || !map.containsKey(DecodeHintType.TRY_HARDER)) ? null : 1;
        boolean z = map != null && map.containsKey(DecodeHintType.PURE_BARCODE);
        int d = this.f2831a.m4842d();
        int c = this.f2831a.m4840c();
        int i = (d * 3) / 228;
        if (i < 3 || obj != null) {
            i = 3;
        }
        boolean z2 = false;
        int[] iArr = new int[5];
        int i2 = i - 1;
        int i3 = i;
        while (i2 < d && !r4) {
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            iArr[3] = 0;
            iArr[4] = 0;
            i = 0;
            int i4 = 0;
            while (i4 < c) {
                if (this.f2831a.m4836a(i4, i2)) {
                    if ((i & 1) == 1) {
                        i++;
                    }
                    iArr[i] = iArr[i] + 1;
                } else if ((i & 1) != 0) {
                    iArr[i] = iArr[i] + 1;
                } else if (i != 4) {
                    i++;
                    iArr[i] = iArr[i] + 1;
                } else if (!FinderPatternFinder.m4737a(iArr)) {
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = iArr[4];
                    iArr[3] = 1;
                    iArr[4] = 0;
                    i = 3;
                } else if (m4745a(iArr, i2, i4, z)) {
                    boolean c2;
                    i3 = 2;
                    if (this.f2833c) {
                        c2 = m4742c();
                    } else {
                        i = m4740b();
                        if (i > iArr[2]) {
                            i4 = i2 + ((i - iArr[2]) - 2);
                            i = c - 1;
                        } else {
                            i = i4;
                            i4 = i2;
                        }
                        i2 = i4;
                        i4 = i;
                        c2 = z2;
                    }
                    iArr[0] = 0;
                    iArr[1] = 0;
                    iArr[2] = 0;
                    iArr[3] = 0;
                    iArr[4] = 0;
                    z2 = c2;
                    i = 0;
                } else {
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = iArr[4];
                    iArr[3] = 1;
                    iArr[4] = 0;
                    i = 3;
                }
                i4++;
            }
            if (FinderPatternFinder.m4737a(iArr) && m4745a(iArr, i2, c, z)) {
                i3 = iArr[0];
                if (this.f2833c) {
                    z2 = m4742c();
                }
            }
            i2 += i3;
        }
        ResultPoint[] d2 = m4743d();
        ResultPoint.m4709a(d2);
        return new FinderPatternInfo(d2);
    }

    private static float m4735a(int[] iArr, int i) {
        return ((float) ((i - iArr[4]) - iArr[3])) - (((float) iArr[2]) / 2.0f);
    }

    protected static boolean m4737a(int[] iArr) {
        boolean z = true;
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        float f = ((float) i) / 7.0f;
        float f2 = f / 2.0f;
        if (Math.abs(f - ((float) iArr[0])) >= f2 || Math.abs(f - ((float) iArr[1])) >= f2 || Math.abs((3.0f * f) - ((float) iArr[2])) >= 3.0f * f2 || Math.abs(f - ((float) iArr[3])) >= f2 || Math.abs(f - ((float) iArr[4])) >= f2) {
            z = false;
        }
        return z;
    }

    private int[] m4738a() {
        this.f2834d[0] = 0;
        this.f2834d[1] = 0;
        this.f2834d[2] = 0;
        this.f2834d[3] = 0;
        this.f2834d[4] = 0;
        return this.f2834d;
    }

    private boolean m4736a(int i, int i2, int i3, int i4) {
        int[] a = m4738a();
        int i5 = 0;
        while (i >= i5 && i2 >= i5 && this.f2831a.m4836a(i2 - i5, i - i5)) {
            a[2] = a[2] + 1;
            i5++;
        }
        if (i < i5 || i2 < i5) {
            return false;
        }
        while (i >= i5 && i2 >= i5 && !this.f2831a.m4836a(i2 - i5, i - i5) && a[1] <= i3) {
            a[1] = a[1] + 1;
            i5++;
        }
        if (i < i5 || i2 < i5 || a[1] > i3) {
            return false;
        }
        while (i >= i5 && i2 >= i5 && this.f2831a.m4836a(i2 - i5, i - i5) && a[0] <= i3) {
            a[0] = a[0] + 1;
            i5++;
        }
        if (a[0] > i3) {
            return false;
        }
        int d = this.f2831a.m4842d();
        int c = this.f2831a.m4840c();
        i5 = 1;
        while (i + i5 < d && i2 + i5 < c && this.f2831a.m4836a(i2 + i5, i + i5)) {
            a[2] = a[2] + 1;
            i5++;
        }
        if (i + i5 >= d || i2 + i5 >= c) {
            return false;
        }
        while (i + i5 < d && i2 + i5 < c && !this.f2831a.m4836a(i2 + i5, i + i5) && a[3] < i3) {
            a[3] = a[3] + 1;
            i5++;
        }
        if (i + i5 >= d || i2 + i5 >= c || a[3] >= i3) {
            return false;
        }
        while (i + i5 < d && i2 + i5 < c && this.f2831a.m4836a(i2 + i5, i + i5) && a[4] < i3) {
            a[4] = a[4] + 1;
            i5++;
        }
        if (a[4] >= i3) {
            return false;
        }
        return Math.abs(((((a[0] + a[1]) + a[2]) + a[3]) + a[4]) - i4) < i4 * 2 && FinderPatternFinder.m4737a(a);
    }

    private float m4739b(int i, int i2, int i3, int i4) {
        BitMatrix bitMatrix = this.f2831a;
        int d = bitMatrix.m4842d();
        int[] a = m4738a();
        int i5 = i;
        while (i5 >= 0 && bitMatrix.m4836a(i2, i5)) {
            a[2] = a[2] + 1;
            i5--;
        }
        if (i5 < 0) {
            return Float.NaN;
        }
        while (i5 >= 0 && !bitMatrix.m4836a(i2, i5) && a[1] <= i3) {
            a[1] = a[1] + 1;
            i5--;
        }
        if (i5 < 0 || a[1] > i3) {
            return Float.NaN;
        }
        while (i5 >= 0 && bitMatrix.m4836a(i2, i5) && a[0] <= i3) {
            a[0] = a[0] + 1;
            i5--;
        }
        if (a[0] > i3) {
            return Float.NaN;
        }
        i5 = i + 1;
        while (i5 < d && bitMatrix.m4836a(i2, i5)) {
            a[2] = a[2] + 1;
            i5++;
        }
        if (i5 == d) {
            return Float.NaN;
        }
        while (i5 < d && !bitMatrix.m4836a(i2, i5) && a[3] < i3) {
            a[3] = a[3] + 1;
            i5++;
        }
        if (i5 == d || a[3] >= i3) {
            return Float.NaN;
        }
        while (i5 < d && bitMatrix.m4836a(i2, i5) && a[4] < i3) {
            a[4] = a[4] + 1;
            i5++;
        }
        if (a[4] >= i3 || Math.abs(((((a[0] + a[1]) + a[2]) + a[3]) + a[4]) - i4) * 5 >= i4 * 2 || !FinderPatternFinder.m4737a(a)) {
            return Float.NaN;
        }
        return FinderPatternFinder.m4735a(a, i5);
    }

    private float m4741c(int i, int i2, int i3, int i4) {
        BitMatrix bitMatrix = this.f2831a;
        int c = bitMatrix.m4840c();
        int[] a = m4738a();
        int i5 = i;
        while (i5 >= 0 && bitMatrix.m4836a(i5, i2)) {
            a[2] = a[2] + 1;
            i5--;
        }
        if (i5 < 0) {
            return Float.NaN;
        }
        while (i5 >= 0 && !bitMatrix.m4836a(i5, i2) && a[1] <= i3) {
            a[1] = a[1] + 1;
            i5--;
        }
        if (i5 < 0 || a[1] > i3) {
            return Float.NaN;
        }
        while (i5 >= 0 && bitMatrix.m4836a(i5, i2) && a[0] <= i3) {
            a[0] = a[0] + 1;
            i5--;
        }
        if (a[0] > i3) {
            return Float.NaN;
        }
        i5 = i + 1;
        while (i5 < c && bitMatrix.m4836a(i5, i2)) {
            a[2] = a[2] + 1;
            i5++;
        }
        if (i5 == c) {
            return Float.NaN;
        }
        while (i5 < c && !bitMatrix.m4836a(i5, i2) && a[3] < i3) {
            a[3] = a[3] + 1;
            i5++;
        }
        if (i5 == c || a[3] >= i3) {
            return Float.NaN;
        }
        while (i5 < c && bitMatrix.m4836a(i5, i2) && a[4] < i3) {
            a[4] = a[4] + 1;
            i5++;
        }
        if (a[4] >= i3 || Math.abs(((((a[0] + a[1]) + a[2]) + a[3]) + a[4]) - i4) * 5 >= i4 || !FinderPatternFinder.m4737a(a)) {
            return Float.NaN;
        }
        return FinderPatternFinder.m4735a(a, i5);
    }

    protected final boolean m4745a(int[] iArr, int i, int i2, boolean z) {
        boolean z2 = false;
        int i3 = (((iArr[0] + iArr[1]) + iArr[2]) + iArr[3]) + iArr[4];
        float a = FinderPatternFinder.m4735a(iArr, i2);
        float b = m4739b(i, (int) a, iArr[2], i3);
        if (Float.isNaN(b)) {
            return false;
        }
        float c = m4741c((int) a, (int) b, iArr[2], i3);
        if (Float.isNaN(c)) {
            return false;
        }
        if (z && !m4736a((int) b, (int) c, iArr[2], i3)) {
            return false;
        }
        float f = ((float) i3) / 7.0f;
        for (int i4 = 0; i4 < this.f2832b.size(); i4++) {
            FinderPattern finderPattern = (FinderPattern) this.f2832b.get(i4);
            if (finderPattern.m4729a(f, b, c)) {
                this.f2832b.set(i4, finderPattern.m4730b(b, c, f));
                z2 = true;
                break;
            }
        }
        if (!z2) {
            ResultPoint finderPattern2 = new FinderPattern(c, b, f);
            this.f2832b.add(finderPattern2);
            if (this.f2835e != null) {
                this.f2835e.m4920a(finderPattern2);
            }
        }
        return true;
    }

    private int m4740b() {
        if (this.f2832b.size() <= 1) {
            return 0;
        }
        ResultPoint resultPoint = null;
        for (ResultPoint resultPoint2 : this.f2832b) {
            ResultPoint resultPoint22;
            if (resultPoint22.m4732d() < 2) {
                resultPoint22 = resultPoint;
            } else if (resultPoint != null) {
                this.f2833c = true;
                return ((int) (Math.abs(resultPoint.m4710a() - resultPoint22.m4710a()) - Math.abs(resultPoint.m4711b() - resultPoint22.m4711b()))) / 2;
            }
            resultPoint = resultPoint22;
        }
        return 0;
    }

    private boolean m4742c() {
        float f = 0.0f;
        int size = this.f2832b.size();
        float f2 = 0.0f;
        int i = 0;
        for (FinderPattern finderPattern : this.f2832b) {
            float c;
            int i2;
            if (finderPattern.m4732d() >= 2) {
                c = finderPattern.m4731c() + f2;
                i2 = i + 1;
            } else {
                c = f2;
                i2 = i;
            }
            i = i2;
            f2 = c;
        }
        if (i < 3) {
            return false;
        }
        float f3 = f2 / ((float) size);
        for (FinderPattern finderPattern2 : this.f2832b) {
            f += Math.abs(finderPattern2.m4731c() - f3);
        }
        if (f <= 0.05f * f2) {
            return true;
        }
        return false;
    }

    private FinderPattern[] m4743d() {
        float f = 0.0f;
        int size = this.f2832b.size();
        if (size < 3) {
            throw NotFoundException.m4608a();
        }
        if (size > 3) {
            float c;
            float f2 = 0.0f;
            float f3 = 0.0f;
            for (FinderPattern c2 : this.f2832b) {
                c = c2.m4731c();
                f3 += c;
                f2 = (c * c) + f2;
            }
            f3 /= (float) size;
            c = (float) Math.sqrt((double) ((f2 / ((float) size)) - (f3 * f3)));
            Collections.sort(this.f2832b, new FinderPatternFinder(null));
            float max = Math.max(0.2f * f3, c);
            int i = 0;
            while (i < this.f2832b.size() && this.f2832b.size() > 3) {
                if (Math.abs(((FinderPattern) this.f2832b.get(i)).m4731c() - f3) > max) {
                    this.f2832b.remove(i);
                    i--;
                }
                i++;
            }
        }
        if (this.f2832b.size() > 3) {
            for (FinderPattern c22 : this.f2832b) {
                f += c22.m4731c();
            }
            Collections.sort(this.f2832b, new FinderPatternFinder(null));
            this.f2832b.subList(3, this.f2832b.size()).clear();
        }
        return new FinderPattern[]{(FinderPattern) this.f2832b.get(0), (FinderPattern) this.f2832b.get(1), (FinderPattern) this.f2832b.get(2)};
    }
}
