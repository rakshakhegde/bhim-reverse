package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version.Version;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.b */
final class DataBlock {
    private final int f2764a;
    private final byte[] f2765b;

    private DataBlock(int i, byte[] bArr) {
        this.f2764a = i;
        this.f2765b = bArr;
    }

    static DataBlock[] m4648a(byte[] bArr, Version version, ErrorCorrectionLevel errorCorrectionLevel) {
        if (bArr.length != version.m4696c()) {
            throw new IllegalArgumentException();
        }
        int i;
        Version a = version.m4694a(errorCorrectionLevel);
        Version[] d = a.m4688d();
        int i2 = 0;
        for (Version a2 : d) {
            i2 += a2.m4683a();
        }
        DataBlock[] dataBlockArr = new DataBlock[i2];
        int length = d.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            Version version2 = d[i3];
            i2 = i4;
            i4 = 0;
            while (i4 < version2.m4683a()) {
                int b = version2.m4684b();
                i = i2 + 1;
                dataBlockArr[i2] = new DataBlock(b, new byte[(a.m4685a() + b)]);
                i4++;
                i2 = i;
            }
            i3++;
            i4 = i2;
        }
        i = dataBlockArr[0].f2765b.length;
        i2 = dataBlockArr.length - 1;
        while (i2 >= 0 && dataBlockArr[i2].f2765b.length != i) {
            i2--;
        }
        length = i2 + 1;
        i -= a.m4685a();
        int i5 = 0;
        i2 = 0;
        while (i5 < i) {
            i3 = i2;
            i2 = 0;
            while (i2 < i4) {
                int i6 = i3 + 1;
                dataBlockArr[i2].f2765b[i5] = bArr[i3];
                i2++;
                i3 = i6;
            }
            i5++;
            i2 = i3;
        }
        i3 = length;
        while (i3 < i4) {
            i6 = i2 + 1;
            dataBlockArr[i3].f2765b[i] = bArr[i2];
            i3++;
            i2 = i6;
        }
        int length2 = dataBlockArr[0].f2765b.length;
        while (i < length2) {
            i3 = 0;
            i6 = i2;
            while (i3 < i4) {
                i5 = i6 + 1;
                dataBlockArr[i3].f2765b[i3 < length ? i : i + 1] = bArr[i6];
                i3++;
                i6 = i5;
            }
            i++;
            i2 = i6;
        }
        return dataBlockArr;
    }

    int m4649a() {
        return this.f2764a;
    }

    byte[] m4650b() {
        return this.f2765b;
    }
}
