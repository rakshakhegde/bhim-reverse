package in.juspay.widget.qrscanner.com.google.zxing.p030b;

import in.juspay.widget.qrscanner.com.google.zxing.BarcodeFormat;
import in.juspay.widget.qrscanner.com.google.zxing.EncodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c.ByteMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c.Encoder;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c.QRCode;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b */
public final class QRCodeWriter {
    public BitMatrix m4750a(String str, BarcodeFormat barcodeFormat, int i, int i2, Map<EncodeHintType, ?> map) {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (barcodeFormat != BarcodeFormat.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got " + barcodeFormat);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            ErrorCorrectionLevel errorCorrectionLevel;
            int parseInt;
            ErrorCorrectionLevel errorCorrectionLevel2 = ErrorCorrectionLevel.L;
            if (map != null) {
                if (map.containsKey(EncodeHintType.ERROR_CORRECTION)) {
                    errorCorrectionLevel2 = ErrorCorrectionLevel.valueOf(map.get(EncodeHintType.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(EncodeHintType.MARGIN)) {
                    errorCorrectionLevel = errorCorrectionLevel2;
                    parseInt = Integer.parseInt(map.get(EncodeHintType.MARGIN).toString());
                    return QRCodeWriter.m4749a(Encoder.m4767a(str, errorCorrectionLevel, (Map) map), i, i2, parseInt);
                }
            }
            errorCorrectionLevel = errorCorrectionLevel2;
            parseInt = 4;
            return QRCodeWriter.m4749a(Encoder.m4767a(str, errorCorrectionLevel, (Map) map), i, i2, parseInt);
        }
    }

    private static BitMatrix m4749a(QRCode qRCode, int i, int i2, int i3) {
        ByteMatrix a = qRCode.m4810a();
        if (a == null) {
            throw new IllegalStateException();
        }
        int b = a.m4758b();
        int a2 = a.m4754a();
        int i4 = (i3 * 2) + b;
        int i5 = (i3 * 2) + a2;
        int max = Math.max(i, i4);
        int max2 = Math.max(i2, i5);
        int min = Math.min(max / i4, max2 / i5);
        i5 = (max - (b * min)) / 2;
        i4 = (max2 - (a2 * min)) / 2;
        BitMatrix bitMatrix = new BitMatrix(max, max2);
        max2 = i4;
        for (int i6 = 0; i6 < a2; i6++) {
            max = 0;
            i4 = i5;
            while (max < b) {
                if (a.m4753a(max, i6) == 1) {
                    bitMatrix.m4835a(i4, max2, min, min);
                }
                max++;
                i4 += min;
            }
            max2 += min;
        }
        return bitMatrix;
    }
}
