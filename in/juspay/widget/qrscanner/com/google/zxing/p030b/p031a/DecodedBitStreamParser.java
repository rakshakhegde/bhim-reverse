package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitSource;
import in.juspay.widget.qrscanner.com.google.zxing.common.CharacterSetECI;
import in.juspay.widget.qrscanner.com.google.zxing.common.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.d */
final class DecodedBitStreamParser {
    private static final char[] f2775a;

    static {
        f2775a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static in.juspay.widget.qrscanner.com.google.zxing.common.DecoderResult m4663a(byte[] r11, in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version r12, in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel r13, java.util.Map<in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType, ?> r14) {
        /*
        r0 = new in.juspay.widget.qrscanner.com.google.zxing.common.c;
        r0.<init>(r11);
        r1 = new java.lang.StringBuilder;
        r2 = 50;
        r1.<init>(r2);
        r4 = new java.util.ArrayList;
        r2 = 1;
        r4.<init>(r2);
        r5 = -1;
        r6 = -1;
        r3 = 0;
        r2 = 0;
        r8 = r6;
        r9 = r5;
        r6 = r2;
    L_0x0019:
        r2 = r0.m4844a();	 Catch:{ IllegalArgumentException -> 0x0068 }
        r5 = 4;
        if (r2 >= r5) goto L_0x004c;
    L_0x0020:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.TERMINATOR;	 Catch:{ IllegalArgumentException -> 0x0068 }
        r7 = r2;
    L_0x0023:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.TERMINATOR;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 == r2) goto L_0x00f7;
    L_0x0027:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.FNC1_FIRST_POSITION;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 == r2) goto L_0x002f;
    L_0x002b:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.FNC1_SECOND_POSITION;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r2) goto L_0x0057;
    L_0x002f:
        r6 = 1;
        r2 = r6;
        r5 = r9;
        r6 = r8;
    L_0x0033:
        r8 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.TERMINATOR;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r8) goto L_0x00fc;
    L_0x0037:
        r0 = new in.juspay.widget.qrscanner.com.google.zxing.common.e;
        r2 = r1.toString();
        r1 = r4.isEmpty();
        if (r1 == 0) goto L_0x00ee;
    L_0x0043:
        r3 = 0;
    L_0x0044:
        if (r13 != 0) goto L_0x00f1;
    L_0x0046:
        r4 = 0;
    L_0x0047:
        r1 = r11;
        r0.<init>(r1, r2, r3, r4, r5, r6);
        return r0;
    L_0x004c:
        r2 = 4;
        r2 = r0.m4845a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.m4679a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r7 = r2;
        goto L_0x0023;
    L_0x0057:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.STRUCTURED_APPEND;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r2) goto L_0x007e;
    L_0x005b:
        r2 = r0.m4844a();	 Catch:{ IllegalArgumentException -> 0x0068 }
        r5 = 16;
        if (r2 >= r5) goto L_0x006e;
    L_0x0063:
        r0 = in.juspay.widget.qrscanner.com.google.zxing.FormatException.m4607a();	 Catch:{ IllegalArgumentException -> 0x0068 }
        throw r0;	 Catch:{ IllegalArgumentException -> 0x0068 }
    L_0x0068:
        r0 = move-exception;
        r0 = in.juspay.widget.qrscanner.com.google.zxing.FormatException.m4607a();
        throw r0;
    L_0x006e:
        r2 = 8;
        r9 = r0.m4845a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = 8;
        r8 = r0.m4845a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x007e:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.ECI;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r2) goto L_0x0095;
    L_0x0082:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4662a(r0);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r3 = in.juspay.widget.qrscanner.com.google.zxing.common.CharacterSetECI.m4846a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r3 != 0) goto L_0x0091;
    L_0x008c:
        r0 = in.juspay.widget.qrscanner.com.google.zxing.FormatException.m4607a();	 Catch:{ IllegalArgumentException -> 0x0068 }
        throw r0;	 Catch:{ IllegalArgumentException -> 0x0068 }
    L_0x0091:
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x0095:
        r2 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.HANZI;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r2) goto L_0x00b0;
    L_0x0099:
        r2 = 4;
        r2 = r0.m4845a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r5 = r7.m4681a(r12);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r5 = r0.m4845a(r5);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r10 = 1;
        if (r2 != r10) goto L_0x00ac;
    L_0x00a9:
        in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4664a(r0, r1, r5);	 Catch:{ IllegalArgumentException -> 0x0068 }
    L_0x00ac:
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00b0:
        r2 = r7.m4681a(r12);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r0.m4845a(r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r5 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.NUMERIC;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r5) goto L_0x00c4;
    L_0x00bc:
        in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4668c(r0, r1, r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00c4:
        r5 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.ALPHANUMERIC;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r5) goto L_0x00d0;
    L_0x00c8:
        in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4666a(r0, r1, r2, r6);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00d0:
        r5 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.BYTE;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r5) goto L_0x00dd;
    L_0x00d4:
        r5 = r14;
        in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4665a(r0, r1, r2, r3, r4, r5);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00dd:
        r5 = in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode.KANJI;	 Catch:{ IllegalArgumentException -> 0x0068 }
        if (r7 != r5) goto L_0x00e9;
    L_0x00e1:
        in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.DecodedBitStreamParser.m4667b(r0, r1, r2);	 Catch:{ IllegalArgumentException -> 0x0068 }
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00e9:
        r0 = in.juspay.widget.qrscanner.com.google.zxing.FormatException.m4607a();	 Catch:{ IllegalArgumentException -> 0x0068 }
        throw r0;	 Catch:{ IllegalArgumentException -> 0x0068 }
    L_0x00ee:
        r3 = r4;
        goto L_0x0044;
    L_0x00f1:
        r4 = r13.toString();
        goto L_0x0047;
    L_0x00f7:
        r2 = r6;
        r5 = r9;
        r6 = r8;
        goto L_0x0033;
    L_0x00fc:
        r8 = r6;
        r9 = r5;
        r6 = r2;
        goto L_0x0019;
        */
        throw new UnsupportedOperationException("Method not decompiled: in.juspay.widget.qrscanner.com.google.zxing.b.a.d.a(byte[], in.juspay.widget.qrscanner.com.google.zxing.b.a.j, in.juspay.widget.qrscanner.com.google.zxing.b.a.f, java.util.Map):in.juspay.widget.qrscanner.com.google.zxing.common.e");
    }

    private static void m4664a(BitSource bitSource, StringBuilder stringBuilder, int i) {
        if (i * 13 > bitSource.m4844a()) {
            throw FormatException.m4607a();
        }
        byte[] bArr = new byte[(i * 2)];
        int i2 = 0;
        while (i > 0) {
            int a = bitSource.m4845a(13);
            a = (a % 96) | ((a / 96) << 8);
            if (a < 959) {
                a += 41377;
            } else {
                a += 42657;
            }
            bArr[i2] = (byte) ((a >> 8) & 255);
            bArr[i2 + 1] = (byte) (a & 255);
            i--;
            i2 += 2;
        }
        try {
            stringBuilder.append(new String(bArr, "GB2312"));
        } catch (UnsupportedEncodingException e) {
            throw FormatException.m4607a();
        }
    }

    private static void m4667b(BitSource bitSource, StringBuilder stringBuilder, int i) {
        if (i * 13 > bitSource.m4844a()) {
            throw FormatException.m4607a();
        }
        byte[] bArr = new byte[(i * 2)];
        int i2 = 0;
        while (i > 0) {
            int a = bitSource.m4845a(13);
            a = (a % 192) | ((a / 192) << 8);
            if (a < 7936) {
                a += 33088;
            } else {
                a += 49472;
            }
            bArr[i2] = (byte) (a >> 8);
            bArr[i2 + 1] = (byte) a;
            i--;
            i2 += 2;
        }
        try {
            stringBuilder.append(new String(bArr, "SJIS"));
        } catch (UnsupportedEncodingException e) {
            throw FormatException.m4607a();
        }
    }

    private static void m4665a(BitSource bitSource, StringBuilder stringBuilder, int i, CharacterSetECI characterSetECI, Collection<byte[]> collection, Map<DecodeHintType, ?> map) {
        if (i * 8 > bitSource.m4844a()) {
            throw FormatException.m4607a();
        }
        String a;
        Object obj = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            obj[i2] = (byte) bitSource.m4845a(8);
        }
        if (characterSetECI == null) {
            a = StringUtils.m4878a(obj, map);
        } else {
            a = characterSetECI.name();
        }
        try {
            stringBuilder.append(new String(obj, a));
            collection.add(obj);
        } catch (UnsupportedEncodingException e) {
            throw FormatException.m4607a();
        }
    }

    private static char m4661a(int i) {
        if (i < f2775a.length) {
            return f2775a[i];
        }
        throw FormatException.m4607a();
    }

    private static void m4666a(BitSource bitSource, StringBuilder stringBuilder, int i, boolean z) {
        int length = stringBuilder.length();
        while (i > 1) {
            if (bitSource.m4844a() < 11) {
                throw FormatException.m4607a();
            }
            int a = bitSource.m4845a(11);
            stringBuilder.append(DecodedBitStreamParser.m4661a(a / 45));
            stringBuilder.append(DecodedBitStreamParser.m4661a(a % 45));
            i -= 2;
        }
        if (i == 1) {
            if (bitSource.m4844a() < 6) {
                throw FormatException.m4607a();
            }
            stringBuilder.append(DecodedBitStreamParser.m4661a(bitSource.m4845a(6)));
        }
        if (z) {
            while (length < stringBuilder.length()) {
                if (stringBuilder.charAt(length) == '%') {
                    if (length >= stringBuilder.length() - 1 || stringBuilder.charAt(length + 1) != '%') {
                        stringBuilder.setCharAt(length, '\u001d');
                    } else {
                        stringBuilder.deleteCharAt(length + 1);
                    }
                }
                length++;
            }
        }
    }

    private static void m4668c(BitSource bitSource, StringBuilder stringBuilder, int i) {
        while (i >= 3) {
            if (bitSource.m4844a() < 10) {
                throw FormatException.m4607a();
            }
            int a = bitSource.m4845a(10);
            if (a >= 1000) {
                throw FormatException.m4607a();
            }
            stringBuilder.append(DecodedBitStreamParser.m4661a(a / 100));
            stringBuilder.append(DecodedBitStreamParser.m4661a((a / 10) % 10));
            stringBuilder.append(DecodedBitStreamParser.m4661a(a % 10));
            i -= 3;
        }
        if (i == 2) {
            if (bitSource.m4844a() < 7) {
                throw FormatException.m4607a();
            }
            a = bitSource.m4845a(7);
            if (a >= 100) {
                throw FormatException.m4607a();
            }
            stringBuilder.append(DecodedBitStreamParser.m4661a(a / 10));
            stringBuilder.append(DecodedBitStreamParser.m4661a(a % 10));
        } else if (i != 1) {
        } else {
            if (bitSource.m4844a() < 4) {
                throw FormatException.m4607a();
            }
            a = bitSource.m4845a(4);
            if (a >= 10) {
                throw FormatException.m4607a();
            }
            stringBuilder.append(DecodedBitStreamParser.m4661a(a));
        }
    }

    private static int m4662a(BitSource bitSource) {
        int a = bitSource.m4845a(8);
        if ((a & 128) == 0) {
            return a & 127;
        }
        if ((a & 192) == 128) {
            return ((a & 63) << 8) | bitSource.m4845a(8);
        } else if ((a & 224) == 192) {
            return ((a & 31) << 16) | bitSource.m4845a(16);
        } else {
            throw FormatException.m4607a();
        }
    }
}
