package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.f */
public enum ErrorCorrectionLevel {
    L(1),
    M(0),
    Q(3),
    H(2);
    
    private static final ErrorCorrectionLevel[] f2781e;
    private final int f2783f;

    static {
        f2781e = new ErrorCorrectionLevel[]{M, L, H, Q};
    }

    private ErrorCorrectionLevel(int i) {
        this.f2783f = i;
    }

    public int m4673a() {
        return this.f2783f;
    }

    public static ErrorCorrectionLevel m4672a(int i) {
        if (i >= 0 && i < f2781e.length) {
            return f2781e[i];
        }
        throw new IllegalArgumentException();
    }
}
