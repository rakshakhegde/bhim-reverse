package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.d */
public final class FinderPattern extends ResultPoint {
    private final float f2827a;
    private final int f2828b;

    FinderPattern(float f, float f2, float f3) {
        this(f, f2, f3, 1);
    }

    private FinderPattern(float f, float f2, float f3, int i) {
        super(f, f2);
        this.f2827a = f3;
        this.f2828b = i;
    }

    public float m4731c() {
        return this.f2827a;
    }

    int m4732d() {
        return this.f2828b;
    }

    boolean m4729a(float f, float f2, float f3) {
        if (Math.abs(f2 - m4711b()) > f || Math.abs(f3 - m4710a()) > f) {
            return false;
        }
        float abs = Math.abs(f - this.f2827a);
        if (abs <= 1.0f || abs <= this.f2827a) {
            return true;
        }
        return false;
    }

    FinderPattern m4730b(float f, float f2, float f3) {
        int i = this.f2828b + 1;
        return new FinderPattern(((((float) this.f2828b) * m4710a()) + f2) / ((float) i), ((((float) this.f2828b) * m4711b()) + f) / ((float) i), ((((float) this.f2828b) * this.f2827a) + f3) / ((float) i), i);
    }
}
