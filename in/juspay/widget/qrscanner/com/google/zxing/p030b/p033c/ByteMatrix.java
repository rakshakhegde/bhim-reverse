package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

import java.lang.reflect.Array;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.b */
public final class ByteMatrix {
    private final byte[][] f2841a;
    private final int f2842b;
    private final int f2843c;

    public ByteMatrix(int i, int i2) {
        this.f2841a = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{i2, i});
        this.f2842b = i;
        this.f2843c = i2;
    }

    public int m4754a() {
        return this.f2843c;
    }

    public int m4758b() {
        return this.f2842b;
    }

    public byte m4753a(int i, int i2) {
        return this.f2841a[i2][i];
    }

    public byte[][] m4759c() {
        return this.f2841a;
    }

    public void m4756a(int i, int i2, int i3) {
        this.f2841a[i2][i] = (byte) i3;
    }

    public void m4757a(int i, int i2, boolean z) {
        this.f2841a[i2][i] = (byte) (z ? 1 : 0);
    }

    public void m4755a(byte b) {
        for (int i = 0; i < this.f2843c; i++) {
            for (int i2 = 0; i2 < this.f2842b; i2++) {
                this.f2841a[i][i2] = b;
            }
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(((this.f2842b * 2) * this.f2843c) + 2);
        for (int i = 0; i < this.f2843c; i++) {
            for (int i2 = 0; i2 < this.f2842b; i2++) {
                switch (this.f2841a[i][i2]) {
                    case R.View_android_theme /*0*/:
                        stringBuilder.append(" 0");
                        break;
                    case R.View_android_focusable /*1*/:
                        stringBuilder.append(" 1");
                        break;
                    default:
                        stringBuilder.append("  ");
                        break;
                }
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }
}
