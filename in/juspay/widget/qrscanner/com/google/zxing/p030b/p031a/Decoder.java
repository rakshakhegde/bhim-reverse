package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.ChecksumException;
import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.ReaderException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.common.DecoderResult;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.GenericGF;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.ReedSolomonException;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.e */
public final class Decoder {
    private final ReedSolomonDecoder f2776a;

    public Decoder() {
        this.f2776a = new ReedSolomonDecoder(GenericGF.f2929e);
    }

    public DecoderResult m4671a(BitMatrix bitMatrix, Map<DecodeHintType, ?> map) {
        DecoderResult a;
        ChecksumException checksumException;
        FormatException e;
        ReaderException readerException = null;
        BitMatrixParser bitMatrixParser = new BitMatrixParser(bitMatrix);
        try {
            a = m4669a(bitMatrixParser, (Map) map);
        } catch (FormatException e2) {
            FormatException formatException = e2;
            checksumException = readerException;
            try {
                bitMatrixParser.m4646d();
                bitMatrixParser.m4643a(true);
                bitMatrixParser.m4644b();
                bitMatrixParser.m4642a();
                bitMatrixParser.m4647e();
                a = m4669a(bitMatrixParser, (Map) map);
                a.m4849a(new QRCodeDecoderMetaData(true));
                return a;
            } catch (FormatException e3) {
                e = e3;
                if (formatException != null) {
                    throw formatException;
                } else if (checksumException == null) {
                    throw checksumException;
                } else {
                    throw e;
                }
            } catch (ChecksumException e4) {
                e = e4;
                if (formatException != null) {
                    throw formatException;
                } else if (checksumException == null) {
                    throw e;
                } else {
                    throw checksumException;
                }
            }
        } catch (ChecksumException e5) {
            checksumException = e5;
            ReaderException readerException2 = readerException;
            bitMatrixParser.m4646d();
            bitMatrixParser.m4643a(true);
            bitMatrixParser.m4644b();
            bitMatrixParser.m4642a();
            bitMatrixParser.m4647e();
            a = m4669a(bitMatrixParser, (Map) map);
            a.m4849a(new QRCodeDecoderMetaData(true));
            return a;
        }
        return a;
    }

    private DecoderResult m4669a(BitMatrixParser bitMatrixParser, Map<DecodeHintType, ?> map) {
        Version b = bitMatrixParser.m4644b();
        ErrorCorrectionLevel a = bitMatrixParser.m4642a().m4677a();
        DataBlock[] a2 = DataBlock.m4648a(bitMatrixParser.m4645c(), b, a);
        int i = 0;
        for (DataBlock a3 : a2) {
            i += a3.m4649a();
        }
        byte[] bArr = new byte[i];
        int length = a2.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            DataBlock dataBlock = a2[i2];
            byte[] b2 = dataBlock.m4650b();
            int a4 = dataBlock.m4649a();
            m4670a(b2, a4);
            i = i3;
            i3 = 0;
            while (i3 < a4) {
                int i4 = i + 1;
                bArr[i] = b2[i3];
                i3++;
                i = i4;
            }
            i2++;
            i3 = i;
        }
        return DecodedBitStreamParser.m4663a(bArr, b, a, (Map) map);
    }

    private void m4670a(byte[] bArr, int i) {
        int i2 = 0;
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i3 = 0; i3 < length; i3++) {
            iArr[i3] = bArr[i3] & 255;
        }
        try {
            this.f2776a.m4902a(iArr, bArr.length - i);
            while (i2 < i) {
                bArr[i2] = (byte) iArr[i2];
                i2++;
            }
        } catch (ReedSolomonException e) {
            throw ChecksumException.m4606a();
        }
    }
}
