package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.a */
final class BlockPair {
    private final byte[] f2839a;
    private final byte[] f2840b;

    BlockPair(byte[] bArr, byte[] bArr2) {
        this.f2839a = bArr;
        this.f2840b = bArr2;
    }

    public byte[] m4751a() {
        return this.f2839a;
    }

    public byte[] m4752b() {
        return this.f2840b;
    }
}
