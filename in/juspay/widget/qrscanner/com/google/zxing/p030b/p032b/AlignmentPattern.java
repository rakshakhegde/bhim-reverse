package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.a */
public final class AlignmentPattern extends ResultPoint {
    private final float f2815a;

    AlignmentPattern(float f, float f2, float f3) {
        super(f, f2);
        this.f2815a = f3;
    }

    boolean m4712a(float f, float f2, float f3) {
        if (Math.abs(f2 - m4711b()) > f || Math.abs(f3 - m4710a()) > f) {
            return false;
        }
        float abs = Math.abs(f - this.f2815a);
        if (abs <= 1.0f || abs <= this.f2815a) {
            return true;
        }
        return false;
    }

    AlignmentPattern m4713b(float f, float f2, float f3) {
        return new AlignmentPattern((m4710a() + f2) / 2.0f, (m4711b() + f) / 2.0f, (this.f2815a + f3) / 2.0f);
    }
}
