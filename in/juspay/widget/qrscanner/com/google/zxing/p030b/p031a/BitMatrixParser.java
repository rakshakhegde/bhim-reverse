package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.a */
final class BitMatrixParser {
    private final BitMatrix f2760a;
    private Version f2761b;
    private FormatInformation f2762c;
    private boolean f2763d;

    BitMatrixParser(BitMatrix bitMatrix) {
        int d = bitMatrix.m4842d();
        if (d < 21 || (d & 3) != 1) {
            throw FormatException.m4607a();
        }
        this.f2760a = bitMatrix;
    }

    FormatInformation m4642a() {
        int i = 0;
        if (this.f2762c != null) {
            return this.f2762c;
        }
        int i2;
        int i3 = 0;
        for (i2 = 0; i2 < 6; i2++) {
            i3 = m4641a(i2, 8, i3);
        }
        i3 = m4641a(8, 7, m4641a(8, 8, m4641a(7, 8, i3)));
        for (i2 = 5; i2 >= 0; i2--) {
            i3 = m4641a(8, i2, i3);
        }
        int d = this.f2760a.m4842d();
        int i4 = d - 7;
        for (i2 = d - 1; i2 >= i4; i2--) {
            i = m4641a(8, i2, i);
        }
        for (i2 = d - 8; i2 < d; i2++) {
            i = m4641a(i2, 8, i);
        }
        this.f2762c = FormatInformation.m4675b(i3, i);
        if (this.f2762c != null) {
            return this.f2762c;
        }
        throw FormatException.m4607a();
    }

    Version m4644b() {
        if (this.f2761b != null) {
            return this.f2761b;
        }
        int d = this.f2760a.m4842d();
        int i = (d - 17) / 4;
        if (i <= 6) {
            return Version.m4690b(i);
        }
        int i2 = d - 11;
        int i3 = 0;
        for (int i4 = 5; i4 >= 0; i4--) {
            for (i = d - 9; i >= i2; i--) {
                i3 = m4641a(i, i4, i3);
            }
        }
        Version c = Version.m4691c(i3);
        if (c == null || c.m4697d() != d) {
            int i5 = 0;
            for (int i6 = 5; i6 >= 0; i6--) {
                for (i = d - 9; i >= i2; i--) {
                    i5 = m4641a(i6, i, i5);
                }
            }
            c = Version.m4691c(i5);
            if (c == null || c.m4697d() != d) {
                throw FormatException.m4607a();
            }
            this.f2761b = c;
            return c;
        }
        this.f2761b = c;
        return c;
    }

    private int m4641a(int i, int i2, int i3) {
        return this.f2763d ? this.f2760a.m4836a(i2, i) : this.f2760a.m4836a(i, i2) ? (i3 << 1) | 1 : i3 << 1;
    }

    byte[] m4645c() {
        FormatInformation a = m4642a();
        Version b = m4644b();
        DataMask dataMask = DataMask.values()[a.m4678b()];
        int d = this.f2760a.m4842d();
        dataMask.m4651a(this.f2760a, d);
        BitMatrix e = b.m4698e();
        byte[] bArr = new byte[b.m4696c()];
        int i = d - 1;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i > 0) {
            if (i == 6) {
                i--;
            }
            for (int i6 = 0; i6 < d; i6++) {
                int i7;
                if (i5 != 0) {
                    i7 = (d - 1) - i6;
                } else {
                    i7 = i6;
                }
                for (int i8 = 0; i8 < 2; i8++) {
                    if (!e.m4836a(i - i8, i7)) {
                        i2++;
                        i3 <<= 1;
                        if (this.f2760a.m4836a(i - i8, i7)) {
                            i3 |= 1;
                        }
                        if (i2 == 8) {
                            i2 = i4 + 1;
                            bArr[i4] = (byte) i3;
                            i3 = 0;
                            i4 = i2;
                            i2 = 0;
                        }
                    }
                }
            }
            i -= 2;
            i5 ^= 1;
        }
        if (i4 == b.m4696c()) {
            return bArr;
        }
        throw FormatException.m4607a();
    }

    void m4646d() {
        if (this.f2762c != null) {
            DataMask.values()[this.f2762c.m4678b()].m4651a(this.f2760a, this.f2760a.m4842d());
        }
    }

    void m4643a(boolean z) {
        this.f2761b = null;
        this.f2762c = null;
        this.f2763d = z;
    }

    void m4647e() {
        for (int i = 0; i < this.f2760a.m4840c(); i++) {
            for (int i2 = i + 1; i2 < this.f2760a.m4842d(); i2++) {
                if (this.f2760a.m4836a(i, i2) != this.f2760a.m4836a(i2, i)) {
                    this.f2760a.m4841c(i2, i);
                    this.f2760a.m4841c(i, i2);
                }
            }
        }
    }
}
