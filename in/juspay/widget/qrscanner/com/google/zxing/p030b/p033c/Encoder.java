package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

import in.juspay.widget.qrscanner.com.google.zxing.EncodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.WriterException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitArray;
import in.juspay.widget.qrscanner.com.google.zxing.common.CharacterSetECI;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.GenericGF;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.ReedSolomonEncoder;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Mode;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.c */
public final class Encoder {
    private static final int[] f2845a;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.c.1 */
    static /* synthetic */ class Encoder {
        static final /* synthetic */ int[] f2844a;

        static {
            f2844a = new int[Mode.values().length];
            try {
                f2844a[Mode.NUMERIC.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2844a[Mode.ALPHANUMERIC.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2844a[Mode.BYTE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f2844a[Mode.KANJI.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    static {
        f2845a = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};
    }

    private static int m4762a(ByteMatrix byteMatrix) {
        return ((MaskUtil.m4782a(byteMatrix) + MaskUtil.m4787b(byteMatrix)) + MaskUtil.m4788c(byteMatrix)) + MaskUtil.m4789d(byteMatrix);
    }

    public static QRCode m4767a(String str, ErrorCorrectionLevel errorCorrectionLevel, Map<EncodeHintType, ?> map) {
        Version a;
        String str2 = "ISO-8859-1";
        Object obj = (map == null || !map.containsKey(EncodeHintType.CHARACTER_SET)) ? null : 1;
        if (obj != null) {
            str2 = map.get(EncodeHintType.CHARACTER_SET).toString();
        }
        Mode a2 = Encoder.m4764a(str, str2);
        BitArray bitArray = new BitArray();
        if (a2 == Mode.BYTE && !(obj == null && "ISO-8859-1".equals(str2))) {
            CharacterSetECI a3 = CharacterSetECI.m4847a(str2);
            if (a3 != null) {
                Encoder.m4773a(a3, bitArray);
            }
        }
        Encoder.m4772a(a2, bitArray);
        BitArray bitArray2 = new BitArray();
        Encoder.m4775a(str, a2, bitArray2, str2);
        if (map == null || !map.containsKey(EncodeHintType.QR_VERSION)) {
            a = Encoder.m4766a(errorCorrectionLevel, a2, bitArray, bitArray2);
        } else {
            a = Version.m4690b(Integer.parseInt(map.get(EncodeHintType.QR_VERSION).toString()));
            if (!Encoder.m4778a(Encoder.m4761a(a2, bitArray, bitArray2, a), a, errorCorrectionLevel)) {
                throw new WriterException("Data too big for requested version");
            }
        }
        BitArray bitArray3 = new BitArray();
        bitArray3.m4827a(bitArray);
        Encoder.m4770a(a2 == Mode.BYTE ? bitArray2.m4830b() : str.length(), a, a2, bitArray3);
        bitArray3.m4827a(bitArray2);
        Version.Version a4 = a.m4694a(errorCorrectionLevel);
        int c = a.m4696c() - a4.m4687c();
        Encoder.m4771a(c, bitArray3);
        BitArray a5 = Encoder.m4768a(bitArray3, a.m4696c(), c, a4.m4686b());
        QRCode qRCode = new QRCode();
        qRCode.m4812a(errorCorrectionLevel);
        qRCode.m4813a(a2);
        qRCode.m4814a(a);
        int d = a.m4697d();
        ByteMatrix byteMatrix = new ByteMatrix(d, d);
        d = Encoder.m4763a(a5, errorCorrectionLevel, a, byteMatrix);
        qRCode.m4811a(d);
        MatrixUtil.m4799a(a5, errorCorrectionLevel, a, d, byteMatrix);
        qRCode.m4815a(byteMatrix);
        return qRCode;
    }

    private static Version m4766a(ErrorCorrectionLevel errorCorrectionLevel, Mode mode, BitArray bitArray, BitArray bitArray2) {
        return Encoder.m4765a(Encoder.m4761a(mode, bitArray, bitArray2, Encoder.m4765a(Encoder.m4761a(mode, bitArray, bitArray2, Version.m4690b(1)), errorCorrectionLevel)), errorCorrectionLevel);
    }

    private static int m4761a(Mode mode, BitArray bitArray, BitArray bitArray2, Version version) {
        return (bitArray.m4824a() + mode.m4681a(version)) + bitArray2.m4824a();
    }

    static int m4760a(int i) {
        if (i < f2845a.length) {
            return f2845a[i];
        }
        return -1;
    }

    private static Mode m4764a(String str, String str2) {
        int i = 0;
        if ("Shift_JIS".equals(str2) && Encoder.m4779a(str)) {
            return Mode.KANJI;
        }
        int i2 = 0;
        int i3 = 0;
        while (i < str.length()) {
            int charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                i3 = 1;
            } else if (Encoder.m4760a(charAt) == -1) {
                return Mode.BYTE;
            } else {
                i2 = 1;
            }
            i++;
        }
        if (i2 != 0) {
            return Mode.ALPHANUMERIC;
        }
        if (i3 != 0) {
            return Mode.NUMERIC;
        }
        return Mode.BYTE;
    }

    private static boolean m4779a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                int i2 = bytes[i] & 255;
                if ((i2 < 129 || i2 > 159) && (i2 < 224 || i2 > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    private static int m4763a(BitArray bitArray, ErrorCorrectionLevel errorCorrectionLevel, Version version, ByteMatrix byteMatrix) {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        int i3 = 0;
        while (i3 < 8) {
            MatrixUtil.m4799a(bitArray, errorCorrectionLevel, version, i3, byteMatrix);
            int a = Encoder.m4762a(byteMatrix);
            if (a < i) {
                i2 = i3;
            } else {
                a = i;
            }
            i3++;
            i = a;
        }
        return i2;
    }

    private static Version m4765a(int i, ErrorCorrectionLevel errorCorrectionLevel) {
        for (int i2 = 1; i2 <= 40; i2++) {
            Version b = Version.m4690b(i2);
            if (Encoder.m4778a(i, b, errorCorrectionLevel)) {
                return b;
            }
        }
        throw new WriterException("Data too big");
    }

    private static boolean m4778a(int i, Version version, ErrorCorrectionLevel errorCorrectionLevel) {
        return version.m4696c() - version.m4694a(errorCorrectionLevel).m4687c() >= (i + 7) / 8;
    }

    static void m4771a(int i, BitArray bitArray) {
        int i2 = i * 8;
        if (bitArray.m4824a() > i2) {
            throw new WriterException("data bits cannot fit in the QR Code" + bitArray.m4824a() + " > " + i2);
        }
        int i3;
        for (i3 = 0; i3 < 4 && bitArray.m4824a() < i2; i3++) {
            bitArray.m4828a(false);
        }
        i3 = bitArray.m4824a() & 7;
        if (i3 > 0) {
            while (i3 < 8) {
                bitArray.m4828a(false);
                i3++;
            }
        }
        int b = i - bitArray.m4830b();
        for (i3 = 0; i3 < b; i3++) {
            bitArray.m4825a((i3 & 1) == 0 ? 236 : 17, 8);
        }
        if (bitArray.m4824a() != i2) {
            throw new WriterException("Bits size does not equal capacity");
        }
    }

    static void m4769a(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) {
        if (i4 >= i3) {
            throw new WriterException("Block ID too large");
        }
        int i5 = i % i3;
        int i6 = i3 - i5;
        int i7 = i / i3;
        int i8 = i7 + 1;
        int i9 = i2 / i3;
        int i10 = i9 + 1;
        i7 -= i9;
        i8 -= i10;
        if (i7 != i8) {
            throw new WriterException("EC bytes mismatch");
        } else if (i3 != i6 + i5) {
            throw new WriterException("RS blocks mismatch");
        } else {
            if (i != (i5 * (i10 + i8)) + ((i9 + i7) * i6)) {
                throw new WriterException("Total bytes mismatch");
            } else if (i4 < i6) {
                iArr[0] = i9;
                iArr2[0] = i7;
            } else {
                iArr[0] = i10;
                iArr2[0] = i8;
            }
        }
    }

    static BitArray m4768a(BitArray bitArray, int i, int i2, int i3) {
        if (bitArray.m4830b() != i2) {
            throw new WriterException("Number of bits and data bytes does not match");
        }
        Collection<BlockPair> arrayList = new ArrayList(i3);
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i4 < i3) {
            int[] iArr = new int[1];
            int[] iArr2 = new int[1];
            Encoder.m4769a(i, i2, i3, i4, iArr, iArr2);
            int i8 = iArr[0];
            byte[] bArr = new byte[i8];
            bitArray.m4826a(i7 * 8, bArr, 0, i8);
            byte[] a = Encoder.m4780a(bArr, iArr2[0]);
            arrayList.add(new BlockPair(bArr, a));
            int max = Math.max(i6, i8);
            i4++;
            i5 = Math.max(i5, a.length);
            i6 = max;
            i7 = iArr[0] + i7;
        }
        if (i2 != i7) {
            throw new WriterException("Data bytes does not match offset");
        }
        BitArray bitArray2 = new BitArray();
        for (max = 0; max < i6; max++) {
            for (BlockPair a2 : arrayList) {
                byte[] a3 = a2.m4751a();
                if (max < a3.length) {
                    bitArray2.m4825a(a3[max], 8);
                }
            }
        }
        for (max = 0; max < i5; max++) {
            for (BlockPair a22 : arrayList) {
                a3 = a22.m4752b();
                if (max < a3.length) {
                    bitArray2.m4825a(a3[max], 8);
                }
            }
        }
        if (i == bitArray2.m4830b()) {
            return bitArray2;
        }
        throw new WriterException("Interleaving error: " + i + " and " + bitArray2.m4830b() + " differ.");
    }

    static byte[] m4780a(byte[] bArr, int i) {
        int i2 = 0;
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i3 = 0; i3 < length; i3++) {
            iArr[i3] = bArr[i3] & 255;
        }
        new ReedSolomonEncoder(GenericGF.f2929e).m4904a(iArr, i);
        byte[] bArr2 = new byte[i];
        while (i2 < i) {
            bArr2[i2] = (byte) iArr[length + i2];
            i2++;
        }
        return bArr2;
    }

    static void m4772a(Mode mode, BitArray bitArray) {
        bitArray.m4825a(mode.m4680a(), 4);
    }

    static void m4770a(int i, Version version, Mode mode, BitArray bitArray) {
        int a = mode.m4681a(version);
        if (i >= (1 << a)) {
            throw new WriterException(i + " is bigger than " + ((1 << a) - 1));
        }
        bitArray.m4825a(i, a);
    }

    static void m4775a(String str, Mode mode, BitArray bitArray, String str2) {
        switch (Encoder.f2844a[mode.ordinal()]) {
            case R.View_android_focusable /*1*/:
                Encoder.m4774a((CharSequence) str, bitArray);
            case R.View_paddingStart /*2*/:
                Encoder.m4781b(str, bitArray);
            case R.View_paddingEnd /*3*/:
                Encoder.m4777a(str, bitArray, str2);
            case R.View_theme /*4*/:
                Encoder.m4776a(str, bitArray);
            default:
                throw new WriterException("Invalid mode: " + mode);
        }
    }

    static void m4774a(CharSequence charSequence, BitArray bitArray) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - 48;
            if (i + 2 < length) {
                bitArray.m4825a(((charAt * 100) + ((charSequence.charAt(i + 1) - 48) * 10)) + (charSequence.charAt(i + 2) - 48), 10);
                i += 3;
            } else if (i + 1 < length) {
                bitArray.m4825a((charAt * 10) + (charSequence.charAt(i + 1) - 48), 7);
                i += 2;
            } else {
                bitArray.m4825a(charAt, 4);
                i++;
            }
        }
    }

    static void m4781b(CharSequence charSequence, BitArray bitArray) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int a = Encoder.m4760a(charSequence.charAt(i));
            if (a == -1) {
                throw new WriterException();
            } else if (i + 1 < length) {
                int a2 = Encoder.m4760a(charSequence.charAt(i + 1));
                if (a2 == -1) {
                    throw new WriterException();
                }
                bitArray.m4825a((a * 45) + a2, 11);
                i += 2;
            } else {
                bitArray.m4825a(a, 6);
                i++;
            }
        }
    }

    static void m4777a(String str, BitArray bitArray, String str2) {
        try {
            for (byte a : str.getBytes(str2)) {
                bitArray.m4825a(a, 8);
            }
        } catch (Throwable e) {
            throw new WriterException(e);
        }
    }

    static void m4776a(String str, BitArray bitArray) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            for (int i = 0; i < length; i += 2) {
                int i2 = ((bytes[i] & 255) << 8) | (bytes[i + 1] & 255);
                if (i2 >= 33088 && i2 <= 40956) {
                    i2 -= 33088;
                } else if (i2 < 57408 || i2 > 60351) {
                    i2 = -1;
                } else {
                    i2 -= 49472;
                }
                if (i2 == -1) {
                    throw new WriterException("Invalid byte sequence");
                }
                bitArray.m4825a((i2 & 255) + ((i2 >> 8) * 192), 13);
            }
        } catch (Throwable e) {
            throw new WriterException(e);
        }
    }

    private static void m4773a(CharacterSetECI characterSetECI, BitArray bitArray) {
        bitArray.m4825a(Mode.ECI.m4680a(), 4);
        bitArray.m4825a(characterSetECI.m4848a(), 8);
    }
}
