package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

import in.juspay.widget.qrscanner.com.google.zxing.DecodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPointCallback;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.common.DetectorResult;
import in.juspay.widget.qrscanner.com.google.zxing.common.GridSampler;
import in.juspay.widget.qrscanner.com.google.zxing.common.PerspectiveTransform;
import in.juspay.widget.qrscanner.com.google.zxing.common.p034a.MathUtils;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version;
import java.util.Map;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.c */
public class Detector {
    private final BitMatrix f2825a;
    private ResultPointCallback f2826b;

    public Detector(BitMatrix bitMatrix) {
        this.f2825a = bitMatrix;
    }

    public final DetectorResult m4728a(Map<DecodeHintType, ?> map) {
        ResultPointCallback resultPointCallback;
        if (map == null) {
            resultPointCallback = null;
        } else {
            resultPointCallback = (ResultPointCallback) map.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
        }
        this.f2826b = resultPointCallback;
        return m4727a(new FinderPatternFinder(this.f2825a, this.f2826b).m4744a((Map) map));
    }

    protected final DetectorResult m4727a(FinderPatternInfo finderPatternInfo) {
        ResultPoint b = finderPatternInfo.m4747b();
        ResultPoint c = finderPatternInfo.m4748c();
        ResultPoint a = finderPatternInfo.m4746a();
        float a2 = m4725a(b, c, a);
        if (a2 < 1.0f) {
            throw NotFoundException.m4608a();
        }
        ResultPoint[] resultPointArr;
        int a3 = Detector.m4721a(b, c, a, a2);
        Version a4 = Version.m4689a(a3);
        int d = a4.m4697d() - 7;
        ResultPoint resultPoint = null;
        if (a4.m4695b().length > 0) {
            float a5 = (c.m4710a() - b.m4710a()) + a.m4710a();
            float b2 = (c.m4711b() - b.m4711b()) + a.m4711b();
            float f = 1.0f - (3.0f / ((float) d));
            int a6 = (int) (((a5 - b.m4710a()) * f) + b.m4710a());
            d = (int) (b.m4711b() + (f * (b2 - b.m4711b())));
            int i = 4;
            while (i <= 16) {
                try {
                    resultPoint = m4726a(a2, a6, d, (float) i);
                    break;
                } catch (NotFoundException e) {
                    i <<= 1;
                }
            }
        }
        BitMatrix a7 = Detector.m4722a(this.f2825a, Detector.m4723a(b, c, a, resultPoint, a3), a3);
        if (resultPoint == null) {
            resultPointArr = new ResultPoint[]{a, b, c};
        } else {
            resultPointArr = new ResultPoint[]{a, b, c, resultPoint};
        }
        return new DetectorResult(a7, resultPointArr);
    }

    private static PerspectiveTransform m4723a(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3, ResultPoint resultPoint4, int i) {
        float a;
        float b;
        float f;
        float f2;
        float f3 = ((float) i) - 3.5f;
        if (resultPoint4 != null) {
            a = resultPoint4.m4710a();
            b = resultPoint4.m4711b();
            f = f3 - 3.0f;
            f2 = f;
        } else {
            a = (resultPoint2.m4710a() - resultPoint.m4710a()) + resultPoint3.m4710a();
            b = (resultPoint2.m4711b() - resultPoint.m4711b()) + resultPoint3.m4711b();
            f = f3;
            f2 = f3;
        }
        return PerspectiveTransform.m4873a(3.5f, 3.5f, f3, 3.5f, f2, f, 3.5f, f3, resultPoint.m4710a(), resultPoint.m4711b(), resultPoint2.m4710a(), resultPoint2.m4711b(), a, b, resultPoint3.m4710a(), resultPoint3.m4711b());
    }

    private static BitMatrix m4722a(BitMatrix bitMatrix, PerspectiveTransform perspectiveTransform, int i) {
        return GridSampler.m4858a().m4860a(bitMatrix, i, i, perspectiveTransform);
    }

    private static int m4721a(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3, float f) {
        int a = ((MathUtils.m4821a(ResultPoint.m4707a(resultPoint, resultPoint2) / f) + MathUtils.m4821a(ResultPoint.m4707a(resultPoint, resultPoint3) / f)) / 2) + 7;
        switch (a & 3) {
            case R.View_android_theme /*0*/:
                return a + 1;
            case R.View_paddingStart /*2*/:
                return a - 1;
            case R.View_paddingEnd /*3*/:
                throw NotFoundException.m4608a();
            default:
                return a;
        }
    }

    protected final float m4725a(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3) {
        return (m4720a(resultPoint, resultPoint2) + m4720a(resultPoint, resultPoint3)) / 2.0f;
    }

    private float m4720a(ResultPoint resultPoint, ResultPoint resultPoint2) {
        float a = m4719a((int) resultPoint.m4710a(), (int) resultPoint.m4711b(), (int) resultPoint2.m4710a(), (int) resultPoint2.m4711b());
        float a2 = m4719a((int) resultPoint2.m4710a(), (int) resultPoint2.m4711b(), (int) resultPoint.m4710a(), (int) resultPoint.m4711b());
        if (Float.isNaN(a)) {
            return a2 / 7.0f;
        }
        if (Float.isNaN(a2)) {
            return a / 7.0f;
        }
        return (a + a2) / 14.0f;
    }

    private float m4719a(int i, int i2, int i3, int i4) {
        float f;
        int i5;
        int i6 = 0;
        float b = m4724b(i, i2, i3, i4);
        int i7 = i - (i3 - i);
        if (i7 < 0) {
            f = ((float) i) / ((float) (i - i7));
            i5 = 0;
        } else if (i7 >= this.f2825a.m4840c()) {
            f = ((float) ((this.f2825a.m4840c() - 1) - i)) / ((float) (i7 - i));
            i5 = this.f2825a.m4840c() - 1;
        } else {
            i5 = i7;
            f = 1.0f;
        }
        i7 = (int) (((float) i2) - (f * ((float) (i4 - i2))));
        if (i7 < 0) {
            f = ((float) i2) / ((float) (i2 - i7));
        } else if (i7 >= this.f2825a.m4842d()) {
            f = ((float) ((this.f2825a.m4842d() - 1) - i2)) / ((float) (i7 - i2));
            i6 = this.f2825a.m4842d() - 1;
        } else {
            i6 = i7;
            f = 1.0f;
        }
        return (m4724b(i, i2, (int) ((f * ((float) (i5 - i))) + ((float) i)), i6) + b) - 1.0f;
    }

    private float m4724b(int i, int i2, int i3, int i4) {
        Object obj;
        if (Math.abs(i4 - i2) > Math.abs(i3 - i)) {
            obj = 1;
        } else {
            obj = null;
        }
        if (obj == null) {
            int i5 = i4;
            i4 = i3;
            i3 = i5;
            int i6 = i2;
            i2 = i;
            i = i6;
        }
        int abs = Math.abs(i4 - i2);
        int abs2 = Math.abs(i3 - i);
        int i7 = (-abs) / 2;
        int i8 = i2 < i4 ? 1 : -1;
        int i9 = i < i3 ? 1 : -1;
        int i10 = 0;
        int i11 = i4 + i8;
        int i12 = i2;
        int i13 = i7;
        i7 = i;
        while (i12 != i11) {
            int i14;
            int i15;
            int i16;
            if (obj != null) {
                i14 = i7;
            } else {
                i14 = i12;
            }
            if (obj != null) {
                i15 = i12;
            } else {
                i15 = i7;
            }
            if ((i10 == 1) != this.f2825a.m4836a(i14, i15)) {
                i15 = i10;
            } else if (i10 == 2) {
                return MathUtils.m4820a(i12, i7, i2, i);
            } else {
                i15 = i10 + 1;
            }
            i10 = i13 + abs2;
            if (i10 <= 0) {
                i16 = i7;
                i7 = i10;
            } else if (i7 == i3) {
                i9 = i15;
                break;
            } else {
                i16 = i7 + i9;
                i7 = i10 - abs;
            }
            i12 += i8;
            i10 = i15;
            i13 = i7;
            i7 = i16;
        }
        i9 = i10;
        if (i9 == 2) {
            return MathUtils.m4820a(i4 + i8, i3, i2, i);
        }
        return Float.NaN;
    }

    protected final AlignmentPattern m4726a(float f, int i, int i2, float f2) {
        int i3 = (int) (f2 * f);
        int max = Math.max(0, i - i3);
        int min = Math.min(this.f2825a.m4840c() - 1, i + i3);
        if (((float) (min - max)) < f * 3.0f) {
            throw NotFoundException.m4608a();
        }
        int max2 = Math.max(0, i2 - i3);
        int min2 = Math.min(this.f2825a.m4842d() - 1, i3 + i2);
        if (((float) (min2 - max2)) < f * 3.0f) {
            throw NotFoundException.m4608a();
        }
        return new AlignmentPatternFinder(this.f2825a, max, max2, min - max, min2 - max2, f, this.f2826b).m4718a();
    }
}
