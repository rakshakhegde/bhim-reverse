package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

import in.juspay.widget.qrscanner.com.google.zxing.WriterException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitArray;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.Version;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.e */
final class MatrixUtil {
    private static final int[][] f2846a;
    private static final int[][] f2847b;
    private static final int[][] f2848c;
    private static final int[][] f2849d;

    static {
        f2846a = new int[][]{new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};
        f2847b = new int[][]{new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};
        f2848c = new int[][]{new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle, -1, -1}, new int[]{6, 28, 54, 80, R.AppCompatTheme_ratingBarStyle, -1, -1}, new int[]{6, 32, 58, 84, R.AppCompatTheme_spinnerStyle, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle, 126, -1}, new int[]{6, 26, 52, 78, R.AppCompatTheme_editTextStyle, 130, -1}, new int[]{6, 30, 56, 82, R.AppCompatTheme_ratingBarStyleSmall, 134, -1}, new int[]{6, 34, 60, 86, 112, 138, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle, 126, 150}, new int[]{6, 24, 50, 76, R.AppCompatTheme_checkboxStyle, 128, 154}, new int[]{6, 28, 54, 80, R.AppCompatTheme_ratingBarStyle, 132, 158}, new int[]{6, 32, 58, 84, R.AppCompatTheme_spinnerStyle, 136, 162}, new int[]{6, 26, 54, 82, R.AppCompatTheme_spinnerStyle, 138, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};
        f2849d = new int[][]{new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};
    }

    static void m4797a(ByteMatrix byteMatrix) {
        byteMatrix.m4755a((byte) -1);
    }

    static void m4799a(BitArray bitArray, ErrorCorrectionLevel errorCorrectionLevel, Version version, int i, ByteMatrix byteMatrix) {
        MatrixUtil.m4797a(byteMatrix);
        MatrixUtil.m4795a(version, byteMatrix);
        MatrixUtil.m4793a(errorCorrectionLevel, i, byteMatrix);
        MatrixUtil.m4801b(version, byteMatrix);
        MatrixUtil.m4798a(bitArray, i, byteMatrix);
    }

    static void m4795a(Version version, ByteMatrix byteMatrix) {
        MatrixUtil.m4808d(byteMatrix);
        MatrixUtil.m4806c(byteMatrix);
        MatrixUtil.m4805c(version, byteMatrix);
        MatrixUtil.m4802b(byteMatrix);
    }

    static void m4793a(ErrorCorrectionLevel errorCorrectionLevel, int i, ByteMatrix byteMatrix) {
        BitArray bitArray = new BitArray();
        MatrixUtil.m4794a(errorCorrectionLevel, i, bitArray);
        for (int i2 = 0; i2 < bitArray.m4824a(); i2++) {
            boolean a = bitArray.m4829a((bitArray.m4824a() - 1) - i2);
            byteMatrix.m4757a(f2849d[i2][0], f2849d[i2][1], a);
            if (i2 < 8) {
                byteMatrix.m4757a((byteMatrix.m4758b() - i2) - 1, 8, a);
            } else {
                byteMatrix.m4757a(8, (byteMatrix.m4754a() - 7) + (i2 - 8), a);
            }
        }
    }

    static void m4801b(Version version, ByteMatrix byteMatrix) {
        if (version.m4693a() >= 7) {
            BitArray bitArray = new BitArray();
            MatrixUtil.m4796a(version, bitArray);
            int i = 17;
            int i2 = 0;
            while (i2 < 6) {
                int i3 = i;
                for (i = 0; i < 3; i++) {
                    boolean a = bitArray.m4829a(i3);
                    i3--;
                    byteMatrix.m4757a(i2, (byteMatrix.m4754a() - 11) + i, a);
                    byteMatrix.m4757a((byteMatrix.m4754a() - 11) + i, i2, a);
                }
                i2++;
                i = i3;
            }
        }
    }

    static void m4798a(BitArray bitArray, int i, ByteMatrix byteMatrix) {
        int b = byteMatrix.m4758b() - 1;
        int a = byteMatrix.m4754a() - 1;
        int i2 = -1;
        int i3 = 0;
        while (b > 0) {
            int i4;
            int i5;
            if (b == 6) {
                i4 = a;
                i5 = b - 1;
                a = i3;
            } else {
                i4 = a;
                i5 = b;
                a = i3;
            }
            while (i4 >= 0 && i4 < byteMatrix.m4754a()) {
                for (i3 = 0; i3 < 2; i3++) {
                    int i6 = i5 - i3;
                    if (MatrixUtil.m4803b(byteMatrix.m4753a(i6, i4))) {
                        boolean z;
                        if (a < bitArray.m4824a()) {
                            boolean a2 = bitArray.m4829a(a);
                            b = a + 1;
                            z = a2;
                        } else {
                            b = a;
                            z = false;
                        }
                        if (i != -1 && MaskUtil.m4784a(i, i6, i4)) {
                            if (z) {
                                z = false;
                            } else {
                                z = true;
                            }
                        }
                        byteMatrix.m4757a(i6, i4, z);
                        a = b;
                    }
                }
                i4 += i2;
            }
            i2 = -i2;
            b = i5 - 2;
            i3 = a;
            a = i4 + i2;
        }
        if (i3 != bitArray.m4824a()) {
            throw new WriterException("Not all bits consumed: " + i3 + '/' + bitArray.m4824a());
        }
    }

    static int m4790a(int i) {
        return 32 - Integer.numberOfLeadingZeros(i);
    }

    static int m4791a(int i, int i2) {
        if (i2 == 0) {
            throw new IllegalArgumentException("0 polynomial");
        }
        int a = MatrixUtil.m4790a(i2);
        int i3 = i << (a - 1);
        while (MatrixUtil.m4790a(i3) >= a) {
            i3 ^= i2 << (MatrixUtil.m4790a(i3) - a);
        }
        return i3;
    }

    static void m4794a(ErrorCorrectionLevel errorCorrectionLevel, int i, BitArray bitArray) {
        if (QRCode.m4809b(i)) {
            int a = (errorCorrectionLevel.m4673a() << 3) | i;
            bitArray.m4825a(a, 5);
            bitArray.m4825a(MatrixUtil.m4791a(a, 1335), 10);
            BitArray bitArray2 = new BitArray();
            bitArray2.m4825a(21522, 15);
            bitArray.m4831b(bitArray2);
            if (bitArray.m4824a() != 15) {
                throw new WriterException("should not happen but we got: " + bitArray.m4824a());
            }
            return;
        }
        throw new WriterException("Invalid mask pattern");
    }

    static void m4796a(Version version, BitArray bitArray) {
        bitArray.m4825a(version.m4693a(), 6);
        bitArray.m4825a(MatrixUtil.m4791a(version.m4693a(), 7973), 12);
        if (bitArray.m4824a() != 18) {
            throw new WriterException("should not happen but we got: " + bitArray.m4824a());
        }
    }

    private static boolean m4803b(int i) {
        return i == -1;
    }

    private static void m4802b(ByteMatrix byteMatrix) {
        for (int i = 8; i < byteMatrix.m4758b() - 8; i++) {
            int i2 = (i + 1) % 2;
            if (MatrixUtil.m4803b(byteMatrix.m4753a(i, 6))) {
                byteMatrix.m4756a(i, 6, i2);
            }
            if (MatrixUtil.m4803b(byteMatrix.m4753a(6, i))) {
                byteMatrix.m4756a(6, i, i2);
            }
        }
    }

    private static void m4806c(ByteMatrix byteMatrix) {
        if (byteMatrix.m4753a(8, byteMatrix.m4754a() - 8) == null) {
            throw new WriterException();
        }
        byteMatrix.m4756a(8, byteMatrix.m4754a() - 8, 1);
    }

    private static void m4792a(int i, int i2, ByteMatrix byteMatrix) {
        int i3 = 0;
        while (i3 < 8) {
            if (MatrixUtil.m4803b(byteMatrix.m4753a(i + i3, i2))) {
                byteMatrix.m4756a(i + i3, i2, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }

    private static void m4800b(int i, int i2, ByteMatrix byteMatrix) {
        int i3 = 0;
        while (i3 < 7) {
            if (MatrixUtil.m4803b(byteMatrix.m4753a(i, i2 + i3))) {
                byteMatrix.m4756a(i, i2 + i3, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }

    private static void m4804c(int i, int i2, ByteMatrix byteMatrix) {
        for (int i3 = 0; i3 < 5; i3++) {
            for (int i4 = 0; i4 < 5; i4++) {
                byteMatrix.m4756a(i + i4, i2 + i3, f2847b[i3][i4]);
            }
        }
    }

    private static void m4807d(int i, int i2, ByteMatrix byteMatrix) {
        for (int i3 = 0; i3 < 7; i3++) {
            for (int i4 = 0; i4 < 7; i4++) {
                byteMatrix.m4756a(i + i4, i2 + i3, f2846a[i3][i4]);
            }
        }
    }

    private static void m4808d(ByteMatrix byteMatrix) {
        int length = f2846a[0].length;
        MatrixUtil.m4807d(0, 0, byteMatrix);
        MatrixUtil.m4807d(byteMatrix.m4758b() - length, 0, byteMatrix);
        MatrixUtil.m4807d(0, byteMatrix.m4758b() - length, byteMatrix);
        MatrixUtil.m4792a(0, 7, byteMatrix);
        MatrixUtil.m4792a(byteMatrix.m4758b() - 8, 7, byteMatrix);
        MatrixUtil.m4792a(0, byteMatrix.m4758b() - 8, byteMatrix);
        MatrixUtil.m4800b(7, 0, byteMatrix);
        MatrixUtil.m4800b((byteMatrix.m4754a() - 7) - 1, 0, byteMatrix);
        MatrixUtil.m4800b(7, byteMatrix.m4754a() - 7, byteMatrix);
    }

    private static void m4805c(Version version, ByteMatrix byteMatrix) {
        if (version.m4693a() >= 2) {
            int a = version.m4693a() - 1;
            int[] iArr = f2848c[a];
            int length = f2848c[a].length;
            for (int i = 0; i < length; i++) {
                for (a = 0; a < length; a++) {
                    int i2 = iArr[i];
                    int i3 = iArr[a];
                    if (!(i3 == -1 || i2 == -1 || !MatrixUtil.m4803b(byteMatrix.m4753a(i3, i2)))) {
                        MatrixUtil.m4804c(i3 - 2, i2 - 2, byteMatrix);
                    }
                }
            }
        }
    }
}
