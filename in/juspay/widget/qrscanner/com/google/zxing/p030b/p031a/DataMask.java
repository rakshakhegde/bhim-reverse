package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.c */
enum DataMask {
    DATA_MASK_000 {
        boolean m4653a(int i, int i2) {
            return ((i + i2) & 1) == 0;
        }
    },
    DATA_MASK_001 {
        boolean m4654a(int i, int i2) {
            return (i & 1) == 0;
        }
    },
    DATA_MASK_010 {
        boolean m4655a(int i, int i2) {
            return i2 % 3 == 0;
        }
    },
    DATA_MASK_011 {
        boolean m4656a(int i, int i2) {
            return (i + i2) % 3 == 0;
        }
    },
    DATA_MASK_100 {
        boolean m4657a(int i, int i2) {
            return (((i / 2) + (i2 / 3)) & 1) == 0;
        }
    },
    DATA_MASK_101 {
        boolean m4658a(int i, int i2) {
            return (i * i2) % 6 == 0;
        }
    },
    DATA_MASK_110 {
        boolean m4659a(int i, int i2) {
            return (i * i2) % 6 < 3;
        }
    },
    DATA_MASK_111 {
        boolean m4660a(int i, int i2) {
            return (((i + i2) + ((i * i2) % 3)) & 1) == 0;
        }
    };

    abstract boolean m4652a(int i, int i2);

    final void m4651a(BitMatrix bitMatrix, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            for (int i3 = 0; i3 < i; i3++) {
                if (m4652a(i2, i3)) {
                    bitMatrix.m4841c(i3, i2);
                }
            }
        }
    }
}
