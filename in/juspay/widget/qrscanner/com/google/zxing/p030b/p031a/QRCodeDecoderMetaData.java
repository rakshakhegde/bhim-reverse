package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.i */
public final class QRCodeDecoderMetaData {
    private final boolean f2800a;

    QRCodeDecoderMetaData(boolean z) {
        this.f2800a = z;
    }

    public void m4682a(ResultPoint[] resultPointArr) {
        if (this.f2800a && resultPointArr != null && resultPointArr.length >= 3) {
            ResultPoint resultPoint = resultPointArr[0];
            resultPointArr[0] = resultPointArr[2];
            resultPointArr[2] = resultPoint;
        }
    }
}
