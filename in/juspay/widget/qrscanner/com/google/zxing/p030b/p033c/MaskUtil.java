package in.juspay.widget.qrscanner.com.google.zxing.p030b.p033c;

import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.c.d */
final class MaskUtil {
    static int m4782a(ByteMatrix byteMatrix) {
        return MaskUtil.m4783a(byteMatrix, true) + MaskUtil.m4783a(byteMatrix, false);
    }

    static int m4787b(ByteMatrix byteMatrix) {
        byte[][] c = byteMatrix.m4759c();
        int b = byteMatrix.m4758b();
        int a = byteMatrix.m4754a();
        int i = 0;
        int i2 = 0;
        while (i < a - 1) {
            int i3 = 0;
            while (i3 < b - 1) {
                byte b2 = c[i][i3];
                if (b2 == c[i][i3 + 1] && b2 == c[i + 1][i3] && b2 == c[i + 1][i3 + 1]) {
                    i2++;
                }
                i3++;
            }
            i++;
        }
        return i2 * 3;
    }

    static int m4788c(ByteMatrix byteMatrix) {
        byte[][] c = byteMatrix.m4759c();
        int b = byteMatrix.m4758b();
        int a = byteMatrix.m4754a();
        int i = 0;
        int i2 = 0;
        while (i < a) {
            int i3 = 0;
            while (i3 < b) {
                byte[] bArr = c[i];
                if (i3 + 6 < b && bArr[i3] == (byte) 1 && bArr[i3 + 1] == null && bArr[i3 + 2] == (byte) 1 && bArr[i3 + 3] == (byte) 1 && bArr[i3 + 4] == (byte) 1 && bArr[i3 + 5] == null && bArr[i3 + 6] == (byte) 1 && (MaskUtil.m4785a(bArr, i3 - 4, i3) || MaskUtil.m4785a(bArr, i3 + 7, i3 + 11))) {
                    i2++;
                }
                if (i + 6 < a && c[i][i3] == (byte) 1 && c[i + 1][i3] == null && c[i + 2][i3] == (byte) 1 && c[i + 3][i3] == (byte) 1 && c[i + 4][i3] == (byte) 1 && c[i + 5][i3] == null && c[i + 6][i3] == (byte) 1 && (MaskUtil.m4786a(c, i3, i - 4, i) || MaskUtil.m4786a(c, i3, i + 7, i + 11))) {
                    i2++;
                }
                i3++;
            }
            i++;
        }
        return i2 * 40;
    }

    private static boolean m4785a(byte[] bArr, int i, int i2) {
        int min = Math.min(i2, bArr.length);
        for (int max = Math.max(i, 0); max < min; max++) {
            if (bArr[max] == (byte) 1) {
                return false;
            }
        }
        return true;
    }

    private static boolean m4786a(byte[][] bArr, int i, int i2, int i3) {
        int min = Math.min(i3, bArr.length);
        for (int max = Math.max(i2, 0); max < min; max++) {
            if (bArr[max][i] == (byte) 1) {
                return false;
            }
        }
        return true;
    }

    static int m4789d(ByteMatrix byteMatrix) {
        int i;
        byte[][] c = byteMatrix.m4759c();
        int b = byteMatrix.m4758b();
        int a = byteMatrix.m4754a();
        int i2 = 0;
        for (int i3 = 0; i3 < a; i3++) {
            byte[] bArr = c[i3];
            for (i = 0; i < b; i++) {
                if (bArr[i] == 1) {
                    i2++;
                }
            }
        }
        i = byteMatrix.m4754a() * byteMatrix.m4758b();
        return ((Math.abs((i2 * 2) - i) * 10) / i) * 10;
    }

    static boolean m4784a(int i, int i2, int i3) {
        int i4;
        switch (i) {
            case R.View_android_theme /*0*/:
                i4 = (i3 + i2) & 1;
                break;
            case R.View_android_focusable /*1*/:
                i4 = i3 & 1;
                break;
            case R.View_paddingStart /*2*/:
                i4 = i2 % 3;
                break;
            case R.View_paddingEnd /*3*/:
                i4 = (i3 + i2) % 3;
                break;
            case R.View_theme /*4*/:
                i4 = ((i3 / 2) + (i2 / 3)) & 1;
                break;
            case R.Toolbar_contentInsetStart /*5*/:
                i4 = i3 * i2;
                i4 = (i4 % 3) + (i4 & 1);
                break;
            case R.Toolbar_contentInsetEnd /*6*/:
                i4 = i3 * i2;
                i4 = ((i4 % 3) + (i4 & 1)) & 1;
                break;
            case R.Toolbar_contentInsetLeft /*7*/:
                i4 = (((i3 * i2) % 3) + ((i3 + i2) & 1)) & 1;
                break;
            default:
                throw new IllegalArgumentException("Invalid mask pattern: " + i);
        }
        if (i4 == 0) {
            return true;
        }
        return false;
    }

    private static int m4783a(ByteMatrix byteMatrix, boolean z) {
        int a = z ? byteMatrix.m4754a() : byteMatrix.m4758b();
        int b = z ? byteMatrix.m4758b() : byteMatrix.m4754a();
        byte[][] c = byteMatrix.m4759c();
        int i = 0;
        int i2 = 0;
        while (i < a) {
            Object obj = -1;
            int i3 = 0;
            int i4 = 0;
            while (i3 < b) {
                int i5;
                int i6;
                byte b2;
                byte b3 = z ? c[i][i3] : c[i3][i];
                if (b3 == obj) {
                    Object obj2 = obj;
                    i5 = i4 + 1;
                    i6 = i2;
                    b2 = obj2;
                } else {
                    if (i4 >= 5) {
                        i2 += (i4 - 5) + 3;
                    }
                    i5 = 1;
                    byte b4 = b3;
                    i6 = i2;
                    b2 = b4;
                }
                i3++;
                i4 = i5;
                obj = b2;
                i2 = i6;
            }
            if (i4 >= 5) {
                i2 += (i4 - 5) + 3;
            }
            i++;
        }
        return i2;
    }
}
