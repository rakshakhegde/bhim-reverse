package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPointCallback;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import java.util.ArrayList;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.b */
final class AlignmentPatternFinder {
    private final BitMatrix f2816a;
    private final List<AlignmentPattern> f2817b;
    private final int f2818c;
    private final int f2819d;
    private final int f2820e;
    private final int f2821f;
    private final float f2822g;
    private final int[] f2823h;
    private final ResultPointCallback f2824i;

    AlignmentPatternFinder(BitMatrix bitMatrix, int i, int i2, int i3, int i4, float f, ResultPointCallback resultPointCallback) {
        this.f2816a = bitMatrix;
        this.f2817b = new ArrayList(5);
        this.f2818c = i;
        this.f2819d = i2;
        this.f2820e = i3;
        this.f2821f = i4;
        this.f2822g = f;
        this.f2823h = new int[3];
        this.f2824i = resultPointCallback;
    }

    AlignmentPattern m4718a() {
        int i = this.f2818c;
        int i2 = this.f2821f;
        int i3 = i + this.f2820e;
        int i4 = this.f2819d + (i2 / 2);
        int[] iArr = new int[3];
        for (int i5 = 0; i5 < i2; i5++) {
            int i6;
            AlignmentPattern a;
            if ((i5 & 1) == 0) {
                i6 = (i5 + 1) / 2;
            } else {
                i6 = -((i5 + 1) / 2);
            }
            int i7 = i4 + i6;
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            i6 = i;
            while (i6 < i3 && !this.f2816a.m4836a(i6, i7)) {
                i6++;
            }
            i6 = 0;
            for (int i8 = i6; i8 < i3; i8++) {
                if (!this.f2816a.m4836a(i8, i7)) {
                    if (i6 == 1) {
                        i6++;
                    }
                    iArr[i6] = iArr[i6] + 1;
                } else if (i6 == 1) {
                    iArr[1] = iArr[1] + 1;
                } else if (i6 == 2) {
                    if (m4717a(iArr)) {
                        a = m4716a(iArr, i7, i8);
                        if (a != null) {
                            return a;
                        }
                    }
                    iArr[0] = iArr[2];
                    iArr[1] = 1;
                    iArr[2] = 0;
                    i6 = 1;
                } else {
                    i6++;
                    iArr[i6] = iArr[i6] + 1;
                }
            }
            if (m4717a(iArr)) {
                a = m4716a(iArr, i7, i3);
                if (a != null) {
                    return a;
                }
            }
        }
        if (!this.f2817b.isEmpty()) {
            return (AlignmentPattern) this.f2817b.get(0);
        }
        throw NotFoundException.m4608a();
    }

    private static float m4715a(int[] iArr, int i) {
        return ((float) (i - iArr[2])) - (((float) iArr[1]) / 2.0f);
    }

    private boolean m4717a(int[] iArr) {
        float f = this.f2822g;
        float f2 = f / 2.0f;
        for (int i = 0; i < 3; i++) {
            if (Math.abs(f - ((float) iArr[i])) >= f2) {
                return false;
            }
        }
        return true;
    }

    private float m4714a(int i, int i2, int i3, int i4) {
        BitMatrix bitMatrix = this.f2816a;
        int d = bitMatrix.m4842d();
        int[] iArr = this.f2823h;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        int i5 = i;
        while (i5 >= 0 && bitMatrix.m4836a(i2, i5) && iArr[1] <= i3) {
            iArr[1] = iArr[1] + 1;
            i5--;
        }
        if (i5 < 0 || iArr[1] > i3) {
            return Float.NaN;
        }
        while (i5 >= 0 && !bitMatrix.m4836a(i2, i5) && iArr[0] <= i3) {
            iArr[0] = iArr[0] + 1;
            i5--;
        }
        if (iArr[0] > i3) {
            return Float.NaN;
        }
        i5 = i + 1;
        while (i5 < d && bitMatrix.m4836a(i2, i5) && iArr[1] <= i3) {
            iArr[1] = iArr[1] + 1;
            i5++;
        }
        if (i5 == d || iArr[1] > i3) {
            return Float.NaN;
        }
        while (i5 < d && !bitMatrix.m4836a(i2, i5) && iArr[2] <= i3) {
            iArr[2] = iArr[2] + 1;
            i5++;
        }
        if (iArr[2] > i3 || Math.abs(((iArr[0] + iArr[1]) + iArr[2]) - i4) * 5 >= i4 * 2 || !m4717a(iArr)) {
            return Float.NaN;
        }
        return AlignmentPatternFinder.m4715a(iArr, i5);
    }

    private AlignmentPattern m4716a(int[] iArr, int i, int i2) {
        int i3 = (iArr[0] + iArr[1]) + iArr[2];
        float a = AlignmentPatternFinder.m4715a(iArr, i2);
        float a2 = m4714a(i, (int) a, iArr[1] * 2, i3);
        if (!Float.isNaN(a2)) {
            float f = ((float) ((iArr[0] + iArr[1]) + iArr[2])) / 3.0f;
            for (AlignmentPattern alignmentPattern : this.f2817b) {
                if (alignmentPattern.m4712a(f, a2, a)) {
                    return alignmentPattern.m4713b(a2, a, f);
                }
            }
            ResultPoint alignmentPattern2 = new AlignmentPattern(a, a2, f);
            this.f2817b.add(alignmentPattern2);
            if (this.f2824i != null) {
                this.f2824i.m4920a(alignmentPattern2);
            }
        }
        return null;
    }
}
