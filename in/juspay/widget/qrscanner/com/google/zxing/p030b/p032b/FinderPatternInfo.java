package in.juspay.widget.qrscanner.com.google.zxing.p030b.p032b;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.b.f */
public final class FinderPatternInfo {
    private final FinderPattern f2836a;
    private final FinderPattern f2837b;
    private final FinderPattern f2838c;

    public FinderPatternInfo(FinderPattern[] finderPatternArr) {
        this.f2836a = finderPatternArr[0];
        this.f2837b = finderPatternArr[1];
        this.f2838c = finderPatternArr[2];
    }

    public FinderPattern m4746a() {
        return this.f2836a;
    }

    public FinderPattern m4747b() {
        return this.f2837b;
    }

    public FinderPattern m4748c() {
        return this.f2838c;
    }
}
