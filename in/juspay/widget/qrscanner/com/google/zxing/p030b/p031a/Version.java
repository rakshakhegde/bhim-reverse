package in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a;

import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.j */
public final class Version {
    private static final int[] f2805a;
    private static final Version[] f2806b;
    private final int f2807c;
    private final int[] f2808d;
    private final Version[] f2809e;
    private final int f2810f;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.j.a */
    public static final class Version {
        private final int f2801a;
        private final int f2802b;

        Version(int i, int i2) {
            this.f2801a = i;
            this.f2802b = i2;
        }

        public int m4683a() {
            return this.f2801a;
        }

        public int m4684b() {
            return this.f2802b;
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.b.a.j.b */
    public static final class Version {
        private final int f2803a;
        private final Version[] f2804b;

        Version(int i, Version... versionArr) {
            this.f2803a = i;
            this.f2804b = versionArr;
        }

        public int m4685a() {
            return this.f2803a;
        }

        public int m4686b() {
            int i = 0;
            Version[] versionArr = this.f2804b;
            int i2 = 0;
            while (i < versionArr.length) {
                i2 += versionArr[i].m4683a();
                i++;
            }
            return i2;
        }

        public int m4687c() {
            return this.f2803a * m4686b();
        }

        public Version[] m4688d() {
            return this.f2804b;
        }
    }

    static {
        f2805a = new int[]{31892, 34236, 39577, 42195, 48118, 51042, 55367, 58893, 63784, 68472, 70749, 76311, 79154, 84390, 87683, 92361, 96236, 102084, 102881, 110507, 110734, 117786, 119615, 126325, 127568, 133589, 136944, 141498, 145311, 150283, 152622, 158308, 161089, 167017};
        f2806b = Version.m4692f();
    }

    private Version(int i, int[] iArr, Version... versionArr) {
        int i2 = 0;
        this.f2807c = i;
        this.f2808d = iArr;
        this.f2809e = versionArr;
        int a = versionArr[0].m4685a();
        Version[] d = versionArr[0].m4688d();
        int length = d.length;
        int i3 = 0;
        while (i2 < length) {
            Version version = d[i2];
            i3 += (version.m4684b() + a) * version.m4683a();
            i2++;
        }
        this.f2810f = i3;
    }

    public int m4693a() {
        return this.f2807c;
    }

    public int[] m4695b() {
        return this.f2808d;
    }

    public int m4696c() {
        return this.f2810f;
    }

    public int m4697d() {
        return (this.f2807c * 4) + 17;
    }

    public Version m4694a(ErrorCorrectionLevel errorCorrectionLevel) {
        return this.f2809e[errorCorrectionLevel.ordinal()];
    }

    public static Version m4689a(int i) {
        if (i % 4 != 1) {
            throw FormatException.m4607a();
        }
        try {
            return Version.m4690b((i - 17) / 4);
        } catch (IllegalArgumentException e) {
            throw FormatException.m4607a();
        }
    }

    public static Version m4690b(int i) {
        if (i >= 1 && i <= 40) {
            return f2806b[i - 1];
        }
        throw new IllegalArgumentException();
    }

    static Version m4691c(int i) {
        int i2 = 0;
        int i3 = Integer.MAX_VALUE;
        int i4 = 0;
        while (i2 < f2805a.length) {
            int i5 = f2805a[i2];
            if (i5 == i) {
                return Version.m4690b(i2 + 7);
            }
            i5 = FormatInformation.m4674a(i, i5);
            if (i5 < i3) {
                i4 = i2 + 7;
                i3 = i5;
            }
            i2++;
        }
        if (i3 <= 3) {
            return Version.m4690b(i4);
        }
        return null;
    }

    BitMatrix m4698e() {
        int d = m4697d();
        BitMatrix bitMatrix = new BitMatrix(d);
        bitMatrix.m4835a(0, 0, 9, 9);
        bitMatrix.m4835a(d - 8, 0, 8, 9);
        bitMatrix.m4835a(0, d - 8, 9, 8);
        int length = this.f2808d.length;
        int i = 0;
        while (i < length) {
            int i2 = this.f2808d[i] - 2;
            int i3 = 0;
            while (i3 < length) {
                if (!((i == 0 && (i3 == 0 || i3 == length - 1)) || (i == length - 1 && i3 == 0))) {
                    bitMatrix.m4835a(this.f2808d[i3] - 2, i2, 5, 5);
                }
                i3++;
            }
            i++;
        }
        bitMatrix.m4835a(6, 9, 1, d - 17);
        bitMatrix.m4835a(9, 6, d - 17, 1);
        if (this.f2807c > 6) {
            bitMatrix.m4835a(d - 11, 0, 3, 6);
            bitMatrix.m4835a(0, d - 11, 6, 3);
        }
        return bitMatrix;
    }

    public String toString() {
        return String.valueOf(this.f2807c);
    }

    private static Version[] m4692f() {
        r0 = new Version[40];
        int[] iArr = new int[0];
        Version[] versionArr = new Version[4];
        versionArr[0] = new Version(7, new Version(1, 19));
        versionArr[1] = new Version(10, new Version(1, 16));
        versionArr[2] = new Version(13, new Version(1, 13));
        versionArr[3] = new Version(17, new Version(1, 9));
        r0[0] = new Version(1, iArr, versionArr);
        iArr = new int[]{6, 18};
        versionArr = new Version[4];
        versionArr[0] = new Version(10, new Version(1, 34));
        versionArr[1] = new Version(16, new Version(1, 28));
        versionArr[2] = new Version(22, new Version(1, 22));
        versionArr[3] = new Version(28, new Version(1, 16));
        r0[1] = new Version(2, iArr, versionArr);
        iArr = new int[]{6, 22};
        versionArr = new Version[4];
        versionArr[0] = new Version(15, new Version(1, 55));
        versionArr[1] = new Version(26, new Version(1, 44));
        versionArr[2] = new Version(18, new Version(2, 17));
        versionArr[3] = new Version(22, new Version(2, 13));
        r0[2] = new Version(3, iArr, versionArr);
        iArr = new int[]{6, 26};
        versionArr = new Version[4];
        versionArr[0] = new Version(20, new Version(1, 80));
        versionArr[1] = new Version(18, new Version(2, 32));
        versionArr[2] = new Version(26, new Version(2, 24));
        versionArr[3] = new Version(16, new Version(4, 9));
        r0[3] = new Version(4, iArr, versionArr);
        iArr = new int[]{6, 30};
        versionArr = new Version[4];
        versionArr[0] = new Version(26, new Version(1, R.AppCompatTheme_ratingBarStyleSmall));
        versionArr[1] = new Version(24, new Version(2, 43));
        versionArr[2] = new Version(18, new Version(2, 15), new Version(2, 16));
        versionArr[3] = new Version(22, new Version(2, 11), new Version(2, 12));
        r0[4] = new Version(5, iArr, versionArr);
        iArr = new int[]{6, 34};
        versionArr = new Version[4];
        versionArr[0] = new Version(18, new Version(2, 68));
        versionArr[1] = new Version(16, new Version(4, 27));
        versionArr[2] = new Version(24, new Version(4, 19));
        versionArr[3] = new Version(28, new Version(4, 15));
        r0[5] = new Version(6, iArr, versionArr);
        iArr = new int[]{6, 22, 38};
        versionArr = new Version[4];
        versionArr[0] = new Version(20, new Version(2, 78));
        versionArr[1] = new Version(18, new Version(4, 31));
        versionArr[2] = new Version(18, new Version(2, 14), new Version(4, 15));
        versionArr[3] = new Version(26, new Version(4, 13), new Version(1, 14));
        r0[6] = new Version(7, iArr, versionArr);
        iArr = new int[]{6, 24, 42};
        versionArr = new Version[4];
        versionArr[0] = new Version(24, new Version(2, 97));
        versionArr[1] = new Version(22, new Version(2, 38), new Version(2, 39));
        versionArr[2] = new Version(22, new Version(4, 18), new Version(2, 19));
        versionArr[3] = new Version(26, new Version(4, 14), new Version(2, 15));
        r0[7] = new Version(8, iArr, versionArr);
        iArr = new int[]{6, 26, 46};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(2, 116));
        versionArr[1] = new Version(22, new Version(3, 36), new Version(2, 37));
        versionArr[2] = new Version(20, new Version(4, 16), new Version(4, 17));
        versionArr[3] = new Version(24, new Version(4, 12), new Version(4, 13));
        r0[8] = new Version(9, iArr, versionArr);
        iArr = new int[]{6, 28, 50};
        versionArr = new Version[4];
        versionArr[0] = new Version(18, new Version(2, 68), new Version(2, 69));
        versionArr[1] = new Version(26, new Version(4, 43), new Version(1, 44));
        versionArr[2] = new Version(24, new Version(6, 19), new Version(2, 20));
        versionArr[3] = new Version(28, new Version(6, 15), new Version(2, 16));
        r0[9] = new Version(10, iArr, versionArr);
        iArr = new int[]{6, 30, 54};
        versionArr = new Version[4];
        versionArr[0] = new Version(20, new Version(4, 81));
        versionArr[1] = new Version(30, new Version(1, 50), new Version(4, 51));
        versionArr[2] = new Version(28, new Version(4, 22), new Version(4, 23));
        versionArr[3] = new Version(24, new Version(3, 12), new Version(8, 13));
        r0[10] = new Version(11, iArr, versionArr);
        iArr = new int[]{6, 32, 58};
        versionArr = new Version[4];
        versionArr[0] = new Version(24, new Version(2, 92), new Version(2, 93));
        versionArr[1] = new Version(22, new Version(6, 36), new Version(2, 37));
        versionArr[2] = new Version(26, new Version(4, 20), new Version(6, 21));
        versionArr[3] = new Version(28, new Version(7, 14), new Version(4, 15));
        r0[11] = new Version(12, iArr, versionArr);
        iArr = new int[]{6, 34, 62};
        versionArr = new Version[4];
        versionArr[0] = new Version(26, new Version(4, R.AppCompatTheme_ratingBarStyleIndicator));
        versionArr[1] = new Version(22, new Version(8, 37), new Version(1, 38));
        versionArr[2] = new Version(24, new Version(8, 20), new Version(4, 21));
        versionArr[3] = new Version(22, new Version(12, 11), new Version(4, 12));
        r0[12] = new Version(13, iArr, versionArr);
        iArr = new int[]{6, 26, 46, 66};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(3, 115), new Version(1, 116));
        versionArr[1] = new Version(24, new Version(4, 40), new Version(5, 41));
        versionArr[2] = new Version(20, new Version(11, 16), new Version(5, 17));
        versionArr[3] = new Version(24, new Version(11, 12), new Version(5, 13));
        r0[13] = new Version(14, iArr, versionArr);
        iArr = new int[]{6, 26, 48, 70};
        versionArr = new Version[4];
        versionArr[0] = new Version(22, new Version(5, 87), new Version(1, 88));
        versionArr[1] = new Version(24, new Version(5, 41), new Version(5, 42));
        versionArr[2] = new Version(30, new Version(5, 24), new Version(7, 25));
        versionArr[3] = new Version(24, new Version(11, 12), new Version(7, 13));
        r0[14] = new Version(15, iArr, versionArr);
        iArr = new int[]{6, 26, 50, 74};
        versionArr = new Version[4];
        versionArr[0] = new Version(24, new Version(5, 98), new Version(1, 99));
        versionArr[1] = new Version(28, new Version(7, 45), new Version(3, 46));
        versionArr[2] = new Version(24, new Version(15, 19), new Version(2, 20));
        versionArr[3] = new Version(30, new Version(3, 15), new Version(13, 16));
        r0[15] = new Version(16, iArr, versionArr);
        iArr = new int[]{6, 30, 54, 78};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(1, R.AppCompatTheme_ratingBarStyleIndicator), new Version(5, R.AppCompatTheme_ratingBarStyleSmall));
        versionArr[1] = new Version(28, new Version(10, 46), new Version(1, 47));
        versionArr[2] = new Version(28, new Version(1, 22), new Version(15, 23));
        versionArr[3] = new Version(28, new Version(2, 14), new Version(17, 15));
        r0[16] = new Version(17, iArr, versionArr);
        iArr = new int[]{6, 30, 56, 82};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(5, 120), new Version(1, 121));
        versionArr[1] = new Version(26, new Version(9, 43), new Version(4, 44));
        versionArr[2] = new Version(28, new Version(17, 22), new Version(1, 23));
        versionArr[3] = new Version(28, new Version(2, 14), new Version(19, 15));
        r0[17] = new Version(18, iArr, versionArr);
        iArr = new int[]{6, 30, 58, 86};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(3, 113), new Version(4, 114));
        versionArr[1] = new Version(26, new Version(3, 44), new Version(11, 45));
        versionArr[2] = new Version(26, new Version(17, 21), new Version(4, 22));
        versionArr[3] = new Version(26, new Version(9, 13), new Version(16, 14));
        r0[18] = new Version(19, iArr, versionArr);
        iArr = new int[]{6, 34, 62, 90};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(3, R.AppCompatTheme_ratingBarStyleIndicator), new Version(5, R.AppCompatTheme_ratingBarStyleSmall));
        versionArr[1] = new Version(26, new Version(3, 41), new Version(13, 42));
        versionArr[2] = new Version(30, new Version(15, 24), new Version(5, 25));
        versionArr[3] = new Version(28, new Version(15, 15), new Version(10, 16));
        r0[19] = new Version(20, iArr, versionArr);
        iArr = new int[]{6, 28, 50, 72, 94};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(4, 116), new Version(4, 117));
        versionArr[1] = new Version(26, new Version(17, 42));
        versionArr[2] = new Version(28, new Version(17, 22), new Version(6, 23));
        versionArr[3] = new Version(30, new Version(19, 16), new Version(6, 17));
        r0[20] = new Version(21, iArr, versionArr);
        iArr = new int[]{6, 26, 50, 74, 98};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(2, R.AppCompatTheme_switchStyle), new Version(7, 112));
        versionArr[1] = new Version(28, new Version(17, 46));
        versionArr[2] = new Version(30, new Version(7, 24), new Version(16, 25));
        versionArr[3] = new Version(24, new Version(34, 13));
        r0[21] = new Version(22, iArr, versionArr);
        iArr = new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(4, 121), new Version(5, 122));
        versionArr[1] = new Version(28, new Version(4, 47), new Version(14, 48));
        versionArr[2] = new Version(30, new Version(11, 24), new Version(14, 25));
        versionArr[3] = new Version(30, new Version(16, 15), new Version(14, 16));
        r0[22] = new Version(23, iArr, versionArr);
        iArr = new int[]{6, 28, 54, 80, R.AppCompatTheme_ratingBarStyle};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(6, 117), new Version(4, 118));
        versionArr[1] = new Version(28, new Version(6, 45), new Version(14, 46));
        versionArr[2] = new Version(30, new Version(11, 24), new Version(16, 25));
        versionArr[3] = new Version(30, new Version(30, 16), new Version(2, 17));
        r0[23] = new Version(24, iArr, versionArr);
        iArr = new int[]{6, 32, 58, 84, R.AppCompatTheme_spinnerStyle};
        versionArr = new Version[4];
        versionArr[0] = new Version(26, new Version(8, R.AppCompatTheme_ratingBarStyle), new Version(4, R.AppCompatTheme_ratingBarStyleIndicator));
        versionArr[1] = new Version(28, new Version(8, 47), new Version(13, 48));
        versionArr[2] = new Version(30, new Version(7, 24), new Version(22, 25));
        versionArr[3] = new Version(30, new Version(22, 15), new Version(13, 16));
        r0[24] = new Version(25, iArr, versionArr);
        iArr = new int[]{6, 30, 58, 86, 114};
        versionArr = new Version[4];
        versionArr[0] = new Version(28, new Version(10, 114), new Version(2, 115));
        versionArr[1] = new Version(28, new Version(19, 46), new Version(4, 47));
        versionArr[2] = new Version(28, new Version(28, 22), new Version(6, 23));
        versionArr[3] = new Version(30, new Version(33, 16), new Version(4, 17));
        r0[25] = new Version(26, iArr, versionArr);
        iArr = new int[]{6, 34, 62, 90, 118};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(8, 122), new Version(4, 123));
        versionArr[1] = new Version(28, new Version(22, 45), new Version(3, 46));
        versionArr[2] = new Version(30, new Version(8, 23), new Version(26, 24));
        versionArr[3] = new Version(30, new Version(12, 15), new Version(28, 16));
        r0[26] = new Version(27, iArr, versionArr);
        iArr = new int[]{6, 26, 50, 74, 98, 122};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(3, 117), new Version(10, 118));
        versionArr[1] = new Version(28, new Version(3, 45), new Version(23, 46));
        versionArr[2] = new Version(30, new Version(4, 24), new Version(31, 25));
        versionArr[3] = new Version(30, new Version(11, 15), new Version(31, 16));
        r0[27] = new Version(28, iArr, versionArr);
        iArr = new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle, 126};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(7, 116), new Version(7, 117));
        versionArr[1] = new Version(28, new Version(21, 45), new Version(7, 46));
        versionArr[2] = new Version(30, new Version(1, 23), new Version(37, 24));
        versionArr[3] = new Version(30, new Version(19, 15), new Version(26, 16));
        r0[28] = new Version(29, iArr, versionArr);
        iArr = new int[]{6, 26, 52, 78, R.AppCompatTheme_editTextStyle, 130};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(5, 115), new Version(10, 116));
        versionArr[1] = new Version(28, new Version(19, 47), new Version(10, 48));
        versionArr[2] = new Version(30, new Version(15, 24), new Version(25, 25));
        versionArr[3] = new Version(30, new Version(23, 15), new Version(25, 16));
        r0[29] = new Version(30, iArr, versionArr);
        iArr = new int[]{6, 30, 56, 82, R.AppCompatTheme_ratingBarStyleSmall, 134};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(13, 115), new Version(3, 116));
        versionArr[1] = new Version(28, new Version(2, 46), new Version(29, 47));
        versionArr[2] = new Version(30, new Version(42, 24), new Version(1, 25));
        versionArr[3] = new Version(30, new Version(23, 15), new Version(28, 16));
        r0[30] = new Version(31, iArr, versionArr);
        iArr = new int[]{6, 34, 60, 86, 112, 138};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(17, 115));
        versionArr[1] = new Version(28, new Version(10, 46), new Version(23, 47));
        versionArr[2] = new Version(30, new Version(10, 24), new Version(35, 25));
        versionArr[3] = new Version(30, new Version(19, 15), new Version(35, 16));
        r0[31] = new Version(32, iArr, versionArr);
        iArr = new int[]{6, 30, 58, 86, 114, 142};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(17, 115), new Version(1, 116));
        versionArr[1] = new Version(28, new Version(14, 46), new Version(21, 47));
        versionArr[2] = new Version(30, new Version(29, 24), new Version(19, 25));
        versionArr[3] = new Version(30, new Version(11, 15), new Version(46, 16));
        r0[32] = new Version(33, iArr, versionArr);
        iArr = new int[]{6, 34, 62, 90, 118, 146};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(13, 115), new Version(6, 116));
        versionArr[1] = new Version(28, new Version(14, 46), new Version(23, 47));
        versionArr[2] = new Version(30, new Version(44, 24), new Version(7, 25));
        versionArr[3] = new Version(30, new Version(59, 16), new Version(1, 17));
        r0[33] = new Version(34, iArr, versionArr);
        iArr = new int[]{6, 30, 54, 78, R.AppCompatTheme_checkboxStyle, 126, 150};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(12, 121), new Version(7, 122));
        versionArr[1] = new Version(28, new Version(12, 47), new Version(26, 48));
        versionArr[2] = new Version(30, new Version(39, 24), new Version(14, 25));
        versionArr[3] = new Version(30, new Version(22, 15), new Version(41, 16));
        r0[34] = new Version(35, iArr, versionArr);
        iArr = new int[]{6, 24, 50, 76, R.AppCompatTheme_checkboxStyle, 128, 154};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(6, 121), new Version(14, 122));
        versionArr[1] = new Version(28, new Version(6, 47), new Version(34, 48));
        versionArr[2] = new Version(30, new Version(46, 24), new Version(10, 25));
        versionArr[3] = new Version(30, new Version(2, 15), new Version(64, 16));
        r0[35] = new Version(36, iArr, versionArr);
        iArr = new int[]{6, 28, 54, 80, R.AppCompatTheme_ratingBarStyle, 132, 158};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(17, 122), new Version(4, 123));
        versionArr[1] = new Version(28, new Version(29, 46), new Version(14, 47));
        versionArr[2] = new Version(30, new Version(49, 24), new Version(10, 25));
        versionArr[3] = new Version(30, new Version(24, 15), new Version(46, 16));
        r0[36] = new Version(37, iArr, versionArr);
        iArr = new int[]{6, 32, 58, 84, R.AppCompatTheme_spinnerStyle, 136, 162};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(4, 122), new Version(18, 123));
        versionArr[1] = new Version(28, new Version(13, 46), new Version(32, 47));
        versionArr[2] = new Version(30, new Version(48, 24), new Version(14, 25));
        versionArr[3] = new Version(30, new Version(42, 15), new Version(32, 16));
        r0[37] = new Version(38, iArr, versionArr);
        iArr = new int[]{6, 26, 54, 82, R.AppCompatTheme_spinnerStyle, 138, 166};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(20, 117), new Version(4, 118));
        versionArr[1] = new Version(28, new Version(40, 47), new Version(7, 48));
        versionArr[2] = new Version(30, new Version(43, 24), new Version(22, 25));
        versionArr[3] = new Version(30, new Version(10, 15), new Version(67, 16));
        r0[38] = new Version(39, iArr, versionArr);
        iArr = new int[]{6, 30, 58, 86, 114, 142, 170};
        versionArr = new Version[4];
        versionArr[0] = new Version(30, new Version(19, 118), new Version(6, 119));
        versionArr[1] = new Version(28, new Version(18, 47), new Version(31, 48));
        versionArr[2] = new Version(30, new Version(34, 24), new Version(34, 25));
        versionArr[3] = new Version(30, new Version(20, 15), new Version(61, 16));
        r0[39] = new Version(40, iArr, versionArr);
        return r0;
    }
}
