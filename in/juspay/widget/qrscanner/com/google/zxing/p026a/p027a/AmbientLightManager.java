package in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraManager;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraSettings;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.a */
public final class AmbientLightManager implements SensorEventListener {
    private CameraManager f2721a;
    private CameraSettings f2722b;
    private Sensor f2723c;
    private Context f2724d;
    private Handler f2725e;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.a.1 */
    class AmbientLightManager implements Runnable {
        final /* synthetic */ boolean f2717a;
        final /* synthetic */ AmbientLightManager f2718b;

        AmbientLightManager(AmbientLightManager ambientLightManager, boolean z) {
            this.f2718b = ambientLightManager;
            this.f2717a = z;
        }

        public void run() {
            this.f2718b.f2721a.m5030a(this.f2717a);
        }
    }

    public AmbientLightManager(Context context, CameraManager cameraManager, CameraSettings cameraSettings) {
        this.f2724d = context;
        this.f2721a = cameraManager;
        this.f2722b = cameraSettings;
        this.f2725e = new Handler();
    }

    public void m4627a() {
        if (this.f2722b.m5046h()) {
            SensorManager sensorManager = (SensorManager) this.f2724d.getSystemService("sensor");
            this.f2723c = sensorManager.getDefaultSensor(5);
            if (this.f2723c != null) {
                sensorManager.registerListener(this, this.f2723c, 3);
            }
        }
    }

    public void m4628b() {
        if (this.f2723c != null) {
            ((SensorManager) this.f2724d.getSystemService("sensor")).unregisterListener(this);
            this.f2723c = null;
        }
    }

    private void m4626a(boolean z) {
        this.f2725e.post(new AmbientLightManager(this, z));
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float f = sensorEvent.values[0];
        if (this.f2721a == null) {
            return;
        }
        if (f <= 45.0f) {
            m4626a(true);
        } else if (f >= 450.0f) {
            m4626a(false);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
