package in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.p028a;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.hardware.Camera.Area;
import android.hardware.Camera.Parameters;
import android.util.Log;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.p035a.CameraSettings.CameraSettings;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a */
public final class CameraConfigurationUtils {
    private static final Pattern f2720a;

    static {
        f2720a = Pattern.compile(";");
    }

    public static void m4617a(Parameters parameters, CameraSettings cameraSettings, boolean z) {
        Collection supportedFocusModes = parameters.getSupportedFocusModes();
        String str = null;
        if (z || cameraSettings == CameraSettings.AUTO) {
            str = CameraConfigurationUtils.m4612a("focus mode", supportedFocusModes, "auto");
        } else if (cameraSettings == CameraSettings.CONTINUOUS) {
            str = CameraConfigurationUtils.m4612a("focus mode", supportedFocusModes, "continuous-picture", "continuous-video", "auto");
        } else if (cameraSettings == CameraSettings.INFINITY) {
            str = CameraConfigurationUtils.m4612a("focus mode", supportedFocusModes, "infinity");
        } else if (cameraSettings == CameraSettings.MACRO) {
            str = CameraConfigurationUtils.m4612a("focus mode", supportedFocusModes, "macro");
        }
        if (!z && r0 == null) {
            str = CameraConfigurationUtils.m4612a("focus mode", supportedFocusModes, "macro", "edof");
        }
        if (str == null) {
            return;
        }
        if (str.equals(parameters.getFocusMode())) {
            Log.i("CameraConfiguration", "Focus mode already set to " + str);
        } else {
            parameters.setFocusMode(str);
        }
    }

    public static void m4618a(Parameters parameters, boolean z) {
        String a;
        Collection supportedFlashModes = parameters.getSupportedFlashModes();
        if (z) {
            a = CameraConfigurationUtils.m4612a("flash mode", supportedFlashModes, "torch", "on");
        } else {
            a = CameraConfigurationUtils.m4612a("flash mode", supportedFlashModes, "off");
        }
        if (a == null) {
            return;
        }
        if (a.equals(parameters.getFlashMode())) {
            Log.i("CameraConfiguration", "Flash mode already set to " + a);
            return;
        }
        Log.i("CameraConfiguration", "Setting flash mode to " + a);
        parameters.setFlashMode(a);
    }

    public static void m4620b(Parameters parameters, boolean z) {
        float f = 0.0f;
        int minExposureCompensation = parameters.getMinExposureCompensation();
        int maxExposureCompensation = parameters.getMaxExposureCompensation();
        float exposureCompensationStep = parameters.getExposureCompensationStep();
        if (!(minExposureCompensation == 0 && maxExposureCompensation == 0) && exposureCompensationStep > 0.0f) {
            if (!z) {
                f = 1.5f;
            }
            int round = Math.round(f / exposureCompensationStep);
            exposureCompensationStep *= (float) round;
            round = Math.max(Math.min(round, maxExposureCompensation), minExposureCompensation);
            if (parameters.getExposureCompensation() == round) {
                Log.i("CameraConfiguration", "Exposure compensation already set to " + round + " / " + exposureCompensationStep);
                return;
            }
            Log.i("CameraConfiguration", "Setting exposure compensation to " + round + " / " + exposureCompensationStep);
            parameters.setExposureCompensation(round);
            return;
        }
        Log.i("CameraConfiguration", "Camera does not support exposure compensation");
    }

    public static void m4615a(Parameters parameters) {
        CameraConfigurationUtils.m4616a(parameters, 10, 20);
    }

    public static void m4616a(Parameters parameters, int i, int i2) {
        Collection<int[]> supportedPreviewFpsRange = parameters.getSupportedPreviewFpsRange();
        Log.i("CameraConfiguration", "Supported FPS ranges: " + CameraConfigurationUtils.m4613a((Collection) supportedPreviewFpsRange));
        if (supportedPreviewFpsRange != null && !supportedPreviewFpsRange.isEmpty()) {
            for (int[] iArr : supportedPreviewFpsRange) {
                int i3 = iArr[0];
                int i4 = iArr[1];
                if (i3 >= i * 1000 && i4 <= i2 * 1000) {
                    break;
                }
            }
            int[] iArr2 = null;
            if (iArr2 == null) {
                Log.i("CameraConfiguration", "No suitable FPS range?");
                return;
            }
            int[] iArr3 = new int[2];
            parameters.getPreviewFpsRange(iArr3);
            if (Arrays.equals(iArr3, iArr2)) {
                Log.i("CameraConfiguration", "FPS range already set to " + Arrays.toString(iArr2));
                return;
            }
            Log.i("CameraConfiguration", "Setting FPS range to " + Arrays.toString(iArr2));
            parameters.setPreviewFpsRange(iArr2[0], iArr2[1]);
        }
    }

    @TargetApi(15)
    public static void m4619b(Parameters parameters) {
        if (parameters.getMaxNumFocusAreas() > 0) {
            Log.i("CameraConfiguration", "Old focus areas: " + CameraConfigurationUtils.m4611a(parameters.getFocusAreas()));
            Iterable a = CameraConfigurationUtils.m4614a(400);
            Log.i("CameraConfiguration", "Setting focus area to : " + CameraConfigurationUtils.m4611a(a));
            parameters.setFocusAreas(a);
            return;
        }
        Log.i("CameraConfiguration", "Device does not support focus areas");
    }

    @TargetApi(15)
    public static void m4621c(Parameters parameters) {
        if (parameters.getMaxNumMeteringAreas() > 0) {
            Log.i("CameraConfiguration", "Old metering areas: " + parameters.getMeteringAreas());
            Iterable a = CameraConfigurationUtils.m4614a(400);
            Log.i("CameraConfiguration", "Setting metering area to : " + CameraConfigurationUtils.m4611a(a));
            parameters.setMeteringAreas(a);
            return;
        }
        Log.i("CameraConfiguration", "Device does not support metering areas");
    }

    @TargetApi(15)
    private static List<Area> m4614a(int i) {
        return Collections.singletonList(new Area(new Rect(-i, -i, i, i), 1));
    }

    @TargetApi(15)
    public static void m4622d(Parameters parameters) {
        if (!parameters.isVideoStabilizationSupported()) {
            Log.i("CameraConfiguration", "This device does not support video stabilization");
        } else if (parameters.getVideoStabilization()) {
            Log.i("CameraConfiguration", "Video stabilization already enabled");
        } else {
            Log.i("CameraConfiguration", "Enabling video stabilization...");
            parameters.setVideoStabilization(true);
        }
    }

    public static void m4623e(Parameters parameters) {
        if ("barcode".equals(parameters.getSceneMode())) {
            Log.i("CameraConfiguration", "Barcode scene mode already set");
            return;
        }
        String a = CameraConfigurationUtils.m4612a("scene mode", parameters.getSupportedSceneModes(), "barcode");
        if (a != null) {
            parameters.setSceneMode(a);
        }
    }

    public static void m4624f(Parameters parameters) {
        if ("negative".equals(parameters.getColorEffect())) {
            Log.i("CameraConfiguration", "Negative effect already set");
            return;
        }
        String a = CameraConfigurationUtils.m4612a("color effect", parameters.getSupportedColorEffects(), "negative");
        if (a != null) {
            parameters.setColorEffect(a);
        }
    }

    private static String m4612a(String str, Collection<String> collection, String... strArr) {
        Log.i("CameraConfiguration", "Requesting " + str + " value from among: " + Arrays.toString(strArr));
        Log.i("CameraConfiguration", "Supported " + str + " values: " + collection);
        if (collection != null) {
            for (String str2 : strArr) {
                if (collection.contains(str2)) {
                    Log.i("CameraConfiguration", "Can set " + str + " to: " + str2);
                    return str2;
                }
            }
        }
        Log.i("CameraConfiguration", "No supported values match");
        return null;
    }

    private static String m4613a(Collection<int[]> collection) {
        if (collection == null || collection.isEmpty()) {
            return "[]";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append('[');
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            stringBuilder.append(Arrays.toString((int[]) it.next()));
            if (it.hasNext()) {
                stringBuilder.append(", ");
            }
        }
        stringBuilder.append(']');
        return stringBuilder.toString();
    }

    @TargetApi(15)
    private static String m4611a(Iterable<Area> iterable) {
        if (iterable == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (Area area : iterable) {
            stringBuilder.append(area.rect).append(':').append(area.weight).append(' ');
        }
        return stringBuilder.toString();
    }
}
