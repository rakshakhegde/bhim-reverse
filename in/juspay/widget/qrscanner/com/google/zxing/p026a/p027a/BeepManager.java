package in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.util.Log;
import in.juspay.widget.qrscanner.R.R;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.b */
public final class BeepManager {
    private static final String f2728a;
    private final Context f2729b;
    private boolean f2730c;
    private boolean f2731d;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.b.1 */
    class BeepManager implements OnCompletionListener {
        final /* synthetic */ BeepManager f2726a;

        BeepManager(BeepManager beepManager) {
            this.f2726a = beepManager;
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.b.2 */
    class BeepManager implements OnErrorListener {
        final /* synthetic */ BeepManager f2727a;

        BeepManager(BeepManager beepManager) {
            this.f2727a = beepManager;
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            Log.w(BeepManager.f2728a, "Failed to beep " + i + ", " + i2);
            mediaPlayer.stop();
            mediaPlayer.release();
            return true;
        }
    }

    static {
        f2728a = BeepManager.class.getSimpleName();
    }

    public BeepManager(Activity activity) {
        this.f2730c = true;
        this.f2731d = false;
        activity.setVolumeControlStream(3);
        this.f2729b = activity.getApplicationContext();
    }

    public void m4631a(boolean z) {
        this.f2730c = z;
    }

    public MediaPlayer m4630a() {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(3);
        mediaPlayer.setOnCompletionListener(new BeepManager(this));
        mediaPlayer.setOnErrorListener(new BeepManager(this));
        AssetFileDescriptor openRawResourceFd;
        try {
            openRawResourceFd = this.f2729b.getResources().openRawResourceFd(R.zxing_beep);
            mediaPlayer.setDataSource(openRawResourceFd.getFileDescriptor(), openRawResourceFd.getStartOffset(), openRawResourceFd.getLength());
            openRawResourceFd.close();
            mediaPlayer.setVolume(0.1f, 0.1f);
            mediaPlayer.prepare();
            mediaPlayer.start();
            return mediaPlayer;
        } catch (Throwable e) {
            Log.w(f2728a, e);
            mediaPlayer.release();
            return null;
        } catch (Throwable th) {
            openRawResourceFd.close();
        }
    }
}
