package in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.p028a.p029a;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.util.Log;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a */
public final class OpenCameraInterface {
    private static final String f2719a;

    static {
        f2719a = OpenCameraInterface.class.getName();
    }

    private OpenCameraInterface() {
    }

    public static int m4609a(int i) {
        int numberOfCameras = Camera.getNumberOfCameras();
        if (numberOfCameras == 0) {
            Log.w(f2719a, "No cameras!");
            return -1;
        }
        int i2;
        Object obj = i >= 0 ? 1 : null;
        if (obj == null) {
            i2 = 0;
            while (i2 < numberOfCameras) {
                CameraInfo cameraInfo = new CameraInfo();
                Camera.getCameraInfo(i2, cameraInfo);
                if (cameraInfo.facing == 0) {
                    break;
                }
                i2++;
            }
        } else {
            i2 = i;
        }
        if (i2 < numberOfCameras) {
            return i2;
        }
        if (obj == null) {
            return 0;
        }
        return -1;
    }

    public static Camera m4610b(int i) {
        int a = OpenCameraInterface.m4609a(i);
        if (a == -1) {
            return null;
        }
        return Camera.open(a);
    }
}
