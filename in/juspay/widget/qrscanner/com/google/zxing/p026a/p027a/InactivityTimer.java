package in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.c */
public final class InactivityTimer {
    private static final String f2735a;
    private final Context f2736b;
    private final BroadcastReceiver f2737c;
    private boolean f2738d;
    private Handler f2739e;
    private Runnable f2740f;
    private boolean f2741g;

    /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.c.a */
    private final class InactivityTimer extends BroadcastReceiver {
        final /* synthetic */ InactivityTimer f2734a;

        /* renamed from: in.juspay.widget.qrscanner.com.google.zxing.a.a.c.a.1 */
        class InactivityTimer implements Runnable {
            final /* synthetic */ boolean f2732a;
            final /* synthetic */ InactivityTimer f2733b;

            InactivityTimer(InactivityTimer inactivityTimer, boolean z) {
                this.f2733b = inactivityTimer;
                this.f2732a = z;
            }

            public void run() {
                this.f2733b.f2734a.m4634a(this.f2732a);
            }
        }

        private InactivityTimer(InactivityTimer inactivityTimer) {
            this.f2734a = inactivityTimer;
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                this.f2734a.f2739e.post(new InactivityTimer(this, intent.getIntExtra("plugged", -1) <= 0));
            }
        }
    }

    static {
        f2735a = InactivityTimer.class.getSimpleName();
    }

    public InactivityTimer(Context context, Runnable runnable) {
        this.f2738d = false;
        this.f2736b = context;
        this.f2740f = runnable;
        this.f2737c = new InactivityTimer();
        this.f2739e = new Handler();
    }

    public void m4638a() {
        m4637f();
        if (this.f2741g) {
            this.f2739e.postDelayed(this.f2740f, 300000);
        }
    }

    public void m4639b() {
        m4636e();
        m4638a();
    }

    public void m4640c() {
        m4637f();
        m4635d();
    }

    private void m4635d() {
        if (this.f2738d) {
            this.f2736b.unregisterReceiver(this.f2737c);
            this.f2738d = false;
        }
    }

    private void m4636e() {
        if (!this.f2738d) {
            this.f2736b.registerReceiver(this.f2737c, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            this.f2738d = true;
        }
    }

    private void m4637f() {
        this.f2739e.removeCallbacksAndMessages(null);
    }

    private void m4634a(boolean z) {
        this.f2741g = z;
        if (this.f2738d) {
            m4638a();
        }
    }
}
