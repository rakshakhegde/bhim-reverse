package in.juspay.widget.qrscanner.com.google.zxing;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.e */
public enum EncodeHintType {
    ERROR_CORRECTION,
    CHARACTER_SET,
    MIN_SIZE,
    MAX_SIZE,
    MARGIN,
    QR_VERSION
}
