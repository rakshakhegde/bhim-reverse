package in.juspay.widget.qrscanner.com.google.zxing;

import in.juspay.widget.qrscanner.com.google.zxing.p030b.QRCodeReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.g */
public final class MultiFormatReader implements Reader {
    private Map<DecodeHintType, ?> f2967a;
    private Reader[] f2968b;

    public Result m4910a(BinaryBitmap binaryBitmap) {
        m4913a(null);
        return m4909c(binaryBitmap);
    }

    public Result m4911a(BinaryBitmap binaryBitmap, Map<DecodeHintType, ?> map) {
        m4913a((Map) map);
        return m4909c(binaryBitmap);
    }

    public Result m4914b(BinaryBitmap binaryBitmap) {
        if (this.f2968b == null) {
            m4913a(null);
        }
        return m4909c(binaryBitmap);
    }

    public void m4913a(Map<DecodeHintType, ?> map) {
        this.f2967a = map;
        Collection collection;
        Collection arrayList;
        if (map == null || map.containsKey(DecodeHintType.TRY_HARDER)) {
            if (map != null) {
                collection = null;
            } else {
                collection = (Collection) map.get(DecodeHintType.POSSIBLE_FORMATS);
            }
            arrayList = new ArrayList();
            if (collection != null && collection.contains(BarcodeFormat.QR_CODE)) {
                arrayList.add(new QRCodeReader());
            }
            if (arrayList.isEmpty()) {
                arrayList.add(new QRCodeReader());
            }
            this.f2968b = (Reader[]) arrayList.toArray(new Reader[arrayList.size()]);
        }
        if (map != null) {
            collection = (Collection) map.get(DecodeHintType.POSSIBLE_FORMATS);
        } else {
            collection = null;
        }
        arrayList = new ArrayList();
        arrayList.add(new QRCodeReader());
        if (arrayList.isEmpty()) {
            arrayList.add(new QRCodeReader());
        }
        this.f2968b = (Reader[]) arrayList.toArray(new Reader[arrayList.size()]);
    }

    public void m4912a() {
        if (this.f2968b != null) {
            for (Reader a : this.f2968b) {
                a.m4701a();
            }
        }
    }

    private Result m4909c(BinaryBitmap binaryBitmap) {
        if (this.f2968b != null) {
            Reader[] readerArr = this.f2968b;
            int length = readerArr.length;
            int i = 0;
            while (i < length) {
                try {
                    return readerArr[i].m4700a(binaryBitmap, this.f2967a);
                } catch (ReaderException e) {
                    i++;
                }
            }
        }
        throw NotFoundException.m4608a();
    }
}
