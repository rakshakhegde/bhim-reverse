package in.juspay.widget.qrscanner.com.google.zxing;

public final class ChecksumException extends ReaderException {
    private static final ChecksumException f2714c;

    static {
        f2714c = new ChecksumException();
        f2714c.setStackTrace(b);
    }

    private ChecksumException() {
    }

    public static ChecksumException m4606a() {
        return a ? new ChecksumException() : f2714c;
    }
}
