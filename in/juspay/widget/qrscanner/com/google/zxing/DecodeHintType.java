package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.d */
public enum DecodeHintType {
    OTHER(Object.class),
    PURE_BARCODE(Void.class),
    POSSIBLE_FORMATS(List.class),
    TRY_HARDER(Void.class),
    CHARACTER_SET(String.class),
    ALLOWED_LENGTHS(int[].class),
    ASSUME_CODE_39_CHECK_DIGIT(Void.class),
    ASSUME_GS1(Void.class),
    RETURN_CODABAR_START_END(Void.class),
    NEED_RESULT_POINT_CALLBACK(ResultPointCallback.class),
    ALLOWED_EAN_EXTENSIONS(int[].class);
    
    private final Class<?> f2957l;

    private DecodeHintType(Class<?> cls) {
        this.f2957l = cls;
    }
}
