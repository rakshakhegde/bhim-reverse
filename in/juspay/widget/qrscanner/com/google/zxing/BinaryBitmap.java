package in.juspay.widget.qrscanner.com.google.zxing;

import com.crashlytics.android.core.BuildConfig;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.c */
public final class BinaryBitmap {
    private final Binarizer f2856a;
    private BitMatrix f2857b;

    public BinaryBitmap(Binarizer binarizer) {
        if (binarizer == null) {
            throw new IllegalArgumentException("Binarizer must be non-null.");
        }
        this.f2856a = binarizer;
    }

    public BitMatrix m4818a() {
        if (this.f2857b == null) {
            this.f2857b = this.f2856a.m4817b();
        }
        return this.f2857b;
    }

    public String toString() {
        try {
            return m4818a().toString();
        } catch (NotFoundException e) {
            return BuildConfig.FLAVOR;
        }
    }
}
