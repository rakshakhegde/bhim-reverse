package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.c */
public final class ReedSolomonDecoder {
    private final GenericGF f2942a;

    public ReedSolomonDecoder(GenericGF genericGF) {
        this.f2942a = genericGF;
    }

    public void m4902a(int[] iArr, int i) {
        int i2 = 0;
        GenericGFPoly genericGFPoly = new GenericGFPoly(this.f2942a, iArr);
        int[] iArr2 = new int[i];
        int i3 = 1;
        for (int i4 = 0; i4 < i; i4++) {
            int b = genericGFPoly.m4894b(this.f2942a.m4880a(this.f2942a.m4888d() + i4));
            iArr2[(iArr2.length - 1) - i4] = b;
            if (b != 0) {
                i3 = 0;
            }
        }
        if (i3 == 0) {
            GenericGFPoly[] a = m4901a(this.f2942a.m4882a(i, 1), new GenericGFPoly(this.f2942a, iArr2), i);
            GenericGFPoly genericGFPoly2 = a[0];
            GenericGFPoly genericGFPoly3 = a[1];
            int[] a2 = m4899a(genericGFPoly2);
            int[] a3 = m4900a(genericGFPoly3, a2);
            while (i2 < a2.length) {
                int length = (iArr.length - 1) - this.f2942a.m4883b(a2[i2]);
                if (length < 0) {
                    throw new ReedSolomonException("Bad error location");
                }
                iArr[length] = GenericGF.m4879b(iArr[length], a3[i2]);
                i2++;
            }
        }
    }

    private GenericGFPoly[] m4901a(GenericGFPoly genericGFPoly, GenericGFPoly genericGFPoly2, int i) {
        if (genericGFPoly.m4893b() >= genericGFPoly2.m4893b()) {
            GenericGFPoly genericGFPoly3 = genericGFPoly2;
            genericGFPoly2 = genericGFPoly;
            genericGFPoly = genericGFPoly3;
        }
        GenericGFPoly a = this.f2942a.m4881a();
        GenericGFPoly b = this.f2942a.m4884b();
        while (genericGFPoly.m4893b() >= i / 2) {
            if (genericGFPoly.m4897c()) {
                throw new ReedSolomonException("r_{i-1} was zero");
            }
            GenericGFPoly a2 = this.f2942a.m4881a();
            int c = this.f2942a.m4886c(genericGFPoly.m4889a(genericGFPoly.m4893b()));
            GenericGFPoly genericGFPoly4 = a2;
            a2 = genericGFPoly2;
            while (a2.m4893b() >= genericGFPoly.m4893b() && !a2.m4897c()) {
                int b2 = a2.m4893b() - genericGFPoly.m4893b();
                int c2 = this.f2942a.m4887c(a2.m4889a(a2.m4893b()), c);
                genericGFPoly4 = genericGFPoly4.m4891a(this.f2942a.m4882a(b2, c2));
                a2 = a2.m4891a(genericGFPoly.m4890a(b2, c2));
            }
            a = genericGFPoly4.m4895b(b).m4891a(a);
            if (a2.m4893b() >= genericGFPoly.m4893b()) {
                throw new IllegalStateException("Division algorithm failed to reduce polynomial?");
            }
            genericGFPoly2 = genericGFPoly;
            genericGFPoly = a2;
            genericGFPoly3 = b;
            b = a;
            a = genericGFPoly3;
        }
        int a3 = b.m4889a(0);
        if (a3 == 0) {
            throw new ReedSolomonException("sigmaTilde(0) was zero");
        }
        a3 = this.f2942a.m4886c(a3);
        b = b.m4896c(a3);
        a = genericGFPoly.m4896c(a3);
        return new GenericGFPoly[]{b, a};
    }

    private int[] m4899a(GenericGFPoly genericGFPoly) {
        int i = 0;
        int i2 = 1;
        int b = genericGFPoly.m4893b();
        if (b == 1) {
            return new int[]{genericGFPoly.m4889a(1)};
        }
        int[] iArr = new int[b];
        while (i2 < this.f2942a.m4885c() && i < b) {
            if (genericGFPoly.m4894b(i2) == 0) {
                iArr[i] = this.f2942a.m4886c(i2);
                i++;
            }
            i2++;
        }
        if (i == b) {
            return iArr;
        }
        throw new ReedSolomonException("Error locator degree does not match number of roots");
    }

    private int[] m4900a(GenericGFPoly genericGFPoly, int[] iArr) {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        for (int i = 0; i < length; i++) {
            int c = this.f2942a.m4886c(iArr[i]);
            int i2 = 1;
            int i3 = 0;
            while (i3 < length) {
                int c2;
                if (i != i3) {
                    c2 = this.f2942a.m4887c(iArr[i3], c);
                    c2 = this.f2942a.m4887c(i2, (c2 & 1) == 0 ? c2 | 1 : c2 & -2);
                } else {
                    c2 = i2;
                }
                i3++;
                i2 = c2;
            }
            iArr2[i] = this.f2942a.m4887c(genericGFPoly.m4894b(c), this.f2942a.m4886c(i2));
            if (this.f2942a.m4888d() != 0) {
                iArr2[i] = this.f2942a.m4887c(iArr2[i], c);
            }
        }
        return iArr2;
    }
}
