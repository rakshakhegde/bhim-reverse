package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.g */
public class DetectorResult {
    private final BitMatrix f2908a;
    private final ResultPoint[] f2909b;

    public DetectorResult(BitMatrix bitMatrix, ResultPoint[] resultPointArr) {
        this.f2908a = bitMatrix;
        this.f2909b = resultPointArr;
    }

    public final BitMatrix m4862a() {
        return this.f2908a;
    }

    public final ResultPoint[] m4863b() {
        return this.f2909b;
    }
}
