package in.juspay.widget.qrscanner.com.google.zxing.common;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.c */
public final class BitSource {
    private final byte[] f2864a;
    private int f2865b;
    private int f2866c;

    public BitSource(byte[] bArr) {
        this.f2864a = bArr;
    }

    public int m4845a(int i) {
        if (i < 1 || i > 32 || i > m4844a()) {
            throw new IllegalArgumentException(String.valueOf(i));
        }
        int i2;
        int i3;
        if (this.f2866c > 0) {
            i2 = 8 - this.f2866c;
            i3 = i < i2 ? i : i2;
            i2 -= i3;
            i2 = (((255 >> (8 - i3)) << i2) & this.f2864a[this.f2865b]) >> i2;
            i -= i3;
            this.f2866c = i3 + this.f2866c;
            if (this.f2866c == 8) {
                this.f2866c = 0;
                this.f2865b++;
            }
            i3 = i2;
            i2 = i;
        } else {
            i3 = 0;
            i2 = i;
        }
        if (i2 <= 0) {
            return i3;
        }
        while (i2 >= 8) {
            i3 = (i3 << 8) | (this.f2864a[this.f2865b] & 255);
            this.f2865b++;
            i2 -= 8;
        }
        if (i2 <= 0) {
            return i3;
        }
        int i4 = 8 - i2;
        i3 = (i3 << i2) | ((((255 >> i4) << i4) & this.f2864a[this.f2865b]) >> i4);
        this.f2866c = i2 + this.f2866c;
        return i3;
    }

    public int m4844a() {
        return ((this.f2864a.length - this.f2865b) * 8) - this.f2866c;
    }
}
