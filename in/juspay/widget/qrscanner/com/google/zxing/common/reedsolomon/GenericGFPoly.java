package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.b */
final class GenericGFPoly {
    private final GenericGF f2940a;
    private final int[] f2941b;

    GenericGFPoly(GenericGF genericGF, int[] iArr) {
        if (iArr.length == 0) {
            throw new IllegalArgumentException();
        }
        this.f2940a = genericGF;
        int length = iArr.length;
        if (length <= 1 || iArr[0] != 0) {
            this.f2941b = iArr;
            return;
        }
        int i = 1;
        while (i < length && iArr[i] == 0) {
            i++;
        }
        if (i == length) {
            this.f2941b = new int[]{0};
            return;
        }
        this.f2941b = new int[(length - i)];
        System.arraycopy(iArr, i, this.f2941b, 0, this.f2941b.length);
    }

    int[] m4892a() {
        return this.f2941b;
    }

    int m4893b() {
        return this.f2941b.length - 1;
    }

    boolean m4897c() {
        return this.f2941b[0] == 0;
    }

    int m4889a(int i) {
        return this.f2941b[(this.f2941b.length - 1) - i];
    }

    int m4894b(int i) {
        int i2 = 0;
        if (i == 0) {
            return m4889a(0);
        }
        int i3;
        if (i == 1) {
            int[] iArr = this.f2941b;
            int length = iArr.length;
            i3 = 0;
            while (i2 < length) {
                int b = GenericGF.m4879b(i3, iArr[i2]);
                i2++;
                i3 = b;
            }
            return i3;
        }
        i2 = this.f2941b[0];
        int length2 = this.f2941b.length;
        i3 = i2;
        i2 = 1;
        while (i2 < length2) {
            b = GenericGF.m4879b(this.f2940a.m4887c(i, i3), this.f2941b[i2]);
            i2++;
            i3 = b;
        }
        return i3;
    }

    GenericGFPoly m4891a(GenericGFPoly genericGFPoly) {
        if (!this.f2940a.equals(genericGFPoly.f2940a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (m4897c()) {
            return genericGFPoly;
        } else {
            if (genericGFPoly.m4897c()) {
                return this;
            }
            Object obj = this.f2941b;
            Object obj2 = genericGFPoly.f2941b;
            if (obj.length <= obj2.length) {
                Object obj3 = obj2;
                obj2 = obj;
                obj = obj3;
            }
            Object obj4 = new int[obj.length];
            int length = obj.length - r1.length;
            System.arraycopy(obj, 0, obj4, 0, length);
            for (int i = length; i < obj.length; i++) {
                obj4[i] = GenericGF.m4879b(r1[i - length], obj[i]);
            }
            return new GenericGFPoly(this.f2940a, obj4);
        }
    }

    GenericGFPoly m4895b(GenericGFPoly genericGFPoly) {
        if (!this.f2940a.equals(genericGFPoly.f2940a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (m4897c() || genericGFPoly.m4897c()) {
            return this.f2940a.m4881a();
        } else {
            int[] iArr = this.f2941b;
            int length = iArr.length;
            int[] iArr2 = genericGFPoly.f2941b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    iArr3[i + i3] = GenericGF.m4879b(iArr3[i + i3], this.f2940a.m4887c(i2, iArr2[i3]));
                }
            }
            return new GenericGFPoly(this.f2940a, iArr3);
        }
    }

    GenericGFPoly m4896c(int i) {
        if (i == 0) {
            return this.f2940a.m4881a();
        }
        if (i == 1) {
            return this;
        }
        int length = this.f2941b.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = this.f2940a.m4887c(this.f2941b[i2], i);
        }
        return new GenericGFPoly(this.f2940a, iArr);
    }

    GenericGFPoly m4890a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f2940a.m4881a();
        } else {
            int length = this.f2941b.length;
            int[] iArr = new int[(length + i)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.f2940a.m4887c(this.f2941b[i3], i2);
            }
            return new GenericGFPoly(this.f2940a, iArr);
        }
    }

    GenericGFPoly[] m4898c(GenericGFPoly genericGFPoly) {
        if (!this.f2940a.equals(genericGFPoly.f2940a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (genericGFPoly.m4897c()) {
            throw new IllegalArgumentException("Divide by 0");
        } else {
            GenericGFPoly a = this.f2940a.m4881a();
            int c = this.f2940a.m4886c(genericGFPoly.m4889a(genericGFPoly.m4893b()));
            GenericGFPoly genericGFPoly2 = a;
            a = this;
            while (a.m4893b() >= genericGFPoly.m4893b() && !a.m4897c()) {
                int b = a.m4893b() - genericGFPoly.m4893b();
                int c2 = this.f2940a.m4887c(a.m4889a(a.m4893b()), c);
                GenericGFPoly a2 = genericGFPoly.m4890a(b, c2);
                genericGFPoly2 = genericGFPoly2.m4891a(this.f2940a.m4882a(b, c2));
                a = a.m4891a(a2);
            }
            return new GenericGFPoly[]{genericGFPoly2, a};
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(m4893b() * 8);
        for (int b = m4893b(); b >= 0; b--) {
            int a = m4889a(b);
            if (a != 0) {
                if (a < 0) {
                    stringBuilder.append(" - ");
                    a = -a;
                } else if (stringBuilder.length() > 0) {
                    stringBuilder.append(" + ");
                }
                if (b == 0 || a != 1) {
                    a = this.f2940a.m4883b(a);
                    if (a == 0) {
                        stringBuilder.append('1');
                    } else if (a == 1) {
                        stringBuilder.append('a');
                    } else {
                        stringBuilder.append("a^");
                        stringBuilder.append(a);
                    }
                }
                if (b != 0) {
                    if (b == 1) {
                        stringBuilder.append('x');
                    } else {
                        stringBuilder.append("x^");
                        stringBuilder.append(b);
                    }
                }
            }
        }
        return stringBuilder.toString();
    }
}
