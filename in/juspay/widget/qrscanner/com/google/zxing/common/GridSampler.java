package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.i */
public abstract class GridSampler {
    private static GridSampler f2907a;

    public abstract BitMatrix m4860a(BitMatrix bitMatrix, int i, int i2, PerspectiveTransform perspectiveTransform);

    static {
        f2907a = new DefaultGridSampler();
    }

    public static GridSampler m4858a() {
        return f2907a;
    }

    protected static void m4859a(BitMatrix bitMatrix, float[] fArr) {
        int i;
        int c = bitMatrix.m4840c();
        int d = bitMatrix.m4842d();
        Object obj = 1;
        for (i = 0; i < fArr.length && r2 != null; i += 2) {
            int i2 = (int) fArr[i];
            int i3 = (int) fArr[i + 1];
            if (i2 < -1 || i2 > c || i3 < -1 || i3 > d) {
                throw NotFoundException.m4608a();
            }
            if (i2 == -1) {
                fArr[i] = 0.0f;
                obj = 1;
            } else if (i2 == c) {
                fArr[i] = (float) (c - 1);
                i2 = 1;
            } else {
                obj = null;
            }
            if (i3 == -1) {
                fArr[i + 1] = 0.0f;
                obj = 1;
            } else if (i3 == d) {
                fArr[i + 1] = (float) (d - 1);
                i2 = 1;
            }
        }
        Object obj2 = 1;
        for (i2 = fArr.length - 2; i2 >= 0 && r0 != null; i2 -= 2) {
            i = (int) fArr[i2];
            i3 = (int) fArr[i2 + 1];
            if (i < -1 || i > c || i3 < -1 || i3 > d) {
                throw NotFoundException.m4608a();
            }
            if (i == -1) {
                fArr[i2] = 0.0f;
                obj2 = 1;
            } else if (i == c) {
                fArr[i2] = (float) (c - 1);
                i = 1;
            } else {
                obj2 = null;
            }
            if (i3 == -1) {
                fArr[i2 + 1] = 0.0f;
                obj2 = 1;
            } else if (i3 == d) {
                fArr[i2 + 1] = (float) (d - 1);
                i = 1;
            }
        }
    }
}
