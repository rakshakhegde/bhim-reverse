package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.Binarizer;
import in.juspay.widget.qrscanner.com.google.zxing.LuminanceSource;
import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.h */
public class GlobalHistogramBinarizer extends Binarizer {
    private static final byte[] f2910a;
    private byte[] f2911b;
    private final int[] f2912c;

    static {
        f2910a = new byte[0];
    }

    public GlobalHistogramBinarizer(LuminanceSource luminanceSource) {
        super(luminanceSource);
        this.f2911b = f2910a;
        this.f2912c = new int[32];
    }

    public BitMatrix m4866b() {
        int i;
        LuminanceSource a = m4816a();
        int b = a.m4907b();
        int c = a.m4908c();
        BitMatrix bitMatrix = new BitMatrix(b, c);
        m4865a(b);
        int[] iArr = this.f2912c;
        for (i = 1; i < 5; i++) {
            int i2;
            byte[] a2 = a.m4906a((c * i) / 5, this.f2911b);
            int i3 = (b * 4) / 5;
            for (i2 = b / 5; i2 < i3; i2++) {
                int i4 = (a2[i2] & 255) >> 3;
                iArr[i4] = iArr[i4] + 1;
            }
        }
        int a3 = GlobalHistogramBinarizer.m4864a(iArr);
        byte[] a4 = a.m4905a();
        for (i = 0; i < c; i++) {
            int i5 = i * b;
            for (i2 = 0; i2 < b; i2++) {
                if ((a4[i5 + i2] & 255) < a3) {
                    bitMatrix.m4838b(i2, i);
                }
            }
        }
        return bitMatrix;
    }

    private void m4865a(int i) {
        if (this.f2911b.length < i) {
            this.f2911b = new byte[i];
        }
        for (int i2 = 0; i2 < 32; i2++) {
            this.f2912c[i2] = 0;
        }
    }

    private static int m4864a(int[] iArr) {
        int i;
        int i2 = 0;
        int length = iArr.length;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        for (i = 0; i < length; i++) {
            if (iArr[i] > i3) {
                i3 = iArr[i];
                i4 = i;
            }
            if (iArr[i] > i5) {
                i5 = iArr[i];
            }
        }
        i = 0;
        int i6 = 0;
        while (i2 < length) {
            i3 = i2 - i4;
            i3 *= iArr[i2] * i3;
            if (i3 > i) {
                i = i2;
            } else {
                i3 = i;
                i = i6;
            }
            i2++;
            i6 = i;
            i = i3;
        }
        if (i4 <= i6) {
            int i7 = i6;
            i6 = i4;
            i4 = i7;
        }
        if (i4 - i6 <= length / 16) {
            throw NotFoundException.m4608a();
        }
        length = i4 - 1;
        i = -1;
        i2 = i4 - 1;
        while (i2 > i6) {
            i3 = i2 - i6;
            i3 = ((i3 * i3) * (i4 - i2)) * (i5 - iArr[i2]);
            if (i3 > i) {
                i = i2;
            } else {
                i3 = i;
                i = length;
            }
            i2--;
            length = i;
            i = i3;
        }
        return length << 3;
    }
}
