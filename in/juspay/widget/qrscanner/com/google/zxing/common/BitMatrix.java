package in.juspay.widget.qrscanner.com.google.zxing.common;

import java.util.Arrays;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.b */
public final class BitMatrix implements Cloneable {
    private final int f2860a;
    private final int f2861b;
    private final int f2862c;
    private final int[] f2863d;

    public /* synthetic */ Object clone() {
        return m4843e();
    }

    public BitMatrix(int i) {
        this(i, i);
    }

    public BitMatrix(int i, int i2) {
        if (i < 1 || i2 < 1) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.f2860a = i;
        this.f2861b = i2;
        this.f2862c = (i + 31) / 32;
        this.f2863d = new int[(this.f2862c * i2)];
    }

    private BitMatrix(int i, int i2, int i3, int[] iArr) {
        this.f2860a = i;
        this.f2861b = i2;
        this.f2862c = i3;
        this.f2863d = iArr;
    }

    public boolean m4836a(int i, int i2) {
        return ((this.f2863d[(this.f2862c * i2) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    public void m4838b(int i, int i2) {
        int i3 = (this.f2862c * i2) + (i / 32);
        int[] iArr = this.f2863d;
        iArr[i3] = iArr[i3] | (1 << (i & 31));
    }

    public void m4841c(int i, int i2) {
        int i3 = (this.f2862c * i2) + (i / 32);
        int[] iArr = this.f2863d;
        iArr[i3] = iArr[i3] ^ (1 << (i & 31));
    }

    public void m4835a(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 < 1 || i3 < 1) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i + i3;
            int i6 = i2 + i4;
            if (i6 > this.f2861b || i5 > this.f2860a) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = i2 * this.f2862c;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.f2863d;
                    int i9 = (i8 / 32) + i7;
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    public int[] m4837a() {
        int i = 0;
        while (i < this.f2863d.length && this.f2863d[i] == 0) {
            i++;
        }
        if (i == this.f2863d.length) {
            return null;
        }
        int i2 = i / this.f2862c;
        int i3 = (i % this.f2862c) * 32;
        int i4 = this.f2863d[i];
        i = 0;
        while ((i4 << (31 - i)) == 0) {
            i++;
        }
        i3 += i;
        return new int[]{i3, i2};
    }

    public int[] m4839b() {
        int length = this.f2863d.length - 1;
        while (length >= 0 && this.f2863d[length] == 0) {
            length--;
        }
        if (length < 0) {
            return null;
        }
        int i = length / this.f2862c;
        int i2 = (length % this.f2862c) * 32;
        int i3 = this.f2863d[length];
        length = 31;
        while ((i3 >>> length) == 0) {
            length--;
        }
        i2 += length;
        return new int[]{i2, i};
    }

    public int m4840c() {
        return this.f2860a;
    }

    public int m4842d() {
        return this.f2861b;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitMatrix)) {
            return false;
        }
        BitMatrix bitMatrix = (BitMatrix) obj;
        if (this.f2860a == bitMatrix.f2860a && this.f2861b == bitMatrix.f2861b && this.f2862c == bitMatrix.f2862c && Arrays.equals(this.f2863d, bitMatrix.f2863d)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.f2860a * 31) + this.f2860a) * 31) + this.f2861b) * 31) + this.f2862c) * 31) + Arrays.hashCode(this.f2863d);
    }

    public String toString() {
        return m4834a("X ", "  ");
    }

    public String m4834a(String str, String str2) {
        return m4833a(str, str2, "\n");
    }

    private String m4833a(String str, String str2, String str3) {
        StringBuilder stringBuilder = new StringBuilder(this.f2861b * (this.f2860a + 1));
        for (int i = 0; i < this.f2861b; i++) {
            for (int i2 = 0; i2 < this.f2860a; i2++) {
                String str4;
                if (m4836a(i2, i)) {
                    str4 = str;
                } else {
                    str4 = str2;
                }
                stringBuilder.append(str4);
            }
            stringBuilder.append(str3);
        }
        return stringBuilder.toString();
    }

    public BitMatrix m4843e() {
        return new BitMatrix(this.f2860a, this.f2861b, this.f2862c, (int[]) this.f2863d.clone());
    }
}
