package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.f */
public final class DefaultGridSampler extends GridSampler {
    public BitMatrix m4861a(BitMatrix bitMatrix, int i, int i2, PerspectiveTransform perspectiveTransform) {
        if (i <= 0 || i2 <= 0) {
            throw NotFoundException.m4608a();
        }
        BitMatrix bitMatrix2 = new BitMatrix(i, i2);
        float[] fArr = new float[(i * 2)];
        for (int i3 = 0; i3 < i2; i3++) {
            int i4;
            int length = fArr.length;
            float f = ((float) i3) + 0.5f;
            for (i4 = 0; i4 < length; i4 += 2) {
                fArr[i4] = ((float) (i4 / 2)) + 0.5f;
                fArr[i4 + 1] = f;
            }
            perspectiveTransform.m4877a(fArr);
            GridSampler.m4859a(bitMatrix, fArr);
            i4 = 0;
            while (i4 < length) {
                try {
                    if (bitMatrix.m4836a((int) fArr[i4], (int) fArr[i4 + 1])) {
                        bitMatrix2.m4838b(i4 / 2, i3);
                    }
                    i4 += 2;
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw NotFoundException.m4608a();
                }
            }
        }
        return bitMatrix2;
    }
}
