package in.juspay.widget.qrscanner.com.google.zxing.common;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.k */
public final class PerspectiveTransform {
    private final float f2914a;
    private final float f2915b;
    private final float f2916c;
    private final float f2917d;
    private final float f2918e;
    private final float f2919f;
    private final float f2920g;
    private final float f2921h;
    private final float f2922i;

    private PerspectiveTransform(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        this.f2914a = f;
        this.f2915b = f4;
        this.f2916c = f7;
        this.f2917d = f2;
        this.f2918e = f5;
        this.f2919f = f8;
        this.f2920g = f3;
        this.f2921h = f6;
        this.f2922i = f9;
    }

    public static PerspectiveTransform m4873a(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) {
        return PerspectiveTransform.m4872a(f9, f10, f11, f12, f13, f14, f15, f16).m4876a(PerspectiveTransform.m4874b(f, f2, f3, f4, f5, f6, f7, f8));
    }

    public void m4877a(float[] fArr) {
        int length = fArr.length;
        float f = this.f2914a;
        float f2 = this.f2915b;
        float f3 = this.f2916c;
        float f4 = this.f2917d;
        float f5 = this.f2918e;
        float f6 = this.f2919f;
        float f7 = this.f2920g;
        float f8 = this.f2921h;
        float f9 = this.f2922i;
        for (int i = 0; i < length; i += 2) {
            float f10 = fArr[i];
            float f11 = fArr[i + 1];
            float f12 = ((f3 * f10) + (f6 * f11)) + f9;
            fArr[i] = (((f * f10) + (f4 * f11)) + f7) / f12;
            fArr[i + 1] = (((f10 * f2) + (f11 * f5)) + f8) / f12;
        }
    }

    public static PerspectiveTransform m4872a(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        float f9 = ((f - f3) + f5) - f7;
        float f10 = ((f2 - f4) + f6) - f8;
        if (f9 == 0.0f && f10 == 0.0f) {
            return new PerspectiveTransform(f3 - f, f5 - f3, f, f4 - f2, f6 - f4, f2, 0.0f, 0.0f, 1.0f);
        }
        float f11 = f3 - f5;
        float f12 = f7 - f5;
        float f13 = f4 - f6;
        float f14 = f8 - f6;
        float f15 = (f11 * f14) - (f12 * f13);
        float f16 = ((f14 * f9) - (f12 * f10)) / f15;
        float f17 = ((f10 * f11) - (f9 * f13)) / f15;
        return new PerspectiveTransform((f3 - f) + (f16 * f3), (f7 - f) + (f17 * f7), f, (f16 * f4) + (f4 - f2), (f17 * f8) + (f8 - f2), f2, f16, f17, 1.0f);
    }

    public static PerspectiveTransform m4874b(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        return PerspectiveTransform.m4872a(f, f2, f3, f4, f5, f6, f7, f8).m4875a();
    }

    PerspectiveTransform m4875a() {
        return new PerspectiveTransform((this.f2918e * this.f2922i) - (this.f2919f * this.f2921h), (this.f2919f * this.f2920g) - (this.f2917d * this.f2922i), (this.f2917d * this.f2921h) - (this.f2918e * this.f2920g), (this.f2916c * this.f2921h) - (this.f2915b * this.f2922i), (this.f2914a * this.f2922i) - (this.f2916c * this.f2920g), (this.f2915b * this.f2920g) - (this.f2914a * this.f2921h), (this.f2915b * this.f2919f) - (this.f2916c * this.f2918e), (this.f2916c * this.f2917d) - (this.f2914a * this.f2919f), (this.f2914a * this.f2918e) - (this.f2915b * this.f2917d));
    }

    PerspectiveTransform m4876a(PerspectiveTransform perspectiveTransform) {
        return new PerspectiveTransform(((this.f2914a * perspectiveTransform.f2914a) + (this.f2917d * perspectiveTransform.f2915b)) + (this.f2920g * perspectiveTransform.f2916c), ((this.f2914a * perspectiveTransform.f2917d) + (this.f2917d * perspectiveTransform.f2918e)) + (this.f2920g * perspectiveTransform.f2919f), ((this.f2914a * perspectiveTransform.f2920g) + (this.f2917d * perspectiveTransform.f2921h)) + (this.f2920g * perspectiveTransform.f2922i), ((this.f2915b * perspectiveTransform.f2914a) + (this.f2918e * perspectiveTransform.f2915b)) + (this.f2921h * perspectiveTransform.f2916c), ((this.f2915b * perspectiveTransform.f2917d) + (this.f2918e * perspectiveTransform.f2918e)) + (this.f2921h * perspectiveTransform.f2919f), ((this.f2915b * perspectiveTransform.f2920g) + (this.f2918e * perspectiveTransform.f2921h)) + (this.f2921h * perspectiveTransform.f2922i), ((this.f2916c * perspectiveTransform.f2914a) + (this.f2919f * perspectiveTransform.f2915b)) + (this.f2922i * perspectiveTransform.f2916c), ((this.f2916c * perspectiveTransform.f2917d) + (this.f2919f * perspectiveTransform.f2918e)) + (this.f2922i * perspectiveTransform.f2919f), ((this.f2916c * perspectiveTransform.f2920g) + (this.f2919f * perspectiveTransform.f2921h)) + (this.f2922i * perspectiveTransform.f2922i));
    }
}
