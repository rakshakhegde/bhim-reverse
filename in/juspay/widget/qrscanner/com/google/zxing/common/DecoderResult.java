package in.juspay.widget.qrscanner.com.google.zxing.common;

import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.e */
public final class DecoderResult {
    private final byte[] f2899a;
    private int f2900b;
    private final String f2901c;
    private final List<byte[]> f2902d;
    private final String f2903e;
    private Object f2904f;
    private final int f2905g;
    private final int f2906h;

    public DecoderResult(byte[] bArr, String str, List<byte[]> list, String str2, int i, int i2) {
        this.f2899a = bArr;
        this.f2900b = bArr == null ? 0 : bArr.length * 8;
        this.f2901c = str;
        this.f2902d = list;
        this.f2903e = str2;
        this.f2905g = i2;
        this.f2906h = i;
    }

    public byte[] m4850a() {
        return this.f2899a;
    }

    public String m4851b() {
        return this.f2901c;
    }

    public List<byte[]> m4852c() {
        return this.f2902d;
    }

    public String m4853d() {
        return this.f2903e;
    }

    public Object m4854e() {
        return this.f2904f;
    }

    public void m4849a(Object obj) {
        this.f2904f = obj;
    }

    public boolean m4855f() {
        return this.f2905g >= 0 && this.f2906h >= 0;
    }

    public int m4856g() {
        return this.f2905g;
    }

    public int m4857h() {
        return this.f2906h;
    }
}
