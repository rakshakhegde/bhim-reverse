package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

import java.util.ArrayList;
import java.util.List;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.d */
public final class ReedSolomonEncoder {
    private final GenericGF f2943a;
    private final List<GenericGFPoly> f2944b;

    public ReedSolomonEncoder(GenericGF genericGF) {
        this.f2943a = genericGF;
        this.f2944b = new ArrayList();
        this.f2944b.add(new GenericGFPoly(genericGF, new int[]{1}));
    }

    private GenericGFPoly m4903a(int i) {
        if (i >= this.f2944b.size()) {
            GenericGFPoly genericGFPoly = (GenericGFPoly) this.f2944b.get(this.f2944b.size() - 1);
            GenericGFPoly genericGFPoly2 = genericGFPoly;
            for (int size = this.f2944b.size(); size <= i; size++) {
                genericGFPoly2 = genericGFPoly2.m4895b(new GenericGFPoly(this.f2943a, new int[]{1, this.f2943a.m4880a((size - 1) + this.f2943a.m4888d())}));
                this.f2944b.add(genericGFPoly2);
            }
        }
        return (GenericGFPoly) this.f2944b.get(i);
    }

    public void m4904a(int[] iArr, int i) {
        if (i == 0) {
            throw new IllegalArgumentException("No error correction bytes");
        }
        int length = iArr.length - i;
        if (length <= 0) {
            throw new IllegalArgumentException("No data bytes provided");
        }
        GenericGFPoly a = m4903a(i);
        Object obj = new int[length];
        System.arraycopy(iArr, 0, obj, 0, length);
        obj = new GenericGFPoly(this.f2943a, obj).m4890a(i, 1).m4898c(a)[1].m4892a();
        int length2 = i - obj.length;
        for (int i2 = 0; i2 < length2; i2++) {
            iArr[length + i2] = 0;
        }
        System.arraycopy(obj, 0, iArr, length + length2, obj.length);
    }
}
