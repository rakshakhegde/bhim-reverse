package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.a */
public final class GenericGF {
    public static final GenericGF f2925a;
    public static final GenericGF f2926b;
    public static final GenericGF f2927c;
    public static final GenericGF f2928d;
    public static final GenericGF f2929e;
    public static final GenericGF f2930f;
    public static final GenericGF f2931g;
    public static final GenericGF f2932h;
    private final int[] f2933i;
    private final int[] f2934j;
    private final GenericGFPoly f2935k;
    private final GenericGFPoly f2936l;
    private final int f2937m;
    private final int f2938n;
    private final int f2939o;

    static {
        f2925a = new GenericGF(4201, CodedOutputStream.DEFAULT_BUFFER_SIZE, 1);
        f2926b = new GenericGF(1033, 1024, 1);
        f2927c = new GenericGF(67, 64, 1);
        f2928d = new GenericGF(19, 16, 1);
        f2929e = new GenericGF(285, 256, 0);
        f2930f = new GenericGF(301, 256, 1);
        f2931g = f2930f;
        f2932h = f2927c;
    }

    public GenericGF(int i, int i2, int i3) {
        this.f2938n = i;
        this.f2937m = i2;
        this.f2939o = i3;
        this.f2933i = new int[i2];
        this.f2934j = new int[i2];
        int i4 = 1;
        for (int i5 = 0; i5 < i2; i5++) {
            this.f2933i[i5] = i4;
            i4 *= 2;
            if (i4 >= i2) {
                i4 = (i4 ^ i) & (i2 - 1);
            }
        }
        for (i4 = 0; i4 < i2 - 1; i4++) {
            this.f2934j[this.f2933i[i4]] = i4;
        }
        this.f2935k = new GenericGFPoly(this, new int[]{0});
        this.f2936l = new GenericGFPoly(this, new int[]{1});
    }

    GenericGFPoly m4881a() {
        return this.f2935k;
    }

    GenericGFPoly m4884b() {
        return this.f2936l;
    }

    GenericGFPoly m4882a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f2935k;
        } else {
            int[] iArr = new int[(i + 1)];
            iArr[0] = i2;
            return new GenericGFPoly(this, iArr);
        }
    }

    static int m4879b(int i, int i2) {
        return i ^ i2;
    }

    int m4880a(int i) {
        return this.f2933i[i];
    }

    int m4883b(int i) {
        if (i != 0) {
            return this.f2934j[i];
        }
        throw new IllegalArgumentException();
    }

    int m4886c(int i) {
        if (i != 0) {
            return this.f2933i[(this.f2937m - this.f2934j[i]) - 1];
        }
        throw new ArithmeticException();
    }

    int m4887c(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        return this.f2933i[(this.f2934j[i] + this.f2934j[i2]) % (this.f2937m - 1)];
    }

    public int m4885c() {
        return this.f2937m;
    }

    public int m4888d() {
        return this.f2939o;
    }

    public String toString() {
        return "GF(0x" + Integer.toHexString(this.f2938n) + ',' + this.f2937m + ')';
    }
}
