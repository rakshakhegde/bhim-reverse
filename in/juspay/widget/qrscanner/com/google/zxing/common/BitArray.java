package in.juspay.widget.qrscanner.com.google.zxing.common;

import java.util.Arrays;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.common.a */
public final class BitArray implements Cloneable {
    private int[] f2858a;
    private int f2859b;

    public /* synthetic */ Object clone() {
        return m4832c();
    }

    public BitArray() {
        this.f2859b = 0;
        this.f2858a = new int[1];
    }

    BitArray(int[] iArr, int i) {
        this.f2858a = iArr;
        this.f2859b = i;
    }

    public int m4824a() {
        return this.f2859b;
    }

    public int m4830b() {
        return (this.f2859b + 7) / 8;
    }

    private void m4822b(int i) {
        if (i > this.f2858a.length * 32) {
            Object c = BitArray.m4823c(i);
            System.arraycopy(this.f2858a, 0, c, 0, this.f2858a.length);
            this.f2858a = c;
        }
    }

    public boolean m4829a(int i) {
        return (this.f2858a[i / 32] & (1 << (i & 31))) != 0;
    }

    public void m4828a(boolean z) {
        m4822b(this.f2859b + 1);
        if (z) {
            int[] iArr = this.f2858a;
            int i = this.f2859b / 32;
            iArr[i] = iArr[i] | (1 << (this.f2859b & 31));
        }
        this.f2859b++;
    }

    public void m4825a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        m4822b(this.f2859b + i2);
        while (i2 > 0) {
            m4828a(((i >> (i2 + -1)) & 1) == 1);
            i2--;
        }
    }

    public void m4827a(BitArray bitArray) {
        int i = bitArray.f2859b;
        m4822b(this.f2859b + i);
        for (int i2 = 0; i2 < i; i2++) {
            m4828a(bitArray.m4829a(i2));
        }
    }

    public void m4831b(BitArray bitArray) {
        if (this.f2859b != bitArray.f2859b) {
            throw new IllegalArgumentException("Sizes don't match");
        }
        for (int i = 0; i < this.f2858a.length; i++) {
            int[] iArr = this.f2858a;
            iArr[i] = iArr[i] ^ bitArray.f2858a[i];
        }
    }

    public void m4826a(int i, byte[] bArr, int i2, int i3) {
        int i4 = 0;
        int i5 = i;
        while (i4 < i3) {
            int i6 = i5;
            i5 = 0;
            for (int i7 = 0; i7 < 8; i7++) {
                if (m4829a(i6)) {
                    i5 |= 1 << (7 - i7);
                }
                i6++;
            }
            bArr[i2 + i4] = (byte) i5;
            i4++;
            i5 = i6;
        }
    }

    private static int[] m4823c(int i) {
        return new int[((i + 31) / 32)];
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitArray)) {
            return false;
        }
        BitArray bitArray = (BitArray) obj;
        if (this.f2859b == bitArray.f2859b && Arrays.equals(this.f2858a, bitArray.f2858a)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.f2859b * 31) + Arrays.hashCode(this.f2858a);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(this.f2859b);
        for (int i = 0; i < this.f2859b; i++) {
            if ((i & 7) == 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(m4829a(i) ? 'X' : '.');
        }
        return stringBuilder.toString();
    }

    public BitArray m4832c() {
        return new BitArray((int[]) this.f2858a.clone(), this.f2859b);
    }
}
