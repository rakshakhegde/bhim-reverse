package in.juspay.widget.qrscanner.com.google.zxing;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.f */
public abstract class LuminanceSource {
    private final int f2965a;
    private final int f2966b;

    public abstract byte[] m4905a();

    public abstract byte[] m4906a(int i, byte[] bArr);

    protected LuminanceSource(int i, int i2) {
        this.f2965a = i;
        this.f2966b = i2;
    }

    public final int m4907b() {
        return this.f2965a;
    }

    public final int m4908c() {
        return this.f2966b;
    }

    public final String toString() {
        byte[] bArr = new byte[this.f2965a];
        StringBuilder stringBuilder = new StringBuilder(this.f2966b * (this.f2965a + 1));
        byte[] bArr2 = bArr;
        for (int i = 0; i < this.f2966b; i++) {
            bArr2 = m4906a(i, bArr2);
            for (int i2 = 0; i2 < this.f2965a; i2++) {
                char c;
                int i3 = bArr2[i2] & 255;
                if (i3 < 64) {
                    c = '#';
                } else if (i3 < 128) {
                    c = '+';
                } else if (i3 < 192) {
                    c = '.';
                } else {
                    c = ' ';
                }
                stringBuilder.append(c);
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }
}
