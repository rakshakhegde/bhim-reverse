package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.EnumMap;
import java.util.Map;

/* renamed from: in.juspay.widget.qrscanner.com.google.zxing.j */
public final class Result {
    private final String f2974a;
    private final byte[] f2975b;
    private final int f2976c;
    private ResultPoint[] f2977d;
    private final BarcodeFormat f2978e;
    private Map<ResultMetadataType, Object> f2979f;
    private final long f2980g;

    public Result(String str, byte[] bArr, ResultPoint[] resultPointArr, BarcodeFormat barcodeFormat) {
        this(str, bArr, resultPointArr, barcodeFormat, System.currentTimeMillis());
    }

    public Result(String str, byte[] bArr, ResultPoint[] resultPointArr, BarcodeFormat barcodeFormat, long j) {
        this(str, bArr, bArr == null ? 0 : bArr.length * 8, resultPointArr, barcodeFormat, j);
    }

    public Result(String str, byte[] bArr, int i, ResultPoint[] resultPointArr, BarcodeFormat barcodeFormat, long j) {
        this.f2974a = str;
        this.f2975b = bArr;
        this.f2976c = i;
        this.f2977d = resultPointArr;
        this.f2978e = barcodeFormat;
        this.f2979f = null;
        this.f2980g = j;
    }

    public String m4918a() {
        return this.f2974a;
    }

    public void m4919a(ResultMetadataType resultMetadataType, Object obj) {
        if (this.f2979f == null) {
            this.f2979f = new EnumMap(ResultMetadataType.class);
        }
        this.f2979f.put(resultMetadataType, obj);
    }

    public String toString() {
        return this.f2974a;
    }
}
