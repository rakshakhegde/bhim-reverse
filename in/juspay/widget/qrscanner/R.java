package in.juspay.widget.qrscanner;

/* renamed from: in.juspay.widget.qrscanner.a */
public final class R {

    /* renamed from: in.juspay.widget.qrscanner.a.a */
    public static final class R {
        public static final int colorAccent = 2131492882;
        public static final int colorPrimary = 2131492883;
        public static final int colorPrimaryDark = 2131492884;
        public static final int zxing_custom_possible_result_points = 2131492953;
        public static final int zxing_custom_result_view = 2131492954;
        public static final int zxing_custom_viewfinder_laser = 2131492955;
        public static final int zxing_custom_viewfinder_mask = 2131492956;
        public static final int zxing_possible_result_points = 2131492957;
        public static final int zxing_result_view = 2131492958;
        public static final int zxing_status_text = 2131492959;
        public static final int zxing_transparent = 2131492960;
        public static final int zxing_viewfinder_laser = 2131492961;
        public static final int zxing_viewfinder_mask = 2131492962;
    }

    /* renamed from: in.juspay.widget.qrscanner.a.b */
    public static final class R {
        public static final int centerCrop = 2131558451;
        public static final int fitCenter = 2131558452;
        public static final int fitXY = 2131558453;
        public static final int zxing_back_button = 2131558409;
        public static final int zxing_barcode_scanner = 2131558551;
        public static final int zxing_barcode_surface = 2131558548;
        public static final int zxing_camera_error = 2131558410;
        public static final int zxing_decode = 2131558411;
        public static final int zxing_decode_failed = 2131558412;
        public static final int zxing_decode_succeeded = 2131558413;
        public static final int zxing_possible_result_points = 2131558414;
        public static final int zxing_preview_failed = 2131558415;
        public static final int zxing_prewiew_size_ready = 2131558416;
        public static final int zxing_status_view = 2131558550;
        public static final int zxing_viewfinder_view = 2131558549;
    }

    /* renamed from: in.juspay.widget.qrscanner.a.c */
    public static final class R {
        public static final int zxing_barcode_scanner = 2130968621;
        public static final int zxing_capture = 2130968622;
    }

    /* renamed from: in.juspay.widget.qrscanner.a.d */
    public static final class R {
        public static final int zxing_beep = 2131099648;
    }

    /* renamed from: in.juspay.widget.qrscanner.a.e */
    public static final class R {
        public static final int app_name = 2131165239;
        public static final int zxing_app_name = 2131165271;
        public static final int zxing_button_ok = 2131165272;
        public static final int zxing_msg_camera_framework_bug = 2131165273;
        public static final int zxing_msg_default_status = 2131165274;
    }

    /* renamed from: in.juspay.widget.qrscanner.a.f */
    public static final class R {
        public static final int[] zxing_camera_preview;
        public static final int zxing_camera_preview_zxing_framing_rect_height = 1;
        public static final int zxing_camera_preview_zxing_framing_rect_width = 0;
        public static final int zxing_camera_preview_zxing_preview_scaling_strategy = 3;
        public static final int zxing_camera_preview_zxing_use_texture_view = 2;
        public static final int[] zxing_finder;
        public static final int zxing_finder_zxing_possible_result_points = 0;
        public static final int zxing_finder_zxing_result_view = 1;
        public static final int zxing_finder_zxing_viewfinder_laser = 2;
        public static final int zxing_finder_zxing_viewfinder_mask = 3;
        public static final int[] zxing_view;
        public static final int zxing_view_zxing_scanner_layout = 0;

        static {
            zxing_camera_preview = new int[]{2130772204, 2130772205, 2130772206, 2130772207};
            zxing_finder = new int[]{2130772208, 2130772209, 2130772210, 2130772211};
            int[] iArr = new int[zxing_finder_zxing_result_view];
            iArr[zxing_finder_zxing_possible_result_points] = 2130772212;
            zxing_view = iArr;
        }
    }
}
