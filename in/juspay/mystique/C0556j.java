package in.juspay.mystique;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.crashlytics.android.core.BuildConfig;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: in.juspay.mystique.j */
public class C0556j {
    private C0543g f2603a;
    private Activity f2604b;
    private View f2605c;
    private View f2606d;
    private ViewGroup f2607e;
    private C0531e f2608f;
    private C0530d f2609g;
    private C0525c f2610h;

    C0556j(Activity activity, C0530d c0530d) {
        this.f2609g = c0530d;
        System.currentTimeMillis();
        this.f2604b = activity;
        this.f2608f = c0530d.m4440c();
        this.f2610h = C0530d.m4432b();
        this.f2603a = new C0543g(this.f2604b, this.f2610h, this.f2608f, c0530d);
    }

    public void m4498a(String str, ViewGroup viewGroup) {
        this.f2607e = viewGroup;
        this.f2605c = m4495a(new JSONObject(str));
        if (!(this.f2606d == null || this.f2606d == this.f2605c)) {
            m4494b(this.f2606d);
        }
        m4493a(this.f2605c);
        this.f2606d = this.f2605c;
    }

    public void m4500a(String str, JSONObject jSONObject, int i, boolean z) {
        int identifier = this.f2604b.getResources().getIdentifier(str, "id", this.f2604b.getPackageName());
        if (i >= 0) {
            ViewGroup viewGroup = (ViewGroup) this.f2604b.findViewById(identifier);
            if (z) {
                viewGroup.removeAllViews();
            }
            View a = m4495a(jSONObject);
            if (a != null) {
                viewGroup.addView(a, i);
                return;
            } else {
                this.f2608f.m4443a("ERROR", " isNull : fn__addViewToParent - child null " + m4497a());
                return;
            }
        }
        if (jSONObject.has("props")) {
            m4499a(jSONObject.getString(CLConstants.FIELD_TYPE), jSONObject.getJSONObject("props"));
        }
        this.f2608f.m4443a("ERROR", " isNull : fn__addViewToParent - negative index " + m4497a());
    }

    public void m4499a(String str, JSONObject jSONObject) {
        this.f2603a.m4481b(str);
        if (jSONObject.has("node_id")) {
            this.f2603a.m4479a(jSONObject.getString("node_id"));
        }
        if (jSONObject.has("__filename")) {
            this.f2603a.m4482c(jSONObject.getString("__filename"));
        }
    }

    public View m4495a(JSONObject jSONObject) {
        String string = jSONObject.getString(CLConstants.FIELD_TYPE);
        JSONObject jSONObject2 = jSONObject.getJSONObject("props");
        if (jSONObject.has("props")) {
            m4499a(string, jSONObject2);
        }
        Class cls = Class.forName(string);
        Object newInstance = cls.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{this.f2604b});
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            this.f2603a.m4480a((String) keys.next(), jSONObject2, newInstance);
        }
        JSONArray jSONArray = jSONObject.getJSONArray("children");
        if (!(jSONArray == null || jSONArray.length() == 0)) {
            for (int i = 0; i < jSONArray.length(); i++) {
                if (m4495a(jSONArray.getJSONObject(i)) != null) {
                    cls.getMethod("addView", new Class[]{View.class}).invoke(newInstance, new Object[]{r5});
                }
            }
        }
        return (View) newInstance;
    }

    private View m4493a(View view) {
        if (view != null) {
            this.f2607e.addView(view);
        } else {
            this.f2608f.m4443a("ERROR", " isNull : fn__Render -  instance null " + m4497a());
        }
        return this.f2607e;
    }

    private void m4494b(View view) {
        this.f2607e.removeViewAt(this.f2607e.indexOfChild(view));
    }

    public String m4497a() {
        return this.f2603a.m4478a();
    }

    public Object m4496a(Object obj, String str, String str2, String str3) {
        this.f2603a.m4481b("modifyDom");
        this.f2603a.m4479a(BuildConfig.FLAVOR);
        this.f2603a.m4482c("ln: " + str2 + " " + str3);
        return this.f2603a.m4477a(obj, str);
    }
}
