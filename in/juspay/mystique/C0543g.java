package in.juspay.mystique;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import com.crashlytics.android.core.BuildConfig;
import in.juspay.mystique.g.AnonymousClass10;
import in.juspay.mystique.g.AnonymousClass11;
import in.juspay.mystique.g.AnonymousClass12;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.juspay.mystique.g */
public class C0543g {
    static HashMap<C0542a, Method> f2531a;
    private static float f2532b;
    private static float f2533c;
    private static float f2534d;
    private static float f2535e;
    private static HashMap<String, Object> f2536h;
    private static final String f2537i;
    private static final Map<Class, Class> f2538v;
    private Activity f2539f;
    private C0531e f2540g;
    private String f2541j;
    private String f2542k;
    private String f2543l;
    private String f2544m;
    private C0530d f2545n;
    private String f2546o;
    private String f2547p;
    private Pattern f2548q;
    private String f2549r;
    private String f2550s;
    private String f2551t;
    private Pattern f2552u;
    private C0525c f2553w;

    /* renamed from: in.juspay.mystique.g.10 */
    class AnonymousClass10 implements OnFocusChangeListener {
        final /* synthetic */ String f2504a;
        final /* synthetic */ C0543g f2505b;

        AnonymousClass10(C0543g c0543g, String str) {
            this.f2505b = c0543g;
            this.f2504a = str;
        }

        public void onFocusChange(View view, boolean z) {
            this.f2505b.f2545n.m4438a("window.callUICallback('" + this.f2504a + "','" + z + "');");
        }
    }

    /* renamed from: in.juspay.mystique.g.11 */
    class AnonymousClass11 implements OnTouchListener {
        final /* synthetic */ String f2506a;
        final /* synthetic */ C0543g f2507b;

        AnonymousClass11(C0543g c0543g, String str) {
            this.f2507b = c0543g;
            this.f2506a = str;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            this.f2507b.f2545n.m4438a("window.callUICallback('" + this.f2506a + "','" + motionEvent.getX() + "','" + motionEvent.getY() + "');");
            return false;
        }
    }

    /* renamed from: in.juspay.mystique.g.12 */
    class AnonymousClass12 implements OnDateChangeListener {
        final /* synthetic */ String f2508a;
        final /* synthetic */ C0543g f2509b;

        AnonymousClass12(C0543g c0543g, String str) {
            this.f2509b = c0543g;
            this.f2508a = str;
        }

        public void onSelectedDayChange(CalendarView calendarView, int i, int i2, int i3) {
            this.f2509b.f2545n.m4438a("window.callUICallback('" + this.f2508a + "','" + i + "','" + i2 + "','" + i3 + "');");
        }
    }

    /* renamed from: in.juspay.mystique.g.1 */
    class C05331 implements InputFilter {
        final /* synthetic */ String f2510a;
        final /* synthetic */ C0543g f2511b;

        C05331(C0543g c0543g, String str) {
            this.f2511b = c0543g;
            this.f2510a = str;
        }

        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            while (i < i2) {
                if (!Pattern.compile(this.f2510a).matcher(String.valueOf(charSequence.charAt(i))).matches()) {
                    return BuildConfig.FLAVOR;
                }
                i++;
            }
            return null;
        }
    }

    /* renamed from: in.juspay.mystique.g.2 */
    class C05342 implements OnTouchListener {
        final /* synthetic */ String f2512a;
        final /* synthetic */ C0543g f2513b;

        C05342(C0543g c0543g, String str) {
            this.f2513b = c0543g;
            this.f2512a = str;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            String str = "0";
            switch (motionEvent.getAction()) {
                case R.View_android_theme /*0*/:
                    C0543g.f2532b = motionEvent.getX();
                    C0543g.f2534d = motionEvent.getY();
                    break;
                case R.View_android_focusable /*1*/:
                    C0543g.f2533c = motionEvent.getX();
                    C0543g.f2535e = motionEvent.getY();
                    float toDegrees = (float) Math.toDegrees(Math.atan2((double) (C0543g.f2535e - C0543g.f2534d), (double) (C0543g.f2533c - C0543g.f2532b)));
                    if (toDegrees < 0.0f) {
                        toDegrees += 360.0f;
                    }
                    if ((toDegrees >= 45.0f && toDegrees <= 135.0f) || (toDegrees >= 225.0f && toDegrees <= 315.0f)) {
                        if (C0543g.f2535e - C0543g.f2534d <= 100.0f) {
                            if (C0543g.f2534d - C0543g.f2535e > 100.0f) {
                                str = "-2";
                                break;
                            }
                        }
                        str = "2";
                        break;
                    } else if (C0543g.f2533c - C0543g.f2532b <= 100.0f) {
                        if (C0543g.f2532b - C0543g.f2533c > 100.0f) {
                            str = "-1";
                            break;
                        }
                    } else {
                        str = "1";
                        break;
                    }
                    break;
            }
            this.f2513b.f2545n.m4438a("window.callUICallback('" + this.f2512a + "','" + str + "');");
            return true;
        }
    }

    /* renamed from: in.juspay.mystique.g.3 */
    class C05353 implements OnMenuItemClickListener {
        final /* synthetic */ String f2514a;
        final /* synthetic */ C0543g f2515b;

        C05353(C0543g c0543g, String str) {
            this.f2515b = c0543g;
            this.f2514a = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            this.f2515b.f2545n.m4438a("window.callUICallback('" + this.f2514a + "', '" + menuItem.getItemId() + "');");
            return true;
        }
    }

    /* renamed from: in.juspay.mystique.g.4 */
    class C05364 implements OnClickListener {
        final /* synthetic */ PopupMenu f2516a;
        final /* synthetic */ C0543g f2517b;

        C05364(C0543g c0543g, PopupMenu popupMenu) {
            this.f2517b = c0543g;
            this.f2516a = popupMenu;
        }

        public void onClick(View view) {
            if (VERSION.SDK_INT >= 11) {
                this.f2516a.show();
            }
        }
    }

    /* renamed from: in.juspay.mystique.g.5 */
    class C05375 implements OnKeyListener {
        final /* synthetic */ String f2518a;
        final /* synthetic */ C0543g f2519b;

        C05375(C0543g c0543g, String str) {
            this.f2519b = c0543g;
            this.f2518a = str;
        }

        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            this.f2519b.f2545n.m4438a("window.callUICallback('" + this.f2518a + "','" + i + "');");
            return false;
        }
    }

    /* renamed from: in.juspay.mystique.g.6 */
    class C05386 implements OnLongClickListener {
        final /* synthetic */ String f2520a;
        final /* synthetic */ C0543g f2521b;

        C05386(C0543g c0543g, String str) {
            this.f2521b = c0543g;
            this.f2520a = str;
        }

        public boolean onLongClick(View view) {
            this.f2521b.f2545n.m4438a("window.callUICallback('" + this.f2520a + "');");
            return false;
        }
    }

    /* renamed from: in.juspay.mystique.g.7 */
    class C05397 implements OnClickListener {
        final /* synthetic */ String f2522a;
        final /* synthetic */ C0543g f2523b;

        C05397(C0543g c0543g, String str) {
            this.f2523b = c0543g;
            this.f2522a = str;
        }

        public void onClick(View view) {
            this.f2523b.f2545n.m4438a("window.callUICallback('" + this.f2522a + "');");
        }
    }

    /* renamed from: in.juspay.mystique.g.8 */
    class C05408 implements OnItemSelectedListener {
        final /* synthetic */ String f2524a;
        final /* synthetic */ C0543g f2525b;

        C05408(C0543g c0543g, String str) {
            this.f2525b = c0543g;
            this.f2524a = str;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            this.f2525b.f2545n.m4438a("window.callUICallback('" + this.f2524a + "', '" + adapterView.getId() + "' ,'" + view.getId() + "','" + i + "', '" + j + "');");
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: in.juspay.mystique.g.9 */
    class C05419 implements TextWatcher {
        final /* synthetic */ String f2526a;
        final /* synthetic */ C0543g f2527b;

        C05419(C0543g c0543g, String str) {
            this.f2527b = c0543g;
            this.f2526a = str;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.f2527b.f2545n.m4438a("window.callUICallback('" + this.f2526a + "', '" + charSequence + "');");
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    /* renamed from: in.juspay.mystique.g.a */
    static class C0542a {
        Class f2528a;
        String f2529b;
        Class[] f2530c;

        public C0542a(Class cls, String str, Class[] clsArr) {
            this.f2528a = cls;
            this.f2529b = str;
            this.f2530c = clsArr;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C0542a c0542a = (C0542a) obj;
            if (this.f2528a.equals(c0542a.f2528a) && this.f2529b.equals(c0542a.f2529b)) {
                return Arrays.equals(this.f2530c, c0542a.f2530c);
            }
            return false;
        }

        public int hashCode() {
            return (this.f2530c != null ? Arrays.hashCode(this.f2530c) : 0) + (((this.f2528a.hashCode() * 31) + this.f2529b.hashCode()) * 31);
        }
    }

    static {
        f2536h = new HashMap();
        f2537i = C0556j.class.getName();
        f2538v = new Hashtable();
        f2538v.put(Boolean.class, Boolean.TYPE);
        f2538v.put(Character.class, Character.TYPE);
        f2538v.put(Byte.class, Byte.TYPE);
        f2538v.put(Short.class, Short.TYPE);
        f2538v.put(Integer.class, Integer.TYPE);
        f2538v.put(Long.class, Long.TYPE);
        f2538v.put(Float.class, Float.TYPE);
        f2538v.put(Double.class, Double.TYPE);
        f2538v.put(Void.class, Void.TYPE);
        f2531a = new HashMap();
    }

    C0543g(Activity activity, C0525c c0525c, C0531e c0531e, C0530d c0530d) {
        this.f2541j = "-1";
        this.f2542k = BuildConfig.FLAVOR;
        this.f2543l = BuildConfig.FLAVOR;
        this.f2544m = BuildConfig.FLAVOR;
        this.f2546o = ":";
        this.f2547p = ",";
        this.f2548q = Pattern.compile("(?<!\\\\)" + Pattern.quote(","));
        this.f2549r = "->";
        this.f2550s = EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
        this.f2551t = "=";
        this.f2552u = Pattern.compile("(?<!\\\\)" + Pattern.quote(";"));
        this.f2545n = c0530d;
        this.f2539f = activity;
        this.f2553w = c0525c;
        this.f2540g = c0531e;
        f2536h.put("duiObj", c0530d);
    }

    public static boolean m4458a(Class<?> cls) {
        return f2538v.containsKey(cls);
    }

    public String m4478a() {
        return this.f2541j + " - " + this.f2543l + "-" + this.f2544m + " - " + this.f2542k;
    }

    private Object[] m4468d(String str) {
        int i = 0;
        ArrayList arrayList = new ArrayList();
        if (m4451a(str, this.f2547p, 0) == -1) {
            arrayList.add(m4473g(str));
        } else {
            String[] split = this.f2548q.split(str);
            int length = split.length;
            while (i < length) {
                arrayList.add(m4473g(split[i]));
                i++;
            }
        }
        return arrayList.toArray();
    }

    private Class[] m4471e(String str) {
        int i = 0;
        if (str == null) {
            return null;
        }
        if (m4451a(str, this.f2547p, 0) != -1) {
            String[] split = this.f2548q.split(str);
            if (split.length > 1) {
                Class[] clsArr = new Class[split.length];
                while (i < split.length) {
                    clsArr[i] = (Class) m4472f(split[i]);
                    i++;
                }
                return clsArr;
            }
        }
        return new Class[]{(Class) m4472f(str)};
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <Any> Any m4472f(java.lang.String r7) {
        /*
        r6 = this;
        r2 = 1;
        r0 = 0;
        if (r7 == 0) goto L_0x0131;
    L_0x0004:
        r1 = r6.f2550s;
        r3 = r6.m4460a(r7, r1);
        r4 = r3[r0];
        r1 = -1;
        r5 = r4.hashCode();
        switch(r5) {
            case -891988091: goto L_0x0075;
            case 98: goto L_0x0024;
            case 102: goto L_0x0038;
            case 105: goto L_0x001b;
            case 108: goto L_0x0056;
            case 115: goto L_0x008b;
            case 3184: goto L_0x002e;
            case 3212: goto L_0x0042;
            case 3677: goto L_0x004c;
            case 98855: goto L_0x0080;
            case 99674: goto L_0x006a;
            case 102230: goto L_0x0060;
            case 3392903: goto L_0x0096;
            default: goto L_0x0014;
        };
    L_0x0014:
        r0 = r1;
    L_0x0015:
        switch(r0) {
            case 0: goto L_0x00a2;
            case 1: goto L_0x00a6;
            case 2: goto L_0x00aa;
            case 3: goto L_0x00ae;
            case 4: goto L_0x00b2;
            case 5: goto L_0x00b6;
            case 6: goto L_0x00ba;
            case 7: goto L_0x00be;
            case 8: goto L_0x011e;
            case 9: goto L_0x0122;
            case 10: goto L_0x0126;
            case 11: goto L_0x012a;
            case 12: goto L_0x012e;
            default: goto L_0x0018;
        };
    L_0x0018:
        r0 = java.lang.String.class;
    L_0x001a:
        return r0;
    L_0x001b:
        r5 = "i";
        r4 = r4.equals(r5);
        if (r4 == 0) goto L_0x0014;
    L_0x0023:
        goto L_0x0015;
    L_0x0024:
        r0 = "b";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x002c:
        r0 = r2;
        goto L_0x0015;
    L_0x002e:
        r0 = "cs";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0036:
        r0 = 2;
        goto L_0x0015;
    L_0x0038:
        r0 = "f";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0040:
        r0 = 3;
        goto L_0x0015;
    L_0x0042:
        r0 = "dp";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x004a:
        r0 = 4;
        goto L_0x0015;
    L_0x004c:
        r0 = "sp";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0054:
        r0 = 5;
        goto L_0x0015;
    L_0x0056:
        r0 = "l";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x005e:
        r0 = 6;
        goto L_0x0015;
    L_0x0060:
        r0 = "get";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0068:
        r0 = 7;
        goto L_0x0015;
    L_0x006a:
        r0 = "dpf";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0072:
        r0 = 8;
        goto L_0x0015;
    L_0x0075:
        r0 = "strget";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x007d:
        r0 = 9;
        goto L_0x0015;
    L_0x0080:
        r0 = "ctx";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0088:
        r0 = 10;
        goto L_0x0015;
    L_0x008b:
        r0 = "s";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x0093:
        r0 = 11;
        goto L_0x0015;
    L_0x0096:
        r0 = "null";
        r0 = r4.equals(r0);
        if (r0 == 0) goto L_0x0014;
    L_0x009e:
        r0 = 12;
        goto L_0x0015;
    L_0x00a2:
        r0 = java.lang.Integer.TYPE;
        goto L_0x001a;
    L_0x00a6:
        r0 = java.lang.Boolean.TYPE;
        goto L_0x001a;
    L_0x00aa:
        r0 = java.lang.CharSequence.class;
        goto L_0x001a;
    L_0x00ae:
        r0 = java.lang.Float.TYPE;
        goto L_0x001a;
    L_0x00b2:
        r0 = java.lang.Integer.TYPE;
        goto L_0x001a;
    L_0x00b6:
        r0 = java.lang.Float.class;
        goto L_0x001a;
    L_0x00ba:
        r0 = java.lang.Long.TYPE;
        goto L_0x001a;
    L_0x00be:
        r0 = f2536h;
        r1 = r3[r2];
        r0 = r0.get(r1);
        if (r0 == 0) goto L_0x00ce;
    L_0x00c8:
        r0 = r0.getClass();
        goto L_0x001a;
    L_0x00ce:
        r0 = r6.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__getClassType - ";
        r2 = r2.append(r3);
        r2 = r2.append(r7);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r6.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r6.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__getClassType - ";
        r2 = r2.append(r3);
        r2 = r2.append(r7);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r6.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
    L_0x011e:
        r0 = java.lang.Float.TYPE;
        goto L_0x001a;
    L_0x0122:
        r0 = java.lang.CharSequence.class;
        goto L_0x001a;
    L_0x0126:
        r0 = android.content.Context.class;
        goto L_0x001a;
    L_0x012a:
        r0 = java.lang.String.class;
        goto L_0x001a;
    L_0x012e:
        r0 = 0;
        goto L_0x001a;
    L_0x0131:
        r0 = r6.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__getClassType -  toConvert ";
        r2 = r2.append(r3);
        r3 = r6.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r6.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__getClassType -  toConvert ";
        r2 = r2.append(r3);
        r3 = r6.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0018;
        */
        throw new UnsupportedOperationException("Method not decompiled: in.juspay.mystique.g.f(java.lang.String):Any");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <Any> Any m4473g(java.lang.String r10) {
        /*
        r9 = this;
        r1 = 0;
        r4 = 1;
        r2 = 0;
        r8 = 92;
        r3 = -1;
        if (r10 == 0) goto L_0x01d3;
    L_0x0008:
        r0 = r9.f2553w;
        r5 = "getValue!";
        r0.m4425a(r5, r10);
        r0 = r9.f2550s;
        r0 = r9.m4460a(r10, r0);
        r5 = r0[r2];
        r0 = r0[r4];
        r6 = r0.indexOf(r8);
        if (r6 == r3) goto L_0x002f;
    L_0x001f:
        r6 = ";";
        r6 = r0.indexOf(r6);
        if (r6 == r3) goto L_0x002f;
    L_0x0027:
        r6 = "\\\\;";
        r7 = ";";
        r0 = r0.replace(r6, r7);
    L_0x002f:
        r6 = r0.indexOf(r8);
        if (r6 == r3) goto L_0x0045;
    L_0x0035:
        r6 = "_";
        r6 = r0.indexOf(r6);
        if (r6 == r3) goto L_0x0045;
    L_0x003d:
        r6 = "\\\\_";
        r7 = "_";
        r0 = r0.replace(r6, r7);
    L_0x0045:
        r6 = r0.indexOf(r8);
        if (r6 == r3) goto L_0x005b;
    L_0x004b:
        r6 = ":";
        r6 = r0.indexOf(r6);
        if (r6 == r3) goto L_0x005b;
    L_0x0053:
        r6 = "\\\\:";
        r7 = ":";
        r0 = r0.replace(r6, r7);
    L_0x005b:
        r6 = r0.indexOf(r8);
        if (r6 == r3) goto L_0x0071;
    L_0x0061:
        r6 = ",";
        r6 = r0.indexOf(r6);
        if (r6 == r3) goto L_0x0071;
    L_0x0069:
        r6 = "\\\\,";
        r7 = ",";
        r0 = r0.replace(r6, r7);
    L_0x0071:
        r6 = r0.indexOf(r8);
        if (r6 == r3) goto L_0x0087;
    L_0x0077:
        r6 = "=";
        r6 = r0.indexOf(r6);
        if (r6 == r3) goto L_0x0087;
    L_0x007f:
        r6 = "\\\\=";
        r7 = "=";
        r0 = r0.replace(r6, r7);
    L_0x0087:
        if (r0 == 0) goto L_0x0195;
    L_0x0089:
        r6 = r5.hashCode();
        switch(r6) {
            case -891988091: goto L_0x0105;
            case 98: goto L_0x009e;
            case 102: goto L_0x00a8;
            case 105: goto L_0x0095;
            case 108: goto L_0x00da;
            case 115: goto L_0x00b2;
            case 3212: goto L_0x00c6;
            case 3677: goto L_0x00bc;
            case 98855: goto L_0x00ef;
            case 99674: goto L_0x00d0;
            case 102230: goto L_0x00e4;
            case 3392903: goto L_0x00fa;
            default: goto L_0x0090;
        };
    L_0x0090:
        r2 = r3;
    L_0x0091:
        switch(r2) {
            case 0: goto L_0x0110;
            case 1: goto L_0x011a;
            case 2: goto L_0x0124;
            case 3: goto L_0x0094;
            case 4: goto L_0x012e;
            case 5: goto L_0x0145;
            case 6: goto L_0x0153;
            case 7: goto L_0x0161;
            case 8: goto L_0x016b;
            case 9: goto L_0x0173;
            case 10: goto L_0x0177;
            case 11: goto L_0x017a;
            default: goto L_0x0094;
        };
    L_0x0094:
        return r0;
    L_0x0095:
        r4 = "i";
        r4 = r5.equals(r4);
        if (r4 == 0) goto L_0x0090;
    L_0x009d:
        goto L_0x0091;
    L_0x009e:
        r2 = "b";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00a6:
        r2 = r4;
        goto L_0x0091;
    L_0x00a8:
        r2 = "f";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00b0:
        r2 = 2;
        goto L_0x0091;
    L_0x00b2:
        r2 = "s";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00ba:
        r2 = 3;
        goto L_0x0091;
    L_0x00bc:
        r2 = "sp";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00c4:
        r2 = 4;
        goto L_0x0091;
    L_0x00c6:
        r2 = "dp";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00ce:
        r2 = 5;
        goto L_0x0091;
    L_0x00d0:
        r2 = "dpf";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00d8:
        r2 = 6;
        goto L_0x0091;
    L_0x00da:
        r2 = "l";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00e2:
        r2 = 7;
        goto L_0x0091;
    L_0x00e4:
        r2 = "get";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00ec:
        r2 = 8;
        goto L_0x0091;
    L_0x00ef:
        r2 = "ctx";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x00f7:
        r2 = 9;
        goto L_0x0091;
    L_0x00fa:
        r2 = "null";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x0102:
        r2 = 10;
        goto L_0x0091;
    L_0x0105:
        r2 = "strget";
        r2 = r5.equals(r2);
        if (r2 == 0) goto L_0x0090;
    L_0x010d:
        r2 = 11;
        goto L_0x0091;
    L_0x0110:
        r0 = java.lang.Integer.parseInt(r0);
        r0 = java.lang.Integer.valueOf(r0);
        goto L_0x0094;
    L_0x011a:
        r0 = java.lang.Boolean.parseBoolean(r0);
        r0 = java.lang.Boolean.valueOf(r0);
        goto L_0x0094;
    L_0x0124:
        r0 = java.lang.Float.parseFloat(r0);
        r0 = java.lang.Float.valueOf(r0);
        goto L_0x0094;
    L_0x012e:
        r0 = java.lang.Float.parseFloat(r0);
        r1 = r9.f2539f;
        r1 = r1.getResources();
        r1 = r1.getDisplayMetrics();
        r1 = r1.scaledDensity;
        r0 = r0 * r1;
        r0 = java.lang.Float.valueOf(r0);
        goto L_0x0094;
    L_0x0145:
        r0 = java.lang.Integer.parseInt(r0);
        r0 = r9.m4476a(r0);
        r0 = java.lang.Integer.valueOf(r0);
        goto L_0x0094;
    L_0x0153:
        r0 = java.lang.Float.parseFloat(r0);
        r0 = r9.m4475a(r0);
        r0 = java.lang.Float.valueOf(r0);
        goto L_0x0094;
    L_0x0161:
        r0 = java.lang.Long.parseLong(r0);
        r0 = java.lang.Long.valueOf(r0);
        goto L_0x0094;
    L_0x016b:
        r1 = f2536h;
        r0 = r1.get(r0);
        goto L_0x0094;
    L_0x0173:
        r0 = r9.f2539f;
        goto L_0x0094;
    L_0x0177:
        r0 = r1;
        goto L_0x0094;
    L_0x017a:
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r2 = f2536h;
        r0 = r2.get(r0);
        r0 = r1.append(r0);
        r1 = "";
        r0 = r0.append(r1);
        r0 = r0.toString();
        goto L_0x0094;
    L_0x0195:
        r1 = r9.f2553w;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__getValue - value ";
        r3 = r3.append(r4);
        r4 = r9.m4478a();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r1.m4426b(r2, r3);
        r1 = r9.f2540g;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__getValue - value ";
        r3 = r3.append(r4);
        r4 = r9.m4478a();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r1.m4443a(r2, r3);
        goto L_0x0094;
    L_0x01d3:
        r0 = r9.f2553w;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__getValue - value ";
        r3 = r3.append(r4);
        r4 = r9.m4478a();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r0.m4426b(r2, r3);
        r0 = r9.f2540g;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__getValue - value ";
        r3 = r3.append(r4);
        r4 = r9.m4478a();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r0.m4443a(r2, r3);
        r0 = r1;
        goto L_0x0094;
        */
        throw new UnsupportedOperationException("Method not decompiled: in.juspay.mystique.g.g(java.lang.String):Any");
    }

    public int m4476a(int i) {
        if (i <= 0) {
            return 0;
        }
        return Math.round(this.f2539f.getResources().getDisplayMetrics().density * ((float) i));
    }

    public float m4475a(float f) {
        if (f > 0.0f) {
            return (float) Math.round(this.f2539f.getResources().getDisplayMetrics().density * f);
        }
        return 0.0f;
    }

    private int m4451a(String str, String str2, int i) {
        int indexOf = str.substring(i).indexOf(str2);
        if (indexOf == -1 || indexOf == 0 || indexOf >= str.length() || str.charAt((indexOf + i) - 1) != '\\') {
            return indexOf != -1 ? indexOf + i : indexOf;
        } else {
            return m4451a(str, str2, (indexOf + i) + str2.length());
        }
    }

    private String[] m4460a(String str, String str2) {
        if (m4451a(str, str2, 0) == -1) {
            return new String[]{str};
        }
        return new String[]{str.substring(0, m4451a(str, str2, 0)), str.substring(m4451a(str, str2, 0) + str2.length())};
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m4453a(java.lang.Object r11, java.lang.Object r12, java.lang.String r13) {
        /*
        r10 = this;
        r8 = -1;
        r1 = 0;
        r9 = 1;
        r6 = 0;
        r10.f2542k = r13;
        r0 = r10.f2549r;
        r0 = r10.m4451a(r13, r0, r6);
        if (r0 == r8) goto L_0x0440;
    L_0x000e:
        r0 = r10.f2549r;
        r0 = r10.m4460a(r13, r0);
        r0 = r0[r6];
        r2 = r10.f2550s;
        r2 = r10.m4451a(r0, r2, r6);
        if (r2 == r8) goto L_0x04a2;
    L_0x001e:
        r2 = 3;
        r2 = r0.substring(r6, r2);
        r3 = "get";
        r2 = r2.equals(r3);
        if (r2 == 0) goto L_0x04a2;
    L_0x002b:
        r2 = r10.f2550s;
        r2 = r10.m4460a(r0, r2);
        r0 = r2[r9];
        r2 = r2[r6];
    L_0x0035:
        r3 = r10.f2546o;
        r3 = r10.m4451a(r13, r3, r6);
        if (r3 == r8) goto L_0x0085;
    L_0x003d:
        r3 = r10.f2549r;
        r3 = r10.m4460a(r13, r3);
        r4 = r3[r9];
        r3 = r10.f2546o;
        r5 = r10.m4460a(r4, r3);
        r3 = r5[r6];
        r5 = r5[r9];
    L_0x004f:
        r7 = r2.hashCode();
        switch(r7) {
            case 98855: goto L_0x009a;
            case 102230: goto L_0x00a4;
            case 3559070: goto L_0x0090;
            default: goto L_0x0056;
        };
    L_0x0056:
        r7 = r8;
    L_0x0057:
        switch(r7) {
            case 0: goto L_0x00ae;
            case 1: goto L_0x0177;
            case 2: goto L_0x0249;
            default: goto L_0x005a;
        };
    L_0x005a:
        r0 = "var_";
        r0 = r10.m4451a(r3, r0, r6);
        if (r0 == r8) goto L_0x03a1;
    L_0x0062:
        r0 = r10.f2550s;
        r0 = r10.m4460a(r3, r0);
        r0 = r0[r9];
        r2 = java.lang.Class.forName(r3);
        r0 = r2.getDeclaredField(r0);
        r0.setAccessible(r9);
        r2 = r10.f2546o;
        r2 = r10.m4460a(r4, r2);
        r2 = r2[r9];
        r2 = r10.m4473g(r2);
        r0.set(r1, r2);
    L_0x0084:
        return r12;
    L_0x0085:
        r3 = r10.f2549r;
        r3 = r10.m4460a(r13, r3);
        r3 = r3[r9];
        r4 = r3;
        r5 = r1;
        goto L_0x004f;
    L_0x0090:
        r7 = "this";
        r7 = r2.equals(r7);
        if (r7 == 0) goto L_0x0056;
    L_0x0098:
        r7 = r6;
        goto L_0x0057;
    L_0x009a:
        r7 = "ctx";
        r7 = r2.equals(r7);
        if (r7 == 0) goto L_0x0056;
    L_0x00a2:
        r7 = r9;
        goto L_0x0057;
    L_0x00a4:
        r7 = "get";
        r7 = r2.equals(r7);
        if (r7 == 0) goto L_0x0056;
    L_0x00ac:
        r7 = 2;
        goto L_0x0057;
    L_0x00ae:
        if (r5 == 0) goto L_0x0115;
    L_0x00b0:
        r0 = r11.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x00c3;
    L_0x00ba:
        r1 = r10.m4468d(r5);
        r12 = r0.invoke(r11, r1);
        goto L_0x0084;
    L_0x00c3:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - classMethodDetails  ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - classMethodDetails  ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x0115:
        r0 = r11.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x0125;
    L_0x011f:
        r12 = r0.invoke(r11, r1);
        goto L_0x0084;
    L_0x0125:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - this  classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - this  classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x0177:
        if (r5 == 0) goto L_0x01e3;
    L_0x0179:
        r0 = r10.f2539f;
        r0 = r0.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x0191;
    L_0x0185:
        r1 = r10.f2539f;
        r2 = r10.m4468d(r5);
        r12 = r0.invoke(r1, r2);
        goto L_0x0084;
    L_0x0191:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - ctx  classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - ctx  classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x01e3:
        r0 = r10.f2539f;
        r0 = r0.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x01f7;
    L_0x01ef:
        r2 = r10.f2539f;
        r12 = r0.invoke(r2, r1);
        goto L_0x0084;
    L_0x01f7:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - ctx classMethodDetails  ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - ctx classMethodDetails  ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x0249:
        if (r0 == 0) goto L_0x0084;
    L_0x024b:
        r2 = f2536h;
        r2 = r2.get(r0);
        r7 = r10.f2550s;
        r6 = r10.m4451a(r3, r7, r6);
        if (r6 != r8) goto L_0x0325;
    L_0x0259:
        if (r2 == 0) goto L_0x0325;
    L_0x025b:
        if (r5 == 0) goto L_0x02c3;
    L_0x025d:
        r0 = r2.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x0271;
    L_0x0267:
        r1 = r10.m4468d(r5);
        r12 = r0.invoke(r2, r1);
        goto L_0x0084;
    L_0x0271:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - get classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - get classMethodDetails ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x02c3:
        r0 = r2.getClass();
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x02d3;
    L_0x02cd:
        r12 = r0.invoke(r2, r1);
        goto L_0x0084;
    L_0x02d3:
        r0 = r10.f2553w;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - get classMethodDetails : ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4426b(r1, r2);
        r0 = r10.f2540g;
        r1 = "WARNING";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = " isNull : fn__runCommand - get classMethodDetails : ";
        r2 = r2.append(r3);
        r2 = r2.append(r4);
        r3 = " ";
        r2 = r2.append(r3);
        r3 = r10.m4478a();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0.m4443a(r1, r2);
        goto L_0x0084;
    L_0x0325:
        if (r2 == 0) goto L_0x0343;
    L_0x0327:
        r1 = r10.f2550s;
        r1 = r10.m4460a(r3, r1);
        r1 = r1[r9];
        r2 = f2536h;
        r0 = r2.get(r0);
        r2 = r10.f2546o;
        r2 = r10.m4460a(r4, r2);
        r2 = r2[r9];
        r12 = r10.m4454a(r0, r1, r2);
        goto L_0x0084;
    L_0x0343:
        r1 = r10.f2553w;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__runCommand - get_";
        r3 = r3.append(r4);
        r3 = r3.append(r0);
        r4 = " is null";
        r3 = r3.append(r4);
        r4 = " ";
        r3 = r3.append(r4);
        r4 = r10.m4478a();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r1.m4426b(r2, r3);
        r1 = r10.f2540g;
        r2 = "WARNING";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = " isNull : fn__runCommand - get_";
        r3 = r3.append(r4);
        r0 = r3.append(r0);
        r3 = " is null";
        r0 = r0.append(r3);
        r3 = " ";
        r0 = r0.append(r3);
        r3 = r10.m4478a();
        r0 = r0.append(r3);
        r0 = r0.toString();
        r1.m4443a(r2, r0);
        goto L_0x0084;
    L_0x03a1:
        r0 = "new";
        r0 = r4.equals(r0);
        if (r0 != 0) goto L_0x03b9;
    L_0x03a9:
        r0 = r10.f2546o;
        r0 = r10.m4460a(r4, r0);
        r0 = r0[r6];
        r3 = "new";
        r0 = r0.equals(r3);
        if (r0 == 0) goto L_0x0414;
    L_0x03b9:
        if (r5 == 0) goto L_0x040a;
    L_0x03bb:
        r0 = "in.juspay.mystique.DuiInvocationHandler";
        r0 = r2.equals(r0);
        if (r0 == 0) goto L_0x03d2;
    L_0x03c3:
        r0 = r10.m4468d(r5);
        r12 = new in.juspay.mystique.b;
        r0 = r0[r6];
        r1 = r10.f2545n;
        r12.<init>(r0, r1);
        goto L_0x0084;
    L_0x03d2:
        r1 = r10.m4471e(r5);
        r0 = java.lang.Class.forName(r2);
        r2 = r0.getConstructors();
        r0 = r6;
    L_0x03df:
        r3 = r2.length;
        if (r0 >= r3) goto L_0x0084;
    L_0x03e2:
        r3 = r2[r0];
        r3 = r3.getParameterTypes();
        r3 = r3.length;
        r4 = r10.m4474h(r5);
        if (r3 != r4) goto L_0x0407;
    L_0x03ef:
        r3 = r2[r0];
        r3 = r3.getParameterTypes();
        r3 = r10.m4459a(r3, r1);
        if (r3 == 0) goto L_0x0407;
    L_0x03fb:
        r0 = r2[r0];
        r1 = r10.m4468d(r5);
        r12 = r0.newInstance(r1);
        goto L_0x0084;
    L_0x0407:
        r0 = r0 + 1;
        goto L_0x03df;
    L_0x040a:
        r0 = java.lang.Class.forName(r2);
        r12 = r0.newInstance();
        goto L_0x0084;
    L_0x0414:
        r0 = java.lang.Class.forName(r2);
        r0 = r10.m4455a(r0, r4);
        if (r0 == 0) goto L_0x0084;
    L_0x041e:
        r2 = r0.getName();
        r3 = "forName";
        r2 = r2.equals(r3);
        if (r2 == 0) goto L_0x0436;
    L_0x042a:
        r0 = r10.m4473g(r5);
        r0 = (java.lang.String) r0;
        r12 = java.lang.Class.forName(r0);
        goto L_0x0084;
    L_0x0436:
        r2 = r10.m4468d(r5);
        r12 = r0.invoke(r1, r2);
        goto L_0x0084;
    L_0x0440:
        if (r12 != 0) goto L_0x0472;
    L_0x0442:
        r0 = r10.f2546o;
        r0 = r10.m4451a(r13, r0, r6);
        if (r0 == r8) goto L_0x0464;
    L_0x044a:
        r0 = r10.f2546o;
        r0 = r10.m4460a(r13, r0);
        r0 = r0[r9];
        r1 = r11.getClass();
        r1 = r10.m4455a(r1, r13);
        r0 = r10.m4468d(r0);
        r12 = r1.invoke(r11, r0);
        goto L_0x0084;
    L_0x0464:
        r0 = r11.getClass();
        r0 = r10.m4455a(r0, r13);
        r12 = r0.invoke(r11, r1);
        goto L_0x0084;
    L_0x0472:
        r0 = r10.f2546o;
        r0 = r10.m4451a(r13, r0, r6);
        if (r0 == r8) goto L_0x0494;
    L_0x047a:
        r0 = r10.f2546o;
        r0 = r10.m4460a(r13, r0);
        r0 = r0[r9];
        r1 = r12.getClass();
        r1 = r10.m4455a(r1, r13);
        r0 = r10.m4468d(r0);
        r12 = r1.invoke(r12, r0);
        goto L_0x0084;
    L_0x0494:
        r0 = r12.getClass();
        r0 = r10.m4455a(r0, r13);
        r12 = r0.invoke(r12, r1);
        goto L_0x0084;
    L_0x04a2:
        r2 = r0;
        r0 = r1;
        goto L_0x0035;
        */
        throw new UnsupportedOperationException("Method not decompiled: in.juspay.mystique.g.a(java.lang.Object, java.lang.Object, java.lang.String):java.lang.Object");
    }

    public Object m4477a(Object obj, String str) {
        Object obj2 = null;
        for (String str2 : this.f2552u.split(str)) {
            if (!str2.equals(BuildConfig.FLAVOR)) {
                if (m4451a(str2, this.f2551t, 0) != -1) {
                    String[] a = m4460a(str2, this.f2551t);
                    String str3 = m4460a(a[0], this.f2550s)[1];
                    Object a2 = m4453a(obj, obj2, a[1]);
                    f2536h.put(str3, a2);
                    this.f2553w.m4425a(f2537i, "setting " + str3 + " to " + a2);
                } else {
                    obj2 = m4453a(obj, obj2, str2);
                }
            }
        }
        return obj;
    }

    private int m4474h(String str) {
        return this.f2548q.split(str).length;
    }

    private boolean m4459a(Class<?>[] clsArr, Class<?>[] clsArr2) {
        int i = 0;
        while (i < clsArr.length) {
            if (!(clsArr2[i] == null || clsArr[i] == null || ((clsArr[i].equals(Object.class) && !clsArr2[i].isPrimitive()) || clsArr[i].equals(clsArr2[i])))) {
                if (clsArr[i].isPrimitive() && !clsArr2[i].isArray()) {
                    try {
                        if (!((Class) clsArr2[i].getField("TYPE").get(null)).equals(clsArr[i])) {
                            return false;
                        }
                    } catch (Exception e) {
                        if (e.getMessage().equals("java.lang.NoSuchFieldException")) {
                            return false;
                        }
                    }
                } else if (clsArr[i].equals(ClassLoader.class)) {
                    if (clsArr2[i].getName().equals("dalvik.system.PathClassLoader")) {
                        return true;
                    }
                } else if (!clsArr[i].equals(clsArr2[i])) {
                    return false;
                } else {
                    if (!clsArr[i].isAssignableFrom(clsArr2[i])) {
                        return false;
                    }
                }
            }
            i++;
        }
        return true;
    }

    private Method m4457a(Class cls, String str, Class[] clsArr) {
        return cls.getMethod(str, clsArr);
    }

    private Method m4456a(Class cls, String str, Class cls2) {
        if (C0543g.m4458a(cls2)) {
            try {
                return cls.getMethod(str, new Class[]{(Class) f2538v.get(cls2)});
            } catch (NoSuchMethodException e) {
            }
        }
        do {
            int i = 0;
            while (i < cls2.getInterfaces().length) {
                try {
                    return cls.getMethod(str, new Class[]{cls2.getInterfaces()[i]});
                } catch (NoSuchMethodException e2) {
                    i++;
                }
            }
            try {
                return cls.getMethod(str, new Class[]{cls2});
            } catch (NoSuchMethodException e3) {
                cls2 = cls2.getSuperclass();
                if (cls2 == null) {
                    this.f2553w.m4426b(f2537i, "Never reach here");
                    return null;
                }
            }
        } while (cls2 == null);
        this.f2553w.m4426b(f2537i, "Never reach here");
        return null;
    }

    private Method m4463b(Class cls, String str, Class[] clsArr) {
        for (Method method : cls.getMethods()) {
            if (method.getName().equals(str) && clsArr != null && method.getParameterTypes().length == clsArr.length && m4459a(method.getParameterTypes(), clsArr)) {
                return method;
            }
        }
        return null;
    }

    private Method m4455a(Class cls, String str) {
        Class[] clsArr = null;
        if (cls == null) {
            return null;
        }
        String str2;
        if (m4451a(str, this.f2546o, 0) != -1) {
            String[] a = m4460a(str, this.f2546o);
            str = a[0];
            str2 = a[1];
        } else {
            str2 = null;
        }
        if (str2 != null) {
            clsArr = m4471e(str2);
        }
        C0542a c0542a = new C0542a(cls, str, clsArr);
        if (f2531a.containsKey(c0542a)) {
            return (Method) f2531a.get(c0542a);
        }
        Method a2;
        try {
            a2 = m4457a(cls, str, clsArr);
        } catch (NoSuchMethodException e) {
            if (clsArr == null || clsArr.length != 1) {
                a2 = m4463b(cls, str, clsArr);
            } else {
                a2 = m4456a(cls, str, clsArr[0]);
            }
        }
        f2531a.put(c0542a, a2);
        return a2;
    }

    private Object m4454a(Object obj, String str, String str2) {
        Field field = null;
        try {
            field = obj.getClass().getField(str);
        } catch (NoSuchFieldException e) {
            Field[] fields = obj.getClass().getFields();
            int length = fields.length;
            int i = 0;
            while (i < length) {
                Field field2 = fields[i];
                if (!field2.getName().equals(str)) {
                    field2 = field;
                }
                i++;
                field = field2;
            }
        }
        if (field != null) {
            field.set(obj, m4473g(str2));
        } else {
            this.f2553w.m4425a(f2537i, "Couldn't set field for " + str);
        }
        return obj;
    }

    public void m4479a(String str) {
        this.f2541j = str;
    }

    public void m4481b(String str) {
        this.f2543l = str;
    }

    public void m4482c(String str) {
        this.f2544m = str;
    }

    public void m4480a(String str, JSONObject jSONObject, Object obj) {
        try {
            Method method;
            String str2;
            int i;
            String string;
            String[] split;
            if (str.equals("pattern")) {
                method = obj.getClass().getMethod("setFilters", new Class[]{InputFilter[].class});
                String[] split2 = jSONObject.getString("pattern").split(",");
                str2 = split2[0];
                if (split2.length == 1) {
                    i = AbstractSpiCall.DEFAULT_TIMEOUT;
                } else {
                    i = Integer.parseInt(split2[1].trim());
                }
                C05331 c05331 = new C05331(this, str2);
                InputFilter[] inputFilterArr = new InputFilter[]{c05331, new LengthFilter(i)};
                method.invoke(obj, new Object[]{inputFilterArr});
            }
            if (str.equals("onKeyUp")) {
                string = jSONObject.getString("onKeyUp");
                obj.getClass().getMethod("setOnKeyListener", new Class[]{OnKeyListener.class}).invoke(obj, new Object[]{new C05375(this, string)});
            }
            if (str.equals("onLongPress")) {
                string = jSONObject.getString("onLongPress");
                obj.getClass().getMethod("setOnLongClickListener", new Class[]{OnLongClickListener.class}).invoke(obj, new Object[]{new C05386(this, string)});
            }
            if (str.equals("onClick")) {
                string = jSONObject.getString("onClick");
                obj.getClass().getMethod("setOnClickListener", new Class[]{OnClickListener.class}).invoke(obj, new Object[]{new C05397(this, string)});
            }
            if (str.equals("onItemClick")) {
                string = jSONObject.getString("onItemClick");
                obj.getClass().getMethod("setOnItemSelectedListener", new Class[]{OnItemSelectedListener.class}).invoke(obj, new Object[]{new C05408(this, string)});
            }
            if (str.equals("onChange")) {
                obj.getClass().getMethod("addTextChangedListener", new Class[]{TextWatcher.class}).invoke(obj, new Object[]{new C05419(this, jSONObject.getString("onChange"))});
            }
            if (str.equals("onFocus")) {
                obj.getClass().getMethod("setOnFocusChangeListener", new Class[]{OnFocusChangeListener.class}).invoke(obj, new Object[]{new AnonymousClass10(this, jSONObject.getString("onFocus"))});
            }
            if (str.equals("onTouch")) {
                string = jSONObject.getString("onTouch");
                obj.getClass().getMethod("setOnTouchListener", new Class[]{OnTouchListener.class}).invoke(obj, new Object[]{new AnonymousClass11(this, string)});
            }
            if (str.equals("onDateChange")) {
                string = jSONObject.getString("onDateChange");
                method = obj.getClass().getMethod("setOnDateChangeListener", new Class[]{OnDateChangeListener.class});
                if (VERSION.SDK_INT >= 11) {
                    method.invoke(obj, new Object[]{new AnonymousClass12(this, string)});
                }
            }
            if (str.equals("onSwipe")) {
                string = jSONObject.getString("onSwipe");
                obj.getClass().getMethod("setOnTouchListener", new Class[]{OnTouchListener.class}).invoke(obj, new Object[]{new C05342(this, string)});
            }
            if (str.equals("popupMenu") && VERSION.SDK_INT >= 11) {
                split = jSONObject.getString("popupMenu").split(this.f2548q.toString());
                str2 = jSONObject.getString("onMenuItemClick");
                PopupMenu popupMenu = new PopupMenu(this.f2539f, (View) obj);
                i = 0;
                while (i < split.length) {
                    if (!(split[i].indexOf("\\") == -1 || split[i].indexOf(",") == -1)) {
                        split[i] = split[i].replace("\\\\,", ",");
                    }
                    popupMenu.getMenu().add(0, i, 0, split[i]);
                    i++;
                }
                popupMenu.setOnMenuItemClickListener(new C05353(this, str2));
                ((View) obj).setOnClickListener(new C05364(this, popupMenu));
            }
            if (str.equals("localImage")) {
                String[] split3 = jSONObject.getString("localImage").split(",");
                Bitmap bitmap = null;
                for (i = 0; i < split3.length && bitmap == null; i++) {
                    bitmap = BitmapFactory.decodeFile(split3[i]);
                }
                ((ImageView) obj).setImageBitmap(bitmap);
            }
            if (str.equals("localBackgoundImage")) {
                split = jSONObject.getString("localImage").split(",");
                Bitmap bitmap2 = null;
                for (i = 0; i < split.length && bitmap2 == null; i++) {
                    bitmap2 = BitmapFactory.decodeFile(split[i]);
                }
                if (VERSION.SDK_INT >= 16) {
                    ((View) obj).setBackground(new BitmapDrawable(this.f2539f.getResources(), bitmap2));
                } else {
                    ((View) obj).setBackgroundDrawable(new BitmapDrawable(this.f2539f.getResources(), bitmap2));
                }
            }
            if (str.equals("runInUI")) {
                m4477a(obj, jSONObject.getString(str));
            }
            if (str.equals("afterRender")) {
                this.f2545n.m4438a("javascript:window.callUICallback('" + jSONObject.getString("afterRender") + "', '" + jSONObject.getString("id") + "');");
            }
        } catch (Exception e) {
            if (e != null) {
                this.f2540g.m4443a("WARNING", " excep: fn__parseKeys  - " + e.getClass().getName() + " - " + m4478a());
            }
        }
    }
}
