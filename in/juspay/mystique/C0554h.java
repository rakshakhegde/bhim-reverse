package in.juspay.mystique;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;
import com.crashlytics.android.core.BuildConfig;
import in.juspay.mystique.h.AnonymousClass10;
import in.juspay.mystique.h.AnonymousClass11;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

/* renamed from: in.juspay.mystique.h */
public class C0554h {
    private static final String f2594h;
    private Activity f2595a;
    private C0556j f2596b;
    private ViewGroup f2597c;
    private C0525c f2598d;
    private C0531e f2599e;
    private String f2600f;
    private C0530d f2601g;

    /* renamed from: in.juspay.mystique.h.10 */
    class AnonymousClass10 implements OnMenuItemClickListener {
        final /* synthetic */ String f2554a;
        final /* synthetic */ h f2555b;

        AnonymousClass10(h hVar, String str) {
            this.f2555b = hVar;
            this.f2554a = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            this.f2555b.f2601g.m4438a("window.callUICallback('" + this.f2554a + "', '" + menuItem.getItemId() + "');");
            Toast.makeText(this.f2555b.f2595a, "You Clicked : " + menuItem.getTitle(), 0).show();
            return true;
        }
    }

    /* renamed from: in.juspay.mystique.h.11 */
    class AnonymousClass11 extends AsyncTask {
        final /* synthetic */ String f2556a;
        final /* synthetic */ String f2557b;
        final /* synthetic */ h f2558c;

        AnonymousClass11(h hVar, String str, String str2) {
            this.f2558c = hVar;
            this.f2556a = str;
            this.f2557b = str2;
        }

        protected Object doInBackground(Object[] objArr) {
            try {
                C0555i.m4491a(this.f2558c.f2595a.getApplicationContext(), this.f2556a);
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE : " + e.getMessage();
            }
        }

        protected void onPostExecute(Object obj) {
            if (this.f2557b != null) {
                this.f2558c.f2601g.m4438a("window." + this.f2557b + "(\"" + ((String) obj) + "\", \"" + this.f2556a + "\");");
            } else {
                Log.d("downloadFile", (String) obj);
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.1 */
    class C05441 implements Runnable {
        final /* synthetic */ String f2559a;
        final /* synthetic */ String f2560b;
        final /* synthetic */ h f2561c;

        C05441(h hVar, String str, String str2) {
            this.f2561c = hVar;
            this.f2559a = str;
            this.f2560b = str2;
        }

        public void run() {
            try {
                this.f2561c.f2596b.m4498a(this.f2559a, this.f2561c.f2597c);
                if (this.f2560b != null) {
                    this.f2561c.f2601g.m4438a("window.callUICallback(" + this.f2560b + ",'success');");
                }
            } catch (Exception e) {
                if (e != null) {
                    String name = e.getClass().getName();
                    this.f2561c.f2598d.m4426b("ERROR", " excep: fn__Render  - " + name + " - " + this.f2561c.f2596b.m4497a());
                    this.f2561c.f2599e.m4443a("ERROR", " excep: fn__Render  - " + name + " - " + this.f2561c.f2596b.m4497a());
                }
                if (this.f2560b != null) {
                    this.f2561c.f2601g.m4438a("window.callUICallback(" + this.f2560b + ",'failure');");
                }
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.2 */
    class C05452 implements Runnable {
        final /* synthetic */ h f2562a;

        C05452(h hVar) {
            this.f2562a = hVar;
        }

        public void run() {
        }
    }

    /* renamed from: in.juspay.mystique.h.3 */
    class C05463 extends AsyncTask {
        final /* synthetic */ String f2563a;
        final /* synthetic */ String f2564b;
        final /* synthetic */ String f2565c;
        final /* synthetic */ String f2566d;
        final /* synthetic */ h f2567e;

        C05463(h hVar, String str, String str2, String str3, String str4) {
            this.f2567e = hVar;
            this.f2563a = str;
            this.f2564b = str2;
            this.f2565c = str3;
            this.f2566d = str4;
        }

        protected void onPostExecute(Object obj) {
            if (obj != null) {
                Log.d(C0554h.f2594h, "Response of API: " + obj);
                if (((String) obj).startsWith("ERR:")) {
                    this.f2567e.f2601g.m4438a("window.callUICallback(\"" + this.f2563a + "\", 'error' ,\"" + obj + "\");");
                } else {
                    this.f2567e.f2601g.m4438a("window.callUICallback(\"" + this.f2563a + "\", " + obj + ");");
                }
            }
        }

        protected Object doInBackground(Object[] objArr) {
            Log.d(C0554h.f2594h, "Now calling API :" + this.f2564b);
            HashMap hashMap = new HashMap();
            try {
                JSONObject jSONObject = new JSONObject(this.f2565c);
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap.put(str, jSONObject.getString(str));
                }
                return new String(RestClient.m4421a(this.f2564b, this.f2566d, hashMap));
            } catch (Throwable e) {
                Log.e("Error", "JSON-EXCEPTION", e);
                return "ERR: " + e.getLocalizedMessage();
            } catch (Exception e2) {
                return "ERR: " + e2.getLocalizedMessage();
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.4 */
    class C05474 implements Runnable {
        final /* synthetic */ String f2568a;
        final /* synthetic */ String f2569b;
        final /* synthetic */ int f2570c;
        final /* synthetic */ boolean f2571d;
        final /* synthetic */ String f2572e;
        final /* synthetic */ h f2573f;

        C05474(h hVar, String str, String str2, int i, boolean z, String str3) {
            this.f2573f = hVar;
            this.f2568a = str;
            this.f2569b = str2;
            this.f2570c = i;
            this.f2571d = z;
            this.f2572e = str3;
        }

        public void run() {
            try {
                this.f2573f.f2596b.m4500a(this.f2568a, new JSONObject(this.f2569b), this.f2570c, this.f2571d);
                if (this.f2572e != null) {
                    this.f2573f.f2601g.m4438a("window.callUICallback('" + this.f2572e + "','" + "success" + "');");
                }
            } catch (Exception e) {
                if (e != null) {
                    String name = e.getClass().getName();
                    this.f2573f.f2598d.m4426b("ERROR", " excep: fn__addViewToParent  - " + name + " - " + this.f2573f.f2596b.m4497a());
                    this.f2573f.f2599e.m4443a("ERROR", " excep: fn__addViewToParent  - " + name + " - " + this.f2573f.f2596b.m4497a());
                }
                if (this.f2572e != null) {
                    this.f2573f.f2601g.m4438a("window.callUICallback('" + this.f2572e + "','" + "failure" + "');");
                }
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.5 */
    class C05485 implements Runnable {
        final /* synthetic */ String f2574a;
        final /* synthetic */ String f2575b;
        final /* synthetic */ String f2576c;
        final /* synthetic */ String f2577d;
        final /* synthetic */ h f2578e;

        C05485(h hVar, String str, String str2, String str3, String str4) {
            this.f2578e = hVar;
            this.f2574a = str;
            this.f2575b = str2;
            this.f2576c = str3;
            this.f2577d = str4;
        }

        public void run() {
            try {
                this.f2578e.f2596b.m4496a(this.f2578e.f2595a, this.f2574a, this.f2575b, this.f2576c);
                if (this.f2577d != null) {
                    this.f2578e.f2601g.m4438a("window.callUICallback(" + this.f2577d + ",'success');");
                }
            } catch (Exception e) {
                if (e != null) {
                    String name = e.getClass().getName();
                    this.f2578e.f2598d.m4426b("ERROR", " excep: fn__runInUI  - " + name + " - " + this.f2578e.f2596b.m4497a());
                    this.f2578e.f2599e.m4443a("ERROR", " excep: fn__runInUI  - " + name + " - " + this.f2578e.f2596b.m4497a());
                }
                if (this.f2577d != null) {
                    this.f2578e.f2601g.m4438a("window.callUICallback(" + this.f2577d + ",'failure');");
                }
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.6 */
    class C05496 implements Runnable {
        final /* synthetic */ String f2579a;
        final /* synthetic */ String f2580b;
        final /* synthetic */ h f2581c;

        C05496(h hVar, String str, String str2) {
            this.f2581c = hVar;
            this.f2579a = str;
            this.f2580b = str2;
        }

        public void run() {
            try {
                this.f2581c.f2596b.m4496a(this.f2581c.f2595a, this.f2579a, BuildConfig.FLAVOR, BuildConfig.FLAVOR);
                if (this.f2580b != null) {
                    this.f2581c.f2601g.m4438a("window.callUICallback(" + this.f2580b + ",'success');");
                }
            } catch (Exception e) {
                if (e != null) {
                    String name = e.getClass().getName();
                    this.f2581c.f2598d.m4426b("ERROR", " excep: fn__runInUI  - " + name + " - " + this.f2581c.f2596b.m4497a());
                    this.f2581c.f2599e.m4443a("ERROR", " excep: fn__runInUI  - " + name + " - " + this.f2581c.f2596b.m4497a());
                }
                if (this.f2580b != null) {
                    this.f2581c.f2601g.m4438a("window.callUICallback(" + this.f2580b + ",'failure');");
                }
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.7 */
    class C05507 implements Runnable {
        final /* synthetic */ int f2582a;
        final /* synthetic */ String f2583b;
        final /* synthetic */ h f2584c;

        C05507(h hVar, int i, String str) {
            this.f2584c = hVar;
            this.f2582a = i;
            this.f2583b = str;
        }

        public void run() {
            try {
                ImageView imageView = (ImageView) this.f2584c.f2595a.findViewById(this.f2582a);
                byte[] decode = Base64.decode(this.f2583b, 0);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(decode, 0, decode.length));
            } catch (Exception e) {
                if (e != null) {
                    String name = e.getClass().getName();
                    this.f2584c.f2598d.m4426b("ERROR", " excep: fn__setImage  - " + name + " - " + this.f2584c.f2596b.m4497a());
                    this.f2584c.f2599e.m4443a("ERROR", " excep: fn__setImage  - " + name + " - " + this.f2584c.f2596b.m4497a());
                }
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.8 */
    class C05518 implements Runnable {
        final /* synthetic */ int f2585a;
        final /* synthetic */ String f2586b;
        final /* synthetic */ h f2587c;

        C05518(h hVar, int i, String str) {
            this.f2587c = hVar;
            this.f2585a = i;
            this.f2586b = str;
        }

        public void run() {
            View findViewById = this.f2587c.f2595a.findViewById(this.f2585a);
            InputMethodManager inputMethodManager = (InputMethodManager) this.f2587c.f2595a.getSystemService("input_method");
            if (this.f2586b.equals("show")) {
                inputMethodManager.showSoftInput(findViewById, 1);
            } else {
                inputMethodManager.hideSoftInputFromWindow(findViewById.getWindowToken(), 0);
            }
        }
    }

    /* renamed from: in.juspay.mystique.h.9 */
    class C05539 implements Runnable {
        final /* synthetic */ String f2589a;
        final /* synthetic */ int f2590b;
        final /* synthetic */ String[] f2591c;
        final /* synthetic */ String f2592d;
        final /* synthetic */ h f2593e;

        /* renamed from: in.juspay.mystique.h.9.1 */
        class C05521 implements OnClickListener {
            final /* synthetic */ C05539 f2588a;

            C05521(C05539 c05539) {
                this.f2588a = c05539;
            }

            public void onClick(View view) {
                Log.d("log!!!:", "popup_clicked1");
                this.f2588a.f2593e.m4490a(view, this.f2588a.f2591c, this.f2588a.f2592d);
                Log.d("log!!!:", "popup_clicked2");
            }
        }

        C05539(h hVar, String str, int i, String[] strArr, String str2) {
            this.f2593e = hVar;
            this.f2589a = str;
            this.f2590b = i;
            this.f2591c = strArr;
            this.f2592d = str2;
        }

        public void run() {
            if (this.f2589a.equals("PopupMenu")) {
                this.f2593e.f2595a.findViewById(this.f2590b).setOnClickListener(new C05521(this));
            }
        }
    }

    static {
        f2594h = h.class.getName();
    }

    C0554h(Activity activity, ViewGroup viewGroup, C0530d c0530d) {
        this.f2595a = activity;
        this.f2601g = c0530d;
        this.f2596b = new C0556j(this.f2595a, c0530d);
        this.f2597c = viewGroup;
        this.f2598d = C0530d.m4432b();
        this.f2599e = c0530d.m4440c();
    }

    @JavascriptInterface
    public void Render(String str, String str2) {
        this.f2595a.runOnUiThread(new C05441(this, str, str2));
    }

    @JavascriptInterface
    public void throwError(String str) {
        this.f2598d.m4426b("throwError", str);
    }

    @JavascriptInterface
    public void addViewToParent(String str, String str2, int i, String str3, boolean z) {
        this.f2595a.runOnUiThread(new C05474(this, str, str2, i, z, str3));
    }

    @JavascriptInterface
    public void addViewToParent(String str, String str2, int i, String str3) {
        addViewToParent(str, str2, i, str3, false);
    }

    @JavascriptInterface
    public void runInUI(String str, String str2, String str3, String str4) {
        this.f2595a.runOnUiThread(new C05485(this, str, str3, str4, str2));
    }

    @JavascriptInterface
    public void runInUI(String str, String str2) {
        this.f2595a.runOnUiThread(new C05496(this, str, str2));
    }

    @JavascriptInterface
    public void run(String str, String str2) {
        try {
            this.f2596b.m4496a(this.f2595a, str, BuildConfig.FLAVOR, BuildConfig.FLAVOR);
            if (str2 != null) {
                this.f2601g.m4438a("window.callUICallback(" + str2 + ",'success');");
            }
        } catch (Exception e) {
            if (e != null) {
                String name = e.getClass().getName();
                this.f2598d.m4426b("runInUI", name);
                this.f2599e.m4443a("runInUI", name + " - " + this.f2596b.m4497a());
            }
            if (str2 != null) {
                this.f2601g.m4438a("window.callUICallback(" + str2 + ",'failure');");
            }
        }
    }

    @JavascriptInterface
    public void saveState(String str) {
        this.f2600f = str;
    }

    @JavascriptInterface
    public String getState() {
        if (this.f2600f != null) {
            return this.f2600f;
        }
        return "{}";
    }

    @JavascriptInterface
    public void setImage(int i, String str) {
        this.f2595a.runOnUiThread(new C05507(this, i, str));
    }

    @JavascriptInterface
    public void setState(String str) {
        this.f2600f = str;
    }

    @JavascriptInterface
    public String fetchData(String str) {
        return this.f2595a.getSharedPreferences("DUI", 0).getString(str, "null");
    }

    @JavascriptInterface
    public void saveData(String str, String str2) {
        this.f2595a.getSharedPreferences("DUI", 0).edit().putString(str, str2).commit();
    }

    @JavascriptInterface
    public String getScreenDimensions() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.f2595a.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return "{\"width\":" + displayMetrics.widthPixels + ",\"height\":" + displayMetrics.heightPixels + " }";
    }

    @JavascriptInterface
    public void toggleKeyboard(int i, String str) {
        this.f2595a.runOnUiThread(new C05518(this, i, str));
    }

    @JavascriptInterface
    public void generateUIElement(String str, int i, String[] strArr, String str2) {
        this.f2595a.runOnUiThread(new C05539(this, str, i, strArr, str2));
    }

    public void m4490a(View view, String[] strArr, String str) {
        if (VERSION.SDK_INT >= 11) {
            PopupMenu popupMenu = new PopupMenu(this.f2595a, view);
            for (Integer valueOf = Integer.valueOf(0); valueOf.intValue() < strArr.length; valueOf = Integer.valueOf(valueOf.intValue() + 1)) {
                popupMenu.getMenu().add(0, valueOf.intValue(), 0, strArr[valueOf.intValue()]);
            }
            popupMenu.setOnMenuItemClickListener(new AnonymousClass10(this, str));
            popupMenu.show();
        }
    }

    @JavascriptInterface
    public String getInternalStorageBaseFilePath() {
        return this.f2595a.getDir("juspay", 0).getAbsolutePath();
    }

    @JavascriptInterface
    public String getAssetBaseFilePath() {
        return "/android_asset";
    }

    @JavascriptInterface
    public String getExternalStorageBaseFilePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    @JavascriptInterface
    public String loadFileFromExternalStorage(String str, String str2) {
        try {
            return new String(C0532f.m4448a(str, str2));
        } catch (Exception e) {
            return BuildConfig.FLAVOR;
        }
    }

    @JavascriptInterface
    public String loadFile(String str) {
        try {
            byte[] a = C0532f.m4447a(this.f2595a, str);
            if (a == null) {
                return BuildConfig.FLAVOR;
            }
            return new String(a);
        } catch (Exception e) {
            return BuildConfig.FLAVOR;
        }
    }

    @JavascriptInterface
    public void downloadFile(String str, String str2) {
        new AnonymousClass11(this, str, str2).execute(new Object[0]);
    }

    @JavascriptInterface
    public String saveFileToInternalStorage(String str, String str2, String str3) {
        try {
            C0532f.m4446a(this.f2595a, str, str2.getBytes());
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE : " + e.getMessage();
        }
    }

    @JavascriptInterface
    public boolean isFilePresent(String str) {
        return new File(str).exists();
    }

    @JavascriptInterface
    public void showLoading() {
        this.f2595a.runOnUiThread(new C05452(this));
    }

    @JavascriptInterface
    public void callAPI(String str, String str2, String str3, String str4) {
        new C05463(this, str4, str, str3, str2).execute(new Object[0]);
    }
}
