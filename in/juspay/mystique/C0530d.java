package in.juspay.mystique;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.crashlytics.android.core.BuildConfig;

/* renamed from: in.juspay.mystique.d */
public final class C0530d {
    private static C0525c f2497b;
    private WebView f2498a;
    private Activity f2499c;
    private C0531e f2500d;
    private h f2501e;
    private FrameLayout f2502f;

    /* renamed from: in.juspay.mystique.d.1 */
    class C05281 implements Runnable {
        final /* synthetic */ String f2493a;
        final /* synthetic */ C0530d f2494b;

        C05281(C0530d c0530d, String str) {
            this.f2494b = c0530d;
            this.f2493a = str;
        }

        public void run() {
            try {
                if (this.f2494b.f2498a != null) {
                    this.f2494b.f2498a.loadUrl("javascript:" + this.f2493a);
                } else {
                    C0530d.f2497b.m4426b("DynamicUI", "browser null, call start first");
                }
            } catch (Object e) {
                C0530d.f2497b.m4426b("DynamicUI", "OutOfMemoryError :" + this.f2494b.m4431a(e));
                this.f2494b.f2500d.m4443a("addJsToWebView", BuildConfig.FLAVOR + this.f2494b.m4431a(e));
            } catch (Object e2) {
                C0530d.f2497b.m4426b("DynamicUI", "Exception :" + this.f2494b.m4431a(e2));
                this.f2494b.f2500d.m4443a("addJsToWebView", BuildConfig.FLAVOR + this.f2494b.m4431a(e2));
            }
        }
    }

    /* renamed from: in.juspay.mystique.d.2 */
    class C05292 implements Runnable {
        final /* synthetic */ String f2495a;
        final /* synthetic */ C0530d f2496b;

        C05292(C0530d c0530d, String str) {
            this.f2496b = c0530d;
            this.f2495a = str;
        }

        public void run() {
            this.f2496b.f2498a.loadUrl(this.f2495a);
        }
    }

    public C0530d(Activity activity, FrameLayout frameLayout, Bundle bundle, C0531e c0531e) {
        f2497b = new C0526a();
        this.f2500d = c0531e;
        if (activity == null || frameLayout == null) {
            f2497b.m4426b("DynamicUI", "container or activity null");
            return;
        }
        this.f2499c = activity;
        this.f2502f = frameLayout;
        this.f2498a = new WebView(activity.getApplicationContext());
        m4435e();
        this.f2498a.setVisibility(8);
        this.f2502f.addView(this.f2498a);
        this.f2498a.getSettings().setJavaScriptEnabled(true);
        this.f2501e = new C0554h(activity, frameLayout, this);
        this.f2498a.addJavascriptInterface(this.f2501e, "Android");
    }

    private void m4435e() {
        if (this.f2499c != null) {
            int identifier = this.f2499c.getResources().getIdentifier("is_dui_debuggable", "string", this.f2499c.getPackageName());
            if (identifier != 0) {
                String string = this.f2499c.getString(identifier);
                if (string != null && string.equalsIgnoreCase("true")) {
                    if (VERSION.SDK_INT >= 19) {
                        WebView.setWebContentsDebuggingEnabled(true);
                    }
                    this.f2498a.setWebChromeClient(new WebChromeClient());
                    this.f2498a.setWebViewClient(new WebViewClient());
                }
            }
            if (VERSION.SDK_INT >= 16) {
                this.f2498a.getSettings().setAllowFileAccessFromFileURLs(true);
                this.f2498a.getSettings().setAllowUniversalAccessFromFileURLs(true);
                return;
            }
            Log.e("DUI", "Will throw cors error if less than version < 16");
        }
    }

    public void m4436a() {
        this.f2498a.loadDataWithBaseURL("http://juspay.in", "<html></html>", "text/html", "utf-8", null);
        this.f2502f.removeView(this.f2498a);
        this.f2498a.removeAllViews();
        this.f2498a.destroy();
        this.f2499c = null;
    }

    public void m4438a(String str) {
        if (this.f2499c != null) {
            this.f2499c.runOnUiThread(new C05281(this, str));
        }
    }

    private String m4431a(Object obj) {
        String str = BuildConfig.FLAVOR;
        for (StackTraceElement stackTraceElement : ((Exception) obj).getStackTrace()) {
            str = str + stackTraceElement.toString() + "\n";
        }
        return str;
    }

    public void m4439b(String str) {
        if (this.f2499c != null) {
            this.f2499c.runOnUiThread(new C05292(this, str));
        }
    }

    @SuppressLint({"JavascriptInterface"})
    public void m4437a(Object obj, String str) {
        this.f2498a.addJavascriptInterface(obj, str);
    }

    public static C0525c m4432b() {
        return f2497b;
    }

    public C0531e m4440c() {
        return this.f2500d;
    }

    public void m4441c(String str) {
        if (this.f2501e != null) {
            this.f2501e.setState(str);
            return;
        }
        throw new Exception("JS-Interface not initailised");
    }

    public void m4442d(String str) {
        m4438a("window.onActivityLifeCycleEvent('" + str + "')");
    }
}
