package in.juspay.mystique;

import android.content.Context;
import android.os.Environment;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;

/* renamed from: in.juspay.mystique.f */
public class C0532f {
    private static final String f2503a;

    static {
        f2503a = C0532f.class.getSimpleName();
    }

    public static byte[] m4447a(Context context, String str) {
        byte[] b = C0532f.m4449b(context, str);
        if (b == null) {
            return C0532f.m4450c(context, str);
        }
        return b;
    }

    public static byte[] m4448a(String str, String str2) {
        return C0532f.m4444a(new ByteArrayOutputStream(), new FileInputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + str, str2))).toByteArray();
    }

    public static byte[] m4449b(Context context, String str) {
        return C0532f.m4444a(new ByteArrayOutputStream(), new FileInputStream(new File(context.getDir("juspay", 0), str))).toByteArray();
    }

    public static byte[] m4450c(Context context, String str) {
        return C0532f.m4444a(new ByteArrayOutputStream(), context.getAssets().open(str, 0)).toByteArray();
    }

    private static ByteArrayOutputStream m4444a(ByteArrayOutputStream byteArrayOutputStream, InputStream inputStream) {
        byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArrayOutputStream;
            }
        }
    }

    public static String m4445a(byte[] bArr) {
        String str = "MD5";
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(bArr);
        byte[] digest = instance.digest();
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : digest) {
            str = Integer.toHexString(b & 255);
            while (str.length() < 2) {
                str = "0" + str;
            }
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }

    public static void m4446a(Context context, String str, byte[] bArr) {
        Throwable th;
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(new File(context.getDir("juspay", 0), str));
            try {
                fileOutputStream.write(bArr);
                fileOutputStream.close();
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream.close();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            fileOutputStream.close();
            throw th;
        }
    }
}
