package in.juspay.mystique;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

/* renamed from: in.juspay.mystique.i */
public class C0555i {
    private static final String f2602a;

    static {
        f2602a = C0555i.class.getSimpleName();
    }

    public static Boolean m4491a(Context context, String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1);
        byte[] b = C0555i.m4492b(context, str);
        if (b == null) {
            return Boolean.valueOf(false);
        }
        C0532f.m4446a(context, substring, b);
        return Boolean.valueOf(true);
    }

    private static byte[] m4492b(Context context, String str) {
        Object obj = null;
        byte[] a = C0532f.m4447a(context, str.substring(str.lastIndexOf("/") + 1));
        if (a != null) {
            obj = C0532f.m4445a(a);
        }
        Map hashMap = new HashMap();
        hashMap.put("ts", String.valueOf(System.currentTimeMillis()));
        hashMap.put("If-None-Match", obj);
        return RestClient.m4422a(str, hashMap);
    }
}
