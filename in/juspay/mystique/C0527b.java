package in.juspay.mystique;

import com.crashlytics.android.core.BuildConfig;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/* renamed from: in.juspay.mystique.b */
public class C0527b implements InvocationHandler {
    private Object f2490a;
    private boolean f2491b;
    private C0530d f2492c;

    C0527b(Object obj, C0530d c0530d) {
        this.f2491b = true;
        this.f2490a = obj;
        this.f2492c = c0530d;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        Object obj2;
        Exception exception;
        Exception exception2;
        Object obj3 = null;
        try {
            String str;
            if (this.f2491b) {
                obj3 = method.invoke(this.f2490a, objArr);
            }
            if (obj3 == null) {
                try {
                    obj2 = "null";
                } catch (Exception e) {
                    exception = e;
                    obj2 = obj3;
                    exception2 = exception;
                    this.f2492c.m4440c().m4443a("InvocationHandler", exception2.getMessage() + "-" + (this.f2490a.getClass().getName() + "-" + method.getName()));
                    return obj2;
                }
            }
            obj2 = obj3;
            if (objArr != null) {
                try {
                    String str2 = "\"";
                    str = ",";
                    for (int i = 0; i < objArr.length; i++) {
                        if (i == objArr.length - 1) {
                            str = BuildConfig.FLAVOR;
                        }
                        str2 = str2 + objArr[i] + str;
                    }
                    str = str2 + "\"";
                } catch (Exception e2) {
                    exception2 = e2;
                    this.f2492c.m4440c().m4443a("InvocationHandler", exception2.getMessage() + "-" + (this.f2490a.getClass().getName() + "-" + method.getName()));
                    return obj2;
                }
            }
            str = "''";
            this.f2492c.m4438a("window.duiProxyCallback('" + obj2 + "','" + this.f2490a.getClass().getName() + "','" + method.getName() + "'," + str + ");");
        } catch (Exception e3) {
            exception = e3;
            obj2 = null;
            exception2 = exception;
            this.f2492c.m4440c().m4443a("InvocationHandler", exception2.getMessage() + "-" + (this.f2490a.getClass().getName() + "-" + method.getName()));
            return obj2;
        }
        return obj2;
    }
}
