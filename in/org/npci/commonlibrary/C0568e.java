package in.org.npci.commonlibrary;

import com.crashlytics.android.core.BuildConfig;
import in.org.npci.commonlibrary.p036a.C0563b;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.xml.security.Init;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

/* renamed from: in.org.npci.commonlibrary.e */
public class C0568e {
    private static List f3212a;
    private C0572i f3213b;
    private C0574k f3214c;
    private C0563b f3215d;
    private String f3216e;

    public C0568e(String str) {
        GeneralSecurityException e;
        this.f3216e = BuildConfig.FLAVOR;
        Init.m5713b();
        try {
            this.f3215d = new C0563b();
            if (this.f3215d.m5136a(str)) {
                System.out.println("XML Validated");
                this.f3214c = new C0574k(str);
                f3212a = this.f3214c.m5163a();
                try {
                    this.f3213b = new C0572i();
                    return;
                } catch (NoSuchAlgorithmException e2) {
                    e = e2;
                    e.printStackTrace();
                    throw new C0569f(C0570g.UNKNOWN_ERROR);
                } catch (NoSuchPaddingException e3) {
                    e = e3;
                    e.printStackTrace();
                    throw new C0569f(C0570g.UNKNOWN_ERROR);
                }
            }
            System.out.println("XML Not Validated");
            throw new C0569f(C0570g.KEYS_NOT_VALID);
        } catch (C0569f e4) {
            e4.printStackTrace();
            throw e4;
        } catch (Exception e5) {
            e5.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        }
    }

    private String m5145a(String str, String str2, String str3, String str4) {
        StringBuilder stringBuilder = new StringBuilder(500);
        try {
            stringBuilder.append(str2).append(CLConstants.SALT_DELIMETER).append(str).append(CLConstants.SALT_DELIMETER).append(C0564a.m5140b(this.f3213b.m5154a(this.f3213b.m5153a(str3), this.f3213b.m5155b(str4)), 2));
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        }
    }

    private byte[] m5146a(String str) {
        byte[] bytes = str.getBytes();
        byte[] bArr = null;
        try {
            Key b = m5147b(this.f3216e);
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            instance.init(1, b);
            bArr = instance.doFinal(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bArr;
    }

    private PublicKey m5147b(String str) {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(C0564a.m5138a(str.getBytes("utf-8"), 2)));
    }

    public Message m5148a(String str, String str2, String str3, String str4, String str5) {
        if (str == null || !str.isEmpty()) {
            C0573j c0573j;
            List arrayList = new ArrayList();
            for (C0573j c0573j2 : f3212a) {
                if (c0573j2.m5159b().equals(str)) {
                    arrayList.add(c0573j2);
                }
            }
            if (arrayList.size() == 0) {
                throw new C0569f(C0570g.KEY_CODE_INVALID);
            }
            c0573j2 = (C0573j) arrayList.get(new Random().nextInt(arrayList.size()));
            this.f3216e = c0573j2.m5161c();
            return new Message(BuildConfig.FLAVOR, BuildConfig.FLAVOR, new Data(c0573j2.m5157a(), c0573j2.m5159b(), C0564a.m5140b(m5146a(m5145a(str2, str3, str4, str5)), 2)));
        }
        throw new C0569f(C0570g.KEY_CODE_EMPTY);
    }

    public void m5149a(String str, String str2, String str3) {
        Exception e;
        try {
            C0572i c0572i = new C0572i();
            String b = C0564a.m5140b(c0572i.m5153a(str2), 2);
            String b2 = C0564a.m5140b(c0572i.m5156b(C0564a.m5137a(str, 2), c0572i.m5155b(str3)), 2);
            if (b2 != null && !b2.equalsIgnoreCase(b)) {
                throw new C0569f(C0570g.TRUST_NOT_VALID);
            }
        } catch (InvalidKeyException e2) {
            e = e2;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (NoSuchPaddingException e4) {
            e = e4;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (IllegalBlockSizeException e5) {
            e = e5;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (BadPaddingException e6) {
            e = e6;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (UnsupportedEncodingException e7) {
            e = e7;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (InvalidAlgorithmParameterException e8) {
            e = e8;
            e.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        } catch (Exception e9) {
            e9.printStackTrace();
            throw new C0569f(C0570g.UNKNOWN_ERROR);
        }
    }
}
