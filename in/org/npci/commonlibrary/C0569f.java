package in.org.npci.commonlibrary;

/* renamed from: in.org.npci.commonlibrary.f */
public class C0569f extends Exception {
    private int f3217a;
    private String f3218b;

    public C0569f(C0570g c0570g) {
        super(c0570g.m5150a());
        this.f3217a = c0570g.m5151b();
        this.f3218b = c0570g.m5150a();
    }

    public String toString() {
        return "Error " + this.f3217a + " : " + this.f3218b;
    }
}
