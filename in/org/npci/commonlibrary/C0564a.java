package in.org.npci.commonlibrary;

import java.io.UnsupportedEncodingException;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.org.npci.commonlibrary.a */
public class C0564a {
    static final /* synthetic */ boolean f3194a;

    static {
        f3194a = !C0564a.class.desiredAssertionStatus();
    }

    private C0564a() {
    }

    public static byte[] m5137a(String str, int i) {
        return C0564a.m5138a(str.getBytes(), i);
    }

    public static byte[] m5138a(byte[] bArr, int i) {
        return C0564a.m5139a(bArr, 0, bArr.length, i);
    }

    public static byte[] m5139a(byte[] bArr, int i, int i2, int i3) {
        C0566c c0566c = new C0566c(i3, new byte[((i2 * 3) / 4)]);
        if (!c0566c.m5143a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (c0566c.b == c0566c.a.length) {
            return c0566c.a;
        } else {
            Object obj = new byte[c0566c.b];
            System.arraycopy(c0566c.a, 0, obj, 0, c0566c.b);
            return obj;
        }
    }

    public static String m5140b(byte[] bArr, int i) {
        try {
            return new String(C0564a.m5142c(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] m5141b(byte[] bArr, int i, int i2, int i3) {
        C0567d c0567d = new C0567d(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!c0567d.f3206d) {
            switch (i2 % 3) {
                case R.View_android_theme /*0*/:
                    break;
                case R.View_android_focusable /*1*/:
                    i4 += 2;
                    break;
                case R.View_paddingStart /*2*/:
                    i4 += 3;
                    break;
                default:
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (c0567d.f3207e && i2 > 0) {
            i4 += (c0567d.f3208f ? 2 : 1) * (((i2 - 1) / 57) + 1);
        }
        c0567d.a = new byte[i4];
        c0567d.m5144a(bArr, i, i2, true);
        if (f3194a || c0567d.b == i4) {
            return c0567d.a;
        }
        throw new AssertionError();
    }

    public static byte[] m5142c(byte[] bArr, int i) {
        return C0564a.m5141b(bArr, 0, bArr.length, i);
    }
}
