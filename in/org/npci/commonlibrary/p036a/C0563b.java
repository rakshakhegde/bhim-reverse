package in.org.npci.commonlibrary.p036a;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/* renamed from: in.org.npci.commonlibrary.a.b */
public class C0563b {
    private Certificate f3193a;

    public C0563b() {
        try {
            this.f3193a = m5135b("signer.crt");
        } catch (CertificateException e) {
            System.out.println("Error in loading exception");
            e.printStackTrace();
        }
    }

    private Certificate m5135b(String str) {
        CertificateFactory instance = CertificateFactory.getInstance("X.509");
        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(str);
        InputStream bufferedInputStream = new BufferedInputStream(resourceAsStream);
        try {
            Certificate generateCertificate = instance.generateCertificate(bufferedInputStream);
            try {
                resourceAsStream.close();
            } catch (IOException e) {
            }
            return generateCertificate;
        } finally {
            try {
                bufferedInputStream.close();
            } catch (IOException e2) {
            }
            try {
                resourceAsStream.close();
            } catch (IOException e3) {
            }
        }
    }

    public boolean m5136a(String str) {
        boolean a;
        Exception e;
        if (this.f3193a == null) {
            try {
                this.f3193a = m5135b("signer.crt");
            } catch (CertificateException e2) {
                System.out.println("Error in loading certificate.");
                e2.printStackTrace();
                return false;
            }
        }
        try {
            a = C0562a.m5134a(C0562a.m5133a(str), this.f3193a.getPublicKey());
        } catch (SAXException e3) {
            e = e3;
            System.err.println("Parsing failed for message:" + str);
            e.printStackTrace();
            a = false;
        } catch (ParserConfigurationException e4) {
            e = e4;
            System.err.println("Parsing failed for message:" + str);
            e.printStackTrace();
            a = false;
        } catch (Exception e5) {
            e5.printStackTrace();
            a = false;
        }
        return a;
    }
}
