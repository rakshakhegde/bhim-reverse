package in.org.npci.commonlibrary;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/* renamed from: in.org.npci.commonlibrary.k */
class C0574k extends DefaultHandler {
    private static List f3236a;
    private static C0573j f3237b;
    private static String f3238c;

    static {
        f3236a = new ArrayList();
        f3237b = null;
        f3238c = null;
    }

    public C0574k(String str) {
        try {
            SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new StringReader(str)), this);
        } catch (ParserConfigurationException e) {
            throw new C0569f(C0570g.PARSER_MISCONFIG);
        } catch (SAXException e2) {
            throw new C0569f(C0570g.PARSER_MISCONFIG);
        } catch (IOException e3) {
            throw new C0569f(C0570g.PARSER_MISCONFIG);
        }
    }

    public List m5163a() {
        return f3236a;
    }

    public void characters(char[] cArr, int i, int i2) {
        f3238c = String.copyValueOf(cArr, i, i2).trim();
    }

    public void endElement(String str, String str2, String str3) {
        Object obj = -1;
        switch (str3.hashCode()) {
            case 106079:
                if (str3.equals("key")) {
                    obj = null;
                    break;
                }
                break;
            case 492250706:
                if (str3.equals("keyValue")) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case R.View_android_theme /*0*/:
                f3236a.add(f3237b);
            case R.View_android_focusable /*1*/:
                f3237b.m5162c(f3238c);
            default:
        }
    }

    protected void finalize() {
        System.out.println("KeyParser Destroyed");
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        Object obj = -1;
        switch (str3.hashCode()) {
            case 106079:
                if (str3.equals("key")) {
                    obj = null;
                    break;
                }
                break;
        }
        switch (obj) {
            case R.View_android_theme /*0*/:
                f3237b = new C0573j();
                f3237b.m5158a(attributes.getValue(CLConstants.FIELD_KI));
                f3237b.m5160b(attributes.getValue("owner"));
            default:
        }
    }
}
