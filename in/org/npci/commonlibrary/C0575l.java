package in.org.npci.commonlibrary;

import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/* renamed from: in.org.npci.commonlibrary.l */
public class C0575l {
    private static Certificate f3239a;

    static {
        try {
            f3239a = C0571h.m5152a("signer.crt");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    public static PublicKey m5164a() {
        return f3239a != null ? f3239a.getPublicKey() : null;
    }
}
