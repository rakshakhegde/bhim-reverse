package in.org.npci.upiapp.core;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.webkit.JavascriptInterface;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import in.juspay.tracker.Event;
import in.juspay.tracker.ExceptionTracker;
import in.juspay.tracker.GodelTracker;
import in.juspay.tracker.Properties;
import in.juspay.tracker.SessionInfo;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: in.org.npci.upiapp.core.b */
public class Tracker {
    private static Tracker f3367a;
    private String f3368b;
    private Context f3369c;
    private Activity f3370d;
    private int f3371e;
    private Properties f3372f;
    private SessionInfo f3373g;
    private boolean f3374h;
    private boolean f3375i;
    private final long f3376j;
    private boolean f3377k;

    /* renamed from: in.org.npci.upiapp.core.b.1 */
    class Tracker implements Runnable {
        final /* synthetic */ Tracker f3366a;

        Tracker(Tracker tracker) {
            this.f3366a = tracker;
        }

        public void run() {
            if (!this.f3366a.f3374h) {
                this.f3366a.initLoggingService();
            }
        }
    }

    public Tracker(Activity activity) {
        this.f3371e = 0;
        this.f3372f = Properties.m4574a();
        this.f3373g = SessionInfo.m4587a();
        this.f3374h = false;
        this.f3375i = false;
        this.f3376j = 10000;
        this.f3370d = activity;
        this.f3369c = activity.getApplicationContext();
        this.f3368b = this.f3370d.getResources().getString(2131165269);
        f3367a = this;
        m5228a();
    }

    private void m5228a() {
        new Handler().postDelayed(new Tracker(this), 10000);
    }

    @JavascriptInterface
    public void initLoggingService(boolean z) {
        if (!this.f3374h) {
            this.f3374h = true;
            m5231c();
            m5230b();
            this.f3377k = z;
        }
    }

    @JavascriptInterface
    public void initLoggingService() {
        initLoggingService(true);
    }

    private void m5230b() {
        GodelTracker.m4538a(GodelTracker.GodelTracker.OBFUSCATE);
        GodelTracker.m4537a().m4552a(this.f3370d);
        GodelTracker.m4537a().m4555a(this.f3369c.getResources().getString(R.app_name));
        GodelTracker.m4537a().m4560b("NPCI");
    }

    @JavascriptInterface
    public void startPushingLogs() {
        this.f3375i = true;
        GodelTracker.m4537a().m4563d();
    }

    private void m5231c() {
        this.f3373g.m4591a(this.f3370d.getResources().getString(2131165258));
        this.f3373g.m4595b(this.f3370d.getResources().getString(2131165270));
        this.f3373g.m4598c(this.f3370d.getResources().getString(2131165263));
        this.f3373g.m4601d(this.f3369c.getResources().getString(R.app_name));
        this.f3372f.m4575a(this.f3371e);
        this.f3372f.m4576a(this.f3368b);
    }

    @JavascriptInterface
    public void setRemoteVersion(String str) {
        this.f3373g.m4595b(this.f3370d.getResources().getString(2131165270));
    }

    @JavascriptInterface
    public void setLogLevel(String str) {
        try {
            this.f3371e = Integer.parseInt(str);
            this.f3372f.m4575a(this.f3371e);
        } catch (Throwable e) {
            GodelTracker.m4537a().m4554a(new ExceptionTracker().m4529a(e));
        }
    }

    @JavascriptInterface
    public void updateLoggingEndPoint(String str) {
        this.f3368b = str;
        this.f3372f.m4576a(this.f3368b);
    }

    @JavascriptInterface
    public void setLoggingRules(String str) {
        try {
            this.f3372f.m4581b(new JSONObject(str));
        } catch (Throwable e) {
            GodelTracker.m4537a().m4554a(new ExceptionTracker().m4529a(e));
        }
    }

    @JavascriptInterface
    public void setLogPushConfig(String str) {
        try {
            this.f3372f.m4578a(new JSONObject(str));
        } catch (Throwable e) {
            GodelTracker.m4537a().m4554a(new ExceptionTracker().m4529a(e));
        }
    }

    @JavascriptInterface
    public void setblackListedArray(String str) {
        try {
            this.f3372f.m4580b(new JSONArray(str));
        } catch (Throwable e) {
            GodelTracker.m4537a().m4554a(new ExceptionTracker().m4529a(e));
        }
    }

    @JavascriptInterface
    public void setWhiteListedArray(String str) {
        try {
            this.f3372f.m4577a(new JSONArray(str));
        } catch (Throwable e) {
            GodelTracker.m4537a().m4554a(new ExceptionTracker().m4529a(e));
        }
    }

    @JavascriptInterface
    public void trackEvent(String str, String str2) {
        Event event = new Event();
        event.m4525b(str);
        event.m4527c(str2);
        if (this.f3375i) {
            GodelTracker.m4537a().m4553a(event);
        }
        if (this.f3377k && str.equalsIgnoreCase("SCREEN")) {
            fabricLogInfo(str, str2);
        }
    }

    @JavascriptInterface
    public void trackInfo(String str) {
        if (this.f3375i) {
            GodelTracker.m4537a().m4561c(str);
        }
    }

    @JavascriptInterface
    public void trackError(String str) {
        if (this.f3375i) {
            GodelTracker.m4537a().m4556a(new Date(), str);
        }
    }

    @JavascriptInterface
    public void trackSession() {
        if (this.f3375i) {
            GodelTracker.m4537a().m4557a(GodelTracker.m4537a().m4564f());
        }
    }

    @JavascriptInterface
    public void fabricLogInfo(String str, String str2) {
        Answers.getInstance().logCustom((CustomEvent) new CustomEvent(str2.toUpperCase()).putCustomAttribute(str, str2));
    }
}
