package in.org.npci.upiapp.core;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.ContactsContract.Profile;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.ActivityCompat;
import android.support.v4.p004a.ContextCompat;
import android.support.v4.p006f.p012b.FastOutSlowInInterpolator;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.common.C0100b;
import in.juspay.mystique.C0530d;
import in.juspay.tracker.GodelTracker;
import in.org.npci.upiapp.HomeActivity;
import in.org.npci.upiapp.gcm.RegistrationIntentService;
import in.org.npci.upiapp.p037a.Logger;
import in.org.npci.upiapp.utils.CryptoUtils;
import in.org.npci.upiapp.utils.FileUtil;
import in.org.npci.upiapp.utils.KeyValueStore;
import in.org.npci.upiapp.utils.RemoteAssetService;
import in.org.npci.upiapp.utils.RestClient;
import in.org.npci.upiapp.utils.Utils;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class JsInterface {
    private static final String f3307b;
    private static String f3308k;
    private KeyValueStore f3309a;
    private Context f3310c;
    private Activity f3311d;
    private C0530d f3312e;
    private BroadcastReceiver f3313f;
    private BroadcastReceiver f3314g;
    private final int f3315h;
    private String f3316i;
    private DatePickerDialog f3317j;
    private final String[] f3318l;

    /* renamed from: in.org.npci.upiapp.core.JsInterface.10 */
    class AnonymousClass10 extends BroadcastReceiver {
        final /* synthetic */ String f3252a;
        final /* synthetic */ JsInterface f3253b;

        AnonymousClass10(JsInterface jsInterface, String str) {
            this.f3253b = jsInterface;
            this.f3252a = str;
        }

        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case -1:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"SUCCESS\")");
                case R.View_android_focusable /*1*/:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"Sms Sending Failed\", \"837\")");
                case R.View_paddingStart /*2*/:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"Sms Sending Failed. Airplane Mode ON\", \"840\")");
                case R.View_paddingEnd /*3*/:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"Sms Sending Failed\", \"839\")");
                case R.View_theme /*4*/:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"Sms Sending Failed. No service\", \"838\")");
                default:
                    this.f3253b.f3312e.m4438a("window.callUICallback(\"" + this.f3252a + "\", \"Sms Sending Failed\", \"837\")");
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.12 */
    class AnonymousClass12 implements Runnable {
        final /* synthetic */ String[] f3255a;
        final /* synthetic */ JsInterface f3256b;

        AnonymousClass12(JsInterface jsInterface, String[] strArr) {
            this.f3256b = jsInterface;
            this.f3255a = strArr;
        }

        public void run() {
            JSONObject jSONObject = new JSONObject();
            List arrayList = new ArrayList();
            String[] strArr = this.f3255a;
            int length = strArr.length;
            int i = 0;
            int i2 = 1;
            while (i < length) {
                int i3;
                String str = strArr[i];
                if (ContextCompat.m76a(this.f3256b.f3311d, str) != 0) {
                    arrayList.add(str);
                    i3 = 0;
                } else {
                    try {
                        jSONObject.put(str, "GRANTED");
                        i3 = i2;
                    } catch (Exception e) {
                        i3 = i2;
                    }
                }
                i++;
                i2 = i3;
            }
            String[] strArr2 = (String[]) arrayList.toArray(new String[arrayList.size()]);
            if (i2 != 0) {
                this.f3256b.f3312e.m4438a("window.callUICallback(\"" + this.f3256b.f3316i + "\", " + jSONObject.toString() + ");");
            }
            if (arrayList.size() != 0) {
                ActivityCompat.m206a(this.f3256b.f3311d, strArr2, 1);
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.13 */
    class AnonymousClass13 extends AsyncTask {
        final /* synthetic */ String f3257a;
        final /* synthetic */ String f3258b;
        final /* synthetic */ String f3259c;
        final /* synthetic */ String f3260d;
        final /* synthetic */ String f3261e;
        final /* synthetic */ JsInterface f3262f;

        AnonymousClass13(JsInterface jsInterface, String str, String str2, String str3, String str4, String str5) {
            this.f3262f = jsInterface;
            this.f3257a = str;
            this.f3258b = str2;
            this.f3259c = str3;
            this.f3260d = str4;
            this.f3261e = str5;
        }

        protected void onPostExecute(Object obj) {
            if (obj != null) {
                Logger.m5184a(JsInterface.f3307b, "Response of API: " + obj);
                String encodeToString;
                if (((String) obj).startsWith("ERR:")) {
                    Logger.m5184a(JsInterface.f3307b, "Response Error: " + this.f3257a);
                    encodeToString = Base64.encodeToString(((String) obj).getBytes(), 2);
                    encodeToString = String.format("window.callJSCallback('%s','%s','%s');", new Object[]{this.f3257a, "success", encodeToString});
                    Logger.m5184a(JsInterface.f3307b, encodeToString);
                    this.f3262f.f3312e.m4438a(encodeToString);
                    return;
                }
                Logger.m5184a(JsInterface.f3307b, "Response Success: " + this.f3257a);
                encodeToString = Base64.encodeToString(((String) obj).getBytes(), 2);
                encodeToString = String.format("window.callJSCallback('%s','%s','%s');", new Object[]{this.f3257a, "success", encodeToString});
                Logger.m5184a(JsInterface.f3307b, encodeToString);
                this.f3262f.f3312e.m4438a(encodeToString);
            }
        }

        protected Object doInBackground(Object[] objArr) {
            Logger.m5184a(JsInterface.f3307b, "Now calling API :" + this.f3258b);
            Logger.m5184a(JsInterface.f3307b, "API Request parameter : URL ->" + this.f3258b + "- Method -> " + this.f3259c);
            Logger.m5184a(JsInterface.f3307b, "API Request parameter : Data ->" + this.f3260d + "- Header -> " + this.f3261e);
            HashMap hashMap = new HashMap();
            try {
                JSONObject jSONObject = new JSONObject(this.f3261e);
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap.put(str, jSONObject.getString(str));
                }
                Logger.m5184a(JsInterface.f3307b, "API Request Header : Header -> " + hashMap);
                if (this.f3259c.equalsIgnoreCase("POST")) {
                    Logger.m5184a(JsInterface.f3307b, "METHOD - " + this.f3259c);
                    Logger.m5184a(JsInterface.f3307b, "URL - " + this.f3258b);
                    Logger.m5184a(JsInterface.f3307b, "REQUEST-BODY - " + this.f3260d);
                    Logger.m5184a(JsInterface.f3307b, "HEADER - " + hashMap);
                    return new String(RestClient.m5248a(this.f3258b, this.f3260d, hashMap));
                } else if (this.f3259c.equalsIgnoreCase("GET")) {
                    Logger.m5184a(JsInterface.f3307b, "METHOD:  - " + this.f3259c);
                    Logger.m5184a(JsInterface.f3307b, "URL - " + this.f3258b);
                    return new String(RestClient.m5250a(this.f3258b, new HashMap(), hashMap));
                } else if (!this.f3259c.equalsIgnoreCase("DELETE")) {
                    return null;
                } else {
                    Logger.m5184a(JsInterface.f3307b, "METHOD:  - " + this.f3259c);
                    Logger.m5184a(JsInterface.f3307b, "URL - " + this.f3258b);
                    return new String(RestClient.m5250a(this.f3258b, new HashMap(), hashMap));
                }
            } catch (Throwable e) {
                e.printStackTrace();
                Logger.m5187a(e);
                return "ERR: " + e.getLocalizedMessage();
            } catch (Exception e2) {
                return "ERR: " + e2.getLocalizedMessage();
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.14 */
    class AnonymousClass14 implements Runnable {
        final /* synthetic */ String f3263a;
        final /* synthetic */ String f3264b;
        final /* synthetic */ boolean f3265c;
        final /* synthetic */ String f3266d;
        final /* synthetic */ JsInterface f3267e;

        AnonymousClass14(JsInterface jsInterface, String str, String str2, boolean z, String str3) {
            this.f3267e = jsInterface;
            this.f3263a = str;
            this.f3264b = str2;
            this.f3265c = z;
            this.f3266d = str3;
        }

        public void run() {
            if (this.f3267e.f3311d instanceof HomeActivity) {
                try {
                    ((ImageView) this.f3267e.f3311d.findViewById(Integer.parseInt(this.f3266d))).setImageBitmap(((HomeActivity) this.f3267e.f3311d).m5180a(this.f3263a, this.f3264b, this.f3265c));
                } catch (Throwable e) {
                    Logger.m5185a(JsInterface.f3307b, "Exception showing QR Code", e);
                }
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.15 */
    class AnonymousClass15 implements Runnable {
        final /* synthetic */ String f3268a;
        final /* synthetic */ boolean f3269b;
        final /* synthetic */ String f3270c;
        final /* synthetic */ String f3271d;
        final /* synthetic */ String f3272e;
        final /* synthetic */ JsInterface f3273f;

        AnonymousClass15(JsInterface jsInterface, String str, boolean z, String str2, String str3, String str4) {
            this.f3273f = jsInterface;
            this.f3268a = str;
            this.f3269b = z;
            this.f3270c = str2;
            this.f3271d = str3;
            this.f3272e = str4;
        }

        public void run() {
            if (this.f3273f.f3311d instanceof HomeActivity) {
                File saveToExternalStorage;
                HomeActivity homeActivity = (HomeActivity) this.f3273f.f3311d;
                File externalFilePath = this.f3273f.getExternalFilePath(this.f3268a);
                if (this.f3269b || !externalFilePath.exists()) {
                    homeActivity.m5180a(this.f3268a, this.f3270c, true);
                    saveToExternalStorage = this.f3273f.saveToExternalStorage(this.f3268a, this.f3273f.getInternalFilePath(this.f3268a).getAbsolutePath());
                } else {
                    saveToExternalStorage = externalFilePath;
                }
                Parcelable fromFile = Uri.fromFile(saveToExternalStorage);
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("image/png");
                intent.putExtra("android.intent.extra.STREAM", fromFile);
                intent.putExtra("android.intent.extra.SUBJECT", this.f3271d);
                intent.putExtra("android.intent.extra.TEXT", this.f3272e);
                this.f3273f.f3311d.startActivity(Intent.createChooser(intent, "Share via"));
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.16 */
    class AnonymousClass16 implements Runnable {
        final /* synthetic */ String f3274a;
        final /* synthetic */ String[] f3275b;
        final /* synthetic */ String f3276c;
        final /* synthetic */ String f3277d;
        final /* synthetic */ JsInterface f3278e;

        AnonymousClass16(JsInterface jsInterface, String str, String[] strArr, String str2, String str3) {
            this.f3278e = jsInterface;
            this.f3274a = str;
            this.f3275b = strArr;
            this.f3276c = str2;
            this.f3277d = str3;
        }

        public void run() {
            try {
                LinearLayout linearLayout = (LinearLayout) this.f3278e.f3311d.findViewById(Integer.parseInt(this.f3274a));
                linearLayout.setDrawingCacheEnabled(true);
                linearLayout.buildDrawingCache();
                this.f3275b[0] = Media.insertImage(this.f3278e.f3311d.getContentResolver(), linearLayout.getDrawingCache(), this.f3276c, BuildConfig.FLAVOR);
                this.f3278e.f3312e.m4438a(String.format("window.callJSCallback('%s','%s');", new Object[]{this.f3277d, BuildConfig.FLAVOR + this.f3275b[0]}));
            } catch (Throwable e) {
                Logger.m5185a(JsInterface.f3307b, "Exception", e);
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.1 */
    class C05781 implements Runnable {
        final /* synthetic */ JsInterface f3279a;

        C05781(JsInterface jsInterface) {
            this.f3279a = jsInterface;
        }

        public void run() {
            try {
                if (this.f3279a.m5195b()) {
                    this.f3279a.f3311d.startService(new Intent(this.f3279a.f3311d, RegistrationIntentService.class));
                }
            } catch (Throwable e) {
                Logger.m5185a(JsInterface.f3307b, "Exception while calling GCM Token", e);
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.2 */
    class C05792 implements Runnable {
        final /* synthetic */ String f3280a;
        final /* synthetic */ JsInterface f3281b;

        C05792(JsInterface jsInterface, String str) {
            this.f3281b = jsInterface;
            this.f3280a = str;
        }

        public void run() {
            try {
                Integer valueOf = Integer.valueOf(this.f3280a);
                View findViewById = this.f3281b.f3311d.findViewById(valueOf.intValue());
                TypedValue typedValue = new TypedValue();
                this.f3281b.f3311d.getTheme().resolveAttribute(R.selectableItemBackground, typedValue, true);
                if (findViewById != null) {
                    findViewById.setBackgroundResource(typedValue.resourceId);
                } else {
                    Logger.m5188b(JsInterface.f3307b, "Unable to find view with resID - " + this.f3280a + " : " + valueOf);
                }
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.3 */
    class C05803 implements Runnable {
        final /* synthetic */ String f3282a;
        final /* synthetic */ JsInterface f3283b;

        C05803(JsInterface jsInterface, String str) {
            this.f3283b = jsInterface;
            this.f3282a = str;
        }

        public void run() {
            try {
                Intent intent = new Intent("android.intent.action.CALL");
                intent.setData(Uri.parse("tel:" + this.f3282a));
                this.f3283b.f3311d.startActivity(intent);
            } catch (SecurityException e) {
                Logger.m5188b(JsInterface.f3307b, "No permission to open this");
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.4 */
    class C05814 implements OnDateSetListener {
        final /* synthetic */ Calendar f3284a;
        final /* synthetic */ String f3285b;
        final /* synthetic */ JsInterface f3286c;

        C05814(JsInterface jsInterface, Calendar calendar, String str) {
            this.f3286c = jsInterface;
            this.f3284a = calendar;
            this.f3285b = str;
        }

        public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
            this.f3284a.set(1, i);
            this.f3284a.set(2, i2);
            this.f3284a.set(5, i3);
            this.f3286c.f3312e.m4438a(String.format("window.callJSCallback('%s','%s');", new Object[]{this.f3285b, i + "/" + (i2 + 1) + "/" + i3}));
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.5 */
    class C05825 implements Runnable {
        final /* synthetic */ String f3287a;
        final /* synthetic */ float f3288b;
        final /* synthetic */ int f3289c;
        final /* synthetic */ JsInterface f3290d;

        C05825(JsInterface jsInterface, String str, float f, int i) {
            this.f3290d = jsInterface;
            this.f3287a = str;
            this.f3288b = f;
            this.f3289c = i;
        }

        public void run() {
            View findViewById = this.f3290d.f3311d.findViewById(Integer.parseInt(this.f3287a));
            PropertyValuesHolder ofKeyframe = PropertyValuesHolder.ofKeyframe(View.SCALE_X, new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(0.1f, 0.9f), Keyframe.ofFloat(0.2f, 0.9f), Keyframe.ofFloat(0.3f, 1.1f), Keyframe.ofFloat(0.4f, 1.1f), Keyframe.ofFloat(0.5f, 1.1f), Keyframe.ofFloat(0.6f, 1.1f), Keyframe.ofFloat(0.7f, 1.1f), Keyframe.ofFloat(0.8f, 1.1f), Keyframe.ofFloat(0.9f, 1.1f), Keyframe.ofFloat(1.0f, 1.0f)});
            PropertyValuesHolder ofKeyframe2 = PropertyValuesHolder.ofKeyframe(View.SCALE_Y, new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(0.1f, 0.9f), Keyframe.ofFloat(0.2f, 0.9f), Keyframe.ofFloat(0.3f, 1.1f), Keyframe.ofFloat(0.4f, 1.1f), Keyframe.ofFloat(0.5f, 1.1f), Keyframe.ofFloat(0.6f, 1.1f), Keyframe.ofFloat(0.7f, 1.1f), Keyframe.ofFloat(0.8f, 1.1f), Keyframe.ofFloat(0.9f, 1.1f), Keyframe.ofFloat(1.0f, 1.0f)});
            PropertyValuesHolder ofKeyframe3 = PropertyValuesHolder.ofKeyframe(View.ROTATION, new Keyframe[]{Keyframe.ofFloat(0.0f, 0.0f), Keyframe.ofFloat(0.1f, this.f3288b * -3.0f), Keyframe.ofFloat(0.2f, this.f3288b * -3.0f), Keyframe.ofFloat(0.3f, 3.0f * this.f3288b), Keyframe.ofFloat(0.4f, this.f3288b * -3.0f), Keyframe.ofFloat(0.5f, 3.0f * this.f3288b), Keyframe.ofFloat(0.6f, this.f3288b * -3.0f), Keyframe.ofFloat(0.7f, 3.0f * this.f3288b), Keyframe.ofFloat(0.8f, this.f3288b * -3.0f), Keyframe.ofFloat(0.9f, 3.0f * this.f3288b), Keyframe.ofFloat(1.0f, 0.0f)});
            ObjectAnimator.ofPropertyValuesHolder(findViewById, new PropertyValuesHolder[]{ofKeyframe, ofKeyframe2, ofKeyframe3}).setDuration((long) this.f3289c).start();
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.6 */
    class C05836 implements Runnable {
        final /* synthetic */ String f3291a;
        final /* synthetic */ int f3292b;
        final /* synthetic */ int f3293c;
        final /* synthetic */ JsInterface f3294d;

        C05836(JsInterface jsInterface, String str, int i, int i2) {
            this.f3294d = jsInterface;
            this.f3291a = str;
            this.f3292b = i;
            this.f3293c = i2;
        }

        public void run() {
            View findViewById = this.f3294d.f3311d.findViewById(Integer.parseInt(this.f3291a));
            int a = this.f3294d.m5198a((float) this.f3292b);
            PropertyValuesHolder ofKeyframe = PropertyValuesHolder.ofKeyframe(View.TRANSLATION_X, new Keyframe[]{Keyframe.ofFloat(0.0f, 0.0f), Keyframe.ofFloat(0.1f, (float) (-a)), Keyframe.ofFloat(0.26f, (float) a), Keyframe.ofFloat(0.42f, (float) (-a)), Keyframe.ofFloat(0.58f, (float) a), Keyframe.ofFloat(0.74f, (float) (-a)), Keyframe.ofFloat(0.9f, (float) a), Keyframe.ofFloat(1.0f, 0.0f)});
            ObjectAnimator.ofPropertyValuesHolder(findViewById, new PropertyValuesHolder[]{ofKeyframe}).setDuration((long) this.f3293c).start();
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.7 */
    class C05847 implements Runnable {
        final /* synthetic */ String f3295a;
        final /* synthetic */ float f3296b;
        final /* synthetic */ int f3297c;
        final /* synthetic */ JsInterface f3298d;

        C05847(JsInterface jsInterface, String str, float f, int i) {
            this.f3298d = jsInterface;
            this.f3295a = str;
            this.f3296b = f;
            this.f3297c = i;
        }

        public void run() {
            View findViewById = this.f3298d.f3311d.findViewById(Integer.parseInt(this.f3295a));
            r1 = new PropertyValuesHolder[2];
            r1[0] = PropertyValuesHolder.ofFloat("scaleX", new float[]{this.f3296b});
            r1[1] = PropertyValuesHolder.ofFloat("scaleY", new float[]{this.f3296b});
            ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(findViewById, r1);
            ofPropertyValuesHolder.setDuration((long) this.f3297c);
            ofPropertyValuesHolder.setRepeatCount(-1);
            ofPropertyValuesHolder.setRepeatMode(2);
            ofPropertyValuesHolder.setInterpolator(new FastOutSlowInInterpolator());
            ofPropertyValuesHolder.start();
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.8 */
    class C05858 implements Runnable {
        final /* synthetic */ String f3299a;
        final /* synthetic */ String f3300b;
        final /* synthetic */ String f3301c;
        final /* synthetic */ JsInterface f3302d;

        C05858(JsInterface jsInterface, String str, String str2, String str3) {
            this.f3302d = jsInterface;
            this.f3299a = str;
            this.f3300b = str2;
            this.f3301c = str3;
        }

        public void run() {
            try {
                ((HomeActivity) this.f3302d.f3311d).m5182a(this.f3299a, this.f3300b, this.f3301c);
            } catch (SecurityException e) {
                this.f3302d.f3312e.m4438a("window.callUICallback(\"" + this.f3301c + "\", \"FAIlURE\")");
                Logger.m5188b(JsInterface.f3307b, "No permission to open this");
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.JsInterface.9 */
    class C05869 extends AsyncTask {
        final /* synthetic */ String f3303a;
        final /* synthetic */ boolean f3304b;
        final /* synthetic */ String f3305c;
        final /* synthetic */ JsInterface f3306d;

        C05869(JsInterface jsInterface, String str, boolean z, String str2) {
            this.f3306d = jsInterface;
            this.f3303a = str;
            this.f3304b = z;
            this.f3305c = str2;
        }

        protected Object doInBackground(Object[] objArr) {
            Boolean.valueOf(false);
            try {
                Boolean a = RemoteAssetService.m5272a(this.f3306d.f3311d.getApplicationContext(), this.f3303a, this.f3304b);
                Logger.m5184a(JsInterface.f3307b, a.toString() + " " + this.f3303a);
                return a.toString();
            } catch (Exception e) {
                Logger.m5184a(JsInterface.f3307b, e.getMessage());
                return "FAILURE";
            }
        }

        protected void onPostExecute(Object obj) {
            if (this.f3305c != null) {
                this.f3306d.f3312e.m4438a("window." + this.f3305c + "(\"" + ((String) obj) + "\", \"" + this.f3303a + "\");");
            }
        }
    }

    static {
        f3307b = JsInterface.class.getName();
    }

    public JsInterface(Activity activity, C0530d c0530d) {
        this.f3315h = 1;
        this.f3310c = activity.getApplicationContext();
        this.f3311d = activity;
        this.f3312e = c0530d;
        this.f3309a = new KeyValueStore(this.f3310c);
        this.f3318l = new String[]{"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
    }

    @JavascriptInterface
    public String getIntentData() {
        JSONObject jSONObject = new JSONObject();
        if (!(this.f3311d == null || this.f3311d.getIntent() == null)) {
            String stringExtra = this.f3311d.getIntent().getStringExtra("notification");
            if (stringExtra != null) {
                try {
                    jSONObject.put(CLConstants.FIELD_TYPE, "GCM");
                    jSONObject.put(CLConstants.FIELD_DATA, stringExtra);
                } catch (Throwable e) {
                    Logger.m5185a(f3307b, "Exception", e);
                }
            } else {
                stringExtra = ((HomeActivity) this.f3311d).m5183k();
                if (stringExtra != null) {
                    jSONObject.put(CLConstants.FIELD_TYPE, "MERCHANT");
                    jSONObject.put(CLConstants.FIELD_DATA, stringExtra);
                }
            }
        }
        return jSONObject.toString();
    }

    @JavascriptInterface
    public String getDeviceId() {
        try {
            return Utils.m5275a(this.f3311d);
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "Error getting deviceId", e);
            return null;
        }
    }

    @JavascriptInterface
    public String getPackageName() {
        try {
            return this.f3311d.getPackageName();
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "Error getting package name", e);
            return null;
        }
    }

    @JavascriptInterface
    public String loadFile(String str) {
        try {
            byte[] a = FileUtil.m5263a(this.f3311d, str);
            if (a == null) {
                return BuildConfig.FLAVOR;
            }
            return new String(a);
        } catch (Exception e) {
            return BuildConfig.FLAVOR;
        }
    }

    private byte[] m5192a(byte[] bArr) {
        byte[] bArr2 = new byte[1024];
        try {
            InputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream, 1024);
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    bArr2 = byteArrayOutputStream.toByteArray();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    byteArrayOutputStream.close();
                    return bArr2;
                }
            }
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "Exception while gunzipping - ", e);
            return null;
        }
    }

    private String m5194b(byte[] bArr) {
        int i = 0;
        try {
            r4 = new byte[8];
            byte[] bArr2 = new byte[(bArr.length - 8)];
            int length = bArr.length;
            r4[0] = bArr[9];
            r4[1] = bArr[19];
            r4[2] = bArr[29];
            r4[3] = bArr[39];
            r4[4] = bArr[49];
            r4[5] = bArr[59];
            r4[6] = bArr[69];
            r4[7] = bArr[79];
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                if (i2 <= 0 || i2 % 10 != 9 || i3 >= 8) {
                    bArr2[i] = (byte) (bArr[i2] ^ r4[i % 8]);
                    i++;
                } else {
                    i3++;
                }
                i2++;
            }
            return new String(m5192a(bArr2));
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "Exception while decrypting - ", e);
            return null;
        }
    }

    @JavascriptInterface
    public String decryptAndloadFile(String str) {
        Logger.m5184a(f3307b, "Processing File - " + str);
        try {
            byte[] a = FileUtil.m5264a(this.f3311d, str, "galileo");
            if (!str.endsWith(".jsa") || a == null) {
                return null;
            }
            try {
                return new String(m5194b(a));
            } catch (Exception e) {
                return BuildConfig.FLAVOR;
            }
        } catch (Exception e2) {
            return BuildConfig.FLAVOR;
        }
    }

    @JavascriptInterface
    public void getGCMToken() {
        this.f3311d.runOnUiThread(new C05781(this));
    }

    private boolean m5195b() {
        C0100b a = C0100b.m3073a();
        int a2 = a.m3075a(this.f3311d);
        if (a2 == 0) {
            return true;
        }
        if (a.m3080a(a2)) {
            a.m3076a(this.f3311d, a2, 9000).show();
        } else {
            Log.e(f3307b, "This device is not supported for GCM");
        }
        return false;
    }

    @JavascriptInterface
    public void downloadFile(String str, boolean z, String str2) {
        Logger.m5184a(f3307b, "Download File - " + str);
        new C05869(this, str, z, str2).execute(new Object[0]);
    }

    @JavascriptInterface
    public boolean isDualSIM() {
        return SimUtil.m5224a(this.f3311d);
    }

    @JavascriptInterface
    public String getDeviceDetails() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(CLConstants.SALT_FIELD_DEVICE_ID, Utils.m5275a(this.f3311d));
            jSONObject.put("os", "ANDROID");
            jSONObject.put("version", VERSION.RELEASE);
            jSONObject.put("manufacturer", Build.MANUFACTURER);
            jSONObject.put("model", Build.MODEL);
            jSONObject.put("packageName", this.f3311d.getPackageName());
        } catch (Throwable e) {
            Logger.m5187a(e);
        }
        return jSONObject.toString();
    }

    @JavascriptInterface
    public int AndroidVersion() {
        return VERSION.SDK_INT;
    }

    @JavascriptInterface
    public String getsymbols(String str) {
        Object obj = -1;
        switch (str.hashCode()) {
            case 3559837:
                if (str.equals("tick")) {
                    obj = null;
                    break;
                }
                break;
            case 108877805:
                if (str.equals("rupee")) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case R.View_android_theme /*0*/:
                return "\u2713";
            case R.View_android_focusable /*1*/:
                return "\u20b9";
            default:
                return "symbol";
        }
    }

    @JavascriptInterface
    public String getMobileNumber() {
        JSONObject jSONObject = new JSONObject();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f3311d.getSystemService("phone");
            Formatter.formatIpAddress(((WifiManager) this.f3311d.getSystemService("wifi")).getConnectionInfo().getIpAddress());
            jSONObject.put("sim", telephonyManager.getLine1Number());
        } catch (Throwable e) {
            Logger.m5187a(e);
        }
        return jSONObject.toString();
    }

    @JavascriptInterface
    public void restart() {
        Intent launchIntentForPackage = this.f3311d.getBaseContext().getPackageManager().getLaunchIntentForPackage(this.f3311d.getBaseContext().getPackageName());
        launchIntentForPackage.addFlags(67108864);
        this.f3311d.startActivity(launchIntentForPackage);
    }

    @SuppressLint({"NewApi"})
    public void m5200a(int i, String[] strArr, int[] iArr) {
        Logger.m5184a(f3307b, "Permission Granted - " + String.valueOf(i));
        if (this.f3312e == null) {
            Logger.m5184a(f3307b, "DUI is null");
            return;
        }
        switch (i) {
            case R.View_android_focusable /*1*/:
                try {
                    JSONObject jSONObject = new JSONObject();
                    for (int i2 = 0; i2 < iArr.length; i2++) {
                        if (iArr[i2] != -1) {
                            jSONObject.put(strArr[i2], "GRANTED");
                        } else if (this.f3311d.shouldShowRequestPermissionRationale(strArr[i2])) {
                            jSONObject.put(strArr[i2], "DENIED");
                        } else {
                            jSONObject.put(strArr[i2], "EVERDENIED");
                        }
                    }
                    this.f3312e.m4438a("window.callUICallback(\"" + this.f3316i + "\", " + jSONObject.toString() + ");");
                } catch (Exception e) {
                    this.f3312e.m4438a("window.callUICallback(\"" + this.f3316i + "\", \"ERROR\");");
                }
            default:
        }
    }

    @JavascriptInterface
    public void sendSMS(String str, String str2, String str3, String str4) {
        this.f3313f = new AnonymousClass10(this, str4);
        this.f3314g = new BroadcastReceiver() {
            final /* synthetic */ JsInterface f3254a;

            {
                this.f3254a = r1;
            }

            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                }
            }
        };
        PendingIntent broadcast = PendingIntent.getBroadcast(this.f3311d, 0, new Intent("SMS_SENT"), 0);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(this.f3311d, 0, new Intent("SMS_DELIVERED"), 0);
        this.f3311d.registerReceiver(this.f3313f, new IntentFilter("SMS_SENT"));
        this.f3311d.registerReceiver(this.f3314g, new IntentFilter("SMS_DELIVERED"));
        SimUtil.m5225a(this.f3311d, Integer.parseInt(str), str2, null, str3, broadcast, broadcast2);
    }

    @JavascriptInterface
    public void unregisterReceiver() {
        if (this.f3313f != null && this.f3314g != null) {
            this.f3311d.unregisterReceiver(this.f3313f);
            this.f3311d.unregisterReceiver(this.f3314g);
        }
    }

    @JavascriptInterface
    public void showKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.f3311d.getApplicationContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(1, 0);
        }
    }

    @JavascriptInterface
    public void hideKeyboard() {
        if (this.f3311d.getCurrentFocus() != null) {
            ((InputMethodManager) this.f3311d.getApplicationContext().getSystemService("input_method")).hideSoftInputFromWindow(this.f3311d.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @JavascriptInterface
    public String getKey(String str, String str2) {
        return this.f3309a.m5271b(str, str2);
    }

    @JavascriptInterface
    public void minimizeApp() {
        hideKeyboard();
        this.f3311d.moveTaskToBack(true);
    }

    @JavascriptInterface
    public boolean isPermissionGranted(String[] strArr) {
        boolean z = true;
        for (String a : strArr) {
            if (ContextCompat.m76a(this.f3311d, a) != 0) {
                z &= 0;
            } else {
                z &= 1;
            }
        }
        return z;
    }

    @JavascriptInterface
    public void checkPermission(String[] strArr, String str) {
        this.f3316i = str;
        if (VERSION.SDK_INT >= 23) {
            this.f3311d.runOnUiThread(new AnonymousClass12(this, strArr));
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("STATUS", "SUCCESS");
            this.f3312e.m4438a("window.callUICallback(\"" + this.f3316i + "\", " + jSONObject.toString() + ");");
        } catch (Exception e) {
            this.f3312e.m4438a("window.callUICallback(\"" + this.f3316i + "\", \"ERROR\");");
        }
    }

    @JavascriptInterface
    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f3311d.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @JavascriptInterface
    public boolean isSimSupport() {
        return SimUtil.m5226b(this.f3311d);
    }

    @JavascriptInterface
    public void setKey(String str, String str2) {
        this.f3309a.m5270a(str, str2);
    }

    @JavascriptInterface
    public void clearPreferences() {
        this.f3309a.m5269a(this.f3311d);
    }

    @JavascriptInterface
    public void closeApp() {
        this.f3311d.finish();
        System.exit(0);
    }

    @JavascriptInterface
    public String getResource(String str) {
        int identifier = this.f3311d.getResources().getIdentifier(str, "string", this.f3311d.getPackageName());
        if (identifier != 0) {
            return this.f3311d.getResources().getString(identifier);
        }
        return null;
    }

    @JavascriptInterface
    public String getResource(String str, String str2) {
        int identifier = this.f3311d.getResources().getIdentifier(str, str2, this.f3311d.getPackageName());
        if (identifier != 0) {
            return this.f3311d.getResources().getString(identifier);
        }
        return null;
    }

    @JavascriptInterface
    public void shoutOut(String str) {
        Logger.m5184a(f3307b, "Shoutout - " + str);
    }

    @JavascriptInterface
    public void logging(String str) {
        Logger.m5188b("Console", str);
    }

    @JavascriptInterface
    public String name() {
        String str = BuildConfig.FLAVOR;
        Cursor query = this.f3311d.getApplication().getContentResolver().query(Profile.CONTENT_URI, null, null, null, null);
        if (query == null) {
            return str;
        }
        query.moveToFirst();
        String string = query.getString(query.getColumnIndex("display_name"));
        query.close();
        return string;
    }

    @JavascriptInterface
    public String baseCheck(String str) {
        return Base64.encodeToString(str.getBytes(), 0);
    }

    @JavascriptInterface
    public void callAPI(String str, String str2, String str3, String str4, String str5) {
        new AnonymousClass13(this, str5, str2, str, str3, str4).execute(new Object[0]);
    }

    @JavascriptInterface
    public void sendMerchantResponseIntent(String str) {
        ((HomeActivity) this.f3311d).m5181a(str);
    }

    @JavascriptInterface
    public String getSIMOperators() {
        return SimUtil.m5227c(this.f3311d);
    }

    @JavascriptInterface
    public void showQrCode(String str, String str2, String str3, boolean z) {
        this.f3311d.runOnUiThread(new AnonymousClass14(this, str3, str2, z, str));
    }

    @JavascriptInterface
    public void deleteQrCode(String str) {
        removeInternalFilePath(str);
    }

    @JavascriptInterface
    public void shareQrCode(String str, String str2, String str3, String str4, boolean z) {
        this.f3311d.runOnUiThread(new AnonymousClass15(this, str2, z, str, str4, str3));
    }

    @JavascriptInterface
    public File getInternalFilePath(String str) {
        return new File(new ContextWrapper(this.f3311d).getDir("imageDir", 0), str);
    }

    @JavascriptInterface
    public File getExternalFilePath(String str) {
        new ContextWrapper(this.f3311d).getExternalFilesDir("imageDir");
        return new File((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + getAppName()) + str);
    }

    @JavascriptInterface
    public void removeExternalFilePath(String str) {
        new ContextWrapper(this.f3311d).getExternalFilesDir("imageDir");
        new File((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + getAppName()) + str).delete();
    }

    @JavascriptInterface
    public void removeInternalFilePath(String str) {
        new File(new ContextWrapper(this.f3311d).getDir("imageDir", 0), str).delete();
    }

    @JavascriptInterface
    public String getAppName() {
        return this.f3311d.getString(R.app_name);
    }

    @JavascriptInterface
    public File saveToExternalStorage(String str, String str2) {
        try {
            new ContextWrapper(this.f3311d).getExternalFilesDir("imageDir");
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + getAppName());
            if (!file.exists()) {
                file.mkdirs();
            }
            new File(file, ".nomedia").createNewFile();
            File file2 = new File(file, str);
            copyFile(file2.getAbsolutePath(), str2);
            return file2;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @JavascriptInterface
    public void copyFile(String str, String str2) {
        InputStream fileInputStream = new FileInputStream(str2);
        OutputStream fileOutputStream = new FileOutputStream(str);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read != -1) {
                fileOutputStream.write(bArr, 0, read);
            } else {
                fileInputStream.close();
                fileOutputStream.flush();
                fileOutputStream.close();
                return;
            }
        }
    }

    @JavascriptInterface
    public boolean isExternalStorageWritable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    @JavascriptInterface
    public boolean isExternalStorageReadable() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    @JavascriptInterface
    public String viewToImage(String str, String str2, String str3) {
        String[] strArr = new String[]{null};
        this.f3311d.runOnUiThread(new AnonymousClass16(this, str, strArr, str2, str3));
        return strArr[0];
    }

    @JavascriptInterface
    public boolean isDeviceRooted() {
        String str = Build.TAGS;
        if (str != null && str.contains("test-keys")) {
            return true;
        }
        boolean z = false;
        for (String str2 : this.f3318l) {
            z = new File(str2 + "/su").exists();
            if (z) {
                Logger.m5186a(f3307b, str2 + " contains su binary", false);
                return z;
            }
        }
        return z;
    }

    @JavascriptInterface
    public String getInstalledAccessiblityServices() {
        JSONArray jSONArray = new JSONArray();
        if (VERSION.SDK_INT >= 14) {
            for (AccessibilityServiceInfo accessibilityServiceInfo : ((AccessibilityManager) this.f3310c.getSystemService("accessibility")).getInstalledAccessibilityServiceList()) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("package", accessibilityServiceInfo.packageNames);
                    jSONObject.put(CLConstants.FIELD_PAY_INFO_NAME, accessibilityServiceInfo.getId());
                    jSONArray.put(jSONObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jSONArray.toString();
    }

    @JavascriptInterface
    public String getAppsWithDrawOverOthers() {
        PackageManager packageManager = this.f3310c.getPackageManager();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(CodedOutputStream.DEFAULT_BUFFER_SIZE);
        JSONArray jSONArray = new JSONArray();
        for (PackageInfo packageInfo : installedPackages) {
            if (packageInfo.requestedPermissions != null) {
                for (String str : packageInfo.requestedPermissions) {
                    if (str.equalsIgnoreCase("android.permission.SYSTEM_ALERT_WINDOW")) {
                        boolean z;
                        if (packageManager.checkPermission(str, packageInfo.packageName) == 0) {
                            z = true;
                        } else {
                            z = false;
                        }
                        try {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("package", packageInfo.packageName);
                            jSONObject.put(CLConstants.FIELD_PAY_INFO_NAME, getAppNameFromPackage(packageInfo.packageName));
                            jSONObject.put("targetSdkVersion", packageInfo.applicationInfo.targetSdkVersion);
                            jSONObject.put("available", z);
                            jSONArray.put(jSONObject);
                            Logger.m5184a(f3307b, "Permission matched for package = [" + packageInfo.packageName + "], [" + packageInfo.applicationInfo.targetSdkVersion + "], [" + z + "]");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return jSONArray.toString();
    }

    @JavascriptInterface
    public void openGallery(String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(str), "image/*");
        this.f3311d.startActivity(intent);
    }

    @JavascriptInterface
    public String getAppNameFromPackage(String str) {
        ApplicationInfo applicationInfo;
        PackageManager packageManager = this.f3310c.getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(str, 0);
        } catch (NameNotFoundException e) {
            applicationInfo = null;
        }
        return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "(unknown)");
    }

    @JavascriptInterface
    public void setClickFeedback(String str) {
        this.f3311d.runOnUiThread(new C05792(this, str));
    }

    @JavascriptInterface
    public void startInstalledAppDetailsActivity() {
        this.f3311d.startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:" + this.f3311d.getPackageName())));
    }

    @JavascriptInterface
    public void saveToClipboard(String str) {
        if (VERSION.SDK_INT < 11) {
            ((ClipboardManager) this.f3311d.getSystemService("clipboard")).setText(str);
        } else {
            ((android.content.ClipboardManager) this.f3311d.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("Copied Text", str));
        }
    }

    @JavascriptInterface
    public String getSessionId() {
        return GodelTracker.m4537a().m4559b();
    }

    @JavascriptInterface
    public String getSha256MessageDigest(String str) {
        byte[] a = CryptoUtils.m5255a(str);
        if (a != null) {
            return new String(a);
        }
        return str;
    }

    @JavascriptInterface
    public String getSha256MessageDigestInHex(String str) {
        return CryptoUtils.m5254a(CryptoUtils.m5255a(str));
    }

    @JavascriptInterface
    public void startCallIntent(String str) {
        this.f3311d.runOnUiThread(new C05803(this, str));
    }

    @JavascriptInterface
    public void startDialIntent(String str) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:" + str));
        this.f3311d.startActivity(intent);
    }

    @JavascriptInterface
    @TargetApi(11)
    public void calender(String str) {
        f3308k = str;
        Calendar instance = Calendar.getInstance();
        this.f3317j = new DatePickerDialog(this.f3311d, new C05814(this, instance, str), instance.get(1), instance.get(2), instance.get(5));
        this.f3317j.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        instance.add(5, 45);
        this.f3317j.getDatePicker().setMaxDate(instance.getTimeInMillis());
        this.f3317j.show();
    }

    @JavascriptInterface
    @TargetApi(14)
    public void tada(String str, float f, int i) {
        this.f3311d.runOnUiThread(new C05825(this, str, f, i));
    }

    @JavascriptInterface
    @TargetApi(14)
    public void nope(String str, int i, int i2) {
        this.f3311d.runOnUiThread(new C05836(this, str, i, i2));
    }

    @JavascriptInterface
    @TargetApi(11)
    public void pulse(String str, float f, int i) {
        this.f3311d.runOnUiThread(new C05847(this, str, f, i));
    }

    public int m5198a(float f) {
        return (int) (((float) (this.f3311d.getResources().getDisplayMetrics().densityDpi / 160)) * f);
    }

    @JavascriptInterface
    public void startBrowserIntent(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.f3311d.startActivity(intent);
    }

    @JavascriptInterface
    public String getMD5(String str) {
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(str.getBytes());
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                stringBuffer.append(Integer.toHexString((b & 255) | 256).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    @JavascriptInterface
    public String getHMACDigest(String str, String str2, String str3) {
        String str4 = null;
        try {
            Key secretKeySpec = new SecretKeySpec(str2.getBytes("UTF-8"), str3);
            Mac instance = Mac.getInstance(str3);
            instance.init(secretKeySpec);
            str4 = Base64.encodeToString(instance.doFinal(str.getBytes("ASCII")), 2);
        } catch (Throwable e) {
            Log.e(f3307b, "Error encoding hmac ", e);
        } catch (Throwable e2) {
            Log.e(f3307b, "Invalid HMAC key ", e2);
        } catch (Throwable e22) {
            Log.e(f3307b, "NoSuchAlgorithmException ", e22);
        }
        return str4;
    }

    @JavascriptInterface
    public static String encrypt(String str, String str2) {
        try {
            return CryptoUtils.m5253a(str, str2);
        } catch (Throwable e) {
            Log.e(f3307b, "exception while encrypting token", e);
            return BuildConfig.FLAVOR;
        }
    }

    @JavascriptInterface
    public static String decrypt(String str, String str2) {
        try {
            return CryptoUtils.m5257b(str, str2);
        } catch (Throwable e) {
            Log.e(f3307b, "exception while decrypting token", e);
            return BuildConfig.FLAVOR;
        }
    }

    public String m5199a(String str) {
        return str.split("\\?")[0].split("#")[0].replaceAll("[^a-zA-Z0-9.]", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    }

    @JavascriptInterface
    public String getFileFromInternalStorage(String str) {
        try {
            return new String(FileUtil.m5266b(m5199a(str), this.f3310c));
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "Exception while loading bitmap file", e);
            return null;
        } catch (Throwable e2) {
            Logger.m5185a(f3307b, "Exception while loading bitmap file", e2);
            return null;
        }
    }

    @JavascriptInterface
    public void saveFileToInternalStorage(String str, String str2) {
        if (str == null || str.length() == 0 || str2 == null) {
            Logger.m5184a(f3307b, "data missing. Not saving file");
            return;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(this.f3310c.getDir("juspay", 0), m5199a(str)));
            fileOutputStream.write(str2.getBytes());
            fileOutputStream.close();
        } catch (Throwable e) {
            Logger.m5185a(f3307b, "File not found ", e);
        } catch (Throwable e2) {
            Logger.m5185a(f3307b, "IO exception ", e2);
        }
    }

    @JavascriptInterface
    public void sendSmsIntent(String str, String str2, String str3) {
        this.f3311d.runOnUiThread(new C05858(this, str, str2, str3));
    }

    @JavascriptInterface
    public String getWebviewUserAgentString() {
        return HomeActivity.f3242m;
    }
}
