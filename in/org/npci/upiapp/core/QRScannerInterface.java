package in.org.npci.upiapp.core;

import android.app.Activity;
import android.support.v4.p004a.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import in.juspay.mystique.C0530d;
import in.juspay.widget.qrscanner.com.google.zxing.ResultPoint;
import in.juspay.widget.qrscanner.com.google.zxing.p026a.p027a.BeepManager;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.BarcodeCallback;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.BarcodeResult;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.CaptureManager;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.DecoratedBarcodeView;
import in.org.npci.upiapp.p037a.Logger;
import java.util.List;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class QRScannerInterface {
    private static final String f3360a;
    private Activity f3361b;
    private C0530d f3362c;
    private CaptureManager f3363d;
    private BeepManager f3364e;
    private String f3365f;

    /* renamed from: in.org.npci.upiapp.core.QRScannerInterface.1 */
    class C05941 implements Runnable {
        final /* synthetic */ QRScannerInterface f3354a;

        C05941(QRScannerInterface qRScannerInterface) {
            this.f3354a = qRScannerInterface;
        }

        public void run() {
            this.f3354a.f3363d.m5089c();
            this.f3354a.f3363d.m5090d();
            this.f3354a.f3363d = null;
        }
    }

    /* renamed from: in.org.npci.upiapp.core.QRScannerInterface.2 */
    class C05952 implements Runnable {
        final /* synthetic */ String f3355a;
        final /* synthetic */ QRScannerInterface f3356b;

        C05952(QRScannerInterface qRScannerInterface, String str) {
            this.f3356b = qRScannerInterface;
            this.f3355a = str;
        }

        public void run() {
            if (this.f3356b.f3363d != null && !TextUtils.isEmpty(this.f3355a)) {
                String str = this.f3355a;
                Object obj = -1;
                switch (str.hashCode()) {
                    case -1401315045:
                        if (str.equals("onDestroy")) {
                            obj = 2;
                            break;
                        }
                        break;
                    case -1340212393:
                        if (str.equals("onPause")) {
                            obj = 1;
                            break;
                        }
                        break;
                    case 1463983852:
                        if (str.equals("onResume")) {
                            obj = null;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case R.View_android_theme /*0*/:
                        this.f3356b.f3363d.m5088b();
                    case R.View_android_focusable /*1*/:
                        this.f3356b.f3363d.m5089c();
                    case R.View_paddingStart /*2*/:
                        this.f3356b.f3363d.m5090d();
                    default:
                }
            }
        }
    }

    /* renamed from: in.org.npci.upiapp.core.QRScannerInterface.3 */
    class C05973 implements Runnable {
        final /* synthetic */ int f3358a;
        final /* synthetic */ QRScannerInterface f3359b;

        /* renamed from: in.org.npci.upiapp.core.QRScannerInterface.3.1 */
        class C05961 implements BarcodeCallback {
            final /* synthetic */ C05973 f3357a;

            C05961(C05973 c05973) {
                this.f3357a = c05973;
            }

            public void m5211a(BarcodeResult barcodeResult) {
                Logger.m5184a(QRScannerInterface.f3360a, "Scanned QR Result: " + barcodeResult.toString());
                if (this.f3357a.f3359b.f3364e == null) {
                    this.f3357a.f3359b.f3364e = new BeepManager(this.f3357a.f3359b.f3361b);
                }
                this.f3357a.f3359b.f3364e.m4631a(true);
                this.f3357a.f3359b.f3364e.m4630a();
                if (this.f3357a.f3359b.f3362c != null) {
                    this.f3357a.f3359b.f3362c.m4438a("window.BarcodeResult(\"" + barcodeResult.toString() + "\");");
                }
            }

            public void m5212a(List<ResultPoint> list) {
            }
        }

        C05973(QRScannerInterface qRScannerInterface, int i) {
            this.f3359b = qRScannerInterface;
            this.f3358a = i;
        }

        public void run() {
            View decoratedBarcodeView = new DecoratedBarcodeView(this.f3359b.f3361b);
            decoratedBarcodeView.setLayoutParams(new LayoutParams(-1, -1));
            FrameLayout frameLayout = (FrameLayout) this.f3359b.f3361b.findViewById(this.f3358a);
            if (frameLayout == null) {
                Logger.m5188b(QRScannerInterface.f3360a, "Unable to find view with resID - " + this.f3359b.f3365f + " : " + this.f3358a);
                return;
            }
            frameLayout.addView(decoratedBarcodeView);
            this.f3359b.f3363d = new CaptureManager(this.f3359b.f3361b, decoratedBarcodeView);
            this.f3359b.f3363d.m5087a(new C05961(this));
            this.f3359b.f3363d.m5088b();
            this.f3359b.f3363d.m5086a();
        }
    }

    static {
        f3360a = QRScannerInterface.class.getSimpleName();
    }

    public QRScannerInterface(Activity activity, C0530d c0530d) {
        this.f3361b = activity;
        this.f3362c = c0530d;
    }

    @JavascriptInterface
    public void openQRScanner(String str) {
        Logger.m5184a(f3360a, "JSInterface openQRScanner Called");
        this.f3365f = str;
        if (m5220c()) {
            m5218b();
            return;
        }
        Log.i(f3360a, "Requesting for Camera Access Permission.....");
        Logger.m5184a(f3360a, "Requesting for Camera Access Permission.");
    }

    @JavascriptInterface
    public void closeQRScanner() {
        Logger.m5184a(f3360a, "JSInterface closeQRScanner Called");
        if (this.f3363d != null) {
            Logger.m5184a(f3360a, "JSInterface closeQRScanner 2 Called");
            this.f3361b.runOnUiThread(new C05941(this));
            return;
        }
        Logger.m5184a(f3360a, "JSInterface closeQRScanner 3 Called");
        Logger.m5188b(f3360a, "ERROR: CaptureManager NULL!!");
    }

    @JavascriptInterface
    public void captureManager(String str) {
        this.f3361b.runOnUiThread(new C05952(this, str));
    }

    public void m5223a(int i, String[] strArr, int[] iArr) {
        if (this.f3362c == null) {
            Logger.m5188b(f3360a, "ERROR: Empty Dynamic UI!!");
            return;
        }
        switch (i) {
            case R.AppCompatTheme_buttonStyleSmall /*101*/:
                if (m5220c()) {
                    Logger.m5184a(f3360a, "Camera Access Permission Granted.");
                    m5218b();
                    return;
                }
                Logger.m5188b(f3360a, "ERROR: Camera Access Permission Not Granted!!");
            default:
        }
    }

    private void m5218b() {
        Logger.m5184a(f3360a, "openQRScanner Invoked!!");
        if (TextUtils.isEmpty(this.f3365f)) {
            Logger.m5188b(f3360a, "ERROR: Frame ID null!!");
            return;
        }
        int parseInt = Integer.parseInt(this.f3365f);
        Logger.m5184a(f3360a, "Opening QR Scanner inside Frame with ID :" + parseInt);
        this.f3361b.runOnUiThread(new C05973(this, parseInt));
    }

    private boolean m5220c() {
        return ContextCompat.m76a(this.f3361b, "android.permission.CAMERA") == 0;
    }
}
