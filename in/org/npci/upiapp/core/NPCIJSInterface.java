package in.org.npci.upiapp.core;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Base64;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import in.juspay.mystique.C0530d;
import in.org.npci.upiapp.p037a.Logger;
import in.org.npci.upiapp.utils.CryptoUtils;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.pinactivitycomponent.R.R;
import org.npci.upi.security.services.CLRemoteResultReceiver;
import org.npci.upi.security.services.CLServices;
import org.npci.upi.security.services.ServiceConnectionStatusNotifier;

public class NPCIJSInterface {
    private static CLServices f3350b;
    private Activity f3351a;
    private C0530d f3352c;
    private String f3353d;

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.1 */
    class C05871 implements ServiceConnectionStatusNotifier {
        final /* synthetic */ String f3319a;
        final /* synthetic */ NPCIJSInterface f3320b;

        C05871(NPCIJSInterface nPCIJSInterface, String str) {
            this.f3320b = nPCIJSInterface;
            this.f3319a = str;
        }

        public void serviceConnected(CLServices cLServices) {
            NPCIJSInterface.f3350b = cLServices;
            this.f3320b.f3352c.m4438a("window.callUICallback(\"" + this.f3319a + "\")");
        }

        public void serviceDisconnected() {
            Logger.m5188b("NPCIJSInterface", "Error Code : 843 - Description : NPCI Service Disconnected");
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.2 */
    class C05882 implements ServiceConnectionStatusNotifier {
        final /* synthetic */ String f3321a;
        final /* synthetic */ String[] f3322b;
        final /* synthetic */ String f3323c;
        final /* synthetic */ CLRemoteResultReceiver f3324d;
        final /* synthetic */ NPCIJSInterface f3325e;

        C05882(NPCIJSInterface nPCIJSInterface, String str, String[] strArr, String str2, CLRemoteResultReceiver cLRemoteResultReceiver) {
            this.f3325e = nPCIJSInterface;
            this.f3321a = str;
            this.f3322b = strArr;
            this.f3323c = str2;
            this.f3324d = cLRemoteResultReceiver;
        }

        public void serviceConnected(CLServices cLServices) {
            NPCIJSInterface.f3350b = cLServices;
            String str = this.f3321a;
            int i = -1;
            switch (str.hashCode()) {
                case -2133316482:
                    if (str.equals("registerApp")) {
                        i = 1;
                        break;
                    }
                    break;
                case -1531153537:
                    if (str.equals("unbindService")) {
                        i = 3;
                        break;
                    }
                    break;
                case -981163955:
                    if (str.equals("getCredential")) {
                        i = 2;
                        break;
                    }
                    break;
                case 1393028525:
                    if (str.equals("getChallenge")) {
                        i = 0;
                        break;
                    }
                    break;
            }
            switch (i) {
                case R.View_android_theme /*0*/:
                    this.f3325e.f3352c.m4438a("window.callUICallback(\"" + this.f3323c + "\"" + ",\"" + NPCIJSInterface.f3350b.getChallenge(this.f3322b[0], this.f3322b[1]) + "\"" + ")");
                case R.View_android_focusable /*1*/:
                    this.f3325e.f3352c.m4438a("window.callUICallback(\"" + this.f3323c + "\"" + ",\"" + Boolean.valueOf(NPCIJSInterface.f3350b.registerApp(this.f3322b[0], this.f3322b[1], this.f3322b[2], this.f3322b[3])) + "\"" + ")");
                case R.View_paddingStart /*2*/:
                    this.f3325e.f3353d = this.f3323c;
                    NPCIJSInterface.f3350b.getCredential(this.f3322b[0], this.f3322b[1], this.f3322b[2], this.f3322b[3], this.f3322b[4], this.f3322b[5], this.f3322b[6], this.f3322b[7], this.f3324d);
                case R.View_paddingEnd /*3*/:
                    try {
                        NPCIJSInterface.f3350b.unbindService();
                    } catch (Throwable e) {
                        Logger.m5187a(e);
                    }
                    this.f3325e.f3352c.m4438a("window.callUICallback(\"" + this.f3323c + "\")");
                default:
            }
        }

        public void serviceDisconnected() {
            Logger.m5188b("NPCIJSInterface", "Error Code : 843 - Description : NPCI Service Disconnected");
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.3 */
    class C05893 implements Runnable {
        final /* synthetic */ String f3326a;
        final /* synthetic */ String f3327b;
        final /* synthetic */ String f3328c;
        final /* synthetic */ NPCIJSInterface f3329d;

        C05893(NPCIJSInterface nPCIJSInterface, String str, String str2, String str3) {
            this.f3329d = nPCIJSInterface;
            this.f3326a = str;
            this.f3327b = str2;
            this.f3328c = str3;
        }

        public void run() {
            this.f3329d.handleInit(this.f3326a, "getChallenge", null, this.f3327b, this.f3328c);
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.4 */
    class C05904 implements Runnable {
        final /* synthetic */ String f3330a;
        final /* synthetic */ String f3331b;
        final /* synthetic */ String f3332c;
        final /* synthetic */ String f3333d;
        final /* synthetic */ String f3334e;
        final /* synthetic */ NPCIJSInterface f3335f;

        C05904(NPCIJSInterface nPCIJSInterface, String str, String str2, String str3, String str4, String str5) {
            this.f3335f = nPCIJSInterface;
            this.f3330a = str;
            this.f3331b = str2;
            this.f3332c = str3;
            this.f3333d = str4;
            this.f3334e = str5;
        }

        public void run() {
            this.f3335f.handleInit(this.f3330a, "registerApp", null, this.f3331b, this.f3332c, this.f3333d, this.f3334e);
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.5 */
    class C05915 extends ResultReceiver {
        final /* synthetic */ String f3336a;
        final /* synthetic */ NPCIJSInterface f3337b;

        C05915(NPCIJSInterface nPCIJSInterface, Handler handler, String str) {
            this.f3337b = nPCIJSInterface;
            this.f3336a = str;
            super(handler);
        }

        protected void onReceiveResult(int i, Bundle bundle) {
            Logger.m5184a("NPCIJSInterface", "ResultCode is " + i);
            JSONObject jSONObject = new JSONObject();
            try {
                super.onReceiveResult(i, bundle);
                if (bundle != null) {
                    for (String str : bundle.keySet()) {
                        try {
                            jSONObject.put(str, this.f3337b.m5203a(bundle.get(str)));
                        } catch (Throwable e) {
                            Logger.m5187a(e);
                        }
                    }
                    try {
                        jSONObject.put("resultCode", i);
                    } catch (Throwable e2) {
                        Logger.m5187a(e2);
                    }
                }
            } catch (Throwable e22) {
                Logger.m5187a(e22);
            }
            this.f3337b.f3352c.m4438a("window.callUICallback(\"" + this.f3336a + "\", " + jSONObject.toString() + ");");
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.6 */
    class C05926 implements Runnable {
        final /* synthetic */ String f3338a;
        final /* synthetic */ CLRemoteResultReceiver f3339b;
        final /* synthetic */ String f3340c;
        final /* synthetic */ String f3341d;
        final /* synthetic */ String f3342e;
        final /* synthetic */ String f3343f;
        final /* synthetic */ String f3344g;
        final /* synthetic */ String f3345h;
        final /* synthetic */ String f3346i;
        final /* synthetic */ String f3347j;
        final /* synthetic */ NPCIJSInterface f3348k;

        C05926(NPCIJSInterface nPCIJSInterface, String str, CLRemoteResultReceiver cLRemoteResultReceiver, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
            this.f3348k = nPCIJSInterface;
            this.f3338a = str;
            this.f3339b = cLRemoteResultReceiver;
            this.f3340c = str2;
            this.f3341d = str3;
            this.f3342e = str4;
            this.f3343f = str5;
            this.f3344g = str6;
            this.f3345h = str7;
            this.f3346i = str8;
            this.f3347j = str9;
        }

        public void run() {
            this.f3348k.handleInit(this.f3338a, "getCredential", this.f3339b, this.f3340c, this.f3341d, this.f3342e, this.f3343f, this.f3344g, this.f3345h, this.f3346i, this.f3347j);
        }
    }

    /* renamed from: in.org.npci.upiapp.core.NPCIJSInterface.7 */
    class C05937 implements Runnable {
        final /* synthetic */ NPCIJSInterface f3349a;

        C05937(NPCIJSInterface nPCIJSInterface) {
            this.f3349a = nPCIJSInterface;
        }

        public void run() {
            if (this.f3349a.f3351a.getCurrentFocus() != null) {
                ((InputMethodManager) this.f3349a.f3351a.getApplicationContext().getSystemService("input_method")).hideSoftInputFromWindow(this.f3349a.f3351a.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public NPCIJSInterface(Activity activity, C0530d c0530d) {
        this.f3351a = activity;
        this.f3352c = c0530d;
    }

    @JavascriptInterface
    public void initialiseNPCICL(String str) {
        try {
            CLServices.initService(this.f3351a, new C05871(this, str));
        } catch (Throwable e) {
            if ("Service already initiated".equals(e.getMessage())) {
                this.f3352c.m4438a("window.callUICallback(\"" + str + "\")");
            } else {
                Logger.m5185a("NPCIJSInterface", "intialiseNPCICL", e);
            }
        }
    }

    @JavascriptInterface
    public void handleInit(String str, String str2, CLRemoteResultReceiver cLRemoteResultReceiver, String... strArr) {
        try {
            CLServices.initService(this.f3351a, new C05882(this, str2, strArr, str, cLRemoteResultReceiver));
        } catch (RuntimeException e) {
            int i = -1;
            switch (str2.hashCode()) {
                case -2133316482:
                    if (str2.equals("registerApp")) {
                        i = 1;
                        break;
                    }
                    break;
                case -1531153537:
                    if (str2.equals("unbindService")) {
                        i = 3;
                        break;
                    }
                    break;
                case -981163955:
                    if (str2.equals("getCredential")) {
                        i = 2;
                        break;
                    }
                    break;
                case 1393028525:
                    if (str2.equals("getChallenge")) {
                        i = 0;
                        break;
                    }
                    break;
            }
            switch (i) {
                case R.View_android_theme /*0*/:
                    this.f3352c.m4438a("window.callUICallback(\"" + str + "\"" + ",\"" + f3350b.getChallenge(strArr[0], strArr[1]) + "\"" + ")");
                case R.View_android_focusable /*1*/:
                    this.f3352c.m4438a("window.callUICallback(\"" + str + "\"" + ",\"" + Boolean.valueOf(f3350b.registerApp(strArr[0], strArr[1], strArr[2], strArr[3])) + "\"" + ")");
                case R.View_paddingStart /*2*/:
                    for (String str3 : strArr) {
                        Logger.m5184a("NPCIJSInterface", "getCredentials - " + str3);
                    }
                    f3350b.getCredential(strArr[0], strArr[1], strArr[2], strArr[3], strArr[4], strArr[5], strArr[6], strArr[7], cLRemoteResultReceiver);
                    this.f3352c.m4438a("window.callUICallback(\"" + str + "\")");
                case R.View_paddingEnd /*3*/:
                    try {
                        f3350b.unbindService();
                    } catch (Throwable e2) {
                        Logger.m5187a(e2);
                    }
                    this.f3352c.m4438a("window.callUICallback(\"" + str + "\")");
                default:
            }
        }
    }

    @JavascriptInterface
    public void getChallenge(String str, String str2, String str3) {
        this.f3351a.runOnUiThread(new C05893(this, str3, str, str2));
    }

    @JavascriptInterface
    public void registerApp(String str, String str2, String str3, String str4, String str5) {
        this.f3351a.runOnUiThread(new C05904(this, str5, str, str2, str3, str4));
    }

    @JavascriptInterface
    public void getCredentials(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.f3353d = str9;
        this.f3351a.runOnUiThread(new C05926(this, str9, new CLRemoteResultReceiver(new C05915(this, new Handler(), str9)), str, str2, str3, str4, str5, str6, str7, str8));
    }

    public void m5209a() {
        Logger.m5184a("NPCIJSInterface", "Lifecycle - onResume Called");
        try {
            if (this.f3353d != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    Logger.m5184a("NPCIJSInterface", "BACK_PRESSED - CALLBACK CALLED");
                    jSONObject.put("event", "back_pressed");
                } catch (Throwable e) {
                    Logger.m5187a(e);
                }
                this.f3352c.m4438a("window.callUICallback(\"" + this.f3353d + "\", " + jSONObject.toString() + ");");
                this.f3353d = null;
            }
        } catch (Throwable e2) {
            Logger.m5187a(e2);
        }
    }

    private Object m5203a(Object obj) {
        if (obj == null) {
            return JSONObject.NULL;
        }
        if ((obj instanceof JSONArray) || (obj instanceof JSONObject) || obj.equals(JSONObject.NULL)) {
            return obj;
        }
        try {
            if (obj instanceof Collection) {
                return new JSONArray((Collection) obj);
            }
            if (obj.getClass().isArray()) {
                return m5207b(obj);
            }
            if (obj instanceof Map) {
                return new JSONObject((Map) obj);
            }
            if ((obj instanceof Boolean) || (obj instanceof Byte) || (obj instanceof Character) || (obj instanceof Double) || (obj instanceof Float) || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Short) || (obj instanceof String)) {
                return obj;
            }
            if (obj.getClass().getPackage().getName().startsWith("java.")) {
                return obj.toString();
            }
            return null;
        } catch (Exception e) {
        }
    }

    private JSONArray m5207b(Object obj) {
        JSONArray jSONArray = new JSONArray();
        if (obj.getClass().isArray()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                jSONArray.put(m5203a(Array.get(obj, i)));
            }
            return jSONArray;
        }
        throw new JSONException("Not a primitive array: " + obj.getClass());
    }

    @JavascriptInterface
    public String fetchData(String str) {
        return this.f3351a.getSharedPreferences("NPCI", 0).getString(str, "NOT_FOUND");
    }

    @JavascriptInterface
    public void saveData(String str, String str2) {
        Logger.m5184a("NPCIJSInterface", "Saving to local store : " + str + ", " + str2);
        this.f3351a.getSharedPreferences("NPCI", 0).edit().putString(str, str2).apply();
    }

    @JavascriptInterface
    public void unbindNPCICL(String str) {
        handleInit(str, "unbindService", null, new String[0]);
    }

    @JavascriptInterface
    public String populateHMAC(String str, String str2, String str3, String str4) {
        m5210b();
        String str5 = null;
        try {
            CryptoUtils cryptoUtils = new CryptoUtils();
            String str6 = str + CLConstants.SALT_DELIMETER + str2 + CLConstants.SALT_DELIMETER + str4;
            Logger.m5188b("NPCIJSInterface", "PSP Hmac Msg - " + str6);
            str5 = Base64.encodeToString(CryptoUtils.m5256a(CryptoUtils.m5255a(str6), Base64.decode(str3, 2)), 0);
        } catch (Throwable e) {
            Logger.m5185a("NPCIJSInterface", "populateHMAC ", e);
        }
        return str5;
    }

    @JavascriptInterface
    public String trustCred(String str, String str2) {
        String str3 = null;
        try {
            CryptoUtils cryptoUtils = new CryptoUtils();
            str3 = Base64.encodeToString(CryptoUtils.m5256a(CryptoUtils.m5255a(str), Base64.decode(str2, 2)), 2);
        } catch (Throwable e) {
            Logger.m5187a(e);
        }
        return str3;
    }

    @JavascriptInterface
    public String decodeNPCIXmlKeys(String str) {
        return new String(Base64.decode(str, 2));
    }

    public void m5210b() {
        this.f3351a.runOnUiThread(new C05937(this));
    }
}
