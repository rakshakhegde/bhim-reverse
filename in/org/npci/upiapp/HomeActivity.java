package in.org.npci.upiapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.p004a.LocalBroadcastManager;
import android.support.v7.p013a.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.common.C0100b;
import in.juspay.mystique.C0530d;
import in.juspay.mystique.C0531e;
import in.juspay.widget.qrscanner.com.google.zxing.BarcodeFormat;
import in.juspay.widget.qrscanner.com.google.zxing.EncodeHintType;
import in.juspay.widget.qrscanner.com.google.zxing.common.BitMatrix;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.QRCodeWriter;
import in.juspay.widget.qrscanner.com.google.zxing.p030b.p031a.ErrorCorrectionLevel;
import in.org.npci.upiapp.core.JsInterface;
import in.org.npci.upiapp.core.NPCIJSInterface;
import in.org.npci.upiapp.core.QRScannerInterface;
import in.org.npci.upiapp.core.Tracker;
import in.org.npci.upiapp.gcm.RegistrationIntentService;
import in.org.npci.upiapp.p037a.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.R.R;

public class HomeActivity extends AppCompatActivity {
    public static String f3242m;
    private static String f3243o;
    private C0577a f3244n;
    private C0530d f3245p;
    private JsInterface f3246q;
    private Tracker f3247r;
    private NPCIJSInterface f3248s;
    private QRScannerInterface f3249t;
    private String f3250u;

    /* renamed from: in.org.npci.upiapp.HomeActivity.1 */
    class C05761 implements C0531e {
        final /* synthetic */ HomeActivity f3240a;

        C05761(HomeActivity homeActivity) {
            this.f3240a = homeActivity;
        }

        public void m5165a(String str, String str2) {
            this.f3240a.f3245p.m4438a("window.onAndroidError('" + str2 + "');");
        }
    }

    /* renamed from: in.org.npci.upiapp.HomeActivity.a */
    private class C0577a extends BroadcastReceiver {
        final /* synthetic */ HomeActivity f3241a;

        private C0577a(HomeActivity homeActivity) {
            this.f3241a = homeActivity;
        }

        public void onReceive(Context context, Intent intent) {
            if (this.f3241a.f3245p != null) {
                try {
                    String stringExtra = intent.getStringExtra("onNotificationReceived");
                    String stringExtra2 = intent.getStringExtra("onTokenReceived");
                    String stringExtra3 = intent.getStringExtra("onTokenRefreshed");
                    if (stringExtra != null) {
                        this.f3241a.f3245p.m4439b("javascript: window.onNotificationReceived(" + stringExtra + ");");
                    }
                    if (stringExtra2 != null) {
                        this.f3241a.f3245p.m4439b("javascript: window.onTokenReceived('" + stringExtra2 + "');");
                    }
                    if (stringExtra3 != null) {
                        this.f3241a.f3245p.m4439b("javascript: window.onTokenRefreshed('" + stringExtra3 + "');");
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    static {
        f3243o = BuildConfig.FLAVOR;
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        m5175n();
        m5173l();
        m5174m();
        this.f3247r = new Tracker(this);
        m5176o();
        setContentView(2130968601);
        f3243o = getString(2131165262);
        m5170a(bundle);
        this.f3245p.m4439b(f3243o);
        try {
            f3242m = new WebView(this).getSettings().getUserAgentString();
        } catch (Exception e) {
            f3242m = BuildConfig.FLAVOR;
        }
    }

    private void m5173l() {
        int i = 0;
        try {
            SharedPreferences sharedPreferences = getSharedPreferences("NPCI", 0);
            if (sharedPreferences != null) {
                String str = "LAST_APP_VERSION";
                int i2 = sharedPreferences.getInt("LAST_APP_VERSION", -1);
                if (i2 == -1) {
                    sharedPreferences.edit().putInt("LAST_APP_VERSION", 8).apply();
                }
                if (8 > i2) {
                    File[] listFiles = getDir("bhim", 0).listFiles();
                    while (listFiles != null && i < listFiles.length) {
                        listFiles[i].delete();
                        i++;
                    }
                    sharedPreferences.edit().putInt("LAST_APP_VERSION", 8).apply();
                }
            }
        } catch (Throwable e) {
            Logger.m5185a("HomeActivity", "Exception: resetInternalStorageIfAppUpdated", e);
        }
    }

    public int m5179a(float f) {
        return (int) (((float) (getResources().getDisplayMetrics().densityDpi / 160)) * f);
    }

    private void m5174m() {
        if (m5178q()) {
            startService(new Intent(this, RegistrationIntentService.class));
        }
    }

    private void m5175n() {
        if ("PRODUCTION".equals("PRODUCTION") && m5177p()) {
            throw new RuntimeException("Debuggable mode wont work. Please download app from playstore");
        }
    }

    private void m5176o() {
        Crashlytics.setUserIdentifier(getString(R.app_name));
        Crashlytics.setUserEmail("test@juspay.in");
        Crashlytics.setUserName("Test");
    }

    private boolean m5177p() {
        return (getApplicationInfo().flags & 2) != 0;
    }

    public Bitmap m5180a(String str, String str2, boolean z) {
        boolean z2 = true;
        Bitmap b = m5171b(str);
        String str3 = "HomeActivity";
        StringBuilder append = new StringBuilder().append("Saved Bitmap ? ");
        if (b == null) {
            z2 = false;
        }
        Logger.m5184a(str3, append.append(z2).toString());
        if (b != null) {
            return b;
        }
        Map hashMap = new HashMap();
        hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        try {
            int i;
            BitMatrix a = new QRCodeWriter().m4750a(str2, BarcodeFormat.QR_CODE, 512, 512, hashMap);
            int c = a.m4840c();
            int d = a.m4842d();
            Bitmap createBitmap = Bitmap.createBitmap(c, d, Config.RGB_565);
            for (i = 0; i < c; i++) {
                for (int i2 = 0; i2 < d; i2++) {
                    createBitmap.setPixel(i, i2, a.m4836a(i, i2) ? -16777216 : -1);
                }
            }
            Options options = new Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), 2130837773, options);
            options.inJustDecodeBounds = false;
            options.inSampleSize = m5166a(options, m5179a(20.0f));
            b = BitmapFactory.decodeResource(getResources(), 2130837773, options);
            Canvas canvas = new Canvas(createBitmap);
            i = canvas.getWidth();
            int height = canvas.getHeight();
            canvas.drawBitmap(createBitmap, new Matrix(), null);
            canvas.drawBitmap(b, (float) ((i - b.getWidth()) / 2), (float) ((height - b.getHeight()) / 2), null);
            if (!z) {
                return createBitmap;
            }
            m5168a(str, createBitmap);
            return createBitmap;
        } catch (Throwable e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            return null;
        }
    }

    private String m5168a(String str, Bitmap bitmap) {
        FileOutputStream fileOutputStream;
        Exception e;
        Throwable th;
        File file = new File(new ContextWrapper(getApplicationContext()).getDir("imageDir", 0), str);
        try {
            fileOutputStream = new FileOutputStream(file);
            try {
                bitmap.compress(CompressFormat.PNG, 100, fileOutputStream);
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                e = e3;
                try {
                    e.printStackTrace();
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e22) {
                            e22.printStackTrace();
                        }
                    }
                    return file.getAbsolutePath();
                } catch (Throwable th2) {
                    th = th2;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                    throw th;
                }
            }
        } catch (Exception e5) {
            e = e5;
            fileOutputStream = null;
            e.printStackTrace();
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            return file.getAbsolutePath();
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            throw th;
        }
        return file.getAbsolutePath();
    }

    private Bitmap m5171b(String str) {
        try {
            return BitmapFactory.decodeStream(new FileInputStream(new File(new ContextWrapper(this).getDir("imageDir", 0), str)));
        } catch (Throwable e) {
            Logger.m5187a(e);
            return null;
        }
    }

    public static int m5166a(Options options, int i) {
        int i2 = options.outHeight;
        int i3 = 1;
        if (i2 > i) {
            while ((i2 / 2) / i3 >= i) {
                i3 *= 2;
            }
        }
        return i3;
    }

    public String m5183k() {
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        if (!action.equals("android.intent.action.VIEW") || data == null) {
            return null;
        }
        Logger.m5184a("HomeActivity", "DATA_FROM_MERCHANT - " + data.toString());
        return data.toString();
    }

    private void m5170a(Bundle bundle) {
        this.f3245p = new C0530d(this, (FrameLayout) findViewById(2131558501), null, new C05761(this));
        this.f3246q = new JsInterface(this, this.f3245p);
        this.f3245p.m4437a(this.f3246q, "JBridge");
        this.f3245p.m4437a(this.f3247r, "Tracker");
        if (bundle != null) {
            try {
                this.f3245p.m4441c(bundle.getString("currentAppState"));
            } catch (Throwable e) {
                Logger.m5187a(e);
                return;
            }
        }
        this.f3246q = new JsInterface(this, this.f3245p);
        this.f3245p.m4437a(this.f3246q, "JBridge");
        this.f3248s = new NPCIJSInterface(this, this.f3245p);
        this.f3245p.m4437a(this.f3248s, "NPCICL");
        this.f3249t = new QRScannerInterface(this, this.f3245p);
        this.f3245p.m4437a(this.f3249t, "QRScanner");
        if (getResources().getBoolean(2131230728) && VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        this.f3246q.m5200a(i, strArr, iArr);
        if (this.f3249t != null) {
            this.f3249t.m5223a(i, strArr, iArr);
        }
    }

    private void m5169a(BroadcastReceiver broadcastReceiver) {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("in.org.npci.upiapp.uibroadcastreceiver");
            LocalBroadcastManager.m106a((Context) this).m110a(broadcastReceiver, intentFilter);
        } catch (Exception e) {
        }
    }

    private void m5172b(BroadcastReceiver broadcastReceiver) {
        try {
            LocalBroadcastManager.m106a((Context) this).m109a(broadcastReceiver);
        } catch (Exception e) {
        }
    }

    protected void onResume() {
        super.onResume();
        this.f3244n = new C0577a();
        m5169a(this.f3244n);
        if (this.f3248s != null) {
            Logger.m5184a("HomeActivity", "Lifecycle - onResume Called");
            this.f3248s.m5209a();
        }
        this.f3245p.m4442d("onResume");
    }

    protected void onPause() {
        super.onPause();
        m5172b(this.f3244n);
        this.f3245p.m4442d("onPause");
    }

    protected void onDestroy() {
        super.onDestroy();
        m5172b(this.f3244n);
        this.f3245p.m4442d("onDestroy");
        this.f3245p.m4436a();
    }

    public void onBackPressed() {
        this.f3245p.m4438a("window.onBackpressed()");
    }

    protected void onActivityResult(int i, int i2, Intent intent) {
        if (i == 8) {
            if (i2 == -1) {
                this.f3245p.m4438a("window.callUICallback(\"" + this.f3250u + "\", \"SUCCESS\")");
            } else if (i2 == 0) {
                this.f3245p.m4438a("window.callUICallback(\"" + this.f3250u + "\", \"SUCCESS\")");
            }
        }
        super.onActivityResult(i, i2, intent);
        Logger.m5184a("HomeActivity", "Activity Result is " + i);
    }

    public void m5181a(String str) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                bundle.putString(str2, (String) jSONObject.get(str2));
            }
        } catch (Throwable e) {
            Logger.m5187a(e);
        }
        intent.putExtras(bundle);
        setResult(0, intent);
        finish();
    }

    public void m5182a(String str, String str2, String str3) {
        this.f3250u = str3;
        Intent intent = new Intent("android.intent.action.VIEW", Uri.fromParts("sms", str2, null));
        intent.putExtra("sms_body", str);
        intent.putExtra("exit_on_sent", true);
        startActivityForResult(intent, 8);
    }

    private boolean m5178q() {
        C0100b a = C0100b.m3073a();
        int a2 = a.m3075a((Context) this);
        if (a2 == 0) {
            return true;
        }
        if (a.m3080a(a2)) {
            a.m3076a((Activity) this, a2, 9000).show();
        } else {
            Log.e("HomeActivity", "This device is not supported for GCM");
        }
        return false;
    }
}
