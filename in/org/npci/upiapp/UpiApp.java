package in.org.npci.upiapp;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import io.fabric.sdk.android.Fabric;

public class UpiApp extends Application {
    public static boolean f3251a;

    static {
        f3251a = false;
    }

    public void onCreate() {
        boolean z = true;
        super.onCreate();
        Fabric.m5313a((Context) this, new Crashlytics());
        Fabric.m5313a((Context) this, new Answers());
        if ((getApplicationInfo().flags & 2) == 0) {
            z = false;
        }
        f3251a = z;
    }
}
