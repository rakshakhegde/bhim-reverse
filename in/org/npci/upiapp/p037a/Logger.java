package in.org.npci.upiapp.p037a;

import android.util.Log;
import com.crashlytics.android.Crashlytics;
import in.org.npci.upiapp.UpiApp;

/* renamed from: in.org.npci.upiapp.a.a */
public class Logger {
    public static void m5184a(String str, String str2) {
        Logger.m5186a(str, str2, false);
    }

    public static void m5186a(String str, String str2, boolean z) {
        if (UpiApp.f3251a) {
            Crashlytics.log(3, str, str2);
        } else if (z) {
            Crashlytics.log(str2);
        }
    }

    public static void m5188b(String str, String str2) {
        Logger.m5189b(str, str2, true);
    }

    public static void m5189b(String str, String str2, boolean z) {
        if (UpiApp.f3251a) {
            Crashlytics.log(6, str, str2);
        } else if (z) {
            Crashlytics.log(str2);
        }
    }

    public static void m5185a(String str, String str2, Throwable th) {
        if (UpiApp.f3251a) {
            Crashlytics.log(6, str, str2);
        }
        Logger.m5187a(th);
    }

    public static void m5187a(Throwable th) {
        if (UpiApp.f3251a) {
            Log.e("Exception", "Exception", th);
            th.printStackTrace();
            return;
        }
        Crashlytics.logException(th);
    }
}
