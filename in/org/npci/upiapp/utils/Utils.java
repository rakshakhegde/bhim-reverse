package in.org.npci.upiapp.utils;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import in.org.npci.upiapp.p037a.Logger;

/* renamed from: in.org.npci.upiapp.utils.e */
public class Utils {
    private static final String f3388a;

    static {
        f3388a = Utils.class.getName();
    }

    public static String m5275a(Context context) {
        String deviceId;
        if (context != null) {
            try {
                deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            } catch (Throwable e) {
                Throwable th = e;
                deviceId = null;
                Throwable th2 = th;
                Logger.m5185a(f3388a, "Error getting deviceId ", th2);
                return deviceId;
            }
        }
        deviceId = null;
        if (context != null && r0 == null) {
            try {
                deviceId = Secure.getString(context.getApplicationContext().getContentResolver(), "android_id");
            } catch (Exception e2) {
                th2 = e2;
                Logger.m5185a(f3388a, "Error getting deviceId ", th2);
                return deviceId;
            }
        }
        return deviceId;
    }
}
