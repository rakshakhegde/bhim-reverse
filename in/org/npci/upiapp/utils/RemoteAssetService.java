package in.org.npci.upiapp.utils;

import android.content.Context;
import in.org.npci.upiapp.p037a.Logger;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;

/* renamed from: in.org.npci.upiapp.utils.d */
public class RemoteAssetService {
    public static Boolean m5272a(Context context, String str, boolean z) {
        String a = RemoteAssetService.m5273a(str.substring(str.lastIndexOf("/") + 1));
        byte[] b = RemoteAssetService.m5274b(context, str, z);
        if (b != null) {
            if (z) {
                a = a.replace(".zip", ".jsa");
            }
            FileUtil.m5262a(context, a, b);
            Logger.m5184a("RemoteAssetService", a + " downloaded successfully");
            return Boolean.valueOf(true);
        }
        Logger.m5188b("RemoteAssetService", a + " null");
        return Boolean.valueOf(false);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] m5274b(android.content.Context r12, java.lang.String r13, boolean r14) {
        /*
        r2 = 0;
        r1 = 0;
        r0 = "/";
        r0 = r13.lastIndexOf(r0);
        r0 = r0 + 1;
        r0 = r13.substring(r0);
        r6 = in.org.npci.upiapp.utils.RemoteAssetService.m5273a(r0);
        if (r14 == 0) goto L_0x008a;
    L_0x0014:
        r0 = ".zip";
        r0 = r13.endsWith(r0);
        if (r0 == 0) goto L_0x008a;
    L_0x001c:
        r0 = "BHIMPreferences";
        r0 = r12.getSharedPreferences(r0, r2);
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r2 = r2.append(r6);
        r3 = "_hash";
        r2 = r2.append(r3);
        r2 = r2.toString();
        r0 = r0.getString(r2, r1);
    L_0x0039:
        r2 = new java.util.HashMap;
        r2.<init>();
        r3 = "ts";
        r4 = java.lang.System.currentTimeMillis();
        r4 = java.lang.String.valueOf(r4);
        r2.put(r3, r4);
        r3 = "If-None-Match";
        r2.put(r3, r0);
        r4 = in.org.npci.upiapp.utils.RestClient.m5247a(r12, r13, r2);
        if (r4 == 0) goto L_0x016c;
    L_0x0056:
        r0 = ".zip";
        r0 = r13.endsWith(r0);
        if (r0 == 0) goto L_0x016c;
    L_0x005e:
        if (r14 == 0) goto L_0x016c;
    L_0x0060:
        r7 = in.org.npci.upiapp.utils.FileUtil.m5261a(r4);
        r0 = new java.io.ByteArrayInputStream;
        r0.<init>(r4);
        r5 = new java.util.zip.ZipInputStream;
        r5.<init>(r0);
        r2 = r1;
        r3 = r1;
    L_0x0070:
        r8 = r5.getNextEntry();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        if (r8 == 0) goto L_0x00c8;
    L_0x0076:
        r9 = new java.io.ByteArrayOutputStream;	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r9.<init>();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r0 = r5.read();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
    L_0x007f:
        r10 = -1;
        if (r0 == r10) goto L_0x0095;
    L_0x0082:
        r9.write(r0);	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r0 = r5.read();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        goto L_0x007f;
    L_0x008a:
        r0 = in.org.npci.upiapp.utils.FileUtil.m5265b(r12, r6);
        if (r0 == 0) goto L_0x016e;
    L_0x0090:
        r0 = in.org.npci.upiapp.utils.FileUtil.m5261a(r0);
        goto L_0x0039;
    L_0x0095:
        r5.closeEntry();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r9.close();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r0 = r8.getName();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r10 = ".signature";
        r0 = r0.endsWith(r10);	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        if (r0 == 0) goto L_0x00b4;
    L_0x00a7:
        r0 = r9.toByteArray();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r2 = 2;
        r0 = android.util.Base64.decode(r0, r2);	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r2 = r3;
    L_0x00b1:
        r3 = r2;
        r2 = r0;
        goto L_0x0070;
    L_0x00b4:
        r0 = r8.getName();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r8 = ".jsa";
        r0 = r0.endsWith(r8);	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        if (r0 == 0) goto L_0x0168;
    L_0x00c0:
        r0 = r9.toByteArray();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
        r11 = r2;
        r2 = r0;
        r0 = r11;
        goto L_0x00b1;
    L_0x00c8:
        if (r3 != 0) goto L_0x00cd;
    L_0x00ca:
        if (r2 != 0) goto L_0x00cd;
    L_0x00cc:
        return r1;
    L_0x00cd:
        r5 = new java.io.ObjectInputStream;	 Catch:{ Exception -> 0x0124, all -> 0x013f }
        r0 = new java.io.ByteArrayInputStream;	 Catch:{ Exception -> 0x0124, all -> 0x013f }
        r8 = "remoteAssetPublicKey";
        r8 = in.org.npci.upiapp.utils.FileUtil.m5268c(r12, r8);	 Catch:{ Exception -> 0x0124, all -> 0x013f }
        r0.<init>(r8);	 Catch:{ Exception -> 0x0124, all -> 0x013f }
        r5.<init>(r0);	 Catch:{ Exception -> 0x0124, all -> 0x013f }
        r0 = r5.readObject();	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        r0 = (java.security.PublicKey) r0;	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        r8 = "DSA";
        r8 = java.security.Signature.getInstance(r8);	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        r8.initVerify(r0);	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        r8.update(r3);	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        r0 = r8.verify(r2);	 Catch:{ Exception -> 0x015e, all -> 0x0154 }
        if (r0 != 0) goto L_0x00fd;
    L_0x00f5:
        r0 = r1;
    L_0x00f6:
        if (r5 == 0) goto L_0x00fb;
    L_0x00f8:
        r5.close();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0152 }
    L_0x00fb:
        r1 = r0;
        goto L_0x00cc;
    L_0x00fd:
        r0 = "BHIMPreferences";
        r2 = 0;
        r0 = r12.getSharedPreferences(r0, r2);	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r0 = r0.edit();	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r2.<init>();	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r2 = r2.append(r6);	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r4 = "_hash";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r0 = r0.putString(r2, r7);	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r0.commit();	 Catch:{ Exception -> 0x0163, all -> 0x0156 }
        r0 = r3;
        goto L_0x00f6;
    L_0x0124:
        r0 = move-exception;
        r2 = r0;
        r3 = r1;
        r0 = r4;
    L_0x0128:
        r4 = "RemoteAssetService";
        r5 = "Exception while checking digital signature of asset";
        in.org.npci.upiapp.p037a.Logger.m5185a(r4, r5, r2);	 Catch:{ all -> 0x0159 }
        if (r3 == 0) goto L_0x00fb;
    L_0x0131:
        r3.close();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0152 }
        goto L_0x00fb;
    L_0x0135:
        r0 = move-exception;
        r2 = "RemoteAssetService";
        r3 = "Got Out of Memory Error";
        in.org.npci.upiapp.p037a.Logger.m5185a(r2, r3, r0);
        r0 = r1;
        goto L_0x00fb;
    L_0x013f:
        r0 = move-exception;
        r5 = r1;
    L_0x0141:
        if (r5 == 0) goto L_0x0146;
    L_0x0143:
        r5.close();	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
    L_0x0146:
        throw r0;	 Catch:{ OutOfMemoryError -> 0x0135, Exception -> 0x0147 }
    L_0x0147:
        r0 = move-exception;
        r1 = r0;
        r0 = r4;
    L_0x014a:
        r2 = "RemoteAssetService";
        r3 = "Exception while checking digital signature of asset";
        in.org.npci.upiapp.p037a.Logger.m5185a(r2, r3, r1);
        goto L_0x00fb;
    L_0x0152:
        r1 = move-exception;
        goto L_0x014a;
    L_0x0154:
        r0 = move-exception;
        goto L_0x0141;
    L_0x0156:
        r0 = move-exception;
        r4 = r3;
        goto L_0x0141;
    L_0x0159:
        r2 = move-exception;
        r5 = r3;
        r4 = r0;
        r0 = r2;
        goto L_0x0141;
    L_0x015e:
        r0 = move-exception;
        r2 = r0;
        r3 = r5;
        r0 = r4;
        goto L_0x0128;
    L_0x0163:
        r0 = move-exception;
        r2 = r0;
        r0 = r3;
        r3 = r5;
        goto L_0x0128;
    L_0x0168:
        r0 = r2;
        r2 = r3;
        goto L_0x00b1;
    L_0x016c:
        r0 = r4;
        goto L_0x00fb;
    L_0x016e:
        r0 = r1;
        goto L_0x0039;
        */
        throw new UnsupportedOperationException("Method not decompiled: in.org.npci.upiapp.utils.d.b(android.content.Context, java.lang.String, boolean):byte[]");
    }

    public static String m5273a(String str) {
        return str.split("\\?")[0].split("#")[0].replaceAll("[^a-zA-Z0-9.]", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    }
}
