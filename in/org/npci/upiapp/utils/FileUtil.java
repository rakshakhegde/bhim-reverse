package in.org.npci.upiapp.utils;

import android.content.Context;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.upiapp.p037a.Logger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;

/* renamed from: in.org.npci.upiapp.utils.b */
public class FileUtil {
    public static byte[] m5264a(Context context, String str, String str2) {
        try {
            byte[] b = FileUtil.m5265b(context, str);
            if (b == null) {
                return FileUtil.m5268c(context, str2 + "/" + str);
            }
            return b;
        } catch (Throwable e) {
            Logger.m5185a("FileUtil", "not found in internal storage.", e);
            return FileUtil.m5268c(context, str2 + "/" + str);
        }
    }

    public static byte[] m5263a(Context context, String str) {
        return FileUtil.m5264a(context, str, BuildConfig.FLAVOR);
    }

    public static byte[] m5265b(Context context, String str) {
        File file = new File(context.getDir("bhim", 0), str);
        if (!file.exists()) {
            return null;
        }
        ByteArrayOutputStream a = FileUtil.m5259a(new ByteArrayOutputStream(), new FileInputStream(file));
        Logger.m5184a("FileUtil", str + " found in internal storage.");
        return a.toByteArray();
    }

    public static File m5260a(String str, Context context) {
        return new File(context.getDir("bhim", 0), str);
    }

    public static byte[] m5268c(Context context, String str) {
        ByteArrayOutputStream a = FileUtil.m5259a(new ByteArrayOutputStream(), context.getAssets().open(str, 0));
        Logger.m5184a("FileUtil", str + " found in assets.");
        return a.toByteArray();
    }

    private static ByteArrayOutputStream m5259a(ByteArrayOutputStream byteArrayOutputStream, InputStream inputStream) {
        byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArrayOutputStream;
            }
        }
    }

    public static String m5261a(byte[] bArr) {
        String str = "MD5";
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(bArr);
        byte[] digest = instance.digest();
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : digest) {
            str = Integer.toHexString(b & 255);
            while (str.length() < 2) {
                str = "0" + str;
            }
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }

    public static void m5262a(Context context, String str, byte[] bArr) {
        Throwable e;
        FileOutputStream fileOutputStream = null;
        FileOutputStream fileOutputStream2;
        try {
            fileOutputStream2 = new FileOutputStream(new File(context.getDir("bhim", 0), str));
            try {
                fileOutputStream2.write(bArr);
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (Throwable e2) {
                        Logger.m5185a("FileUtil", "Exception while closing stream", e2);
                    }
                }
            } catch (Exception e3) {
                e2 = e3;
                try {
                    Logger.m5185a("FileUtil", "Exception while writing stream", e2);
                    Logger.m5187a(e2);
                    if (fileOutputStream2 != null) {
                        try {
                            fileOutputStream2.close();
                        } catch (Throwable e22) {
                            Logger.m5185a("FileUtil", "Exception while closing stream", e22);
                        }
                    }
                } catch (Throwable th) {
                    e22 = th;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Throwable e4) {
                            Logger.m5185a("FileUtil", "Exception while closing stream", e4);
                        }
                    }
                    throw e22;
                }
            }
        } catch (Exception e5) {
            e22 = e5;
            fileOutputStream2 = null;
            Logger.m5185a("FileUtil", "Exception while writing stream", e22);
            Logger.m5187a(e22);
            if (fileOutputStream2 != null) {
                fileOutputStream2.close();
            }
        } catch (Throwable th2) {
            e22 = th2;
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            throw e22;
        }
    }

    public static byte[] m5266b(String str, Context context) {
        try {
            return FileUtil.m5259a(new ByteArrayOutputStream(), new FileInputStream(FileUtil.m5260a(str, context))).toByteArray();
        } catch (Throwable e) {
            Logger.m5185a("FileUtil", "Could not read " + str, e);
            throw new RuntimeException(e);
        } catch (Throwable e2) {
            Logger.m5185a("FileUtil", "Could not read " + str, e2);
            FileUtil.m5267c(str, context);
            throw new RuntimeException(e2);
        } catch (Throwable e22) {
            Logger.m5185a("FileUtil", "Could not read " + str, e22);
            FileUtil.m5267c(str, context);
            throw new RuntimeException(e22);
        }
    }

    public static boolean m5267c(String str, Context context) {
        File a = FileUtil.m5260a(str, context);
        if (a.exists()) {
            Logger.m5188b("FileUtil", "FILE CORRUPTED. DISABLING GODEL");
            return a.delete();
        }
        Logger.m5184a("FileUtil", str + " not found");
        return false;
    }
}
