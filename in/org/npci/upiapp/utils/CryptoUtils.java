package in.org.npci.upiapp.utils;

import android.util.Base64;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.upiapp.p037a.Logger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: in.org.npci.upiapp.utils.a */
public class CryptoUtils {
    private static final String f3386a;

    static {
        f3386a = CryptoUtils.class.getName();
    }

    public static byte[] m5255a(String str) {
        try {
            return MessageDigest.getInstance("SHA-256").digest(str.getBytes("UTF-8"));
        } catch (Throwable e) {
            Logger.m5185a(f3386a, "Error generating hash ", e);
            return null;
        }
    }

    public static byte[] m5256a(byte[] bArr, byte[] bArr2) {
        Key secretKeySpec = new SecretKeySpec(bArr2, "AES");
        AlgorithmParameterSpec ivParameterSpec = new IvParameterSpec(new byte[16]);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, secretKeySpec, ivParameterSpec);
        return instance.doFinal(bArr);
    }

    public static String m5254a(byte[] bArr) {
        if (bArr == null) {
            return BuildConfig.FLAVOR;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            String toHexString = Integer.toHexString(b & 255);
            if (toHexString.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(toHexString);
        }
        return stringBuffer.toString();
    }

    private static Key m5258b(String str) {
        return new SecretKeySpec(str.getBytes("UTF-8"), "AES");
    }

    public static String m5253a(String str, String str2) {
        Key b = CryptoUtils.m5258b(str2);
        Cipher instance = Cipher.getInstance("AES");
        instance.init(1, b);
        return new String(Base64.encode(instance.doFinal(str.getBytes()), 0));
    }

    public static String m5257b(String str, String str2) {
        Key b = CryptoUtils.m5258b(str2);
        Cipher instance = Cipher.getInstance("AES");
        instance.init(2, b);
        return new String(instance.doFinal(Base64.decode(str, 0)));
    }
}
