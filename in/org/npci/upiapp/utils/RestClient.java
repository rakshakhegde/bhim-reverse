package in.org.npci.upiapp.utils;

import android.content.Context;
import com.crashlytics.android.core.BuildConfig;
import in.org.npci.upiapp.p037a.Logger;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class RestClient {
    private static final String f3384a;
    private static DefaultHttpClient f3385b;

    public static class UnsuccessfulRestCall extends RuntimeException {
        private final HttpResponse f3382a;
        private final HttpRequestBase f3383b;

        public UnsuccessfulRestCall(HttpRequestBase httpRequestBase, HttpResponse httpResponse) {
            super(httpRequestBase.getURI() + " returned " + httpResponse.getStatusLine().getStatusCode());
            this.f3382a = httpResponse;
            this.f3383b = httpRequestBase;
        }
    }

    static {
        f3384a = RestClient.class.getName();
        m5245a();
    }

    private static DefaultHttpClient m5244a(HttpParams httpParams) {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        SocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier(SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        schemeRegistry.register(new Scheme("https", socketFactory, 443));
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
    }

    public static void m5245a() {
        Logger.m5184a(f3384a, "Default http client");
        HttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, AbstractSpiCall.DEFAULT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        f3385b = m5244a(basicHttpParams);
    }

    public static byte[] m5249a(String str, HashMap<String, String> hashMap) {
        HttpRequestBase httpGet = new HttpGet(str);
        if (hashMap != null) {
            for (Entry entry : hashMap.entrySet()) {
                httpGet.setHeader((String) entry.getKey(), (String) entry.getValue());
                Logger.m5184a(f3384a, "RestClientHeader : Header -> " + entry.getKey() + "=>" + entry.getValue());
            }
        }
        return m5251a(httpGet);
    }

    public static byte[] m5250a(String str, Map<String, String> map, HashMap<String, String> hashMap) {
        try {
            Logger.m5184a("URL", str);
            Logger.m5184a("PARAMETER", m5243a((Map) map));
            return m5249a(str, (HashMap) hashMap);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String m5243a(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();
        if (map.size() > 0) {
            stringBuilder.append("?");
        }
        for (Entry entry : map.entrySet()) {
            stringBuilder.append((String) entry.getKey());
            stringBuilder.append("=");
            if (entry.getValue() == null) {
                stringBuilder.append(BuildConfig.FLAVOR);
            } else {
                stringBuilder.append(URLEncoder.encode((String) entry.getValue(), "utf-8"));
            }
            stringBuilder.append("&");
        }
        return stringBuilder.toString();
    }

    private static byte[] m5252a(HttpRequestBase httpRequestBase, HttpResponse httpResponse) {
        GZIPInputStream gZIPInputStream;
        try {
            if (m5246a(httpResponse)) {
                Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                if (contentEncoding == null || !contentEncoding.getValue().equals("gzip")) {
                    return EntityUtils.toByteArray(httpResponse.getEntity());
                }
                Logger.m5184a(f3384a, "GZIP Header Present");
                gZIPInputStream = new GZIPInputStream(httpResponse.getEntity().getContent());
                byte[] bArr = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = gZIPInputStream.read(bArr);
                    if (read < 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bArr = byteArrayOutputStream.toByteArray();
                if (gZIPInputStream == null) {
                    return bArr;
                }
                Logger.m5184a(f3384a, "CLOSING GZIP STREAM");
                gZIPInputStream.close();
                return bArr;
            }
            throw new UnsuccessfulRestCall(httpRequestBase, httpResponse);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        } catch (ClientProtocolException e2) {
            throw new RuntimeException(e2.getMessage());
        } catch (Throwable th) {
            if (gZIPInputStream != null) {
                Logger.m5184a(f3384a, "CLOSING GZIP STREAM");
                gZIPInputStream.close();
            }
        }
    }

    private static byte[] m5251a(HttpRequestBase httpRequestBase) {
        Logger.m5184a(f3384a, "Executing " + httpRequestBase.getMethod() + " " + httpRequestBase.getURI());
        try {
            httpRequestBase.setHeader("Accept-Encoding", "gzip");
            HttpResponse execute = f3385b.execute(httpRequestBase);
            Logger.m5184a(f3384a, "Got response for " + httpRequestBase.getURI() + ", response code " + execute.getStatusLine().getStatusCode());
            return m5252a(httpRequestBase, execute);
        } catch (IOException e) {
            return null;
        }
    }

    private static boolean m5246a(HttpResponse httpResponse) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        return statusCode >= 200 && statusCode < 300;
    }

    public static byte[] m5248a(String str, String str2, HashMap<String, String> hashMap) {
        HttpRequestBase httpPost = new HttpPost(str);
        if (hashMap != null) {
            for (Entry entry : hashMap.entrySet()) {
                httpPost.setHeader((String) entry.getKey(), (String) entry.getValue());
                Logger.m5184a(f3384a, "RestClientHeader : Header -> " + entry.getKey() + "=>" + entry.getValue());
            }
        }
        Logger.m5184a("RestClient", str2);
        httpPost.setEntity(new ByteArrayEntity(str2.getBytes("UTF-8")));
        Logger.m5184a("REQUEST HTTP POST", "<URL>" + str + "<HEADER>" + hashMap + "<BODY>" + str2);
        return m5251a(httpPost);
    }

    public static byte[] m5247a(Context context, String str, Map<String, String> map) {
        HttpRequestBase httpGet = new HttpGet();
        for (Entry entry : map.entrySet()) {
            httpGet.setHeader((String) entry.getKey(), (String) entry.getValue());
        }
        httpGet.setURI(URI.create(str));
        HttpResponse execute = f3385b.execute(httpGet);
        Logger.m5184a(f3384a, str + " " + execute.getStatusLine().getStatusCode() + BuildConfig.FLAVOR);
        if (execute.getStatusLine().getStatusCode() == 200) {
            return m5252a(httpGet, execute);
        }
        if (execute.getStatusLine().getStatusCode() == 304) {
            return FileUtil.m5265b(context, RemoteAssetService.m5273a(str.substring(str.lastIndexOf("/") + 1)));
        }
        Logger.m5184a(f3384a, "Error While Downloading " + str + " . Response Code:  " + execute.getStatusLine().getStatusCode());
        return null;
    }
}
