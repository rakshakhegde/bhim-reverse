package in.org.npci.upiapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/* renamed from: in.org.npci.upiapp.utils.c */
public class KeyValueStore {
    private final SharedPreferences f3387a;

    public KeyValueStore(Context context) {
        this.f3387a = context.getSharedPreferences("BHIMPreferences", 0);
    }

    public void m5270a(String str, String str2) {
        Editor edit = this.f3387a.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public String m5271b(String str, String str2) {
        return this.f3387a.getString(str, str2);
    }

    public void m5269a(Context context) {
        context.getSharedPreferences("BHIMPreferences", 0).edit().clear().commit();
    }
}
