package in.org.npci.upiapp.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.p004a.LocalBroadcastManager;
import com.google.android.gms.gcm.C0397b;
import com.google.android.gms.iid.C0408a;
import in.org.npci.upiapp.p037a.Logger;

public class RegistrationIntentService extends IntentService {
    private static final String[] f3381a;

    static {
        f3381a = new String[]{"allcustomers"};
    }

    public RegistrationIntentService() {
        super("RegistrationIntentService");
    }

    protected void onHandleIntent(Intent intent) {
        try {
            String a = C0408a.m3693b(this).m3694a(getString(2131165267), "GCM", null);
            m5241a(a);
            m5242b(a);
        } catch (Throwable e) {
            Logger.m5185a("RegistrationIntentService", "Failed to complete token refresh", e);
        }
    }

    private void m5241a(String str) {
        Logger.m5184a("RegistrationIntentService", "Token for my GCM Listener is : " + str);
        try {
            Intent intent = new Intent("in.org.npci.upiapp.uibroadcastreceiver");
            intent.putExtra("onTokenReceived", str);
            LocalBroadcastManager.m106a((Context) this).m111a(intent);
        } catch (Exception e) {
        }
    }

    private void m5242b(String str) {
        try {
            C0397b a = C0397b.m3646a(this);
            for (String str2 : f3381a) {
                a.m3647a(str, "/topics/" + str2, null);
            }
        } catch (Exception e) {
        }
    }
}
