package in.org.npci.upiapp.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.aa.NotificationCompat;
import android.support.v4.p004a.LocalBroadcastManager;
import com.crashlytics.android.core.BuildConfig;
import com.google.android.gms.gcm.C0396a;
import in.org.npci.upiapp.HomeActivity;
import in.org.npci.upiapp.p037a.Logger;
import in.org.npci.upiapp.utils.RemoteAssetService;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;

public class GalileoListenerService extends C0396a {

    /* renamed from: in.org.npci.upiapp.gcm.GalileoListenerService.1 */
    class C05981 extends AsyncTask {
        final /* synthetic */ String f3378a;
        final /* synthetic */ boolean f3379b;
        final /* synthetic */ GalileoListenerService f3380c;

        C05981(GalileoListenerService galileoListenerService, String str, boolean z) {
            this.f3380c = galileoListenerService;
            this.f3378a = str;
            this.f3379b = z;
        }

        protected Object doInBackground(Object[] objArr) {
            try {
                RemoteAssetService.m5272a(this.f3380c.getApplicationContext(), this.f3378a, this.f3379b);
                return "SUCCESS";
            } catch (Exception e) {
                Logger.m5184a("GalileoListenerService", e.getMessage());
                return "FAILURE";
            }
        }

        protected void onPostExecute(Object obj) {
        }
    }

    public void m5240a(String str, Bundle bundle) {
        String string = bundle.getString("heading");
        String string2 = bundle.getString("body");
        if (string == null && string2 == null) {
            m5238c(bundle);
        } else {
            m5236b(bundle);
        }
    }

    private void m5236b(Bundle bundle) {
        try {
            CharSequence string = bundle.getString("heading");
            CharSequence string2 = bundle.getString("body");
            if (string2 == null) {
                string2 = "A new notification has arrived";
            }
            int random = (int) (Math.random() * 1000.0d);
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(402653184);
            intent.putExtra("notification", m5239d(bundle));
            intent.setAction(System.currentTimeMillis() + BuildConfig.FLAVOR);
            PendingIntent activity = PendingIntent.getActivity(this, random, intent, 134217728);
            Uri defaultUri = RingtoneManager.getDefaultUri(2);
            NotificationCompat notificationCompat = new NotificationCompat();
            notificationCompat.m219a(string);
            notificationCompat.m220b(string2);
            ((NotificationManager) getSystemService("notification")).notify(random, new NotificationCompat(this).m224a(2130837706).m227a(BitmapFactory.decodeResource(getResources(), 2130837762)).m232b(Color.parseColor("#1b3281")).m230a(string).m234b(string2).m229a(notificationCompat).m231a(true).m228a(defaultUri).m226a(activity).m223a());
            Intent intent2 = new Intent("in.org.npci.upiapp.uibroadcastreceiver");
            intent2.putExtra("onNotificationReceived", m5239d(bundle));
            LocalBroadcastManager.m106a((Context) this).m111a(intent2);
        } catch (Throwable e) {
            Logger.m5185a("GalileoListenerService", "Exception while showing notification", e);
        }
    }

    private void m5238c(Bundle bundle) {
        try {
            JSONArray jSONArray = new JSONObject(bundle.getString(CLConstants.FIELD_DATA)).getJSONObject(CLConstants.FIELD_DATA).getJSONArray("tasks");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString(CLConstants.OUTPUT_KEY_ACTION);
                if (string.equals("download")) {
                    m5237b(jSONObject);
                } else if (string.equals("editSharedPref")) {
                    m5234a(jSONObject);
                }
            }
        } catch (Throwable e) {
            Logger.m5185a("GalileoListenerService", "Exception", e);
        }
    }

    private void m5234a(JSONObject jSONObject) {
        try {
            SharedPreferences sharedPreferences = getSharedPreferences("BHIMPreferences", 0);
            if (sharedPreferences != null) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("keyPairs");
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    sharedPreferences.edit().putString(str, jSONObject2.getString(str)).apply();
                }
            }
        } catch (Throwable e) {
            Logger.m5185a("GalileoListenerService", "Exception", e);
        }
    }

    private void m5237b(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("request");
            String string = jSONObject2.getString("url");
            jSONObject2.optString("method", "GET");
            jSONObject2.getJSONObject("headers");
            boolean z = Boolean.getBoolean(jSONObject2.optString("isSignedJsa", "false"));
            if (string != null) {
                new C05981(this, string, z).execute(new Object[0]);
            }
        } catch (Throwable e) {
            Logger.m5185a("GalileoListenerService", "Exception", e);
        }
    }

    private String m5239d(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (String str : bundle.keySet()) {
            bundle.get(str);
            try {
                jSONObject.put(str, m5233a(bundle.get(str)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject.toString();
    }

    public static Object m5233a(Object obj) {
        if (obj == null) {
            return JSONObject.NULL;
        }
        if ((obj instanceof JSONArray) || (obj instanceof JSONObject) || obj.equals(JSONObject.NULL)) {
            return obj;
        }
        try {
            if (obj instanceof Collection) {
                return new JSONArray((Collection) obj);
            }
            if (obj.getClass().isArray()) {
                return m5235b(obj);
            }
            if (obj instanceof Map) {
                return new JSONObject((Map) obj);
            }
            if ((obj instanceof Boolean) || (obj instanceof Byte) || (obj instanceof Character) || (obj instanceof Double) || (obj instanceof Float) || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Short) || (obj instanceof String)) {
                return obj;
            }
            if (obj.getClass().getPackage().getName().startsWith("java.")) {
                return obj.toString();
            }
            return null;
        } catch (Exception e) {
        }
    }

    public static JSONArray m5235b(Object obj) {
        JSONArray jSONArray = new JSONArray();
        if (obj.getClass().isArray()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                jSONArray.put(m5233a(Array.get(obj, i)));
            }
            return jSONArray;
        }
        throw new JSONException("Not a primitive array: " + obj.getClass());
    }
}
