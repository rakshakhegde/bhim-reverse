package io.fabric.sdk.android;

import android.util.Log;

/* renamed from: io.fabric.sdk.android.b */
public class DefaultLogger implements Logger {
    private int f3395a;

    public DefaultLogger(int i) {
        this.f3395a = i;
    }

    public DefaultLogger() {
        this.f3395a = 4;
    }

    public boolean m5297a(String str, int i) {
        return this.f3395a <= i;
    }

    public void m5296a(String str, String str2, Throwable th) {
        if (m5297a(str, 3)) {
            Log.d(str, str2, th);
        }
    }

    public void m5299b(String str, String str2, Throwable th) {
        if (m5297a(str, 2)) {
            Log.v(str, str2, th);
        }
    }

    public void m5301c(String str, String str2, Throwable th) {
        if (m5297a(str, 4)) {
            Log.i(str, str2, th);
        }
    }

    public void m5303d(String str, String str2, Throwable th) {
        if (m5297a(str, 5)) {
            Log.w(str, str2, th);
        }
    }

    public void m5305e(String str, String str2, Throwable th) {
        if (m5297a(str, 6)) {
            Log.e(str, str2, th);
        }
    }

    public void m5295a(String str, String str2) {
        m5296a(str, str2, null);
    }

    public void m5298b(String str, String str2) {
        m5299b(str, str2, null);
    }

    public void m5300c(String str, String str2) {
        m5301c(str, str2, null);
    }

    public void m5302d(String str, String str2) {
        m5303d(str, str2, null);
    }

    public void m5304e(String str, String str2) {
        m5305e(str, str2, null);
    }

    public void m5293a(int i, String str, String str2) {
        m5294a(i, str, str2, false);
    }

    public void m5294a(int i, String str, String str2, boolean z) {
        if (z || m5297a(str, i)) {
            Log.println(i, str, str2);
        }
    }
}
