package io.fabric.sdk.android;

/* renamed from: io.fabric.sdk.android.j */
public class KitInfo {
    private final String f3443a;
    private final String f3444b;
    private final String f3445c;

    public KitInfo(String str, String str2, String str3) {
        this.f3443a = str;
        this.f3444b = str2;
        this.f3445c = str3;
    }

    public String m5367a() {
        return this.f3443a;
    }

    public String m5368b() {
        return this.f3444b;
    }

    public String m5369c() {
        return this.f3445c;
    }
}
