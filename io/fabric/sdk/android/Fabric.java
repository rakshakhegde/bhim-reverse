package io.fabric.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import io.fabric.sdk.android.ActivityLifecycleManager.ActivityLifecycleManager;
import io.fabric.sdk.android.services.concurrency.DependsOn;
import io.fabric.sdk.android.services.concurrency.PriorityThreadPoolExecutor;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.p021b.IdManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: io.fabric.sdk.android.c */
public class Fabric {
    static volatile Fabric f3410a;
    static final Logger f3411b;
    final Logger f3412c;
    final boolean f3413d;
    private final Context f3414e;
    private final Map<Class<? extends Kit>, Kit> f3415f;
    private final ExecutorService f3416g;
    private final Handler f3417h;
    private final InitializationCallback<Fabric> f3418i;
    private final InitializationCallback<?> f3419j;
    private final IdManager f3420k;
    private ActivityLifecycleManager f3421l;
    private WeakReference<Activity> f3422m;
    private AtomicBoolean f3423n;

    /* renamed from: io.fabric.sdk.android.c.1 */
    class Fabric extends ActivityLifecycleManager {
        final /* synthetic */ Fabric f3396a;

        Fabric(Fabric fabric) {
            this.f3396a = fabric;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            this.f3396a.m5325a(activity);
        }

        public void onActivityStarted(Activity activity) {
            this.f3396a.m5325a(activity);
        }

        public void onActivityResumed(Activity activity) {
            this.f3396a.m5325a(activity);
        }
    }

    /* renamed from: io.fabric.sdk.android.c.2 */
    class Fabric implements InitializationCallback {
        final CountDownLatch f3398a;
        final /* synthetic */ int f3399b;
        final /* synthetic */ Fabric f3400c;

        Fabric(Fabric fabric, int i) {
            this.f3400c = fabric;
            this.f3399b = i;
            this.f3398a = new CountDownLatch(this.f3399b);
        }

        public void m5309a(Object obj) {
            this.f3398a.countDown();
            if (this.f3398a.getCount() == 0) {
                this.f3400c.f3423n.set(true);
                this.f3400c.f3418i.m5307a(this.f3400c);
            }
        }

        public void m5308a(Exception exception) {
            this.f3400c.f3418i.m5306a(exception);
        }
    }

    /* renamed from: io.fabric.sdk.android.c.a */
    public static class Fabric {
        private final Context f3401a;
        private Kit[] f3402b;
        private PriorityThreadPoolExecutor f3403c;
        private Handler f3404d;
        private Logger f3405e;
        private boolean f3406f;
        private String f3407g;
        private String f3408h;
        private InitializationCallback<Fabric> f3409i;

        public Fabric(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.f3401a = context;
        }

        public Fabric m5310a(Kit... kitArr) {
            if (this.f3402b != null) {
                throw new IllegalStateException("Kits already set.");
            }
            this.f3402b = kitArr;
            return this;
        }

        public Fabric m5311a() {
            Map hashMap;
            if (this.f3403c == null) {
                this.f3403c = PriorityThreadPoolExecutor.m5563a();
            }
            if (this.f3404d == null) {
                this.f3404d = new Handler(Looper.getMainLooper());
            }
            if (this.f3405e == null) {
                if (this.f3406f) {
                    this.f3405e = new DefaultLogger(3);
                } else {
                    this.f3405e = new DefaultLogger();
                }
            }
            if (this.f3408h == null) {
                this.f3408h = this.f3401a.getPackageName();
            }
            if (this.f3409i == null) {
                this.f3409i = InitializationCallback.f3397d;
            }
            if (this.f3402b == null) {
                hashMap = new HashMap();
            } else {
                hashMap = Fabric.m5319b(Arrays.asList(this.f3402b));
            }
            return new Fabric(this.f3401a, hashMap, this.f3403c, this.f3404d, this.f3405e, this.f3406f, this.f3409i, new IdManager(this.f3401a, this.f3408h, this.f3407g, hashMap.values()));
        }
    }

    static {
        f3411b = new DefaultLogger();
    }

    static Fabric m5312a() {
        if (f3410a != null) {
            return f3410a;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    Fabric(Context context, Map<Class<? extends Kit>, Kit> map, PriorityThreadPoolExecutor priorityThreadPoolExecutor, Handler handler, Logger logger, boolean z, InitializationCallback initializationCallback, IdManager idManager) {
        this.f3414e = context.getApplicationContext();
        this.f3415f = map;
        this.f3416g = priorityThreadPoolExecutor;
        this.f3417h = handler;
        this.f3412c = logger;
        this.f3413d = z;
        this.f3418i = initializationCallback;
        this.f3423n = new AtomicBoolean(false);
        this.f3419j = m5326a(map.size());
        this.f3420k = idManager;
        m5325a(m5320c(context));
    }

    public static Fabric m5313a(Context context, Kit... kitArr) {
        if (f3410a == null) {
            synchronized (Fabric.class) {
                if (f3410a == null) {
                    Fabric.m5321c(new Fabric(context).m5310a(kitArr).m5311a());
                }
            }
        }
        return f3410a;
    }

    private static void m5321c(Fabric fabric) {
        f3410a = fabric;
        fabric.m5324j();
    }

    public Fabric m5325a(Activity activity) {
        this.f3422m = new WeakReference(activity);
        return this;
    }

    public Activity m5329b() {
        if (this.f3422m != null) {
            return (Activity) this.f3422m.get();
        }
        return null;
    }

    private void m5324j() {
        this.f3421l = new ActivityLifecycleManager(this.f3414e);
        this.f3421l.m5281a(new Fabric(this));
        m5327a(this.f3414e);
    }

    public String m5331c() {
        return "1.3.14.143";
    }

    public String m5332d() {
        return "io.fabric.sdk.android:fabric";
    }

    void m5327a(Context context) {
        StringBuilder append;
        Future b = m5330b(context);
        Collection g = m5335g();
        Onboarding onboarding = new Onboarding(b, g);
        List<Kit> arrayList = new ArrayList(g);
        Collections.sort(arrayList);
        onboarding.injectParameters(context, this, InitializationCallback.f3397d, this.f3420k);
        for (Kit injectParameters : arrayList) {
            injectParameters.injectParameters(context, this, this.f3419j, this.f3420k);
        }
        onboarding.initialize();
        if (Fabric.m5322h().m5286a("Fabric", 3)) {
            append = new StringBuilder("Initializing ").append(m5332d()).append(" [Version: ").append(m5331c()).append("], with the following kits:\n");
        } else {
            append = null;
        }
        for (Kit injectParameters2 : arrayList) {
            injectParameters2.initializationTask.m5358a(onboarding.initializationTask);
            m5328a(this.f3415f, injectParameters2);
            injectParameters2.initialize();
            if (append != null) {
                append.append(injectParameters2.getIdentifier()).append(" [Version: ").append(injectParameters2.getVersion()).append("]\n");
            }
        }
        if (append != null) {
            Fabric.m5322h().m5284a("Fabric", append.toString());
        }
    }

    void m5328a(Map<Class<? extends Kit>, Kit> map, Kit kit) {
        DependsOn dependsOn = kit.dependsOnAnnotation;
        if (dependsOn != null) {
            for (Class cls : dependsOn.m5556a()) {
                if (cls.isInterface()) {
                    for (Kit kit2 : map.values()) {
                        if (cls.isAssignableFrom(kit2.getClass())) {
                            kit.initializationTask.m5358a(kit2.initializationTask);
                        }
                    }
                } else if (((Kit) map.get(cls)) == null) {
                    throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                } else {
                    kit.initializationTask.m5358a(((Kit) map.get(cls)).initializationTask);
                }
            }
        }
    }

    private Activity m5320c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    public ActivityLifecycleManager m5333e() {
        return this.f3421l;
    }

    public ExecutorService m5334f() {
        return this.f3416g;
    }

    public Collection<Kit> m5335g() {
        return this.f3415f.values();
    }

    public static <T extends Kit> T m5314a(Class<T> cls) {
        return (Kit) Fabric.m5312a().f3415f.get(cls);
    }

    public static Logger m5322h() {
        if (f3410a == null) {
            return f3411b;
        }
        return f3410a.f3412c;
    }

    public static boolean m5323i() {
        if (f3410a == null) {
            return false;
        }
        return f3410a.f3413d;
    }

    private static Map<Class<? extends Kit>, Kit> m5319b(Collection<? extends Kit> collection) {
        Map hashMap = new HashMap(collection.size());
        Fabric.m5317a(hashMap, (Collection) collection);
        return hashMap;
    }

    private static void m5317a(Map<Class<? extends Kit>, Kit> map, Collection<? extends Kit> collection) {
        for (Kit kit : collection) {
            map.put(kit.getClass(), kit);
            if (kit instanceof KitGroup) {
                Fabric.m5317a((Map) map, ((KitGroup) kit).getKits());
            }
        }
    }

    InitializationCallback<?> m5326a(int i) {
        return new Fabric(this, i);
    }

    Future<Map<String, KitInfo>> m5330b(Context context) {
        return m5334f().submit(new FabricKitsFinder(context.getPackageCodePath()));
    }
}
