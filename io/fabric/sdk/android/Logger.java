package io.fabric.sdk.android;

/* renamed from: io.fabric.sdk.android.k */
public interface Logger {
    void m5282a(int i, String str, String str2);

    void m5283a(int i, String str, String str2, boolean z);

    void m5284a(String str, String str2);

    void m5285a(String str, String str2, Throwable th);

    boolean m5286a(String str, int i);

    void m5287b(String str, String str2);

    void m5288c(String str, String str2);

    void m5289d(String str, String str2);

    void m5290d(String str, String str2, Throwable th);

    void m5291e(String str, String str2);

    void m5292e(String str, String str2, Throwable th);
}
