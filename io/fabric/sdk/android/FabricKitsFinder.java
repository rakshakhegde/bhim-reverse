package io.fabric.sdk.android;

import android.os.SystemClock;
import android.text.TextUtils;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import java.io.Closeable;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* renamed from: io.fabric.sdk.android.e */
class FabricKitsFinder implements Callable<Map<String, KitInfo>> {
    final String f3426a;

    public /* synthetic */ Object call() {
        return m5337a();
    }

    FabricKitsFinder(String str) {
        this.f3426a = str;
    }

    public Map<String, KitInfo> m5337a() {
        Map<String, KitInfo> hashMap = new HashMap();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        ZipFile b = m5338b();
        Enumeration entries = b.entries();
        int i = 0;
        while (entries.hasMoreElements()) {
            int i2 = i + 1;
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("fabric/") && zipEntry.getName().length() > "fabric/".length()) {
                KitInfo a = m5336a(zipEntry, b);
                if (a != null) {
                    hashMap.put(a.m5367a(), a);
                    Fabric.m5322h().m5287b("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[]{a.m5367a(), a.m5368b()}));
                }
            }
            i = i2;
        }
        if (b != null) {
            try {
                b.close();
            } catch (IOException e) {
            }
        }
        Fabric.m5322h().m5287b("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - elapsedRealtime) + " reading:" + i);
        return hashMap;
    }

    private KitInfo m5336a(ZipEntry zipEntry, ZipFile zipFile) {
        Throwable e;
        Closeable inputStream;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                Object property = properties.getProperty("fabric-identifier");
                Object property2 = properties.getProperty("fabric-version");
                String property3 = properties.getProperty("fabric-build-type");
                if (TextUtils.isEmpty(property) || TextUtils.isEmpty(property2)) {
                    throw new IllegalStateException("Invalid format of fabric file," + zipEntry.getName());
                }
                KitInfo kitInfo = new KitInfo(property, property2, property3);
                CommonUtils.m5431a(inputStream);
                return kitInfo;
            } catch (IOException e2) {
                e = e2;
                try {
                    Fabric.m5322h().m5292e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
                    CommonUtils.m5431a(inputStream);
                    return null;
                } catch (Throwable th) {
                    e = th;
                    CommonUtils.m5431a(inputStream);
                    throw e;
                }
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            Fabric.m5322h().m5292e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
            CommonUtils.m5431a(inputStream);
            return null;
        } catch (Throwable th2) {
            e = th2;
            inputStream = null;
            CommonUtils.m5431a(inputStream);
            throw e;
        }
    }

    protected ZipFile m5338b() {
        return new ZipFile(this.f3426a);
    }
}
