package io.fabric.sdk.android;

import java.util.Collection;

/* renamed from: io.fabric.sdk.android.i */
public interface KitGroup {
    Collection<? extends Kit> getKits();
}
