package io.fabric.sdk.android;

import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.PriorityAsyncTask;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.p021b.TimingMetric;

/* renamed from: io.fabric.sdk.android.g */
class InitializationTask<Result> extends PriorityAsyncTask<Void, Void, Result> {
    final Kit<Result> f3442a;

    public InitializationTask(Kit<Result> kit) {
        this.f3442a = kit;
    }

    protected void m5364a() {
        super.m5350a();
        TimingMetric a = m5361a("onPreExecute");
        try {
            boolean onPreExecute = this.f3442a.onPreExecute();
            a.m5518b();
            if (!onPreExecute) {
                m5352a(true);
            }
        } catch (UnmetDependencyException e) {
            throw e;
        } catch (Throwable e2) {
            Fabric.m5322h().m5292e("Fabric", "Failure onPreExecute()", e2);
            a.m5518b();
            m5352a(true);
        } catch (Throwable th) {
            a.m5518b();
            m5352a(true);
        }
    }

    protected Result m5363a(Void... voidArr) {
        TimingMetric a = m5361a("doInBackground");
        Result result = null;
        if (!m5357d()) {
            result = this.f3442a.doInBackground();
        }
        a.m5518b();
        return result;
    }

    protected void m5365a(Result result) {
        this.f3442a.onPostExecute(result);
        this.f3442a.initializationCallback.m5307a((Object) result);
    }

    protected void m5366b(Result result) {
        this.f3442a.onCancelled(result);
        this.f3442a.initializationCallback.m5306a(new InitializationException(this.f3442a.getIdentifier() + " Initialization was cancelled"));
    }

    public Priority getPriority() {
        return Priority.HIGH;
    }

    private TimingMetric m5361a(String str) {
        TimingMetric timingMetric = new TimingMetric(this.f3442a.getIdentifier() + "." + str, "KitInitialization");
        timingMetric.m5517a();
        return timingMetric;
    }
}
