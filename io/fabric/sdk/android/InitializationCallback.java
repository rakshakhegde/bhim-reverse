package io.fabric.sdk.android;

/* renamed from: io.fabric.sdk.android.f */
public interface InitializationCallback<T> {
    public static final InitializationCallback f3397d;

    /* renamed from: io.fabric.sdk.android.f.a */
    public static class InitializationCallback implements InitializationCallback<Object> {
        private InitializationCallback() {
        }

        public void m5340a(Object obj) {
        }

        public void m5339a(Exception exception) {
        }
    }

    void m5306a(Exception exception);

    void m5307a(T t);

    static {
        f3397d = new InitializationCallback();
    }
}
