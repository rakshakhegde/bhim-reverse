package io.fabric.sdk.android;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.ApiKey;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.DeliveryMechanism;
import io.fabric.sdk.android.services.p023e.AppRequestData;
import io.fabric.sdk.android.services.p023e.AppSettingsData;
import io.fabric.sdk.android.services.p023e.CreateAppSpiCall;
import io.fabric.sdk.android.services.p023e.IconRequest;
import io.fabric.sdk.android.services.p023e.Settings;
import io.fabric.sdk.android.services.p023e.SettingsData;
import io.fabric.sdk.android.services.p023e.UpdateAppSpiCall;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

/* renamed from: io.fabric.sdk.android.l */
class Onboarding extends Kit<Boolean> {
    private final HttpRequestFactory f3446a;
    private PackageManager f3447b;
    private String f3448c;
    private PackageInfo f3449d;
    private String f3450e;
    private String f3451f;
    private String f3452g;
    private String f3453h;
    private String f3454i;
    private final Future<Map<String, KitInfo>> f3455j;
    private final Collection<Kit> f3456k;

    protected /* synthetic */ Object doInBackground() {
        return m5376a();
    }

    public Onboarding(Future<Map<String, KitInfo>> future, Collection<Kit> collection) {
        this.f3446a = new DefaultHttpRequestFactory();
        this.f3455j = future;
        this.f3456k = collection;
    }

    public String getVersion() {
        return "1.3.14.143";
    }

    protected boolean onPreExecute() {
        try {
            this.f3452g = getIdManager().m5480j();
            this.f3447b = getContext().getPackageManager();
            this.f3448c = getContext().getPackageName();
            this.f3449d = this.f3447b.getPackageInfo(this.f3448c, 0);
            this.f3450e = Integer.toString(this.f3449d.versionCode);
            this.f3451f = this.f3449d.versionName == null ? "0.0" : this.f3449d.versionName;
            this.f3453h = this.f3447b.getApplicationLabel(getContext().getApplicationInfo()).toString();
            this.f3454i = Integer.toString(getContext().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (Throwable e) {
            Fabric.m5322h().m5292e("Fabric", "Failed init", e);
            return false;
        }
    }

    protected Boolean m5376a() {
        boolean a;
        String k = CommonUtils.m5453k(getContext());
        SettingsData c = m5374c();
        if (c != null) {
            try {
                Map map;
                if (this.f3455j != null) {
                    map = (Map) this.f3455j.get();
                } else {
                    map = new HashMap();
                }
                a = m5372a(k, c.f3670a, m5377a(map, this.f3456k).values());
            } catch (Throwable e) {
                Fabric.m5322h().m5292e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(a);
        }
        a = false;
        return Boolean.valueOf(a);
    }

    private SettingsData m5374c() {
        try {
            Settings.m5615a().m5617a(this, this.idManager, this.f3446a, this.f3450e, this.f3451f, m5378b()).m5620c();
            return Settings.m5615a().m5619b();
        } catch (Throwable e) {
            Fabric.m5322h().m5292e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    Map<String, KitInfo> m5377a(Map<String, KitInfo> map, Collection<Kit> collection) {
        for (Kit kit : collection) {
            if (!map.containsKey(kit.getIdentifier())) {
                map.put(kit.getIdentifier(), new KitInfo(kit.getIdentifier(), kit.getVersion(), "binary"));
            }
        }
        return map;
    }

    public String getIdentifier() {
        return "io.fabric.sdk.android:fabric";
    }

    private boolean m5372a(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        if ("new".equals(appSettingsData.f3625b)) {
            if (m5373b(str, appSettingsData, collection)) {
                return Settings.m5615a().m5621d();
            }
            Fabric.m5322h().m5292e("Fabric", "Failed to create app with Crashlytics service.", null);
            return false;
        } else if ("configured".equals(appSettingsData.f3625b)) {
            return Settings.m5615a().m5621d();
        } else {
            if (!appSettingsData.f3628e) {
                return true;
            }
            Fabric.m5322h().m5284a("Fabric", "Server says an update is required - forcing a full App update.");
            m5375c(str, appSettingsData, collection);
            return true;
        }
    }

    private boolean m5373b(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        return new CreateAppSpiCall(this, m5378b(), appSettingsData.f3626c, this.f3446a).m5582a(m5370a(IconRequest.m5613a(getContext(), str), (Collection) collection));
    }

    private boolean m5375c(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        return m5371a(appSettingsData, IconRequest.m5613a(getContext(), str), (Collection) collection);
    }

    private boolean m5371a(AppSettingsData appSettingsData, IconRequest iconRequest, Collection<KitInfo> collection) {
        return new UpdateAppSpiCall(this, m5378b(), appSettingsData.f3626c, this.f3446a).m5623a(m5370a(iconRequest, (Collection) collection));
    }

    private AppRequestData m5370a(IconRequest iconRequest, Collection<KitInfo> collection) {
        return new AppRequestData(new ApiKey().m5407a(getContext()), getIdManager().m5473c(), this.f3451f, this.f3450e, CommonUtils.m5427a(CommonUtils.m5455m(r0)), this.f3453h, DeliveryMechanism.m5460a(this.f3452g).m5461a(), this.f3454i, "0", iconRequest, collection);
    }

    String m5378b() {
        return CommonUtils.m5440b(getContext(), "com.crashlytics.ApiEndpoint");
    }
}
