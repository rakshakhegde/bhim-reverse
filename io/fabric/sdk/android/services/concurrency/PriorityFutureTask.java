package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: io.fabric.sdk.android.services.concurrency.h */
public class PriorityFutureTask<V> extends FutureTask<V> implements Dependency<Task>, PriorityProvider, Task {
    final Object f3587b;

    public /* synthetic */ void addDependency(Object obj) {
        m5560a((Task) obj);
    }

    public PriorityFutureTask(Callable<V> callable) {
        super(callable);
        this.f3587b = m5559a((Object) callable);
    }

    public PriorityFutureTask(Runnable runnable, V v) {
        super(runnable, v);
        this.f3587b = m5559a((Object) runnable);
    }

    public int compareTo(Object obj) {
        return ((PriorityProvider) m5558a()).compareTo(obj);
    }

    public void m5560a(Task task) {
        ((Dependency) ((PriorityProvider) m5558a())).addDependency(task);
    }

    public Collection<Task> getDependencies() {
        return ((Dependency) ((PriorityProvider) m5558a())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((Dependency) ((PriorityProvider) m5558a())).areDependenciesMet();
    }

    public Priority getPriority() {
        return ((PriorityProvider) m5558a()).getPriority();
    }

    public void setFinished(boolean z) {
        ((Task) ((PriorityProvider) m5558a())).setFinished(z);
    }

    public boolean isFinished() {
        return ((Task) ((PriorityProvider) m5558a())).isFinished();
    }

    public void setError(Throwable th) {
        ((Task) ((PriorityProvider) m5558a())).setError(th);
    }

    public <T extends Dependency<Task> & PriorityProvider & Task> T m5558a() {
        return (Dependency) this.f3587b;
    }

    protected <T extends Dependency<Task> & PriorityProvider & Task> T m5559a(Object obj) {
        if (PriorityTask.isProperDelegate(obj)) {
            return (Dependency) obj;
        }
        return new PriorityTask();
    }
}
