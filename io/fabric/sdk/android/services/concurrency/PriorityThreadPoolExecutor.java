package io.fabric.sdk.android.services.concurrency;

import android.annotation.TargetApi;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: io.fabric.sdk.android.services.concurrency.k */
public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {
    private static final int f3592a;
    private static final int f3593b;
    private static final int f3594c;

    /* renamed from: io.fabric.sdk.android.services.concurrency.k.a */
    protected static final class PriorityThreadPoolExecutor implements ThreadFactory {
        private final int f3591a;

        public PriorityThreadPoolExecutor(int i) {
            this.f3591a = i;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.f3591a);
            thread.setName("Queue");
            return thread;
        }
    }

    public /* synthetic */ BlockingQueue getQueue() {
        return m5565b();
    }

    static {
        f3592a = Runtime.getRuntime().availableProcessors();
        f3593b = f3592a + 1;
        f3594c = (f3592a * 2) + 1;
    }

    <T extends Runnable & Dependency & Task & PriorityProvider> PriorityThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, DependencyPriorityBlockingQueue<T> dependencyPriorityBlockingQueue, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, dependencyPriorityBlockingQueue, threadFactory);
        prestartAllCoreThreads();
    }

    public static <T extends Runnable & Dependency & Task & PriorityProvider> PriorityThreadPoolExecutor m5564a(int i, int i2) {
        return new PriorityThreadPoolExecutor(i, i2, 1, TimeUnit.SECONDS, new DependencyPriorityBlockingQueue(), new PriorityThreadPoolExecutor(10));
    }

    public static PriorityThreadPoolExecutor m5563a() {
        return PriorityThreadPoolExecutor.m5564a(f3593b, f3594c);
    }

    protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new PriorityFutureTask(runnable, t);
    }

    protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new PriorityFutureTask(callable);
    }

    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (PriorityTask.isProperDelegate(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, null));
        }
    }

    protected void afterExecute(Runnable runnable, Throwable th) {
        Task task = (Task) runnable;
        task.setFinished(true);
        task.setError(th);
        m5565b().m5555d();
        super.afterExecute(runnable, th);
    }

    public DependencyPriorityBlockingQueue m5565b() {
        return (DependencyPriorityBlockingQueue) super.getQueue();
    }
}
