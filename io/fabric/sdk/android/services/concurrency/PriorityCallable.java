package io.fabric.sdk.android.services.concurrency;

import java.util.concurrent.Callable;

/* renamed from: io.fabric.sdk.android.services.concurrency.g */
public abstract class PriorityCallable<V> extends PriorityTask implements Callable<V> {
}
