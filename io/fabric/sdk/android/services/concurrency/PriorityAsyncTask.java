package io.fabric.sdk.android.services.concurrency;

import io.fabric.sdk.android.services.concurrency.AsyncTask.AsyncTask;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

/* renamed from: io.fabric.sdk.android.services.concurrency.f */
public abstract class PriorityAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements Dependency<Task>, PriorityProvider, Task {
    private final PriorityTask f3441a;

    /* renamed from: io.fabric.sdk.android.services.concurrency.f.a */
    private static class PriorityAsyncTask<Result> implements Executor {
        private final Executor f3589a;
        private final PriorityAsyncTask f3590b;

        /* renamed from: io.fabric.sdk.android.services.concurrency.f.a.1 */
        class PriorityAsyncTask extends PriorityFutureTask<Result> {
            final /* synthetic */ PriorityAsyncTask f3588a;

            PriorityAsyncTask(PriorityAsyncTask priorityAsyncTask, Runnable runnable, Object obj) {
                this.f3588a = priorityAsyncTask;
                super(runnable, obj);
            }

            public <T extends Dependency<Task> & PriorityProvider & Task> T m5561a() {
                return this.f3588a.f3590b;
            }
        }

        public PriorityAsyncTask(Executor executor, PriorityAsyncTask priorityAsyncTask) {
            this.f3589a = executor;
            this.f3590b = priorityAsyncTask;
        }

        public void execute(Runnable runnable) {
            this.f3589a.execute(new PriorityAsyncTask(this, runnable, null));
        }
    }

    public /* synthetic */ void addDependency(Object obj) {
        m5358a((Task) obj);
    }

    public PriorityAsyncTask() {
        this.f3441a = new PriorityTask();
    }

    public final void m5359a(ExecutorService executorService, Params... paramsArr) {
        super.m5348a(new PriorityAsyncTask(executorService, this), (Object[]) paramsArr);
    }

    public int compareTo(Object obj) {
        return Priority.m5557a(this, obj);
    }

    public void m5358a(Task task) {
        if (m5353b() != AsyncTask.PENDING) {
            throw new IllegalStateException("Must not add Dependency after task is running");
        }
        ((Dependency) ((PriorityProvider) m5360e())).addDependency(task);
    }

    public Collection<Task> getDependencies() {
        return ((Dependency) ((PriorityProvider) m5360e())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((Dependency) ((PriorityProvider) m5360e())).areDependenciesMet();
    }

    public Priority getPriority() {
        return ((PriorityProvider) m5360e()).getPriority();
    }

    public void setFinished(boolean z) {
        ((Task) ((PriorityProvider) m5360e())).setFinished(z);
    }

    public boolean isFinished() {
        return ((Task) ((PriorityProvider) m5360e())).isFinished();
    }

    public void setError(Throwable th) {
        ((Task) ((PriorityProvider) m5360e())).setError(th);
    }

    public <T extends Dependency<Task> & PriorityProvider & Task> T m5360e() {
        return this.f3441a;
    }
}
