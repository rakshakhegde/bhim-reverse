package io.fabric.sdk.android.services.concurrency.p020a;

/* renamed from: io.fabric.sdk.android.services.concurrency.a.e */
public class RetryState {
    private final int f3577a;
    private final Backoff f3578b;
    private final RetryPolicy f3579c;

    public RetryState(Backoff backoff, RetryPolicy retryPolicy) {
        this(0, backoff, retryPolicy);
    }

    public RetryState(int i, Backoff backoff, RetryPolicy retryPolicy) {
        this.f3577a = i;
        this.f3578b = backoff;
        this.f3579c = retryPolicy;
    }

    public long m5543a() {
        return this.f3578b.getDelayMillis(this.f3577a);
    }

    public RetryState m5544b() {
        return new RetryState(this.f3577a + 1, this.f3578b, this.f3579c);
    }

    public RetryState m5545c() {
        return new RetryState(this.f3578b, this.f3579c);
    }
}
