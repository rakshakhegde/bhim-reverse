package io.fabric.sdk.android.services.concurrency.p020a;

/* renamed from: io.fabric.sdk.android.services.concurrency.a.b */
public class DefaultRetryPolicy implements RetryPolicy {
    private final int f3574a;

    public DefaultRetryPolicy() {
        this(1);
    }

    public DefaultRetryPolicy(int i) {
        this.f3574a = i;
    }
}
