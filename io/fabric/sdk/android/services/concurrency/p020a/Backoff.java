package io.fabric.sdk.android.services.concurrency.p020a;

/* renamed from: io.fabric.sdk.android.services.concurrency.a.a */
public interface Backoff {
    long getDelayMillis(int i);
}
