package io.fabric.sdk.android.services.concurrency.p020a;

/* renamed from: io.fabric.sdk.android.services.concurrency.a.c */
public class ExponentialBackoff implements Backoff {
    private final long f3575a;
    private final int f3576b;

    public ExponentialBackoff(long j, int i) {
        this.f3575a = j;
        this.f3576b = i;
    }

    public long getDelayMillis(int i) {
        return (long) (((double) this.f3575a) * Math.pow((double) this.f3576b, (double) i));
    }
}
