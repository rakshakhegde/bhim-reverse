package io.fabric.sdk.android.services.concurrency;

/* renamed from: io.fabric.sdk.android.services.concurrency.l */
public interface Task {
    boolean isFinished();

    void setError(Throwable th);

    void setFinished(boolean z);
}
