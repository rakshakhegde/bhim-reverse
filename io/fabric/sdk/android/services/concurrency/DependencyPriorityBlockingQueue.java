package io.fabric.sdk.android.services.concurrency;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: io.fabric.sdk.android.services.concurrency.c */
public class DependencyPriorityBlockingQueue<E extends Dependency & Task & PriorityProvider> extends PriorityBlockingQueue<E> {
    final Queue<E> f3580a;
    private final ReentrantLock f3581b;

    public /* synthetic */ Object peek() {
        return m5552b();
    }

    public /* synthetic */ Object poll() {
        return m5554c();
    }

    public /* synthetic */ Object poll(long j, TimeUnit timeUnit) {
        return m5548a(j, timeUnit);
    }

    public /* synthetic */ Object take() {
        return m5546a();
    }

    public DependencyPriorityBlockingQueue() {
        this.f3580a = new LinkedList();
        this.f3581b = new ReentrantLock();
    }

    public E m5546a() {
        return m5553b(0, null, null);
    }

    public E m5552b() {
        E e = null;
        try {
            e = m5553b(1, null, null);
        } catch (InterruptedException e2) {
        }
        return e;
    }

    public E m5548a(long j, TimeUnit timeUnit) {
        return m5553b(3, Long.valueOf(j), timeUnit);
    }

    public E m5554c() {
        E e = null;
        try {
            e = m5553b(2, null, null);
        } catch (InterruptedException e2) {
        }
        return e;
    }

    public int size() {
        try {
            this.f3581b.lock();
            int size = this.f3580a.size() + super.size();
            return size;
        } finally {
            this.f3581b.unlock();
        }
    }

    public <T> T[] toArray(T[] tArr) {
        try {
            this.f3581b.lock();
            T[] a = m5551a(super.toArray(tArr), this.f3580a.toArray(tArr));
            return a;
        } finally {
            this.f3581b.unlock();
        }
    }

    public Object[] toArray() {
        try {
            this.f3581b.lock();
            Object[] a = m5551a(super.toArray(), this.f3580a.toArray());
            return a;
        } finally {
            this.f3581b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        try {
            this.f3581b.lock();
            int drainTo = super.drainTo(collection) + this.f3580a.size();
            while (!this.f3580a.isEmpty()) {
                collection.add(this.f3580a.poll());
            }
            return drainTo;
        } finally {
            this.f3581b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection, int i) {
        try {
            this.f3581b.lock();
            int drainTo = super.drainTo(collection, i);
            while (!this.f3580a.isEmpty() && drainTo <= i) {
                collection.add(this.f3580a.poll());
                drainTo++;
            }
            this.f3581b.unlock();
            return drainTo;
        } catch (Throwable th) {
            this.f3581b.unlock();
        }
    }

    public boolean contains(Object obj) {
        try {
            this.f3581b.lock();
            boolean z = super.contains(obj) || this.f3580a.contains(obj);
            this.f3581b.unlock();
            return z;
        } catch (Throwable th) {
            this.f3581b.unlock();
        }
    }

    public void clear() {
        try {
            this.f3581b.lock();
            this.f3580a.clear();
            super.clear();
        } finally {
            this.f3581b.unlock();
        }
    }

    public boolean remove(Object obj) {
        try {
            this.f3581b.lock();
            boolean z = super.remove(obj) || this.f3580a.remove(obj);
            this.f3581b.unlock();
            return z;
        } catch (Throwable th) {
            this.f3581b.unlock();
        }
    }

    public boolean removeAll(Collection<?> collection) {
        try {
            this.f3581b.lock();
            boolean removeAll = super.removeAll(collection) | this.f3580a.removeAll(collection);
            return removeAll;
        } finally {
            this.f3581b.unlock();
        }
    }

    E m5547a(int i, Long l, TimeUnit timeUnit) {
        switch (i) {
            case R.View_android_theme /*0*/:
                return (Dependency) super.take();
            case R.View_android_focusable /*1*/:
                return (Dependency) super.peek();
            case R.View_paddingStart /*2*/:
                return (Dependency) super.poll();
            case R.View_paddingEnd /*3*/:
                return (Dependency) super.poll(l.longValue(), timeUnit);
            default:
                return null;
        }
    }

    boolean m5549a(int i, E e) {
        try {
            this.f3581b.lock();
            if (i == 1) {
                super.remove(e);
            }
            boolean offer = this.f3580a.offer(e);
            return offer;
        } finally {
            this.f3581b.unlock();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    E m5553b(int r3, java.lang.Long r4, java.util.concurrent.TimeUnit r5) {
        /*
        r2 = this;
    L_0x0000:
        r0 = r2.m5547a(r3, r4, r5);
        if (r0 == 0) goto L_0x000c;
    L_0x0006:
        r1 = r2.m5550a(r0);
        if (r1 == 0) goto L_0x000d;
    L_0x000c:
        return r0;
    L_0x000d:
        r2.m5549a(r3, r0);
        goto L_0x0000;
        */
        throw new UnsupportedOperationException("Method not decompiled: io.fabric.sdk.android.services.concurrency.c.b(int, java.lang.Long, java.util.concurrent.TimeUnit):E");
    }

    boolean m5550a(E e) {
        return e.areDependenciesMet();
    }

    public void m5555d() {
        try {
            this.f3581b.lock();
            Iterator it = this.f3580a.iterator();
            while (it.hasNext()) {
                Dependency dependency = (Dependency) it.next();
                if (m5550a(dependency)) {
                    super.offer(dependency);
                    it.remove();
                }
            }
        } finally {
            this.f3581b.unlock();
        }
    }

    <T> T[] m5551a(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        Object[] objArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, objArr, 0, length);
        System.arraycopy(tArr2, 0, objArr, length, length2);
        return objArr;
    }
}
