package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;

/* renamed from: io.fabric.sdk.android.services.concurrency.b */
public interface Dependency<T> {
    void addDependency(T t);

    boolean areDependenciesMet();

    Collection<T> getDependencies();
}
