package io.fabric.sdk.android.services.concurrency;

/* renamed from: io.fabric.sdk.android.services.concurrency.i */
public interface PriorityProvider<T> extends Comparable<T> {
    Priority getPriority();
}
