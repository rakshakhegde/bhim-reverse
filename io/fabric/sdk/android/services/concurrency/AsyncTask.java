package io.fabric.sdk.android.services.concurrency;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: io.fabric.sdk.android.services.concurrency.a */
public abstract class AsyncTask<Params, Progress, Result> {
    private static final int f3427a;
    public static final Executor f3428b;
    public static final Executor f3429c;
    private static final int f3430d;
    private static final int f3431e;
    private static final ThreadFactory f3432f;
    private static final BlockingQueue<Runnable> f3433g;
    private static final AsyncTask f3434h;
    private static volatile Executor f3435i;
    private final AsyncTask<Params, Result> f3436j;
    private final FutureTask<Result> f3437k;
    private volatile AsyncTask f3438l;
    private final AtomicBoolean f3439m;
    private final AtomicBoolean f3440n;

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.1 */
    static class AsyncTask implements ThreadFactory {
        private final AtomicInteger f3559a;

        AsyncTask() {
            this.f3559a = new AtomicInteger(1);
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.f3559a.getAndIncrement());
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.e */
    private static abstract class AsyncTask<Params, Result> implements Callable<Result> {
        Params[] f3560b;

        private AsyncTask() {
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.2 */
    class AsyncTask extends AsyncTask<Params, Result> {
        final /* synthetic */ AsyncTask f3561a;

        AsyncTask(AsyncTask asyncTask) {
            this.f3561a = asyncTask;
            super();
        }

        public Result call() {
            this.f3561a.f3440n.set(true);
            Process.setThreadPriority(10);
            return this.f3561a.m5346d(this.f3561a.m5349a(this.b));
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.3 */
    class AsyncTask extends FutureTask<Result> {
        final /* synthetic */ AsyncTask f3562a;

        AsyncTask(AsyncTask asyncTask, Callable callable) {
            this.f3562a = asyncTask;
            super(callable);
        }

        protected void done() {
            try {
                this.f3562a.m5345c(get());
            } catch (Throwable e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                this.f3562a.m5345c(null);
            }
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.4 */
    static /* synthetic */ class AsyncTask {
        static final /* synthetic */ int[] f3563a;

        static {
            f3563a = new int[AsyncTask.values().length];
            try {
                f3563a[AsyncTask.RUNNING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3563a[AsyncTask.FINISHED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.a */
    private static class AsyncTask<Data> {
        final AsyncTask f3564a;
        final Data[] f3565b;

        AsyncTask(AsyncTask asyncTask, Data... dataArr) {
            this.f3564a = asyncTask;
            this.f3565b = dataArr;
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.b */
    private static class AsyncTask extends Handler {
        public AsyncTask() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            AsyncTask asyncTask = (AsyncTask) message.obj;
            switch (message.what) {
                case R.View_android_focusable /*1*/:
                    asyncTask.f3564a.m5347e(asyncTask.f3565b[0]);
                case R.View_paddingStart /*2*/:
                    asyncTask.f3564a.m5355b(asyncTask.f3565b);
                default:
            }
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.c */
    private static class AsyncTask implements Executor {
        final LinkedList<Runnable> f3568a;
        Runnable f3569b;

        /* renamed from: io.fabric.sdk.android.services.concurrency.a.c.1 */
        class AsyncTask implements Runnable {
            final /* synthetic */ Runnable f3566a;
            final /* synthetic */ AsyncTask f3567b;

            AsyncTask(AsyncTask asyncTask, Runnable runnable) {
                this.f3567b = asyncTask;
                this.f3566a = runnable;
            }

            public void run() {
                try {
                    this.f3566a.run();
                } finally {
                    this.f3567b.m5542a();
                }
            }
        }

        private AsyncTask() {
            this.f3568a = new LinkedList();
        }

        public synchronized void execute(Runnable runnable) {
            this.f3568a.offer(new AsyncTask(this, runnable));
            if (this.f3569b == null) {
                m5542a();
            }
        }

        protected synchronized void m5542a() {
            Runnable runnable = (Runnable) this.f3568a.poll();
            this.f3569b = runnable;
            if (runnable != null) {
                AsyncTask.f3428b.execute(this.f3569b);
            }
        }
    }

    /* renamed from: io.fabric.sdk.android.services.concurrency.a.d */
    public enum AsyncTask {
        PENDING,
        RUNNING,
        FINISHED
    }

    protected abstract Result m5349a(Params... paramsArr);

    static {
        f3427a = Runtime.getRuntime().availableProcessors();
        f3430d = f3427a + 1;
        f3431e = (f3427a * 2) + 1;
        f3432f = new AsyncTask();
        f3433g = new LinkedBlockingQueue(128);
        f3428b = new ThreadPoolExecutor(f3430d, f3431e, 1, TimeUnit.SECONDS, f3433g, f3432f);
        f3429c = new AsyncTask();
        f3434h = new AsyncTask();
        f3435i = f3429c;
    }

    public AsyncTask() {
        this.f3438l = AsyncTask.PENDING;
        this.f3439m = new AtomicBoolean();
        this.f3440n = new AtomicBoolean();
        this.f3436j = new AsyncTask(this);
        this.f3437k = new AsyncTask(this, this.f3436j);
    }

    private void m5345c(Result result) {
        if (!this.f3440n.get()) {
            m5346d(result);
        }
    }

    private Result m5346d(Result result) {
        f3434h.obtainMessage(1, new AsyncTask(this, result)).sendToTarget();
        return result;
    }

    public final AsyncTask m5353b() {
        return this.f3438l;
    }

    protected void m5350a() {
    }

    protected void m5351a(Result result) {
    }

    protected void m5355b(Progress... progressArr) {
    }

    protected void m5354b(Result result) {
        m5356c();
    }

    protected void m5356c() {
    }

    public final boolean m5357d() {
        return this.f3439m.get();
    }

    public final boolean m5352a(boolean z) {
        this.f3439m.set(true);
        return this.f3437k.cancel(z);
    }

    public final AsyncTask<Params, Progress, Result> m5348a(Executor executor, Params... paramsArr) {
        if (this.f3438l != AsyncTask.PENDING) {
            switch (AsyncTask.f3563a[this.f3438l.ordinal()]) {
                case R.View_android_focusable /*1*/:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case R.View_paddingStart /*2*/:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.f3438l = AsyncTask.RUNNING;
        m5350a();
        this.f3436j.f3560b = paramsArr;
        executor.execute(this.f3437k);
        return this;
    }

    private void m5347e(Result result) {
        if (m5357d()) {
            m5354b((Object) result);
        } else {
            m5351a((Object) result);
        }
        this.f3438l = AsyncTask.FINISHED;
    }
}
