package io.fabric.sdk.android.services.p038d;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import java.io.File;

/* renamed from: io.fabric.sdk.android.services.d.b */
public class FileStoreImpl implements FileStore {
    private final Context f3595a;
    private final String f3596b;
    private final String f3597c;

    public FileStoreImpl(Kit kit) {
        if (kit.getContext() == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f3595a = kit.getContext();
        this.f3596b = kit.getPath();
        this.f3597c = "Android/" + this.f3595a.getPackageName();
    }

    public File m5567a() {
        return m5568a(this.f3595a.getFilesDir());
    }

    File m5568a(File file) {
        if (file == null) {
            Fabric.m5322h().m5284a("Fabric", "Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            Fabric.m5322h().m5289d("Fabric", "Couldn't create file");
        }
        return null;
    }
}
