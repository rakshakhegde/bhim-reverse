package io.fabric.sdk.android.services.p038d;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import io.fabric.sdk.android.Kit;

/* renamed from: io.fabric.sdk.android.services.d.d */
public class PreferenceStoreImpl implements PreferenceStore {
    private final SharedPreferences f3598a;
    private final String f3599b;
    private final Context f3600c;

    public PreferenceStoreImpl(Context context, String str) {
        if (context == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f3600c = context;
        this.f3599b = str;
        this.f3598a = this.f3600c.getSharedPreferences(this.f3599b, 0);
    }

    @Deprecated
    public PreferenceStoreImpl(Kit kit) {
        this(kit.getContext(), kit.getClass().getName());
    }

    public SharedPreferences m5572a() {
        return this.f3598a;
    }

    public Editor m5574b() {
        return this.f3598a.edit();
    }

    @TargetApi(9)
    public boolean m5573a(Editor editor) {
        if (VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
