package io.fabric.sdk.android.services.p038d;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/* renamed from: io.fabric.sdk.android.services.d.c */
public interface PreferenceStore {
    SharedPreferences m5569a();

    boolean m5570a(Editor editor);

    Editor m5571b();
}
