package io.fabric.sdk.android.services.p023e;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.p021b.CommonUtils;

/* renamed from: io.fabric.sdk.android.services.e.n */
public class IconRequest {
    public final String f3644a;
    public final int f3645b;
    public final int f3646c;
    public final int f3647d;

    public IconRequest(String str, int i, int i2, int i3) {
        this.f3644a = str;
        this.f3645b = i;
        this.f3646c = i2;
        this.f3647d = i3;
    }

    public static IconRequest m5613a(Context context, String str) {
        if (str != null) {
            try {
                int l = CommonUtils.m5454l(context);
                Fabric.m5322h().m5284a("Fabric", "App icon resource ID is " + l);
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), l, options);
                return new IconRequest(str, l, options.outWidth, options.outHeight);
            } catch (Throwable e) {
                Fabric.m5322h().m5292e("Fabric", "Failed to load icon", e);
            }
        }
        return null;
    }
}
