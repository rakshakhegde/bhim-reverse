package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.w */
public class SettingsRequest {
    public final String f3680a;
    public final String f3681b;
    public final String f3682c;
    public final String f3683d;
    public final String f3684e;
    public final String f3685f;
    public final String f3686g;
    public final String f3687h;
    public final String f3688i;
    public final String f3689j;
    public final int f3690k;
    public final String f3691l;

    public SettingsRequest(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, int i, String str11) {
        this.f3680a = str;
        this.f3681b = str2;
        this.f3682c = str3;
        this.f3683d = str4;
        this.f3684e = str5;
        this.f3685f = str6;
        this.f3686g = str7;
        this.f3687h = str8;
        this.f3688i = str9;
        this.f3689j = str10;
        this.f3690k = i;
        this.f3691l = str11;
    }
}
