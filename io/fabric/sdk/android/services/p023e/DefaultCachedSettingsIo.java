package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p038d.FileStoreImpl;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import org.json.JSONObject;

/* renamed from: io.fabric.sdk.android.services.e.i */
class DefaultCachedSettingsIo implements CachedSettingsIo {
    private final Kit f3632a;

    public DefaultCachedSettingsIo(Kit kit) {
        this.f3632a = kit;
    }

    public JSONObject m5583a() {
        Closeable fileInputStream;
        Throwable e;
        Closeable closeable = null;
        Fabric.m5322h().m5284a("Fabric", "Reading cached settings...");
        try {
            JSONObject jSONObject;
            File file = new File(new FileStoreImpl(this.f3632a).m5567a(), "com.crashlytics.settings.json");
            if (file.exists()) {
                fileInputStream = new FileInputStream(file);
                try {
                    jSONObject = new JSONObject(CommonUtils.m5421a((InputStream) fileInputStream));
                    closeable = fileInputStream;
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Fabric.m5322h().m5292e("Fabric", "Failed to fetch cached settings", e);
                        CommonUtils.m5432a(fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        e = th;
                        closeable = fileInputStream;
                        CommonUtils.m5432a(closeable, "Error while closing settings cache file.");
                        throw e;
                    }
                }
            }
            Fabric.m5322h().m5284a("Fabric", "No cached settings found.");
            jSONObject = null;
            CommonUtils.m5432a(closeable, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            Fabric.m5322h().m5292e("Fabric", "Failed to fetch cached settings", e);
            CommonUtils.m5432a(fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            e = th2;
            CommonUtils.m5432a(closeable, "Error while closing settings cache file.");
            throw e;
        }
    }

    public void m5584a(long j, JSONObject jSONObject) {
        Throwable e;
        Fabric.m5322h().m5284a("Fabric", "Writing settings to cache file...");
        if (jSONObject != null) {
            Closeable closeable = null;
            Closeable fileWriter;
            try {
                jSONObject.put("expires_at", j);
                fileWriter = new FileWriter(new File(new FileStoreImpl(this.f3632a).m5567a(), "com.crashlytics.settings.json"));
                try {
                    fileWriter.write(jSONObject.toString());
                    fileWriter.flush();
                    CommonUtils.m5432a(fileWriter, "Failed to close settings writer.");
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Fabric.m5322h().m5292e("Fabric", "Failed to cache settings", e);
                        CommonUtils.m5432a(fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        e = th;
                        closeable = fileWriter;
                        CommonUtils.m5432a(closeable, "Failed to close settings writer.");
                        throw e;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                fileWriter = null;
                Fabric.m5322h().m5292e("Fabric", "Failed to cache settings", e);
                CommonUtils.m5432a(fileWriter, "Failed to close settings writer.");
            } catch (Throwable th2) {
                e = th2;
                CommonUtils.m5432a(closeable, "Failed to close settings writer.");
                throw e;
            }
        }
    }
}
