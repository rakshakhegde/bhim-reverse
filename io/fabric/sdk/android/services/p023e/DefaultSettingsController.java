package io.fabric.sdk.android.services.p023e;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.CurrentTimeProvider;
import io.fabric.sdk.android.services.p038d.PreferenceStore;
import io.fabric.sdk.android.services.p038d.PreferenceStoreImpl;
import org.json.JSONObject;

/* renamed from: io.fabric.sdk.android.services.e.j */
class DefaultSettingsController implements SettingsController {
    private final SettingsRequest f3633a;
    private final SettingsJsonTransform f3634b;
    private final CurrentTimeProvider f3635c;
    private final CachedSettingsIo f3636d;
    private final SettingsSpiCall f3637e;
    private final Kit f3638f;
    private final PreferenceStore f3639g;

    public DefaultSettingsController(Kit kit, SettingsRequest settingsRequest, CurrentTimeProvider currentTimeProvider, SettingsJsonTransform settingsJsonTransform, CachedSettingsIo cachedSettingsIo, SettingsSpiCall settingsSpiCall) {
        this.f3638f = kit;
        this.f3633a = settingsRequest;
        this.f3635c = currentTimeProvider;
        this.f3634b = settingsJsonTransform;
        this.f3636d = cachedSettingsIo;
        this.f3637e = settingsSpiCall;
        this.f3639g = new PreferenceStoreImpl(this.f3638f);
    }

    public SettingsData m5589a() {
        return m5590a(SettingsCacheBehavior.USE_CACHE);
    }

    public SettingsData m5590a(SettingsCacheBehavior settingsCacheBehavior) {
        Throwable th;
        SettingsData settingsData;
        Throwable th2;
        SettingsData settingsData2 = null;
        try {
            if (!(Fabric.m5323i() || m5594d())) {
                settingsData2 = m5588b(settingsCacheBehavior);
            }
            if (settingsData2 == null) {
                try {
                    JSONObject a = this.f3637e.m5605a(this.f3633a);
                    if (a != null) {
                        settingsData2 = this.f3634b.m5595a(this.f3635c, a);
                        this.f3636d.m5581a(settingsData2.f3676g, a);
                        m5587a(a, "Loaded settings: ");
                        m5591a(m5592b());
                    }
                } catch (Throwable e) {
                    th = e;
                    settingsData = settingsData2;
                    th2 = th;
                    Fabric.m5322h().m5292e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", th2);
                    return settingsData;
                }
            }
            settingsData = settingsData2;
            if (settingsData == null) {
                try {
                    settingsData = m5588b(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
                } catch (Exception e2) {
                    th2 = e2;
                    Fabric.m5322h().m5292e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", th2);
                    return settingsData;
                }
            }
        } catch (Throwable e3) {
            th = e3;
            settingsData = null;
            th2 = th;
            Fabric.m5322h().m5292e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", th2);
            return settingsData;
        }
        return settingsData;
    }

    private SettingsData m5588b(SettingsCacheBehavior settingsCacheBehavior) {
        Throwable th;
        SettingsData settingsData = null;
        try {
            if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                return null;
            }
            JSONObject a = this.f3636d.m5580a();
            if (a != null) {
                SettingsData a2 = this.f3634b.m5595a(this.f3635c, a);
                if (a2 != null) {
                    m5587a(a, "Loaded cached settings: ");
                    long a3 = this.f3635c.m5459a();
                    if (SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior) || !a2.m5622a(a3)) {
                        try {
                            Fabric.m5322h().m5284a("Fabric", "Returning cached settings.");
                            return a2;
                        } catch (Throwable e) {
                            Throwable th2 = e;
                            settingsData = a2;
                            th = th2;
                            Fabric.m5322h().m5292e("Fabric", "Failed to get cached settings", th);
                            return settingsData;
                        }
                    }
                    Fabric.m5322h().m5284a("Fabric", "Cached settings have expired.");
                    return null;
                }
                Fabric.m5322h().m5292e("Fabric", "Failed to transform cached settings data.", null);
                return null;
            }
            Fabric.m5322h().m5284a("Fabric", "No cached settings data found.");
            return null;
        } catch (Exception e2) {
            th = e2;
            Fabric.m5322h().m5292e("Fabric", "Failed to get cached settings", th);
            return settingsData;
        }
    }

    private void m5587a(JSONObject jSONObject, String str) {
        Fabric.m5322h().m5284a("Fabric", str + jSONObject.toString());
    }

    String m5592b() {
        return CommonUtils.m5427a(CommonUtils.m5455m(this.f3638f.getContext()));
    }

    String m5593c() {
        return this.f3639g.m5569a().getString("existing_instance_identifier", BuildConfig.FLAVOR);
    }

    @SuppressLint({"CommitPrefEdits"})
    boolean m5591a(String str) {
        Editor b = this.f3639g.m5571b();
        b.putString("existing_instance_identifier", str);
        return this.f3639g.m5570a(b);
    }

    boolean m5594d() {
        return !m5593c().equals(m5592b());
    }
}
