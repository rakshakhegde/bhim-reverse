package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequestFactory;

/* renamed from: io.fabric.sdk.android.services.e.h */
public class CreateAppSpiCall extends AbstractAppSpiCall {
    public /* bridge */ /* synthetic */ boolean m5582a(AppRequestData appRequestData) {
        return super.m5578a(appRequestData);
    }

    public CreateAppSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
    }
}
