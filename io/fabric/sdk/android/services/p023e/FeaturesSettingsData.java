package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.m */
public class FeaturesSettingsData {
    public final boolean f3640a;
    public final boolean f3641b;
    public final boolean f3642c;
    public final boolean f3643d;

    public FeaturesSettingsData(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f3640a = z;
        this.f3641b = z2;
        this.f3642c = z3;
        this.f3643d = z4;
    }
}
