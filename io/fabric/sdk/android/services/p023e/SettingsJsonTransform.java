package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.services.p021b.CurrentTimeProvider;
import org.json.JSONObject;

/* renamed from: io.fabric.sdk.android.services.e.v */
public interface SettingsJsonTransform {
    SettingsData m5595a(CurrentTimeProvider currentTimeProvider, JSONObject jSONObject);
}
