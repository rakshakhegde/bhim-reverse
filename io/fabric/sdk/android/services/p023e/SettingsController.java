package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.s */
public interface SettingsController {
    SettingsData m5585a();

    SettingsData m5586a(SettingsCacheBehavior settingsCacheBehavior);
}
