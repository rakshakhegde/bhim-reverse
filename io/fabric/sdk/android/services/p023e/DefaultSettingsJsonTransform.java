package io.fabric.sdk.android.services.p023e;

import com.crashlytics.android.beta.BuildConfig;
import io.fabric.sdk.android.services.p019c.EventsFilesManager;
import io.fabric.sdk.android.services.p021b.CurrentTimeProvider;
import org.json.JSONObject;

/* renamed from: io.fabric.sdk.android.services.e.k */
class DefaultSettingsJsonTransform implements SettingsJsonTransform {
    DefaultSettingsJsonTransform() {
    }

    public SettingsData m5604a(CurrentTimeProvider currentTimeProvider, JSONObject jSONObject) {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new SettingsData(m5596a(currentTimeProvider, (long) optInt2, jSONObject), m5597a(jSONObject.getJSONObject("app")), m5601e(jSONObject.getJSONObject("session")), m5602f(jSONObject.getJSONObject("prompt")), m5599c(jSONObject.getJSONObject("features")), m5600d(jSONObject.getJSONObject("analytics")), m5603g(jSONObject.getJSONObject(BuildConfig.ARTIFACT_ID)), optInt, optInt2);
    }

    private AppSettingsData m5597a(JSONObject jSONObject) {
        String string = jSONObject.getString("identifier");
        String string2 = jSONObject.getString("status");
        String string3 = jSONObject.getString("url");
        String string4 = jSONObject.getString("reports_url");
        boolean optBoolean = jSONObject.optBoolean("update_required", false);
        AppIconSettingsData appIconSettingsData = null;
        if (jSONObject.has("icon") && jSONObject.getJSONObject("icon").has("hash")) {
            appIconSettingsData = m5598b(jSONObject.getJSONObject("icon"));
        }
        return new AppSettingsData(string, string2, string3, string4, optBoolean, appIconSettingsData);
    }

    private AppIconSettingsData m5598b(JSONObject jSONObject) {
        return new AppIconSettingsData(jSONObject.getString("hash"), jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    private FeaturesSettingsData m5599c(JSONObject jSONObject) {
        return new FeaturesSettingsData(jSONObject.optBoolean("prompt_enabled", false), jSONObject.optBoolean("collect_logged_exceptions", true), jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_analytics", false));
    }

    private AnalyticsSettingsData m5600d(JSONObject jSONObject) {
        return new AnalyticsSettingsData(jSONObject.optString("url", "https://e.crashlytics.com/spi/v2/events"), jSONObject.optInt("flush_interval_secs", 600), jSONObject.optInt("max_byte_size_per_file", EventsFilesManager.MAX_BYTE_SIZE_PER_FILE), jSONObject.optInt("max_file_count_per_send", 1), jSONObject.optInt("max_pending_send_file_count", 100), jSONObject.optBoolean("track_custom_events", true), jSONObject.optBoolean("track_predefined_events", true), jSONObject.optInt("sampling_rate", 1), jSONObject.optBoolean("flush_on_background", true));
    }

    private SessionSettingsData m5601e(JSONObject jSONObject) {
        return new SessionSettingsData(jSONObject.optInt("log_buffer_size", 64000), jSONObject.optInt("max_chained_exception_depth", 8), jSONObject.optInt("max_custom_exception_events", 64), jSONObject.optInt("max_custom_key_value_pairs", 64), jSONObject.optInt("identifier_mask", 255), jSONObject.optBoolean("send_session_without_crash", false));
    }

    private PromptSettingsData m5602f(JSONObject jSONObject) {
        return new PromptSettingsData(jSONObject.optString("title", "Send Crash Report?"), jSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), jSONObject.optString("send_button_title", "Send"), jSONObject.optBoolean("show_cancel_button", true), jSONObject.optString("cancel_button_title", "Don't Send"), jSONObject.optBoolean("show_always_send_button", true), jSONObject.optString("always_send_button_title", "Always Send"));
    }

    private BetaSettingsData m5603g(JSONObject jSONObject) {
        return new BetaSettingsData(jSONObject.optString("update_endpoint", SettingsJsonConstants.f3679a), jSONObject.optInt("update_suspend_duration", 3600));
    }

    private long m5596a(CurrentTimeProvider currentTimeProvider, long j, JSONObject jSONObject) {
        if (jSONObject.has("expires_at")) {
            return jSONObject.getLong("expires_at");
        }
        return currentTimeProvider.m5459a() + (1000 * j);
    }
}
