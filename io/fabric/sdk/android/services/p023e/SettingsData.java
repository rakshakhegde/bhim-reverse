package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.t */
public class SettingsData {
    public final AppSettingsData f3670a;
    public final SessionSettingsData f3671b;
    public final PromptSettingsData f3672c;
    public final FeaturesSettingsData f3673d;
    public final AnalyticsSettingsData f3674e;
    public final BetaSettingsData f3675f;
    public final long f3676g;
    public final int f3677h;
    public final int f3678i;

    public SettingsData(long j, AppSettingsData appSettingsData, SessionSettingsData sessionSettingsData, PromptSettingsData promptSettingsData, FeaturesSettingsData featuresSettingsData, AnalyticsSettingsData analyticsSettingsData, BetaSettingsData betaSettingsData, int i, int i2) {
        this.f3676g = j;
        this.f3670a = appSettingsData;
        this.f3671b = sessionSettingsData;
        this.f3672c = promptSettingsData;
        this.f3673d = featuresSettingsData;
        this.f3677h = i;
        this.f3678i = i2;
        this.f3674e = analyticsSettingsData;
        this.f3675f = betaSettingsData;
    }

    public boolean m5622a(long j) {
        return this.f3676g < j;
    }
}
