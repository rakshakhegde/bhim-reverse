package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.c */
class AppIconSettingsData {
    public final String f3610a;
    public final int f3611b;
    public final int f3612c;

    public AppIconSettingsData(String str, int i, int i2) {
        this.f3610a = str;
        this.f3611b = i;
        this.f3612c = i2;
    }
}
