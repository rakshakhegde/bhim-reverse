package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.KitInfo;
import java.util.Collection;

/* renamed from: io.fabric.sdk.android.services.e.d */
public class AppRequestData {
    public final String f3613a;
    public final String f3614b;
    public final String f3615c;
    public final String f3616d;
    public final String f3617e;
    public final String f3618f;
    public final int f3619g;
    public final String f3620h;
    public final String f3621i;
    public final IconRequest f3622j;
    public final Collection<KitInfo> f3623k;

    public AppRequestData(String str, String str2, String str3, String str4, String str5, String str6, int i, String str7, String str8, IconRequest iconRequest, Collection<KitInfo> collection) {
        this.f3613a = str;
        this.f3614b = str2;
        this.f3615c = str3;
        this.f3616d = str4;
        this.f3617e = str5;
        this.f3618f = str6;
        this.f3619g = i;
        this.f3620h = str7;
        this.f3621i = str8;
        this.f3622j = iconRequest;
        this.f3623k = collection;
    }
}
