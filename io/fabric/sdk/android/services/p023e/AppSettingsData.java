package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.e */
public class AppSettingsData {
    public final String f3624a;
    public final String f3625b;
    public final String f3626c;
    public final String f3627d;
    public final boolean f3628e;
    public final AppIconSettingsData f3629f;

    public AppSettingsData(String str, String str2, String str3, String str4, boolean z, AppIconSettingsData appIconSettingsData) {
        this.f3624a = str;
        this.f3625b = str2;
        this.f3626c = str3;
        this.f3627d = str4;
        this.f3628e = z;
        this.f3629f = appIconSettingsData;
    }
}
