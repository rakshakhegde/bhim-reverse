package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.r */
public enum SettingsCacheBehavior {
    USE_CACHE,
    SKIP_CACHE_LOOKUP,
    IGNORE_CACHE_EXPIRATION
}
