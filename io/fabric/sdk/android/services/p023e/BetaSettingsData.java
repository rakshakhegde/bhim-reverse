package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.f */
public class BetaSettingsData {
    public final String f3630a;
    public final int f3631b;

    public BetaSettingsData(String str, int i) {
        this.f3630a = str;
        this.f3631b = i;
    }
}
