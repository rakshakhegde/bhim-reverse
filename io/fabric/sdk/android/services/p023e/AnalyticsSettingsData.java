package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.b */
public class AnalyticsSettingsData {
    public final String f3601a;
    public final int f3602b;
    public final int f3603c;
    public final int f3604d;
    public final int f3605e;
    public final boolean f3606f;
    public final boolean f3607g;
    public final boolean f3608h;
    public final int f3609i;

    public AnalyticsSettingsData(String str, int i, int i2, int i3, int i4, boolean z, boolean z2, int i5, boolean z3) {
        this.f3601a = str;
        this.f3602b = i;
        this.f3603c = i2;
        this.f3604d = i3;
        this.f3605e = i4;
        this.f3606f = z;
        this.f3607g = z2;
        this.f3609i = i5;
        this.f3608h = z3;
    }
}
