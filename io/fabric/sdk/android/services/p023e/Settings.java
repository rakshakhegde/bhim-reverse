package io.fabric.sdk.android.services.p023e;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.ApiKey;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.DeliveryMechanism;
import io.fabric.sdk.android.services.p021b.IdManager;
import io.fabric.sdk.android.services.p021b.SystemCurrentTimeProvider;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: io.fabric.sdk.android.services.e.q */
public class Settings {
    private final AtomicReference<SettingsData> f3662a;
    private final CountDownLatch f3663b;
    private SettingsController f3664c;
    private boolean f3665d;

    /* renamed from: io.fabric.sdk.android.services.e.q.b */
    public interface Settings<T> {
        T usingSettings(SettingsData settingsData);
    }

    /* renamed from: io.fabric.sdk.android.services.e.q.a */
    static class Settings {
        private static final Settings f3661a;

        static {
            f3661a = new Settings();
        }
    }

    public static Settings m5615a() {
        return Settings.f3661a;
    }

    private Settings() {
        this.f3662a = new AtomicReference();
        this.f3663b = new CountDownLatch(1);
        this.f3665d = false;
    }

    public synchronized Settings m5617a(Kit kit, IdManager idManager, HttpRequestFactory httpRequestFactory, String str, String str2, String str3) {
        Settings settings;
        if (this.f3665d) {
            settings = this;
        } else {
            if (this.f3664c == null) {
                Context context = kit.getContext();
                String c = idManager.m5473c();
                String a = new ApiKey().m5407a(context);
                String j = idManager.m5480j();
                SystemCurrentTimeProvider systemCurrentTimeProvider = new SystemCurrentTimeProvider();
                DefaultSettingsJsonTransform defaultSettingsJsonTransform = new DefaultSettingsJsonTransform();
                DefaultCachedSettingsIo defaultCachedSettingsIo = new DefaultCachedSettingsIo(kit);
                String k = CommonUtils.m5453k(context);
                Kit kit2 = kit;
                String str4 = str3;
                DefaultSettingsSpiCall defaultSettingsSpiCall = new DefaultSettingsSpiCall(kit2, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", new Object[]{c}), httpRequestFactory);
                j = str2;
                String str5 = str;
                this.f3664c = new DefaultSettingsController(kit, new SettingsRequest(a, idManager.m5477g(), idManager.m5476f(), idManager.m5475e(), idManager.m5483m(), idManager.m5472b(), idManager.m5484n(), CommonUtils.m5427a(CommonUtils.m5455m(context)), j, str5, DeliveryMechanism.m5460a(j).m5461a(), k), systemCurrentTimeProvider, defaultSettingsJsonTransform, defaultCachedSettingsIo, defaultSettingsSpiCall);
            }
            this.f3665d = true;
            settings = this;
        }
        return settings;
    }

    public <T> T m5618a(Settings<T> settings, T t) {
        SettingsData settingsData = (SettingsData) this.f3662a.get();
        return settingsData == null ? t : settings.usingSettings(settingsData);
    }

    public SettingsData m5619b() {
        try {
            this.f3663b.await();
            return (SettingsData) this.f3662a.get();
        } catch (InterruptedException e) {
            Fabric.m5322h().m5291e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    public synchronized boolean m5620c() {
        SettingsData a;
        a = this.f3664c.m5585a();
        m5616a(a);
        return a != null;
    }

    public synchronized boolean m5621d() {
        SettingsData a;
        a = this.f3664c.m5586a(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        m5616a(a);
        if (a == null) {
            Fabric.m5322h().m5292e("Fabric", "Failed to force reload of settings from Crashlytics.", null);
        }
        return a != null;
    }

    private void m5616a(SettingsData settingsData) {
        this.f3662a.set(settingsData);
        this.f3663b.countDown();
    }
}
