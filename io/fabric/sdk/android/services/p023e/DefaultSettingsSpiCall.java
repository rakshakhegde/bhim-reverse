package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: io.fabric.sdk.android.services.e.l */
class DefaultSettingsSpiCall extends AbstractSpiCall implements SettingsSpiCall {
    public DefaultSettingsSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        this(kit, str, str2, httpRequestFactory, HttpMethod.GET);
    }

    DefaultSettingsSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }

    public JSONObject m5610a(SettingsRequest settingsRequest) {
        HttpRequest httpRequest = null;
        try {
            Map b = m5609b(settingsRequest);
            httpRequest = m5606a(getHttpRequest(b), settingsRequest);
            Fabric.m5322h().m5284a("Fabric", "Requesting settings from " + getUrl());
            Fabric.m5322h().m5284a("Fabric", "Settings query params were: " + b);
            JSONObject a = m5611a(httpRequest);
            return a;
        } finally {
            if (httpRequest != null) {
                Fabric.m5322h().m5284a("Fabric", "Settings request ID: " + httpRequest.m5666b(AbstractSpiCall.HEADER_REQUEST_ID));
            }
        }
    }

    JSONObject m5611a(HttpRequest httpRequest) {
        int b = httpRequest.m5664b();
        Fabric.m5322h().m5284a("Fabric", "Settings result was: " + b);
        if (m5612a(b)) {
            return m5607a(httpRequest.m5675e());
        }
        Fabric.m5322h().m5291e("Fabric", "Failed to retrieve settings from " + getUrl());
        return null;
    }

    boolean m5612a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    private JSONObject m5607a(String str) {
        try {
            return new JSONObject(str);
        } catch (Throwable e) {
            Fabric.m5322h().m5285a("Fabric", "Failed to parse settings JSON from " + getUrl(), e);
            Fabric.m5322h().m5284a("Fabric", "Settings response " + str);
            return null;
        }
    }

    private Map<String, String> m5609b(SettingsRequest settingsRequest) {
        Map<String, String> hashMap = new HashMap();
        hashMap.put("build_version", settingsRequest.f3689j);
        hashMap.put("display_version", settingsRequest.f3688i);
        hashMap.put("source", Integer.toString(settingsRequest.f3690k));
        if (settingsRequest.f3691l != null) {
            hashMap.put("icon_hash", settingsRequest.f3691l);
        }
        String str = settingsRequest.f3687h;
        if (!CommonUtils.m5445c(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    private HttpRequest m5606a(HttpRequest httpRequest, SettingsRequest settingsRequest) {
        m5608a(httpRequest, AbstractSpiCall.HEADER_API_KEY, settingsRequest.f3680a);
        m5608a(httpRequest, AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        m5608a(httpRequest, AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion());
        m5608a(httpRequest, AbstractSpiCall.HEADER_ACCEPT, AbstractSpiCall.ACCEPT_JSON_VALUE);
        m5608a(httpRequest, "X-CRASHLYTICS-DEVICE-MODEL", settingsRequest.f3681b);
        m5608a(httpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", settingsRequest.f3682c);
        m5608a(httpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", settingsRequest.f3683d);
        m5608a(httpRequest, "X-CRASHLYTICS-ADVERTISING-TOKEN", settingsRequest.f3684e);
        m5608a(httpRequest, "X-CRASHLYTICS-INSTALLATION-ID", settingsRequest.f3685f);
        m5608a(httpRequest, "X-CRASHLYTICS-ANDROID-ID", settingsRequest.f3686g);
        return httpRequest;
    }

    private void m5608a(HttpRequest httpRequest, String str, String str2) {
        if (str2 != null) {
            httpRequest.m5654a(str, str2);
        }
    }
}
