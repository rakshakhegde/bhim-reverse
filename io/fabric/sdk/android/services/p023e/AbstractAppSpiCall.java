package io.fabric.sdk.android.services.p023e;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.KitInfo;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.p021b.AbstractSpiCall;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.ResponseParser;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Locale;

/* renamed from: io.fabric.sdk.android.services.e.a */
abstract class AbstractAppSpiCall extends AbstractSpiCall {
    public AbstractAppSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }

    public boolean m5578a(AppRequestData appRequestData) {
        HttpRequest b = m5576b(m5575a(getHttpRequest(), appRequestData), appRequestData);
        Fabric.m5322h().m5284a("Fabric", "Sending app info to " + getUrl());
        if (appRequestData.f3622j != null) {
            Fabric.m5322h().m5284a("Fabric", "App icon hash is " + appRequestData.f3622j.f3644a);
            Fabric.m5322h().m5284a("Fabric", "App icon size is " + appRequestData.f3622j.f3646c + "x" + appRequestData.f3622j.f3647d);
        }
        int b2 = b.m5664b();
        Fabric.m5322h().m5284a("Fabric", ("POST".equals(b.m5688p()) ? "Create" : "Update") + " app request ID: " + b.m5666b(AbstractSpiCall.HEADER_REQUEST_ID));
        Fabric.m5322h().m5284a("Fabric", "Result was " + b2);
        if (ResponseParser.m5514a(b2) == 0) {
            return true;
        }
        return false;
    }

    private HttpRequest m5575a(HttpRequest httpRequest, AppRequestData appRequestData) {
        return httpRequest.m5654a(AbstractSpiCall.HEADER_API_KEY, appRequestData.f3613a).m5654a(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE).m5654a(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion());
    }

    private HttpRequest m5576b(HttpRequest httpRequest, AppRequestData appRequestData) {
        HttpRequest e = httpRequest.m5674e("app[identifier]", appRequestData.f3614b).m5674e("app[name]", appRequestData.f3618f).m5674e("app[display_version]", appRequestData.f3615c).m5674e("app[build_version]", appRequestData.f3616d).m5653a("app[source]", Integer.valueOf(appRequestData.f3619g)).m5674e("app[minimum_sdk_version]", appRequestData.f3620h).m5674e("app[built_sdk_version]", appRequestData.f3621i);
        if (!CommonUtils.m5445c(appRequestData.f3617e)) {
            e.m5674e("app[instance_identifier]", appRequestData.f3617e);
        }
        if (appRequestData.f3622j != null) {
            Closeable closeable = null;
            try {
                closeable = this.kit.getContext().getResources().openRawResource(appRequestData.f3622j.f3645b);
                e.m5674e("app[icon][hash]", appRequestData.f3622j.f3644a).m5658a("app[icon][data]", "icon.png", "application/octet-stream", (InputStream) closeable).m5653a("app[icon][width]", Integer.valueOf(appRequestData.f3622j.f3646c)).m5653a("app[icon][height]", Integer.valueOf(appRequestData.f3622j.f3647d));
            } catch (Throwable e2) {
                Fabric.m5322h().m5292e("Fabric", "Failed to find app icon with resource ID: " + appRequestData.f3622j.f3645b, e2);
            } finally {
                String str = "Failed to close app icon InputStream.";
                CommonUtils.m5432a(closeable, str);
            }
        }
        if (appRequestData.f3623k != null) {
            for (KitInfo kitInfo : appRequestData.f3623k) {
                e.m5674e(m5577a(kitInfo), kitInfo.m5368b());
                e.m5674e(m5579b(kitInfo), kitInfo.m5369c());
            }
        }
        return e;
    }

    String m5577a(KitInfo kitInfo) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{kitInfo.m5367a()});
    }

    String m5579b(KitInfo kitInfo) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{kitInfo.m5367a()});
    }
}
