package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.p */
public class SessionSettingsData {
    public final int f3655a;
    public final int f3656b;
    public final int f3657c;
    public final int f3658d;
    public final int f3659e;
    public final boolean f3660f;

    public SessionSettingsData(int i, int i2, int i3, int i4, int i5, boolean z) {
        this.f3655a = i;
        this.f3656b = i2;
        this.f3657c = i3;
        this.f3658d = i4;
        this.f3659e = i5;
        this.f3660f = z;
    }
}
