package io.fabric.sdk.android.services.p023e;

/* renamed from: io.fabric.sdk.android.services.e.o */
public class PromptSettingsData {
    public final String f3648a;
    public final String f3649b;
    public final String f3650c;
    public final boolean f3651d;
    public final String f3652e;
    public final boolean f3653f;
    public final String f3654g;

    public PromptSettingsData(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.f3648a = str;
        this.f3649b = str2;
        this.f3650c = str3;
        this.f3651d = z;
        this.f3652e = str4;
        this.f3653f = z2;
        this.f3654g = str5;
    }
}
