package io.fabric.sdk.android.services.p021b;

import android.os.Process;

/* renamed from: io.fabric.sdk.android.services.b.h */
public abstract class BackgroundPriorityRunnable implements Runnable {
    protected abstract void onRun();

    public final void run() {
        Process.setThreadPriority(10);
        onRun();
    }
}
