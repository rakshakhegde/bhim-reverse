package io.fabric.sdk.android.services.p021b;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.Fabric;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.Flushable;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: io.fabric.sdk.android.services.b.i */
public class CommonUtils {
    public static final Comparator<File> f3482a;
    private static Boolean f3483b;
    private static final char[] f3484c;
    private static long f3485d;

    /* renamed from: io.fabric.sdk.android.services.b.i.1 */
    static class CommonUtils implements Comparator<File> {
        CommonUtils() {
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m5411a((File) obj, (File) obj2);
        }

        public int m5411a(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    }

    /* renamed from: io.fabric.sdk.android.services.b.i.a */
    enum CommonUtils {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        
        private static final Map<String, CommonUtils> f3480k;

        static {
            f3480k = new HashMap(4);
            f3480k.put("armeabi-v7a", ARMV7);
            f3480k.put("armeabi", ARMV6);
            f3480k.put("arm64-v8a", ARM64);
            f3480k.put("x86", X86_32);
        }

        static CommonUtils m5412a() {
            Object obj = Build.CPU_ABI;
            if (TextUtils.isEmpty(obj)) {
                Fabric.m5322h().m5284a("Fabric", "Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return UNKNOWN;
            }
            CommonUtils commonUtils = (CommonUtils) f3480k.get(obj.toLowerCase(Locale.US));
            if (commonUtils == null) {
                return UNKNOWN;
            }
            return commonUtils;
        }
    }

    static {
        f3483b = null;
        f3484c = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        f3485d = -1;
        f3482a = new CommonUtils();
    }

    public static SharedPreferences m5418a(Context context) {
        return context.getSharedPreferences("com.crashlytics.prefs", 0);
    }

    public static String m5420a(File file, String str) {
        Closeable bufferedReader;
        Throwable e;
        Throwable th;
        String str2 = null;
        if (file.exists()) {
            try {
                String[] split;
                bufferedReader = new BufferedReader(new FileReader(file), 1024);
                while (true) {
                    try {
                        CharSequence readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        split = Pattern.compile("\\s*:\\s*").split(readLine, 2);
                        if (split.length > 1 && split[0].equals(str)) {
                            break;
                        }
                    } catch (Exception e2) {
                        e = e2;
                    }
                }
                str2 = split[1];
                CommonUtils.m5432a(bufferedReader, "Failed to close system file reader.");
            } catch (Exception e3) {
                e = e3;
                bufferedReader = null;
                try {
                    Fabric.m5322h().m5292e("Fabric", "Error parsing " + file, e);
                    CommonUtils.m5432a(bufferedReader, "Failed to close system file reader.");
                    return str2;
                } catch (Throwable th2) {
                    th = th2;
                    CommonUtils.m5432a(bufferedReader, "Failed to close system file reader.");
                    throw th;
                }
            } catch (Throwable e4) {
                bufferedReader = null;
                th = e4;
                CommonUtils.m5432a(bufferedReader, "Failed to close system file reader.");
                throw th;
            }
        }
        return str2;
    }

    public static int m5413a() {
        return CommonUtils.m5412a().ordinal();
    }

    public static synchronized long m5436b() {
        long j;
        synchronized (CommonUtils.class) {
            if (f3485d == -1) {
                j = 0;
                Object a = CommonUtils.m5420a(new File("/proc/meminfo"), "MemTotal");
                if (!TextUtils.isEmpty(a)) {
                    String toUpperCase = a.toUpperCase(Locale.US);
                    try {
                        if (toUpperCase.endsWith("KB")) {
                            j = CommonUtils.m5416a(toUpperCase, "KB", 1024);
                        } else if (toUpperCase.endsWith("MB")) {
                            j = CommonUtils.m5416a(toUpperCase, "MB", 1048576);
                        } else if (toUpperCase.endsWith("GB")) {
                            j = CommonUtils.m5416a(toUpperCase, "GB", 1073741824);
                        } else {
                            Fabric.m5322h().m5284a("Fabric", "Unexpected meminfo format while computing RAM: " + toUpperCase);
                        }
                    } catch (Throwable e) {
                        Fabric.m5322h().m5292e("Fabric", "Unexpected meminfo format while computing RAM: " + toUpperCase, e);
                    }
                }
                f3485d = j;
            }
            j = f3485d;
        }
        return j;
    }

    static long m5416a(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[0].trim()) * ((long) i);
    }

    public static RunningAppProcessInfo m5417a(String str, Context context) {
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return runningAppProcessInfo;
                }
            }
        }
        return null;
    }

    public static String m5421a(InputStream inputStream) {
        Scanner useDelimiter = new Scanner(inputStream).useDelimiter("\\A");
        return useDelimiter.hasNext() ? useDelimiter.next() : BuildConfig.FLAVOR;
    }

    public static String m5423a(String str) {
        return CommonUtils.m5424a(str, "SHA-1");
    }

    public static String m5441b(InputStream inputStream) {
        return CommonUtils.m5422a(inputStream, "SHA-1");
    }

    private static String m5422a(InputStream inputStream, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return CommonUtils.m5425a(instance.digest());
                }
                instance.update(bArr, 0, read);
            }
        } catch (Throwable e) {
            Fabric.m5322h().m5292e("Fabric", "Could not calculate hash for app icon.", e);
            return BuildConfig.FLAVOR;
        }
    }

    private static String m5426a(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return CommonUtils.m5425a(instance.digest());
        } catch (Throwable e) {
            Fabric.m5322h().m5292e("Fabric", "Could not create hashing algorithm: " + str + ", returning empty string.", e);
            return BuildConfig.FLAVOR;
        }
    }

    private static String m5424a(String str, String str2) {
        return CommonUtils.m5426a(str.getBytes(), str2);
    }

    public static String m5427a(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        List<String> arrayList = new ArrayList();
        for (String str : strArr) {
            if (str != null) {
                arrayList.add(str.replace("-", BuildConfig.FLAVOR).toLowerCase(Locale.US));
            }
        }
        Collections.sort(arrayList);
        StringBuilder stringBuilder = new StringBuilder();
        for (String append : arrayList) {
            stringBuilder.append(append);
        }
        String append2 = stringBuilder.toString();
        return append2.length() > 0 ? CommonUtils.m5423a(append2) : null;
    }

    public static long m5437b(Context context) {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    public static long m5438b(String str) {
        StatFs statFs = new StatFs(str);
        long blockSize = (long) statFs.getBlockSize();
        return (((long) statFs.getBlockCount()) * blockSize) - (((long) statFs.getAvailableBlocks()) * blockSize);
    }

    public static Float m5442c(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        return Float.valueOf(((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1)));
    }

    public static boolean m5446d(Context context) {
        if (CommonUtils.m5448f(context)) {
            return false;
        }
        return ((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null;
    }

    public static void m5429a(Context context, String str) {
        if (CommonUtils.m5447e(context)) {
            Fabric.m5322h().m5284a("Fabric", str);
        }
    }

    public static void m5430a(Context context, String str, Throwable th) {
        if (CommonUtils.m5447e(context)) {
            Fabric.m5322h().m5291e("Fabric", str);
        }
    }

    public static void m5428a(Context context, int i, String str, String str2) {
        if (CommonUtils.m5447e(context)) {
            Fabric.m5322h().m5282a(i, "Fabric", str2);
        }
    }

    public static boolean m5447e(Context context) {
        if (f3483b == null) {
            f3483b = Boolean.valueOf(CommonUtils.m5435a(context, "com.crashlytics.Trace", false));
        }
        return f3483b.booleanValue();
    }

    public static boolean m5435a(Context context, String str, boolean z) {
        if (context == null) {
            return z;
        }
        Resources resources = context.getResources();
        if (resources == null) {
            return z;
        }
        int a = CommonUtils.m5414a(context, str, "bool");
        if (a > 0) {
            return resources.getBoolean(a);
        }
        int a2 = CommonUtils.m5414a(context, str, "string");
        if (a2 > 0) {
            return Boolean.parseBoolean(context.getString(a2));
        }
        return z;
    }

    public static int m5414a(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str, str2, CommonUtils.m5452j(context));
    }

    public static boolean m5448f(Context context) {
        return "sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT) || Secure.getString(context.getContentResolver(), "android_id") == null;
    }

    public static boolean m5449g(Context context) {
        boolean f = CommonUtils.m5448f(context);
        String str = Build.TAGS;
        if ((!f && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        File file = new File("/system/xbin/su");
        if (f || !file.exists()) {
            return false;
        }
        return true;
    }

    public static boolean m5443c() {
        return Debug.isDebuggerConnected() || Debug.waitingForDebugger();
    }

    public static int m5450h(Context context) {
        int i = 0;
        if (CommonUtils.m5448f(context)) {
            i = 1;
        }
        if (CommonUtils.m5449g(context)) {
            i |= 2;
        }
        if (CommonUtils.m5443c()) {
            return i | 4;
        }
        return i;
    }

    public static int m5415a(Context context, boolean z) {
        Float c = CommonUtils.m5442c(context);
        if (!z || c == null) {
            return 1;
        }
        if (((double) c.floatValue()) >= 99.0d) {
            return 3;
        }
        if (((double) c.floatValue()) < 99.0d) {
            return 2;
        }
        return 0;
    }

    public static String m5425a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            cArr[i * 2] = f3484c[i2 >>> 4];
            cArr[(i * 2) + 1] = f3484c[i2 & 15];
        }
        return new String(cArr);
    }

    public static boolean m5451i(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static String m5440b(Context context, String str) {
        int a = CommonUtils.m5414a(context, str, "string");
        if (a > 0) {
            return context.getString(a);
        }
        return BuildConfig.FLAVOR;
    }

    public static void m5432a(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable e) {
                Fabric.m5322h().m5292e("Fabric", str, e);
            }
        }
    }

    public static void m5433a(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (Throwable e) {
                Fabric.m5322h().m5292e("Fabric", str, e);
            }
        }
    }

    public static boolean m5445c(String str) {
        return str == null || str.length() == 0;
    }

    public static String m5419a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("value must be zero or greater");
        }
        return String.format(Locale.US, "%1$10s", new Object[]{Integer.valueOf(i)}).replace(' ', '0');
    }

    public static String m5452j(Context context) {
        int i = context.getApplicationContext().getApplicationInfo().icon;
        if (i > 0) {
            return context.getResources().getResourcePackageName(i);
        }
        return context.getPackageName();
    }

    public static void m5434a(InputStream inputStream, OutputStream outputStream, byte[] bArr) {
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static String m5439b(int i) {
        switch (i) {
            case R.View_paddingStart /*2*/:
                return "V";
            case R.View_paddingEnd /*3*/:
                return "D";
            case R.View_theme /*4*/:
                return "I";
            case R.Toolbar_contentInsetStart /*5*/:
                return "W";
            case R.Toolbar_contentInsetEnd /*6*/:
                return "E";
            case R.Toolbar_contentInsetLeft /*7*/:
                return "A";
            default:
                return "?";
        }
    }

    public static String m5453k(Context context) {
        Throwable e;
        Throwable th;
        String str = null;
        Closeable openRawResource;
        try {
            openRawResource = context.getResources().openRawResource(CommonUtils.m5454l(context));
            try {
                String b = CommonUtils.m5441b((InputStream) openRawResource);
                if (!CommonUtils.m5445c(b)) {
                    str = b;
                }
                CommonUtils.m5432a(openRawResource, "Failed to close icon input stream.");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.m5322h().m5292e("Fabric", "Could not calculate hash for app icon.", e);
                    CommonUtils.m5432a(openRawResource, "Failed to close icon input stream.");
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    CommonUtils.m5432a(openRawResource, "Failed to close icon input stream.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            openRawResource = null;
            Fabric.m5322h().m5292e("Fabric", "Could not calculate hash for app icon.", e);
            CommonUtils.m5432a(openRawResource, "Failed to close icon input stream.");
            return str;
        } catch (Throwable e4) {
            openRawResource = null;
            th = e4;
            CommonUtils.m5432a(openRawResource, "Failed to close icon input stream.");
            throw th;
        }
        return str;
    }

    public static int m5454l(Context context) {
        return context.getApplicationContext().getApplicationInfo().icon;
    }

    public static String m5455m(Context context) {
        int a = CommonUtils.m5414a(context, "io.fabric.android.build_id", "string");
        if (a == 0) {
            a = CommonUtils.m5414a(context, "com.crashlytics.android.build_id", "string");
        }
        if (a == 0) {
            return null;
        }
        String string = context.getResources().getString(a);
        Fabric.m5322h().m5284a("Fabric", "Build ID is: " + string);
        return string;
    }

    public static void m5431a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    public static boolean m5444c(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean m5456n(Context context) {
        if (!CommonUtils.m5444c(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
