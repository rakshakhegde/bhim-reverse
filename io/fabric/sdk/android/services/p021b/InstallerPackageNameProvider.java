package io.fabric.sdk.android.services.p021b;

import android.content.Context;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.p022a.MemoryValueCache;
import io.fabric.sdk.android.services.p022a.ValueLoader;

/* renamed from: io.fabric.sdk.android.services.b.p */
public class InstallerPackageNameProvider {
    private final ValueLoader<String> f3525a;
    private final MemoryValueCache<String> f3526b;

    /* renamed from: io.fabric.sdk.android.services.b.p.1 */
    class InstallerPackageNameProvider implements ValueLoader<String> {
        final /* synthetic */ InstallerPackageNameProvider f3524a;

        InstallerPackageNameProvider(InstallerPackageNameProvider installerPackageNameProvider) {
            this.f3524a = installerPackageNameProvider;
        }

        public /* synthetic */ Object load(Context context) {
            return m5485a(context);
        }

        public String m5485a(Context context) {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? BuildConfig.FLAVOR : installerPackageName;
        }
    }

    public InstallerPackageNameProvider() {
        this.f3525a = new InstallerPackageNameProvider(this);
        this.f3526b = new MemoryValueCache();
    }

    public String m5486a(Context context) {
        try {
            String str = (String) this.f3526b.m5382a(context, this.f3525a);
            if (BuildConfig.FLAVOR.equals(str)) {
                return null;
            }
            return str;
        } catch (Throwable e) {
            Fabric.m5322h().m5292e("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
