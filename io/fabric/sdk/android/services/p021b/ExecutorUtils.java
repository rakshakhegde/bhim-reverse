package io.fabric.sdk.android.services.p021b;

import io.fabric.sdk.android.Fabric;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: io.fabric.sdk.android.services.b.n */
public final class ExecutorUtils {

    /* renamed from: io.fabric.sdk.android.services.b.n.1 */
    static class ExecutorUtils implements ThreadFactory {
        final /* synthetic */ String f3496a;
        final /* synthetic */ AtomicLong f3497b;

        /* renamed from: io.fabric.sdk.android.services.b.n.1.1 */
        class ExecutorUtils extends BackgroundPriorityRunnable {
            final /* synthetic */ Runnable f3494a;
            final /* synthetic */ ExecutorUtils f3495b;

            ExecutorUtils(ExecutorUtils executorUtils, Runnable runnable) {
                this.f3495b = executorUtils;
                this.f3494a = runnable;
            }

            public void onRun() {
                this.f3494a.run();
            }
        }

        ExecutorUtils(String str, AtomicLong atomicLong) {
            this.f3496a = str;
            this.f3497b = atomicLong;
        }

        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new ExecutorUtils(this, runnable));
            newThread.setName(this.f3496a + this.f3497b.getAndIncrement());
            return newThread;
        }
    }

    /* renamed from: io.fabric.sdk.android.services.b.n.2 */
    static class ExecutorUtils extends BackgroundPriorityRunnable {
        final /* synthetic */ String f3498a;
        final /* synthetic */ ExecutorService f3499b;
        final /* synthetic */ long f3500c;
        final /* synthetic */ TimeUnit f3501d;

        ExecutorUtils(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.f3498a = str;
            this.f3499b = executorService;
            this.f3500c = j;
            this.f3501d = timeUnit;
        }

        public void onRun() {
            try {
                Fabric.m5322h().m5284a("Fabric", "Executing shutdown hook for " + this.f3498a);
                this.f3499b.shutdown();
                if (!this.f3499b.awaitTermination(this.f3500c, this.f3501d)) {
                    Fabric.m5322h().m5284a("Fabric", this.f3498a + " did not shut down in the" + " allocated time. Requesting immediate shutdown.");
                    this.f3499b.shutdownNow();
                }
            } catch (InterruptedException e) {
                Fabric.m5322h().m5284a("Fabric", String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", new Object[]{this.f3498a}));
                this.f3499b.shutdownNow();
            }
        }
    }

    public static ExecutorService m5462a(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(ExecutorUtils.m5466c(str));
        ExecutorUtils.m5463a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    public static ScheduledExecutorService m5465b(String str) {
        Object newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor(ExecutorUtils.m5466c(str));
        ExecutorUtils.m5463a(str, newSingleThreadScheduledExecutor);
        return newSingleThreadScheduledExecutor;
    }

    public static final ThreadFactory m5466c(String str) {
        return new ExecutorUtils(str, new AtomicLong(1));
    }

    private static final void m5463a(String str, ExecutorService executorService) {
        ExecutorUtils.m5464a(str, executorService, 2, TimeUnit.SECONDS);
    }

    public static final void m5464a(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime.getRuntime().addShutdownHook(new Thread(new ExecutorUtils(str, executorService, j, timeUnit), "Crashlytics Shutdown Hook for " + str));
    }
}
