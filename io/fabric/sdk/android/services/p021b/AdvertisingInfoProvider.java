package io.fabric.sdk.android.services.p021b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.p038d.PreferenceStore;
import io.fabric.sdk.android.services.p038d.PreferenceStoreImpl;

/* renamed from: io.fabric.sdk.android.services.b.c */
class AdvertisingInfoProvider {
    private final Context f3463a;
    private final PreferenceStore f3464b;

    /* renamed from: io.fabric.sdk.android.services.b.c.1 */
    class AdvertisingInfoProvider extends BackgroundPriorityRunnable {
        final /* synthetic */ AdvertisingInfo f3461a;
        final /* synthetic */ AdvertisingInfoProvider f3462b;

        AdvertisingInfoProvider(AdvertisingInfoProvider advertisingInfoProvider, AdvertisingInfo advertisingInfo) {
            this.f3462b = advertisingInfoProvider;
            this.f3461a = advertisingInfo;
        }

        public void onRun() {
            AdvertisingInfo a = this.f3462b.m5391e();
            if (!this.f3461a.equals(a)) {
                Fabric.m5322h().m5284a("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                this.f3462b.m5389b(a);
            }
        }
    }

    public AdvertisingInfoProvider(Context context) {
        this.f3463a = context.getApplicationContext();
        this.f3464b = new PreferenceStoreImpl(context, "TwitterAdvertisingInfoPreferences");
    }

    public AdvertisingInfo m5392a() {
        AdvertisingInfo b = m5393b();
        if (m5390c(b)) {
            Fabric.m5322h().m5284a("Fabric", "Using AdvertisingInfo from Preference Store");
            m5387a(b);
            return b;
        }
        b = m5391e();
        m5389b(b);
        return b;
    }

    private void m5387a(AdvertisingInfo advertisingInfo) {
        new Thread(new AdvertisingInfoProvider(this, advertisingInfo)).start();
    }

    @SuppressLint({"CommitPrefEdits"})
    private void m5389b(AdvertisingInfo advertisingInfo) {
        if (m5390c(advertisingInfo)) {
            this.f3464b.m5570a(this.f3464b.m5571b().putString("advertising_id", advertisingInfo.f3459a).putBoolean("limit_ad_tracking_enabled", advertisingInfo.f3460b));
        } else {
            this.f3464b.m5570a(this.f3464b.m5571b().remove("advertising_id").remove("limit_ad_tracking_enabled"));
        }
    }

    protected AdvertisingInfo m5393b() {
        return new AdvertisingInfo(this.f3464b.m5569a().getString("advertising_id", BuildConfig.FLAVOR), this.f3464b.m5569a().getBoolean("limit_ad_tracking_enabled", false));
    }

    public AdvertisingInfoStrategy m5394c() {
        return new AdvertisingInfoReflectionStrategy(this.f3463a);
    }

    public AdvertisingInfoStrategy m5395d() {
        return new AdvertisingInfoServiceStrategy(this.f3463a);
    }

    private boolean m5390c(AdvertisingInfo advertisingInfo) {
        return (advertisingInfo == null || TextUtils.isEmpty(advertisingInfo.f3459a)) ? false : true;
    }

    private AdvertisingInfo m5391e() {
        AdvertisingInfo a = m5394c().m5396a();
        if (m5390c(a)) {
            Fabric.m5322h().m5284a("Fabric", "Using AdvertisingInfo from Reflection Provider");
        } else {
            a = m5395d().m5396a();
            if (m5390c(a)) {
                Fabric.m5322h().m5284a("Fabric", "Using AdvertisingInfo from Service Provider");
            } else {
                Fabric.m5322h().m5284a("Fabric", "AdvertisingInfo not present");
            }
        }
        return a;
    }
}
