package io.fabric.sdk.android.services.p021b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import com.crashlytics.android.core.BuildConfig;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: io.fabric.sdk.android.services.b.o */
public class IdManager {
    private static final Pattern f3511d;
    private static final String f3512e;
    AdvertisingInfoProvider f3513a;
    AdvertisingInfo f3514b;
    boolean f3515c;
    private final ReentrantLock f3516f;
    private final InstallerPackageNameProvider f3517g;
    private final boolean f3518h;
    private final boolean f3519i;
    private final Context f3520j;
    private final String f3521k;
    private final String f3522l;
    private final Collection<Kit> f3523m;

    /* renamed from: io.fabric.sdk.android.services.b.o.a */
    public enum IdManager {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(R.AppCompatTheme_buttonStyleSmall),
        ANDROID_SERIAL(R.AppCompatTheme_checkboxStyle),
        ANDROID_ADVERTISING_ID(R.AppCompatTheme_checkedTextViewStyle);
        
        public final int f3510h;

        private IdManager(int i) {
            this.f3510h = i;
        }
    }

    static {
        f3511d = Pattern.compile("[^\\p{Alnum}]");
        f3512e = Pattern.quote("/");
    }

    public IdManager(Context context, String str, String str2, Collection<Kit> collection) {
        this.f3516f = new ReentrantLock();
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection == null) {
            throw new IllegalArgumentException("kits must not be null");
        } else {
            this.f3520j = context;
            this.f3521k = str;
            this.f3522l = str2;
            this.f3523m = collection;
            this.f3517g = new InstallerPackageNameProvider();
            this.f3513a = new AdvertisingInfoProvider(context);
            this.f3518h = CommonUtils.m5435a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (!this.f3518h) {
                Fabric.m5322h().m5284a("Fabric", "Device ID collection disabled for " + context.getPackageName());
            }
            this.f3519i = CommonUtils.m5435a(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (!this.f3519i) {
                Fabric.m5322h().m5284a("Fabric", "User information collection disabled for " + context.getPackageName());
            }
        }
    }

    public boolean m5471a() {
        return this.f3519i;
    }

    private String m5468a(String str) {
        return str == null ? null : f3511d.matcher(str).replaceAll(BuildConfig.FLAVOR).toLowerCase(Locale.US);
    }

    public String m5472b() {
        String str = this.f3522l;
        if (str != null) {
            return str;
        }
        SharedPreferences a = CommonUtils.m5418a(this.f3520j);
        str = a.getString("crashlytics.installation.id", null);
        if (str == null) {
            return m5467a(a);
        }
        return str;
    }

    public String m5473c() {
        return this.f3521k;
    }

    public String m5474d() {
        return m5475e() + "/" + m5476f();
    }

    public String m5475e() {
        return m5470b(VERSION.RELEASE);
    }

    public String m5476f() {
        return m5470b(VERSION.INCREMENTAL);
    }

    public String m5477g() {
        return String.format(Locale.US, "%s/%s", new Object[]{m5470b(Build.MANUFACTURER), m5470b(Build.MODEL)});
    }

    private String m5470b(String str) {
        return str.replaceAll(f3512e, BuildConfig.FLAVOR);
    }

    public String m5478h() {
        String str = BuildConfig.FLAVOR;
        if (!this.f3518h) {
            return str;
        }
        str = m5484n();
        if (str != null) {
            return str;
        }
        SharedPreferences a = CommonUtils.m5418a(this.f3520j);
        str = a.getString("crashlytics.installation.id", null);
        if (str == null) {
            return m5467a(a);
        }
        return str;
    }

    private String m5467a(SharedPreferences sharedPreferences) {
        this.f3516f.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", null);
            if (string == null) {
                string = m5468a(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            this.f3516f.unlock();
            return string;
        } catch (Throwable th) {
            this.f3516f.unlock();
        }
    }

    public Map<IdManager, String> m5479i() {
        Map hashMap = new HashMap();
        for (Kit kit : this.f3523m) {
            if (kit instanceof DeviceIdentifierProvider) {
                for (Entry entry : ((DeviceIdentifierProvider) kit).getDeviceIdentifiers().entrySet()) {
                    m5469a(hashMap, (IdManager) entry.getKey(), (String) entry.getValue());
                }
            }
        }
        m5469a(hashMap, IdManager.ANDROID_ID, m5484n());
        m5469a(hashMap, IdManager.ANDROID_ADVERTISING_ID, m5483m());
        return Collections.unmodifiableMap(hashMap);
    }

    public String m5480j() {
        return this.f3517g.m5486a(this.f3520j);
    }

    synchronized AdvertisingInfo m5481k() {
        if (!this.f3515c) {
            this.f3514b = this.f3513a.m5392a();
            this.f3515c = true;
        }
        return this.f3514b;
    }

    public Boolean m5482l() {
        if (!this.f3518h) {
            return null;
        }
        AdvertisingInfo k = m5481k();
        if (k != null) {
            return Boolean.valueOf(k.f3460b);
        }
        return null;
    }

    public String m5483m() {
        if (!this.f3518h) {
            return null;
        }
        AdvertisingInfo k = m5481k();
        if (k != null) {
            return k.f3459a;
        }
        return null;
    }

    private void m5469a(Map<IdManager, String> map, IdManager idManager, String str) {
        if (str != null) {
            map.put(idManager, str);
        }
    }

    public String m5484n() {
        if (!this.f3518h) {
            return null;
        }
        String string = Secure.getString(this.f3520j.getContentResolver(), "android_id");
        if ("9774d56d682e549c".equals(string)) {
            return null;
        }
        return m5468a(string);
    }
}
