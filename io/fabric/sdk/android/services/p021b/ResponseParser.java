package io.fabric.sdk.android.services.p021b;

/* renamed from: io.fabric.sdk.android.services.b.r */
public class ResponseParser {
    public static int m5514a(int i) {
        if (i >= 200 && i <= 299) {
            return 0;
        }
        if (i >= 300 && i <= 399) {
            return 1;
        }
        if (i >= 400 && i <= 499) {
            return 0;
        }
        if (i >= 500) {
            return 1;
        }
        return 1;
    }
}
