package io.fabric.sdk.android.services.p021b;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: io.fabric.sdk.android.services.b.q */
public class QueueFile implements Closeable {
    private static final Logger f3536b;
    int f3537a;
    private final RandomAccessFile f3538c;
    private int f3539d;
    private QueueFile f3540e;
    private QueueFile f3541f;
    private final byte[] f3542g;

    /* renamed from: io.fabric.sdk.android.services.b.q.c */
    public interface QueueFile {
        void read(InputStream inputStream, int i);
    }

    /* renamed from: io.fabric.sdk.android.services.b.q.1 */
    class QueueFile implements QueueFile {
        boolean f3527a;
        final /* synthetic */ StringBuilder f3528b;
        final /* synthetic */ QueueFile f3529c;

        QueueFile(QueueFile queueFile, StringBuilder stringBuilder) {
            this.f3529c = queueFile;
            this.f3528b = stringBuilder;
            this.f3527a = true;
        }

        public void read(InputStream inputStream, int i) {
            if (this.f3527a) {
                this.f3527a = false;
            } else {
                this.f3528b.append(", ");
            }
            this.f3528b.append(i);
        }
    }

    /* renamed from: io.fabric.sdk.android.services.b.q.a */
    static class QueueFile {
        static final QueueFile f3530a;
        final int f3531b;
        final int f3532c;

        static {
            f3530a = new QueueFile(0, 0);
        }

        QueueFile(int i, int i2) {
            this.f3531b = i;
            this.f3532c = i2;
        }

        public String toString() {
            return getClass().getSimpleName() + "[" + "position = " + this.f3531b + ", length = " + this.f3532c + "]";
        }
    }

    /* renamed from: io.fabric.sdk.android.services.b.q.b */
    private final class QueueFile extends InputStream {
        final /* synthetic */ QueueFile f3533a;
        private int f3534b;
        private int f3535c;

        private QueueFile(QueueFile queueFile, QueueFile queueFile2) {
            this.f3533a = queueFile;
            this.f3534b = queueFile.m5497b(queueFile2.f3531b + 4);
            this.f3535c = queueFile2.f3532c;
        }

        public int read(byte[] bArr, int i, int i2) {
            QueueFile.m5499b(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            } else if (this.f3535c <= 0) {
                return -1;
            } else {
                if (i2 > this.f3535c) {
                    i2 = this.f3535c;
                }
                this.f3533a.m5500b(this.f3534b, bArr, i, i2);
                this.f3534b = this.f3533a.m5497b(this.f3534b + i2);
                this.f3535c -= i2;
                return i2;
            }
        }

        public int read() {
            if (this.f3535c == 0) {
                return -1;
            }
            this.f3533a.f3538c.seek((long) this.f3534b);
            int read = this.f3533a.f3538c.read();
            this.f3534b = this.f3533a.m5497b(this.f3534b + 1);
            this.f3535c--;
            return read;
        }
    }

    static {
        f3536b = Logger.getLogger(QueueFile.class.getName());
    }

    public QueueFile(File file) {
        this.f3542g = new byte[16];
        if (!file.exists()) {
            QueueFile.m5495a(file);
        }
        this.f3538c = QueueFile.m5498b(file);
        m5504e();
    }

    private static void m5501b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    private static void m5496a(byte[] bArr, int... iArr) {
        int i = 0;
        int length = iArr.length;
        int i2 = 0;
        while (i < length) {
            QueueFile.m5501b(bArr, i2, iArr[i]);
            i2 += 4;
            i++;
        }
    }

    private static int m5488a(byte[] bArr, int i) {
        return ((((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16)) + ((bArr[i + 2] & 255) << 8)) + (bArr[i + 3] & 255);
    }

    private void m5504e() {
        this.f3538c.seek(0);
        this.f3538c.readFully(this.f3542g);
        this.f3537a = QueueFile.m5488a(this.f3542g, 0);
        if (((long) this.f3537a) > this.f3538c.length()) {
            throw new IOException("File is truncated. Expected length: " + this.f3537a + ", Actual length: " + this.f3538c.length());
        }
        this.f3539d = QueueFile.m5488a(this.f3542g, 4);
        int a = QueueFile.m5488a(this.f3542g, 8);
        int a2 = QueueFile.m5488a(this.f3542g, 12);
        this.f3540e = m5489a(a);
        this.f3541f = m5489a(a2);
    }

    private void m5492a(int i, int i2, int i3, int i4) {
        QueueFile.m5496a(this.f3542g, i, i2, i3, i4);
        this.f3538c.seek(0);
        this.f3538c.write(this.f3542g);
    }

    private QueueFile m5489a(int i) {
        if (i == 0) {
            return QueueFile.f3530a;
        }
        this.f3538c.seek((long) i);
        return new QueueFile(i, this.f3538c.readInt());
    }

    private static void m5495a(File file) {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b = QueueFile.m5498b(file2);
        try {
            b.setLength(4096);
            b.seek(0);
            byte[] bArr = new byte[16];
            QueueFile.m5496a(bArr, CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 0, 0);
            b.write(bArr);
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } finally {
            b.close();
        }
    }

    private static RandomAccessFile m5498b(File file) {
        return new RandomAccessFile(file, "rwd");
    }

    private int m5497b(int i) {
        return i < this.f3537a ? i : (i + 16) - this.f3537a;
    }

    private void m5493a(int i, byte[] bArr, int i2, int i3) {
        int b = m5497b(i);
        if (b + i3 <= this.f3537a) {
            this.f3538c.seek((long) b);
            this.f3538c.write(bArr, i2, i3);
            return;
        }
        int i4 = this.f3537a - b;
        this.f3538c.seek((long) b);
        this.f3538c.write(bArr, i2, i4);
        this.f3538c.seek(16);
        this.f3538c.write(bArr, i2 + i4, i3 - i4);
    }

    private void m5500b(int i, byte[] bArr, int i2, int i3) {
        int b = m5497b(i);
        if (b + i3 <= this.f3537a) {
            this.f3538c.seek((long) b);
            this.f3538c.readFully(bArr, i2, i3);
            return;
        }
        int i4 = this.f3537a - b;
        this.f3538c.seek((long) b);
        this.f3538c.readFully(bArr, i2, i4);
        this.f3538c.seek(16);
        this.f3538c.readFully(bArr, i2 + i4, i3 - i4);
    }

    public void m5508a(byte[] bArr) {
        m5509a(bArr, 0, bArr.length);
    }

    public synchronized void m5509a(byte[] bArr, int i, int i2) {
        QueueFile.m5499b(bArr, "buffer");
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        int i3;
        m5502c(i2);
        boolean b = m5511b();
        if (b) {
            i3 = 16;
        } else {
            i3 = m5497b((this.f3541f.f3531b + 4) + this.f3541f.f3532c);
        }
        QueueFile queueFile = new QueueFile(i3, i2);
        QueueFile.m5501b(this.f3542g, 0, i2);
        m5493a(queueFile.f3531b, this.f3542g, 0, 4);
        m5493a(queueFile.f3531b + 4, bArr, i, i2);
        m5492a(this.f3537a, this.f3539d + 1, b ? queueFile.f3531b : this.f3540e.f3531b, queueFile.f3531b);
        this.f3541f = queueFile;
        this.f3539d++;
        if (b) {
            this.f3540e = this.f3541f;
        }
    }

    public int m5506a() {
        if (this.f3539d == 0) {
            return 16;
        }
        if (this.f3541f.f3531b >= this.f3540e.f3531b) {
            return (((this.f3541f.f3531b - this.f3540e.f3531b) + 4) + this.f3541f.f3532c) + 16;
        }
        return (((this.f3541f.f3531b + 4) + this.f3541f.f3532c) + this.f3537a) - this.f3540e.f3531b;
    }

    private int m5505f() {
        return this.f3537a - m5506a();
    }

    public synchronized boolean m5511b() {
        return this.f3539d == 0;
    }

    private void m5502c(int i) {
        int i2 = i + 4;
        int f = m5505f();
        if (f < i2) {
            int i3 = this.f3537a;
            do {
                f += i3;
                i3 <<= 1;
            } while (f < i2);
            m5503d(i3);
            i2 = m5497b((this.f3541f.f3531b + 4) + this.f3541f.f3532c);
            if (i2 < this.f3540e.f3531b) {
                FileChannel channel = this.f3538c.getChannel();
                channel.position((long) this.f3537a);
                int i4 = i2 - 4;
                if (channel.transferTo(16, (long) i4, channel) != ((long) i4)) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            if (this.f3541f.f3531b < this.f3540e.f3531b) {
                f = (this.f3537a + this.f3541f.f3531b) - 16;
                m5492a(i3, this.f3539d, this.f3540e.f3531b, f);
                this.f3541f = new QueueFile(f, this.f3541f.f3532c);
            } else {
                m5492a(i3, this.f3539d, this.f3540e.f3531b, this.f3541f.f3531b);
            }
            this.f3537a = i3;
        }
    }

    private void m5503d(int i) {
        this.f3538c.setLength((long) i);
        this.f3538c.getChannel().force(true);
    }

    public synchronized void m5507a(QueueFile queueFile) {
        int i = this.f3540e.f3531b;
        for (int i2 = 0; i2 < this.f3539d; i2++) {
            QueueFile a = m5489a(i);
            queueFile.read(new QueueFile(a, null), a.f3532c);
            i = m5497b(a.f3532c + (a.f3531b + 4));
        }
    }

    private static <T> T m5499b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public synchronized void m5512c() {
        if (m5511b()) {
            throw new NoSuchElementException();
        } else if (this.f3539d == 1) {
            m5513d();
        } else {
            int b = m5497b((this.f3540e.f3531b + 4) + this.f3540e.f3532c);
            m5500b(b, this.f3542g, 0, 4);
            int a = QueueFile.m5488a(this.f3542g, 0);
            m5492a(this.f3537a, this.f3539d - 1, b, this.f3541f.f3531b);
            this.f3539d--;
            this.f3540e = new QueueFile(b, a);
        }
    }

    public synchronized void m5513d() {
        m5492a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 0, 0);
        this.f3539d = 0;
        this.f3540e = QueueFile.f3530a;
        this.f3541f = QueueFile.f3530a;
        if (this.f3537a > CodedOutputStream.DEFAULT_BUFFER_SIZE) {
            m5503d(CodedOutputStream.DEFAULT_BUFFER_SIZE);
        }
        this.f3537a = CodedOutputStream.DEFAULT_BUFFER_SIZE;
    }

    public synchronized void close() {
        this.f3538c.close();
    }

    public boolean m5510a(int i, int i2) {
        return (m5506a() + 4) + i <= i2;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getClass().getSimpleName()).append('[');
        stringBuilder.append("fileLength=").append(this.f3537a);
        stringBuilder.append(", size=").append(this.f3539d);
        stringBuilder.append(", first=").append(this.f3540e);
        stringBuilder.append(", last=").append(this.f3541f);
        stringBuilder.append(", element lengths=[");
        try {
            m5507a(new QueueFile(this, stringBuilder));
        } catch (Throwable e) {
            f3536b.log(Level.WARNING, "read error", e);
        }
        stringBuilder.append("]]");
        return stringBuilder.toString();
    }
}
