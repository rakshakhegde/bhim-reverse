package io.fabric.sdk.android.services.p021b;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;

/* renamed from: io.fabric.sdk.android.services.b.g */
public class ApiKey {
    public String m5407a(Context context) {
        Object b = m5408b(context);
        if (TextUtils.isEmpty(b)) {
            b = m5409c(context);
        }
        if (TextUtils.isEmpty(b)) {
            m5410d(context);
        }
        return b;
    }

    protected String m5408b(Context context) {
        String str = null;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                str = bundle.getString("io.fabric.ApiKey");
                if (str == null) {
                    Fabric.m5322h().m5284a("Fabric", "Falling back to Crashlytics key lookup from Manifest");
                    str = bundle.getString("com.crashlytics.ApiKey");
                }
            }
        } catch (Exception e) {
            Fabric.m5322h().m5284a("Fabric", "Caught non-fatal exception while retrieving apiKey: " + e);
        }
        return str;
    }

    protected String m5409c(Context context) {
        int a = CommonUtils.m5414a(context, "io.fabric.ApiKey", "string");
        if (a == 0) {
            Fabric.m5322h().m5284a("Fabric", "Falling back to Crashlytics key lookup from Strings");
            a = CommonUtils.m5414a(context, "com.crashlytics.ApiKey", "string");
        }
        if (a != 0) {
            return context.getResources().getString(a);
        }
        return null;
    }

    protected void m5410d(Context context) {
        if (Fabric.m5323i() || CommonUtils.m5451i(context)) {
            throw new IllegalArgumentException(m5406a());
        }
        Fabric.m5322h().m5291e("Fabric", m5406a());
    }

    protected String m5406a() {
        return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
    }
}
