package io.fabric.sdk.android.services.p021b;

import android.content.Context;
import io.fabric.sdk.android.Fabric;

/* renamed from: io.fabric.sdk.android.services.b.d */
class AdvertisingInfoReflectionStrategy implements AdvertisingInfoStrategy {
    private final Context f3465a;

    public AdvertisingInfoReflectionStrategy(Context context) {
        this.f3465a = context.getApplicationContext();
    }

    boolean m5401a(Context context) {
        try {
            if (((Integer) Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke(null, new Object[]{context})).intValue() == 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public AdvertisingInfo m5400a() {
        if (m5401a(this.f3465a)) {
            return new AdvertisingInfo(m5397b(), m5398c());
        }
        return null;
    }

    private String m5397b() {
        try {
            return (String) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("getId", new Class[0]).invoke(m5399d(), new Object[0]);
        } catch (Exception e) {
            Fabric.m5322h().m5289d("Fabric", "Could not call getId on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            return null;
        }
    }

    private boolean m5398c() {
        try {
            return ((Boolean) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("isLimitAdTrackingEnabled", new Class[0]).invoke(m5399d(), new Object[0])).booleanValue();
        } catch (Exception e) {
            Fabric.m5322h().m5289d("Fabric", "Could not call isLimitAdTrackingEnabled on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            return false;
        }
    }

    private Object m5399d() {
        Object obj = null;
        try {
            obj = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{this.f3465a});
        } catch (Exception e) {
            Fabric.m5322h().m5289d("Fabric", "Could not call getAdvertisingIdInfo on com.google.android.gms.ads.identifier.AdvertisingIdClient");
        }
        return obj;
    }
}
