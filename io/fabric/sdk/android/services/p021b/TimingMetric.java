package io.fabric.sdk.android.services.p021b;

import android.os.SystemClock;
import android.util.Log;

/* renamed from: io.fabric.sdk.android.services.b.t */
public class TimingMetric {
    private final String f3543a;
    private final String f3544b;
    private final boolean f3545c;
    private long f3546d;
    private long f3547e;

    public TimingMetric(String str, String str2) {
        this.f3543a = str;
        this.f3544b = str2;
        this.f3545c = !Log.isLoggable(str2, 2);
    }

    public synchronized void m5517a() {
        if (!this.f3545c) {
            this.f3546d = SystemClock.elapsedRealtime();
            this.f3547e = 0;
        }
    }

    public synchronized void m5518b() {
        if (!this.f3545c) {
            if (this.f3547e == 0) {
                this.f3547e = SystemClock.elapsedRealtime() - this.f3546d;
                m5516c();
            }
        }
    }

    private void m5516c() {
        Log.v(this.f3544b, this.f3543a + ": " + this.f3547e + "ms");
    }
}
