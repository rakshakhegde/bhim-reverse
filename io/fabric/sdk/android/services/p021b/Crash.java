package io.fabric.sdk.android.services.p021b;

/* renamed from: io.fabric.sdk.android.services.b.j */
public abstract class Crash {
    private final String f3486a;
    private final String f3487b;

    /* renamed from: io.fabric.sdk.android.services.b.j.a */
    public static class Crash extends Crash {
        public Crash(String str, String str2) {
            super(str, str2);
        }
    }

    /* renamed from: io.fabric.sdk.android.services.b.j.b */
    public static class Crash extends Crash {
        public Crash(String str, String str2) {
            super(str, str2);
        }
    }

    public Crash(String str, String str2) {
        this.f3486a = str;
        this.f3487b = str2;
    }

    public String m5457a() {
        return this.f3486a;
    }

    public String m5458b() {
        return this.f3487b;
    }
}
