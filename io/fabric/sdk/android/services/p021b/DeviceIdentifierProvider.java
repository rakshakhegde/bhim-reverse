package io.fabric.sdk.android.services.p021b;

import io.fabric.sdk.android.services.p021b.IdManager.IdManager;
import java.util.Map;

/* renamed from: io.fabric.sdk.android.services.b.m */
public interface DeviceIdentifierProvider {
    Map<IdManager, String> getDeviceIdentifiers();
}
