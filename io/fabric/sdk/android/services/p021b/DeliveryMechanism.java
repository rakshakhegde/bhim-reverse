package io.fabric.sdk.android.services.p021b;

/* renamed from: io.fabric.sdk.android.services.b.l */
public enum DeliveryMechanism {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    
    private final int f3493e;

    private DeliveryMechanism(int i) {
        this.f3493e = i;
    }

    public int m5461a() {
        return this.f3493e;
    }

    public String toString() {
        return Integer.toString(this.f3493e);
    }

    public static DeliveryMechanism m5460a(String str) {
        if ("io.crash.air".equals(str)) {
            return TEST_DISTRIBUTION;
        }
        if (str != null) {
            return APP_STORE;
        }
        return DEVELOPER;
    }
}
