package io.fabric.sdk.android.services.p021b;

/* renamed from: io.fabric.sdk.android.services.b.b */
class AdvertisingInfo {
    public final String f3459a;
    public final boolean f3460b;

    AdvertisingInfo(String str, boolean z) {
        this.f3459a = str;
        this.f3460b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdvertisingInfo advertisingInfo = (AdvertisingInfo) obj;
        if (this.f3460b != advertisingInfo.f3460b) {
            return false;
        }
        if (this.f3459a != null) {
            if (this.f3459a.equals(advertisingInfo.f3459a)) {
                return true;
            }
        } else if (advertisingInfo.f3459a == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        int i = 0;
        if (this.f3459a != null) {
            hashCode = this.f3459a.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode *= 31;
        if (this.f3460b) {
            i = 1;
        }
        return hashCode + i;
    }
}
