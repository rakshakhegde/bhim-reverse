package io.fabric.sdk.android.services.p021b;

/* renamed from: io.fabric.sdk.android.services.b.s */
public class SystemCurrentTimeProvider implements CurrentTimeProvider {
    public long m5515a() {
        return System.currentTimeMillis();
    }
}
