package io.fabric.sdk.android.services.p022a;

import android.content.Context;

/* renamed from: io.fabric.sdk.android.services.a.a */
public abstract class AbstractValueCache<T> implements ValueCache<T> {
    private final ValueCache<T> f3457a;

    protected abstract T m5381a(Context context);

    protected abstract void m5383a(Context context, T t);

    public AbstractValueCache(ValueCache<T> valueCache) {
        this.f3457a = valueCache;
    }

    public final synchronized T m5382a(Context context, ValueLoader<T> valueLoader) {
        T a;
        a = m5381a(context);
        if (a == null) {
            a = this.f3457a != null ? this.f3457a.m5379a(context, valueLoader) : valueLoader.load(context);
            m5380b(context, a);
        }
        return a;
    }

    private void m5380b(Context context, T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        m5383a(context, (Object) t);
    }
}
