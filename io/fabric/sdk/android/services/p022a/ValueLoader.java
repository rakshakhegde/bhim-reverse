package io.fabric.sdk.android.services.p022a;

import android.content.Context;

/* renamed from: io.fabric.sdk.android.services.a.d */
public interface ValueLoader<T> {
    T load(Context context);
}
