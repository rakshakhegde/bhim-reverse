package io.fabric.sdk.android.services.p022a;

import android.content.Context;

/* renamed from: io.fabric.sdk.android.services.a.c */
public interface ValueCache<T> {
    T m5379a(Context context, ValueLoader<T> valueLoader);
}
