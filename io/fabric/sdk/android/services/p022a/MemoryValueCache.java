package io.fabric.sdk.android.services.p022a;

import android.content.Context;

/* renamed from: io.fabric.sdk.android.services.a.b */
public class MemoryValueCache<T> extends AbstractValueCache<T> {
    private T f3458a;

    public MemoryValueCache() {
        this(null);
    }

    public MemoryValueCache(ValueCache<T> valueCache) {
        super(valueCache);
    }

    protected T m5384a(Context context) {
        return this.f3458a;
    }

    protected void m5385a(Context context, T t) {
        this.f3458a = t;
    }
}
