package io.fabric.sdk.android.services.network;

/* renamed from: io.fabric.sdk.android.services.network.c */
public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
