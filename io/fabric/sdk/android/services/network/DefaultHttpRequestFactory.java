package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.DefaultLogger;
import io.fabric.sdk.android.Logger;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.npci.upi.security.pinactivitycomponent.R.R;

/* renamed from: io.fabric.sdk.android.services.network.b */
public class DefaultHttpRequestFactory implements HttpRequestFactory {
    private final Logger f3712a;
    private PinningInfoProvider f3713b;
    private SSLSocketFactory f3714c;
    private boolean f3715d;

    /* renamed from: io.fabric.sdk.android.services.network.b.1 */
    static /* synthetic */ class DefaultHttpRequestFactory {
        static final /* synthetic */ int[] f3711a;

        static {
            f3711a = new int[HttpMethod.values().length];
            try {
                f3711a[HttpMethod.GET.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3711a[HttpMethod.POST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f3711a[HttpMethod.PUT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f3711a[HttpMethod.DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public DefaultHttpRequestFactory() {
        this(new DefaultLogger());
    }

    public DefaultHttpRequestFactory(Logger logger) {
        this.f3712a = logger;
    }

    public void m5700a(PinningInfoProvider pinningInfoProvider) {
        if (this.f3713b != pinningInfoProvider) {
            this.f3713b = pinningInfoProvider;
            m5694a();
        }
    }

    private synchronized void m5694a() {
        this.f3715d = false;
        this.f3714c = null;
    }

    public HttpRequest m5698a(HttpMethod httpMethod, String str) {
        return m5699a(httpMethod, str, Collections.emptyMap());
    }

    public HttpRequest m5699a(HttpMethod httpMethod, String str, Map<String, String> map) {
        HttpRequest a;
        switch (DefaultHttpRequestFactory.f3711a[httpMethod.ordinal()]) {
            case R.View_android_focusable /*1*/:
                a = HttpRequest.m5636a((CharSequence) str, (Map) map, true);
                break;
            case R.View_paddingStart /*2*/:
                a = HttpRequest.m5641b((CharSequence) str, (Map) map, true);
                break;
            case R.View_paddingEnd /*3*/:
                a = HttpRequest.m5644d((CharSequence) str);
                break;
            case R.View_theme /*4*/:
                a = HttpRequest.m5645e((CharSequence) str);
                break;
            default:
                throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (m5695a(str) && this.f3713b != null) {
            SSLSocketFactory b = m5696b();
            if (b != null) {
                ((HttpsURLConnection) a.m5663a()).setSSLSocketFactory(b);
            }
        }
        return a;
    }

    private boolean m5695a(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith("https");
    }

    private synchronized SSLSocketFactory m5696b() {
        if (this.f3714c == null && !this.f3715d) {
            this.f3714c = m5697c();
        }
        return this.f3714c;
    }

    private synchronized SSLSocketFactory m5697c() {
        SSLSocketFactory a;
        this.f3715d = true;
        try {
            a = NetworkUtils.m5701a(this.f3713b);
            this.f3712a.m5284a("Fabric", "Custom SSL pinning enabled");
        } catch (Throwable e) {
            this.f3712a.m5292e("Fabric", "Exception while validating pinned certs", e);
            a = null;
        }
        return a;
    }
}
