package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

public class HttpRequest {
    private static final String[] f3699b;
    private static C0602b f3700c;
    public final URL f3701a;
    private HttpURLConnection f3702d;
    private final String f3703e;
    private C0604d f3704f;
    private boolean f3705g;
    private boolean f3706h;
    private boolean f3707i;
    private int f3708j;
    private String f3709k;
    private int f3710l;

    /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.c */
    protected static abstract class C0599c<V> implements Callable<V> {
        protected abstract V m5624b();

        protected abstract void m5625c();

        protected C0599c() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public V call() {
            /*
            r3 = this;
            r1 = 1;
            r2 = 0;
            r0 = r3.m5624b();	 Catch:{ HttpRequestException -> 0x0011, IOException -> 0x0018, all -> 0x0028 }
            r3.m5625c();	 Catch:{ IOException -> 0x000a }
            return r0;
        L_0x000a:
            r0 = move-exception;
            r1 = new io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException;
            r1.<init>(r0);
            throw r1;
        L_0x0011:
            r0 = move-exception;
            throw r0;	 Catch:{ all -> 0x0013 }
        L_0x0013:
            r0 = move-exception;
        L_0x0014:
            r3.m5625c();	 Catch:{ IOException -> 0x001f }
        L_0x0017:
            throw r0;
        L_0x0018:
            r0 = move-exception;
            r2 = new io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException;	 Catch:{ all -> 0x0013 }
            r2.<init>(r0);	 Catch:{ all -> 0x0013 }
            throw r2;	 Catch:{ all -> 0x0013 }
        L_0x001f:
            r2 = move-exception;
            if (r1 != 0) goto L_0x0017;
        L_0x0022:
            r0 = new io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException;
            r0.<init>(r2);
            throw r0;
        L_0x0028:
            r0 = move-exception;
            r1 = r2;
            goto L_0x0014;
            */
            throw new UnsupportedOperationException("Method not decompiled: io.fabric.sdk.android.services.network.HttpRequest.c.call():V");
        }
    }

    /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.a */
    protected static abstract class C0600a<V> extends C0599c<V> {
        private final Closeable f3692a;
        private final boolean f3693b;

        protected C0600a(Closeable closeable, boolean z) {
            this.f3692a = closeable;
            this.f3693b = z;
        }

        protected void m5626c() {
            if (this.f3692a instanceof Flushable) {
                ((Flushable) this.f3692a).flush();
            }
            if (this.f3693b) {
                try {
                    this.f3692a.close();
                    return;
                } catch (IOException e) {
                    return;
                }
            }
            this.f3692a.close();
        }
    }

    /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.1 */
    class C06011 extends C0600a<HttpRequest> {
        final /* synthetic */ InputStream f3694a;
        final /* synthetic */ OutputStream f3695b;
        final /* synthetic */ HttpRequest f3696c;

        C06011(HttpRequest httpRequest, Closeable closeable, boolean z, InputStream inputStream, OutputStream outputStream) {
            this.f3696c = httpRequest;
            this.f3694a = inputStream;
            this.f3695b = outputStream;
            super(closeable, z);
        }

        public /* synthetic */ Object m5628b() {
            return m5627a();
        }

        public HttpRequest m5627a() {
            byte[] bArr = new byte[this.f3696c.f3708j];
            while (true) {
                int read = this.f3694a.read(bArr);
                if (read == -1) {
                    return this.f3696c;
                }
                this.f3695b.write(bArr, 0, read);
            }
        }
    }

    public static class HttpRequestException extends RuntimeException {
        public /* synthetic */ Throwable getCause() {
            return m5629a();
        }

        protected HttpRequestException(IOException iOException) {
            super(iOException);
        }

        public IOException m5629a() {
            return (IOException) super.getCause();
        }
    }

    /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.b */
    public interface C0602b {
        public static final C0602b f3697a;

        /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.b.1 */
        static class C06031 implements C0602b {
            C06031() {
            }

            public HttpURLConnection m5632a(URL url) {
                return (HttpURLConnection) url.openConnection();
            }

            public HttpURLConnection m5633a(URL url, Proxy proxy) {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        }

        HttpURLConnection m5630a(URL url);

        HttpURLConnection m5631a(URL url, Proxy proxy);

        static {
            f3697a = new C06031();
        }
    }

    /* renamed from: io.fabric.sdk.android.services.network.HttpRequest.d */
    public static class C0604d extends BufferedOutputStream {
        private final CharsetEncoder f3698a;

        public C0604d(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.f3698a = Charset.forName(HttpRequest.m5647f(str)).newEncoder();
        }

        public C0604d m5634a(String str) {
            ByteBuffer encode = this.f3698a.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    static {
        f3699b = new String[0];
        f3700c = C0602b.f3697a;
    }

    private static String m5647f(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    private static StringBuilder m5639a(String str, StringBuilder stringBuilder) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            stringBuilder.append('/');
        }
        return stringBuilder;
    }

    private static StringBuilder m5642b(String str, StringBuilder stringBuilder) {
        int indexOf = str.indexOf(63);
        int length = stringBuilder.length() - 1;
        if (indexOf == -1) {
            stringBuilder.append('?');
        } else if (indexOf < length && str.charAt(length) != '&') {
            stringBuilder.append('&');
        }
        return stringBuilder;
    }

    public static String m5637a(CharSequence charSequence) {
        try {
            URL url = new URL(charSequence.toString());
            String host = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                host = host + ':' + Integer.toString(port);
            }
            try {
                String toASCIIString = new URI(url.getProtocol(), host, url.getPath(), url.getQuery(), null).toASCIIString();
                int indexOf = toASCIIString.indexOf(63);
                if (indexOf > 0 && indexOf + 1 < toASCIIString.length()) {
                    toASCIIString = toASCIIString.substring(0, indexOf + 1) + toASCIIString.substring(indexOf + 1).replace("+", "%2B");
                }
                return toASCIIString;
            } catch (Throwable e) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(e);
                throw new HttpRequestException(iOException);
            }
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public static String m5638a(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map == null || map.isEmpty()) {
            return charSequence2;
        }
        StringBuilder stringBuilder = new StringBuilder(charSequence2);
        m5639a(charSequence2, stringBuilder);
        m5642b(charSequence2, stringBuilder);
        Iterator it = map.entrySet().iterator();
        Entry entry = (Entry) it.next();
        stringBuilder.append(entry.getKey().toString());
        stringBuilder.append('=');
        Object value = entry.getValue();
        if (value != null) {
            stringBuilder.append(value);
        }
        while (it.hasNext()) {
            stringBuilder.append('&');
            entry = (Entry) it.next();
            stringBuilder.append(entry.getKey().toString());
            stringBuilder.append('=');
            value = entry.getValue();
            if (value != null) {
                stringBuilder.append(value);
            }
        }
        return stringBuilder.toString();
    }

    public static HttpRequest m5640b(CharSequence charSequence) {
        return new HttpRequest(charSequence, "GET");
    }

    public static HttpRequest m5636a(CharSequence charSequence, Map<?, ?> map, boolean z) {
        CharSequence a = m5638a(charSequence, (Map) map);
        if (z) {
            a = m5637a(a);
        }
        return m5640b(a);
    }

    public static HttpRequest m5643c(CharSequence charSequence) {
        return new HttpRequest(charSequence, "POST");
    }

    public static HttpRequest m5641b(CharSequence charSequence, Map<?, ?> map, boolean z) {
        CharSequence a = m5638a(charSequence, (Map) map);
        if (z) {
            a = m5637a(a);
        }
        return m5643c(a);
    }

    public static HttpRequest m5644d(CharSequence charSequence) {
        return new HttpRequest(charSequence, "PUT");
    }

    public static HttpRequest m5645e(CharSequence charSequence) {
        return new HttpRequest(charSequence, "DELETE");
    }

    public HttpRequest(CharSequence charSequence, String str) {
        this.f3702d = null;
        this.f3706h = true;
        this.f3707i = false;
        this.f3708j = 8192;
        try {
            this.f3701a = new URL(charSequence.toString());
            this.f3703e = str;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    private Proxy m5648q() {
        return new Proxy(Type.HTTP, new InetSocketAddress(this.f3709k, this.f3710l));
    }

    private HttpURLConnection m5649r() {
        try {
            HttpURLConnection a;
            if (this.f3709k != null) {
                a = f3700c.m5631a(this.f3701a, m5648q());
            } else {
                a = f3700c.m5630a(this.f3701a);
            }
            a.setRequestMethod(this.f3703e);
            return a;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public String toString() {
        return m5688p() + ' ' + m5687o();
    }

    public HttpURLConnection m5663a() {
        if (this.f3702d == null) {
            this.f3702d = m5649r();
        }
        return this.f3702d;
    }

    public int m5664b() {
        try {
            m5683k();
            return m5663a().getResponseCode();
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public boolean m5670c() {
        return 200 == m5664b();
    }

    protected ByteArrayOutputStream m5673d() {
        int j = m5682j();
        if (j > 0) {
            return new ByteArrayOutputStream(j);
        }
        return new ByteArrayOutputStream();
    }

    public String m5662a(String str) {
        OutputStream d = m5673d();
        try {
            m5652a(m5678f(), d);
            return d.toString(m5647f(str));
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public String m5675e() {
        return m5662a(m5680h());
    }

    public BufferedInputStream m5678f() {
        return new BufferedInputStream(m5679g(), this.f3708j);
    }

    public InputStream m5679g() {
        if (m5664b() < 400) {
            try {
                InputStream inputStream = m5663a().getInputStream();
            } catch (IOException e) {
                throw new HttpRequestException(e);
            }
        }
        inputStream = m5663a().getErrorStream();
        if (inputStream == null) {
            try {
                inputStream = m5663a().getInputStream();
            } catch (IOException e2) {
                throw new HttpRequestException(e2);
            }
        }
        if (!this.f3707i || !"gzip".equals(m5681i())) {
            return inputStream;
        }
        try {
            return new GZIPInputStream(inputStream);
        } catch (IOException e22) {
            throw new HttpRequestException(e22);
        }
    }

    public HttpRequest m5651a(int i) {
        m5663a().setConnectTimeout(i);
        return this;
    }

    public HttpRequest m5654a(String str, String str2) {
        m5663a().setRequestProperty(str, str2);
        return this;
    }

    public HttpRequest m5660a(Entry<String, String> entry) {
        return m5654a((String) entry.getKey(), (String) entry.getValue());
    }

    public String m5666b(String str) {
        m5684l();
        return m5663a().getHeaderField(str);
    }

    public int m5668c(String str) {
        return m5650a(str, -1);
    }

    public int m5650a(String str, int i) {
        m5684l();
        return m5663a().getHeaderFieldInt(str, i);
    }

    public String m5667b(String str, String str2) {
        return m5669c(m5666b(str), str2);
    }

    protected String m5669c(String str, String str2) {
        if (str == null || str.length() == 0) {
            return null;
        }
        int length = str.length();
        int indexOf = str.indexOf(59) + 1;
        if (indexOf == 0 || indexOf == length) {
            return null;
        }
        int indexOf2 = str.indexOf(59, indexOf);
        if (indexOf2 == -1) {
            indexOf2 = indexOf;
            indexOf = length;
        } else {
            int i = indexOf2;
            indexOf2 = indexOf;
            indexOf = i;
        }
        while (indexOf2 < indexOf) {
            int indexOf3 = str.indexOf(61, indexOf2);
            if (indexOf3 != -1 && indexOf3 < indexOf && str2.equals(str.substring(indexOf2, indexOf3).trim())) {
                String trim = str.substring(indexOf3 + 1, indexOf).trim();
                indexOf3 = trim.length();
                if (indexOf3 != 0) {
                    if (indexOf3 > 2 && '\"' == trim.charAt(0) && '\"' == trim.charAt(indexOf3 - 1)) {
                        return trim.substring(1, indexOf3 - 1);
                    }
                    return trim;
                }
            }
            indexOf++;
            indexOf2 = str.indexOf(59, indexOf);
            if (indexOf2 == -1) {
                indexOf2 = length;
            }
            i = indexOf2;
            indexOf2 = indexOf;
            indexOf = i;
        }
        return null;
    }

    public String m5680h() {
        return m5667b("Content-Type", "charset");
    }

    public HttpRequest m5661a(boolean z) {
        m5663a().setUseCaches(z);
        return this;
    }

    public String m5681i() {
        return m5666b("Content-Encoding");
    }

    public HttpRequest m5671d(String str) {
        return m5672d(str, null);
    }

    public HttpRequest m5672d(String str, String str2) {
        if (str2 == null || str2.length() <= 0) {
            return m5654a("Content-Type", str);
        }
        String str3 = "; charset=";
        return m5654a("Content-Type", str + "; charset=" + str2);
    }

    public int m5682j() {
        return m5668c("Content-Length");
    }

    protected HttpRequest m5652a(InputStream inputStream, OutputStream outputStream) {
        return (HttpRequest) new C06011(this, inputStream, this.f3706h, inputStream, outputStream).call();
    }

    protected HttpRequest m5683k() {
        if (this.f3704f != null) {
            if (this.f3705g) {
                this.f3704f.m5634a("\r\n--00content0boundary00--\r\n");
            }
            if (this.f3706h) {
                try {
                    this.f3704f.close();
                } catch (IOException e) {
                }
            } else {
                this.f3704f.close();
            }
            this.f3704f = null;
        }
        return this;
    }

    protected HttpRequest m5684l() {
        try {
            return m5683k();
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    protected HttpRequest m5685m() {
        if (this.f3704f == null) {
            m5663a().setDoOutput(true);
            this.f3704f = new C0604d(m5663a().getOutputStream(), m5669c(m5663a().getRequestProperty("Content-Type"), "charset"), this.f3708j);
        }
        return this;
    }

    protected HttpRequest m5686n() {
        if (this.f3705g) {
            this.f3704f.m5634a("\r\n--00content0boundary00\r\n");
        } else {
            this.f3705g = true;
            m5671d("multipart/form-data; boundary=00content0boundary00").m5685m();
            this.f3704f.m5634a("--00content0boundary00\r\n");
        }
        return this;
    }

    protected HttpRequest m5656a(String str, String str2, String str3) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("form-data; name=\"").append(str);
        if (str2 != null) {
            stringBuilder.append("\"; filename=\"").append(str2);
        }
        stringBuilder.append('\"');
        m5677f("Content-Disposition", stringBuilder.toString());
        if (str3 != null) {
            m5677f("Content-Type", str3);
        }
        return m5676f((CharSequence) "\r\n");
    }

    public HttpRequest m5674e(String str, String str2) {
        return m5665b(str, null, str2);
    }

    public HttpRequest m5665b(String str, String str2, String str3) {
        return m5659a(str, str2, null, str3);
    }

    public HttpRequest m5659a(String str, String str2, String str3, String str4) {
        try {
            m5686n();
            m5656a(str, str2, str3);
            this.f3704f.m5634a(str4);
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public HttpRequest m5653a(String str, Number number) {
        return m5655a(str, null, number);
    }

    public HttpRequest m5655a(String str, String str2, Number number) {
        return m5665b(str, str2, number != null ? number.toString() : null);
    }

    public HttpRequest m5657a(String str, String str2, String str3, File file) {
        InputStream bufferedInputStream;
        IOException e;
        Throwable th;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            try {
                HttpRequest a = m5658a(str, str2, str3, bufferedInputStream);
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException e2) {
                    }
                }
                return a;
            } catch (IOException e3) {
                e = e3;
                try {
                    throw new HttpRequestException(e);
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedInputStream != null) {
                        try {
                            bufferedInputStream.close();
                        } catch (IOException e4) {
                        }
                    }
                    throw th;
                }
            }
        } catch (IOException e5) {
            e = e5;
            bufferedInputStream = null;
            throw new HttpRequestException(e);
        } catch (Throwable th3) {
            th = th3;
            bufferedInputStream = null;
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }
            throw th;
        }
    }

    public HttpRequest m5658a(String str, String str2, String str3, InputStream inputStream) {
        try {
            m5686n();
            m5656a(str, str2, str3);
            m5652a(inputStream, this.f3704f);
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public HttpRequest m5677f(String str, String str2) {
        return m5676f((CharSequence) str).m5676f((CharSequence) ": ").m5676f((CharSequence) str2).m5676f((CharSequence) "\r\n");
    }

    public HttpRequest m5676f(CharSequence charSequence) {
        try {
            m5685m();
            this.f3704f.m5634a(charSequence.toString());
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    public URL m5687o() {
        return m5663a().getURL();
    }

    public String m5688p() {
        return m5663a().getRequestMethod();
    }
}
