package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.Fabric;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/* renamed from: io.fabric.sdk.android.services.network.g */
class PinningTrustManager implements X509TrustManager {
    private static final X509Certificate[] f3721a;
    private final TrustManager[] f3722b;
    private final SystemKeyStore f3723c;
    private final long f3724d;
    private final List<byte[]> f3725e;
    private final Set<X509Certificate> f3726f;

    static {
        f3721a = new X509Certificate[0];
    }

    public PinningTrustManager(SystemKeyStore systemKeyStore, PinningInfoProvider pinningInfoProvider) {
        this.f3725e = new LinkedList();
        this.f3726f = Collections.synchronizedSet(new HashSet());
        this.f3722b = m5706a(systemKeyStore);
        this.f3723c = systemKeyStore;
        this.f3724d = pinningInfoProvider.getPinCreationTimeInMillis();
        for (String a : pinningInfoProvider.getPins()) {
            this.f3725e.add(m5705a(a));
        }
    }

    private TrustManager[] m5706a(SystemKeyStore systemKeyStore) {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance("X509");
            instance.init(systemKeyStore.f3727a);
            return instance.getTrustManagers();
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (KeyStoreException e2) {
            throw new AssertionError(e2);
        }
    }

    private boolean m5704a(X509Certificate x509Certificate) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(x509Certificate.getPublicKey().getEncoded());
            for (byte[] equals : this.f3725e) {
                if (Arrays.equals(equals, digest)) {
                    return true;
                }
            }
            return false;
        } catch (Throwable e) {
            throw new CertificateException(e);
        }
    }

    private void m5703a(X509Certificate[] x509CertificateArr, String str) {
        for (TrustManager trustManager : this.f3722b) {
            ((X509TrustManager) trustManager).checkServerTrusted(x509CertificateArr, str);
        }
    }

    private void m5702a(X509Certificate[] x509CertificateArr) {
        if (this.f3724d == -1 || System.currentTimeMillis() - this.f3724d <= 15552000000L) {
            X509Certificate[] a = CertificateChainCleaner.m5690a(x509CertificateArr, this.f3723c);
            int length = a.length;
            int i = 0;
            while (i < length) {
                if (!m5704a(a[i])) {
                    i++;
                } else {
                    return;
                }
            }
            throw new CertificateException("No valid pins found in chain!");
        }
        Fabric.m5322h().m5289d("Fabric", "Certificate pins are stale, (" + (System.currentTimeMillis() - this.f3724d) + " millis vs " + 15552000000L + " millis) " + "falling back to system trust.");
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        throw new CertificateException("Client certificates not supported!");
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (!this.f3726f.contains(x509CertificateArr[0])) {
            m5703a(x509CertificateArr, str);
            m5702a(x509CertificateArr);
            this.f3726f.add(x509CertificateArr[0]);
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return f3721a;
    }

    private byte[] m5705a(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
