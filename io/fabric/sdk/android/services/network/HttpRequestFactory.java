package io.fabric.sdk.android.services.network;

import java.util.Map;

/* renamed from: io.fabric.sdk.android.services.network.d */
public interface HttpRequestFactory {
    HttpRequest m5691a(HttpMethod httpMethod, String str);

    HttpRequest m5692a(HttpMethod httpMethod, String str, Map<String, String> map);

    void m5693a(PinningInfoProvider pinningInfoProvider);
}
