package io.fabric.sdk.android.services.p019c;

import android.content.Context;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.CurrentTimeProvider;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: io.fabric.sdk.android.services.c.b */
public abstract class EventsFilesManager<T> {
    public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
    public static final int MAX_FILES_IN_BATCH = 1;
    public static final int MAX_FILES_TO_KEEP = 100;
    public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
    protected final Context context;
    protected final CurrentTimeProvider currentTimeProvider;
    private final int defaultMaxFilesToKeep;
    protected final EventsStorage eventStorage;
    protected volatile long lastRollOverTime;
    protected final List<EventsStorageListener> rollOverListeners;
    protected final EventTransform<T> transform;

    /* renamed from: io.fabric.sdk.android.services.c.b.1 */
    class EventsFilesManager implements Comparator<EventsFilesManager> {
        final /* synthetic */ EventsFilesManager f3548a;

        EventsFilesManager(EventsFilesManager eventsFilesManager) {
            this.f3548a = eventsFilesManager;
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m5519a((EventsFilesManager) obj, (EventsFilesManager) obj2);
        }

        public int m5519a(EventsFilesManager eventsFilesManager, EventsFilesManager eventsFilesManager2) {
            return (int) (eventsFilesManager.f3550b - eventsFilesManager2.f3550b);
        }
    }

    /* renamed from: io.fabric.sdk.android.services.c.b.a */
    static class EventsFilesManager {
        final File f3549a;
        final long f3550b;

        public EventsFilesManager(File file, long j) {
            this.f3549a = file;
            this.f3550b = j;
        }
    }

    protected abstract String generateUniqueRollOverFileName();

    public EventsFilesManager(Context context, EventTransform<T> eventTransform, CurrentTimeProvider currentTimeProvider, EventsStorage eventsStorage, int i) {
        this.rollOverListeners = new CopyOnWriteArrayList();
        this.context = context.getApplicationContext();
        this.transform = eventTransform;
        this.eventStorage = eventsStorage;
        this.currentTimeProvider = currentTimeProvider;
        this.lastRollOverTime = this.currentTimeProvider.m5459a();
        this.defaultMaxFilesToKeep = i;
    }

    public void writeEvent(T t) {
        byte[] toBytes = this.transform.toBytes(t);
        rollFileOverIfNeeded(toBytes.length);
        this.eventStorage.m5524a(toBytes);
    }

    public void registerRollOverListener(EventsStorageListener eventsStorageListener) {
        if (eventsStorageListener != null) {
            this.rollOverListeners.add(eventsStorageListener);
        }
    }

    public boolean rollFileOver() {
        boolean z = true;
        String str = null;
        if (this.eventStorage.m5526b()) {
            z = false;
        } else {
            str = generateUniqueRollOverFileName();
            this.eventStorage.m5522a(str);
            Object[] objArr = new Object[MAX_FILES_IN_BATCH];
            objArr[0] = str;
            CommonUtils.m5428a(this.context, 4, "Fabric", String.format(Locale.US, "generated new file %s", objArr));
            this.lastRollOverTime = this.currentTimeProvider.m5459a();
        }
        triggerRollOverOnListeners(str);
        return z;
    }

    private void rollFileOverIfNeeded(int i) {
        if (!this.eventStorage.m5525a(i, getMaxByteSizePerFile())) {
            CommonUtils.m5428a(this.context, 4, "Fabric", String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.eventStorage.m5520a()), Integer.valueOf(i), Integer.valueOf(getMaxByteSizePerFile())}));
            rollFileOver();
        }
    }

    protected int getMaxFilesToKeep() {
        return this.defaultMaxFilesToKeep;
    }

    protected int getMaxByteSizePerFile() {
        return MAX_BYTE_SIZE_PER_FILE;
    }

    public long getLastRollOverTime() {
        return this.lastRollOverTime;
    }

    private void triggerRollOverOnListeners(String str) {
        for (EventsStorageListener onRollOver : this.rollOverListeners) {
            try {
                onRollOver.onRollOver(str);
            } catch (Throwable e) {
                CommonUtils.m5430a(this.context, "One of the roll over listeners threw an exception", e);
            }
        }
    }

    public List<File> getBatchOfFilesToSend() {
        return this.eventStorage.m5521a((int) MAX_FILES_IN_BATCH);
    }

    public void deleteSentFiles(List<File> list) {
        this.eventStorage.m5523a((List) list);
    }

    public void deleteAllEventsFiles() {
        this.eventStorage.m5523a(this.eventStorage.m5527c());
        this.eventStorage.m5528d();
    }

    public void deleteOldestInRollOverIfOverMax() {
        List<File> c = this.eventStorage.m5527c();
        int maxFilesToKeep = getMaxFilesToKeep();
        if (c.size() > maxFilesToKeep) {
            int size = c.size() - maxFilesToKeep;
            CommonUtils.m5429a(this.context, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(c.size()), Integer.valueOf(maxFilesToKeep), Integer.valueOf(size)}));
            TreeSet treeSet = new TreeSet(new EventsFilesManager(this));
            for (File file : c) {
                treeSet.add(new EventsFilesManager(file, parseCreationTimestampFromFileName(file.getName())));
            }
            List arrayList = new ArrayList();
            Iterator it = treeSet.iterator();
            while (it.hasNext()) {
                arrayList.add(((EventsFilesManager) it.next()).f3549a);
                if (arrayList.size() == size) {
                    break;
                }
            }
            this.eventStorage.m5523a(arrayList);
        }
    }

    public long parseCreationTimestampFromFileName(String str) {
        long j = 0;
        String[] split = str.split(ROLL_OVER_FILE_NAME_SEPARATOR);
        if (split.length == 3) {
            try {
                j = Long.valueOf(split[2]).longValue();
            } catch (NumberFormatException e) {
            }
        }
        return j;
    }
}
