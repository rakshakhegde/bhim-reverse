package io.fabric.sdk.android.services.p019c;

import java.io.File;
import java.util.List;

/* renamed from: io.fabric.sdk.android.services.c.f */
public interface FilesSender {
    boolean send(List<File> list);
}
