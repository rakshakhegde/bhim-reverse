package io.fabric.sdk.android.services.p019c;

import android.content.Context;
import io.fabric.sdk.android.services.p021b.CommonUtils;
import io.fabric.sdk.android.services.p021b.QueueFile;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: io.fabric.sdk.android.services.c.h */
public class QueueFileEventStorage implements EventsStorage {
    private final Context f3551a;
    private final File f3552b;
    private final String f3553c;
    private final File f3554d;
    private QueueFile f3555e;
    private File f3556f;

    public QueueFileEventStorage(Context context, File file, String str, String str2) {
        this.f3551a = context;
        this.f3552b = file;
        this.f3553c = str2;
        this.f3554d = new File(this.f3552b, str);
        this.f3555e = new QueueFile(this.f3554d);
        m5530e();
    }

    private void m5530e() {
        this.f3556f = new File(this.f3552b, this.f3553c);
        if (!this.f3556f.exists()) {
            this.f3556f.mkdirs();
        }
    }

    public void m5536a(byte[] bArr) {
        this.f3555e.m5508a(bArr);
    }

    public int m5531a() {
        return this.f3555e.m5506a();
    }

    public void m5534a(String str) {
        this.f3555e.close();
        m5529a(this.f3554d, new File(this.f3556f, str));
        this.f3555e = new QueueFile(this.f3554d);
    }

    private void m5529a(File file, File file2) {
        Closeable fileInputStream;
        Throwable th;
        Closeable closeable = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                closeable = m5532a(file2);
                CommonUtils.m5434a((InputStream) fileInputStream, (OutputStream) closeable, new byte[1024]);
                CommonUtils.m5432a(fileInputStream, "Failed to close file input stream");
                CommonUtils.m5432a(closeable, "Failed to close output stream");
                file.delete();
            } catch (Throwable th2) {
                th = th2;
                CommonUtils.m5432a(fileInputStream, "Failed to close file input stream");
                CommonUtils.m5432a(closeable, "Failed to close output stream");
                file.delete();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            CommonUtils.m5432a(fileInputStream, "Failed to close file input stream");
            CommonUtils.m5432a(closeable, "Failed to close output stream");
            file.delete();
            throw th;
        }
    }

    public OutputStream m5532a(File file) {
        return new FileOutputStream(file);
    }

    public List<File> m5533a(int i) {
        List<File> arrayList = new ArrayList();
        for (Object add : this.f3556f.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    public void m5535a(List<File> list) {
        for (File file : list) {
            CommonUtils.m5429a(this.f3551a, String.format("deleting sent analytics file %s", new Object[]{file.getName()}));
            file.delete();
        }
    }

    public List<File> m5539c() {
        return Arrays.asList(this.f3556f.listFiles());
    }

    public void m5540d() {
        try {
            this.f3555e.close();
        } catch (IOException e) {
        }
        this.f3554d.delete();
    }

    public boolean m5538b() {
        return this.f3555e.m5511b();
    }

    public boolean m5537a(int i, int i2) {
        return this.f3555e.m5510a(i, i2);
    }
}
