package io.fabric.sdk.android.services.p019c;

import java.io.File;
import java.util.List;

/* renamed from: io.fabric.sdk.android.services.c.c */
public interface EventsStorage {
    int m5520a();

    List<File> m5521a(int i);

    void m5522a(String str);

    void m5523a(List<File> list);

    void m5524a(byte[] bArr);

    boolean m5525a(int i, int i2);

    boolean m5526b();

    List<File> m5527c();

    void m5528d();
}
