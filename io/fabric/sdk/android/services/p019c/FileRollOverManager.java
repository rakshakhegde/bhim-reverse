package io.fabric.sdk.android.services.p019c;

/* renamed from: io.fabric.sdk.android.services.c.e */
public interface FileRollOverManager {
    void cancelTimeBasedFileRollOver();

    boolean rollFileOver();
}
