package io.fabric.sdk.android.services.p019c;

import android.content.Context;
import io.fabric.sdk.android.services.p021b.CommonUtils;

/* renamed from: io.fabric.sdk.android.services.c.i */
public class TimeBasedFileRollOverRunnable implements Runnable {
    private final Context f3557a;
    private final FileRollOverManager f3558b;

    public TimeBasedFileRollOverRunnable(Context context, FileRollOverManager fileRollOverManager) {
        this.f3557a = context;
        this.f3558b = fileRollOverManager;
    }

    public void run() {
        try {
            CommonUtils.m5429a(this.f3557a, "Performing time based file roll over.");
            if (!this.f3558b.rollFileOver()) {
                this.f3558b.cancelTimeBasedFileRollOver();
            }
        } catch (Throwable e) {
            CommonUtils.m5430a(this.f3557a, "Failed to roll over file", e);
        }
    }
}
