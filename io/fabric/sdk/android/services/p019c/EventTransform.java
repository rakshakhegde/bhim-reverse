package io.fabric.sdk.android.services.p019c;

/* renamed from: io.fabric.sdk.android.services.c.a */
public interface EventTransform<T> {
    byte[] toBytes(T t);
}
