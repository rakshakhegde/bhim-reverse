package io.fabric.sdk.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

/* renamed from: io.fabric.sdk.android.a */
public class ActivityLifecycleManager {
    private final Application f3393a;
    private ActivityLifecycleManager f3394b;

    /* renamed from: io.fabric.sdk.android.a.b */
    public static abstract class ActivityLifecycleManager {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }
    }

    /* renamed from: io.fabric.sdk.android.a.a */
    private static class ActivityLifecycleManager {
        private final Set<ActivityLifecycleCallbacks> f3391a;
        private final Application f3392b;

        /* renamed from: io.fabric.sdk.android.a.a.1 */
        class ActivityLifecycleManager implements ActivityLifecycleCallbacks {
            final /* synthetic */ ActivityLifecycleManager f3389a;
            final /* synthetic */ ActivityLifecycleManager f3390b;

            ActivityLifecycleManager(ActivityLifecycleManager activityLifecycleManager, ActivityLifecycleManager activityLifecycleManager2) {
                this.f3390b = activityLifecycleManager;
                this.f3389a = activityLifecycleManager2;
            }

            public void onActivityCreated(Activity activity, Bundle bundle) {
                this.f3389a.onActivityCreated(activity, bundle);
            }

            public void onActivityStarted(Activity activity) {
                this.f3389a.onActivityStarted(activity);
            }

            public void onActivityResumed(Activity activity) {
                this.f3389a.onActivityResumed(activity);
            }

            public void onActivityPaused(Activity activity) {
                this.f3389a.onActivityPaused(activity);
            }

            public void onActivityStopped(Activity activity) {
                this.f3389a.onActivityStopped(activity);
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                this.f3389a.onActivitySaveInstanceState(activity, bundle);
            }

            public void onActivityDestroyed(Activity activity) {
                this.f3389a.onActivityDestroyed(activity);
            }
        }

        ActivityLifecycleManager(Application application) {
            this.f3391a = new HashSet();
            this.f3392b = application;
        }

        @TargetApi(14)
        private void m5276a() {
            for (ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : this.f3391a) {
                this.f3392b.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }

        @TargetApi(14)
        private boolean m5279a(ActivityLifecycleManager activityLifecycleManager) {
            if (this.f3392b == null) {
                return false;
            }
            ActivityLifecycleCallbacks activityLifecycleManager2 = new ActivityLifecycleManager(this, activityLifecycleManager);
            this.f3392b.registerActivityLifecycleCallbacks(activityLifecycleManager2);
            this.f3391a.add(activityLifecycleManager2);
            return true;
        }
    }

    public ActivityLifecycleManager(Context context) {
        this.f3393a = (Application) context.getApplicationContext();
        if (VERSION.SDK_INT >= 14) {
            this.f3394b = new ActivityLifecycleManager(this.f3393a);
        }
    }

    public boolean m5281a(ActivityLifecycleManager activityLifecycleManager) {
        return this.f3394b != null && this.f3394b.m5279a(activityLifecycleManager);
    }

    public void m5280a() {
        if (this.f3394b != null) {
            this.f3394b.m5276a();
        }
    }
}
